<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
	<style>
		@page { margin: 50px 50px; }
		#header { 
			position: fixed; 
			left: 0px; 
			top: -30px; 
			right: 0px; 
			height: 30px;
			text-align: left;
			font-size: 12pt;
			margin-bottom: 10px;
			border-bottom: 1px solid black;
		}
		#footer { 
			position: fixed; 
			left: 0px; 
			bottom: -30px; 
			right: 0px; 
			height: 30px;
			text-align: right;
		}
		#user{
			position: fixed; 
			right: 0px;
			top: -40px; 
			height: 30px; 
			text-align: right;
			font-size: 8pt;
		}
		body { 
			font-family: verdana, sans-serif;
			font-size: 8pt;
			padding-top: 20px;
		} 
		
		h1{
			width: 100%;
			margin-bottom: 20px;
			text-align: center;
		}
		small{
			color: grey;
		}
		hr{
			margin-top: 15px;
		}
		table{
			margin-bottom: 10px;
		}
		.pdf-agenda-classe{
			page-break-before: always;
		}
	</style>
</head>
<body>
	<div id="header">		
		GestionDeClasses<small>.net</small> <img src='views/pdf/apple-touch-icon-57x57.jpg' width='25px'/>	
		<div id="user">
			<strong><?= ucfirst($user['user_prenom'])." ".$user['user_nom']; ?>		
				<br/>
				<?= ucfirst($user['user_matiere']); ?>
			</strong>
		</div>
	</div>
	<div id="footer">
	</div>
	<?php 
	echo utf8_encode($html_data);
	?>
</body>
</html>