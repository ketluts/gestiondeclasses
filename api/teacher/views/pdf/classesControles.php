<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
	<style>
		body { 
			font-family: verdana, sans-serif;
			font-size: 8pt;
		} 
		table {
			border: 1pt solid black;
			border-spacing: 0pt;
		}
		thead td{			
			font-weight: bold;
			overflow: hidden;
			text-align: center;
		}		
		td{
			position: relative;
			border: 1pt solid black;
			padding: 0pt;
			width: 60pt;
			text-align: center;
		}
		tr:nth-child(even) {
			background-color: #bdc3c7;
		}
		.controle_titre{	
			padding: 5pt;
			text-align: center;
		}
		.moyenne_alerte{
			background-color: #e74c3c;
		}
		.moyenne_success{
			background-color: #2ecc71;	
		}
		td:first-child {
			font-weight: bold;
		}
		tr:last-child {
			font-weight: bold;
		}
	</style>
</head>
<body>
	<?php 
	$nb_periodes=count($controlesByPeriodes);
	$periode_num=0;
	foreach ($controlesByPeriodes as $controles) {
		$nb_controles=count($controles);
		$periode_id=$controles[0]['controle_periode'];
		$sommeMoyennes=array();
		foreach ($eleves as $eleve) {
			$moyenne="-";
			if(isset($eleve['notes'][$periode_id])){
				$moyenne=moyenneWithCoefficients($eleve['notes'][$periode_id]);
			}
			if($moyenne!=="-"){
				$sommeMoyennes[]=(float)$moyenne;
			}
			$eleves[$eleve['eleve_id']]['moyenne']=$moyenne;
		}
		?>
		<h1>
			<?php
			if($periode_id==='-1'){
				echo "Non classés";
			}else{
				echo $periodes[$periode_id];
			}
			; 
			?>
		</h1>
		<table>
			<thead>
				<tr>
					<td>
						<?= strtoupper($classe['classe_nom']); ?>
						<br/>
						<?= strtoupper($_['pseudo_utilisateur']); ?>
						<br/>
						<?= ucfirst(utf8_to_octal($user['user_matiere'])); ?>
					</td>
					<td>
						<div class="controle_titre">
							Moyenne
						</div>
					</td>
					<?php for ($i=0; $i < $nb_controles; $i++) { 
						?>
						<td>
							<div class="controle_titre">
								<?= $controles[$i]['controle_titre']; ?>
								<br/>
								Coeff : <?= $controles[$i]['controle_coefficient']; ?>
								<br/>
								Noté sur <?= $controles[$i]['controle_note']; ?>
							</div>

						</td>
						<?php
					}
					?>
				</tr>
			</thead>
			<?php 
			$n=0;
			foreach ($eleves as $eleve) { 
				if(!isset($eleve['eleve_nom'])){
					continue;
				}
				?>
				<tr>
					<td>
						<?= ucfirst($eleve['eleve_nom']); ?> <?= ucfirst($eleve['eleve_prenom']); ?>
					</td>
					<?php

					if($eleve['moyenne']!=="-"){
						if($eleve['moyenne']<10){$mclass=" moyenne_alerte";}else{$mclass=" moyenne_success";} 
					}
					else{
						$mclass="";
					}
					?>
					<td class="<?= $mclass; ?>">
						<?php 
						echo $eleve['moyenne']; 
						?>
					</td>
					<?php for ($i=0; $i < $nb_controles; $i++) { 
						if(isset($eleve['notes'][$periode_id][$controles[$i]['controle_id']])){
							?>
							<td>
								<?php 
								if(is_numeric($eleve['notes'][$periode_id][$controles[$i]['controle_id']]['value'])){
									echo $eleve['notes'][$periode_id][$controles[$i]['controle_id']]['value']+$eleve['notes'][$periode_id][$controles[$i]['controle_id']]['bonus']; 
								}
								else{
									echo $eleve['notes'][$periode_id][$controles[$i]['controle_id']]['value'];
								}
								?>
							</td>
							<?php
						}
						else{
							?>
							<td>
								-
							</td>
							<?php
						}
					}
					?>
				</tr>
				<?php 
				$n++;
			} ?>
			<tr>
				<td>
					Moyenne
				</td>
				<td>
					<?= round(array_sum($sommeMoyennes)/count($sommeMoyennes),2); ?>
				</td>
				<?php foreach ($controles as $controle) { ?>
				<td>
					<?= $controle['moyenne']; ?>
				</td>
				<?php
			}
			?>
		</tr>
	</table>
	<?php
	$periode_num++;
	if($periode_num!=$nb_periodes){
		?>
		<div style="page-break-after:always;"></div>
		<?php
	}
	?>
	<?php
}
?>
</body>
</html>