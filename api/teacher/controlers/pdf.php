<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Pdf {
     /**
     *
     * @var Instance
     */
     private static $_instance;
    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return PDF
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
      return self::$_instance;
    }
    public function a4(){
     global $_, $user;
     MainControl::init('users','get');
     $html_data=$_POST['export-pdf-data'];
     $snappy = new Knp\Snappy\Pdf('lib/wkhtmltox/bin/wkhtmltopdf');
     if(!isset($_['export-pdf-orientation'])){
        $_['export-pdf-orientation']="Landscape";
     }
     header('Content-Type: application/pdf');
     header('Content-Disposition: attachment; filename="gdc_'.$_['export-pdf-titre'].'.pdf"');
     echo $snappy->getOutputFromHtml($html_data,
       array('orientation'=>$_['export-pdf-orientation'],
         'footer-center' => 'Page [page] de [toPage]',
         'footer-font-size' => 8
       )
     );
     exit;
   }
 }
 ?>