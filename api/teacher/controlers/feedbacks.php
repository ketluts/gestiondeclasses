<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Feedbacks {
     /**
     *
     * @var Instance
     */
     private static $_instance;
    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Filters
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
      return self::$_instance;
    }
    public function add() {
     global $_, $user;
     MainControl::init('users','get');
     if(!isset($_['feedback'])){
      return;
    }
    $feedback=json_decode($_['feedback'],true);
    $feedback['feedback_date']=$_['time'];
    addFeedback($feedback);
    MainControl::init('feedbacks','getAll');
  }
  public function getAll(){
    MainControl::init('users','get'); 
    $_SESSION['render']['feedbacks']=getFeedbacksByUser();
  }
  public function delete() {
   global $_, $user;
   MainControl::init('users','get');
   if(!isset($_['feedback_id'])){
    return;
  }          
  deleteFeedback($_['feedback_id']);
  MainControl::init('feedbacks','getAll');
}
public function print() {
 global $_, $user;
 MainControl::init('users','get');
 if(!isset($_['feedback_id'])){
  return;
}         
$feedbacks=getFeedbackById($_['feedback_id']) ;
$dir=strtolower(substr($_['nom_etablissement'],0,1));
$dir_path=_DATA_."schools/".$dir.'/'.$_['nom_etablissement']."/files/";
$html="<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'>
<head><title>Microsoft Office HTML Example</title>
<meta charset='UTF-8' /> 
<style> <!-- 
@page
{
  size: 21cm 29.7cm;  /* A4 */
  margin: 1cm 1cm 1cm 1cm; /* Margins: 1 cm on each side */
  mso-page-orientation: portrait;  
}
@page Section1 { }
div.Section1 { page:Section1; }
h2{    
 font-size: 14pt;      
 font-weight: bold;
}
-->
</style>
</head>
<body>";
$data=json_decode($feedbacks['feedback_data'],true);

$feedbacksByEleves=array();
foreach ($data['liste'] as $feedback) {
  if(!isset($feedbacksByEleves[$feedback['eleve_id']])){
    $feedbacksByEleves[$feedback['eleve_id']]=array();
  }
  $feedbacksByEleves[$feedback['eleve_id']][]=$feedback;
}
$toZip=array();
foreach ($feedbacksByEleves as $eleveFeedbacks) {
  $eleve=getEleveById($eleveFeedbacks[0]['eleve_id']);
  $html.="<h2>".$eleve['eleve_prenom']." ".$eleve['eleve_nom'].' / '.$feedbacks['feedback_code']."</h2>";
  $html.="<ul>"; 
  foreach ($eleveFeedbacks as $feedback) {
    $item=getItemById($feedback['item_id']);  
    $html.="<li>"; 
    $html.="<u>".$item['item_name']."</u>";
    if($feedback['activity_file']>=0){  
     $file=getFileById($feedback['activity_file']); 
     $toZip[]=$file;
     $filename = $dir_path.$file['file_name'];
     if(in_array($file['file_type'],['jpg','jpeg','gif','png'])){
      $img=base64_encode(file_get_contents($filename));
      $html.="<img src='data:image/".$file['file_type'].";base64,".$img."' style='max-width:100%;'/>";
    }
    else{
      $html.="<p>".$file['file_fullname']."</p>";
    }
  }else{    
    $html.="</br></br></br></br></br></br>";
  }
}
$html.="</li>"; 
$html.="</ul>"; 
}
$html.="</body>
</html>";
$returnName='Feedback - '.$feedbacks['feedback_code'];
file_put_contents($dir_path.$returnName.'.odt', $html);

$zip = new ZipArchive();
if ($zip->open($dir_path.$returnName.'.zip', ZipArchive::CREATE) === true) {
 $zip->addFile($dir_path.$returnName.'.odt',$returnName.'.odt');
 foreach ($toZip as $file) {
   $zip->addFile($dir_path.$file['file_name'],$file['file_fullname']);
 }
 $zip->setArchiveComment(time());
 $zip->close();
}
unlink($dir_path.$returnName.'.odt');
$file=[];
$file['file_name']=$returnName.'.zip';
$file['file_fullname']=$returnName.'.zip';
downloadFile($file);
exit;
}
}
?>