<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Notes {
     /**
     *
     * @var Instance
     */
     private static $_instance;
    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Notes
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self;
        return self::$_instance;
      }
      public function getAll(){
     global $_, $user;
        MainControl::init('users','get');      
        MainControl::init('periodes','getMillesime');
        $select="note_id, note_eleve, note_controle, note_value, note_date, note_bonus";
        $_SESSION['render']['notes']=getAllNotes($select);
      }
      public function update(){
     global $_, $user;
        MainControl::init('users','get');
        $note['note_id']=$_['note_id'];
        $controle=getControlesById(getNoteById($_['note_id'])['note_controle']);
        if($controle['controle_user']!=$user['user_id']){return false;}
        isPeriodeLock($controle['controle_periode']);
        if(trim($_['note_value'])==""){
          deleteNote($note);
          MainControl::init('notes','getAll');
        }
        else{
         $note['note_value']=preg_replace("#,#",".",$_['note_value']);
         $note['note_bonus']=preg_replace("#,#",".",$_['note_bonus']);
         updateNote($note);
       }
     }
     public function add(){
     global $_, $user;
      MainControl::init('users','get');
      $notes=json_decode($_POST['notes'],true);
      $controle=getControlesById($_['controle_id']);
      if($controle['controle_user']!=$user['user_id']){return false;}
      isPeriodeLock($controle['controle_periode']);
      $time=time();
      foreach ($notes as $note) {
        $temp_note=[];
        $temp_note['note_eleve']=$note[0];
        $temp_note['note_controle']=$_['controle_id'];
        $temp_note['note_value']=preg_replace("#,#",".",$note[1]);
        $temp_note['note_date']=$time;
        addnote($temp_note);
      }
      MainControl::init('notes','getAll');
    }   
  }
  ?>