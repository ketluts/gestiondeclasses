<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Flux {
          /**
     *
     * @var Instance
     */
          private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Flux
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
      return self::$_instance;
    }
    public function init() {
      global $_;
      $_['nom_etablissement']=$_['e'];
      if(!isset($_['t'])){  
       exit;
     }
     $defi=getDefiByToken($_['t']);
     if($defi==null){     
       exit;
     }
     require_once('lib/feed2array/Array2feed.php');
     if($defi['defi_flux']!=null){
      echo array2feed(json_decode($defi['defi_flux'],true));
      exit;
    }
    else{
      $array=array(
        'infos'=>array(
          'type'=>'rss',
          'description'=>'Liste des élèves qui ont participé au défi "'.$defi['defi_titre'].'"',
          'title'=>$defi['defi_titre'],
          'link'=>$defi['defi_url'],
        ),
        'items'=>array(             
        )
      );
      echo array2feed($array);
      exit;
    }
  }
}
?>