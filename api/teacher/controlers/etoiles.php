<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Etoiles {
     /**
     *
     * @var Instance
     */
     private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}

    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}

    /**
     * 
     * @return Etoiles
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
      return self::$_instance;
    }
    public function getAll(){
     global $_, $user;
     MainControl::init('users','get');
     $select="etoile_id,etoile_value,etoile_state,etoile_eleve, etoile_defi, etoile_date,etoile_type";
     $_SESSION['render']['etoiles']=getAllEtoiles($select);
   }
   public function add(){
     global $_, $user;
     MainControl::init('users','get');
     $etoiles=json_decode($_POST['etoiles'],true);
     if(!isset($_['etoile_type'])){
      $_['etoile_type']="defi";
    }
    if($_['etoile_type']=="defi"){
     $defi=getDefiById($_['defi_id']);
     if(isset($defi['defi_user']) && $defi['defi_user']!=$user['user_id']){
      MainControl::init('render');        
      exit;
    }
    $etoile_type_id=$defi['defi_id'];
    $max=$defi['defi_etoiles'];
  }else{
    $max=3;
    $controle=getControlesById($_['defi_id']);
    if($controle['controle_user']!=$user['user_id']){
      MainControl::init('render');        
      exit;
    }
    $etoile_type_id=$controle['controle_id'];
  }
  foreach ($etoiles as $etoile) {
    $temp_etoile=[];
    $temp_etoile['etoile_eleve']=$etoile[0];
    $temp_etoile['etoile_defi']=$etoile_type_id;
    $temp_etoile['etoile_value']=$etoile[1];
    $temp_etoile['etoile_type']=$_['etoile_type'];
    $temp_etoile['etoile_date']=$_['time'];;
    if($temp_etoile['etoile_value']>=-1 && $temp_etoile['etoile_value']<=$max){
      addEtoile($temp_etoile);
    }
  }
  MainControl::init('etoiles','getAll'); 
}
public function useEtoiles(){
 global $_, $user;
 MainControl::init('users','get');
   //if(!isset($_['eleve_id']) || !isset($_['etoile_id'])){ return false;}
 $etoile=getEtoileById($_['etoile_id']);
 if($etoile['etoile_type']=="defi"){
   $defi=getDefiById($etoile['etoile_defi']);
   if($defi['defi_user']!=$user['user_id']){
     MainControl::init('render'); 
   }
 }else{
   $controle=getControlesById($etoile['etoile_defi']);
   if($controle['controle_user']!=$user['user_id']){
     MainControl::init('render'); 
   }
 }   
 useEtoiles($etoile['etoile_id']);
 MainControl::init('etoiles','getAll');  

}
}
?>