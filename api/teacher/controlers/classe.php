<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Classe {
      /**
     *
     * @var Instance
     */
      private static $_instance;
    /**
     *
     * @var classe
     */
    private $classe;
    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Classe
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
        return self::$_instance;
      }
      public function get(){
        global $millesime;
       MainControl::init('users','get');
        MainControl::init('periodes','getMillesime');
       $_SESSION['render']['classes']=getClasses($millesime);
     }    
     public function add(){
     global $_, $user;
      MainControl::init('users','get');     
      MainControl::init('users','checkAdmin');
      MainControl::init('periodes','getMillesime');
      if (isset($_['classe_nom'])){        
        $classe_nom=trim($_["classe_nom"]);
        $_SESSION['classe_nom']=$classe_nom;
        if($classe_nom==""){           
          $_SESSION['render']['info'][] =array("Il faut choisir un nom pour la classe.","error");  
          MainControl::init('render');
        }
        if(existClasse($classe_nom)!=false){
         $_SESSION['render']['info'][] =array("Cette classe existe déjà.","error");      
         MainControl::init('render');     
       }
       addClasse($classe_nom);
        MainControl::init('classe','get');
       $_SESSION['render']['elevesByClasses']=getElevesByClasses();  
     }
   }
   public function delete(){
    global $_,$user;
    MainControl::init('users','get');
    MainControl::init('users','checkAdmin');   
    MainControl::init('school','backup');
    delClasse($_['classe_id']); 
   MainControl::init('classe','get');
    $_SESSION['render']['elevesByClasses']=getElevesByClasses();  
  }

  public function update(){
    global $_,$user;
    MainControl::init('users','get');  
    if(!isset($_['classe_id'])){
     MainControl::init('render');  
   }
   $this->classe=getOneClasse($_['classe_id']);
   if(isset($_['messagerie'])){
    $this->updateClassroomContacts();
  }
  if(isset($_['intelligences'])){
    $this->classe['classe_intelligences']=$_['intelligences'];
  }
  if(isset($_['classe_nom']))
  {
    $this->classe['classe_nom']=$_['classe_nom'];
    if(existClasse($this->classe['classe_nom'])!=false){
     $_SESSION['render']['info'][] = array('Une autre classe porte déjà ce nom.','error');
     MainControl::init('render');        
     exit;
   }
 }
 updateClasse($this->classe);
 MainControl::init('classe','get');
 $_SESSION['render']['elevesByClasses']=getElevesByClasses();   
}
public function removeStudent(){
     global $_, $user;
 MainControl::init('users','get');
 deleteRec($_['eleve_id'],$_['classe_id']);     
 $_SESSION['render']['elevesByClasses']=getElevesByClasses();   
// MainControl::init('eleves','getAlone');  
}

//##############
//PRIVATE
//##############

private function updateClassroomContacts(){
  global $_,$user;
  $destinataires=$this->classe['classe_destinataires'];
  if($destinataires=="null" || $destinataires==""){
    $destinataires=[];
  }
  else{ 
    $destinataires=explode(',', $destinataires);
  }
  if($_['messagerie']=='true'){
    array_push($destinataires,$user['user_id']);
  }
  else{
    $new_destinataires=[];
    foreach ($destinataires as $id) {
      if($id!=$user['user_id']){
        $new_destinataires[]=$id;
      }
    }
    $destinataires=$new_destinataires;
  }
  $this->classe['classe_destinataires']=implode(',', $destinataires);
}
}
?>