<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Users {  
        /**
     *
     * @var Instance
     */
        private static $_instance;
    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}

    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Users
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
      return self::$_instance;
    }
    public function getAll(){
     MainControl::init('users','get');
     $_SESSION['render']['users']=getUsers("user_id,user_pseudo,user_matiere,user_nom,user_prenom,user_type,user_mail");
   }
   public function sessionInit(){
    global $_, $user;
    $this->checkIP();
    MainControl::init('school','bddUpdate'); 
    $user=verifUser(strtolower($_["pseudo_utilisateur"]), $_["pass_utilisateur"]);
    if($user!==false){
      $sessionId=getGUID();
      $updateUser['user_id']=$user['user_id'];
      $updateUser['user_sessionID']=$sessionId;
      updateUser($updateUser);
      $_SESSION['render']['sessionID']=$sessionId;
    }else{
      $_SESSION['render']['statut']=false;
      $_SESSION['render']['info'][] = array("Le mot de passe et le pseudo ne correspondent pas.","error");
      $this->add_banned_ip();
      return;
    }
  }
  public function get(){
    global $_, $user;    
    if($user!=null){
      return;
    }
    if(!isset($_["pseudo_utilisateur"])){
     $_SESSION['render']['statut']=false;     
     MainControl::init('render');
     exit;   
   }
   $user=getUserBySessionID(strtolower($_["pseudo_utilisateur"]), $_["sessionID"]);
   if ($user==false) {
    $_SESSION['render']['statut']=false;
    $_SESSION['render']['info'][] = array("La session est expirée.","error");
    $this->add_banned_ip();
    MainControl::init('render');
    exit;         
  }else{
    $_SESSION['render']['statut']=true;
  }
}
public function create(){
  global $_, $user;
  MainControl::init('school','get');
  if (!isset($_["pass_etablissement"])) {
   $_SESSION['render']['statut']=false;
   return false;
 }
 $dir=strtolower(substr($_['nom_etablissement'],0,1));
 require_once(_DATA_."schools/".$dir.'/'.$_['nom_etablissement']."/pwd.php");
 if (!password_verify($_["pass_etablissement"], $password)) {
  $_SESSION['render']['info'][] =array("Le mot de passe et l'établissement ne correspondent pas.","error");
  $_SESSION['render']['statut']=false;
  MainControl::init('render');
  exit;
}    
$subscriptions=getOption('subscriptions');
if ($subscriptions['option_value']==0) {
 $_SESSION['render']['statut']=false;
 $_SESSION['render']['info'][] =array("Les inscriptions sont fermées.","error");               
 return false;
}
if (existUser(strtolower($_["pseudo_utilisateur"]))) {
 $_SESSION['render']['statut']=false;
 $_SESSION['render']['info'][] =array("Ce compte existe déjà.","error");  
 return false;
}
$passwordHash = password_hash($_["pass_utilisateur"], PASSWORD_DEFAULT);

$type="user";
if(count(getAdmins())==0){
 $type="admin";   
}
$new_user=array('user_nom'=>'',
  'user_prenom'=>'',
  'user_pseudo'=>strtolower($_["pseudo_utilisateur"]),
  'user_password'=>$passwordHash,
  'user_type'=>$type,
  'user_mail' =>'',    
);
addUser($new_user);
$_SESSION['render']['statut']=true;
}
public function delete(){
 global $_, $user;
 MainControl::init('users','get');
 MainControl::init('users','checkAdmin');
 $user_id = $_['user_id'];
 $user=getUserById($user_id);
 if($user['user_pseudo']==$_["pseudo_utilisateur"]){
  $_SESSION['render']['info'][] =array("Vous ne pouvez pas supprimer votre compte.","error");            
  MainControl::init('render');        
  exit;
}
delUser($user);
//MainControl::init('school','renderConfig');
MainControl::init('users','getAll');
}


public function update(){
  //MISE A JOUR D'UN UTILISATEUR
  global $_, $user;
  MainControl::init('users','get'); 
  $user_id=$_['user_id'];

  if($user_id!=$user['user_id']){
    MainControl::init('users','checkAdmin');
  }
  $userToUpdate=array();
  $userToUpdate['user_id']=$user_id;
  //DISCIPLINE
  if(isset($_['discipline_id'])){
    $userToUpdate['user_matiere']=$_['discipline_id'];
    $_SESSION['render']['user']['matiere']= $_['discipline_id'];
  }  
  //NOM
  if(isset($_['nom'])){
    $userToUpdate['user_nom']=$_['nom'];
    $_SESSION['render']['user']['nom']= $_['nom'];
  }
  //PRENOM
  if(isset($_['prenom'])){
    $userToUpdate['user_prenom']=$_['prenom'];
    $_SESSION['render']['user']['prenom']= $_['prenom'];
  }  
  //MAIL
  if(isset($_['mail'])){
    if(is_email($_['mail']) || $_['mail']==""){               
     $_SESSION['render']['user']['mail']= $_['mail'];
     $userToUpdate['user_mail']=$_['mail'];
   }else{
    $_SESSION['render']['info'][] = array('Cette adresse mail n\'est pas valide.',"error");
  }
}
//MOT DE PASSE
if(isset($_POST['new_passe'])){
  if($_POST['new_passe']!="" && $_POST['new_passe_bis']!=""){ 
    if($_POST['new_passe_bis']==$_POST['new_passe']){ 
      if(verifUser(strtolower($_["pseudo_utilisateur"]), $_POST['old_passe'])){
       $result=updateUserPassword($_POST['new_passe']);
       if($result){
         $_SESSION['render']['info'][] = array("Votre mot de passe a été modifié.","success");
         $_SESSION['render']['statut']=false;         
       }
     }else{
       $_SESSION['render']['info'][] = array('L\'ancien mot de passe ne correspond pas.',"error");
     }    
   }else{
     $_SESSION['render']['info'][] = array('Les mots de passe sont différents.',"error");
   }
 }
}
//CONFIGURATION
if(isset($_['user_config'])){
 $userToUpdate['user_config']=$_POST['user_config'];
}
//STATUT
if(isset($_['statut'])){
  if($userToUpdate['user_id']==$user['user_id']){
    $_SESSION['render']['info'][] =array("Vous ne pouvez pas changer votre statut.","error"); 
  }
  MainControl::init('users','checkAdmin');
  if($_['statut']=='user'){
    if(count(getAdmins())==1){
     $_SESSION['render']['info'][] =array("Il doit rester au moins un admin.","error");   
   }
 }
 $userToUpdate['user_type']=$_['statut'];
}

//MISE A JOUR
updateUser($userToUpdate);
MainControl::init('users','getAll');
}





public function getProfil(){
  global $user;
  MainControl::init('users','get');
  if(!isset($user['user_matiere']) OR trim($user['user_matiere'])==""){
   $_SESSION['render']['user']['matiere']="";
 }else{
   $_SESSION['render']['user']['matiere']= $user['user_matiere'];
 }
 if(!isset($user['user_mail']) OR trim($user['user_mail'])==""){
   $_SESSION['render']['user']['mail']="";
 }else{
   $_SESSION['render']['user']['mail']= $user['user_mail'];
 }
 $_SESSION['render']['user']['nom']= $user['user_nom'];
 $_SESSION['render']['user']['prenom']= $user['user_prenom'];
 if(isset($user['user_config'])){
  $_SESSION['render']['user']['user_config']=$user['user_config'];
}
$_SESSION['render']['user']['user_id']= $user['user_id'];
}



public function passwordRecovery(){
  global $_;
  if(!isset($_['retrieve_etablissement']) || $_['retrieve_etablissement']==''){
   $_SESSION['render']['info'][] = array("Cet établissement n'existe pas.","error");
   return;
 }
 $_['nom_etablissement']=$_['retrieve_etablissement'];
 $dir=strtolower(substr($_['nom_etablissement'],0,1));
 if(!is_dir(_DATA_."schools/".$dir.'/'.$_['nom_etablissement']."/")){
   $_SESSION['render']['info'][] = array("Cet établissement n'existe pas.","error");
   return;
 }
 if(!isset($_['retrieve_pseudo'])){
   $_SESSION['render']['info'][] = array("Le pseudo est nécessaire.","error");
   return;
 }
 $user=existUser($_['retrieve_pseudo']);
 if(!$user){
   $_SESSION['render']['info'][] = array("Cet établissement et ce pseudo ne correspondent pas.","error");
   //MainControl::init('fail2Ban','add_banned_ip');
   $this->add_banned_ip();
   return;
 }
 if(!isset($_['action'])){
  return;
}


if($_['action']=="getCode"){
  if(trim($user['user_mail'])==""){
   $_SESSION['render']['info'][] = array("Aucune adresse mail n'est définie pour ce compte...","error");
   return;
 }
 $new_token=genereRandomStringNb(6);
 updateUserToken($new_token,$user['user_id']);
 $html="
 Bonjour,
 Vous recevez cet e-mail car un changement de mot de passe a été demandé pour l'identifiant ".$_['retrieve_pseudo'].".
 Voici votre code de récupération :
 ".$new_token."
 Cordialement,
 Pierre Girardot.
 ";
 sendmail($user['user_mail'],'GestionDeClasses.net - Récupération de mot de passe',''.$html.'','ketluts@ketluts.net','text/html');

 $_SESSION['render']['info'][]=array("Votre code de récupération a été envoyé à l'adresse ".$user['user_mail'].".","success");
}
else{
  if(!isset($_['retrieve_token']) || $_['retrieve_token']==null || $_['retrieve_token']=="null"){
   $_SESSION['render']['info'][] = array("Le code de récupération est nécessaire.","error");
   return;
 }
 if(trim($_['retrieve_passe'])!=trim($_['retrieve_passe2'])){
   $_SESSION['render']['info'][] = array("Les mots de passe sont différents.","error");
   return;
 }
 $result=updateUserPasswordWithToken($_['retrieve_pseudo'],$_['retrieve_token'],trim($_['retrieve_passe']));
 if($result){
   $_SESSION['render']['info'][] = array("Votre mot de passe a été modifié.","success");
   updateUserToken("",$user['user_id']);
 }
 else{
   $_SESSION['render']['info'][] = array("Le pseudo et le code de récupération ne correspondent pas.","error");
 }
}
}
public function addUsers(){
  global $_;
  MainControl::init('users','get');
  MainControl::init('users','checkAdmin');
  $users=json_decode($_['users'],true);
  foreach ($users as $user) {
    if(!isset($user[0])){$user[0]="";}
    if(!isset($user[1])){$user[1]="";}
    if(!isset($user[2])){$user[2]="";}
    if(!isset($user[3])){$user[3]="";}
    if(trim($user[0].$user[1].$user[2])==""){
      continue;
    } 

/*
NOM
Prénom
Pseudo
Password
Mail
*/

$user[0]=strtoupper($user[0]);


if(trim($user[1])==""){
    $password=strtolower($user[0]); #nom
  }else{
    $password=strtolower(substr($user[1],0,1).$user[0]); #pnom
  }

  if(!isset($user[2]) OR trim($user[2])==""){
    $user[2]=$password;
  }else{
    $user[2]=strtolower($user[2]);
  }
  $user[4]=$password;



  if(isset($user[3])){
    if(!is_email($user[3])){               
      $user[3]="";
    }
  }
  if (!existUser($user[2])) {
    $userToAdd=[
      "user_nom"=>$user[0],
      "user_prenom"=>$user[1],
      "user_pseudo"=>$user[2],
      "user_mail"=>$user[3],
      "user_password"=> password_hash($user[4], PASSWORD_DEFAULT),
      "user_type"=>"user"
    ];
    addUser($userToAdd);
  }
  //MainControl::init('school','renderConfig');
  MainControl::init('users','getAll');
}
}
public function checkAdmin(){
  if (isAdmin()==false) {
    $_SESSION['render']['info'][] = array("Contactez un administrateur ".ucfirst($admin)." pour effectuer cette opération.","alert");
    MainControl::init('render');        
    exit;
  }
  return true;
}

private function checkIP($ip=null) {
  $banned=false;
  if(empty($ip)){$ip=$_SERVER['REMOTE_ADDR'];}
  $file=_DATA_.'jail/ip_'.md5($ip).'.php';
  if(!is_file($file)){
    $banned=false;
  }
  else{
    global $auto_restrict;
    require_once($file);
    if ($auto_restrict["banned_ip"]['nb']>=5){
            $banned=true; // below max login fails 
          }
          if ($auto_restrict["banned_ip"]['date']<@date('U')){
           $this->remove_banned_ip();
             $banned=false;// old banishment 
           }
         }
         if($banned){
           $_SESSION['render']['info'][] = array("Votre IP a été bannie pour quelques minutes.","error");
           MainControl::init('render');
           exit;    
         }
       }
       /**
* Inspiré d'auto_restrict
 * @author bronco@warriordudimanche.com / www.warriordudimanche.net
 * @copyright open source and free to adapt (keep me aware !)
 * @version 3.1 - one user only version / version mono utilisateur
*/
private function add_banned_ip($ip=null){
 if(empty($ip)){$ip=$_SERVER['REMOTE_ADDR'];}
 $file=_DATA_.'jail/ip_'.md5($ip).'.php';
 if(!is_file($file)){
  $auto_restrict["banned_ip"]['nb']=1;
  $auto_restrict["banned_ip"]['date']=@date('U')+90;
  file_put_contents($file,'<?php /*Banned IP*/ $auto_restrict["banned_ip"]='.var_export($auto_restrict["banned_ip"],true).' ?>');
}
else{
  global $auto_restrict;
  require_once($file);
  $auto_restrict["banned_ip"]['nb']++;
  $auto_restrict["banned_ip"]['date']=@date('U')+90;
  file_put_contents($file,'<?php /*Banned IP*/ $auto_restrict["banned_ip"]='.var_export($auto_restrict["banned_ip"],true).' ?>');
}
}
private function remove_banned_ip($ip=null){
 if(empty($ip)){$ip=$_SERVER['REMOTE_ADDR'];}
 $file=_DATA_.'jail/ip_'.md5($ip).'.php';
 unlink($file);
}
}
?>