<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Periodes {
             /**       
     *
     * @var Instance
     */
             private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return School
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
        return self::$_instance;
      }
      public function addMillesime(){
     global $_;
       MainControl::init('users','get');
       MainControl::init('users','checkAdmin');
       $millesime=array();
       $millesime['periode_titre']=$_['millesime_titre'];
       $millesime['periode_start']=$_['millesime_start'];
       $millesime['periode_end']=$_['millesime_end'];
       $millesime['periode_parent']=null;
        $millesime['periode_type']="m";//millesime
        addPeriode($millesime);  
        $_SESSION['render']['periodes']=getPeriodes(); 
      }
      public function getMillesime(){
       MainControl::init('users','get');
       global $millesime;
       if($millesime==null){
         $millesime=getActiveMillesime();
       }
       return $millesime;
     }
     public function deleteMillesime(){
     global $_;
       MainControl::init('users','get');
       MainControl::init('users','checkAdmin');           
       $millesime_id=$_['millesime_id'];
       if($millesime_id==getActiveMillesime()){
        $_SESSION['render']['info'][] =array("Impossible de supprimer le millesime actif.","error");
        return;
      }
      MainControl::init('school','backup');
      $periodesByMillesime=getPeriodesByMillesime($millesime_id);
      foreach ($periodesByMillesime as $periode) { 
        delPeriode($periode['periode_id']);  
      }
      $classes=getClasses($millesime_id);
      foreach ($classes as $classe) {
        delClasse($classe['classe_id']);
      }
      delMillesime($millesime_id);    
      $_SESSION['render']['periodes']=getPeriodes(); 
    }

    public function addPeriode(){      
     global $_;
     MainControl::init('users','get');
     MainControl::init('users','checkAdmin');
     $periode=array();
     $periode['periode_titre']=$_['periode_titre'];
     $periode['periode_start']=$_['periode_start'];
     $periode['periode_end']=$_['periode_end'];
     $periode['periode_parent']=$_['millesime_id'];
      $periode['periode_type']="p";//periode
      addPeriode($periode);  
      $_SESSION['render']['periodes']=getPeriodes(); 
    }

    public function updPeriode(){
     global $_;
     MainControl::init('users','get');
     MainControl::init('users','checkAdmin');
     $periode=array();
     $periode['periode_titre']=$_['periode_titre'];
     $periode['periode_start']=$_['periode_start'];
     $periode['periode_end']=$_['periode_end'];
      $periode['periode_id']=$_['periode_id'];
      updatePeriode($periode);  
      $_SESSION['render']['periodes']=getPeriodes(); 
    }
    public function setPeriode(){
     global $_;
      MainControl::init('users','get');
      MainControl::init('users','checkAdmin');
      $periode_id=$_['periode_id'];
      setPeriode($periode_id);   
      MainControl::init('loader','init');
      $_SESSION['render']['info'][] = array("Période activée.","alert");
    }
    public function lockPeriode(){
     global $_;
      MainControl::init('users','get');
      MainControl::init('users','checkAdmin');
      $periode_id=$_['periode_id'];
      lockPeriode($periode_id,$_['periode_lock'],$_['time']);   
      $_SESSION['render']['periodes']=getPeriodes(); 
      if($_['periode_lock']==1){
        $_SESSION['render']['info'][] = array("Période vérouillée.","alert");
      }else{
        $_SESSION['render']['info'][] = array("Période dévérouillée.","alert");
      }  
    }
    public function deletePeriode(){
     global $_;
     MainControl::init('users','get');
     MainControl::init('users','checkAdmin');
     $periode_id=$_['periode_id'];
     $periodeActive=getActivePeriode();
     if($periodeActive==$periode_id){
      $_SESSION['render']['info'][] =array("Impossible de supprimer la période active.","error");
      return;
    }
    MainControl::init('school','backup');
    delPeriode($periode_id);  
    MainControl::init('loader','init');
  }
  
}
?>