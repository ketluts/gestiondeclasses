<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Controles {
     /**
     *
     * @var Instance
     */
     private static $_instance;
    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Controles
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
        return self::$_instance;
      }
      public function add(){
        global $_,$user;
        MainControl::init('users','get');                
        $controle=json_decode($_POST['controle'],true);
        isPeriodeLock($controle['controle_periode']); 
        if(trim($controle['controle_titre'])==""){
         $_SESSION['render']['info'][]=array("Il faut choisir un titre pour le contrôle.","error");
         MainControl::init('render');  
       }
       if(!is_numeric($controle['controle_note']) OR $controle['controle_note']<=0){
         $_SESSION['render']['info'][]=array("La note maximale doit être un nombre strictement positif.","error");
         MainControl::init('render');    
       }
       if(!is_numeric($controle['controle_coefficient']) OR $controle['controle_coefficient']<=0){
         $_SESSION['render']['info'][]=array("Le coefficient doit être un nombre strictement positif.","error");
         MainControl::init('render'); 
       }
       $controle['controle_user']=$user['user_id'];
       $controle['controle_discipline']=$user['user_matiere'];
       $controle['controle_classe']=$_['classe_id'];
       $controle['controle_date']=$controle['controle_date'];
       addControle($controle);
       MainControl::init('controles','getAll');
     }
     public function delete(){
      global $_,$user;
      MainControl::init('users','get');
      $controle=getControlesById($_['controle_id']);   
      if($controle['controle_user']!=$user['user_id']){return false;}
      isPeriodeLock($controle['controle_periode']);
      delControle($controle);
      MainControl::init('controles','getAll');
    }
    public function getAll(){
     MainControl::init('users','get');  
     MainControl::init('periodes','getMillesime');     
     $controles=getAllControles();
     $_SESSION['render']['controles'] = $controles;
   }
   public function update(){
    global $_,$user;
    MainControl::init('users','get');
    $controle=getControlesById($_['controle_id']);   
    if($controle['controle_user']!=$user['user_id']){return false;}
    isPeriodeLock($controle['controle_periode']);
    if(isset($controle['controle_user']) && $controle['controle_user']!=$user['user_id']){
     MainControl::init('render');  
   }
   $update_controle['id']=$_['controle_id'];
   $update_controle['controle_titre']=$_['controle_titre'];
   $update_controle['coefficient']=$_['controle_coefficient'];
   $update_controle['controle_periode']=$_['controle_periode'];
   $update_controle['controle_categorie']=$_['controle_categorie'];
  $update_controle['controle_date']=$_['controle_date'];
   $update_controle['note']=$_['controle_note'];
   updateControle($update_controle);
   MainControl::init('controles','getAll');
 }
}
?>