<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Agenda {
      /**
     *
     * @var Instance
     */
      private static $_instance;
    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Agenda
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
        return self::$_instance;
      }
    /**
     * Agenda
     */
    public function addEvent(){
     global $_, $user;
     MainControl::init('users','get');
     isPeriodeLock($_['event_periode']);      
     $lock=0;
     if(isset($_['event_lock'])){
      $lock=1;
    }
    $event=[
     "event_title" => $_['event_title'],
     "event_data" => $_['event_data'],
     "event_color" => $_['event_color'],
     "event_icon" => $_['event_icon'],
     "event_type" => $_['event_type'],
     "event_type_id" => $_['event_type_id'],
     "event_start" => $_['event_start'],
     "event_end" => $_['event_end'],
     "event_date" => $_['event_date'],
     "event_allDay" => $_['event_allDay'],
     "event_user" => $user['user_id'],
     "event_visibility" => $_['event_visibility'],
     "event_periode" => $_['event_periode'],
     "event_discipline" => $_['event_discipline'],
     "event_lock" => $lock     
   ];  
   addAgendaEvent($event);
  // $_SESSION['render']['info'][] = array("L'événement a été ajouté.","alert");
   MainControl::init('agenda','getAll'); 
 }
 public function addVisa(){
   global $_, $user;
   MainControl::init('users','get');
   MainControl::init('users','checkAdmin');        
   $event=[
     "event_title" => $_['event_title'],
     "event_data" => $_['event_data'],
     "event_color" => $_['event_color'],
     "event_icon" => $_['event_icon'],
     "event_type" => $_['event_type'],
     "event_type_id" => $_['event_type_id'],
     "event_start" => $_['event_start'],
     "event_end" => $_['event_end'],
     "event_date" => $_['event_date'],
     "event_allDay" => $_['event_allDay'],
     "event_user" => $_['event_user'],
     "event_visibility" => $_['event_visibility'],
     "event_discipline" => -1,
     "event_periode" => $_['event_periode'],
     "event_lock" => 1     
   ];  
   addAgendaEvent($event);
   MainControl::init('agenda','getAll'); 
 }
 public function getAll(){   
   global $_;
   MainControl::init('users','get');    
   MainControl::init('periodes','getMillesime');
   $select="event_id,
   event_title,
   event_data,
   event_color,
   event_icon,
   event_type,
   event_type_id,
   event_start,
   event_end,
   event_allDay,
   event_user,
   event_visibility,
   event_state,
   event_date,
   event_lock,
   event_periode,
   event_discipline";
   $_SESSION['render']['agenda']=getAllAgendaEvents($select);
    // $_SESSION['render']['agenda']=getAllAgendaEvents($select,$_['time']);
 }
 public function deleteEvent(){
  global $_, $user;
  MainControl::init('users','get');        
  if(!isset($_['event_id'])){
   MainControl::init('render');  
 }    
 $event=getEventById($_['event_id']);
 if($event['event_user']!=$user['user_id']){return false;}
 isPeriodeLock($event['event_periode']);     
 delAgendaEventById($_['event_id']);
 MainControl::init('agenda','getAll'); 
}
public function deleteVisa(){
  global $_, $user;
  MainControl::init('users','get');          
  MainControl::init('users','checkAdmin');
  if(!isset($_['event_id'])){
   MainControl::init('render');   
 }    
 delAgendaVisaById($_['event_id']);
 MainControl::init('agenda','getAll'); 
}
public function update(){
  global $_, $user;
  MainControl::init('users','get');     
  if(!isset($_['event_id'])){
   MainControl::init('render');  
 }

 $event=getEventById($_['event_id']);
 if($event['event_user']!=$user['user_id']){return false;}
 isPeriodeLock($event['event_periode']);  

 $new_event['event_id']=$_['event_id'];
 if(isset($_['event_visibility'])){
   $new_event['event_visibility']=$_['event_visibility'];
 }
 if(isset($_['event_start'])){
   $new_event['event_start']=$_['event_start'];
 }
 if(isset($_['event_end'])){
   $new_event['event_end']=$_['event_end'];
 }
 if(isset($_['event_allDay'])){
   $new_event['event_allDay']=$_['event_allDay'];
 }
 if(isset($_['event_state'])){
   $new_event['event_state']=$_['event_state'];
 }
 if(isset($_['event_color'])){
   $new_event['event_color']=$_['event_color'];
 }
 if(isset($_['event_title'])){
   $new_event['event_title']=$_['event_title'];
 }
 if(isset($_['event_type_id'])){
   $new_event['event_type_id']=$_['event_type_id'];
 }
 if(isset($_['event_data'])){
   $new_event['event_data']=$_['event_data'];
 }
 if(isset($_['event_icon'])){
   $new_event['event_icon']=$_['event_icon'];
 }
 if(isset($_['event_date'])){
   $new_event['event_date']=$_['event_date'];
 }
 updateAgendaEvent($new_event);
 MainControl::init('agenda','getAll'); 
} 
}
?>