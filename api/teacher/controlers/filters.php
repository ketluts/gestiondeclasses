<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Filters {
     /**
     *
     * @var Instance
     */
     private static $_instance;
    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Filters
     */
    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }
    public function add() {
     global $_, $user;
        MainControl::init('users','get');
        if(!isset($_['filter'])){
            return;
        }
        $filter=$_['filter'];
        addFilter($filter);
        MainControl::init('filters','getAll');
    }
    public function getAll(){
      MainControl::init('users','get'); 
      $_SESSION['render']['filters']=getFiltersByUser();
  }
  public function delete() {
     global $_, $user;
    MainControl::init('users','get');
    if(!isset($_['filters'])){
        return;
    }      
    $filters=json_decode($_['filters'],true);
    deleteFilters($filters);
    MainControl::init('filters','getAll');
}
}
?>