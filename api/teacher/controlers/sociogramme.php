<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Sociogramme{
    /**       
     *
     * @var Instance
     */
    private static $_instance;
    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Sociogramme
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
      return self::$_instance;
    }
    public function getAllRelations(){
            global $_, $user;
     MainControl::init('users','get');
     $select="*";
     $_SESSION['render']['relations']=getAllRelations($select);
   }
   public function updateRelations(){
     global $_;
     MainControl::init('users','get');
     if(!isset($_['eleve_id'])){
       MainControl::init('render');        
       exit;
     }
     $eleve=getEleveById($_['eleve_id']);
     delRelations($eleve['eleve_id']);
     for ($i=1; $i <= 7; $i++) { 
      $relation[$i]['from']=$eleve['eleve_id'];
      $relation[$i]['to']=$_['relation_'.$i];
    if($_['relation_'.$i]!="false"){
      addRelation($relation[$i],$i);
    }  
  };
  MainControl::init('sociogramme','getAllRelations');
}
public function add(){
  global $_, $user;
  MainControl::init('users','get');
  $sociogramme['sociogramme_data']=$_['data'];
  $sociogramme['sociogramme_user']=$user['user_id'];
  $sociogramme['sociogramme_classe']=$_['classe_id'];
  $sociogramme['sociogramme_date']=$_['time'];
  addSociogramme($sociogramme);
  MainControl::init('sociogramme','getAll'); 
}
public function delete(){
  global $_, $user;
  MainControl::init('users','get');
  $sociogramme['sociogramme_user']=$user['user_id'];
  $sociogramme['sociogramme_id']=$_['sociogramme_id'];
  delSociogramme($sociogramme);
  MainControl::init('sociogramme','getAll'); 
}
public function getAll(){
    MainControl::init('periodes','getMillesime');
 $_SESSION['render']['userSociogrammes'] =getSociogrammesByUser();
}
}
?>