<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Share {
          /**
     *
     * @var Instance
     */
          private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Share
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
      return self::$_instance;
    }
     //##########
        //SHARE
        //##########
    public function addShare(){
      global $_, $user;
      MainControl::init('users','get');
      if(!isset($_['share_item_type'],$_['share_type'],$_['share_with'])){
       MainControl::init('render');        
       exit;
     }

     $share['share_with']=$_['share_with'];
     $share['share_type']=$_['share_type'];
     $share['share_date']=$_['time'];
     $share['share_item_type']=$_['share_item_type'];
     $share['share_user']=$user['user_id'];
     switch ($_['share_item_type']) {
      case 'file':
      if(!isset($_['share_item_id'])){
       MainControl::init('render');        
       exit;
     }
     $file=getFileByIdAndUser($_['share_item_id'],$user['user_id']);
     if(!$file){
      MainControl::init('render');        
      exit;
    }     
    break;
    case 'link':
    $link=getLinkByIdAndUser($_['share_item_id'],$user['user_id']);
    if(!$link){
     MainControl::init('render');        
     exit;
   }
   break;
 }
 $share['share_item_id']=$_['share_item_id'];
 addShare($share);
 MainControl::init('share','getShares'); 
}
public function deleteShare(){
  global $_, $user;
  MainControl::init('users','get');
  if(!isset($_['share_id'])){
    MainControl::init('render');        
    exit;
  }
  delShareById($_['share_id']);
  MainControl::init('share','getShares'); 
}
public function getShares(){
  global $_, $user;
  MainControl::init('users','get');
  if(!isset($_['share_item_id']) || !isset($_['share_item_type'])){
    MainControl::init('render');        
    exit;
  }
  $shares=getSharesByItem($_['share_item_id'],$_['share_item_type']);
  $_SESSION['render']['shares']=$shares;
  MainControl::init('render'); 
}
//##########
        //FILES
        //##########

public function addFile(){
  global $_, $user;
  MainControl::init('users','get');
  if(!isset($_FILES['file']) || $_FILES['file']['error']>0){return;}
  $size = filesize($_FILES['file']['tmp_name']);
  if($size>=file_upload_max_size()){
    MainControl::init('render');        
    exit;
  }
  $files=getFilesByUser($user['user_id'],"file_id,file_fullname,file_type,file_date,file_size");
  $quotaUsed=getFilesSize($files);
  $quotaByUser=getOption('quotaByUser')['option_value'];
  if($quotaUsed+$size>$quotaByUser){
    $_SESSION['render']['info_alert']= 'Vous n\'avez plus assez d\'espace de stockage.';
    MainControl::init('render');        
    exit;
  }
  $extension_upload = strtolower(  substr(  strrchr($_FILES['file']['name'], '.')  ,1)  );
  $name=time()."_".genereRandomStringNb(10).".file";
   $file['file_visibility']=$_['visibility'];

 $file['file_name']=$name;
 $file['file_type']=$extension_upload;
 $file['file_size']=$size;
 $file['file_date']=$_['time'];
 $file['file_fullname']=$_FILES['file']['name'];
 $file['file_user']=$user['user_id'];
 $file_id=addFile($file);
 $dir=strtolower(substr($_['nom_etablissement'],0,1));
 $dir_path=_DATA_."schools/".$dir.'/'.$_['nom_etablissement']."/files";
 $uploaddir = $dir_path.'/';
 $uploadfile = $uploaddir . $name;
 move_uploaded_file($_FILES['file']['tmp_name'],$uploadfile);
 $_SESSION['data']['file_id']=$file_id;
}
public function deleteFile(){
  global $_, $user;
  MainControl::init('users','get');
  if(!isset($_['file_id'])){
   MainControl::init('render');        
   exit;
 }
 $file=getFileByIdAndUser($_['file_id'],$user['user_id']);
 if($file['file_user']!=$user['user_id']){
   MainControl::init('render');        
   exit;
 }
 delFile($_['file_id'],$user['user_id']);
 if($file!==false){
  $dir=strtolower(substr($_['nom_etablissement'],0,1));
  $dir_path=_DATA_."schools/".$dir.'/'.$_['nom_etablissement']."/files";
  unlink($dir_path."/" . $file['file_name']);
}
delShareByItem($file['file_id'],'file');
$this->delFeedbBackByFile($file['file_id']);
MainControl::init('share','getAll');
}
public function getFile(){
  global $_, $user;
  MainControl::init('users','get');
  if(!isset($_['file_id'])){
   header("HTTP/1.1 404 Not Found");
   exit;
 }
 $file=getFileByIdAndUser($_['file_id'],$user['user_id']);
 if($file==false){
  header("HTTP/1.1 404 Not Found");
  exit;
}
downloadFile($file);
exit;
}
public function getImage(){

 global $_, $user;
 MainControl::init('users','get');

 $file=getFileByIdAndUser($_['file_id'],$user['user_id']);

 if($file==false){
  header("HTTP/1.1 404 Not Found");
  exit;
}

if(in_array($file['file_type'],['gif','jpg','jpeg','png'])){

  $dir=strtolower(substr($_['nom_etablissement'],0,1));
  $dir_path=_DATA_."schools/".$dir.'/'.$_['nom_etablissement']."/files/";
  $filename = $dir_path.$file['file_name'];
  if (!is_file($filename) || !is_readable($filename)) {
    header("HTTP/1.1 404 Not Found");
    exit;
  }
  header("content-type: image/".$file['file_type']);
  echo file_get_contents($filename);
//readfile($filename);

}else{
  header("HTTP/1.1 404 Not Found");
  exit;
}


}
public function getFiles(){
  global $_, $user;
  MainControl::init('users','get');
  $files=getFilesByUser($user['user_id'],"file_id,file_fullname,file_type,file_date,file_size,file_visibility");
  for ($i=0; $i < count($files); $i++) { 
    $files[$i]['isShared']=isShared($files[$i]['file_id'],'file');
  }
  $_SESSION['render']['files']['liste']=$files;
  $_SESSION['render']['files']['quotaUsed']=getFilesSize($files);
}

private function delFeedbBackByFile($file_id){
 global $_, $user;
 MainControl::init('users','get');
 $items=getItems('item_id,item_feedbacks,item_user');
 $getItems=false;
 for ($i=0; $i < count($items); $i++) { 
   $item=$items[$i];
   $to_update=false;
   $item_feedbacks=json_decode($item['item_feedbacks'],true);

   for ($j=0; $j <count($item_feedbacks) ; $j++) { 
    $new_feedback=[];
    for ($k=0; $k < count($item_feedbacks[$j]); $k++) { 
     if($item_feedbacks[$j][$k]!=$file_id){
      $new_feedback[]=$item_feedbacks[$j][$k];
    }else{
     $to_update=true;
   }
 }
 $item_feedbacks[$j]=$new_feedback;
}
if($to_update==true){
  $itemToUpdate['item_id']=$item['item_id'];
  $itemToUpdate['item_feedbacks']=json_encode($item_feedbacks);
  
  updateItem($itemToUpdate,"",$user['user_id']);
  $getItems=true;
}
}
if($getItems==true){  
  MainControl::init('items','get');
}

}
//##########
        //LINKS
        //##########
public function addLink(){
  global $_, $user;
  MainControl::init('users','get');
  if(!isset($_['link_url'])){
   MainControl::init('render');        
   exit;
 }
 if(!filter_var($_['link_url'], FILTER_VALIDATE_URL)){
   $_SESSION['render']['info'][] =array("L'adresse spécifiée semble invalide.","error");
   MainControl::init('render');        
   exit;
 }
 $parseUrl=parse_url($_['link_url']);
 if(trim($_['link_titre'])==""){
  $_['link_titre']=ucfirst(preg_replace("#www.#", "", $parseUrl['host']));
}
$favicon = new \Favicon\Favicon();
$link['link_titre']=$_['link_titre'];
$link['link_url']=$_['link_url'];
@$link['link_favicon']=$favicon->get($parseUrl['scheme'].'://'.$parseUrl['host']);
$link['link_user']=$user['user_id'];
$link['link_date']=$_['time'];
addLink($link);
MainControl::init('share','getAll');
}
public function deleteLink(){
  global $_, $user;
  MainControl::init('users','get');
  if(!isset($_['link_id'])){
    MainControl::init('render');        
    exit;
  }
  $link=getLinkByIdAndUser($_['link_id'],$user['user_id']);
  if($link['link_user']!=$user['user_id']){
    MainControl::init('render');        
    exit;
  }
  delLink($_['link_id'],$user['user_id']);
  delShareByItem($link['link_id'],'link');
  MainControl::init('share','getAll'); 
}
public function getLinks(){
  global $_, $user;
  MainControl::init('users','get');
  $links=getLinksByUser($user['user_id'],"*");
  for ($i=0; $i < count($links); $i++) { 
    $links[$i]['isShared']=isShared($links[$i]['link_id'],'link');
  }
  $_SESSION['render']['links']=$links;
}
//##########
//ALL
//##########
public function getAll(){
  global $_, $user;
  MainControl::init('users','get');
  MainControl::init('share','getFiles');
  MainControl::init('share','getLinks');
}
public function QRCode(){
  global $_, $user;
  MainControl::init('users','get');
  require_once("lib/phpqrcode/qrlib.php");
  ob_start();
  QRCode::png($_['share_text'],null);
  $imageString = base64_encode( ob_get_contents() );
  ob_end_clean();
  $qrcode = "<img src='data:image/png;base64," . $imageString."'/>";
  ob_start();
  require_once 'views/pdf/shareQRCode.php';
  $html= ob_get_contents();
  ob_end_clean();
  $html_data=$html;
  $snappy = new Knp\Snappy\Pdf('lib/wkhtmltox/bin/wkhtmltopdf');
  header('Content-Type: application/pdf');
// Remove the next line to let the browser display the PDF
  header('Content-Disposition: attachment; filename="'.time().'_QRCode.pdf"');
  echo $snappy->getOutputFromHtml($html_data);
  exit;
}
}
?>