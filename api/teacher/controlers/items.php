<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Items {
          /**
     *
     * @var Instance
     */
          private static $_instance;
    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Items
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
        return self::$_instance;
      }
      public function add(){
     global $_, $user;
        MainControl::init('users','get');
        $items=json_decode($_['items'],true);
        $n=count($items);
        if($n>5){
          MainControl::init('school','backup');
        }
        for ($i=0; $i < $n; $i++) { 
         $item=$items[$i];
         $new_item=[];
         $new_item['item_name']=$item['item_name'];      
         $new_item['item_categorie']=$item['item_categorie'];
         $new_item['item_sous_categorie']=$item['item_sous_categorie'];      
         $new_item['item_tags']=$item['item_tags'];
         $new_item['item_color']=$item['item_color'];
        // $new_item['item_cycle']=null;
         //if(isset($item['item_cycle'])){
          $new_item['item_cycle']=$item['item_cycle'];
         //}         
         $new_item['item_vue']=$item['item_vue'];
         $new_item['item_mode']=$item['item_mode'];
         $new_item['item_value_max']=$item['item_value_max'];
         $new_item['item_colors']=$item['item_colors'];       
         $new_item['item_descriptions']=$item['item_descriptions'];
         $new_item['item_date']=$_['time']; 
         addItem($new_item);
       }     
       MainControl::init('items','get');
     }
     public function updateItems(){
     global $_, $user;
      MainControl::init('users','get');
      $itemsToUpdate=json_decode($_POST['item_id'],true);     
      if(count($itemsToUpdate)>5){
        MainControl::init('school','backup');
      }
      $new_item=[];
      if(isset($_['item_name']) AND $_['item_name']!="null"){
     $new_item['item_name']=$_['item_name'];  //TODO
   }
   if(isset($_['item_mode']) AND $_['item_mode']!="null"){
     $new_item['item_mode']=$_['item_mode'];  
   }

   if(isset($_['item_cycle']) AND $_['item_cycle']!="-1"){
     $new_item['item_cycle']=$_['item_cycle'];  
   }
   if(isset($_['item_color']) AND $_['item_color']!="null"){
     $new_item['item_color']=$_['item_color'];  
   }
   if(isset($_['item_categorie']) AND $_['item_categorie']!="null"){
     $new_item['item_categorie']=$_['item_categorie'];  
   }
   if(isset($_['item_public']) AND $_['item_public']!="null"){
     $new_item['item_public']=$_['item_public'];  
   }
   if(isset($_['item_sous_categorie']) AND $_['item_sous_categorie']!="null"){
     $new_item['item_sous_categorie']=$_['item_sous_categorie'];  
   }
   if(isset($_['item_vue']) AND $_['item_vue']!="false"){
    $new_item['item_vue']=$_['item_vue'];
    $new_item['item_value_max']=$_['item_value_max'];
    $new_item['item_colors']=$_['item_colors'];  
    $new_item['item_descriptions']=$_['item_descriptions'];
  }
  if(isset($_['item_tags']) AND $_['item_tags']!="null"){
    $new_item['item_tags']=$_['item_tags']; 
  }
  if(!isset($_['tagsMode'])){
    $_['tagsMode']="add";
  }
  foreach ($itemsToUpdate as $item_id) {
    $new_item['item_id']=$item_id;
    updateItem($new_item,$_['tagsMode'],$user['user_id']);
  }
  MainControl::init('items','get');
}
public function del(){
     global $_, $user;
  MainControl::init('users','get');
  $items=json_decode($_['items'],true);
  if(count($items)>5){
   MainControl::init('school','backup');
 }
 delItem($items,$user['user_id']);
 MainControl::init('items','get');
}
public function delProgression(){
     global $_, $user;
  MainControl::init('users','get');
  $progression=getProgressionById($_['progression_id'],"rei_user,rei_user_type");
  if($progression['rei_user']!=$user['user_id'] || $progression['rei_user_type']!="user"){return;}
  delProgression($_['progression_id']);     
  MainControl::init('items','getProgressions');
}
public function updateProgression(){
     global $_, $user;
  MainControl::init('users','get');
  $progression=getProgressionById($_['progression_id'],"*");
  if($progression['rei_user']!=$user['user_id'] || $progression['rei_user_type']!="user"){return;}
  if(isset($_['progression_date']))
  {
    $progression['rei_date']=$_['progression_date'];
  }
  if(isset($_['progression_activite']))
  { 
    $progression['rei_activite']=$_['progression_activite'];
  }
  updateProgression($progression); 
  MainControl::init('items','getProgressions');
}
public function uncheck(){
     global $_, $user;
  MainControl::init('users','get');
  $eleves=json_decode($_['eleves'],true);
  uncheckItem($_['item_id'],$eleves);   
  MainControl::init('items','getProgressions');
}
public function get(){
 MainControl::init('users','get');
 $_SESSION['render']['items']=getItems();  
}
public function addProgression(){
 // sleep(20);
     global $_, $user;
  MainControl::init('users','get');
  $items=json_decode($_POST['items'],true);
  $eleves=json_decode($_POST['eleves'],true);
  if($_['progression']<0){    
    delItemView($items,$eleves);
  }
  if(!isset($_['activite'])){
    $_['activite']="";
  }
  if(!isset($_['comment'])){
    $_['comment']="";
  }

  if($_['progression']>=-2){
    $new_progression=[];
    $new_progression['rei_date']=$_['time'];
    $new_progression['rei_activite']=$_['activite'];
    $new_progression['rei_symbole']=$_['symbole'];
    $new_progression['rei_comment']=$_['comment'];
    $new_progression['rei_value']=$_['progression'];      
    $new_progression['rei_user_type']="user";
    
    addProgression($new_progression,$user['user_id'],$items,$eleves);
  }
  MainControl::init('items','getProgressions');
}
public function getProgressions(){
     global $_, $user;
  MainControl::init('users','get');
  $items=getItems();
  $tab_id=[];
  for ($i=0; $i < count($items); $i++) { 
    array_push($tab_id, $items[$i]['item_id']);
  }
  $_SESSION['render']['progressions']=getProgressionsByItems($tab_id,"rei_id,rei_comment,rei_date,rei_user,rei_user_type,rei_item,rei_eleve,rei_value,rei_activite,rei_symbole");
} 

public function addFeedbackFile(){
   global $_, $user;
    MainControl::init('users','get');
MainControl::init('share','addFile');
$file_id=$_SESSION['data']['file_id'];
$item=getItemById($_['item_id']);
$item_feedbacks=json_decode($item['item_feedbacks'],true);
if(!is_array($item_feedbacks)){
  $item_feedbacks=Array();
}
$item_feedbacks[$_['feedback_num']][]=$file_id;
$itemsToUpdate['item_id']=$item['item_id'];
$itemsToUpdate['item_feedbacks']=json_encode($item_feedbacks);
 updateItem($itemsToUpdate,"",$user['user_id']);
 MainControl::init('items','get');
  MainControl::init('share','getFiles');
}
public function deleteFeedbackFile(){
   global $_, $user;
    MainControl::init('users','get');
    MainControl::init('share','deleteFile');
}
}
?>