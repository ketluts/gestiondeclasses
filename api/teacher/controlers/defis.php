<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Defis {
     /**
     *
     * @var Instance
     */
     private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Defis
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
        return self::$_instance;
      }
      public function get(){
        global $_,$user;
        MainControl::init('users','get');        
        $_SESSION['render']['defis']=getDefisByUser($user["user_id"]);
      }
      public function add(){
        global $_,$user;
        MainControl::init('users','get');  
        $defi['defi_titre']=$_['defi_titre'];
        $defi['defi_description']=$_['defi_description'];
        $defi['defi_tags']=$_['defi_tags'];
        $defi['defi_etoiles']=$_['defi_etoiles'];
        $defi['defi_icon']=$_['defi_icon'];
        $defi['defi_date']=$_['time'];
        $defi['defi_user']=$user["user_id"];
        $defi['defi_url']="";
        if(isset($_["defi_url"])){
          $defi['defi_url']=$_["defi_url"];
        }
        if(trim($defi['defi_titre'])==""){
         $_SESSION['render']['info'][]=array("Il faut choisir un titre pour le défi.",'error');
         MainControl::init('render');  
       }
       if(addDefis($defi)){
       }
       else{
        $_SESSION['render']['info'][]=array("Il y a eu une erreur lors de l'ajout.",'error');
      }
      MainControl::init('defis','get');
    }
  
    public function update(){
      global $_,$user;
      MainControl::init('users','get');    
      $defi=getDefiById($_['defi_id']);
      if(isset($defi['defi_user']) && $defi['defi_user']!=$user['user_id']){
       MainControl::init('render'); 
     }
     $update_defi['defi_id']=$_['defi_id'];
     $update_defi['defi_url']=$_['defi_url'];
     $update_defi['defi_titre']=$_['defi_titre'];
     $update_defi['defi_description']=$_['defi_description'];
     updateDefi($update_defi);
     MainControl::init('defis','get');
   }
   public function delete(){
    global $_,$user;
    MainControl::init('users','get');
    $defi=getDefiById($_['defi_id']);
    if(isset($defi['defi_user']) && $defi['defi_user']!=$user['user_id']){
     MainControl::init('render'); 
   }       
   delDefi($defi);
   MainControl::init('defis','get');
    MainControl::init('etoiles','getAll'); 
 }  

}
?>