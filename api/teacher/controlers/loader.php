<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Loader {
           /**
     *
     * @var Instance
     */
           private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Loader
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
        return self::$_instance;
    }
    public function init() {    ; 
       $_SESSION['render']['home']=true; 
//       MainControl::init('school','get');

       MainControl::init('users','get');
        //OPTIONS DE L'ETABLISSEMENT
       MainControl::init('school','renderConfig'); 
         MainControl::init('disciplines','get');
       //USERS
       MainControl::init('users','getProfil'); 
       MainControl::init('users','getAll');            
       MainControl::init('memos','getAll');//OK
       //ELEVES
       MainControl::init('eleves','getAll');
//       MainControl::init('eleves','getAlone');
        //CLASSES
       MainControl::init('classe','get');//OK
       MainControl::init('plans','getAll');//OK  
       MainControl::init('groupes','getAll');//OK
        //SOCIOGRAMMES
       MainControl::init('sociogramme','getAllRelations');
       MainControl::init('sociogramme','getAll');//OK
       //AGENDA
       MainControl::init('agenda','getAll');//OK
       //CONTROLES ET NOTES
       MainControl::init('controles','getAll'); //OK
       MainControl::init('notes','getAll');//OK     
        //ITEMS
       MainControl::init('items','get'); 
       MainControl::init('items','getProgressions'); 
       MainControl::init('feedbacks','getAll');//OK    
        //TODO getItems est appelé deux fois...   
       //FILTRES
       MainControl::init('filters','getAll'); 
       //DEFIS ET ETOILES
       MainControl::init('defis','get');
       MainControl::init('etoiles','getAll');   
      //SHARES
       MainControl::init('share','getAll');
   }
}
?>