<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Plans {
     /**
     *
     * @var Instance
     */
     private static $_instance;
    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Plans
     */
    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
            return self::$_instance;
        }
        public function add() {
            global $_, $user;
            MainControl::init('users','get');
            $plan['plan_classe']=$_['classe_id'];
            $plan['plan_data']=$_['data'];
            $plan['plan_user']=$user["user_id"];
            addPlan($plan);
            MainControl::init('plans','getAll');
        }
        public function getAll(){
            MainControl::init('users','get'); 
            MainControl::init('periodes','getMillesime');
            $_SESSION['render']['userPlans']=getPlansByUser();
        }
    }
    ?>