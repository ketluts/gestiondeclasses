<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Messagerie {
     /**
     *
     * @var Instance
     */
     private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * Messagerie
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
      return self::$_instance;
    }
    public function newMessage(){
     global $_, $user;
      MainControl::init('users','get');
      if(!isset($_['message_destinataires'])){
        MainControl::init('render');        
        exit;
      }
      $destinataires=json_decode($_POST['message_destinataires'],true);
      $discussion['message_subject']=$_['message_subject'];
      $discussion['message_content']=$_['message_content'];
      $discussion['message_date']=$_['time'];
      $discussion['message_author']=$user['user_id'];
      $discussion['message_author_type']='user';
      $discussion['message_parent_message']="-1";
      $discussion_id=discussionAdd($discussion);
      foreach ($destinataires as $destinataire) {
        if($destinataire['destinataire_type']=='user' || $destinataire['destinataire_type']=='eleve'){
          destinataireADD($destinataire,$discussion_id);    
        }
        elseif ($destinataire['destinataire_type']=='classe') {
          $eleves=getElevesByClasseId($destinataire['destinataire_id'],'eleve_id');
          foreach ($eleves as $eleve_tab) {
            destinataireADD(["destinataire_id"=>$eleve_tab['eleve_id'],"destinataire_type"=>"eleve"],$discussion_id);
          }
        }
        elseif ($destinataire['destinataire_type']=='all_users') {
          $users=getUsers();
          foreach ($users as $user_tab) {
            destinataireADD(["destinataire_id"=>$user_tab['user_id'],"destinataire_type"=>"user"],$discussion_id);
          }
        }
      }
      destinataireADD(["destinataire_id"=>$user['user_id'],"destinataire_type"=>"user"],$discussion_id,true);
      MainControl::init('messagerie','get');
    }
    public function delete(){
     global $_, $user;
      MainControl::init('users','get');
      if(!isset($_['ids'])){
        MainControl::init('render');        
        exit;
      }
      $messagesToDelete=json_decode($_POST['ids'],true); 
      for ($i=0; $i < count($messagesToDelete); $i++) { 
        $id=$messagesToDelete[$i];
        relationDiscussionDel($id,$user['user_id'],'user');
        $participants=getParticipantsByDiscussion($id);
        if(count($participants)==0){
         discussionDel($id);   
       }else{
        $discussion['message_subject']="";
        $discussion['message_content']=ucfirst($user['user_pseudo'])." a quitté la discussion.";
        $discussion['message_parent_message']=$id;
        $discussion['message_date']=$_['time'];
        $discussion['message_author']=$user['user_id'];
        $discussion['message_author_type']='user';
        $discussion_id=discussionAdd($discussion);
      }
    }     
  }
  public function get(){
     global $_, $user;
    MainControl::init('users','get');
    $select="message_id,message_author,message_author_type,message_subject,message_content,message_date,message_parent_message,rm_messages,rm_read,rm_lock,rm_type_id,user_pseudo,user_matiere,eleve_nom,eleve_prenom";
    $temp_messages=getMessagesByUser($user['user_id'],$select,$_['syncId']);
    $messages=array();
    $id_liste=array();
    foreach ($temp_messages as $message) {
      if(!in_array($message['message_id'],$id_liste)){
        array_push($messages, $message);
        array_push($id_liste, $message['message_id']);
      }
    }
    for ($i=0; $i < count($messages); $i++) { 
      $message=$messages[$i];
      $messages[$i]['state']=true;
      if($message["message_author"]!=$user['user_id'] || $message["message_author_type"]!="user"){
        $messages[$i]['state']=$message["rm_read"];
      }
      if($message['message_parent_message']=="-1"){
        $messages[$i]['participants']=getParticipantsByDiscussion($message['message_id']);
      }
    }
    $_SESSION['render']['messages']=$messages;
  }
  public function update(){
     global $_, $user;
    MainControl::init('users','get');
    if(!isset($_['discussion_id'])){
     MainControl::init('render');        
     exit;
   }
   $discussion=getDiscussionById($_['discussion_id']);
   if(isset($_['discussion_lock'])){
     if($discussion['message_author']!=$user['user_id'] OR $discussion['message_author_type']!='user'){
       MainControl::init('render');        
       exit;
     }
     $new_discussion['message_subject']="~lock";
     $new_discussion['message_parent_message']=$_['discussion_id'];
     $new_discussion['message_author']=$user['user_id'];
     $new_discussion['message_author_type']='user';
     $new_discussion['message_date']=$_['time'];
     if($_['discussion_lock']=="1"){
      $new_discussion['message_content']=ucfirst($user['user_pseudo'])." a bloqué les réponses.";
    }else{
      $new_discussion['message_content']=ucfirst($user['user_pseudo'])." a autorisé les réponses.";
    }
    discussionAdd($new_discussion);
    lockDiscussion($_['discussion_id'],$_['discussion_lock']);             
    MainControl::init('messagerie','get');
  }
  if(isset($_['discussion_read'])){
    markAsRead($_['discussion_id'],'user',$user['user_id'],$_['discussion_read']);
  }
}
public function newAnswer(){
     global $_, $user;
  MainControl::init('users','get');
  if(!isset($_['message_parent_message'])){
    MainControl::init('render');
    exit;
  }
  $discussion=getDiscussionById($_['message_parent_message']);
  if(!answerEnable($_['message_parent_message'],'user',$user['user_id']) && $discussion['message_author']!=$user['user_id']){
    MainControl::init('render');
    exit;
  }
  $answer['message_subject']="";
  $answer['message_content']=$_['message_content'];
  $answer['message_date']=$_['time'];
  $answer['message_parent_message']=$_['message_parent_message'];
  $answer['message_author']=$user['user_id'];
  $answer['message_author_type']='user';
  discussionAdd($answer);
  MainControl::init('messagerie','get');
}   
}
?>