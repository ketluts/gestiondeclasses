<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function addPlan($plan){  
  global $user;
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM plans WHERE plan_classe='".$plan['plan_classe']."' AND plan_user='".$user['user_id']."' LIMIT 1");
  $stmt->execute();
  $stmt = $pdo->prepare("INSERT INTO plans (plan_classe,plan_user,plan_data) VALUES (:plan_classe,:plan_user,:plan_data)");
  $stmt->execute(
    array(':plan_classe' => $plan['plan_classe'],
     ':plan_user' => $user['user_id'],
     ':plan_data' => $plan['plan_data']
   )
  );
  return true;
}
function getPlansByUser(){
 global $user,$millesime;
 $pdo = sqliteConnect();
 $stmt = $pdo->prepare("SELECT plan_data,plan_classe FROM plans
  INNER JOIN classes
  ON 'classes'.'classe_id' = 'plans'.'plan_classe' 
  WHERE plan_user='".$user['user_id']."' AND 'classes'.'classe_millesime'=$millesime");
 $stmt->execute();
 $result=$stmt->fetchAll();
 return $result;

}
function delAllPlans(){
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM plans");
  $stmt->execute();
}
function delPlansByUser($user_id){
 $pdo = sqliteConnect();
 $stmt = $pdo->prepare("DELETE FROM plans WHERE plan_user='$user_id'");
 $stmt->execute();
}