<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function getClasses($millesime) {   
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("SELECT * FROM classes WHERE (classe_millesime='$millesime') ORDER BY classe_nom ASC");
    $stmt->execute();
    return $stmt->fetchAll();
}
function delClasse($classe_id) {
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("DELETE FROM classes WHERE classe_id=".$classe_id." LIMIT 1");
    $stmt->execute();
    $stmt = $pdo->prepare("DELETE FROM relations_eleves_classes WHERE rec_classe=".$classe_id."");
    $stmt->execute();  
    $stmt = $pdo->prepare("DELETE FROM shares WHERE share_type='classe' AND share_with='".$classe_id."'");   
    $stmt->execute();
    $stmt = $pdo->prepare("DELETE FROM plans WHERE plan_classe='".$classe_id."'");
    $stmt->execute();
    $stmt = $pdo->prepare("DELETE FROM groupes WHERE groupe_classe='".$classe_id."'");
    $stmt->execute();
    $stmt = $pdo->prepare("DELETE FROM agenda WHERE event_type_id='".$classe_id."' AND event_type='classe'");
    $stmt->execute();
     $stmt = $pdo->prepare("DELETE FROM sociogrammes WHERE sociogramme_classe='".$classe_id."'");
    $stmt->execute();
    $stmt = $pdo->prepare("DELETE FROM memos WHERE memo_type_id='".$classe_id."' AND memo_type='classroom'");
    $stmt->execute();
    return true;
}
function getOneClasse($classe_id) {
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("SELECT * FROM classes WHERE classe_id=$classe_id LIMIT 1");
    $stmt->execute();
    $result = $stmt->fetch();
    return $result;
}

function existClasse($classe_nom) {
    global $millesime;
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("SELECT classe_id FROM classes WHERE classe_nom='$classe_nom' AND classe_millesime='$millesime' LIMIT 1");
    $stmt->execute();
    $result = $stmt->fetchAll();
    if (count($result) != 0) {
        return $result[0]['classe_id'];
    } else {
        return false;
    }
}
function addClasse($classe_nom) {
    global $millesime;
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("INSERT INTO classes (classe_nom,classe_millesime) VALUES (:classe_nom,:classe_millesime)");
    $stmt->execute(
        array(':classe_nom' => "$classe_nom",
            ':classe_millesime' => $millesime
            )
        );
    return $pdo->lastInsertId('classe_id');
}
function updateClasse($classe) {
    $pdo = sqliteConnect();
    $updates = array();
    foreach ($classe as $key => $value) {
        if ($key != 'classe_id') {
            $updates[] = "" . $key . "='" . $value . "'";
        }
    }
    $updateStr = implode(",", $updates);
    $sql="UPDATE classes 
    SET " . $updateStr . "         
    WHERE classe_id='" . $classe['classe_id'] . "' LIMIT 1";     
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
}