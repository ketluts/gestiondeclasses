<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function getDefisByUser($user_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT defi_id, defi_titre, defi_description,defi_tags,defi_date,defi_user,defi_etoiles,defi_icon,defi_url,defi_token FROM defis WHERE defi_user='$user_id' ORDER BY defi_date DESC");
	$stmt->execute();
	return $stmt->fetchAll();
}
function getDefiById($defi_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT * FROM defis WHERE defi_id='$defi_id' LIMIT 1");
	$stmt->execute();
	return $stmt->fetch();
}
function getDefiByToken($defi_token){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT * FROM defis WHERE defi_token='$defi_token' LIMIT 1");
	$stmt->execute();
	return $stmt->fetch();
}
function addDefis($defi){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("INSERT INTO defis (defi_titre, defi_description,defi_tags,defi_date,defi_user,defi_etoiles,defi_icon,defi_url,defi_token) 
		VALUES (:defi_titre, :defi_description,:defi_tags,:defi_date,:defi_user,:defi_etoiles,:defi_icon,:defi_url,:defi_token)");
	$stmt->execute(
		array(':defi_titre' => $defi['defi_titre'],
			':defi_description' => $defi['defi_description'],
			':defi_tags' => $defi['defi_tags'],
			':defi_user' => $defi['defi_user'],
			':defi_etoiles' => $defi['defi_etoiles'],
			':defi_icon' => $defi['defi_icon'],
			':defi_date' =>$defi['defi_date'],
			':defi_url' =>  $defi['defi_url'],
			':defi_token' => genereRandomStringNb(6)
			)
		);
	return true;
}
function updateDefi($defi) {
	$pdo = sqliteConnect();
	$updates = array();
	foreach ($defi as $key => $value) {
		if ($key != 'defi_id') {
			$updates[] = "" . $key . "=" . $pdo->quote($value) . "";
		}
	}
	$updateStr = implode(",", $updates);
	$stmt = $pdo->prepare("UPDATE defis 
		SET " . $updateStr . "         
		WHERE defi_id='" . $defi['defi_id'] . "'");
	$stmt->execute();	
}
function delDefi($defi) {
	global $user;
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM defis WHERE defi_id=".$defi['defi_id']." AND defi_user='".$user['user_id']."'  LIMIT 1");
	$stmt->execute();
	$count = $stmt->rowCount();
	if($count>0){
		$stmt = $pdo->prepare("DELETE FROM etoiles WHERE etoile_defi=".$defi['defi_id']." AND etoile_type='defi'");
		$stmt->execute();
	}
}
function delAllDefis(){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM defis");
	$stmt->execute();
}