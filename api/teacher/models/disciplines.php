<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function getDisciplines(){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT * FROM disciplines");
	$stmt->execute();
	return($stmt->fetchAll());
}
function addDiscipline($discipline){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("INSERT INTO disciplines (discipline_name) 
		VALUES (:discipline_name)");
	$stmt->execute(
		array(':discipline_name' =>$discipline['discipline_name'])
	);
	return $pdo->lastInsertId('discipline_id');
}
function delDiscipline($discipline_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM disciplines WHERE discipline_id='".$discipline_id."' LIMIT 1");
	$stmt->execute();
	$stmt = $pdo->prepare("UPDATE users SET user_matiere='' WHERE user_matiere=$discipline_id");
	$stmt->execute();

}