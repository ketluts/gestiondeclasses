<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function addLink($link){  
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("INSERT INTO links (link_titre, link_url, link_date, link_user, link_favicon) VALUES (:link_titre, :link_url, :link_date, :link_user, :link_favicon)");
    $stmt->execute(
        array(':link_titre' => $link['link_titre'],
        	':link_url' => $link['link_url'],
            ':link_favicon' => $link['link_favicon'],      
            ':link_date' => $link['link_date'],    
            ':link_user' => $link['link_user']
            )
        );
    return true;
}
function getLinksByUser($user_id,$select="*"){
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("SELECT $select FROM links WHERE link_user='$user_id' ORDER BY link_date ASC");
    $stmt->execute();
    return $stmt->fetchAll();
}
function getLinkByIdAndUser($link_id,$user_id){
   $pdo = sqliteConnect();
   $stmt = $pdo->prepare("SELECT * FROM links WHERE link_user='$user_id' AND link_id='$link_id' LIMIT 1");
   $stmt->execute();
   return $stmt->fetch();   
}
function delLink($link_id,$user_id) {
    global $user;
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("DELETE FROM links WHERE link_id='$link_id' AND link_user='$user_id'  LIMIT 1");
    $stmt->execute();
}
function delLinksByUser($user_id){
 $pdo = sqliteConnect();
 $stmt = $pdo->prepare("DELETE FROM links WHERE link_user='$user_id'");
 $stmt->execute();
}