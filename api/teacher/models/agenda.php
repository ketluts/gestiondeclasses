<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function addAgendaEvent($event){  
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("INSERT INTO agenda (event_title,event_data,event_color,event_icon,event_type,event_type_id,event_start,event_end,event_allDay,event_user,event_visibility,event_state,event_date,event_lock,event_periode,event_discipline) VALUES (:event_title,:event_data,:event_color,:event_icon,:event_type,:event_type_id,:event_start,:event_end,:event_allDay,:event_user,:event_visibility,:event_state,:event_date,:event_lock,:event_periode,:event_discipline)");
  $stmt->execute(
    array(':event_title' => $event['event_title'],
     ':event_data' => nl2br($event['event_data']),
     ':event_color' => $event['event_color'],
     ':event_icon' => $event['event_icon'],
     ':event_type' => $event['event_type'],  
     ':event_type_id' => $event['event_type_id'],
     ':event_start' => $event['event_start'],
     ':event_end' => $event['event_end'],
     ':event_allDay' => $event['event_allDay'],
     ':event_user' => $event['event_user'],
     ':event_visibility' => $event['event_visibility'],
     ':event_state' => "enable",
     ':event_date' => $event['event_date'],
     ':event_lock' => $event['event_lock'],
     ':event_periode' => $event['event_periode'],
     ':event_discipline' => $event['event_discipline']
   )
  );
  return true;
}
//WHERE event_date>=$time-1296000
function getAllAgendaEvents($select="*"){
  global $millesime;
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT $select FROM agenda 
    INNER JOIN periodes
    ON 'agenda'.'event_periode' = 'periodes'.'periode_id'
    WHERE 'periodes'.'periode_parent'=$millesime
    ");
  $stmt->execute();
  return($stmt->fetchAll());
}
function delAgendaEventById($event_id){  
  global $user;
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM agenda
    WHERE event_id=$event_id AND event_user='".$user['user_id']."' AND event_lock!='1' LIMIT 1");
  $stmt->execute();
}
function delAgendaVisaById($event_id){  
  global $user;
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM agenda
    WHERE event_id=$event_id AND event_lock='1' LIMIT 1");
  $stmt->execute();
}
function updateAgendaEvent($event) {
  $pdo = sqliteConnect();
  $updates = array();
  foreach ($event as $key => $value) {
    if ($key != 'event_id') {
      $updates[] = "" . $key . "=" . $pdo->quote($value) . "";
    }
  }
  $updateStr = implode(",", $updates);
  $sql="UPDATE agenda
  SET " . $updateStr . "         
  WHERE event_id='" . $event['event_id'] . "' AND (event_lock IS NULL OR event_lock=0) LIMIT 1";  
  $stmt = $pdo->prepare($sql);
  $stmt->execute();
}
function delAllEvents(){
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM agenda");
  $stmt->execute();
}
function getEventById($event_id){  
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT * FROM agenda
    WHERE event_id=$event_id LIMIT 1");
  $stmt->execute();
  $result=$stmt->fetchAll();
  if(count($result)==0){
    return false;
  }  
   return($result[0]);
}