<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function addEtoile($etoile){ //TODO : A optimiser
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT etoile_id FROM etoiles 
		WHERE etoile_defi=".$etoile['etoile_defi']." 
		AND etoile_eleve=".$etoile['etoile_eleve']."   
		LIMIT 1");
	$stmt->execute();
	if($stmt->fetch()!==false){
		$stmt = $pdo->prepare("UPDATE etoiles SET etoile_value='".$etoile['etoile_value']."', etoile_date='".$etoile['etoile_date']."'
			WHERE etoile_defi=".$etoile['etoile_defi']." 
			AND etoile_eleve=".$etoile['etoile_eleve']." LIMIT 1");
		// $stmt = $pdo->prepare("UPDATE etoiles SET etoile_value='".$etoile['etoile_value']."', etoile_date='".$etoile['etoile_date']."'
		// 	WHERE etoile_defi=".$etoile['etoile_defi']." 
		// 	AND etoile_eleve=".$etoile['etoile_eleve']." 
		// 	AND etoile_value<".$etoile['etoile_value']."
		// 	 LIMIT 1");
		$stmt->execute();
	}else{
		$stmt = $pdo->prepare("INSERT INTO etoiles (etoile_eleve, etoile_defi, etoile_value, etoile_date,etoile_type) 
			VALUES (:etoile_eleve, :etoile_defi, :etoile_value, :etoile_date,:etoile_type)");
		$stmt->execute(
			array(':etoile_eleve' => $etoile['etoile_eleve'],
				':etoile_defi' => $etoile['etoile_defi'],
				':etoile_value' => $etoile['etoile_value'],
				':etoile_date' => $etoile['etoile_date'],
				':etoile_type' => $etoile['etoile_type']
				)
			);
	}
}
function delEtoilesByDefis($defis){
	$pdo = sqliteConnect();
	$where = array();
	for ($i=0; $i < count($defis); $i++) {         
		$where[] = "etoile_defi=" . $defis[$i]['defi_id'] . "";        
	}
	$sql="DELETE FROM etoiles WHERE (".implode(" OR ", $where).")";
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
}

function useEtoiles($etoile_id){
	global $user;
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT * FROM etoiles
		WHERE 'etoiles'.'etoile_id'=$etoile_id LIMIT 1");
	$stmt->execute();
	$etoile=$stmt->fetch();
	$state=($etoile['etoile_state']+1)%($etoile['etoile_value']+1);
	$stmt = $pdo->prepare("UPDATE etoiles SET etoile_state=$state WHERE etoile_id=$etoile_id LIMIT 1");
	$stmt->execute();
}
function delAllEtoiles(){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM etoiles");
	$stmt->execute();
}
function delEtoilesDeProgression(){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM etoiles WHERE etoile_type='progression'");
	$stmt->execute();
}
function getAllEtoiles($select="*"){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT $select FROM etoiles");
	$stmt->execute();
	return $stmt->fetchAll();
}

function getEtoileById($etoile_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT * FROM etoiles WHERE etoile_id='$etoile_id' LIMIT 1");
	$stmt->execute();
	return $stmt->fetch();
}