<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function delUser($user) {
  global $_;
  MainControl::init('school','backup');
  $pdo = sqliteConnect();
  if($user['user_pseudo']!=$_["pseudo_utilisateur"]){
    $stmt = $pdo->prepare("DELETE FROM users WHERE user_id=:user_id LIMIT 1");
    $stmt->bindValue(':user_id', $user['user_id']);
    $stmt->execute();
  }
  $stmt = $pdo->prepare("DELETE FROM sociogrammes WHERE sociogramme_user=:user_id");
  $stmt->bindValue(':user_id', $user['user_id']);
  $stmt->execute();
  $stmt = $pdo->prepare("DELETE FROM groupes WHERE groupe_user=:user_id");
  $stmt->bindValue(':user_id', $user['user_id']);
  $stmt->execute();
  $stmt = $pdo->prepare("DELETE FROM relations WHERE relation_user=:user_id");
  $stmt->bindValue(':user_id', $user['user_id']);
  $stmt->execute();
  $stmt = $pdo->prepare("DELETE FROM agenda WHERE event_user=:user_id");
  $stmt->bindValue(':user_id', $user['user_id']);
  $stmt->execute();
  //SUPPRESSION DES CONTROLES
  delControlesByUser($user['user_id']);
  //SUPPRESSION DES DEFIS
  $defis=getDefisByUser($user['user_id']);
  foreach ($defis as $defi) {
    delDefi($defi['defi_id']);
  }
  //SUPPRESSION DES MESSAGES
  $discussions=getMessagesByUser($user['user_id'],"*",0);
  foreach ($discussions as $discussion) {
   if($discussion['message_parent_message']=="-1"){
    relationDiscussionDel($message['message_id'],$user['user_id'],'user');
    $participants=getParticipantsByDiscussion($message['message_id']);
    if(count($participants)==0){
     discussionDel($_['message_id']);   
   }  
 }
}
  //SUPPRESSION DES FICHIERS, DES LIENS, DES PARTAGES
$files=getFilesByUser($user['user_id'],"*");
$dir=strtolower(substr($_['nom_etablissement'],0,1));
$dir_path=_DATA_."schools/".$dir.'/'.$_['nom_etablissement']."/files";
for ($i=0; $i <count($files) ; $i++) { 
  unlink($dir_path."/".$files[$i]['file_name']);
}
delFilesByUser($user['user_id']);
delLinksByUser($user['user_id']);
delShareByUser($user['user_id']);
delFiltersByUser($user['user_id']);
delPlansByUser($user['user_id']);
delMemosByUser($user['user_id']);
delFeedbacksByUser($user['user_id']);
return true;
}
function existUser($user_pseudo) {
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT * FROM users WHERE user_pseudo=:user_pseudo  LIMIT 1");
  $stmt->bindValue(':user_pseudo', $user_pseudo);
  $stmt->execute();
  $result = $stmt->fetchAll();
  // if(!is_countable($result)){
  //   return false;
  // }
  if (count($result) != 0) {
    return $result;
  } 
  return false;  
}
function addUser($user){  
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("INSERT INTO users (user_nom,user_prenom,user_pseudo,user_password,user_type,user_mail) VALUES (:user_nom,:user_prenom,:user_pseudo,:user_password,:user_type,:user_mail)");
  $stmt->execute(
    array(':user_nom' => $user['user_nom'],
      ':user_prenom' => $user['user_prenom'],
      ':user_pseudo' => $user['user_pseudo'],
      ':user_password' => $user['user_password'],
      ':user_type' => $user['user_type'],
      ':user_mail' => $user['user_mail'],    
      )
    );
  return true;
}
function verifUser($user_pseudo, $user_pass) {
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT * FROM users WHERE user_pseudo=:user_pseudo  LIMIT 1");
  $stmt->bindValue(':user_pseudo', $user_pseudo);
  $stmt->execute();
  $user = $stmt->fetch();
  if (!password_verify($user_pass, $user['user_password'])) {
    return false;
  } else {
    return $user;
  }
}

function getUserBySessionID($user_pseudo, $sessionID){
$pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT * FROM users WHERE user_pseudo=:user_pseudo AND user_sessionID=:user_sessionID  LIMIT 1");
  $stmt->bindValue(':user_pseudo', $user_pseudo);
   $stmt->bindValue(':user_sessionID', $sessionID);
  $stmt->execute();
  $user = $stmt->fetch();
  if ($user!=false) {
    return $user;
  } else {
    return false;
  }
}



function getUsers($select="*") {
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT $select FROM users ORDER BY user_pseudo ASC");
  $stmt->execute();
  return $stmt->fetchAll();
}
function getUserById($user_id){
 $pdo = sqliteConnect();
 $stmt = $pdo->prepare("SELECT * FROM users WHERE user_id=$user_id LIMIT 1");
 $stmt->execute();
 return $stmt->fetch();
}
function isAdmin() {
  global $_,$user;
  $admins=getAdmins();
  if(in_array($user['user_id'],$admins) OR count($admins)==0){
    return true;
  }
  return false;
}
function getAdmins(){
 global $_;
 $pdo = sqliteConnect();
 $stmt = $pdo->prepare("SELECT user_id FROM users WHERE user_type='admin'");
 $stmt->execute();
 $result =$stmt->fetchAll();
 $admins=array();
 foreach ($result as $users) {
  $admins[]=$users['user_id'];
}
return $admins;
}
function updateUser($user) {
  $pdo = sqliteConnect();
  $updates = array();
  foreach ($user as $key => $value) {
    if ($key != 'user_id') {
      $updates[] = "" . $key . "=:" . $key  . "";
    }
  }
  $updateStr = implode(",", $updates);
  $sql="UPDATE users 
  SET " . $updateStr . "         
  WHERE user_id='" . $user['user_id'] . "' LIMIT 1";

  $stmt = $pdo->prepare($sql);

  foreach ($user as $key => &$value){
    if ($key != 'user_id') {
      $stmt->bindParam(':'.$key.'', $value);   
    }
  }
  $stmt->execute();
}
function updateUserToken($user_token,$user_id){
 $pdo = sqliteConnect();
 $stmt = $pdo->prepare("UPDATE users SET user_token='".$user_token."' WHERE user_id='".$user_id."' LIMIT 1");
 $stmt->execute();
 return true;
}
function updateUserPasswordWithToken($user_pseudo,$user_token,$user_passe){
 $pdo = sqliteConnect();
 $stmt = $pdo->prepare("UPDATE users SET user_password='".password_hash($user_passe, PASSWORD_DEFAULT)."', user_token=null 
  WHERE user_pseudo='".$user_pseudo."' AND user_token='".$user_token."' LIMIT 1");
 $stmt->execute();
 $count = $stmt->rowCount();
 if($count==0){
  return false;
}
return true;
}
function updateUserPassword($user_passe){
  global $user;
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("UPDATE users SET user_password='".password_hash($user_passe, PASSWORD_DEFAULT)."', user_token=null 
    WHERE user_id='".$user['user_id']."' LIMIT 1");
  $stmt->execute();
  $count = $stmt->rowCount();
  if($count==0){
    return false;
  }
  return true;
}