<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function addFeedback($feedback){  
  global $user;
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("INSERT INTO feedbacks (feedback_user,feedback_code,feedback_data,feedback_date,feedback_description) VALUES (:feedback_user,:feedback_code,:feedback_data,:feedback_date,:feedback_description)");
  $stmt->execute(
    array(':feedback_user' => $user['user_id'],
      ':feedback_code' => $feedback['feedback_code'],
      ':feedback_data' => json_encode($feedback['feedback_data']),
      ':feedback_date' => $feedback['feedback_date'],
      ':feedback_description' => $feedback['feedback_description']
    )
  );
  return true;
}
function getFeedbacksByUser(){
 global $user;
 $pdo = sqliteConnect();
 $stmt = $pdo->prepare("SELECT * FROM feedbacks
  WHERE feedback_user='".$user['user_id']."'");
 $stmt->execute();
 $result=$stmt->fetchAll();
 return $result;
}
function getFeedbackById($feedback_id){
 global $user;
 $pdo = sqliteConnect();
 $stmt = $pdo->prepare("SELECT * FROM feedbacks
  WHERE feedback_user='".$user['user_id']."' AND feedback_id=$feedback_id");
 $stmt->execute();
 $result=$stmt->fetch();
 return $result;
}
function deleteFeedback($feedback_id) {
  global $user,$_;
  $feedback=getFeedbackById($feedback_id);
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM feedbacks WHERE feedback_id='$feedback_id' AND feedback_user='".$user['user_id']."'  LIMIT 1");
  $stmt->execute();
  $dir=strtolower(substr($_['nom_etablissement'],0,1));
  $dir_path=_DATA_."schools/".$dir.'/'.$_['nom_etablissement']."/files/";
  if(is_file($dir_path.'Feedback - '.$feedback['feedback_code'].'.zip')){
    unlink($dir_path.'Feedback - '.$feedback['feedback_code'].'.zip');
  }
}
function delAllFeedbacks(){
  global $_;
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM feedbacks");
  $stmt->execute();
  $dir=strtolower(substr($_['nom_etablissement'],0,1));
  $dir_path=_DATA_."schools/".$dir.'/'.$_['nom_etablissement']."/files/";
  $scan=scandir($dir_path);
  foreach ($scan as $file) {
    if(preg_match('#Feedback - #', $file)){
      unlink($dir_path.$file);
    }
  }
}
function delFeedbacksByUser($user_id){
 $pdo = sqliteConnect();
 $stmt = $pdo->prepare("DELETE FROM feedbacks WHERE feedback_user='$user_id'");
 $stmt->execute();
}