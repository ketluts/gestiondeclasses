<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function delLegends(){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM legends");
	$stmt->execute();
	return $stmt->fetchAll();
}
function addLegend($legend){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("INSERT INTO legends (legend_event,legend_text) 
		VALUES (:legend_event,:legend_text)");
	$stmt->execute(
		array(':legend_event' =>$legend[0],
			':legend_text' =>$legend[1]
			)
		);
}
function getLegends(){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT * FROM legends");
	$stmt->execute();
	return($stmt->fetchAll());
}