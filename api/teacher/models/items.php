<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function addItem($item){  
 global $user;
 $pdo = sqliteConnect();
 $stmt = $pdo->prepare("INSERT INTO items (item_name, item_categorie, item_value_max, item_date, item_user,item_colors,item_mode,item_descriptions,item_tags,item_color,item_vue, item_sous_categorie,item_cycle,item_feedbacks) VALUES (:item_name, :item_categorie, :item_value_max, :item_date, :item_user, :item_colors,:item_mode,:item_descriptions,:item_tags,:item_color,:item_vue,:item_sous_categorie,:item_cycle,:item_feedbacks)");
 $stmt->execute(
    array(':item_name' => $item['item_name'],
     ':item_categorie' => $item['item_categorie'],
     ':item_value_max' => $item['item_value_max'],      
     ':item_date' => $item['item_date'],  
     ':item_user' => $user['user_id'],
     ':item_colors' => $item['item_colors'],            
     ':item_mode' => $item['item_mode'],            
     ':item_descriptions' => $item['item_descriptions'],            
     ':item_tags' => $item['item_tags'],
     ':item_color' => $item['item_color'],
     ':item_vue' => $item['item_vue'],
     ':item_sous_categorie' => $item['item_sous_categorie'],
     ':item_cycle' => $item['item_cycle'],
     ':item_feedbacks' => $item['item_feedbacks']||Array()    
     )
    );
 return true;
}
function addProgression($progression,$user_id,$items,$eleves){  
    $pdo = sqliteConnect();
    $values=array();
    for ($i=0; $i < count($items); $i++) {     
        for ($j=0; $j < count($eleves); $j++) { 
            $values[] = array(
            $eleves[$j],
            $items[$i],
            $progression['rei_value'],
            $progression['rei_date'],
            $user_id,
            $progression['rei_user_type'],
            $progression['rei_activite'],
            $progression['rei_comment'],
            $progression['rei_symbole']
            );   
        }
    }



$row_length = count($values[0]);
$nb_rows = count($values);
$length = $nb_rows * $row_length;

/* Fill in chunks with '?' and separate them by group of $row_length */
$args = implode(',', array_map(
                                function($el) { return '('.implode(',', $el).')'; },
                                array_chunk(array_fill(0, $length, '?'), $row_length)
                            ));
$params = [];
// foreach($values as $row)
// {
//    foreach($row as $value)
//    {
//       $params[] = $value;
//    }
// }

for ($i=0; $i < count($values); $i++) { 
$row=$values[$i];
for ($j=0; $j <count($row) ; $j++) { 
  $value=$row[$j];
    $params[] = $value;
}
}




    $sql="INSERT INTO relations_eleves_items (rei_eleve, rei_item, rei_value, rei_date, rei_user,rei_user_type,rei_activite,rei_comment,rei_symbole) VALUES ".$args;
 //  echo print_r($params);
     // echo print_r($params,true);
   // echo $sql;
  //   exit;
    $stmt = $pdo->prepare($sql);
 $stmt->execute($params);
  //exit();
    //return true;
}
function delProgression($progression_id) {
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("DELETE FROM relations_eleves_items WHERE rei_id='$progression_id' LIMIT 1");
    $stmt->execute();
}
function delItemView($items,$eleves) {
    $pdo = sqliteConnect();
    $where = array();
    for ($i=0; $i < count($items); $i++) {     
        $w="(rei_item='".$items[$i]."' AND (";
        $t=array();
        for ($j=0; $j < count($eleves); $j++) {     
            $t[]="rei_eleve='".$eleves[$j]."'";         
        }
        $w.=implode(" OR ",$t)."))";
        $where[] = $w;       
    }
    $sql="DELETE FROM relations_eleves_items WHERE (".implode(" OR ", $where).") AND rei_value<0";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
}
function delItem($items,$user_id) {
    $pdo = sqliteConnect();
    $where = array();
    $admin=isAdmin();
    for ($i=0; $i < count($items); $i++) {     
        if($admin==true){
            $where[] = "(item_id=".$items[$i].")";
        }else{
            $where[] = "(item_id=".$items[$i]." AND item_user='$user_id')"; 
        }        
        delProgressionByItem($items[$i]['item_id']);
    }
    $sql="DELETE FROM items WHERE ".implode(" OR ", $where)."";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();    
}
function delProgressionByItem($item_id){
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("DELETE FROM relations_eleves_items WHERE rei_item='$item_id'");
    $stmt->execute();
}
function uncheckItem($item_id,$eleves){
    global $user;
    $where = array();   
    for ($j=0; $j < count($eleves); $j++) {     
        $where[]="rei_eleve='".$eleves[$j]."'";         
    }
    $pdo = sqliteConnect();
    $sql="DELETE FROM relations_eleves_items WHERE rei_item='$item_id' AND (".implode(" OR ", $where).") AND rei_user='".$user['user_id']."' AND rei_value>=0";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
}

function delAllProgressions(){
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("DELETE FROM relations_eleves_items");
    $stmt->execute();
}
function getItems($select="*"){
    $pdo = sqliteConnect();
    global $user;
    $stmt = $pdo->prepare("SELECT $select FROM items WHERE item_public='true' OR item_user='".$user['user_id']."' ORDER BY item_date ASC");
    $stmt->execute();
    return $stmt->fetchAll();
}
function getItemById($item_id,$select="*"){
    $pdo = sqliteConnect();
    global $user;
    $stmt = $pdo->prepare("SELECT $select FROM items WHERE (item_public='true' OR item_user='".$user['user_id']."') AND item_id=$item_id ");
    $stmt->execute();
    return $stmt->fetch();
}
function getProgressionById($progression_id,$select="*"){
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("SELECT $select FROM relations_eleves_items WHERE rei_id='$progression_id' LIMIT 1");
    $stmt->execute();
    return $stmt->fetch();
}
function getProgressionsByItems($ids,$select="*"){
    $pdo = sqliteConnect();
    if(count($ids)==0){
        return array();
    }
    $where = array();
    for ($i=0; $i < count($ids); $i++) {         
        $where[] = "rei_item=" . $ids[$i] . "";        
    }
    $sql="SELECT $select FROM relations_eleves_items
    -- LEFT JOIN users ON 'users'.'user_id'='relations_eleves_items'.'rei_user'
    WHERE (".implode(" OR ", $where).") ";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll();
}
function updateItem($item,$tagsMode,$user_id) {
    $pdo = sqliteConnect();
    $updates = array();
    foreach ($item as $key => $value) {
        if ($key != 'item_id' && $key != 'item_tags') {
            $updates[] = "" . $key . "=" . $pdo->quote($value) . "";
        }
    }
    $updateStr = implode(",", $updates);
    $tagsStr="";
    if(isset($item['item_tags'])){
       if($tagsMode=="replace"){
        $tagsStr="item_tags=".$pdo->quote($item['item_tags']);
    }
    else if($tagsMode=="add"){
        $tagsStr='item_tags=IFNULL(item_tags,"") || ",'.$item['item_tags'].'"';
    }  
    if(count($updates)>0){
       $tagsStr=','.$tagsStr;   
   }
}
//TODO produit des mots clés du style ', 5e'  qu'il faut nettoyer après avec app.itemsTagsRender(item_tags);.
//Voir pour nettoyer les chaînes 
$userClause=" AND item_user=$user_id";
if(isAdmin()==true){
    $userClause="";
}
$sql="UPDATE items 
SET " . $updateStr .$tagsStr. "         
WHERE item_id='" . $item['item_id'] . "'".$userClause." LIMIT 1";

$stmt = $pdo->prepare($sql);
$stmt->execute();
}
function updateProgression($progression) {
    $pdo = sqliteConnect();
    $updates = array();
    foreach ($progression as $key => $value) {
        if ($key != 'rei_id') {
            $updates[] = "" . $key . "=" . $pdo->quote($value) . "";
        }
    }
    $updateStr = implode(",", $updates);
    
$sql="UPDATE relations_eleves_items 
SET " . $updateStr. "         
WHERE rei_id='" . $progression['rei_id'] . "' LIMIT 1";

 
$stmt = $pdo->prepare($sql);
$stmt->execute();
}