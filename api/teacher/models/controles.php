<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function addControle($controle){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("INSERT INTO controles (controle_titre, controle_user,controle_note,controle_coefficient,controle_date,controle_classe,controle_categorie,controle_periode,controle_discipline) 
		VALUES (:controle_titre,:controle_user,:controle_note,:controle_coefficient,:controle_date,:controle_classe,:controle_categorie,:controle_periode,:controle_discipline)");
	$stmt->execute(
		array(':controle_titre' => $controle['controle_titre'],			
			':controle_user' => $controle['controle_user'],
			':controle_note' => $controle['controle_note'],
			':controle_coefficient' => $controle['controle_coefficient'],
			':controle_categorie' => $controle['controle_categorie'],
			':controle_classe' => $controle['controle_classe'],
			':controle_periode' => $controle['controle_periode'],
			':controle_date' => $controle['controle_date'],
			':controle_discipline' => $controle['controle_discipline']
		)
	);
	return true;
}
function getControlesById($controle_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT * FROM controles 
		WHERE controle_id='$controle_id'
		LIMIT 1");
	$stmt->execute();
	return $stmt->fetch();
}
function getControlesByPeriode($periode_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT controle_id FROM controles 
		WHERE controle_periode='$periode_id'");
	$stmt->execute();
	return $stmt->fetchAll();
}
function getAllControles(){
	global $millesime;
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT * FROM controles
		INNER JOIN periodes
		ON 'periodes'.'periode_id' = 'controles'.'controle_periode' 
		WHERE 'periodes'.'periode_parent'='$millesime'
		ORDER BY controle_date DESC");
	$stmt->execute();
	return $stmt->fetchAll();
}

function updateControle($controle){
	global $user;
	$pdo = sqliteConnect();
	$sql="UPDATE controles SET 
	controle_titre=:controle_titre,
	controle_coefficient='".$controle['coefficient']."',
	controle_periode=:controle_periode,
	controle_categorie=:controle_categorie,
	controle_date=:controle_date,
	controle_note='".$controle['note']."'		
	WHERE (
		controle_titre!=:controle_titre
		OR controle_coefficient!='".$controle['coefficient']."'
		OR controle_categorie!=:controle_categorie
		OR controle_periode!=:controle_periode
		OR controle_note!='".$controle['note']."'	
		OR controle_date!='".$controle['date']."'		
	) AND controle_id='".$controle['id']."'
	AND controle_user='".$user['user_id']."'
	LIMIT 1";
	$stmt = $pdo->prepare($sql);
	$stmt->execute(
		array(':controle_titre' => $controle['controle_titre'],	
			':controle_periode' => $controle['controle_periode'],
			':controle_categorie' => $controle['controle_categorie'],
				':controle_date' => $controle['controle_date']
		)
	);
}
function delControle($controle) {
	global $user;
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM controles WHERE controle_id=:controle_id AND controle_user=:user_id LIMIT 1");
	$stmt->bindValue(':user_id', $user['user_id']);
	$stmt->bindValue(':controle_id', $controle['controle_id']);
	$stmt->execute();
	$count = $stmt->rowCount();
	if($count>0){
		$stmt = $pdo->prepare("DELETE FROM notes WHERE note_controle='".$controle['controle_id']."'");
		$stmt->execute();
		$stmt = $pdo->prepare("DELETE FROM etoiles WHERE etoile_defi='".$controle['controle_id']."' AND etoile_type='progression'");
		$stmt->execute();
	}
}
function delAllControles(){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM controles");
	$stmt->execute();
}
function delControlesByUser($user_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM controles WHERE controle_user=$user_id");
	$stmt->execute();
}