<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function addFilter($filter){  
    global $user;
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("INSERT INTO filters (filter_data,filter_user) VALUES (:filter_data,:filter_user)");
    $stmt->execute(
        array(
            ':filter_data' => $filter,
            ':filter_user' => $user['user_id']            
            )
        );
    return true;
}
function getFiltersByUser(){
   global $user;
   $pdo = sqliteConnect();
   $stmt = $pdo->prepare("SELECT filter_data,filter_id FROM filters WHERE filter_user='".$user['user_id']."'");
   $stmt->execute();
   return $stmt->fetchAll();
}
function delAllFilters(){
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM filters");
  $stmt->execute();
}
function delFiltersByUser($user_id){
 $pdo = sqliteConnect();
 $stmt = $pdo->prepare("DELETE FROM filters WHERE filter_user='$user_id'");
 $stmt->execute();
}
function deleteFilters($filters) {
  global $user;
  $pdo = sqliteConnect();
  $where = array();
  for ($i=0; $i < count($filters); $i++) {     
    $where[] = "(filter_id=".$filters[$i]." AND filter_user='".$user['user_id']."')";         
}
$sql="DELETE FROM filters WHERE ".implode(" OR ", $where)."";
$stmt = $pdo->prepare($sql);
$stmt->execute();
}