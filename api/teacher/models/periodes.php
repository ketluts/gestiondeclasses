<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function delPeriode($periode_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM periodes WHERE periode_id='".$periode_id."'");
	$stmt->execute();
	$stmt = $pdo->prepare("DELETE FROM appreciations WHERE appreciation_periode='".$periode_id."'");
	$stmt->execute();
	$stmt = $pdo->prepare("DELETE FROM agenda WHERE event_periode='".$periode_id."'");
	$stmt->execute();
	$controlesByPeriode=getControlesByPeriode($periode_id);
	$stmt = $pdo->prepare("DELETE FROM controles WHERE controle_periode='".$periode_id."'");
	$stmt->execute();
	foreach ($controlesByPeriode as $controle) {
		$stmt = $pdo->prepare("DELETE FROM notes WHERE note_controle='".$controle['controle_id']."'");
		$stmt->execute();
		$stmt = $pdo->prepare("DELETE FROM etoiles WHERE etoile_defi='".$controle['controle_id']."' AND etoile_type='progression'");
		$stmt->execute();
	}
}
function addPeriode($periode){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("INSERT INTO periodes (periode_titre,periode_start,periode_end,periode_type,periode_parent) 
		VALUES (:periode_titre,:periode_start,:periode_end,:periode_type,:periode_parent)");
	$stmt->execute(
		array(':periode_titre' =>$periode['periode_titre'],
			':periode_start' =>$periode['periode_start'],
			':periode_end' =>$periode['periode_end'],
			':periode_type' =>$periode['periode_type'],
			':periode_parent' =>$periode['periode_parent']
		)
	);
	return $pdo->lastInsertId('periode_id');
}
function getPeriodes(){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT * FROM periodes");
	$stmt->execute();
	return($stmt->fetchAll());
}
function getPeriodesByMillesime($millesime_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT periode_id FROM periodes WHERE periode_parent=$millesime_id");
	$stmt->execute();
	return($stmt->fetchAll());
}
function setPeriode($periode_id){
	$pdo = sqliteConnect();

	$stmt = $pdo->prepare("UPDATE periodes SET periode_active=0 WHERE periode_active=1 LIMIT 1");
	$stmt->execute();
	$stmt = $pdo->prepare("UPDATE periodes SET periode_active=1 WHERE periode_id=:periode_id LIMIT 1");
	$stmt->execute(
		array(':periode_id' =>$periode_id			
		)
	);
}
function lockPeriode($periode_id,$periode_lock,$time){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("UPDATE periodes SET periode_lock=$periode_lock, periode_lock_date=$time WHERE periode_id=$periode_id");
	$stmt->execute();
}

function updatePeriode($periode) {
	$pdo = sqliteConnect();
	$updates = array();
	foreach ($periode as $key => $value) {
		if ($key != 'periode_id') {
			$updates[] = "" . $key . "=" . $pdo->quote($value) . "";
		}
	}
	$updateStr = implode(",", $updates);
	$stmt = $pdo->prepare("UPDATE periodes 
		SET " . $updateStr . "         
		WHERE periode_id='" . $periode['periode_id'] . "'");
	$stmt->execute();	
}

function getActiveMillesime(){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT periode_parent FROM periodes WHERE periode_active=1 LIMIT 1");
	$stmt->execute();
	$result = $stmt->fetch();
	$result=$result['periode_parent'];
	if($result===null){
		return -1;
	}
	return $result;

}
function getActivePeriode(){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT periode_id FROM periodes WHERE periode_active=1 LIMIT 1");
	$stmt->execute();
	$result = $stmt->fetch();
	$result=$result['periode_id'];
	if($result===null){
		return -1;
	}
	return $result;

}
function delMillesime($millesime_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM periodes WHERE periode_id='".$millesime_id."' LIMIT 1");
	$stmt->execute();
}

function isPeriodeLock($periode_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT periode_lock FROM periodes WHERE periode_id='$periode_id' LIMIT 1");
	$stmt->execute();
	$result = $stmt->fetch();	
	$lock=$result['periode_lock'];
	if($lock==1){		
		$_SESSION['render']['info'][] =array("Cette période est vérouillée.","error");
		MainControl::init('render');  
		exit;
	}

}