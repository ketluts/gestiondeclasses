<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function addNote($note){ //TODO : A optimiser
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT note_id FROM notes 
		WHERE note_controle='".$note['note_controle']."' 
		AND note_eleve='".$note['note_eleve']."'   
		LIMIT 1");
	$stmt->execute();
	$result=$stmt->fetch();
	unset($stmt);
	if($result!==false){
		if(trim($note['note_value'])==""){
			deleteNote($result);
			return;
		}else{
			$pdo = sqliteConnect();
			$stmt = $pdo->prepare("UPDATE notes SET note_value='".$note['note_value']."', note_date='".$note['note_date']."'
				WHERE note_controle=".$note['note_controle']." 
				AND note_eleve=".$note['note_eleve']." 
				AND note_value!='".$note['note_value']."' LIMIT 1");
			$stmt->execute();
		}
	}else{
		if($note['note_value']!=""){

			$stmt = $pdo->prepare("INSERT INTO notes (note_eleve, note_controle, note_value, note_date) 
				VALUES (:note_eleve, :note_controle, :note_value, :note_date)");
			$stmt->execute(
				array(':note_eleve' => $note['note_eleve'],
					':note_controle' => $note['note_controle'],
					':note_value' => $note['note_value'],
					':note_date' => $note['note_date']
				)
			);
		}
	}
}
function getNoteById($note_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT * FROM notes 
		WHERE note_id='$note_id'
		LIMIT 1");
	$stmt->execute();
	return $stmt->fetch();
}
function updateNote($note){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("UPDATE notes SET note_value='".$note['note_value']."', note_bonus='".$note['note_bonus']."' WHERE note_id='".$note['note_id']."' LIMIT 1");
	$stmt->execute();
}
function deleteNote($note){
	$pdo = sqliteConnect();
	$sql="DELETE FROM notes WHERE note_id=".$note['note_id']." LIMIT 1";
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
}

function delAllNotes(){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM notes");
	$stmt->execute();
}
function getAllNotes($select="*"){
	global $millesime;
	$pdo = sqliteConnect();
	$sql="SELECT ".$select." FROM notes
	INNER JOIN controles
	ON 'controles'.'controle_id' = 'notes'.'note_controle' 
	INNER JOIN periodes
	ON 'periodes'.'periode_id' = 'controles'.'controle_periode' 
	WHERE 'periodes'.'periode_parent'='$millesime'
	";		
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	return($stmt->fetchAll());
}