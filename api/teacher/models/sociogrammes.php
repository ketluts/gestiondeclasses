<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function addSociogramme($sociogramme){  
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("INSERT INTO sociogrammes (sociogramme_data,sociogramme_classe,sociogramme_date,sociogramme_user) VALUES (:sociogramme_data,:sociogramme_classe,:sociogramme_date,:sociogramme_user)");
  $stmt->execute(
    array(':sociogramme_data' => $sociogramme['sociogramme_data'],
      ':sociogramme_classe' => $sociogramme['sociogramme_classe'],
      ':sociogramme_date' => $sociogramme['sociogramme_date'],
      ':sociogramme_user' => $sociogramme['sociogramme_user']
      )
    );
  return true;
}
function getSociogrammesByUser($select="*"){
  global $user,$millesime;
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT $select FROM sociogrammes
  INNER JOIN classes
 ON 'classes'.'classe_id' = 'sociogrammes'.'sociogramme_classe' 
  WHERE sociogramme_user='".$user['user_id']."' AND 'classes'.'classe_millesime'=$millesime ORDER BY sociogramme_date ASC");
  $stmt->execute();
  return $stmt->fetchAll();
}
function delSociogramme($sociogramme){
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM sociogrammes WHERE sociogramme_id=".$sociogramme['sociogramme_id']." AND sociogramme_user=".$sociogramme['sociogramme_user']." LIMIT 1");
  $stmt->execute();    
}
function delAllSociogrammes(){
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM sociogrammes");
  $stmt->execute();
}