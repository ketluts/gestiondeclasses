<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function deleteEleve($eleves) {
  global $_;
  MainControl::init('school','backup'); 
  $pdo = sqliteConnect();


  $where = array();
  $where_etoiles= array();
  $where_notes= array();
  $where_rec= array();
  $where_relation= array();
  $where_agenda= array();
  $where_shares= array();
  $where_rei= array();
  for ($i=0; $i < count($eleves); $i++) {     
    $where_eleves[] = "(eleve_id=".$eleves[$i].")"; 
    $where_etoiles[] = "(etoile_eleve=".$eleves[$i].")"; 
    $where_notes[] = "(note_eleve=".$eleves[$i].")"; 
    $where_rec[] = "(rec_eleve=".$eleves[$i].")"; 
    $where_relation[] = "(relation_from=".$eleves[$i]." OR relation_to=".$eleves[$i].")"; 
    $where_agenda[] = "(event_type_id=".$eleves[$i].")"; 
    $where_shares[] = "(share_with=".$eleves[$i].")"; 
    $where_rei[] = "(rei_eleve=".$eleves[$i].")";    
    $where_memos[] = "(memo_type='student' AND memo_type_id=".$eleves[$i].")";    
  }
  

  for ($j=0; $j < count($eleves); $j++) {  
    $eleve=getEleveById($eleves[$j]);

    $messages=getMessagesByEleve($eleves[$j],'*',0); 
    for ($i=0; $i < count($messages); $i++) { 
      if($messages[$i]['message_parent_message']==-1){
       relationDiscussionDel($messages[$i]['message_id'],$eleves[$j],'eleve');
       $participants=getParticipantsByDiscussion($messages[$i]['message_id']);
       if(count($participants)==0){
         discussionDel($messages[$i]['message_id']);   
       }else{
        $discussion['message_subject']="";
        $discussion['message_content']=$eleve['eleve_prenom']." ".$eleve['eleve_nom']." a quitté la discussion.";
        $discussion['message_parent_message']=$messages[$i]['message_id'];
        $discussion['message_author']=$eleves[$j];
        $discussion['message_author_type']='eleve';
        $discussion['message_date']=$_['time'];
        $discussion_id=discussionAdd($discussion);
      }
    }
  }
}
 $stmt = $pdo->prepare("DELETE FROM memos WHERE ".implode(" OR ", $where_memos)."");
  $stmt->execute();
 $stmt = $pdo->prepare("DELETE FROM eleves WHERE ".implode(" OR ", $where_eleves)."");
  $stmt->execute(); 
  $stmt = $pdo->prepare("DELETE FROM etoiles WHERE ".implode(" OR ", $where_etoiles)."");
  $stmt->execute();
  $stmt = $pdo->prepare("DELETE FROM notes WHERE ".implode(" OR ", $where_notes)."");
  $stmt->execute();
  $stmt = $pdo->prepare("DELETE FROM relations_eleves_classes WHERE ".implode(" OR ", $where_rec)."");
  $stmt->execute();
  $stmt = $pdo->prepare("DELETE FROM relations WHERE ".implode(" OR ", $where_relation)."");
  $stmt->execute();  
  $stmt = $pdo->prepare("DELETE FROM agenda WHERE ".implode(" OR ", $where_agenda)." AND event_type='eleve'");
  $stmt->execute();
  $stmt = $pdo->prepare("DELETE FROM shares WHERE  ".implode(" OR ", $where_shares)." AND share_type='eleve'");
  $stmt->execute();
  $stmt = $pdo->prepare("DELETE FROM relations_eleves_items WHERE ".implode(" OR ", $where_rei)."");
  $stmt->execute();
return true;
}
function getEleveEvents($eleve_id, $event_date) {
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT * FROM events WHERE event_eleve_id=$eleve_id  LIMIT 1");
  $stmt->execute();
  return $stmt->fetchAll();
}
function getElevesByClasseId($classe_id,$select="*") {
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT $select FROM relations_eleves_classes 
    INNER JOIN eleves
    ON 'eleves'.'eleve_id' = 'relations_eleves_classes'.'rec_eleve'
    WHERE rec_classe=$classe_id ORDER BY eleve_nom ASC");
  $stmt->execute();
  return $stmt->fetchAll();
}
function getElevesByClasses() {
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT rec_eleve, rec_classe FROM relations_eleves_classes");
  $stmt->execute();
  return $stmt->fetchAll();
}
function getAllEleves($select="*") {
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT $select FROM eleves");
  $stmt->execute();
  return $stmt->fetchAll();
}

function getEleveById($eleve_id) {
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT * FROM eleves 
    WHERE eleve_id=$eleve_id  LIMIT 1");
  $stmt->execute();
  $result = $stmt->fetch();
  return $result;
}
function addEleve($eleve, $eleve_classe) {
  $pdo = sqliteConnect();  
  if($eleve['eleve_id']==""){
    $stmt = $pdo->prepare("INSERT INTO eleves (eleve_nom,eleve_token,eleve_prenom,eleve_classe,eleve_genre,eleve_birthday) 
      VALUES (:eleve_nom, :eleve_token, :eleve_prenom,:eleve_classe,:eleve_genre,:eleve_birthday)");
    $stmt->execute(
      array(':eleve_nom' => $eleve['eleve_nom'],
        ':eleve_token'=> genereRandomStringNb(6),
        ':eleve_prenom'=> $eleve['eleve_prenom'],
        ':eleve_classe'=>"1",
        ':eleve_genre'=> $eleve['eleve_genre'],
        ':eleve_birthday'=> $eleve['eleve_birthday'],
      )
    );
    $eleve['eleve_id']=$pdo->lastInsertId('eleve_id');  
  }
  $stmt = $pdo->prepare("INSERT INTO relations_eleves_classes (rec_eleve,rec_classe) 
    VALUES (".$eleve['eleve_id'].",".$eleve_classe.")");
  $stmt->execute();
  $updEleve['eleve_id']=$eleve['eleve_id'];   
  $updEleve['eleve_classe']=1;
  updateEleve($updEleve);
  return;
}
function deleteRec($eleve_id,$classe_id){ 
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT rec_id FROM relations_eleves_classes WHERE rec_eleve=".$eleve_id."");
  $stmt->execute();
  $count = count($stmt->fetchAll());
  // On passe eleve_classe à 0 si l'élève n'appartient à aucune classe.
  if($count==1){
    $eleve['eleve_id']=$eleve_id;
    $eleve['eleve_classe']=0;
    updateEleve($eleve);
//    MainControl::init('eleves','getAlone');  
  }
  $stmt = $pdo->prepare("DELETE FROM relations_eleves_classes WHERE rec_eleve=$eleve_id AND rec_classe=$classe_id LIMIT 1");
  $stmt->execute();
}
function existEleve($eleve){ 
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT eleve_id FROM eleves 
    WHERE eleve_nom='".$eleve['eleve_nom']."' 
    AND eleve_prenom='".$eleve['eleve_prenom']."'
    LIMIT 1");
  $stmt->execute();
  $result = $stmt->fetchAll();
  if (count($result) != 0) {
    return true;
  } else {
    return false;
  }
}
function updateEleve($eleve) {
  $pdo = sqliteConnect();
  $updates = array();
  foreach ($eleve as $key => $value) {
    if ($key != 'eleve_id') {
      $updates[] = "" . $key . "=" . $pdo->quote($value) . "";
    }
  }
  $updateStr = implode(",", $updates);
  $sql="UPDATE eleves 
  SET " . $updateStr . "         
  WHERE eleve_id='" . $eleve['eleve_id'] . "'";
  $stmt = $pdo->prepare($sql);
  $stmt->execute();
}

function delAllEleves(){
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM eleves");
  $stmt->execute();
}