<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function addFile($file){  
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("INSERT INTO files (file_name, file_type, file_size, file_fullname, file_date, file_user,file_visibility) VALUES (:file_name, :file_type, :file_size, :file_fullname, :file_date, :file_user,:file_visibility)");
    $stmt->execute(
        array(':file_name' => $file['file_name'],
        	':file_type' => $file['file_type'],
            ':file_size' => $file['file_size'],
            ':file_fullname' => $file['file_fullname'],            
            ':file_date' =>$file['file_date'],
            ':file_user' => $file['file_user'],
            ':file_visibility'=>$file['file_visibility']
            )
        );
    return $pdo->lastInsertId('classe_id');
}
function getFilesByUser($user_id,$select="*"){
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("SELECT $select FROM files WHERE file_user='$user_id' ORDER BY file_date ASC");
    $stmt->execute();
    return $stmt->fetchAll();
}
function getFileById($file_id){
    global $user;
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("SELECT * FROM files WHERE file_user='".$user['user_id']."' AND file_id=$file_id");
    $stmt->execute();
    return $stmt->fetch();
}
function getFileByIdAndUser($file_id,$user_id){
   $pdo = sqliteConnect();
   $stmt = $pdo->prepare("SELECT * FROM files WHERE file_user='$user_id' AND file_id='$file_id' LIMIT 1");
   $stmt->execute();
   return $stmt->fetch();   
}
function delFile($file_id,$user_id) {
    global $user;
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("DELETE FROM files WHERE file_id='$file_id' AND file_user='$user_id'  LIMIT 1");
    $stmt->execute();
}
function delFilesByUser($user_id){
 $pdo = sqliteConnect();
 $stmt = $pdo->prepare("DELETE FROM files WHERE file_user='$user_id'");
 $stmt->execute();
}
function delAllFiles(){
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("DELETE FROM files");
    $stmt->execute();
}