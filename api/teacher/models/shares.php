<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function addShare($share){  
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("INSERT INTO shares (share_type,share_item_id,share_item_type,share_with,share_date,share_user) VALUES (:share_type,:share_item_id,:share_item_type,:share_with,:share_date,:share_user)");
  $stmt->execute(
    array(':share_type' => $share['share_type'],
      ':share_item_id' => $share['share_item_id'],
      ':share_item_type' => $share['share_item_type'],
      ':share_with' => $share['share_with'],
      ':share_user' => $share['share_user'],
      ':share_date' => $share['share_date']
      )
    );
  return true;
}
function getSharesByItem($item_id,$item_type){  
  $pdo = sqliteConnect();
   $stmt = $pdo->prepare("SELECT * FROM shares WHERE share_item_id=$item_id AND share_item_type='$item_type'");
  $stmt->execute();
  return $stmt->fetchAll();
}
function getSharesById($share_id){  
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT * FROM shares WHERE share_id=$share_id LIMIT 1");
  $stmt->execute();
  return $stmt->fetch();
}

function delShareByItem($item_id,$type){  
  global $user;
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM shares
    WHERE share_item_id='".$item_id."' AND share_item_type='$type' AND share_user=".$user['user_id']."");
  $stmt->execute();
}
function delShareByUser($user_id){  
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM shares
    WHERE share_user='".$user_id."'");
  $stmt->execute();
}
function delShareById($share_id){  
  global $user;
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM shares
    WHERE share_id=$share_id AND share_user='".$user['user_id']."' LIMIT 1");
  $stmt->execute();
}
function isShared($item_id,$item_type){
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT COUNT(share_id) FROM shares   
    WHERE share_item_id='$item_id' AND share_item_type='$item_type'");
  $stmt->execute();
  return $stmt->fetchColumn();
}
function delAllShares(){
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM shares");
  $stmt->execute();
}