<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function getRelationsByEleve($eleve_id,$select){
 global $_;
 $pdo = sqliteConnect(); 
 $stmt = $pdo->prepare("SELECT $select FROM relations WHERE relation_from=$eleve_id"); 
 $stmt->execute();
 return $stmt->fetchAll();
}
function publicAddRelation($relation,$type_num){
  global $_;
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("INSERT INTO relations (relation_from,relation_to,relation_user,relation_type) VALUES (:relation_from,:relation_to,:relation_user,:relation_type)");
  $stmt->execute(
    array(':relation_from' => $relation['from'],
      ':relation_to' => $relation['to'],
   
      ':relation_user' => 'eleve_'.$relation['from'],
      ':relation_type' => $type_num
      )
    );
  return true;
}
function publicDelRelations($eleve_id){
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("DELETE FROM relations WHERE relation_user='eleve_".$eleve_id."'");
  $stmt->execute();  
  return true;
}