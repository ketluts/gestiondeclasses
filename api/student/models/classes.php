<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function getClasses() {
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("SELECT classe_id,classe_nom,classe_destinataires,classe_intelligences FROM classes ORDER BY classe_nom ASC");
    $stmt->execute();
    return $stmt->fetchAll();
}
function getClassesByEleve($eleve_id){
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("SELECT * FROM relations_eleves_classes
        INNER JOIN classes ON 'classes'.'classe_id'='relations_eleves_classes'.'rec_classe'
        WHERE rec_eleve='$eleve_id'");
    $stmt->execute();
    $result = $stmt->fetchAll();
    if(isset($result->num_rows) && $result->num_rows > 0){
        return $result;
    }
    else{ 
        return [];
    }
}