<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function getNotesByEleve($eleve_id,$select){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT ".$select." FROM notes 
		INNER JOIN controles
		ON 'controles'.'controle_id' = 'notes'.'note_controle'
		INNER JOIN classes
		ON 'classes'.'classe_id' = 'controles'.'controle_classe'		
		WHERE note_eleve='".$eleve_id."'
		ORDER BY controle_date DESC");
	$stmt->execute();
	return($stmt->fetchAll());
}