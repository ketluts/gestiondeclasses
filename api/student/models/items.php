<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function addProgression($progression,$user_id,$items,$eleves){  
    $pdo = sqliteConnect();
    $values=array();
    for ($i=0; $i < count($items); $i++) {     
        for ($j=0; $j < count($eleves); $j++) { 
            $values[] = array(
            $eleves[$j],
            $items[$i],
            $progression['rei_value'],
            $progression['rei_date'],
            $user_id,
            $progression['rei_user_type'],
            $progression['rei_activite'],
            $progression['rei_comment'],
            $progression['rei_symbole']
            );   
        }
    }



$row_length = count($values[0]);
$nb_rows = count($values);
$length = $nb_rows * $row_length;

/* Fill in chunks with '?' and separate them by group of $row_length */
$args = implode(',', array_map(
                                function($el) { return '('.implode(',', $el).')'; },
                                array_chunk(array_fill(0, $length, '?'), $row_length)
                            ));
$params = [];
// foreach($values as $row)
// {
//    foreach($row as $value)
//    {
//       $params[] = $value;
//    }
// }

for ($i=0; $i < count($values); $i++) { 
$row=$values[$i];
for ($j=0; $j <count($row) ; $j++) { 
  $value=$row[$j];
    $params[] = $value;
}
}




    $sql="INSERT INTO relations_eleves_items (rei_eleve, rei_item, rei_value, rei_date, rei_user,rei_user_type,rei_activite,rei_comment,rei_symbole) VALUES ".$args;
 //  echo print_r($params);
     // echo print_r($params,true);
   // echo $sql;
  //   exit;
    $stmt = $pdo->prepare($sql);
 $stmt->execute($params);
  //exit();
    //return true;
}
function delProgression($progression_id) {
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("DELETE FROM relations_eleves_items WHERE rei_id='$progression_id' LIMIT 1");
    $stmt->execute();
}
function publicUncheckItem($item_id,$eleve_id){
    global $user;
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("DELETE FROM relations_eleves_items WHERE rei_item='$item_id' AND rei_eleve='$eleve_id' AND rei_user_type='eleve' AND rei_value>=0");
    $stmt->execute();
}

function getProgressionById($progression_id,$select="*"){
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("SELECT $select FROM relations_eleves_items WHERE rei_id='$progression_id' LIMIT 1");
    $stmt->execute();
    return $stmt->fetch();
}
function getProgressionsByEleve($eleve_id,$select){
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("SELECT rei_item FROM relations_eleves_items WHERE rei_eleve=$eleve_id AND rei_value<0");
    $stmt->execute();
    $items_enabled=$stmt->fetchAll();
    $items_id=[];
    foreach ($items_enabled as $items) {
      $items_id[]="rei_item=" . $items['rei_item'] . "";
  }
  if(count($items_id)>0){
   $where="AND (".implode(" OR ", $items_id).")";
}
else{
 return array();
}
$sql="SELECT $select FROM relations_eleves_items
LEFT JOIN items
ON 'relations_eleves_items'.'rei_item' = 'items'.'item_id'
WHERE 'relations_eleves_items'.'rei_eleve'='$eleve_id' $where";
$stmt = $pdo->prepare($sql);
$stmt->execute();
return $stmt->fetchAll();
}
function checkAutoEvaluation($eleve_id,$item_id){
    $pdo = sqliteConnect();
    $stmt = $pdo->prepare("SELECT rei_item FROM relations_eleves_items WHERE rei_eleve='$eleve_id' AND rei_value==-2 AND rei_item='$item_id' LIMIT 1");
    $stmt->execute();
    $result=$stmt->fetch();
    if(count($result)>0){
        return true;
    }
    return false;
}