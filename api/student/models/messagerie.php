<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function getMessagesByEleve($eleve_id,$select="*",$syncId){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT $select FROM relations_messagerie
		LEFT JOIN messages ON ('messages'.'message_id'='relations_messagerie'.'rm_messages') OR ('messages'.'message_parent_message'='relations_messagerie'.'rm_messages')
		LEFT JOIN users ON ('users'.'user_id'='messages'.'message_author' AND 'messages'.'message_author_type'='user')
		LEFT JOIN eleves ON ('eleves'.'eleve_id'='messages'.'message_author' AND 'messages'.'message_author_type'='eleve')
		WHERE rm_type_id='".$eleve_id."' AND rm_type='eleve' AND 'messages'.'message_id'>'".$syncId."'
		ORDER BY 'messages'.'message_date' DESC");
	$stmt->execute();
	return $stmt->fetchAll();
}

function getParticipantsByDiscussion($discussion_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT user_pseudo,user_matiere,eleve_nom,eleve_prenom,rm_type_id,rm_type,rm_read FROM relations_messagerie
		LEFT JOIN users ON ('users'.'user_id'='relations_messagerie'.'rm_type_id' AND 'relations_messagerie'.'rm_type'='user')
		LEFT JOIN eleves ON ('eleves'.'eleve_id'='relations_messagerie'.'rm_type_id' AND 'relations_messagerie'.'rm_type'='eleve')
		WHERE rm_messages='".$discussion_id."'");
	$stmt->execute();
	return $stmt->fetchAll();
}
function discussionAdd($discussion){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("INSERT INTO messages (message_author,message_subject,message_content,message_date,message_parent_message,message_parent_reponse,message_author_type) VALUES (:message_author,:message_subject,:message_content,:message_date,:message_parent_message,:message_parent_reponse,:message_author_type)");
	$stmt->execute(
		array(':message_author' => $discussion['message_author'],
			':message_subject' => $discussion['message_subject'],
			':message_content' => nl2br($discussion['message_content']),
			':message_date' => $discussion['message_date'],
			':message_parent_message' => $discussion['message_parent_message'],
			':message_parent_reponse' => -1,
			':message_author_type'=>$discussion['message_author_type']
			)
		);
	return $pdo->lastInsertId('message_id'); 
}
function discussionDel($discussion_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM messages WHERE message_id='".$discussion_id."' OR message_parent_message='".$discussion_id."'");
	$stmt->execute();
}
function relationDiscussionDel($discussion_id,$rm_type_id,$rm_type){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("DELETE FROM relations_messagerie WHERE rm_messages='".$discussion_id."' AND rm_type_id='".$rm_type_id."' AND rm_type='".$rm_type."'");
	$stmt->execute();
}
function destinataireADD($destinataire,$discussion_id,$markAsRead=false){
	$pdo = sqliteConnect();
	$rm_read="";
	if($markAsRead==true){
		$rm_read=$discussion_id;
	}
	$stmt = $pdo->prepare("INSERT INTO relations_messagerie (rm_messages,rm_type,rm_type_id,rm_read) VALUES (:rm_messages,:rm_type,:rm_type_id,:rm_read)");
	$stmt->execute(
		array(':rm_messages' => $discussion_id,
			':rm_type' => $destinataire['destinataire_type'],
			':rm_type_id' => $destinataire['destinataire_id'],
			':rm_read' => $rm_read
			)
		);
}

function markAsRead($discussion_id,$rm_type,$rm_type_id,$rem_read_id){
	$pdo = sqliteConnect();
	$sql="UPDATE relations_messagerie  SET rm_read='".$rem_read_id."' WHERE rm_messages='" . $discussion_id . "' AND rm_type='".$rm_type."' AND rm_type_id='".$rm_type_id."'";
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
}
function getDestinatairesByEleve($eleve_id,$select="user_pseudo,user_matiere,user_id"){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT classe_destinataires FROM relations_eleves_classes
		INNER JOIN classes ON 'classes'.'classe_id'='relations_eleves_classes'.'rec_classe'		
		WHERE 'relations_eleves_classes'.'rec_eleve'='".$eleve_id."'");
	$stmt->execute();
	$destinatairesByClasses=$stmt->fetchAll();
	$destinataires_tab=[];
	foreach ($destinatairesByClasses as $destinataires_liste) {
		$destinataires_tab[]=$destinataires_liste['classe_destinataires'];
	}
	$destinataires=array_filter(explode(',', implode(',', $destinataires_tab)));
	$where=[];
	foreach ($destinataires as $destinataire) {
		array_push($where, "user_id='".$destinataire."'");
	}
	if(count($where)==0){
		return array();
	}
	$sql="SELECT $select FROM users	WHERE ".implode(" OR ", $where)."";
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	return $stmt->fetchAll();
}

function answerEnable($discussion_id,$rm_type,$rm_type_id){
	$pdo = sqliteConnect();
	$stmt = $pdo->prepare("SELECT rm_lock,rm_type_id FROM relations_messagerie
		WHERE rm_messages='".$discussion_id."' AND rm_type='$rm_type' AND rm_type_id='$rm_type_id' LIMIT 1");
	$stmt->execute();
	$result=$stmt->fetch();	
	if(count($result)==0){return false;}
	if($result['rm_lock']=="1"){return false;}
	return true;
}