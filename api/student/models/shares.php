<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function getSharesByEleve($eleve_id,$classes,$select=false){  
  $pdo = sqliteConnect();
  $where=array();
  foreach ($classes as $classe) {
   $where[]="share_with='".$classe['classe_id']."'";
 }
 if(!$select){
  $select="link_id,link_titre,link_favicon,link_url,link_date,file_id,file_fullname,file_type,file_date,file_size,share_date,share_item_type,share_user";  
}
if(count($classes)>0){
  $where="AND (".implode(' OR ',$where).")";
}else{
  $where="";
}

$sql="SELECT $select FROM shares 
  LEFT JOIN files ON ('files'.'file_id'='shares'.'share_item_id' AND 'shares'.'share_item_type'='file')
  LEFT JOIN links ON ('links'.'link_id'='shares'.'share_item_id' AND 'shares'.'share_item_type'='link') 
  WHERE (share_type='eleve' AND share_with='$eleve_id')
  OR (share_type='classe' ".$where.")";

$stmt = $pdo->prepare($sql);

$stmt->execute();
return $stmt->fetchAll();
}