<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function getElevesByClasseId($classe_id,$select="*") {
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT $select FROM relations_eleves_classes 
    INNER JOIN eleves
    ON 'eleves'.'eleve_id' = 'relations_eleves_classes'.'rec_eleve'
    WHERE rec_classe=$classe_id ORDER BY eleve_nom ASC");
  $stmt->execute();
  return $stmt->fetchAll();
}
function getAllEleves($select="*") {
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT $select FROM eleves");
  $stmt->execute();
  return $stmt->fetchAll();
}
function updateEleve($eleve) {
  $pdo = sqliteConnect();
  $updates = array();
  foreach ($eleve as $key => $value) {
    if ($key != 'eleve_id') {
      $updates[] = "" . $key . "=" . $pdo->quote($value) . "";
    }
  }
  $updateStr = implode(",", $updates);
  $sql="UPDATE eleves 
  SET " . $updateStr . "         
  WHERE eleve_id='" . $eleve['eleve_id'] . "'";
  
  $stmt = $pdo->prepare($sql);
  $stmt->execute();
}
function getEleveByToken($token){
  global $_;
  $dir=strtolower(substr($_['nom_etablissement'],0,1));
  if(!is_dir(_DATA_.'schools/'.$dir.'/'.$_['nom_etablissement']) || trim($_['nom_etablissement'])==""){
    $_SESSION['render']['info'][]=array("L'établissement et le code ne correspondent pas.","error");
    MainControl::init('render');
    return false;
  }
  $pdo = sqliteConnect();
  $stmt = $pdo->prepare("SELECT * FROM eleves WHERE eleve_token='$token' LIMIT 1");
  $stmt->execute();
  $result=$stmt->fetch();
  if(!$result){
   $_SESSION['render']['info'][]=array("L'établissement et le code ne correspondent pas.","error");
   MainControl::init('render');
   exit;
 }
 return $result;
}