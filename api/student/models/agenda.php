<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function getEventsByEleve($eleve_id,$classes){
  global $_;
  $where=array();
  foreach ($classes as $classe) {
   $where[]="agenda.event_type_id='".$classe['classe_id']."'";
 }
 $pdo = sqliteConnect();
 $time=time();
 if(count($classes)>0){
  $where="AND (".implode(' OR ',$where).")";
}else{
  $where="";
}
 $sql="SELECT event_id,event_title,event_data,event_color,event_icon,event_type,event_type_id,event_start,event_end,event_allDay,event_user,event_state,event_date,event_periode,event_discipline FROM agenda 
 WHERE 'agenda'.'event_visibility'='true' AND (('agenda'.'event_type_id'='$eleve_id' AND 'agenda'.'event_type'='eleve')
 OR ('agenda'.'event_type'='classe' ".$where."))";
 $stmt = $pdo->prepare($sql); 
 $stmt->execute();
 return $stmt->fetchAll();
}