<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

function logs($info) {
     global $_;
       $dir=strtolower(substr($_['nom_etablissement'],0,1));
    $log = fopen(_DATA_.'schools/'.$dir.'/'. $_['nom_etablissement'] . '/' . _LOG_, "a+");
    fputs($log, $info);
    fputs($log, "\n");
    fclose($log);
}
function sqliteConnect() {
    global $_;
    $dir=strtolower(substr($_['nom_etablissement'],0,1));
    try {
        $pdo = new PDO('sqlite:'._DATA_.'schools/'.$dir.'/'. $_['nom_etablissement'] . '/' . _DB_);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
        return $pdo;
    } catch (Exception $e) {
        echo "Impossible d'accéder à la base de données SQLite : " . $e->getMessage();
        die();
    }
}
/**
 * Check if a table exists in the current database.
 *
 * @param PDO $pdo PDO instance connected to a database.
 * @param string $table Table to search for.
 * @return bool TRUE if table exists, FALSE if no table found.
 */
function tableExists($table,$entree) {
  $pdo = sqliteConnect();
    // Try a select statement against the table
    // Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
  try {
    $result = $pdo->query("SELECT 1 FROM $table WHERE $entree='1' LIMIT 1");
} catch (Exception $e) {
        // We got an exception == table not found
    return FALSE;
}
    // Result is either boolean FALSE (no table found) or PDOStatement Object (table found)
return $result !== FALSE;
}
function tableStructureUPD($table){
    global ${"shema_".$table};
    $pdo = sqliteConnect();
    $n=0;
    foreach (${"shema_".$table} as $key=>$column) {     
        if(!tableExists($table,$key)){
            if($n==0){
                $stmt = $pdo->prepare("CREATE TABLE IF NOT EXISTS $table (".$column.")");
                $stmt->execute(); 
            }else{
                $stmt = $pdo->prepare("ALTER TABLE $table ADD COLUMN ".$column."");
                $stmt->execute(); 
            }
        }
        $n++;
    }
}
function renameColumn($table,$newColumn,$oldColumn){
    global ${"shema_".$table};
    $pdo = sqliteConnect();
    $n=0;
    $oldColumns=[];
    $newColumns=[];
    foreach (${"shema_".$table} as $key=>$column) {  
        if($key==$newColumn){
            array_push($oldColumns,$oldColumn);
            array_push($newColumns,$key);
        }else{
          array_push($newColumns,$key);
          array_push($oldColumns,$key);  
      }   
      if(!tableExists("temp_".$table."",$key)){
        if($n==0){
            $stmt = $pdo->prepare("CREATE TABLE IF NOT EXISTS temp_".$table." (".$column.")");
            $stmt->execute(); 
        }else{
            $stmt = $pdo->prepare("ALTER TABLE 'temp_".$table."' ADD COLUMN ".$column."");
            $stmt->execute(); 
        }
    }
    $n++;
}
$sql="INSERT INTO temp_".$table." (".implode(",",$newColumns).") SELECT ".implode(",",$oldColumns)." FROM ".$table."";
$stmt = $pdo->prepare($sql);
$stmt->execute(); 
$stmt = $pdo->prepare("DROP TABLE $table");
$stmt->execute(); 
$stmt = $pdo->prepare("ALTER TABLE 'temp_".$table."' RENAME TO '".$table."'");
$stmt->execute(); 
}
function dropColumn($table,$columnToDel,$insert=true){
  global ${"shema_".$table};
  $pdo = sqliteConnect();
  $n=0;
  $columns=[];
  foreach (${"shema_".$table} as $key=>$column) {  
    if($column==$columnToDel){continue;}
    array_push($columns,$key);
    if(!tableExists("temp_".$table."",$key)){
        if($n==0){
            $stmt = $pdo->prepare("CREATE TABLE IF NOT EXISTS temp_".$table." (".$column.")");
            $stmt->execute(); 
        }else{
            $stmt = $pdo->prepare("ALTER TABLE 'temp_".$table."' ADD COLUMN ".$column."");
            $stmt->execute(); 
        }
    }
    $n++;
}
if($insert==true){
    $sql="INSERT INTO 'temp_".$table."' (".implode(",",$columns).") SELECT ".implode(",",$columns)." FROM ".$table."";
    echo $sql;
    $stmt = $pdo->prepare($sql);
    $stmt->execute(); 
}
$stmt = $pdo->prepare("DROP TABLE $table");
$stmt->execute(); 
$stmt = $pdo->prepare("ALTER TABLE 'temp_".$table."' RENAME TO '".$table."'");
$stmt->execute(); 
}
function genereRandomStringNb($nb) {
    $tpass = array();
    $id = 0;
    for ($i = 65; $i < 90; $i++) {
        $tpass[$id++] = chr($i);
    }
    $passwd = "";
    for ($i = 0; $i < $nb; $i++) {
        $passwd.=$tpass[rand(0, $id - 1)];
    }
    return $passwd;
}
function getEtablissements(){
    $dossier_url=_DATA_."schools";
    $etablissements=array();
    if($dossier = opendir($dossier_url)){
        while(false !== ($alpha = readdir($dossier)))
        {
            if(is_dir($dossier_url.'/'.$alpha) AND $alpha!="." AND $alpha!=".."){
               // array_push($etablissements, $fichier);


             if($sous_dossier = opendir($dossier_url.'/'.$alpha)){


                $k=0;
                while(false !== ($fichier = readdir($sous_dossier)))
                {
                    if(is_dir($dossier_url.'/'.$alpha.'/'.$fichier) AND $fichier!="." AND $fichier!=".." AND $fichier!="files"){

                        array_push($etablissements, $fichier);
                        $k++;
                    }
                }
                if($k==0){
                    array_push($etablissements, $alpha);
                }



            }


        }
    }
}
return $etablissements;
}
function updateEtablissementPassword($new_password){
    global $_;
    $dir=strtolower(substr($_['nom_etablissement'],0,1));
    $file=_DATA_."schools/". $dir.'/'.$_['nom_etablissement']."/pwd.php";
    $passwordHash=password_hash($new_password, PASSWORD_DEFAULT);
    $password=$passwordHash;
    file_put_contents($file, "<?php \$password='$passwordHash'; ?>");
}
function utf8_to_octal($string){
    return $string;
    // $search1[]="\242"; $search2[]="#\&cent\;#";
    // $search1[]="\243"; $search2[]="#\&pound\;#";
    // $search1[]="\244"; $search2[]="#\&euro\;#";
    // $search1[]="\245"; $search2[]="#\&yen\;#";
    // $search1[]="\260"; $search2[]="#\&deg\;#";
    // $search1[]="\274"; $search2[]="#\&OElig\;#";
    // $search1[]="\275"; $search2[]="#\&oelig\;#";
    // $search1[]="\276"; $search2[]="#\&Yuml\;#";
    // $search1[]="\241"; $search2[]="#\&iexcl\;#";
    // $search1[]="\253"; $search2[]="#\&laquo\;#";
    // $search1[]="\273"; $search2[]="#\&raquo\;#";
    // $search1[]="\277"; $search2[]="#\&iquest\;#";
    // $search1[]="\300"; $search2[]="#\&Agrave\;#";
    // $search1[]="\301"; $search2[]="#\&Aacute\;#";
    // $search1[]="\302"; $search2[]="#\&Acirc\;#";
    // $search1[]="\303"; $search2[]="#\&Atilde\;#";
    // $search1[]="\304"; $search2[]="#\&Auml\;#";
    // $search1[]="\305"; $search2[]="#\&Aring\;#";
    // $search1[]="\306"; $search2[]="#\&AElig\;#";
    // $search1[]="\307"; $search2[]="#\&Ccedil\;#";
    // $search1[]="\310"; $search2[]="#\&Egrave\;#";
    // $search1[]="\311"; $search2[]="#\&Eacute\;#";
    // $search1[]="\312"; $search2[]="#\&Ecirc\;#";
    // $search1[]="\313"; $search2[]="#\&Euml\;#";
    // $search1[]="\314"; $search2[]="#\&Igrave\;#";
    // $search1[]="\315"; $search2[]="#\&Iacute\;#";
    // $search1[]="\316"; $search2[]="#\&Icirc\;#";
    // $search1[]="\317"; $search2[]="#\&Iuml\;#";
    // $search1[]="\320"; $search2[]="#\&ETH\;#";
    // $search1[]="\321"; $search2[]="#\&Ntilde\;#";
    // $search1[]="\322"; $search2[]="#\&Ograve\;#";
    // $search1[]="\323"; $search2[]="#\&Oacute\;#";
    // $search1[]="\324"; $search2[]="#\&Ocirc\;#";
    // $search1[]="\325"; $search2[]="#\&Otilde\;#";
    // $search1[]="\326"; $search2[]="#\&Ouml\;#";
    // $search1[]="\330"; $search2[]="#\&Oslash\;#";
    // $search1[]="\331"; $search2[]="#\&Ugrave\;#";
    // $search1[]="\332"; $search2[]="#\&Uacute\;#";
    // $search1[]="\333"; $search2[]="#\&Ucirc\;#";
    // $search1[]="\334"; $search2[]="#\&Uuml\;#";
    // $search1[]="\335"; $search2[]="#\&Yacute\;#";
    // $search1[]="\336"; $search2[]="#\&THORN\;#";
    // $search1[]="\337"; $search2[]="#\&szlig\;#";
    // $search1[]="\340"; $search2[]="#\&agrave\;#";
    // $search1[]="\341"; $search2[]="#\&aacute\;#";
    // $search1[]="\342"; $search2[]="#\&acirc\;#";
    // $search1[]="\343"; $search2[]="#\&atilde\;#";
    // $search1[]="\344"; $search2[]="#\&auml\;#";
    // $search1[]="\345"; $search2[]="#\&aring\;#";
    // $search1[]="\346"; $search2[]="#\&aelig\;#";
    // $search1[]="\347"; $search2[]="#\&ccedil\;#";
    // $search1[]="\350"; $search2[]="#\&egrave\;#";
    // $search1[]="\351"; $search2[]="#\&eacute\;#";
    // $search1[]="\352"; $search2[]="#\&ecirc\;#";
    // $search1[]="\353"; $search2[]="#\&euml\;#";
    // $search1[]="\354"; $search2[]="#\&igrave\;#";
    // $search1[]="\355"; $search2[]="#\&iacute\;#";
    // $search1[]="\357"; $search2[]="#\&icirc\;#";
    // $search1[]="\356"; $search2[]="#\&iuml\;#";
    // $search1[]="\360"; $search2[]="#\&eth\;#";
    // $search1[]="\361"; $search2[]="#\&ntilde\;#";
    // $search1[]="\362"; $search2[]="#\&ograve\;#";
    // $search1[]="\363"; $search2[]="#\&oacute\;#";
    // $search1[]="\364"; $search2[]="#\&ocirc\;#";
    // $search1[]="\365"; $search2[]="#\&otilde\;#";
    // $search1[]="\366"; $search2[]="#\&ouml\;#";
    // $search1[]="\370"; $search2[]="#\&oslash\;#";
    // $search1[]="\371"; $search2[]="#\&ugrave\;#";
    // $search1[]="\372"; $search2[]="#\&uacute\;#";
    // $search1[]="\373"; $search2[]="#\&ucirc\;#";
    // $search1[]="\374"; $search2[]="#\&uuml\;#";
    // $search1[]="\375"; $search2[]="#\&yacute\;#";
    // $search1[]="\376"; $search2[]="#\&thorn\;#";
    // $search1[]="\376"; $search2[]="#\&yuml\;#";
    // $string = preg_replace($search2, $search1, $string);
    // return utf8_encode($string);
}
function sendmail($to,$subject='message',$msg,$from=null,$format='text/plain'){
    echo $format;
    $r="\r\n";$header='';
    $msg=wordwrap($msg, 70,$r);
    if ($format!='text/plain'){$msg=htmlspecialchars($msg);}
    if (!empty($from)){$header.='From: '.$from.$r;}
        $header='Content-Type: text/plain; charset="utf-8"'.$r.'Content-Transfer-Encoding: 8bit'.$r.$header;
        return mail($to,$subject,$msg,$header);
    }

    function moyenneWithCoefficients($notes){
        if(count($notes)==0){
          return "-";  
      }
      $sommeNotes=0;
      $sommeNotesMax=0;
      foreach ($notes as $note) {
          if(is_numeric($note['value'])){
            $sommeNotes+=($note['value']+$note['bonus'])*$note['coefficient'];
            $sommeNotesMax+=$note['noteMax']*$note['coefficient'];
        }
    }
    if($sommeNotesMax!=0){
      return round($sommeNotes*20/$sommeNotesMax,2);  
  }
  return "-";
}
// Returns a file size limit in bytes based on the PHP upload_max_filesize
// and post_max_size
function file_upload_max_size() {
  static $max_size = -1;
  if ($max_size < 0) {
    // Start with post_max_size.
    $max_size = parse_size(ini_get('post_max_size'));
    // If upload_max_size is less, then reduce. Except if upload_max_size is
    // zero, which indicates no limit.
    $upload_max = parse_size(ini_get('upload_max_filesize'));
    if ($upload_max > 0 && $upload_max < $max_size) {
      $max_size = $upload_max;
  }
}
return $max_size;
}
function parse_size($size) {
  $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
  $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
  if ($unit) {
    // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
    return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
}
else {
    return round($size);
}
}
function getFilesSize($files){
    $size=0;
    foreach ($files as $file) {
        $size+=$file['file_size'];
    }
    return $size;
}
/**
* Returns human size
*/
function getSize($filesize, $precision = 2)
{
    $units = array('', 'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y');
    foreach ($units as $idUnit => $unit)
    {
        if ($filesize > 1024)
            $filesize /= 1024;
        else
            break;
    }        
    return round($filesize, $precision).' '.$units[$idUnit].'B';
}
function downloadFile($file){
    global $_;
    set_time_limit(0);
    $name = $file['file_name'];
    $dir=strtolower(substr($_['nom_etablissement'],0,1));
    $dir_path=_DATA_."schools/".$dir.'/'.$_['nom_etablissement']."/files/";
    $filename = $dir_path.$name;
    if (!is_file($filename) || !is_readable($filename)) {
        header("HTTP/1.1 404 Not Found");
        exit;
    }
    $size = filesize($filename);
    if (ini_get("zlib.output_compression")) {
        ini_set("zlib.output_compression", "Off");
    } 
    session_write_close();
    header("Cache-Control: no-cache, must-revalidate");
    header("Cache-Control: post-check=0,pre-check=0");
    header("Cache-Control: max-age=0");
    header("Pragma: no-cache");
    header("Expires: 0");
    header("Content-Type: application/force-download");
    header('Content-Disposition: attachment; filename="'.$file['file_fullname'].'"');
 // on indique au client la prise
// en charge de l'envoi de données
// par portion.
    header("Accept-Ranges: bytes");
 // par défaut, on commence au début du fichier
    $start = 0;
 // par défaut, on termine à la fin du fichier (envoi complet)
    $end = $size - 1;
    if (isset($_SERVER["HTTP_RANGE"])) {
    // l'entête doit être dans un format valide
        if (!preg_match("#bytes=([0-9]+)?-([0-9]+)?(/[0-9]+)?#i", $_SERVER['HTTP_RANGE'], $m)) {
            header("HTTP/1.1 416 Requested Range Not Satisfiable");
            exit;
        }
     // modification de $start et $end
    // et on vérifie leur validité
        $start = !empty($m[1])?(int)$m[1]:null;
        $end = !empty($m[2])?(int)$m[2]:$end;
        if (!$start && !$end || $end !== null && $end >= $size
            || $end && $start && $end < $start) {
            header("HTTP/1.1 416 Requested Range Not Satisfiable");
            exit;
        } 
    // si $start n'est pas spécifié,
    // on commence à $size - $end
        if ($start === null) {
            $start = $size - $end;
            $end -= 1;
        }
     // indique l'envoi d'un contenu partiel
        header("HTTP/1.1 206 Partial Content");
     // décrit quelle plage de données est envoyée
        header("Content-Range: ".$start."-".$end."/".$size);
    } 
// on indique bien la taille des données envoyées
    header("Content-Length: ".($end-$start+1));
 // ouverture du fichier en lecture et en mode binaire
    $f = fopen($filename, "rb");
 // on se positionne au bon endroit ($start)
    fseek($f, $start);
 // cette variable sert à connaître le nombre
// d'octet envoyé.
    $remainingSize = $end-$start+1;
 // calcul la taille des lots de données
// je choisi 4ko ou $remainingSize si plus
// petit que 4ko.
    $length = $remainingSize < 4096?$remainingSize:4096;
    while (false !== $datas = fread($f, $length)) {
    // envoie des données vers le client
        echo $datas;
     // on a envoyé $length octets,
    // on le soustrait alors du
    // nombre d'octets restant.
        $remainingSize -= $length;
     // si tout est envoyé, on quitte
    // la boucle.
        if ($remainingSize <= 0) {
            break;
        } 
    // si reste moins de $length octets
    // à envoyer, on le rédefinit en conséquence.
        if ($remainingSize < $length) {
            $length = $remainingSize;
        }
    }
    fclose($f);
}

function recurse_copy($src,$dst) { 
    $dir = opendir($src); 
    @mkdir($dst); 
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                recurse_copy($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
                copy($src . '/' . $file,$dst . '/' . $file); 
            } 
        } 
    } 
    closedir($dir); 
} 
// removes files and non-empty directories
function rrmdir($dir) {
  if (is_dir($dir)) {
    $files = scandir($dir);
    foreach ($files as $file)
        if ($file != "." && $file != "..") rrmdir("$dir/$file");
    rmdir($dir);
}
else if (file_exists($dir)) unlink($dir);
} 


function array_values_recursive($array)
{
    $arrayValues = array();

    foreach ($array as $value)
    {
        if (is_scalar($value) OR is_resource($value))
        {
             $arrayValues[] = $value;
        }
        elseif (is_array($value))
        {
             $arrayValues = array_merge($arrayValues, array_values_recursive($value));
        }
    }

    return $arrayValues;
}