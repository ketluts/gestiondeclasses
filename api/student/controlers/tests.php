<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Tests {
             /**       
     *
     * @var Instance
     */
             private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return School
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
    return self::$_instance;
}



public function getAll(){
   global $_;
   $eleve=getEleveByToken($_['token']);   
   $notes=getNotesByEleve($eleve['eleve_id'],"classe_nom,note_value, note_bonus, note_eleve, note_date, controle_note, controle_coefficient, controle_categorie,controle_date,controle_id, controle_periode, controle_user, controle_discipline, controle_titre");
for ($i=0; $i <count($notes) ; $i++) { 
  $note=$notes[$i];
  if(is_numeric($notes[$i]['note_value'])){
    $notes[$i]['note_value']+= $notes[$i]['note_bonus'];
  }
  $notes[$i]['note_bonus']=0;






  // if($note['user_matiere']!=null){
  //   $notes[$i]['user_pseudo']=$note['user_matiere'];        
  // }







}
$_SESSION['render']['public']['notes']=$notes;
}
}
?>