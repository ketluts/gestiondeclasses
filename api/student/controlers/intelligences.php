<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Intelligences {
             /**       
     *
     * @var Instance
     */
             private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return School
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
    return self::$_instance;
}



public function get(){
   global $_;
   $eleve=getEleveByToken($_['token']);   
   $classes=getClassesByEleve($eleve['eleve_id']); 
   $testIntelligences=false;
   for ($i=0; $i < count($classes); $i++) { 
    $classes[$i]['eleves']=getElevesByClasseId($classes[$i]['classe_id'],'eleve_nom,eleve_prenom,eleve_id');
    if($classes[$i]['classe_intelligences']=="true"){
      $testIntelligences=true;
    }
  }
  $_SESSION['render']['public']['testIntelligences']=$testIntelligences;
}
public function updateIntelligences(){
  global $_;
  $eleve=getEleveByToken($_['token']);    
  $toUpdate['eleve_id']=$eleve['eleve_id'];
  $toUpdate['eleve_intelligences']=$_['resultats'];
  updateEleve($toUpdate);
}
}
?>