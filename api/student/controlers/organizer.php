<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Organizer {
             /**       
     *
     * @var Instance
     */
             private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return School
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
    return self::$_instance;
}



public function getAll(){
   global $_;
   $eleve=getEleveByToken($_['token']);   
   $classes=getClassesByEleve($eleve['eleve_id']); 
   $tempEvents=getEventsByEleve($eleve['eleve_id'],$classes);
   $events=array();
   $time=time();

$legends=array_values_recursive(getLegends('legend_event'));  



   foreach ($tempEvents as $event) {
      if(in_array($event['event_icon'],$legends)
         OR $event['event_icon']=="glyphicon-home"
         OR $event['event_icon']=="glyphicon-links"
         OR $event['event_icon']=="glyphicon-file"
         OR ($event['event_icon']=="glyphicon-blackboard" AND $event['event_start']<$time)
         OR $event['event_icon']=="glyphicon-comment"){
        array_push($events, $event);
  }
}
$_SESSION['render']['public']['agenda']=$events;
}
}
?>