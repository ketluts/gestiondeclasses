<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Sociogramme {
             /**       
     *
     * @var Instance
     */
             private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return School
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
    return self::$_instance;
}



public function getAll(){
   global $_;
   $eleve=getEleveByToken($_['token']);   
    $relations_temp=getRelationsByEleve($eleve['eleve_id'],"*");
  $relations=array();
  foreach ($relations_temp as $relation) {
    if($relation['relation_user']=="eleve_".$eleve['eleve_id']){
      $relations[]=$relation;
    }
  }
  $_SESSION['render']['public']['relations']=$relations;
}
 public function updateRelations(){
  global $_;
  $eleve=getEleveByToken($_['token']);    
  publicDelRelations($eleve['eleve_id']);
  for ($i=1; $i <= 3; $i++) { 
    $relation[$i]['from']=$eleve['eleve_id'];
    $relation[$i]['to']=$_['relation_'.$i];
    if($relation[$i]['to']==-1){
      continue;
    }
  if($_['relation_'.$i]!="false"){
    publicAddRelation($relation[$i],$i);
  }  
};
}
}
?>