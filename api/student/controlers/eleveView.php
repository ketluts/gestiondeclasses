<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class EleveView {
     /**
     *
     * @var Instance
     */
     private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}

    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}

    /**
     * 
     * @return Public
     *
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
      return self::$_instance;
    }
    public function get(){
      global $_;      
      $eleve=getEleveByToken($_['token']);
      $classes=getClassesByEleve($eleve['eleve_id']);
      $n=count($classes);
      for ($i=0; $i <$n ; $i++) { 
        $classe=$classes[$i];
        $classes[$i]['eleves']=getElevesByClasseId($classe['classe_id'],'eleve_id,eleve_nom,eleve_prenom');
      }

      $_SESSION['render']['public']['classes']=$classes;  
      MainControl::init('school','getConfig');
      MainControl::init('intelligences','get');  
      MainControl::init('sociogramme','getAll');
      MainControl::init('shares','getAll');
      MainControl::init('organizer','getAll');
      $etoiles=MainControl::init('defis','getAll');
      $eleve['nb_etoiles']=0;
      for ($i=0; $i <count($etoiles) ; $i++) { 
        $etoile=$etoiles[$i]; 
        $eleve['nb_etoiles']+=$etoile['etoile_value'];
      }
      MainControl::init('tests','getAll');
      MainControl::init('skills','getProgressions');
      $_SESSION['render']['public']['destinataires']=getDestinatairesByEleve($eleve['eleve_id']);
      $_SESSION['render']['public']['token']=$_['token'];
      $_SESSION['render']['public']['render']=true;
      $_SESSION['render']['public']['eleve_id']=$eleve['eleve_id'];
      $_SESSION['render']['public']['eleve']=$eleve;
    }
  }
  ?>