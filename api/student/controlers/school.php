<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class School {
             /**       
     *
     * @var Instance
     */
             private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return School
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
      return self::$_instance;
    }
    public function getAll(){
     global $_;
     $etablissements=getEtablissements();
     $etablissementsLST=array();
     foreach ($etablissements as $etablissement) {
      if(!in_array($etablissement, $etablissementsLST)){
        array_push($etablissementsLST, utf8_encode(utf8_decode($etablissement)));
      }
    }
    $_SESSION['render']['etablissementsLST']=$etablissementsLST;
  }
  public function getConfig(){
   global $_;
   $eleve=getEleveByToken($_['token']);
   $_SESSION['render']['legends']=getLegends();
   $_SESSION['render']['periodes']=getPeriodes();
   MainControl::init('users','getAll');    
   $_SESSION['render']['disciplines']=getDisciplines();
   $_SESSION['render']['options']['weeks']=getOption('weeks')['option_value'];
   $_SESSION['render']['options']['plugins']=getOption('plugins')['option_value'];
 }
}
?>