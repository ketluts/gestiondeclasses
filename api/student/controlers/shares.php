<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Shares {
             /**       
     *
     * @var Instance
     */
             private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return School
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
      return self::$_instance;
    }



    public function getAll(){
     global $_;
     $eleve=getEleveByToken($_['token']);   
     $classes=getClassesByEleve($eleve['eleve_id']); 
     $shares=getSharesByEleve($eleve['eleve_id'],$classes);
     $files=array();
     $filesId=array();
     $links=array();
     $linksId=array();
     for ($i=0; $i <count($shares) ; $i++) { 
      $share=$shares[$i];
      if($share['share_item_type']=="file"){
       if(!in_array($share['file_id'],$filesId)){
        $files[$i]=$share;
        $filesId[]=$share['file_id'];
      }
    }else{
     if(!in_array($share['link_id'],$linksId)){
      $links[$i]=$share;
      $linksId[]=$share['link_id'];
    }
  }
}
$_SESSION['render']['public']['files']=array_slice($files,0);
$_SESSION['render']['public']['links']=array_slice($links,0);
}
public function getFile(){
  global $_;
  $eleve=getEleveByToken($_['token']);
  if(!isset($_['file_id'])){
   header("HTTP/1.1 404 Not Found");
   exit;
 }
 $classes=getClassesByEleve($eleve['eleve_id']);
 $select="*";
 $shares=getSharesByEleve($eleve['eleve_id'],$classes,$select);
 foreach ($shares as $share) {
  if($share['share_item_id']==$_['file_id'] AND $share['share_item_type']=="file"){
    downloadFile($share);
    exit;
  }
}   
header("HTTP/1.1 404 Not Found");
exit;    
}
}
?>