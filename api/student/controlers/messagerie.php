<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Messagerie {
             /**       
     *
     * @var Instance
     */
             private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return School
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
    return self::$_instance;
}

 public function addAnswer(){
    global $_;
    $eleve=getEleveByToken($_['token']);   
    $classes=getClassesByEleve($eleve['eleve_id']); 
    if(!isset($_['message_parent_message'])){
      MainControl::init('render');
      exit;
    }
    if(!answerEnable($_['message_parent_message'],'eleve',$eleve['eleve_id'])){
      MainControl::init('render');
      exit;
    }
    if(!in_array($_['message_parent_message'],getMessagesByEleve($eleve['eleve_id'],'rm_messages',0))){
      //return; //TODO
    }
    $discussion['message_subject']="";
    $discussion['message_content']=$_['message_content'];
    $discussion['message_parent_message']=$_['message_parent_message'];
    $discussion['message_author']=$eleve['eleve_id'];
    $discussion['message_author_type']='eleve';
    $discussion['message_date']=$_['time'];
    $discussion_id=discussionAdd($discussion);
    MainControl::init('messagerie','getDiscussion');
  }
  public function addDiscussion() {
    global $_;
    $eleve=getEleveByToken($_['token']);   
    $classes=getClassesByEleve($eleve['eleve_id']); 
    if(!isset($_['message_destinataires'])){
     MainControl::init('render');        
     exit;
   }
   $destinataires=json_decode($_POST['message_destinataires'],true);
   $eleve_destinataires=getDestinatairesByEleve($eleve['eleve_id'],'user_id');
   $eleve_destinataires_tab=[];
   foreach ($eleve_destinataires as $value) {
    $eleve_destinataires_tab[]=$value['user_id'];
  }
  foreach ($destinataires as $destinataire) {
    if(!in_array($destinataire['destinataire_id'], $eleve_destinataires_tab)){
      MainControl::init('render');        
      exit;
    }
  }
  $discussion['message_subject']=$_['message_subject'];
  $discussion['message_content']=$_['message_content'];
  $discussion['message_author']=$eleve['eleve_id'];
  $discussion['message_author_type']='eleve';
  $discussion['message_date']=$_['time'];
  $discussion['message_parent_message']="-1";
  $discussion_id=discussionAdd($discussion);
  foreach ($destinataires as $destinataire) {
    if($destinataire['destinataire_type']=='user' || $destinataire['destinataire_type']=='eleve'){
      destinataireADD($destinataire,$discussion_id);    
    }
    elseif ($destinataire['destinataire_type']=='classe') {
      $eleves=getEleves($destinataire['destinataire_id'],'eleve_id');
      foreach ($eleves as $eleve_tab) {
        destinataireADD(["destinataire_id"=>$eleve_tab['eleve_id'],"destinataire_type"=>"eleve"],$discussion_id);
      }
    }
    elseif ($destinataire['destinataire_type']=='all_users') {
      $users=getUsers();
      foreach ($users as $user_tab) {
        destinataireADD(["destinataire_id"=>$user_tab['user_id'],"destinataire_type"=>"user"],$discussion_id);
      }
    }
  }
  destinataireADD(["destinataire_id"=>$eleve['eleve_id'],"destinataire_type"=>"eleve"],$discussion_id,true);
  MainControl::init('messagerie','getDiscussion');
}
public function deleteDiscussion(){
 global $_;      
 $eleve=getEleveByToken($_['token']);   
 relationDiscussionDel($_['message_id'],$eleve['eleve_id'],'eleve');
 $participants=getParticipantsByDiscussion($_['message_id']);
 if(count($participants)==0){
   discussionDel($_['message_id']);   
 }else{
  $discussion['message_subject']="";
  $discussion['message_content']=$eleve['eleve_prenom']." ".$eleve['eleve_nom']." a quitté la discussion.";
  $discussion['message_parent_message']=$_['message_id'];
  $discussion['message_author']=$eleve['eleve_id'];
  $discussion['message_date']=$_['time'];
  $discussion['message_author_type']='eleve';
  $discussion_id=discussionAdd($discussion);
}
}
public function getDiscussion(){
  global $_;      
  $eleve=getEleveByToken($_['token']);   
  $select="message_id,message_author,message_subject,message_content,message_author_type,message_date,message_parent_message,rm_messages,rm_lock,rm_type,rm_read,rm_type_id,user_pseudo,user_matiere,eleve_nom,eleve_prenom";
  $messages=getMessagesByEleve($eleve['eleve_id'],$select,$_['syncId']);
  for ($i=0; $i < count($messages); $i++) { 
    $message=$messages[$i];
    $messages[$i]['state']=true;
    if($message['message_author_type']!="eleve" || $message['message_author']!=$eleve['eleve_id']){
     $messages[$i]['state']=$message["rm_read"];
   }
   if($message['message_parent_message']=="-1"){
    $messages[$i]['participants']=getParticipantsByDiscussion($message['message_id']);
  }
}
$_SESSION['render']['public']['messages']=$messages;
if(isset($_['render'])){
  MainControl::init('render');        
  exit;
}
}
public function updateDiscussion(){
 global $_;
 $eleve=getEleveByToken($_['token']);   
 if(isset($_['discussion_read'])){
  markAsRead($_['discussion_id'],'eleve',$eleve['eleve_id'],$_['discussion_read']);
}
MainControl::init('messagerie','getDiscussion');
}
}
?>