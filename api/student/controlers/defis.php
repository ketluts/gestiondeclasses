<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Defis {
     /**
     *
     * @var Instance
     */
     private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return Defis
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
        return self::$_instance;
      }
      public function getAll(){
   global $_;
   $eleve=getEleveByToken($_['token']);   
  $etoiles=getEtoilesByEleve($eleve['eleve_id'],"etoile_value, etoile_date, etoile_state,etoile_type, etoile_eleve, etoile_defi, defi_etoiles, defi_icon, defi_id, defi_titre, defi_description, defi_user, defi_url,controle_id");
$eleve['nb_etoiles']=0;
for ($i=0; $i <count($etoiles) ; $i++) { 
  $etoile=$etoiles[$i];
}
$_SESSION['render']['public']['etoiles']=$etoiles;
return $etoiles;
}
      public function open(){
        global $_;
        MainControl::init('defis','updateFlux');
        header( 'Location: '.$_['url'].'' ) ;    
      }
      public function updateFlux(){
       global $_;
       if(!isset($_['defi_id']) AND !isset($_['eleve_id'])){
         MainControl::init('render');        
         exit;
       }
       $defi=getDefiById($_['defi_id']);
       if($defi==null){
         MainControl::init('render');        
         exit;
       }
       $eleve=getEleveByToken($_['token']);   
       if($eleve==false){
         MainControl::init('render');        
         exit;
       }
       $classes=getClassesByEleve($eleve['eleve_id']);
       $classes_nom=array();
       foreach ($classes as $classe) {
        $classes_nom[]=$classe['classe_nom'];
      }
      $flux=$defi['defi_flux'];
      if($flux==null){
        $array=array(
          'infos'=>array(
            'type'=>'rss',
            'description'=>'Liste des élèves qui ont participé au défi "'.$defi['defi_titre'].'"',
            'title'=>ucfirst($defi['defi_titre']),
            'link'=>$defi['defi_url'],
          ),
          'items'=>array(
            0=>array(
              'description'=>'',
              'title'=>ucfirst($eleve['eleve_prenom']).' '.strtoupper($eleve['eleve_nom']).' ('.implode(' - ', $classes_nom).') a accédé au défi "'.$defi['defi_titre'].'".',
              'link'=>$defi['defi_url'],
              'guid'=>time(),
                'pubDate'=>@date('r'),// Be carefull, the rss pubDate format is specific ! RFC 2822 (see http://www.faqs.org/rfcs/rfc2822.html)
              )
          )
        );
      }
      else{
        $array=json_decode($flux,true);
        $newItem=array(
          'description'=>'',
          'title'=>ucfirst($eleve['eleve_prenom']).' '.strtoupper($eleve['eleve_nom']).' ('.implode(' - ', $classes_nom).') a accédé au défi "'.$defi['defi_titre'].'".',
          'link'=>$defi['defi_url'],
          'guid'=>time(),
                'pubDate'=>@date('r'),// Be carefull, the rss pubDate format is specific ! RFC 2822 (see http://www.faqs.org/rfcs/rfc2822.html)
              );
        array_unshift($array['items'],$newItem);
      }
      $update['defi_flux']=json_encode($array);
      $update['defi_id']=$defi['defi_id'];
      updateDefi($update);
    }

  }
  ?>