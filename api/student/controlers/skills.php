<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

class Skills {
             /**       
     *
     * @var Instance
     */
             private static $_instance;

    /**
     * Empêche la création externe d'instances.
     */
    private function __construct() {}
    /**
     * Empêche la copie externe de l'instance.
     */
    private function __clone() {}
    /**
     * 
     * @return School
     */
    public static function getInstance() {
      if (!(self::$_instance instanceof self))
        self::$_instance = new self();
    return self::$_instance;
}



public function addProgression(){
  global $_;
  $eleve=getEleveByToken($_['token']);  
  if(!checkAutoEvaluation($eleve['eleve_id'],$_['item_id'])){return;}
  $new_progression=[];
  $new_progression['rei_eleve']=$eleve['eleve_id'];
  $new_progression['rei_user']=$eleve['eleve_id'];
  $new_progression['rei_item']=$_['item_id'];
  $new_progression['rei_value']=$_['progression'];   
  $new_progression['rei_value']=$_['progression'];         
  $new_progression['rei_user_type']="eleve";
  $new_progression['rei_date']=$_['time'];
  $new_progression['rei_activite']="";         
  addProgression($new_progression,$eleve['eleve_id'],array($_['item_id']),array($eleve['eleve_id']));
  MainControl::init('skills','getProgressions');
}
public function getProgressions(){
  global $_;
  $eleve=getEleveByToken($_['token']);  
  $select="item_id,item_tags,item_name,item_categorie,item_sous_categorie,item_value_max,item_user,item_color,item_colors,item_mode,item_descriptions,rei_eleve,rei_value,rei_comment,rei_date,rei_user_type,rei_user,rei_id,rei_activite,item_vue,rei_symbole";
  $progressions=getProgressionsByEleve($eleve['eleve_id'],$select);
  for ($i=0; $i <count($progressions) ; $i++) { 
    $progression=$progressions[$i];  

  }
  $_SESSION['render']['public']['progressions']=$progressions;
}
public function deleteProgression(){
  // global $_;
  // $eleve=getEleveByToken($_['token']);  
  // $progression=getProgressionById($_['progression_id'],"rei_user,rei_user_type");
  //     if($progression['rei_user']!=$eleve['eleve_id'] || $progression['rei_user_type']!="eleve"){return;}
  // delProgression($_['progression_id']);     
  // MainControl::init('skills','getProgressions');
}

public function uncheck(){
  global $_;
  $eleve=getEleveByToken($_['token']);  
  publicUncheckItem($_['item_id'],$eleve['eleve_id']);     
  MainControl::init('eleveView','getProgressions');
}
}
?>