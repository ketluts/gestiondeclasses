<?php
// Copyright 2018 Pierre GIRARDOT

// This file is part of GestionDeClasses.

// GestionDeClasses is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version GPL-3.0-or-later of the License.

// GestionDeClasses is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.

session_start();
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Headers: X-Requested-With');

ini_set('memory_limit', '-1');
date_default_timezone_set('UTC');
setlocale(LC_TIME, 'fr_FR');

set_time_limit(300);

$_=array();
foreach($_POST as $key=>$val){
	if(!is_array($val)){
		$_[$key]=trim($val);
	}
}
foreach($_GET as $key=>$val){
	$_[$key]=trim($val);
}

if(isset($_['nom_etablissement'])){
	$_['nom_etablissement']=str_replace("/", "_", $_['nom_etablissement']);
}

define("_DB_", "db.sqlite");
define("_LOG_", "debug.log");

require_once 'config.php';

$_SESSION['render']=[];
$_SESSION['render']['info']=[];
$_SESSION['render']['statut']=true;



require_once 'models/function.php';
require_once 'models/eleves.php';
require_once 'models/classes.php';
require_once 'models/relations.php';
require_once 'models/etoiles.php';
require_once 'models/notes.php';
require_once 'models/periodes.php';
require_once 'models/disciplines.php';
require_once 'models/legends.php';
require_once 'models/options.php';
require_once 'models/shares.php';
require_once 'models/defis.php';
require_once 'models/messagerie.php';
require_once 'models/items.php';
require_once 'models/agenda.php';
require_once 'models/users.php';
require_once 'mainControl.php';

require_once 'lib/is_email.php';

require_once("lib/vendor/autoload.php");