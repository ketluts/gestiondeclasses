/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.publicItemToggleMode=function(mode){
	//Affichage des boutons de mode
	$('.competences-view').removeClass('btn-default').removeClass('btn-primary');
	app.itemsView=mode;
	if(app.itemsView=='view'){
		$('#btn-competences-view').addClass('btn-primary');
		$('#btn-competences-edit').addClass('btn-default');
	}else{
		$('#btn-competences-edit').addClass('btn-primary');
		$('#btn-competences-view').addClass('btn-default');
		$("#acquisition-slider" ).slider( "values", [ 0, 100 ] );
	}	
	app.publicProgressionsRender();
	app.publicItemSetFilters();
}
app.publicItemsRender=function(){
	itemsFiltersUsers=[];
	app.graphDataCategories=[];

	document.getElementById('itemsNothingToShow').style.display='none';	
	$(".eleve_competences").css('display','block');	
	//On affiche par categorie
	var html=[];
	for (var i =0, lng=app.itemsCategories.length; i<lng; i++) {
		var categorie=app.itemsCategories[i];
		html.push('<div data-toggle="collapse" data-parent="#competences_liste" href="#items_categorie_list_'+i+'" class="h3 items-categorie" id="items_categorie_'+i+'">');
		html.push('<strong>');
		html.push(categorie['name']);
		html.push('</strong>');
		html.push('<span id="categorie_'+i+'_progression" class="categories_progression"></span>');
		html.push('</div>');
		html.push('<ul class="list-group collapse in" id="items_categorie_list_'+i+'">');
		//Début de la sous-catégorie
		for (var c =0, lllng=categorie['sous_categories'].length;c<lllng; c++) {
			var sous_categorie=categorie['sous_categories'][c];
			var visibility="";
			if(sous_categorie['name']=='Non classés'){
				visibility='hide';
			}
			html.push('<div data-toggle="collapse" data-parent="#items_categorie_'+i+'" href="#items_sous_categorie_list_'+i+'_'+c+'" class="h4 items-sous-categorie '+visibility+'" id="items_sous_categorie_'+i+'_'+c+'">');
			html.push('<strong>');
			html.push(sous_categorie['name']);
			html.push('</strong>');
			html.push('<span id="sous_categorie_'+c+'_progression" class="categories_progression" title="Accomplissement des élèves évalués."></span>');
			html.push('</div>');

			if(!app.graphDataCategories[i]){
				app.graphDataCategories[i]={
					values:[],
					sous_categories:[]
				};
			}										
			if(!app.graphDataCategories[i].sous_categories[c]){					
				app.graphDataCategories[i].sous_categories[c]={
					values:[],
					name:ucfirst(sous_categorie['name'])
				};
			}
			html.push('<ul class="list-group collapse in"  id="items_sous_categorie_list_'+i+'_'+c+'" >');
			for (var j =0, llng=sous_categorie['list'].length;j<llng; j++) {
				var item=sous_categorie['list'][llng-1-j];
				html.push('<li class="list-group-item  item-box"  id="item_'+item['item_id']+'_bloc"   style="border-color:'+item['item_color']+'">');
				html.push('<div class="flex-rows">');
				html.push('<span class="flex-2 item-title" onclick="app.viewToggle(\'item_'+item['item_id']+'_details_block\');">');
				html.push(item['item_name']);
				html.push('</span>');
				html.push('<span class="flex-2 text-center" id="item_'+item['item_id']+'" style="position:relative; overflow:hidden;">');
				html.push('<span class="items_autoeval_filter" id="item_'+item['item_id']+'_autoeval_filter" onclick="app.publicItemToggleMode(\'edit\');$(\'#competences_liste\').goTo();">');
				html.push('Donne ton avis d\'abord !');
				html.push('</span>');
				if(item['item_vue']=="cursor"){
					//Curseur
					//Génération de l'apercu des couleurs
					var max=item["item_value_max"];
					html.push('<div style="position:relative;">');
					for (var k = max; k>=0; k--) {
						var color=app.getColorAffine(k,item['item_colors']);
						var width=(k*1+1)*100/(max*1+1);
						html.push('<div class="slider-style item-background" style="background-color:'+color+';width:'+width+'%"></div>');
					}
					html.push('<div class="slider-style item-background" style="background-color:rgba(255,255,255,0.85);width:100%"></div>');			
					html.push('</div>');
					//####################################
					html.push('<span id="item_'+item['item_id']+'_results"></span>');
				}
				else{
					//Checkbox
					html.push('<input type="checkbox" class="check-box" id="item_'+item['item_id']+'_checkbox_mode" onclick="app.publicItemCheckboxChange('+item['item_id']+');"/>\
						<label for="item_'+item['item_id']+'_checkbox_mode" class="check-box"></label>');
				}
				html.push('</span>');
				html.push('<span class="flex-1">');
				html.push('<span id="item_'+item['item_id']+'_btn" ></span>');
				html.push('</span>');
				html.push('</div>');
				
				html.push('<div class="flex-rows" style="display:none;" id="item_'+item['item_id']+'_details_block">');
				html.push('<hr/>');
				html.push('<div class="flex-rows item_description" id="item_'+item['item_id']+'_current_description">');
				html.push('</div>');
				html.push('<div id="item_'+item['item_id']+'_details">');
				html.push('</div>');			
				html.push('</div>');
				html.push('<div id="item_'+item['item_id']+'_tags" class="item-tags">');
				if(item['item_tags']){
					html.push(item['item_tags']);
				}
				html.push('</div>');
				html.push('</div>');
				html.push('</li>');
			};  
			html.push('</ul>');
			//Fin de la sous-catégorie
		};
		html.push('</ul>');
		//Fin de la catégorie
	};
	if(html.length==0){
		html.push('<div class="well well-sm">Aucun item à afficher.</div>');
	}
	document.getElementById('competences_liste').innerHTML=html.join('');
	$('.items_autoeval_filter').css('display','none');
	
}
app.publicItemsProgressionSlideStyleRender=function(event,ui){
	var num=$(ui.handle).parent().attr('data-num');
	var item_id=$(ui.handle).parent().attr('data-id');
	var max=$(ui.handle).parent().attr('data-max');
	var color="";
	var descriptions=jsonParse(app.items[num]['item_descriptions']);
	var value=max;
	if(ui.value>=0){
		if(ui.value!=max){
			value=Math.floor(ui.value*1/(max/(max*1+1)));
		}	
		color=app.getColorAffine(value,app.items[num]['item_colors']);					
	}			
	$('#item_'+item_id+'_slider_style').css('width',Math.floor(ui.value*100/max)+"%");	
	$('#item_'+item_id+'_slider_style').css('background-color', color); 	
	if(descriptions[value]!="<p><br></p>"){
		$('#item_'+item_id+'_current_description').html("<span class='h4'><small>"+descriptions[value]+"</small></span>").css('display','bloc');
	}
}
app.publicProgressionsRender=function(){
	//Création du tableau contenant la progression de l'élève par item selon le mode
	var progressions=app.progressions;
	var eleve_items_values=[];
	$(".categories_progression").html('');
	for (var i = progressions.length - 1; i >= 0; i--) {
		var progression=progressions[i];
		if(!eleve_items_values[progression['item_id']]){
			eleve_items_values[progression['item_id']]=[];			
			eleve_items_values[progression['item_id']]['liste']=[];
			eleve_items_values[progression['item_id']]['views']=[];
		}	
		var item=app.getItemById(progression['item_id']);
		if(progression['rei_user_type']=='eleve'){
			item.item_auto_eval_filter=false;
		}
		//Si la valeur <0, cela signifie que l'item est visible pour l'élève et on en tient pas compte dans le calcul
		if(progression['rei_value']<0){
			eleve_items_values[progression['item_id']]['views'].push(progression['rei_value']);
		}else{
			if(app.itemsView=='view' && progression['rei_user_type']=='eleve'){
				continue;
			}
			if(app.itemsView=='edit' && progression['rei_user_type']=='user'){
				continue;
			}	
			eleve_items_values[progression['item_id']]['liste'].push(progression);
		}
	};
	//Render
	for (var c =0, llng=app.itemsCategories.length; c<llng; c++) {
		var categorie=app.itemsCategories[c];
		for (var cc =0, lllng=categorie['sous_categories'].length;cc<lllng; cc++) {
			var sous_categorie=categorie['sous_categories'][cc];
			for (var i =0, lng=sous_categorie['list'].length;i<lng; i++) {
				var item=sous_categorie['list'][i];
				//Affichage des résultats par item	
				if(app.itemsEditable.indexOf(""+item['item_id']+"")<0 && app.itemsView=='edit'){ continue;}
				item.is_visible=true;
				var progression=app.itemsStudentProgressionByItem(item['item_id']);
				var value=progression.value;
				app.graphDataCategories[c].values.push(progression.string);
				app.graphDataCategories[c].sous_categories[cc].values.push(
				{
					id:item.item_id,
					value:progression.string
				});	
				var is_disable=true;
				if(eleve_items_values[item['item_id']]['views'].indexOf('-2')>=0 && app.itemsView=='edit'){
					is_disable=false;
				}						
				var max=item['item_value_max'];
				$('#item_'+item['item_id']+'_current_description').html('').css('display','none');
				var html=[];
				if(item['item_vue']=="cursor"){
					//Curseur
					html.push('<div style="position:relative;">');
					html.push('<div id="item_'+item['item_id']+'_slider_style" class="slider-style"></div>');
					html.push('<div data-max="'+item['item_value_max']+'" data-num="'+i+'" data-id="'+item['item_id']+'" id="item_'+item['item_id']+'_slider" class="itemSlider"></div>');
					html.push('</div>');
					document.getElementById('item_'+item['item_id']+'_results').innerHTML=html.join('');
					var min=0;		
					var step=0.01;
					var slideValue=progression.slideValue;
					item['eleve_progression']=Math.max(value*100/max,0);
					var slide=$("#item_"+item['item_id']+"_slider");
					slide.slider({
						min:min,
						max:max,
						disabled:is_disable,
						step:step,
						value:slideValue
					});		
					slide.on( "slidechange",app.publicItemsProgressionAdd);
					slide.on( "slide",app.publicItemsProgressionSlideStyleRender);

					if(value>=0){

						$('#item_'+item['item_id']+'_slider_style').css('width',Math.floor(slideValue*100/max)+"%");
						if(value!=max){		
							value=Math.floor(value/(max/(max*1+1)));		
						}
						var color=app.getColorAffine(value,item['item_colors']);
						var descriptions=jsonParse(item['item_descriptions']);
						if(descriptions[value]!="<p><br></p>"){
							$('#item_'+item['item_id']+'_current_description').html("<span class='h4'><small>"+descriptions[value]+"</small></span>").css('display','block');
						}
						$('#item_'+item['item_id']+'_slider_style').css('background-color', color); 
					}
				}
				else{
					//Checkbox
					if(value==1){
						document.getElementById('item_'+item['item_id']+'_checkbox_mode').checked="checked";
						item['eleve_progression']=100;
					}else{
						document.getElementById('item_'+item['item_id']+'_checkbox_mode').checked="";
						item['eleve_progression']=0;
					}
				}
				var listeResultatsEvaluations=eleve_items_values[item['item_id']]['liste'];
				var n=listeResultatsEvaluations.length;
				document.getElementById('item_'+item['item_id']+'_btn').innerHTML=" <button class='btn btn-default btn-xs' onclick=\"app.viewToggle('item_"+item['item_id']+"_details_block');\">"+n+" évaluation"+app.pluralize(n,"s");+"</button>";
				//On affiche la liste des progressions
				var html=[];
				for (var j = 0; j <n; j++) {
					var progression=listeResultatsEvaluations[j];
					var pColor="";
					if(item['item_colors']){
						pColor=app.getColorAffine(progression['rei_value'],item['item_colors']);
					}
					else{
						pColor=app.colorByMoyenne((progression['rei_value']/max)*20);
					}
					if(item['item_vue']=="checkbox"){
						pColor="#34b93d";
					}
					var user="";
					if(progression['rei_user_type']=='user'){
						user=" par <strong>"+app.renderUserName(app.getUserById(progression.rei_user))+"</strong>";
					}
					else{
						user=" par <strong>"+app.eleve.eleve_prenom+' '+app.eleve.eleve_nom+"</strong>";
					}
					var activite="";
					if(progression['rei_activite']){
						activite=" "+progression['rei_activite']+" ";
					}
					var comment="";
					if(progression['rei_comment']){
						comment=" - <i> "+progression['rei_comment']+" </i>";
					}	

					progression.rei_symbole=progression.rei_symbole||"square";


					html.push('<div class="items-progression">\
						<span  class="items-'+progression.rei_symbole+'" style="color:'+pColor+';"></span> le '+moment(parseInt(progression['rei_date'])*1000).format('DD/MM/YY')+activite+comment+user+'');
					//if(progression['rei_user_type']=='eleve'){
						//html.push('<button class="btn btn-primary btn-xs pull-right" data-loading-text="..." onclick="$(this).button(\'loading\');app.publicDelProgression('+progression['rei_id']+','+progression['item_id']+');"><span class="glyphicon glyphicon-remove"></span></button>');
					//}				
					html.push('</div>');
				};
				if(n==0){
					html.push('Aucune évaluation');
				}
				if(item.item_auto_eval!=true || item.item_auto_eval_filter==false){
					document.getElementById('item_'+item['item_id']+'_details').innerHTML=html.join('');
				}				
			}
		}
	}
	//On désactive les slides si on est hors-ligne
	if (!app.checkConnection()) {
		$( ".itemSlider" ).slider( "disable" );
	}
}
app.itemsCategoriesProgressRender=function(){
	var categories_progression=[];
	var categorie_id=[];
	for (var i = app.items.length - 1; i >= 0; i--) {
		var item=app.items[i];
		if(!item.isOnGraph){
			continue;
		}
		var progression=app.itemsStudentProgressionByItem(item.item_id);
		if(progression.string=="-" || progression.string==null){continue;}
		var index=categorie_id.indexOf(item.item_categorie_num);
		if(index<0){
			categories_progression.push({
				id:item.item_categorie_num,
				liste:[progression.string]
			});
			categorie_id.push(item.item_categorie_num);					
		}
		else{
			categories_progression[index].liste.push(progression.string);	
		}	
	}
		//On affiche la progression de la catégorie
		for (var i = categories_progression.length- 1; i >= 0; i--) {
			var lng=categories_progression[i].liste.length;
			
			if(categories_progression[i].id!=undefined){
				var value=categories_progression[i].liste.reduce(app.add,0)/lng;
				document.getElementById('categorie_'+categories_progression[i].id+'_progression').innerHTML="<small title='"+Math.round(value*20)/100+"/20'> "+Math.round(value*100)/100+"%</small>";	
			}
		}
	}
	app.publicSkillsGraph=function(){
		var categories=[];
		var data=[];
		for (var i = app.graphDataCategories.length - 1; i >= 0; i--) {
			var categorie=app.graphDataCategories[i];
			for (var j = categorie.sous_categories.length - 1; j >= 0; j--) {
				var sous_categorie=categorie.sous_categories[j];
				if(!sous_categorie){continue;}
				categories.push(sous_categorie.name);
				var value=0;
				var n=0;
				var values=[];
				for (var k = sous_categorie.values.length - 1; k >= 0; k--) {
					var value=sous_categorie.values[k];
					var item=app.getItemById(value.id);

					if(item.isOnGraph && value.value!=="-"){	
						values.push(value.value);
						n++;
					}
				}
				if(n>0){
					value=Math.round(values.reduce(app.add,0)/n);
				}
				data.push(value);
			}
		}
		$('#competences_graph').highcharts({
			chart: {
				polar: true,
				type: 'line'
			},

			title: {
				text: '',
				style:"display:none"
			},

			pane: {
				size: '80%'
			},

			xAxis: {
				categories: categories,
				tickmarkPlacement: 'on',
				lineWidth: 0
			},

			yAxis: {
				gridLineInterpolation: 'polygon',
				lineWidth: 0,
				min: 0,
				max:100
			},
			series: [{
				name: 'Mes acquis',
				data: data,
				pointPlacement: 'on'
			}
			]
		});
	}