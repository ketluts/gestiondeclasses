/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.publicItemsProgressionAdd=function(event,ui,confirm){	
	if (!app.checkConnection()) {return;}
	
	if(!confirm){
		app.alert({title:'Je confirme ! ',type:'confirm'},function(){app.publicItemsProgressionAdd(event,ui,true);},function(){app.publicProgressionsRender();});
		return;
	}
	//On met à jour la progression de l'élève pour cet item
	var item_id=$(ui.handle).parent().attr('data-id');
	var max=$(ui.handle).parent().attr('data-max');
	if(ui.value!=max){
		ui.value=Math.floor(ui.value*1/(max/(max*1+1)));
	}	
	$.post(app.serveur + "index.php?go=skills&q=addProgression",
	{
		eleve_id:app.eleve_id,
		nom_etablissement:app.nom_etablissement,
		token:app.token,
		symbole:'square',
		item_id:item_id,
		progression:ui.value,
		time:Math.floor(app.myTime()/1000)
	}, function(data) {
		app.render(data);
		app.publicProgressionsRender();
	}
	);
}
app.publicItemCheckboxChange=function(item_id){
	if(!app.checkConnection()){
		return false;
	}
	if(document.getElementById('item_'+item_id+'_checkbox_mode').checked){		
		$.post(app.serveur + "index.php?go=skills&q=addProgression"+app.connexionParam,
		{			
			eleves:JSON.stringify([app.eleve_id]),
			nom_etablissement:app.nom_etablissement,
			token:app.token,
			symbole:'square',
			item_id:item_id,
			progression:1,
			time:Math.floor(app.myTime()/1000)
		}, function(data) {
			app.render(data);
			app.publicProgressionsRender();
		}
		);
	}
	else{
		$.post(app.serveur + "index.php?go=skills&q=uncheck"+app.connexionParam,
		{
			eleves:JSON.stringify([app.eleve_id]),
			nom_etablissement:app.nom_etablissement,
			token:app.token,
			item_id:item_id
		}, function(data) {
			app.render(data);
			app.publicProgressionsRender();
		}
		);
	}
}
app.publicDelProgression=function(progression_id,item_id){
	// if (!app.checkConnection()) {return;}
	// $.post(app.serveur + "index.php?go=skills&q=deleteProgression",
	// {
	// 	eleve_id:app.eleve_id,
	// 	nom_etablissement:app.nom_etablissement,
	// 	token:app.token,
	// 	item_id:item_id,				
	// 	progression_id:progression_id
	// }, function(data) {
	// 	app.render(data);
	// 	app.publicProgressionsRender();
	// }
	// );
}
