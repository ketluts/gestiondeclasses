/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

	app.publicItemSetFilters=function(){
		var min=app.itemsProgressionsFilterValueMin;
		var max=app.itemsProgressionsFilterValueMax;
		$('.item-box').css('display','block');
		$('.items-sous-categorie').css('display','block');
		$('.items-categorie').css('display','block');
		var k=0;
		for (var c =0,llng= app.itemsCategories.length; c<llng; c++) {
			var categorie=app.itemsCategories[c];
			var is_categorie_visible=false;
			for (var i =0, lng=categorie['sous_categories'].length;i<lng; i++) {
				var sous_categorie=categorie['sous_categories'][i];
				var is_sous_categorie_visible=false;
				for (var j =0, lllng=sous_categorie['list'].length;j<lllng; j++) {
					var item=sous_categorie['list'][j];
					var is_visible=0;
					if(app.itemsFiltersUsers.indexOf(item['user_pseudo'])>=0 || app.itemsFiltersUsers.length==0){					
						is_visible++;
					}
					if(app.itemsFiltersCategories.indexOf(item['item_categorie'])>=0 || app.itemsFiltersCategories.length==0){					
						is_visible++;
					}
					if(app.itemsFiltersSousCategories.indexOf(item['item_sous_categorie'])>=0 || app.itemsFiltersSousCategories.length==0){					
						is_visible++;
					}
					if(item.is_visible){
						is_visible++;
					}
					if(app.isItemVisibleByTags(item)){
						is_visible++;
					}
					var progression=app.itemsStudentProgressionByItem(item.item_id);
					if(progression){
						if(progression.string<=max && progression.string>=min){
							is_visible++;
						}	
					}
					if((app.itemsView=='edit' && app.itemsEditable.indexOf(""+item['item_id']+"")>=0) || app.itemsView!='edit'){
						is_visible++;				
					}
					if(app.itemsView=="edit"){
						$('#item_'+item['item_id']+'_checkbox_mode').prop("disabled", false);
					}
					else{
						$('#item_'+item['item_id']+'_checkbox_mode').prop("disabled", true);	
					}

					if(is_visible<7){
						item.isOnGraph=false;
						document.getElementById('item_'+item['item_id']+'_bloc').style.display='none';
					}else{
						item.isOnGraph=true;
						is_sous_categorie_visible=true;	
					}
					if(app.itemsView=="view"){
						if(item.item_auto_eval==true && item.item_auto_eval_filter==true){
						//AUTO EVAL FILTRE
						var slide=$("#item_"+item['item_id']+"_slider");
						slide.off('slidechange');
						slide.off('slide');
						$('#item_'+item.item_id+'_autoeval_filter').css('display','block');	
						slide.slider('value',0);
						$('#item_'+item['item_id']+'_slider_style').css('background-color', "rgba(0,0,0,0)"); 
					}
				}else{
					$('#item_'+item.item_id+'_autoeval_filter').css('display','none');		
				}	
			}
			if(is_sous_categorie_visible==false){
				document.getElementById('items_sous_categorie_'+c+'_'+i).style.display='none';
		}
		else{			
			document.getElementById('items_sous_categorie_'+c+'_'+i).style.display='block';
				is_categorie_visible=true;
			}
		}
		if(is_categorie_visible==false){
			document.getElementById('items_categorie_'+c+'').style.display='none';
			document.getElementById('items_categorie_list_'+c+'').style.display='none';
			k++;
		}
		else{
			document.getElementById('items_categorie_'+c+'').style.display='block';
			document.getElementById('items_categorie_list_'+c+'').style.display='';
		}
	}
	if(k==app.itemsCategories.length && app.items.length>0){
		document.getElementById('itemsNothingToShow').style.display='block';
	}
	else{
		document.getElementById('itemsNothingToShow').style.display='none';	
	}
	app.itemsCategoriesProgressRender();
	app.publicSkillsGraph();
}
app.isItemVisibleByTags=function(item){
	var tags=item.item_tags;
	var n=app.itemsFiltersTags.length;
	//var m=app.itemFiltersTagsExclude.length;
	if(n==0){
		return true;
	}
	if(!tags && n>0){
		return false;
	}
	var k=0;
	tags=tags.split(',');
	//tags.push(ucfirst(item.item_categorie.toLowerCase()));
	//tags.push(ucfirst(item.item_sous_categorie.toLowerCase()));
	tags=arrayUnique(tags.map(function(value){if(value){return value.trim();}})).sort();
	for (var i = tags.length - 1; i >= 0; i--) {
		if(app.itemsFiltersTags.indexOf(tags[i])>=0){
			k++;
		}
	}
	if(k>0){
		// if(app.itemFiltersTagsMode=="et"){
		// 	if(k==n){
		// 		return true;
		// 	}
		// 	return false;
		// }
		return true;
	}
	// if(m>0 && n==0){
	// 	return true;
	// }
	return false;
}
app.publicItemFilterToggle=function(num,type){
	if(type=="user"){
		var list=app.itemsFiltersUsers;
		var value=app.itemsUsersDatalist[num];
		var item=$("#filtre_user_"+num+"");
	}
	else if(type=="categorie"){
		var list=app.itemsFiltersCategories;
		var value=app.itemsCategoriesDatalist[num];
		var item=$("#filtre_categorie_"+num+"");
	}
	else if(type=="sous_categorie"){
		var list=app.itemsFiltersSousCategories;
		var value=app.itemsSousCategoriesDatalist[num].name;
		var item=$("#filtre_sous_categorie_"+num+"");
	}
	else if(type=="tag"){
		var list=app.itemsFiltersTags;
		var value=app.itemsTagsDatalist[num];
		var item=$("#filtre_tag_"+num+"");
	}
	if(list.indexOf(value)<0){
		list.push(value);		
		//item.removeClass('btn-default').addClass('btn-primary');
	}else{
		var new_filtres=[];
		for (var i = list.length - 1; i >= 0; i--) {
			if(list[i]!=value){
				new_filtres.push(list[i]);
			}
		}
		list=new_filtres;			
		//item.removeClass('btn-primary').addClass('btn-default');
	}
	if(type=="user"){
		app.itemsFiltersUsers=list;		
	}
	else if(type=="categorie"){
		app.itemsFiltersCategories=list;
	}
	else if(type=="sous_categorie"){
		app.itemsFiltersSousCategories=list;
	}
	else if(type=="tag"){
		app.itemsFiltersTags=list;
	}
	//alert(app.itemsFiltersSousCategories);
	app.publicItemSetFilters();
	app.publicItemsTagsRender(); 
}
app.publicItemsTagsRender=function(){
	//##############
	//Filtres render
	//##############
	var html=[];
	
	for (var i =0,lng= app.itemsUsersDatalist.length ; i <lng; i++) {
		var user=app.itemsUsersDatalist[i];
		var style="btn-default";
		if(app.itemsFiltersUsers.indexOf(user)>=0){
			style="btn-primary";
		}
		html.push("<button class='btn "+style+"' id='filtre_user_"+i+"' style='margin-left:5px;' onclick='app.publicItemFilterToggle("+i+",\"user\");'>" + ucfirst(user) + "</button>");
	}
	document.getElementById('competences_filtre_users').innerHTML=html.join('');
	var html=[];	
	for (var i =0,lng= app.itemsCategoriesDatalist.length ; i <lng; i++) {
		var style="btn-default";
		var categorie=app.itemsCategoriesDatalist[i];
		if(!categorie || categorie==""){continue;}
		if(app.itemsFiltersCategories.indexOf(categorie)>=0){
			style="btn-primary";
		}
		html.push("<button class='btn "+style+"' id='filtre_categorie_"+i+"' style='margin-left:5px;' onclick='app.publicItemFilterToggle("+i+",\"categorie\");'>" + ucfirst(categorie) + "</button>");
	}
	document.getElementById('competences_filtre_categories').innerHTML=html.join('');
	
	var html=[];		
	for (var i = 0, lng=app.itemsSousCategoriesDatalist.length ; i<lng; i++) {
		var style="btn-default";
		var sous_categorie=app.itemsSousCategoriesDatalist[i];
		if(!sous_categorie.name || sous_categorie.name==""){continue;}
		if(app.itemsFiltersSousCategories.indexOf(sous_categorie.name)>=0){
			style="btn-primary";
		}
			//if(app.itemsFiltersCategories.indexOf(sous_categorie.parent)>=0){
				html.push("<button class='btn "+style+"' id='filtre_sous_categorie_"+i+"' style='margin-left:5px;' onclick='app.publicItemFilterToggle("+i+",\"sous_categorie\");'>" + ucfirst(sous_categorie.name) + "</button>");
			//}			
		}
		document.getElementById('competences_filtre_sous_categories').innerHTML=html.join('');

		var html=[];	
		for (var i =0,lng= app.itemsTagsDatalist.length ; i<lng; i++) {
			var style="btn-default";
			var tag=app.itemsTagsDatalist[i];
			if(!tag || tag==""){continue;}
			if(app.itemsFiltersTags.indexOf(tag)>=0){
				style="btn-primary";
			}
			html.push("<button class='btn "+style+"' id='filtre_tag_"+i+"' style='margin-left:5px;' onclick='app.publicItemFilterToggle("+i+",\"tag\");'>" + ucfirst(tag) + "</button>");
		}
		document.getElementById('competences_filtre_tags').innerHTML=html.join('');
	}
	app.publicItemInitProgressionsFilter=function(){
		$("#acquisition-slider" ).slider( "values", [ 0, 100 ] );
		$("#itemsFiltersProgressionsReset").css('visibility','hidden');	
		app.pluginsLauncher('skillsFiltersInit');
	}