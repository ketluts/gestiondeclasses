/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.skillsInit=function(){
	app.publicItemsRender();  
	app.publicProgressionsRender();
	$("#acquisition-slider").slider({
		min:0,
		max:100,
		step:0.01,
		values:[0,100],
		range:true,
		change: function( event, ui ) {   
			app.itemsProgressionsFilterValueMin=ui.values[0];
			app.itemsProgressionsFilterValueMax=ui.values[1];
			if(ui.values[0]>0 || ui.values[1]<100){
				$("#itemsFiltersProgressionsReset").css('visibility','visible'); 
			}
			app.publicItemSetFilters(); 
		}
	});
	app.publicItemInitProgressionsFilter();
	app.publicItemToggleMode('view');
}
app.buildItemsIndex=function(){	
	app.itemsIndex=[];
	for (var i = 0, lng = app.items.length; i < lng; i++) {
		app.itemsIndex[app.items[i].item_id]=app.items[i];
	}
}
app.getItemById=function(item_id){
	return app.itemsIndex[item_id];
}
app.publicBuiltItemsListe=function(){
	var items_ids=[];
	var items=[];
	var filtersUsers=[];
	app.itemsEditable=[];
	var categories=[];
	var sous_categories=[];
	var list_sous_categories=[];
	var tags=[];
	for (var i = app.progressions.length - 1; i >= 0; i--) {
		var progression=app.progressions[i];
		var index=items_ids.indexOf(""+progression['item_id']+"");
		if(progression['rei_value']=="-2"){				
			app.itemsEditable.push(progression['item_id']);		
		}
		if(index>=0){		
			continue;
		}	
		progression.item_categorie=progression.item_categorie||"Non classés";
		progression.item_sous_categorie=progression.item_sous_categorie||"Non classés";
		

		var user=app.getUserById(progression.item_user);

		var item={
			item_name:progression.item_name,
			item_id:progression.item_id,
			item_categorie:ucfirst(progression.item_categorie),
			item_sous_categorie:ucfirst(progression.item_sous_categorie),
			item_value_max:progression.item_value_max,
			item_vue:progression.item_vue,
			item_color:progression.item_color,
			item_colors:progression.item_colors,
			item_descriptions:progression.item_descriptions,
			item_mode:progression.item_mode,
			item_tags:progression.item_tags,
			user_pseudo:app.renderUserName(user)
		};




		categories.push(progression.item_categorie);

		if(item['item_tags']){tags=tags.concat(item['item_tags'].split(','));}

		if(list_sous_categories.indexOf(progression.item_sous_categorie)<0){
			sous_categories.push({
				name:progression.item_sous_categorie,
				parent:progression.item_categorie
			});
			list_sous_categories.push(progression.item_sous_categorie);
		}		
		if(progression['rei_value']=="-2"){
			item.item_auto_eval=true;
			item.item_auto_eval_filter=true;
		}
		var user=app.renderUserName(app.getUserById(progression.rei_user));
		if(filtersUsers.indexOf(user)<0 && progression['rei_user_type']=='user'){
			filtersUsers.push(user);
		}
		items.push(item);
		items_ids.push(progression.item_id);
	}
	tags=arrayUnique(tags.map(function(value){if(value){return value.trim();}})).sort(function(a,b){
		return (a).localeCompare(b);
	});
	categories=arrayUnique(categories.map(function(value){if(value){return value.trim();}})).sort(function(a,b){
		return (a).localeCompare(b);
	});
	// sous_categories.sort(function(a,b){
	// 	return (a.name).localeCompare(b.name);
	// });
	sous_categories=arrayUnique(sous_categories.map(function(value){if(value){value.name.trim(); return value;}})).sort(function(a,b){
		return (a.name).localeCompare(b.name);
	});
	//alert(dump(sous_categories));
	app.items=items;
	app.itemsUsersDatalist=filtersUsers.sort(function(a,b){
		return (a).localeCompare(b);
	});
	app.itemsCategoriesDatalist=categories;
	app.itemsSousCategoriesDatalist=sous_categories;
	app.itemsTagsDatalist=tags;
	//##############
	//Trie des items par catégories et sous-catégories
	//##############
	app.items=app.orderBy(app.items,'item_name','ASC');
	app.itemsCategories=[];
	var categories_name=[];
	for (var i =0, lng= app.items.length; i<lng; i++) {
		var item=app.items[i];
		item['item_num']=i;
		item['is_visible']=true;
		var index=categories_name.indexOf(item['item_categorie']);
		if(index<0){
			//Si la catégorie n'existe pas
			var new_categorie=[];
			new_categorie['sous_categories']=[];
			new_categorie['name']=item['item_categorie'];	
			new_categorie['id']=categories_name.length-1;		
			new_categorie['sous_categories_liste']=[item['item_sous_categorie']];
			new_categorie['sous_categories']=[];
			var new_sous_categorie=[];
			new_sous_categorie['name']=item['item_sous_categorie'];
			new_sous_categorie['list']=[item];
			new_sous_categorie['id']=0;		
			new_categorie['sous_categories'].push(new_sous_categorie);
			app.itemsCategories.push(new_categorie);
			categories_name.push(item['item_categorie']);
			item['item_categorie_num']=0;
			item['item_sous_categorie_num']=0;
		}
		else{
			//Si la catégorie existe		
			var index_sous_categorie=app.itemsCategories[index]['sous_categories_liste'].indexOf(item['item_sous_categorie']);
			if(index_sous_categorie<0){
				//Si la sous catégorie n'exite pas 
				var new_sous_categorie=[];
				new_sous_categorie['name']=item['item_sous_categorie'];
				new_sous_categorie['list']=[item];
				new_sous_categorie['id']=app.itemsCategories[index]['sous_categories_liste'].length-1;
				app.itemsCategories[index]['sous_categories'].push(new_sous_categorie);
				app.itemsCategories[index]['sous_categories_liste'].push(item['item_sous_categorie']);
				item['item_sous_categorie_num']=0;
			}else{
				//Si la sous catégorie exite
				app.itemsCategories[index]['sous_categories'][index_sous_categorie]['list'].push(item);
				item['item_sous_categorie_num']=index_sous_categorie;
			}
			item['item_categorie_num']=index;
		}		
	};
	app.buildItemsIndex();	
	for (var i = app.items.length - 1; i >= 0; i--) {
		var item=app.items[i];
		if(app.itemsEditable.indexOf(item.item_id)>=0){
			item.item_auto_eval=true;
			item.item_auto_eval_filter=true;
		}
	}
	app.publicItemsTagsRender();
}
app.itemsStudentProgressionsBuild=function(){
	var progressions=app.progressions;	
	var itemList=[];
	var progressionByUser={
		itemsList:[],
		itemsIds:[]
	};
	for (var i = progressions.length - 1; i >= 0; i--) {
		var progression=progressions[i];
		if(progression['rei_value']<0){
			continue;
		}
		if(app.itemsView=='view' && progression['rei_user_type']=='eleve'){
			continue;
		}
		if(app.itemsView=='edit' && progression['rei_user_type']=='user'){
			continue;
		}	
		var item_id=progression['item_id'];
		var user_id=progression['rei_user'];
		if(itemList.indexOf(item_id)<0){
			itemList.push(item_id);
		}
		var item=app.getItemById(item_id);
		if(item.item_vue!="cursor" && progression.rei_value>1){
			progression['rei_value']=1;
		}	
		//ELEVES PROGRESSIONS CONSTRUCTIONS
		var indexItemId=progressionByUser.itemsIds.indexOf(item_id);
		if(indexItemId<0){
			progressionByUser.itemsList.push({
				liste:[[]],
				userIds:[user_id],
				item_id:item_id
			});
			progressionByUser.itemsIds.push(item_id);
			indexItemId=progressionByUser.itemsIds.length-1;
		}		
		var indexUser=progressionByUser.itemsList[indexItemId].userIds.indexOf(user_id);
		if(indexUser==-1){
			progressionByUser.itemsList[indexItemId].userIds.push(user_id);
			indexUser=progressionByUser.itemsList[indexItemId].userIds.length-1;
			progressionByUser.itemsList[indexItemId].liste[indexUser]=[];
		}
		progressionByUser.itemsList[indexItemId].liste[indexUser].push(progression);
		
		if(!app.studentProgressions[item_id]){
			app.studentProgressions[item_id]={
				"value":0,
				"nb":0
			};
		}
		app.studentProgressions[item_id].nb++;		
	};
	var items=progressionByUser.itemsList;
	for (var j = items.length - 1; j >= 0; j--) {
		var oitem=items[j];
		var item=app.getItemById(oitem.item_id);
		var progressions=[];
		for (var k = oitem.liste.length - 1; k >= 0; k--) {
			var n=oitem.liste[k].length;
			if(n){
				progressions.push(app.itemsProgressionsSynthese(oitem.liste[k],item.item_mode));
			}				
		}
		n=progressions.length;
		var value=0;
		if(n){
			value=progressions.reduce(app.add,0)/n;
		}
		app.studentProgressions[item.item_id].value=value;
	}
}
app.itemsProgressionsSynthese=function(progressions,mode){ 
	progressions=app.orderBy(progressions,'rei_date','DESC');
	if(mode=="auto"){
		var total=0;
		var coeff={
			"square":1,
			"star1":1,
			"star2":1,
			"star3":1
		};
		var somme=0;	
		var current_date=null;	
		for (var i =0,lng= progressions.length; i <lng; i++) {
			var progression=progressions[i];

			if(!current_date){
				current_date=progression.rei_date;
			}
			progression.rei_symbole=progression.rei_symbole||"square";
			if(current_date-progression.rei_date>2592000){
				coeff[progression.rei_symbole]=Math.max(0,coeff[progression.rei_symbole]-0.3);
				current_date=progression.rei_date;
			}
			var c=coeff[progression.rei_symbole];
			somme+=app.itemsGetProgressionValue(progression)*c*app.itemsCyclesCoeffs[progression.rei_symbole];
			total+=c*app.itemsCyclesCoeffs[progression.rei_symbole];
		}
		return somme/total;
	}
	else{					
		return app.itemsGetProgressionValue(progressions[0]);
	}
}
app.itemsGetProgressionValue=function(progression){
//var progression=app.getProgressionById(progression_id);
//return progression.rei_value;
if(!progression.rei_symbole || progression.rei_symbole=="square"){
	return progression.rei_value;
}
if(progression.rei_value<0){
	return progression.rei_value;
}
//var eleve=app.getEleveById(progression.rei_eleve);
if(!app.eleve.eleve_cycle || app.eleve.eleve_cycle=="false"){
	return progression.rei_value;
}
//alert(dump(eleve));
// var item=app.getItemById(progression.rei_item);
// if(item.item_cycle<0){
// 	return progression.rei_value;
// }
var level=app.eleve.eleve_cycle[1];
// if(item.item_cycle>eleve.eleve_cycle[0]){
// return progression.rei_value;	
// }
// if(item.item_cycle<eleve.eleve_cycle[0]){
// level="F";	
// }
return progression.rei_value*app.itemsCyclesCoeffsMalus[progression.rei_symbole][level];
}
app.itemsStudentProgressionByItem=function(item_id){
	var item=app.getItemById(item_id);
	var mode=item['item_mode'];
	var max=item['item_value_max'];
	var value=-1;
	var value_in_percent="-";
	var n=0;	
	if(app.studentProgressions[item_id]){
		n=app.studentProgressions[item_id]['nb'];
		value=app.studentProgressions[item_id]['value'];
		value_in_percent=Math.min(value*100/max,100);	
	}
	var slideValue=value;
	if(n!=0){
		slideValue=Math.max(value,max*0.08);
	}
	return {
		"value":Math.min(value,max),
		"slideValue":Math.min(slideValue,max),
		"string":value_in_percent,
		'nb_evaluations':n
	};
}