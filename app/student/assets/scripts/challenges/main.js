/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.publicRenderEtoiles=function(){
	var etoiles=app.etoiles;
	var html=[];		
	document.getElementById('defis').style.display = "none";
	$('#header-menu-eleve-defi').attr('disabled','disabled');
	if(etoiles.length==0){	
		$('#etoiles_info').css('display','none');
		return;	
	}else{
		$('#etoiles_info').css('display','block');
	}
	$('#agenda').fullCalendar( 'removeEventSource',  app.etoilesEventsSource );
	app.etoilesEventsSource=[];
	var display=false;
	for (var i=0,lng=etoiles.length; i<lng; i++) {
		var etoile=etoiles[i];
		if(etoile.etoile_type!='defi'){
			continue;
		}	
		display=true;
		var user=app.getUserById(etoile.defi_user);
		var etoile_user=app.renderUserName(user);
		app.legendsUsers.push(etoile_user);
		html.push('<div class="col-lg-6">');
		html.push('<div class="well well-sm col-lg-12">');
		html.push('<div class="col-lg-2 text-center"><img class="defi_icon" style="background-color:#'+app.getColor(etoile_user)+'" src="assets/lib/educational-icons/svg/'+etoile.defi_icon+'.svg"/><br/>');
		html.push(app.renderEtoiles(etoile.etoile_value,etoile.defi_etoiles,etoile.etoile_state));
		html.push('</div>');
		html.push('<div class="col-lg-10">');
		if(etoile.defi_url && etoile.defi_url!=""){
			html.push('<div class="btn-close"><a href="'+app.serveur+'index.php?go=defis&q=open&url='+etoile.defi_url+'&defi_id='+etoile.defi_id+'&eleve_id='+app.eleve_id+'&nom_etablissement='+app.nom_etablissement+'&token='+app.token+'" target="_blank"><button class="btn btn-primary btn-xs">Accéder au défi</button></a></div>');
		}
		html.push('<div class="defi_titre h3">'+etoile.defi_titre+'</div>');
		html.push('<div class="defi_description h5">'+etoile.defi_description+'</div>');
		html.push('</div>');
		html.push('</div>');
		html.push('</div>');
		app.etoilesEventsSource.push({
			title: ucfirst(etoile.defi_titre)+' <br/> '+app.renderEtoiles(etoile.etoile_value,etoile.defi_etoiles,etoile.etoile_state),
			allDay:true,
			start: etoile.etoile_date*1000,
			backgroundColor:"#ffffff",
			textColor:"#000000",               
			borderColor:"#" + app.getColor(etoile_user),
			className :"etoile_event"
		});
	};
	if(display){
		$('#header-menu-eleve-defi').removeAttr('disabled');
		document.getElementById('defis').style.display = "block";
	}
	document.getElementById('etoiles').innerHTML=html.join('');
	$('#agenda').fullCalendar( 'addEventSource',  app.etoilesEventsSource );
	app.renderLegendsUsers('eleve_calendar_legende');
};