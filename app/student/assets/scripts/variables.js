/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.nom_etablissement="";

app.loadUi=0;
app.disciplines=[];
app.partners=[];
app.itemsTagsConfig=[];
app.plugins=app.plugins||[];
app.init=function(){  

//**********
// App
//*********
app.version = "v20191106";
app.currentMode="connexion";
app.platform="web";
app.isConnected =true;
app.urlHash="";
app.mouse={
  x:0,
  y:0 
};
app.timerPing=null;
app.active_periode=null;
app.periodes=[];
app.users=[];
app.usersIndex=[];
app.pi=Math.PI;
app.cacheColor=[];
app.legends=[];
clearTimeout(app.discussionTimer);
app.discussionTimer=null;
app.discussionRefreshTime=60000;
app.enableKeyboardShorcuts=true;
app.editors=[];
//**********
// Connexion
//*********
app.connexionParam = "";
//**********
// UI
//*********
app.colorCalendarBy = "user";
app.currentView="";
app.currentDiscussion=null;
app.homeCurrentEditionWeek="AB";
app.zindex=10;
app.zindexBlock=false;
app.newAlertWatched=[];
app.newAlertTrigerred="";
app.weeks=0;
//**********
// Challenges
//*********
app.etoiles=[];
//**********
// Files
//*********
app.shareEventsSource=[];
app.files=[];
//**********
// Skills
//********* 
app.items=[];
app.itemsIndex=[];
app.graphDataCategories=[];
app.itemsView='view';
app.itemsEditable=[];
app.itemsProgressionsFilterValueMin=0;
app.itemsProgressionsFilterValueMax=100; 
app.progressions=[];
app.itemsCyclesCoeffsMalus={
  "star1":{
    "D":1,
    "M":0.8,
    "F":0.4
  },
  "star2":{
    "D":1,
    "M":1,
    "F":0.8
  },
  "star3":{
    "D":1,
    "M":1,
    "F":1
  }
};
app.itemsCyclesCoeffs={
  "square":1,
  "star1":1,
  "star2":1,
  "star3":1
};
//**********
// Filters
//*********
app.filters=[];
//**********
// Tests
//*********
app.notes=[];
//**********
// Links
//*********
app.links=[];
//**********
// Messagerie
//*********
app.messages=[];
app.destinataires=[];
app.messagesSort=0;
app.messagesSyncId=0;
app.destinataires=[];
//**********
// Agenda
//*********
app.agenda=[];
app.agendaEventsSource=[];
app.pubicAgendaTooltip=null;
app.edtEditionMode=false;
app.agendaNextDaysNb=4;
app.edtMinTime=null;
app.edtMaxTime=null;
app.agendaEventHomeBackGround="#cbcbcb";
app.agendaEventCommentBackGround="#b8b8b8";
app.agendaEventBlackBoardBackGround="#ffffff";
app.timers=[];
//**********
// Datatable
//*********
app.datatableFR={    "sProcessing":     "Traitement en cours...",    "sSearch":         "Rechercher&nbsp;:",    "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",    "sInfo":           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",    "sInfoEmpty":      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",    "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",    "sInfoPostFix":    "",    "sLoadingRecords": "Chargement en cours...",    "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",    "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",    "oPaginate": {        "sFirst":      "Premier",        "sPrevious":   "Pr&eacute;c&eacute;dent",        "sNext":       "Suivant",        "sLast":       "Dernier"    },    "oAria": {        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"    }};

//**********
// Public
//*********
app.classes=[];
app.token="";
app.relations=[];
app.eleve_id="";
app.eleve={};
app.itemsFiltersUsers=[];
app.itemsFiltersCategories=[];
app.itemsFiltersSousCategories=[];
app.itemsFiltersTags=[];
app.studentProgressions=[];
app.calendarView = "agendaWeek";
app.testIntelligences=false;
app.intelligencesQuestions=[];
app.intelligencesResults=[];
app.intelligencesResultsTemp=[];
//Init UI
$('.btn').prop("disabled", false);
if(app.loadUi==0)
{
  app.loadUi++;
  // $("#header").stick_in_parent({
  //   sticky_class:"header_sticky"
  // }); 
}
}
