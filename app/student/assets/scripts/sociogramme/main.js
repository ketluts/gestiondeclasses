/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.publicSociogrammeIniSelect=function(){
	var eleves=[];
	var eleves_ids=[];
	for (var i = app.classes.length - 1; i >= 0; i--) {
		var classe_eleves=app.classes[i].eleves;
		for (var j = classe_eleves.length - 1; j >= 0; j--) {
			var eleve=classe_eleves[j];
			if(eleves_ids.indexOf(eleve.eleve_id)<0){
				eleves.push(eleve);
				eleves_ids.push(eleve.eleve_id);
			}
		}
	};
	var html=[];
	html.push('<option value="-1">');
	html.push('-');
	html.push('</option>');
	for (var i = eleves.length - 1; i >= 0; i--) {
		var eleve=eleves[i];
		if(eleve.eleve_id==app.eleve_id){
			continue;
		}
		html.push('<option value="'+eleve.eleve_id+'">');
		html.push(eleve.eleve_nom+' '+eleve.eleve_prenom);
		html.push('</option>');
	}
	document.getElementById('socigramme-plusplus').innerHTML=html.join('');
	document.getElementById('socigramme-plus').innerHTML=html.join('');
	document.getElementById('socigramme-moins').innerHTML=html.join('');
	for (var i = app.relations.length - 1; i >= 0; i--) {
		var relation=app.relations[i];
		if(relation.relation_value==2){
			document.getElementById('socigramme-plusplus').value=relation.relation_to;
		}
		else if(relation.relation_value==1){
			document.getElementById('socigramme-plus').value=relation.relation_to;
		}
		else if(relation.relation_value==-1){
			document.getElementById('socigramme-moins').value=relation.relation_to;
		}
	}
}
app.publicRelationAdd=function(){
	if (!app.checkConnection()) {return;}
	var relation_1=document.getElementById('socigramme-plusplus').value;
	var relation_2=document.getElementById('socigramme-plus').value;
	var relation_3=document.getElementById('socigramme-moins').value;
	$.post(app.serveur + "index.php?go=sociogramme&q=updateRelations&nom_etablissement="+app.nom_etablissement+"&token="+app.token,{
		relation_1:relation_1,
		relation_2:relation_2,
		relation_3:relation_3,
		time:Math.floor(app.myTime()/1000)
	}, function(data) {
		app.render(data);   
	}
	);
}