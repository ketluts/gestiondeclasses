/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

	app.renderPublicLinks=function(){
		var links=app.links;
		var html=[];
		html.push('<table id="eleve_links_table"  width="100%">');
		html.push('<thead>');
		html.push('<tr>'); 
		html.push('<th>Matière</th>');
		html.push('<th>Site</th>');
		html.push('<th>Adresse</th>');
		html.push('<th>Date</th>');
		html.push('<th></th>');
		html.push('</tr>');
		html.push('</thead>');    
		html.push('<tbody>');
		for (var i = 0, lng=links.length ; i<lng; i++) {
			var link=links[i];
			var user=app.getUserById(link.share_user);
			app.legendsUsers.push(app.renderUserName(user));
			var share_icon="";
			if(link.link_favicon!=""){
				share_icon="<img src='"+link.link_favicon+"' class='link_favicon' />";
			}				 
			html.push('<tr>');
			html.push('<td>');
			html.push(app.renderUserName(user));
			html.push('</td>');
			html.push('<td>');
			html.push(ucfirst(link.link_titre));
			html.push(share_icon);
			html.push('</td>');	
			html.push('<td>');
			html.push('<a href="'+link.link_url + '" target="_blank">');
			html.push(link.link_url);
			html.push('</a> ');
			html.push('</td>');

			html.push('<td data-order="'+link.share_date+'">');
			html.push(moment(parseInt(link.share_date)*1000).format('DD MMM YYYY'));
			html.push('</td>');		
			html.push('<td>');
			html.push('<a href="'+link.link_url + '" target="_blank" class="btn btn-primary btn-sm pull-left">');
			html.push('<span class="glyphicon glyphicon-share-alt"></span> Ouvrir');
			html.push('</a> ');
			html.push('</td>');		
			html.push('</tr>');
			
			app.shareEventsSource.push({
				title: '<span class="glyphicon glyphicon-link"></span> Nouveau lien partagé<br/>'+ucfirst(link.link_titre),
				allDay:true,
				start: link.share_date*1000,
				backgroundColor:"#ffffff",
				textColor:"#000000",               
				borderColor:"#" + app.getColor(app.renderUserName(user))
			});
		}
		html.push('</tbody>');
		html.push("</table>");
		document.getElementById('eleve_links').innerHTML=html.join('');	
		$('#eleve_links_table').DataTable({
			stateSave: true,
			language:app.datatableFR,
			"order": [[ 3, 'desc' ]]
		});
		$('#eleve_links_table').addClass('table table-striped ');
		app.renderLegendsUsers('eleve_calendar_legende');
	}