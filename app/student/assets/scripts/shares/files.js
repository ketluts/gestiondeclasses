/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.renderPublicFiles=function(){
	
		var files=app.files;
		var html=[];
		 
		html.push('<table id="eleve_files_table"  width="100%">');
		html.push('<thead>');
		html.push('<tr>'); 
		html.push('<th>Matières</th>'); 
		html.push('<th>Type</th>'); 
		html.push('<th>Nom du fichier</th>');
		html.push('<th>Poids</th>');
		html.push('<th>Date</th>');
		html.push('<th></th>');
		html.push('</tr>');
		html.push('</thead>');    
		html.push('<tbody>');
		for (var i = 0, lng=files.length ; i<lng; i++) {
			var file=files[i];
			var user=app.getUserById(file.share_user);
			app.legendsUsers.push(app.renderUserName(user));
			html.push('<tr>');
			html.push('<td>');
			html.push(app.renderUserName(user));
			html.push('</td>');
			html.push('<td class="file_type">');
			html.push("<span class='' style='background-color:#" + app.getColor(file.file_type.toLowerCase()+'') + ";'>"+file.file_type.toUpperCase()+"</span>");
			html.push('</td>');
			html.push('<td>');
			html.push(file.file_fullname);
			html.push('</td>');
			html.push('<td data-order="'+file.file_size+'">');
			html.push(bytesToSize(file.file_size,2));
			html.push('</td>');
			html.push('<td data-order="'+file.share_date+'">');
			html.push(moment(parseInt(file.share_date)*1000).format('DD MMM YYYY'));
			html.push('</td>');		
			html.push('<td data-order="'+file.file_size+'">');
			html.push('<a href="'+app.serveur + 'index.php?go=shares&q=getFile&file_id='+file.file_id+'&nom_etablissement='+app.nom_etablissement+'&token='+app.token+'" target="_blank" class="btn btn-primary btn-sm">');
			html.push('<span class="glyphicon glyphicon-cloud-download"></span> Télécharger');
			html.push('</a> ');
			html.push('</td>');		
			html.push('</tr>');
			app.shareEventsSource.push({
						title: '<span class="glyphicon glyphicon-file"></span> Nouveau fichier partagé<br/>'+file.file_fullname,
						allDay:true,
						file_id:file.file_id,
						start: file.share_date*1000,
						backgroundColor:"#ffffff",
						textColor:"#000000",               
						borderColor:"#" + app.getColor(app.renderUserName(user))
					});
		}
		html.push('</tbody>');
		html.push("</table>");
		document.getElementById('eleve_files').innerHTML=html.join('');	
		$('#eleve_files_table').DataTable({
			stateSave: true,
			language:app.datatableFR,
			 "order": [[ 4, 'desc' ]]
		});
		$('#eleve_files_table').addClass('table table-striped ');
		
		app.renderLegendsUsers('eleve_calendar_legende');
	}
