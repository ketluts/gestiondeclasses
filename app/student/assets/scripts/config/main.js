/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.setLegends=function(legends){
 if(legends.length==0){return;}
 for (var i = legends.length - 1; i >= 0; i--) {
  var legend=legends[i];
  app.legends[legend.legend_event]=legend.legend_text;
};
};
app.getDisciplineById=function(discipline_id){
  for (var i = app.disciplines.length - 1; i >= 0; i--) {
    if(app.disciplines[i].discipline_id==discipline_id){
      return app.disciplines[i];
    }
  };
  return false;
};
app.getPeriodeById=function(periode_id){
  for (var i = app.periodes.length - 1; i >= 0; i--) {
    if(app.periodes[i].periode_id==periode_id){
      return app.periodes[i];
    }
  };
  return false;
};

app.buildUsersIndex=function(){ 
  app.usersIndex=[];
  for (var i = 0, lng = app.users.length; i < lng; i++) {
    app.users[i].user_pseudo=ucfirst(app.users[i].user_pseudo);
    app.usersIndex[app.users[i].user_id]=app.users[i];
  }
}
app.getUserById=function(user_id){
  return app.usersIndex[user_id]||false;  
}

app.renderUserName=function(user){
  if(user.user_nom){
    return user.user_nom.toUpperCase();
  }
  else{
    return user.user_pseudo;
  }
}