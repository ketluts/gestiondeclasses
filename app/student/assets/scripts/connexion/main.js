/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.connexionFormEtablissementAutocompletion=function(etablissements_liste){
  var liste=etablissements_liste;
  var options = '';
  for(var i = 0,lng=liste.length; i < lng; i++)
    options += '<option value="'+liste[i]+'" />';
  document.getElementById('list_schools').innerHTML = options;
}
app.deconnexion=function(){  
  app.init();
  app.currentMode="";
  app.connexionRender();
}
app.checkConnection=function() {
 if (!app.isConnected) {
   app.alert({title:'Vous devez être connecté pour effectuer cette action.'});
   return false;
 }
 return true;
}

app.publicConnexion=function(){   
  app.nom_etablissement=document.getElementById('etablissement_eleve_nom').value;
  var token=document.getElementById('cnx_eleve_token').value;
  $.get(app.serveur + "index.php?go=eleveView&q=get&nom_etablissement="+app.nom_etablissement+"&token="+token, function(data) {
    app.render(data);  
    $('#connexion-btn').button('reset');
  }
  );
}