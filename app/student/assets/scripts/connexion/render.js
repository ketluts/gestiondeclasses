/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.connexionRender=function(){
	app.viewClear();
	app.discussionTimer=null;
	$(".btn-membre").css('display','inline-block');
	$('#home-don-btn').css('display','');
	document.getElementById('header').style.display = "none";	
	$(".template_connexion").css('display','flex');
	$('.connexion-btn').button('reset');
	document.getElementById('titre').innerHTML = "";
	$(document.body).trigger("sticky_kit:recalc");
	if(app.currentMode!="connexion"){
		document.getElementById("etablissement_eleve_nom").value="";
		document.getElementById("cnx_eleve_token").value="";
	}
	app.currentMode="connexion";
	$('#connexion-eleve-form').removeClass('col-md-offset-3');
	app.pluginsLauncher('connexionAfterRender');
}