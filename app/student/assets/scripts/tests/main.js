/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.publicRenderNotes=function(){
	var notes=app.notes;
	if(notes.length==0){
		$(".eleve_notes").css('display','none');
		$('#header-menu-eleve-notes').attr('disabled');
		return;
	}
	else{
		$(".eleve_notes").css('display','block');	
		$('#header-menu-eleve-notes').removeAttr('disabled');
	}		
	document.getElementById('notes_liste').innerHTML="";
	$('#agenda').fullCalendar( 'removeEventSource', app.notesEventsSource );
	app.notesEventsSource=[];
	var periodes=app.notesSortByPeriodes(notes);
	for (var i=0, lng=periodes.length; i<lng; i++) {
		var notesBymatieres=[];
		var matieres=[];
		for (var j = 0, llng= periodes[i].notes.length ; j <llng; j++) {
			var note=periodes[i].notes[j];			
			var matiere={
				fullname:note.note_matiere+" ("+note.classe_nom+")",
				color:app.getColor(note.note_matiere),
				name:note.note_matiere
			};
			if(!notesBymatieres[matiere.fullname]){
				notesBymatieres[matiere.fullname]=[];
				matieres.push(matiere);
			}
			notesBymatieres[matiere.fullname].push(note);
		};
		var periode_titre=periodes[i].titre;
		var html=['<h3>'+periode_titre+'</h3>'];
		for (var j =0, llng=matieres.length ; j <llng; j++) {
			var matiere=matieres[j];
			app.legendsUsers.push(matiere.name);
			html.push('<div class="well well-sm col-lg-12">');
			html.push('<span class="h4 col-lg-12">');
			html.push('<span class="pseudo_color_mini" style="background-color:#' + matiere.color + ';"></span> <span><strong>'+ ucfirst(matiere.fullname)+'</strong></span> - <span><strong>'+app.moyenne(notesBymatieres[matiere.fullname])+'</strong><small>/20</small></span>');
			html.push('</span>');
			html.push('<span class="col-lg-12">');
			html.push('<br/>');
			for (var k =0, lllng= notesBymatieres[matiere.fullname].length ; k<lllng; k++) {
				var note=notesBymatieres[matiere.fullname][k];
				if(k!=0){
					html.push(' - ');
				}
				html.push('<div class="btn-group">');
				html.push('<button class="btn btn-default eleve_notes" data-toggle="tooltip" data-placement="top" title="'+ucfirst(note.controle_titre)+' - Coeff. '+note.controle_coefficient+'">');
				var note_render="";
				if(isNaN(note.note_value)){
					html.push(note.note_value);
					note_render=note.note_value;
				}
				else{
					html.push(note.note_value+'<small>/'+note.controle_note+'</small>');
					note_render=note.note_value+'/'+note.controle_note;
				}		
				html.push('</button>');
				html.push(' <button class="btn btn-default note_etoiles" style="display:none;" id="eleve_note_etoile_'+note.controle_id+'"></button>');
				html.push('</div>');
				app.notesEventsSource.push({
					title: ucfirst(note.controle_titre)+' <br/> '+note_render+'<br/><span id="eleve_note_event_etoile_'+note.controle_id+'"></span>',
					allDay:true,
					note_id:note.note_id,
					start: note.note_date*1000,
					backgroundColor:"#ffffff",
					textColor:"#000000",               
					borderColor:"#" + app.getColor(matiere.name),
					className :"note_event"
				});
			};
			html.push('</span>');
			html.push('</div>');
		};
		html.push('<br/>');
		document.getElementById('notes_liste').innerHTML+=html.join('');
	}
	document.getElementById('notes_liste').innerHTML+='<div id="notes_graphique" class="col-sm-12" style="width:100%; height:200px;"></div>';
	app.renderLegendsUsers('eleve_calendar_legende');
	app.notesGraphiqueRender(app.notes);
	$('#agenda').fullCalendar( 'addEventSource', app.notesEventsSource );
	$('.eleve_notes').tooltip();
};


app.notesEtoilesRender=function(){
	var eleve_id=app.eleve_id;
	var etoiles=app.etoiles;
	for (var i = etoiles.length - 1; i >= 0; i--) {
		var etoile=etoiles[i];
		if(etoile.etoile_type==="progression"){
			var html=[];
			html.push(app.renderEtoiles(etoile.etoile_value,3,etoile.etoile_state));
			html=html.join('');
			var div=document.getElementById('eleve_note_etoile_'+etoile.controle_id+'');			
			if(div){
				div.innerHTML=html;
				div.style.display="block";
			}
			div=document.getElementById('eleve_note_event_etoile_'+etoile.controle_id+'');
			if(div){
				div.innerHTML=html;
				
			}
		}
	};
}
app.notesGraphiqueRender=function(notes){
	var series=[];
	var tabMatieres=[];
	div='#notes_graphique';
	for (var i =0,lng= notes.length; i <lng; i++) {
		var note=notes[i];
		if(isNaN(note.note_value)){continue;}
		var user=app.getUserById(note.controle_user);
		var matiere=note.note_matiere;
		if(note.controle_categorie){
			matiere+=' - '+note.controle_categorie;
		}
		var index=tabMatieres.indexOf(matiere);
		if(index==-1){
			index=tabMatieres.length;
			series[index]={};
			series[index].type="line";
			series[index].color="#"+app.getColor(note.note_matiere);
			series[index].name=ucfirst(matiere),
			series[index].data=[]; 
			tabMatieres.push(matiere);    
		}
		series[index].data.push([note.controle_date*1000,note.note_value*1]);
	};
	$(div).highcharts({
		chart: {
			type: 'spline'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: document.ontouchstart === undefined ?
			'Click and drag in the plot area to zoom in' :
			'Pinch the chart to zoom in'
		},
		xAxis: {
			type: 'datetime',
            minRange: 1 * 24 * 3600000*365 // one days
        },
        yAxis: {
        	min:0,
        	title: {
        		text: ''
        	}
        },        
        legend: {
        	layout: 'vertical',
        	align: 'left',
        	verticalAlign: 'middle',
        	borderWidth: 0,
        	useHTML:true
        },      
        series: series
    });
}
app.notesSortByPeriodes=function(notes){
	var notesByPeriodes=[];
	var notesNonClassees=[];
	var periodes=[];
	var periodesTitres=[];
	var k=0;
	for (var i = 0, lng= notes.length ; i <lng; i++) {
		var note=notes[i];
		note.num=i;	
		note.note_matiere=app.getDisciplineById(note.controle_discipline).discipline_name;
		var titre=app.getPeriodeById(note.controle_periode).periode_titre;
		var index=periodesTitres.indexOf(titre);
		if(index<0){
			periodesTitres.push(titre);
			periodes[k]={
				titre:titre,
				notes:[]
			};
			k++;
			index=periodes.length-1;				
		}
		periodes[index].notes.unshift(note);				
	};
	return periodes;
}

app.moyenne=function(notes){
	if(!notes){
		return "-";
	}
	/*
	note={
		note_value:...
		note_bonus:...
		controle_coefficient:...
		controle_note:...
	}
	*/
	var somme_notes=0;
	var somme_max=0;
	for (var i = notes.length - 1; i >= 0; i--) {
		var note=notes[i];
		if(isNaN(note.note_value)){continue;}
		somme_notes=somme_notes*1+(note.note_value*1+note.note_bonus*1)*note.controle_coefficient*1;
		somme_max=somme_max*1+note.controle_note*note.controle_coefficient*1;
	};
	if(somme_max==0){return 0;}
	return Math.round((somme_notes*20/somme_max)*100)/100;
};