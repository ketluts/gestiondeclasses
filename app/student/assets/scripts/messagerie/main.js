/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.publicGetDiscussions=function(){
	clearTimeout(app.discussionTimer);
	if(app.currentMode!="public"){return;}
	$.get(app.serveur + "index.php?go=messagerie&q=getDiscussion&syncId="+app.messagesSyncId+"&render=true&nom_etablissement="+app.nom_etablissement+"&token="+app.token, function(data) {
		app.render(data);

		if(app.destinataires.length==0 && app.messages.length==0){
			document.getElementById('message_add-btn').style.display = "none";
			$('#messagerie').css('display','none');
			$('#header-menu-eleve-messagerie').attr('disabled');
		}
		else{
			document.getElementById('message_add-btn').style.display = "block";
			$('#messagerie').css('display','block');		
			$('#header-menu-eleve-messagerie').removeAttr('disabled');
		}

		app.publicMessagesRender(); 
		if(app.currentDiscussion){
			app.publicRenderDiscussionMessages(app.currentDiscussion);
		} 	 
	}
	);
	app.discussionTimer=setTimeout(app.publicGetDiscussions,app.discussionRefreshTime);
}

app.publicDelDiscussion=function(message_id,confirm,fullname,discussion_num){
	if (!app.checkConnection()) {return;}
	if(!confirm){
		app.alert({title:'Quitter la discussion «'+fullname+'» ?',type:'confirm'},function(){app.publicDelDiscussion(message_id,true,"",discussion_num);});
		return;
	}
	$.post(app.serveur + "index.php?go=messagerie&q=deleteDiscussion&message_id="+message_id+"&nom_etablissement="+app.nom_etablissement+"&token="+app.token,{
		time:Math.floor(app.myTime()/1000)
	}, function(data) {
		var liste=[];
		for (var i = app.messages.length - 1; i >= 0; i--) {
			var message=app.messages[i];
			if(message.message_id==message_id || message.message_parent_message==message_id){
				continue;
			}		
			liste.push(message);
		};
		app.messages=liste;
		app.render(data);  	
		if(app.currentDiscussion==message_id){
			app.publicCloseDiscussion();
		}
		app.publicMessagesRender();
	}
	);
}
app.publicCloseDiscussion=function(){
	document.getElementById('messages-view').style.display = "none";
	app.currentDiscussion=null;
}

app.publicDestinataireAdd=function(destinataire_id,destinataire_type,destinataire_nom){
	//On evite les doublons.
	for (var i =0,lng= app.destinataires.length; i<lng; i++) {
		var destinataire=app.destinataires[i];
		if(destinataire.destinataire_id==destinataire_id && destinataire.destinataire_type==destinataire_type){
			return;
		}
	}
//On ajoute le destinataire.
app.newMessageDestinataires.push({
	destinataire_id:destinataire_id,
	destinataire_type:destinataire_type,
	destinataire_nom:destinataire_nom
});
app.publicDestinatairesRender();
}
app.publicDestinatairesRender=function(){
	var html=[];
	for (var i =0,lng= app.newMessageDestinataires.length; i<lng; i++) {
		var destinataire=app.newMessageDestinataires[i];
		html.push('<span class="well well-sm legende destinataire" style="border-color:#' + app.getColor(destinataire.destinataire_type+'_1337') + ';">');
		html.push(destinataire.destinataire_nom);
		html.push(' <button class="btn btn-xs btn-default pull-right" onclick="app.publicDestinataireDel(\''+destinataire.destinataire_id+'\',\''+destinataire.destinataire_type+'\');">');
		html.push('<span class="glyphicon glyphicon-trash"></span>');
		html.push('</button>');
		html.push('</span>');
	};	
	if(lng==0){
		document.getElementById('destinataires_liste').innerHTML="Aucun destinataire.";
	}else{
		document.getElementById('destinataires_liste').innerHTML=html.join('');
	}
}
app.publicDestinataireDel=function(destinataire_id,destinataire_type){
	var temp_destinataires=[];
	for (var i =0,lng= app.newMessageDestinataires.length; i<lng; i++) {
		var destinataire=app.newMessageDestinataires[i];

		if(destinataire.destinataire_id!=destinataire_id || destinataire.destinataire_type!=destinataire_type){
			temp_destinataires.push(destinataire);
		}
	}
	app.newMessageDestinataires=temp_destinataires;
	app.publicDestinatairesRender();
}
app.publicAnswerAdd=function(){
	if (!app.checkConnection()) {return;}	
	var message_content=app.trim(document.getElementById('answer-text').value);
	if(message_content==""){
		app.alert({title:"Il faut entrer un message."});
		$('#answer_send_btn').button('reset');  
		return;
	}
	clearTimeout(app.discussionTimer);
	$.post(app.serveur + "index.php?go=messagerie&q=addAnswer&syncId="+app.messagesSyncId+"&nom_etablissement="+app.nom_etablissement+"&token="+app.token,{
		message_content:message_content,
		message_parent_message: app.currentDiscussion,
		time:Math.floor(app.myTime()/1000)
	}, function(data) {
		app.render(data);   
		$('#answer_send_btn').button('reset');  
		document.getElementById('answer-text').value="";
		app.publicRenderDiscussionMessages(app.currentDiscussion);		
		app.discussionTimer=setTimeout(app.publicGetDiscussions,app.discussionRefreshTime);
	}
	);
}
app.publicRenderParticipants=function(){
	for (var i =0,lng= app.messages.length ; i <lng; i++) {
		var message=app.messages[i];
		if(message.message_parent_message!="-1" && message.message_subject!="~lock"){
			document.getElementById('message_date_'+message.message_parent_message).innerHTML=moment(parseInt(message.message_date)*1000).format('DD MMM à HH[h]mm');
			$('#message_date_'+message.message_parent_message).attr('data-order',parseInt(message.message_date)*1000);
		}
		var html=[];
		if(message.state==false){
			if(message.message_parent_message=="-1"){
				document.getElementById('message_subject_'+message.message_id+'').style.fontWeight ="bold";
			}else{
				if(document.getElementById('message_subject_'+message.message_parent_message+'')){
					document.getElementById('message_subject_'+message.message_parent_message+'').style.fontWeight ="bold";
				}
				else{
					message.state=true;
				}
			}			
		}
		if(message.message_parent_message=="-1"){
			var author='Moi';
			for (var j = message.participants.length - 1; j >= 0; j--) {
				var participant=message.participants[j];
				if(participant.user_pseudo){
					author=ucfirst(participant.user_pseudo);
				}
				else{
					participant.eleve_prenom=participant.eleve_prenom||" ";
					participant.eleve_nom=participant.eleve_nom||" ";
					author=ucfirst(participant.eleve_prenom)+" "+participant.eleve_nom.toUpperCase();
				}
				html.push(author);
			};
			html=html.join(', ');
			document.getElementById('message_'+message.message_id+'_participants').title=html;
			if(html.length>40){
				html=html.substr(0,37)+"...";
			}
			document.getElementById('message_'+message.message_id+'_participants').innerHTML=html;
		}
	};
	$('#messages-table').DataTable({
		stateSave: true,
		language:app.datatableFR,
		"order":[[3,'desc']]
	});
}
app.publicDiscussionAdd=function(){
	if (!app.checkConnection()) {return;}
	if(app.newMessageDestinataires.length==0){
		app.alert({title:"Il faut sélectionner des participants."});
		app.publicDestinatairesForm();
		$('#message_send_btn').button('reset');  
		return;
	}	
	var message_subject=app.trim(document.getElementById('message_subject').value);
	var message_content=app.trim(document.getElementById('message_content').value);
	if(message_subject=="" || message_content==""){
		app.alert({title:"Il faut entrer un sujet et un message."});
		$('#message_send_btn').button('reset');  
		return;
	}
	clearTimeout(app.discussionTimer);
	$.post(app.serveur + "index.php?go=messagerie&q=addDiscussion&syncId="+app.messagesSyncId+"&nom_etablissement="+app.nom_etablissement+"&token="+app.token,{
		message_subject:message_subject,
		message_content:message_content,
		message_destinataires: JSON.stringify(app.newMessageDestinataires),
		time:Math.floor(app.myTime()/1000)
	}, function(data) {
		app.render(data);   
		app.hide('message_form');
		app.publicMessagesRender();		
		app.discussionTimer=setTimeout(app.publicGetDiscussions,app.discussionRefreshTime);
	}
	);
}
app.publicMessagesSort=function(){
	app.messagesSort=(app.messagesSort*1+1)%2;
	app.publicRenderDiscussion(app.currentDiscussion);
	app.publicRenderDiscussionMessages();
}
app.setMessagesSyncId=function(){
	var messages=app.messages;
	var syncId=0;
	var n=messages.length;
	for (var i = 0, lng=n ; i<lng; i++) {
		var message=messages[i];
		if(message.message_id*1>syncId*1){
			syncId=message.message_id;
		}
	}
	app.messagesSyncId=syncId;
}
app.closeDiscussion=function(){
	$('#messages-view').css('display','none');
	app.currentDiscussion=null;
}