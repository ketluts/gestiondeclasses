/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.trim=function(myString)
{
  return myString.replace(/^\s+/g,'').replace(/\s+$/g,'');
} 
function dump(obj) {
  var out = '';
  for (var i in obj) {
    if(typeof obj[i]===typeof {}){
      out+=dump(obj[i])+ "\n";
    }else{
     out += i + ": " + obj[i] + "\n";
   }
 }
 return out;   
}
function hashCode(str) { 
  str += '00000';
  str=hex_md5(str);
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }

  return Math.abs(hash);
}
function intToRGB(i) {
  var color= ((i >> 24) & 0xFF).toString(16) +
  ((i >> 16) & 0xFF).toString(16) +
  ((i >> 8) & 0xFF).toString(16);
  while(color.length<6){
    color="0"+color;
  }
  return color;
}
function arrayUnique(array) {
  var a = array.concat();
  for(var i=0; i<a.length; ++i) {
    for(var j=i+1; j<a.length; ++j) {
      if(a[i] === a[j])
        a.splice(j--, 1);
    }
  }
  return a;
} 
function ucfirst(str)
{
  if(str=="" || !str){return "";}
  return str.charAt(0).toUpperCase() + str.substr(1);
}

app.orderBy=function(array,orderBy,order){
  app.orderByParam=orderBy;
  if(order=="ASC"){
    return array.sort(function(a,b){
      if(a[app.orderByParam]>b[app.orderByParam]) return 1;
      if(a[app.orderByParam]<b[app.orderByParam]) return -1;
      return 0;
    });
  }
  return array.sort(function(a,b){
    if(a[app.orderByParam]>b[app.orderByParam]) return -1;
    if(a[app.orderByParam]<b[app.orderByParam]) return 1;
    return 0;
  });
}
/**
* Convert number of bytes into human readable format
*
* @param integer bytes     Number of bytes to convert
* @param integer precision Number of digits after the decimal separator
* @return string
*/
function bytesToSize(bytes, precision)
{  
  var kilobyte = 1024;
  var megabyte = kilobyte * 1024;
  var gigabyte = megabyte * 1024;
  var terabyte = gigabyte * 1024;
  if ((bytes >= 0) && (bytes < kilobyte)) {
    return bytes + ' B';
  } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
    return (bytes / kilobyte).toFixed(precision) + ' KB';
  } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
    return (bytes / megabyte).toFixed(precision) + ' MB';
  } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
    return (bytes / gigabyte).toFixed(precision) + ' GB';
  } else if (bytes >= terabyte) {
    return (bytes / terabyte).toFixed(precision) + ' TB';
  } else {
    return bytes + ' B';
  }
}
app.add=function(a,b) {
  return a*1 + b*1;
};   

app.renderYearsOld=function(birthday){
  if(!birthday){return 0;}
  var today = new Date();
  var tab= birthday.split('/');
  var birthDate = new Date(tab[2], tab[1]-1,tab[0]);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
}