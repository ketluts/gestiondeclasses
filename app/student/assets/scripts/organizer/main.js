/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.publicOrganizerInit=function(){
  $('#agenda').fullCalendar({
    lang: 'fr',
    views: {
      month: { 
        titleFormat: 'MM/YYYY'
      },
      day: { 
        titleFormat: 'D/MM/YYYY'
      }
    },  
    timezone:'local',
    nowIndicator:true,
    height:'auto',
    defaultTimedEventDuration:"01:00:00",
    slotEventOverlap:false,
    defaultView: "basicWeek",
    allDaySlot: true,
    slotLabelFormat:"HH:mm",
    firstDay: 1,
    weekends:false,
    timeFormat: 'HH:mm',    
    eventOrder:'event_order',  
    eventBackgroundColor:"#ffffff",
    eventBorderColor:"#E3E3E3",
    eventTextColor:"black",
    scrollTime:"07:30:00",
    columnFormat:'ddd DD/MM/YY',
    views: {
      basicThreeDay: {
        type: 'basic',
        duration: { days: 3 },
        buttonText: '3 jours',
        columnFormat:'ddd DD/MM/YY'
      },
      month: {
        columnFormat:'ddd'
      }
    },
    header: {
      left: '',
      center: '',
      right: ''
    }, 
    eventClick: function(event, jsEvent, view) {
      app.agendaOneEventDetailsToggle(event.event_id);
    },
    eventAfterAllRender:function(){
      app.notesEtoilesRender();   
    },         
    viewRender:function( view, element ){
      var format="";
      if(view.name=="month"){
        format="MMMM YYYY";
        document.getElementById('agenda-date').innerHTML= $('#agenda').fullCalendar( 'getDate' ).format(format);
      }
      else if(view.name=="basicThreeDay"){
        format="DD/MM/YYYY";
        document.getElementById('agenda-date').innerHTML= $('#agenda').fullCalendar( 'getDate' ).add({day:1}).format(format);
      }
      else{
        format="DD/MM/YYYY";
        document.getElementById('agenda-date').innerHTML= $('#agenda').fullCalendar( 'getDate' ).format(format);
      }
      document.getElementById('agenda-semaine').innerHTML =app.getWeekName(  $('#agenda').fullCalendar( 'getDate' ).unix()*1000);
      
    }
  });
}
app.agendaGoToday=function(){
  var view=app.currentView;
  var agendaView= $('#'+view+'_agenda').fullCalendar( 'getView');
  $('#'+view+'_agenda').fullCalendar( 'today');
  if(agendaView.name=="basicThreeDay"){
    $('#'+view+'_agenda').fullCalendar( 'incrementDate', { days:-1});
  } 
}
app.agendaEventDetailsToggle=function(value,enableSave){
  if(value){
    $(".agenda-events-details-btn").removeClass('btn-default').addClass('btn-primary');
    $(".event-data").css('display','block');
  }
  else{
    $(".agenda-events-details-btn").removeClass('btn-primary').addClass('btn-default');
    $(".event-data").css('display','none');
  }
  app.userConfig.agendaEventDetails=value;
  if(enableSave!==false){    
    app.pushUserConfig();
  }
}
app.agendaOneEventDetailsToggle=function(event_id){
  var div=$('#event-data-'+event_id);
  if(div.css('display')=='block'){  
    div.css('display','none');
  }
  else{
   div.css('display','block');
 }
}
app.organizerEventTitleRender=function(title){
  return title.replace(/\*/g,"");
}
