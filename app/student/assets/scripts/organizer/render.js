/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.publicOrganizerRender=function(){
  var events=app.agenda;
  $('#agenda').fullCalendar( 'removeEventSource',  app.eventsEventsSource );
  app.eventsEventsSource=[];
  for (var i =0, lng= events.length; i <lng; i++) {
    var event=events[i];  
    var user=app.getUserById(event.event_user);

if(event.event_discipline!=-1){
  var matiere=app.getDisciplineById(event.event_discipline).discipline_name;
  app.legendsUsers.push(matiere);
  user=matiere;
}
else{
  user=app.renderUserName(user);
  app.legendsUsers.push(user);
}

    

    var icon="";
    if(event.event_icon){
      icon='<span class="glyphicon '+event.event_icon+'"></span>';
    }
    var title=ucfirst(app.organizerEventTitleRender(event.event_title));
    var order=1;
    if(event.event_icon=="glyphicon-home"){
      order=1;  
      if(title==""){
        title="Devoirs";
      }
    }
    else if(event.event_icon=="glyphicon-blackboard"){
      order=2;
    }
    else if(event.event_icon=="glyphicon-comment"){
      order=3;
    }
    else{
      order=4;
    }
    if(event.event_data==""){
      event.event_data="&nbsp;";
    }
    var eventDataStyle="display:block;";
    if(event.event_icon=="glyphicon-blackboard"){
      eventDataStyle="display:none;";
    }
    var oEvent={
      title: '<div class="calendar-event-title">'+icon+' '+user+'<br/>'+app.organizerEventTitleRender(event.event_title)+'</div><span class="event-data" id="event-data-'+event.event_id+'" style="'+eventDataStyle+'">'+event.event_data+'</span>',  
      allDay:event.event_allDay,
      event_id:event.event_id,
      start: event.event_start*1000,        
      end: event.event_end*1000,    
      borderColor:"#" + app.getColor(user),
      event_order:order
    };
    if(event.event_icon=="glyphicon-blackboard"){
      oEvent.backgroundColor="#ffffff";
      event.start+=1;
    }
    else if(event.event_icon=="glyphicon-home"){
      oEvent.backgroundColor="#cbcbcb";
    }
    else{
      oEvent.backgroundColor="#b8b8b8";
    }
    app.eventsEventsSource.push(oEvent);
  };
  $('#agenda').fullCalendar( 'addEventSource',  app.eventsEventsSource );
  app.renderLegendsUsers('eleve_calendar_legende');
}