/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

 (function($) {
  $.fn.goTo = function() {
   $('html, body').animate({
     scrollTop: ($(this).offset().top*1-60) + 'px'
   }, 'fast');
   return this; 
 }
})(jQuery);
app.go=function(hash){
  window.onhashchange=null;
  window.location.hash=hash;
  app.navigate();
  window.onhashchange=function(){
   app.navigate();
 }
}
app.navigationInit=function(){
  app.urlHash=window.location.hash.split('#')[1];
  window.onhashchange=function(){
    app.navigate();
  }
}
app.navigate=function(hash){
 hash=hash||window.location.hash.split('#')[1];
 if(!hash){
  return;
}
var dir=hash.split('/');
switch(dir[0]){

default:
break;
}
}