/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.render=function(data) {
  var data = jsonParse(data); 
  /*---------------
-----------------
------APP-------
-----------------
---------------*/ 
if (data['info'] && data['info'] != "") {
  app.renderNoty(data['info']);
}
if(data['statut']==false){
 app.deconnexion();
 return;
}
if(data['alertes']){
 for (var i = data['alertes'].length - 1; i >= 0; i--) {
  app.alert({title:data['alertes'][i]});
}
}

if (data['titre']) {
  document.getElementById('titre').innerHTML = data['titre'];
}
 /*---------------
-----------------
------CONFIG------
-----------------
---------------*/
if(data['options']){
  app.weeks=data['options']['weeks'];
  app.serveurOptions=data['options'];
 for (var i = app.plugins.length - 1; i >= 0; i--) {
    var plugin= app.plugins[i];
    if(app.serveurOptions.plugins && app.serveurOptions.plugins.indexOf(plugin.name)>=0){
      plugin.enabled=true;
    }
   } 
}
if (data['legends']) {
  app.setLegends(data['legends']);
}
if (data['periodes']) {
  app.periodes = data['periodes'];
  app.active_periode=null;
  
  for (var i = app.periodes.length - 1; i >= 0; i--) {
    if(app.periodes[i].periode_active==1){
      app.active_periode=i;        
    }
  };
}
if (data['disciplines']) {
 app.disciplines = data['disciplines'];
}
/*---------------
-----------------
------CONNEXION------
-----------------
---------------*/
if (data['users']) {
  app.users=data['users'];
  app.buildUsersIndex();
}        
if (data['etablissementsLST']) {
  app.connexionFormEtablissementAutocompletion(data['etablissementsLST']);
}   
/*---------------
-----------------
-----------
-----------------
---------------*/ 
if (data['public']) {
 app.publicDataInit(data['public']); 
}
return data;   
};
app.publicDataInit=function(data){
app.files=data['files']||app.files;
  app.links=data['links']||app.links;
  app.agenda=data['agenda']||app.agenda;
  app.etoiles=data['etoiles']||app.etoiles;
  app.notes=data['notes']||app.notes;
  app.relations=data['relations']||app.relations;
  app.progressions=data['progressions']||app.progressions;
  app.classes=data['classes']||app.classes;
  app.testIntelligences=data['testIntelligences']||false;
  if (data['messages']) {
    app.messages=app.messages.concat(data['messages']);
    app.setMessagesSyncId();
  }
  app.destinataires=data['destinataires']||app.destinataires;
  app.token=data['token']||app.token;
  app.eleve_id=data['eleve_id']||app.eleve_id;
  app.eleve=data['eleve']||app.eleve;
  app.publicBuiltItemsListe();
  app.itemsStudentProgressionsBuild();
  if(data['render']){
    app.renderPublicView();
  }  
  $('#btn-reload').button('reset');    
}