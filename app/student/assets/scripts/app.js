/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
/*******/
/* Run */
/*******/
$(document).ready(function() {
  app.timerPing=setTimeout(app.ping, 0);
  app.init();
  app.navigationInit(); 
  app.connexionRender();
  $.get(app.serveur + "index.php?go=school&q=getAll", function(data) {
    app.render(data);         
  }
  );  
});

app.renderPublicView=function(){
  app.currentMode="public";
  app.viewClear();
  app.currentView="public";
  app.legendsUsers=[];
  app.discussionTimer=null;
  app.discussionRefreshTime=10000;
  $(".template_public").css("display","block");
  document.getElementById('header').style.display = "";
  document.getElementById('messages-view').style.display = "none";
  document.getElementById('message_form').style.display = "none";
  $('#home-don-btn').css('display','none');
  document.getElementById('classe').style.display = "none";
  document.getElementById('titre').innerHTML = "GestionDeClasses <img src='assets/favicons/apple-touch-icon-57x57.png' width='40px'>";
  $("#header-menu-eleve").css('display','block');
  $("#header-menu-prof").css('display','none');
if(app.hebergeur.mail!=""){
$('#footer-hebergeur').html(' | Hébergé par <a href="mailto:'+app.hebergeur.mail+'">'+app.hebergeur.nom+'</a>');
}
$('#footer-version').html(app.version);


  document.getElementById('eleve-monster').innerHTML='<img src="'+app.serveur+'/lib/monsterid/monsterid.php?seed='+app.eleve.eleve_id+'&size=100" />';
  document.getElementById('eleve_name').innerHTML=app.eleve.eleve_prenom+' <br/>'+app.eleve.eleve_nom;
  document.getElementById('eleve_statut').innerHTML="";
  if(app.eleve.eleve_statut && app.eleve.eleve_statut=="delegue"){
    document.getElementById('eleve_statut').innerHTML="<span class='glyphicon flaticon-medal45'></span> Délégué(e)";
  }
  var classes=[];
  for (var i = app.classes.length - 1; i >= 0; i--) {
    classes.push(app.classes[i].classe_nom);
  };
  document.getElementById('eleve_classes').innerHTML=classes.join(' - ');
  document.getElementById('eleve_nb_etoiles').innerHTML=app.eleve.nb_etoiles;
  document.getElementById('agenda-semaine').innerHTML =app.getWeekName();
  app.publicSociogrammeIniSelect();
  //**********
  // Intelligences
  //********* 
  app.intelligencesInit();
  //**********
  // Skills
  //*********
  app.skillsInit();
  //**********
  // Others
  //*********
  app.publicOrganizerInit();
  app.publicOrganizerRender(); 
  app.publicRenderNotes();

  $('#agenda').fullCalendar( 'removeEventSource',  app.shareEventsSource  );
  app.shareEventsSource=[];
  app.renderPublicFiles();
  app.renderPublicLinks();  
  $('#agenda').fullCalendar( 'addEventSource', app.shareEventsSource );

  app.publicGetDiscussions();
  app.publicRenderEtoiles();

}

app.ping=function() {
 clearTimeout(app.timerPing);
 $.ajax({type: "HEAD",cache:false,url: app.serveur + "ping.php",
  timeout: 10000
})
 .done(function(data) {
  if (!app.isConnected) {
    app.isConnected = true;      
    $(".connexion-requise").prop("disabled", false);
  }
}
).fail(
function() {
  app.isConnected = false;
  $(".connexion-requise").prop("disabled", true);
}
);
app.timerPing=setTimeout(app.ping, 15000);
};

app.pluginsLauncher=function(callback){
  var plugins=app.plugins;
  for (var i = plugins.length - 1; i >= 0; i--) {
   var plugin=plugins[i];
   if(!plugin.enabled){continue;}
   if(plugin[callback]){
    (plugin[callback])();
  }
}
}