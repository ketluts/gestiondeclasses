/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
//###########
//Intelligences multiples
app.intelligencesInit=function(){
	if(app.testIntelligences){
		if(!app.eleve.eleve_intelligences){
			app.intelligencesResults=[];
		}
		else{
			app.intelligencesResults=jsonParse(app.eleve.eleve_intelligences);
		} 
		app.intelligencesTestResultsRender();
		app.uiShow(['intelligences_test_btn_start','intelligences'],[],"inline-block");
	}
	else{
		app.uiHide(['intelligences'],[],"");
	}
}
app.intelligences=[
{
	name:"Verbale et linguistique",
	questions:[
	"J’exprime mes idées facilement.",
	"J’aime parler au téléphone.",
	"J’écoute généralement avec attention les paroles de chansons.",
	"J’aime écouter les histoires.",
	"J’aime les jeux comme les mots croisés.",
	"J’aime visiter la bibliothèque ou la librairie.",
	"Je préfère lire plutôt que regarder la télévision.",
	"J’aime écouter la radio.",
	"Je m’arrête pour lire les affiches et les messages affichés aux babillards.",
	"On me dit souvent que j’exprime bien mes idées."
	]
},
{
	name:"Spatiale et visuelle",
	questions:[
	"J’aime écrire ou dessiner quand je suis au téléphone.",
	"Écrire ou dessiner est un passe-temps pour moi.",
	"J’ai un bon sens de l’orientation.",
	"Quand je lis, il me vient plein d’images dans la tête.",
	"Je cherche à obtenir différentes couleurs quand je joue avec les tons et les ombres.",
	"J’aime la géométrie.",
	"J’aime faire des dessins, des jeux de labyrinthe ou d’autres casse-tête visuels.",
	"J’aime créer des bandes dessinées."
	]
},
{
	name:"Corporelle et kinesthésique",
	questions:[
	"J’aime bouger.",
	"J’aime les activités qui me permettent de bouger en classe.",
	"Je suis bon ou bonne dans la plupart des sports.",
	"J’aime utiliser des outils pour fabriquer des choses.",
	"J’aime comprendre le fonctionnement des choses ou des mécanismes.",
	"J’aime mieux montrer à quelqu’un comment faire quelque chose que le lui expliquer.",
	"Je démonte et parfois remonte des objets et des mécanismes."
	]
},
{
	name:"Naturaliste",
	questions:[
	"J’ai du plaisir à observer les différences dans l’environnement.",
	"J’apprends beaucoup au contact de la nature.",
	"J’aime observer les animaux.",
	"Je fais souvent de bonnes prédictions météorologiques.",
	"Je peux nommer différents insectes et animaux.",
	"Je m’intéresse à l’espace ou aux phénomènes naturels.",
	"J’aime collectionner des roches, des coquillages ou autres.",
	"Je me soucie de l’environnement.",
	"J’aime faire des classifications."
	]
},
{
	name:"Intra-personnelle",
	questions:[
	"On me dit souvent que je suis tranquille et calme.",
	"Je suis curieux ou curieuse de savoir ce que les autres pensent de moi.",
	"Je connais mes forces et mes faiblesses.",
	"Je connais la raison de mes sentiments et de mes opinions.",
	"On me dit souvent que je suis indépendant ou indépendante, ou que je suis solitaire.",
	"J’aime résoudre mes propres problèmes.",
	"Je passe de longs moments à réfléchir."
	]
},
{
	name:"Logico-mathématique",
	questions:[
	"Résoudre des problèmes est facile pour moi.",
	"J’aime faire des classifications.",
	"Je peux facilement faire des additions, des soustractions, des multiplications et des divisions dans ma tête.",
	"Je trouve facile de comprendre un graphique ou un tableau.",
	"J’aime jouer aux échecs et aux dames.",
	"J’aime m’exercer à estimer.",
	"Je cherche à faire les choses selon un ordre logique."
	]
},
{
	name:"Inter-personnelle",
	questions:[
	"J’aime parler aux gens.",
	"Je me sens responsable de mes amis/es lorsque je suis avec eux et elles.",
	"Mes amis/es me demandent souvent conseil.",
	"Je préfère les sports d’équipe aux sports individuels.",
	"J’aime retrouver mes amis/es dans mes temps libres.",
	"Je travaille mieux dans une équipe que tout seul ou toute seule.",
	"Je fais souvent part de mes problèmes à mes amis/es."
	]
},
{
	name:"Musicale et rythmique",
	questions:[
	"Je fredonne souvent en travaillant ou en marchant.",
	"J’aime composer des chansons.",
	"J’aime les cours de musique à l’école ou ceux que je prends à l’extérieur de l’école.",
	"On me dit souvent que j’ai une belle voix.",
	"J’écoute souvent de la musique dans mes temps libres.",
	"Je retiens les mélodies que j’entends.",
	"C’est facile pour moi de marquer le rythme de diverses chansons."
	]
}
];
app.intelligencesTestStart=function(){
	app.intelligencesResultsTemp=[];
	app.uiHide(['intelligences_test_btn_start']);
	app.uiShow(['intelligences_test']);
	var questions=[];
	for (var i = 7; i >= 0; i--) {
		app.intelligences[i].questions.sort(function(){ return Math.round(Math.random())});
		for (var j = 6; j >= 0; j--) {
			var question={
				num:i,
				text:app.intelligences[i].questions[j]
			};
			questions.push(question);
		}
	}
	app.intelligencesQuestions=questions.sort(function(){ return Math.round(Math.random())});
	app.intelligencesTestQuestionsRender(0);
}
app.intelligencesTestQuestionsFinish=function(){
	if(app.intelligencesResultsTemp.length>0){
		app.intelligencesResults=app.intelligencesResultsTemp;
		$.post(app.serveur + "index.php?go=intelligences&q=updateIntelligences&nom_etablissement="+app.nom_etablissement+"&token="+app.token,{
			resultats:JSON.stringify(app.intelligencesResults),
			time:Math.floor(app.myTime()/1000)
		}, function(data) {
			app.render(data);   
			
		}
		);
	}
	app.uiHide(['intelligences_test']);
	app.uiShow(['intelligences_test_btn_start'],[],"inline-block");
	app.intelligencesTestResultsRender();
	$('#intelligences_results').goTo();
}
app.intelligencesTestCancel=function(){
	app.uiHide(['intelligences_test']);
	app.uiShow(['intelligences_test_btn_start'],[],"inline-block");
}
app.intelligencesTestQuestionsRender=function(page){
	var html=[];
	var i=page*7;
	html.push('<hr/>');
	html.push('<h4>');
	html.push('Clique sur <button class="btn btn-default btn-xs"><span class="glyphicon glyphicon-heart"></span></button> si les phrases te correspondent.');
	html.push('</h4>');
	for (var i = page*7; i<page*7+7; i++) {
		html.push('<div class="flex-rows intelligences-test-row">');
		html.push('<div class="flex-3">');
		html.push(app.intelligencesQuestions[i].text);
		html.push('</div>');
		html.push('<div class="flex-2 text-right">');
		html.push('<button class="btn btn-default" onclick="app.intelligencesTestAddAnswer('+app.intelligencesQuestions[i].num+');$(this).addClass(\'btn-primary\').prop(\'disabled\', true);"><span class="glyphicon glyphicon-heart"></span></button>');
		html.push('</div>');
		html.push('</div>');
	}
	html.push('<div class="flex-rows" id="intelligences_test_footer">');

	html.push('<div class="flex-1 text-left">');
	html.push('<button class="btn btn-default" onclick="app.intelligencesTestCancel();">Annuler</button>');
	html.push('</div>');
	html.push('<div class="flex-1 text-center">');
	if(page!=7){
		html.push('<h4>Étape '+(page+1)+'<small>/8</small></h4>');
	}else{
		html.push('<button class="btn btn-primary" onclick="app.intelligencesTestQuestionsFinish();">Terminer</button>');	
	}

	html.push('</div>');
	html.push('<div class="flex-1 text-right">');
	if(page!=7){
		html.push('<button class="btn btn-primary" onclick="app.intelligencesTestQuestionsRender('+(page+1)+');">Suivant</button>');
	}
	html.push('</div>');
	html.push('</div>');
	document.getElementById('intelligences_test').innerHTML=html.join('');
}
app.intelligencesTestAddAnswer=function(num){
	if(!app.intelligencesResultsTemp[num]){
		app.intelligencesResultsTemp[num]=0;
	}
	app.intelligencesResultsTemp[num]++;
}
app.intelligencesTestResultsRender=function(){
	var categories=[];
	var data=[];
	for (var i =0; i<8; i++) {
		categories.push(app.intelligences[i].name);
		var result=app.intelligencesResults[i];
		if(!result){
			result=0;
		}
		data.push(result);
	}
	$('#intelligences_results').highcharts({
		chart: {
			polar: true,
			type: 'line'
		},

		title: {
			text: '',
			style:"display:none"
		},

		pane: {
			size: '80%'
		},

		xAxis: {
			categories: categories,
			tickmarkPlacement: 'on',
			lineWidth: 0
		},

		yAxis: {
			gridLineInterpolation: 'polygon',
			lineWidth: 0,
			min: 0
		},
		series: [{
			name: 'Mon profil',
			data: data,
			pointPlacement: 'on'
		}
		]
	});
}
