/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.viewClear=function() {
  app.currentView=null;
  app.legendsUsers=[];  
  $(".template").css('display', 'none'); 
  document.getElementById('header').style.display = "";
  app.discussionRefreshTime=60000;
};
app.show=function(id,display){
  display=display||"block";
  document.getElementById(id).style.display=display;
}
app.hide=function(id){
  document.getElementById(id).style.display="none";
}
app.viewToggle=function(id,display){
  display=display||"block";
  var elem=document.getElementById(id);
  if(elem.style.display!="none"){
    elem.style.display="none";
    return false;
  }else{
    elem.style.display=display;
    return true;
  }
}
app.uiShow=function(ids,classes,display){
  ids=ids||[];
  classes=classes||[];
  display=display||"block";
  for (var i = classes.length - 1; i >= 0; i--) {  
   $("."+classes[i]).css('display',display);
 }
 for (var i = ids.length - 1; i >= 0; i--) {  
  $("#"+ids[i]).css('display',display);
}
}
app.uiHide=function(ids,classes,display){
 ids=ids||[];
 classes=classes||[];
 display=display||"none";
 for (var i = classes.length - 1; i >= 0; i--) {  
   $("."+classes[i]).css('display',display);
 }
 for (var i =0, lng=ids.length; i<lng; i++) {  
   $("#"+ids[i]).css('display',display);
}
}
app.uiClean=function(ids,classes,html){
 ids=ids||[];
 classes=classes||[];
 html=html||"";
 for (var i = classes.length - 1; i >= 0; i--) {  
   $("."+classes[i]).html(html);
 }
 for (var i = ids.length - 1; i >= 0; i--) {  
   document.getElementById(ids[i]).innerHTML=html;
 }
}
app.renderNoty=function(infos) {
  var html = [];
  for (var i = 0, lng = infos.length; i < lng; i++) {
   var n = noty({
    layout: 'top', //topRight
    theme: 'defaultTheme', // or 'relax'
    type: infos[i][1],
    text: infos[i][0], // can be html or string
    dismissQueue: true, // If you want to use queue feature set this true
    template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
    animation: {
        open: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceInLeft'
        close: {height: 'toggle'}, // or Animate.css class names like: 'animated bounceOutLeft'
        easing: 'swing',
        speed: 200 // opening & closing animation speed
      },
    timeout: infos[i][2]||5000, // delay for closing event. Set false for sticky notifications
    force: true, // adds notification to the beginning of queue when set to true
    modal: false,
    maxVisible: 5, // you can set max visible notification for dismissQueue true option,
    killer: false, // for close all notifications before show
    closeWith: ['click'], // ['click', 'button', 'hover', 'backdrop'] // backdrop click will close all notifications
    callback: {
      onShow: function() {},
      afterShow: function() {},
      onClose: function() {},
      afterClose: function() {},
      onCloseClick: function() {},
    },
    buttons: false // an array of buttons
  }
  );
 }
 document.getElementById('infos').innerHTML += html.join('');
};
app.closeInfo=function(id){
 if(document.getElementById('info_'+id+'')){
  document.getElementById('info_'+id+'').style.display = "none";
}
};
app.alert=function(swal_options,callback,fallback){
  callback=callback||function(){};
  fallback=fallback||function(){};
  swal_options.cancelButtonText="Annuler";
  swal_options.confirmButtonText=swal_options.confirmButtonText||'Valider';
  swal_options.confirmButtonColor="#428BCA";
  if(swal_options.type=='confirm'){
   swal_options.showCancelButton=true;    
 }
 swal_options.type='warning';
 swal_options.html=true;
 swal(swal_options, 
  function(isConfirm){
    if(isConfirm){
      (callback)();
    }else{
     (fallback)();
   }   
 });
};
app.renderEtoiles=function(nb_etoiles,max,used){
  var html=[];
  var used=used||0;
  var max=max||nb_etoiles;
  for (var i = 0; i<max; i++) {
    if(i<used){
      html.push('<img class="etoile-icon" src="assets/img/etoile_use.png" width="20"/>');
    }
    else if(i<nb_etoiles){
      html.push('<img class="etoile-icon" src="assets/img/etoile_on.png" width="20"/>');
    }
    else{
      html.push('<img class="etoile-icon" src="assets/img/etoile_off.png" width="20"/>');
    }
  };
  return html.join('');
};
app.renderLoader=function(){
  return '<div id="renderLoader"><img src="assets/img/ajax-loader.gif" width="50" title="Chargement en cours..."/></br></br><span class="h4">Chargement en cours...</span></div>';
}
app.pluralize=function(n,str,sing){
  sing=sing||"";
  if(n>1)
    return str;
  return sing;
}
app.feminize=function(genre,str){
  if(genre=="F")
    return str;
  return "";
}
app.colorByMoyenne=function(m){
  if(m<=10){
    var r="FF";
    var v=Math.round(153*(m/10)).toString(16);
    var b="00";

  }else{
    m=m-10;
    var r=Math.round((1-m/10)*255).toString(16);
    var v="99";
    var b="00";
  }
  if(r.length<2){r='0'+r;}
  if(v.length<2){v='0'+v;}
  if(b.length<2){b='0'+b;}
  return "#"+r+v+b;
};
app.getColorAffine=function(value,colors){
  colors=JSON.parse(colors);
  if(value>colors.length-1){
    return "#ffffff";
  }
  var n=Math.floor(value);
  if(value==n){
    return colors[value];
  }  
  var colorA=[parseInt(colors[n].slice(1,3),16),parseInt(colors[n].slice(3,5),16),parseInt(colors[n].slice(5,7),16)];
  var colorB=[parseInt(colors[n+1].slice(1,3),16),parseInt(colors[n+1].slice(3,5),16),parseInt(colors[n+1].slice(5,7),16)];
  var r=Math.floor(colorB[0]*(value-n)+colorA[0]*(1-value+n*1)).toString(16);
  var v=Math.floor(colorB[1]*(value-n)+colorA[1]*(1-value+n*1)).toString(16);
  var b=Math.floor(colorB[2]*(value-n)+colorA[2]*(1-value+n*1)).toString(16);
  if(r.length<2){r="0"+r;}
  if(v.length<2){v="0"+r;}
  if(b.length<2){b="0"+r;}
  return "#"+r+v+b;   
}
app.getColor=function(texte){
  if(texte==""){
    return "ffffff";
  }
  if(!app.cacheColor[''+texte+'']) {
    app.cacheColor[''+texte+'']=intToRGB(hashCode(texte));
  }
  return app.cacheColor[''+texte+''];
};
app.renderLegendsUsers=function(div_id){
  app.legendsUsers.sort(function(a,b){
   return a.localeCompare(b);
 });
  var lng=app.legendsUsers.length;
  var legendeHtml=[];
  var eventUser = "";
  if(lng==0){
    legendeHtml.push("...");
  }
  else{
   for (var i =0; i<lng; i++) {
    var legend=app.legendsUsers[i];
    if(eventUser.indexOf("~"+legend+"~")<0){
      legendeHtml.push("<div class='well well-sm legende' style='border-color:#" + app.getColor(legend) + ";'>" + ucfirst(legend) + "</div>");
    }
    eventUser += "~"+legend+"~";
  };
  if(legendeHtml==""){
    legendeHtml.push("...");
  }
}
document.getElementById(""+div_id+"").style.display="block";
document.getElementById(""+div_id+"").innerHTML = legendeHtml.join('');
};