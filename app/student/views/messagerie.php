<!-- TEMPLATE MESSAGERIE -->
<div class="box template template_public col-md-12" id="messagerie">
					<span class="btn btn-default btn-close" onclick="$('#app').goTo();">
			<span class="glyphicon glyphicon-chevron-up"></span>
		</span>
	<h2>Mes messages</h2>
	<div class="btn-toolbar col-sm-12  text-center" role="toolbar">            
		<div class="btn-group" style="float:left;">     
			<button class="btn btn-default" id="message_add-btn" onclick="app.publicRenderMessageForm();"><span class="glyphicon glyphicon-plus"></span> Nouvelle discussion</button>
		</div>    
	</div>
	<div id="message_form" style="display:none;" class="col-sm-12 well well-sm">
		<div  class="col-sm-8">
			<div id="message_destinataires">
				<div id="destinataires_liste" class="well well-sm"></div>
			</div>
			<input type="text" class="form-control"  id="message_subject" placeholder="Sujet" />
			<textarea type="text" class="form-control"  id="message_content" placeholder="Votre message" ></textarea> 
		</div>
		<div id="destinataires_select"  class="col-sm-4">
		</div>
		<div class="col-sm-12 text-center">
			<br/>
			<div class="btn-group">
				<button class="btn btn-default" onclick="app.hide('message_form');">Annuler</button>
				<button id="message_send_btn" class="btn btn-primary connexion-requise" onclick="$(this).button('loading');app.publicDiscussionAdd();" data-loading-text="Envoie en cours...">Envoyer</button>
			</div>
		</div>
	</div>
	<div class="col-sm-12 messages_tab">
		<div id="messages-liste">
		</div>
		<div id="messages-view">
			<div class="pull-right"><button class="btn btn-default" onclick="app.publicCloseDiscussion();"><span class="glyphicon glyphicon-remove"></span> Fermer</button></div>
			<div id="messages-view-subject" class="col-sm-12 text-center">
			</div>
			<div id="messages-view-toolbar" class="col-sm-12">
			</div>
			<div class=" well well-sm collapse" id="messages-view-participants">
				<h4>Liste des participants</h4>
				<div id="messages-view-participants-liste">
				</div>
			</div>
			<div id="messages-view-text" class="col-sm-12 flex-container">
			</div>
		</div>
	</div>
</div>