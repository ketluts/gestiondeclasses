<!-- TEMPLATE CLASSE --> 
<div class="template template_public box col-md-12" id="classe">
		<span class="btn btn-default btn-close" onclick="app.hide('classe');$('#app').goTo();">
			<span class="glyphicon glyphicon-remove"></span>
		</span>
	<h2>En classe</h2>
	<div>
		<div class="h4">Je préfère travailler avec :</div>

		<select class="form-control" id="socigramme-plusplus" onchange="app.publicRelationAdd();"></select>
		<select class="form-control" id="socigramme-plus" onchange="app.publicRelationAdd();"></select>
	</div>
	<div>
		<hr/>
		<div class="h4">J'ai des difficultés à travailler avec :</div>
		
		<select class="form-control" id="socigramme-moins" onchange="app.publicRelationAdd();"></select>
	</div>
</div>