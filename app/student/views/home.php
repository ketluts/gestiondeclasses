<div class="template template_public box col-md-12">
	<div class="flex-rows">
		<div class="flex-3">
		<?php require_once('organizer.php'); ?>
		</div>
		<div class="flex-1 flex-columns aside text-center">
		<?php require_once('aside.php'); ?>
		</div>
	</div>
</div>

<?php require_once('sociogramme.php'); ?>
<?php require_once('notes.php'); ?>
<?php require_once('skills.php'); ?>
<?php require_once('challenges.php'); ?>
<?php require_once('messagerie.php'); ?>
<?php require_once('shares.php'); ?>
<?php require_once('intelligences.php'); ?>