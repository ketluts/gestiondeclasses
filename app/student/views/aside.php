	<div class="flex-rows">
				<div id="eleve-monster"></div>
				<div class="flex-1 flex-columns">
					<div class="h2" id="eleve_name"></div>
					<div class="" id="eleve_statut"></div>
					<div class="" id="eleve_classes"></div>
				</div>	
			</div>	
			<hr style="width:100%;"/>
			<div class="h3">
				Semaine <span id="agenda-semaine"></span>
			</div>			
			<div class="col-sm-12">
				<hr/>				
				<div id="eleve_calendar_legende" class="col-sm-12"></div>
			</div>			
			<div class="btn-group-vertical" role="group" style="width:100%;">
				<hr/>
				<button class="btn btn-default" onclick="$('#classe').css('display','block').goTo();">
					<span class="glyphicon flaticon-teamwork"> </span>  En classe
				</button> 
			</div>
			<div class="col-sm-12" id="etoiles_info"><hr/>
				Tu as obtenu <strong id="eleve_nb_etoiles"></strong>
				<img class="etoile" src="assets/img/etoile_on.png" width="25" /> !
			</div>