<!-- TEMPLATE INTELLIGENCES --> 
<div class="template template_public box col-md-12" id="intelligences">
					<span class="btn btn-default btn-close" onclick="$('#app').goTo();">
			<span class="glyphicon glyphicon-chevron-up"></span>
		</span>
	<h2>Intelligences multiples</h2>
	<br/>
	<div class="flex-rows">
		<div id="intelligences_results" class="flex-1"></div>
		<div class="flex-1 text-center">
			<div id="intelligences_presentation">
				<blockquote class="text-left">
					<p>La théorie des intelligences multiples suggère qu'il existe plusieurs types d'intelligence chez l'enfant d'âge scolaire et aussi, par extension, chez l'être humain en général. Cette théorie fut pour la première fois proposée par Howard Gardner en 1983.</p>

					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										Verbale et linguistique
									</a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									C’est l’aptitude à penser avec des mots et à employer le langage pour exprimer ou saisir des idées complexes. On la retrouve chez les écrivains et les poètes, les traducteurs et les interprètes. 
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										Spatiale et visuelle
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
									L’intelligence spatiale permet à la personne d’utiliser des capacités intellectuelles spécifiques pour avoir mentalement une représentation spatiale du monde. Les Amérindiens voyagent en forêt à l’aide de leur représentation mentale du terrain. Ils visualisent des points de repère : cours d’eau, lacs, types de végétation, montagnes… et s’en servent pour progresser ; des navigateurs autochtones font de même et naviguent sans instrument dans certaines îles du Pacifique.
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										Corporelle et kinesthésique
									</a>
								</h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body">
									L’intelligence kinesthésique est la capacité d’utiliser son corps pour exprimer une idée ou un sentiment ou réaliser une activité physique donnée. Elle est particulièrement utilisée par les professions de danseur, d'athlète, de chirurgien et d'artisan.
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading4">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
										Naturaliste
									</a>
								</h4>
							</div>
							<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
								<div class="panel-body">
									L'intelligence naturaliste, qui permet de classer les objets, et de les différencier en catégories. Très sollicitée chez les zoologistes, botanistes, archéologues tel Darwin. C’est l’intelligence qui permet d’être sensible à ce qui est vivant ou de comprendre l’environnement dans lequel l’homme évolue. C'est la capacité d’apprécier, de reconnaître et de classer la faune, la flore et le monde minéral. Cette capacité s’applique aussi, par extension, à l’univers culturel qu’il permet d’interpréter.
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading5">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
										Intra-personnelle
									</a>
								</h4>
							</div>
							<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
								<div class="panel-body">
									L'intelligence intra-personnelle permet de se former une représentation de soi précise et fidèle et de l'utiliser efficacement dans la vie. Elle sollicite plus le champ des représentations et des images que celui du langage. Il s'agit de la capacité à décrypter ses propres émotions, à rester ouvert à ses besoins et à ses désirs. C'est l'intelligence de l'introspection, de la psychologie analytique. Elle permet d'anticiper sur ses comportements en fonction de la bonne connaissance de soi. Il est possible mais pas systématique, qu'une personne ayant une grande intelligence intrapersonnelle, soit qualifiée par son entourage de personne égocentrique.
								</div>
							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading7">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
										Logico-mathématique
									</a>
								</h4>
							</div>
							<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
								<div class="panel-body">
									Les personnes qui ont une intelligence logico-mathématique développée possèdent la capacité de calculer, de mesurer, de faire preuve de logique et de résoudre des problèmes mathématiques et scientifiques. Elles analysent les causes et les conséquences d'un phénomène ou d'une action et sont capables d'expliquer le pourquoi des choses. Elles ont aussi tendance à catégoriser et à ordonner les objets. Elles aiment les chiffres, l'analyse et le raisonnement.
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading8">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
										Musicale et rythmique
									</a>
								</h4>
							</div>
							<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
								<div class="panel-body">
									L’intelligence musicale constitue l’aptitude à penser en rythme et en mélodies, de reconnaître des modèles musicaux, de les interpréter et d'en créer.

									Les musiciens possèdent celle-ci, plus ou moins développée, selon les études musicales. Il reste cependant des exceptions, des personnes ayant développé cette intelligence musicale-rythmique sans aucune aide, souvent après une enfance aux côtés de personnes douées musicalement.
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading6">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
										Inter-personnelle
									</a>
								</h4>
							</div>
							<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
								<div class="panel-body">
									L’intelligence inter-personnelle (ou sociale) permet à l’individu d’agir et de réagir avec les autres de façon correcte et adaptée. Elle l’amène à constater les différences et nuances de tempérament, de caractère, de motifs d’action entre les personnes. Elle permet l’empathie, la coopération, la tolérance. Elle permet de détecter les intentions de quelqu’un sans qu’elles soient avouées. Cette intelligence permet de résoudre des problèmes liés à nos relations avec les autres ; elle nous permet de comprendre et de générer des solutions valables pour les aider. Les personnalités charismatiques ont toutes une intelligence inter-personnelle très élevée.
								</div>
							</div>
						</div>
					</div>
					<footer>Théorie des intelligences multiples, <cite title="Wikipédia"><a href="https://fr.wikipedia.org/wiki/Théorie_des_intelligences_multiples" title="Wikipédia" target="_blank">Wikipédia</a></cite></footer>
				</blockquote>
				<!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/DCanko5Xhys" frameborder="0" allowfullscreen></iframe> -->
				<button class="btn btn-primary" id="intelligences_test_btn_start" onclick="app.intelligencesTestStart();$('#intelligences_test').goTo();">Commencer le test
				</button>
			</div>
			<div id="intelligences_test"></div>
		</div>
	</div>	
</div>