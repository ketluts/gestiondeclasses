		<div id="agenda-date" style="display: none;"></div>
		<div class="btn-group pull-left">
			<button class="btn btn-default btn-md" onclick="$('#agenda').fullCalendar('prev');"><span class="fc-icon fc-icon-left-single-arrow"></span></button>
			<button class="btn btn-default btn-md" onclick="app.agendaGoToday();">Aujourd'hui</button>
			<button class="btn btn-default btn-md" onclick=" $('#agenda').fullCalendar('next');"><span class="fc-icon fc-icon-right-single-arrow"></span></button>
		</div>
		<div class="btn-group pull-left">
			<button class="btn btn-default btn-md agenda-view" id="agenda-view-agendaDay" onclick=" $('#agenda').fullCalendar( 'changeView', 'agendaDay' )">1 j.</button>
			<button class="btn btn-default btn-md agenda-view" id="agenda-view-basicThreeDay" onclick=" $('#agenda').fullCalendar( 'changeView', 'basicThreeDay' )">3 j.</button>
			<button class="btn btn-default btn-md agenda-view" id="agenda-view-basicWeek" onclick=" $('#agenda').fullCalendar( 'changeView', 'basicWeek' )">Semaine</button>
			<button class="btn btn-default btn-md agenda-view" id="agenda-view-month" onclick=" $('#agenda').fullCalendar( 'changeView', 'month' )">Mois</button>
		</div>
		<div class="pull-right">
			<button class="btn btn-default" id="btn-reload" onclick="app.publicConnexion();$(this).button('loading');" data-loading-text="..." title="Recharger la page."><span class="glyphicon glyphicon-refresh"></span></button> 
		</div>
		<div id='eleve_calendar'>				
			<div id='agenda' class="screen-small"></div>
		</div>