<!-- TEMPLATE CONNEXION____________
_______________________________
_______________________________-->  
<div class="template template_connexion connexion-header box col-sm-12">
<div class="btn-group connexion-toolbar">
  <a href="../teacher/" class="btn btn-primary connexion-requise">Professeur <span class="glyphicon glyphicon-chevron-right"></span></a>
 <span id="limited_version" class="screen-medium"></span>  
</div>       
   <a href="https://gestiondeclasses.net/" title="Présentation" class="h1">   
   
      <span class="screen-medium">
       <img src='assets/favicons/apple-touch-icon-72x72.png'/>
     </span>
     GestionDeClasses
   </a>
   </div>






   <div class="template template_connexion connexion-form box col-sm-12 flex-1">
<div class="col-sm-6 text-center" id="connexion-eleve-form">
  <div class="well well-sm">
    <h3>Élèves</h3>
    <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
        <input class="form-control" list="list_schools" id="etablissement_eleve_nom" name="etablissement_eleve_nom" placeholder="Etablissement" />
      </div>
      <div class="input-group">
       <div class="input-group-addon"><span class="glyphicon glyphicon-chevron-right"></span></div>
       <input class="form-control" id="cnx_eleve_token" name="cnx_eleve_token" placeholder="Code élève" type="password"/>
     </div>
   </div>
   <div class="form-group">
    <button class="btn btn-primary connexion-requise" id="connexion-btn" onclick="$(this).button('loading');app.publicConnexion();" data-loading-text="Connexion en cours..."><span class='glyphicon glyphicon-log-in'></span> Connexion</button>
  </div>
</div>
</div>
</div>
