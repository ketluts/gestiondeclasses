<!-- TEMPLATE COMPETENCES --> 
<div class="template template_public box eleve_competences col-md-12" id="competences">
					<span class="btn btn-default btn-close" onclick="$('#app').goTo();">
			<span class="glyphicon glyphicon-chevron-up"></span>
		</span>
	<div class="flex-columns">
		<div>
			<div class="pull-left btn-group">
				<button class="btn btn-default competences-view" id="btn-competences-view" onclick="app.publicItemToggleMode('view');"><span class="glyphicon glyphicon glyphicon-eye-open"></span> Voir mes résultats</button> 
				<button class="btn btn-default competences-view" id="btn-competences-edit" onclick="app.publicItemToggleMode('edit');"><span class="glyphicon glyphicon glyphicon-pencil"></span> M'auto-évaluer</button> 
			</div>
		</div>
		<div class="flex-rows">
			<div class="flex-3">
				<h2 class="col-sm-12">Mes compétences</h2>
			

				<div id="competences_graph" class="col-sm-12"></div>
					<div class="well well-sm col-sm-12" id="itemsNothingToShow">Aucun item ne correspond à ces filtres.</div>
				<div id="competences_liste" class="col-sm-12"></div>
			</div>
			<div class="flex-1 aside">
				<h2>Filtres</h2>
				<h4>Niveau d'acquisition  <button class="btn btn-default btn-sm" id="itemsFiltersProgressionsReset" onclick="app.publicItemInitProgressionsFilter();"><span class="glyphicon glyphicon-refresh"><span class="screen-medium"></span></button></h4>
				<div>
					<span id="acquisition-slider"></span>
				</div>
				<hr/>
				<div id="competences_filtre_users"></div>
				<hr/>
				<div id="competences_filtre_categories"></div>
				<hr/>
				<div id="competences_filtre_sous_categories"></div>
				<hr/>
				<div id="competences_filtre_tags"></div>
			</div>
		</div>
	</div>	
</div>