 <div class="box flex-rows"  id="header" style="display:none">     
<div id="home" class="flex-1">        
         <div id="header-menu-eleve">
          <div class="btn-group">
            <button  class="btn btn-primary" onclick="$('#app').goTo();" title="Mon agenda"><span class="glyphicon glyphicon-th"></span></button>
          </div>
          <div class="btn-group">
           <button  class="btn btn-primary"  id="header-menu-eleve-notes" onclick="$('#notes').goTo();" title="Mes notes"><span class="glyphicon glyphicon-book"> </span></button>
           <button  class="btn btn-primary" onclick="$('#competences').goTo();" title="Mes compétences"><span class="glyphicon glyphicon-tasks"></span></button>
           <button  class="btn btn-primary"  id="header-menu-eleve-defi" onclick="$('#defis').goTo();" title="Mes défis">
            <span class="glyphicon flaticon-puzzle26"></span>
          </button>
        </div> 
        <div class="btn-group">
          <button  class="btn btn-primary" onclick="$('#files').goTo();" title="Mes fichiers">
            <span class="glyphicon glyphicon-folder-open"></span>
          </button>
          <button  class="btn btn-primary" onclick="$('#links').goTo();" title="Mes liens">
            <span class="glyphicon glyphicon-link"></span>
          </button>
          <button  class="btn btn-primary" id="header-menu-eleve-messagerie" onclick="$('#messagerie').goTo();" title="Mes messages">
            <span class="glyphicon glyphicon-envelope"></span>
            <span class="badge" id="messagerie-badge"></span>
          </button>
        </div>
      </div> 
    </div>
    <div id="titre" class="flex-1 h1"></div>  
    <div class="flex-1 text-right"> 
      
      <div class="btn-group">
        <button class='btn btn-primary' onclick='app.deconnexion();' title="Déconnexion"><span class='glyphicon glyphicon-off'></span></button>
      </div>
    </div> 
    </div>