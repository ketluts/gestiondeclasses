</div>
<div id="footer" class="box">
 D'après <img src='assets/favicons/apple-touch-icon-57x57.png' width='20px'> <a href="https://gestiondeclasses.net/" target="_blank" rel="noreferrer">GestionDeClasses</a> | <span id="footer-version"></span><span id="footer-hebergeur"></span> <span id="footer-plugin-bloc"></span>
</div>
</div>
<datalist id="list_schools"></datalist>
<datalist id="list_users"></datalist>
<!-- ###LIBRARIES### --> 
<script type="text/javascript" src="assets/lib/jquery-2.1.4.min.js"></script>  
<script type="text/javascript" src="assets/lib/DataTables/datatables.min.js"></script>
<script type="text/javascript" src="assets/lib/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script type="text/javascript" src="assets/lib/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="assets/lib/json2.min.js"></script>
<script type="text/javascript" src="assets/lib/json_sans_eval.js"></script>
<script type="text/javascript" src="assets/lib/mustache.min.js"></script>

<script type="text/javascript" src="assets/lib/moment-with-locales.js"></script>
<script type="text/javascript" src="assets/lib/md5.js"></script>
<script type="text/javascript" src="assets/lib/qtip/jquery.qtip.min.js"></script>
<!-- <script type="text/javascript" src="assets/lib/jquery.sticky-kit.min.js"></script> -->
<script type='text/javascript' src='assets/lib/fullcalendar-3.0.1/fullcalendar.js'></script>
<script type='text/javascript' src='assets/lib/fullcalendar-3.0.1/locale/fr.js'></script>
<script type="text/javascript" src="assets/lib/datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/lib/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="assets/lib/quill1.1.7/quill.min.js"></script>
<script type="text/javascript" src="assets/lib/pace/pace.js"></script>
<script type="text/javascript" src="assets/lib/filesaver/FileSaver.min.js"></script>
<script type="text/javascript" src="assets/lib/noty/js/noty/packaged/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="assets/lib/sweetalert/sweetalert.min.js"></script>

<script type="text/javascript" src="assets/lib/highcharts/highcharts.js"></script>
<script type="text/javascript" src="assets/lib/highcharts/highcharts-more.js"></script>
<!-- ###GESTION DE CLASSES### --> 
<!-- <script type="text/javascript" src="assets/script/classes.min.js"></script> -->
<script src="config.js"></script>
<script type="text/javascript" src="assets/scripts/app.js?20191106"></script>  
<script type="text/javascript" src="assets/scripts/callback.js?20191106"></script>  
<script type="text/javascript" src="assets/scripts/variables.js?20191106"></script>
<script type="text/javascript" src="assets/scripts/tools.js?20191106"></script>   
<script type="text/javascript" src="assets/scripts/time.js?20191106"></script>  
<script type="text/javascript" src="assets/scripts/navigate.js?20191106"></script> 
<!-- ###UI### -->  
<script type="text/javascript" src="assets/scripts/ui/render.js?20191106"></script>  
<!-- ###CONNEXION### --> 
<script type="text/javascript" src="assets/scripts/connexion/main.js?20191106"></script> 
<script type="text/javascript" src="assets/scripts/connexion/render.js?20191106"></script> 
<!-- ###ORGANIZER### --> 
<script type="text/javascript" src="assets/scripts/organizer/main.js?20191106"></script> 
<script type="text/javascript" src="assets/scripts/organizer/render.js?20191106"></script>
<!-- ###CONFIG### -->    
<script type="text/javascript" src="assets/scripts/config/main.js?20191106"></script>
<!-- ###CHALLENGES### --> 
<script type="text/javascript" src="assets/scripts/challenges/main.js?20191106"></script>  
<!-- ###SKILLS### --> 
<script type="text/javascript" src="assets/scripts/skills/main.js?20191106"></script> 
<script type="text/javascript" src="assets/scripts/skills/render.js?20191106"></script> 
<script type="text/javascript" src="assets/scripts/skills/autoeval.js?20191106"></script> 
<script type="text/javascript" src="assets/scripts/skills/filters.js?20191106"></script> 
<!-- ###MESSAGERIE### --> 
<script type="text/javascript" src="assets/scripts/messagerie/render.js?20191106"></script>
<script type="text/javascript" src="assets/scripts/messagerie/main.js?20191106"></script> 
 <!-- ###SOCIOGRAMME### --> 
<script type="text/javascript" src="assets/scripts/sociogramme/main.js?20191106"></script> 
<!-- ###TESTS### --> 
<script type="text/javascript" src="assets/scripts/tests/main.js?20191106"></script> 
<!-- ###SHARES### -->  
<script type="text/javascript" src="assets/scripts/shares/links.js?20191106"></script> 
<script type="text/javascript" src="assets/scripts/shares/files.js?20191106"></script> 
<!-- ###INTELLIGENCES MULTIPLES### --> 
<script type="text/javascript" src="assets/scripts/intelligences/main.js?20191106"></script>  
<!-- ###PLUGINS### --> 
<?php
$cfg='assets/scripts/plugins';
$dossier = opendir($cfg);
while($fichier = readdir($dossier)){
    if(is_file($cfg.'/'.$fichier) && $fichier !='/' && $fichier !='.' && $fichier != '..'){
      echo '<script type="text/javascript" src="assets/scripts/plugins/'.$fichier.'?20191106"></script>';
    }
}
closedir($dossier);
?> 


</body>
</html>