</div>
<div id="footer" class="box">
 D'après <img src='assets/favicons/apple-touch-icon-57x57.png' width='20px'> <a href="https://gestiondeclasses.net/" target="_blank" rel="noreferrer">GestionDeClasses</a> | <span id="footer-version"></span><span id="footer-hebergeur"></span> <span id="footer-plugin-bloc"></span>
</div>
</div>

<datalist id="list_schools"></datalist>
<datalist id="list_item_categories"></datalist>
<datalist id="list_item_sous_categories"></datalist>
<datalist id="list_item_tags"></datalist>
<datalist id="list_progressions_activites"></datalist>
<datalist id="list_event_titre"></datalist>
<datalist id="list_controles_categories"></datalist>
<!-- ###LIBRARIES### --> 
<script src="assets/lib/jquery-3.4.1.min.js"></script>  
<script src="assets/lib/DataTables/datatables.min.js"></script>
<script src="assets/lib/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="assets/lib/bootstrap/js/bootstrap.js"></script>
<script src="assets/lib/json2.min.js"></script>
<script src="assets/lib/json_sans_eval.js"></script>
<script src="assets/lib/mustache.min.js"></script>
<script src="assets/lib/handlebars-v4.1.2.js"></script>
<script src="assets/lib/moment-with-locales.js"></script>
<script src="assets/lib/md5.js"></script>
<script src="assets/lib/qtip/jquery.qtip.min.js"></script>
<!-- <script src="assets/lib/jquery.sticky-kit.min.js"></script> -->
<script src='assets/lib/fullcalendar-3.0.1/fullcalendar.js'></script>
<script src='assets/lib/fullcalendar-3.0.1/locale/fr.js'></script>
<script src="assets/lib/datepicker/js/bootstrap-datepicker.js"></script>
<script src="assets/lib/jquery.ui.touch-punch.min.js"></script>
<script src="assets/lib/quill1.3.6/quill.min.js"></script>
<script src="assets/lib/pace/pace.min.js"></script>
<script src="assets/lib/filesaver/FileSaver.min.js"></script>
<script src="assets/lib/noty/js/noty/packaged/jquery.noty.packaged.min.js"></script>
<script src="assets/lib/sweetalert/sweetalert.min.js"></script>

<script src="assets/lib/highcharts/highcharts.js"></script>
<script src="assets/lib/highcharts/highcharts-more.js"></script>
<script src="assets/lib/highcharts/modules/treemap.js"></script>
<!-- ###GESTION DE CLASSES### --> 
<script src="config.js"></script>

 <?php
function includeJs($dir){
$version="20191106";
$dossier = opendir($dir);
while($fichier = readdir($dossier)){
    if(is_file($dir.'/'.$fichier) && $fichier !='/' && $fichier !='.' && $fichier != '..'){
      echo '<script src="'.$dir.'/'.$fichier.'?'.$version.'"></script>'.PHP_EOL;
    }else{
    	if(is_dir($dir.'/'.$fichier) && $fichier !='/' && $fichier !='.' && $fichier != '..'){
    		includeJs($dir.'/'.$fichier);
    	}
    }
}
closedir($dossier);
}
includeJs('assets/scripts');
?>
</body>
</html>