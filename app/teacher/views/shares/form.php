<div id="share_form" class="template template_shares text-left box" >
 <div class="pull-right"><button class="btn btn-default" onclick="app.closeShare();$('#app').goTo();"><span class="glyphicon glyphicon-remove"></span></button>
 </div>
 <h4><span id="share_icon" class="file_type"></span> <span id="share_name"></span>
  <br/>
  <small>Cliquez sur le nom de la classe pour afficher les élèves.</small>
</h4>
<hr/>
<div class="col-sm-6 white">
  <div id="share_classes"></div>
</div>
<div class="col-sm-6 aside white sticky">
  <div id="share_liste"></div>
</div>
</div>