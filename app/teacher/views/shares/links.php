<div id="template_links" class="template template_shares text-center box">
 <div class="flex-rows">
  <div class="flex-4 flex-columns">
 <div id="link_form" class="well well-sm">
 <input type="text" class="form-control"  id="link_url" placeholder="http(s)://...." />
 <input type="text" class="form-control"  id="link_titre" placeholder="Titre du lien (facultatif)" />
 <input type="hidden"/>
 <input type="hidden"/>
 <input type="hidden"/>
 <br/>
 <div class="btn-group">
  <button class="btn btn-default" onclick="app.closeLinkForm();">Annuler</button>
  <button id="link_save_btn" class="btn btn-primary" onclick="$(this).button('loading');app.linkAdd();" data-loading-text="Enregistrement..."><span class="glyphicon glyphicon-plus"></span> Ajouter</button>
  </div>
</div>
  <div id="links-liste" class="col-sm-12 text-left"></div>
<div  class="col-sm-12 text-left">
  <hr/>
  <button class="btn btn-danger btn-sm" onclick="app.linksDeleteSelected();">
   <span class="glyphicon glyphicon-trash"></span> Supprimer</button>
 </div>
  </div>
  <div class="flex-1 text-center aside">       
    <button class="btn btn-default" onclick="app.renderLinkForm();">
    <span class="glyphicon glyphicon-link"></span> Ajouter un lien
    </button> 
  </div>
</div>
</div>