<div id="template_files" class="template template_shares text-center box"> 
 <div class="flex-rows">
  <div class="flex-4 flex-columns">
    <div id="files_form" class="col-sm-12 well well-sm">
     <div class="pull-right"><button class="btn btn-default" onclick="app.closeFilesForm();$('#app').goTo();"><span class="glyphicon glyphicon-remove"></span></button>
     </div>
     <div class="form-group text-left">
      <input type="file" id="files_input" name="file" multiple/>
      <p class="help-block">Sélectionner au maximum 10 fichiers. (max. <span id="upload_max_filesize"></span>)</p>
    </div>
    <div id="files_temp" class="text-left"></div>
    <div class="btn-group">
      <button class="btn btn-default" onclick="app.closeFilesForm();">Annuler</button>
      <button class="btn btn-primary" id="files_save_btn" onclick="$(this).button('loading');app.addFiles();" data-loading-text="Envoie en cours..."><span class="glyphicon glyphicon-send"></span> Envoyer</button>
    </div>
  </div>
  <div id="files-liste" class="col-sm-12 text-left"></div>
  <div  class="col-sm-12 text-left">
    <hr/>
    <button class="btn btn-danger btn-sm" onclick="app.filesDeleteSelected();">
     <span class="glyphicon glyphicon-trash"></span> Supprimer</button>
   </div>  
 </div>
 <div class="flex-1 text-center aside"> 
   <span class="h5"><span id="quota_used"></span><small>/<span id="quota_total"></span></small></span>
   <div class="progress progress_quota">
    <div class="progress-bar" role="progressbar" id="progress_quota" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
    </div>
  </div>      
  <button class="btn btn-default" onclick="app.renderFilesForm();">
   <span class="glyphicon glyphicon-cloud-upload"></span> Ajouter des fichiers
 </button>
</div>
</div>
</div>