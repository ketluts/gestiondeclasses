<!-- TEMPLATE SKILLS --> 
<div id="template_skills" class="template box">
  <div class="flex-columns">
    <?php
    include('title.php');
    ?>
    <div class="flex-rows">
      
     <div class="flex-1" id="items-classes-container">
        <div class="input-group">
         <span class="input-group-addon"><span class="glyphicon glyphicon-folder-open"></span></span>
         <select class="form-control" id="items-classes-select" onchange="app.itemsToggleClasse(this.value);"> </select>
       </div>  
       <div id="items-classes"></div>
     </div>
    
     <div class="flex-4" id="items-container">
       <div class="flex-rows">
         <div class="flex-5 flex-columns">
          <?php
          
          include('importItems.php');
          include('editor.php');
          include('importResults.php');
          ?>
          <div class="flex-1" id="items-main">   
           <?php
           include('items.php');
           ?>  
         </div> 
       </div> 
       
     </div>
   </div>
  
      <div class="aside flex-2" id="itemsAsideFiltres">
        <div class=" flex-columns ">
         <?php
         include('aside.php');
         ?>
         </div>
       </div>


 </div>
 <?php
 include('footer.php');
 ?>
</div>
</div>
<?php
include('templates.php');
?>