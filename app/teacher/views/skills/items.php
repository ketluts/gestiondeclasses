<div class="well well-sm" id="itemsNothingToShow">Aucun item ne correspond à ces filtres.</div>
<div id="items-activity">
</div>
<div id="items-student-graph-block">
 <div id="items-student-diagramme" ></div>
 <hr/>
</div>
<div id="items-classroom-graph-block" class="item-classe-view">
  <div>
    <input type="radio" onclick="app.itemsSyntheseSetMode('categories');" name="itemsSyntheseMode" class="itemsSyntheseMode_categories" id="itemsSyntheseMode_categories"><label for="itemsSyntheseMode_categories">Par catégories</label>
    <input type="radio" onclick="app.itemsSyntheseSetMode('sous_categories');" name="itemsSyntheseMode" class="itemsSyntheseMode_sous_categories" id="itemsSyntheseMode_sous_categories"><label for="itemsSyntheseMode_sous_categories">Par sous-catégories</label>
    <input type="radio" onclick="app.itemsSyntheseSetMode('tags');" name="itemsSyntheseMode" class="itemsSyntheseMode_tags" id="itemsSyntheseMode_tags"><label for="itemsSyntheseMode_tags">Par mot-clés</label>
  </div>
  <div id="classroom_competences_graph"> </div>
  <hr/>
</div>
<div id="items-classroom-tab-block" class="item-classe-view">
  <div>
    <input type="radio" onclick="app.itemsSyntheseSetMode('categories');" name="tableitemsSyntheseMode" class="itemsSyntheseMode_categories" id="tableitemsSyntheseMode_categories">
    <label for="tableitemsSyntheseMode_categories">Par catégories</label>
    <input type="radio" onclick="app.itemsSyntheseSetMode('sous_categories');" name="tableitemsSyntheseMode" class="itemsSyntheseMode_sous_categories" id="tableitemsSyntheseMode_sous_categories">
    <label for="tableitemsSyntheseMode_sous_categories">Par sous-catégories</label>
    <input type="radio" onclick="app.itemsSyntheseSetMode('tags');" name="tableitemsSyntheseMode" class="itemsSyntheseMode_tags" id="tableitemsSyntheseMode_tags">
    <label for="tableitemsSyntheseMode_tags">Par mot-clés</label>
  </div>
  <div id="items-classroom-tab"> </div>
</div>
<?php 
include('feedbacks.php'); 
?>
<div id="items-liste"></div>