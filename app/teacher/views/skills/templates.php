 <template id="template-items-liste">
   {{#categories}} 
   <div class="h3 items-categorie" id="items_categorie_{{categorie_id}}" onclick="app.itemsAutoSelect({{categorie_id}},false,'categorie');">
    <strong>
      {{categorie_name}}
    </strong>
    <span id="categorie_{{categorie_id}}_progression" class="categories_progression" title="Accomplissement des élèves évalués.">  
    </span>
  </div>
  {{#sous_categories}}
  <div class="h5 items-sous-categorie {{visibility}}" id="items_sous_categorie_{{categorie_id}}_{{sous_categorie_id}}" onclick="app.itemsAutoSelect({{categorie_id}},{{sous_categorie_id}},'sous_categorie');">
    <strong>
      {{sous_categorie_name}}
    </strong>
    <span id="sous_categorie_{{sous_categorie_id}}_progression" class="categories_progression" title="Accomplissement des élèves évalués."></span>
  </div>
  <div class="items-sortable">
    {{#list}}
    <div class="item-box ui-sort-enable" id="item_{{item_id}}_bloc" data-id="{{item_id}}" >
      <span class="item-handle" style="background-color:{{item_color}};"></span>
      <div class="flex-rows">
        <input type="checkbox" id="item_{{item_id}}_checkbox" onclick="app.itemsSelectedUpdate();"/>
        <span class="flex-3 item-title" onclick="app.itemsRenderDetails({{item_id}});" title="{{item_name}}">
          {{item_name}}
          <div id="item_{{item_id}}_tags" class="item-tags flex-1">
           {{item_tags_render}}
         </div>
       </span>
       <span class="flex-2 text-center" id="item_{{item_id}}" style="position:relative; overflow:hidden;">
        {{#cursor}}
        <div style="position:relative;">
          {{{item_cursor_style}}}
          <div class="slider-style item-background" style="background-color:rgba(255,255,255,0.85);width:100%"></div>
        </div>  
        <span id="item_{{item_id}}_results">
          <div style="position:relative;">
            <div id="item_{{item_id}}_slider_style" class="slider-style slider-progress-style"></div>
            <div data-max="{{item_value_max}}" data-id="{{item_id}}" id="item_{{item_id}}_slider" class="itemSlider"></div>
          </div>
        </span>
       <!--   <div style="position:relative;">
          {{{item_cursor_style}}}
        </div>  -->
        {{/cursor}}
        {{#box}}
        <input type="checkbox" class="check-box" id="item_{{item_id}}_checkbox_mode" onclick="app.itemCheckboxChange({{item_id}});"/>
        <label for="item_{{item_id}}_checkbox_mode" class="check-box"></label>
        {{/box}}
        <span class="item_checkbox_details" id="item_{{item_id}}_checkbox_details">
        </span>
      </span>
      <span class="item_nbeval_btn" id="item_{{item_id}}_btn" >
      </span>
      <span class="text-right">
        {{#item_admin}}
        <div class="btn-group">
          <div class="btn btn-default btn-sm item_view item_view_edit" id="item_view_{{item_id}}_edit" onclick="app.toggleItemView({{item_id}},'edit');" title="Permettre l'auto-évaluation."><span class="glyphicon glyphicon glyphicon-pencil"></span></div>
          <div class="btn btn-default btn-sm item_view" id="item_view_{{item_id}}" onclick="app.toggleItemView({{item_id}},'view');"  title="Montrer l'item aux élèves."><span class="glyphicon glyphicon glyphicon-eye-open"></span></div>
        </div>
        <div class="btn-group item-toolbar-right">
          <div class="btn {{item_public_style}} btn-sm item_admin" id="item_view_{{item_id}}_share" onclick="app.toggleItemShare({{item_id}});" title="Partager cet item."><span class="glyphicon glyphicon-education"></span></div>
          <span class="btn btn-default btn-sm item_admin" onclick="app.renderEditItem({{item_id}});$('#app').goTo();" title="Éditer cet item."><span class="glyphicon glyphicon-cog"></span></span>
          <span class="btn btn-default btn-sm item_admin" onclick="app.renderCloneItem({{item_id}});$('#app').goTo();" title="Cloner cet item."><span class="glyphicon glyphicon-duplicate"></span></span>
        </div>
        {{/item_admin}}
      </span>
    </div>
    <div class="flex-rows item_details" data-id="{{item_id}}" style="display:none;" id="item_{{item_id}}_details_block">
     <!--  <hr/> -->
     <div class="flex-rows item_description" id="item_{{item_id}}_current_description"></div>
     <div id="item_{{item_id}}_details"></div>
   </div>
 </div>
 {{/list}}
</div>
{{/sous_categories}}
{{/categories}}
<div class="clearfix"></div>
</template>
<template id="template-items-classes">
  <ul class="list-group items_classe_eleves">
    <li class="list-group-item itemsEleve">
     <input type="checkbox" onchange="app.cptElevesSelectedAll({{classe_id}});" class="cpt_classe_checkbox" id="cpt_classe_{{classe_id}}_checkbox" onclick="event.cancelBubble=true;"/>
     &nbsp;
   </li>
   {{#eleves}}
   <li class="list-group-item itemsEleve items_eleve_{{.}}" onclick="app.go('skills/student/{{.}}');">
    <input type="checkbox" onchange="app.cptElevesSelectedUPD();" class="cpt_eleve_checkbox" id="cpt_eleve_{{.}}_checkbox" onclick="event.cancelBubble=true;"/>
    {{eleveNom}}
  </li>
  {{/eleves}}
</ul>
</template>
<template id="template-items-classes-select">
  <option value="-3" disabled="disabled" style="display: none;">Mes classes</option>
  {{#classes}}
  {{#isVisible}}
  <option value="{{classe_id}}">{{classeNom}}</option>
  {{/isVisible}}
  {{/classes}}
</template>
<template id="template-items-student-progress">
 {{#progress}}
 <div class="items-progression" id="progression_{{rei_id}}">
  <span class="items-{{rei_symbole}}" style="color:{{color}};"></span>  {{rei_activite}} {{{commentStr}}} {{{userStr}}} <span class="progression-date">{{progressDate}}</span>
  {{#admin}}
  <div class="btn-group pull-right item-progression-toolbar">
    <div class="btn btn-default btn-xs" title="Éditer" onclick="app.viewToggle('progression_{{rei_id}}_edit');">
      <span class="glyphicon glyphicon-pencil"></span>
    </div>
    <div class="btn btn-primary btn-xs" data-loading-text="..." title="Supprimer" onclick="$(this).button('loading');app.delProgression({{rei_id}},{{rei_item}});">
      <span class="glyphicon glyphicon-remove"></span>
    </div>    
  </div>
  {{/admin}}
  <div id="progression_{{rei_id}}_edit" style="display: none;">
    <input type="text" id="progression_{{rei_id}}_edit_activite" list="list_progressions_activites" value="{{rei_activite}}" placeholder="Activité évaluée" />
    <input type="text" id="progression_{{rei_id}}_edit_date" value="{{progressDate}}" placeholder="JJ/MM/AAAA" />
    <span class="btn btn-primary btn-xs progression-update-btn" title="Enregistrer" data-loading-text="..." onclick="$(this).button('loading');app.progressionUpdate({{rei_id}},{{rei_item}});">
     <span class="glyphicon glyphicon-save"></span>
   </span>
 </div>
</div>
{{/progress}}
</template>



<!-- <template id="template-items-feedback-liste">
  <ul>
 {{#files}}
<li>{{file_fullname}} <a class="btn btn-xs btn-default" onclick="app.itemsFeedbacksFileDelete({{file_id}});"><span class="glyphicon glyphicon-trash"></span></a></li>
{{/files}}
<ul>
</template> -->


<script id="template-feedbacks" type="text/x-handlebars">
  <?php
  include('feedbacksTpl.php');
  ?>  
</script>