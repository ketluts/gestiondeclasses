<div id="items_results_import" class="well well-sm">
 <div class="btn btn-default btn-close" onclick="app.hide('items_results_import');">
   <span class="glyphicon glyphicon-remove"></span>
 </div>
 <div class="h3">
  Importer des résultats<br/>
  <small>Colonne 1 : ID de l'élève  / Colonne 2 : Résultat / Colonne 3 : Résultat...</small>
</div>
<textarea id="items-results-import-tableur" class="form-control" placeholder="Copier/Coller depuis le tableur."></textarea>
<div class="text-right">
  <div class="btn btn-primary" id="items-results-import-tableur-btn" onclick="$(this).button('loading');app.itemsResultsImportInit();" data-loading-text="Analyse en cours...">
    <span class="glyphicon glyphicon-import"></span> Analyser
  </div>
</div> 
<div id="items-results-import-selection">
</div>
<div id="items-results-import-apercu">
</div>
<div class="btn btn-primary pull-right connexion-requise" id="items-results-import-save-btn" onclick="app.itemsResultsImportSave();" data-loading-text="Sauvegarde en cours...">
  <span class="glyphicon glyphicon-save"></span> Enregistrer
</div>
</div>