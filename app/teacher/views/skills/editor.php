<div id="item_form" class="well well-sm">
  <div class="btn btn-default" id="item-form-close" onclick="app.itemsFormClose();">
  <span class="glyphicon glyphicon-remove"></span>
</div>
<div class="flex-rows ">
 <div class="flex-1">
  <div class="h3 text-center" id="item-form-titre"></div>
  <div class="input-group">
    <span class="input-group-btn items-edition-input">
      <input id="item_color" class="form-control" type="color" value="#ffffff"/> 
      <div class="items-edition-input-hover"></div>
    </span>
    <div class="items-edition-input">
      <input id="item_name" class="form-control" placeholder="Nom de l'item" /> 
      <div class="items-edition-input-hover"></div>
    </div>
  </div>
  <br/>
  <div class="input-group items-edition-input">
    <span class="input-group-addon">
     <span class="glyphicon glyphicon-record"></span>
   </span>
   <select id="item_cycle" class="form-control">
    <option value="-1">Choix du cycle</option>
    <option value="1">Cycle 1</option>
    <option value="2">Cycle 2</option>
    <option value="3">Cycle 3</option>
    <option value="4">Cycle 4</option>    
  </select>
  <div class="items-edition-input-hover"></div>
</div>
<div class="input-group items-edition-input">
  <span class="input-group-addon">
    <span class="glyphicon glyphicon-folder-open"></span>
  </span>
  <input id="item_categorie" list="list_item_categories" class="form-control" placeholder="Catégorie de l'item" />
  <div class="items-edition-input-hover"></div> 
</div>
<div class="input-group items-edition-input">
  <span class="input-group-addon">
    <span class="glyphicon glyphicon-folder-open"></span>
  </span>
  <input id="item_sous_categorie" list="list_item_sous_categories" class="form-control" placeholder="Sous-catégorie de l'item" /> 
  <div class="items-edition-input-hover"></div> 
</div>

<div class="input-group items-edition-input">
  <span class="input-group-addon">
    <span class="glyphicon glyphicon-tags"></span>
  </span>
  <input id="item_tags" list="list_item_tags" class="form-control" placeholder="Mot-clés séparés par des virgules" oninput="app.setItemsTagsDatalist();app.itemsEditionTagAdd();" onblur="app.itemEditionTags=app.itemEditionTagsCurrent;this.value=''" /> 
  <div class="items-edition-input-hover"></div>
</div> 
<br/>
<div id="items-tags-list"></div>
<br/>
<div id="items-edition-tags-mode" class="h5">
 <input type="radio" id="items-edit-tags-add" value="add" name="items-edit-tags-mode" onchange="app.itemsEditionTagsModeSet('edit');" checked="checked"/> 
 <label for="items-edit-tags-add"> Ajouter ces mot-clés</label>
 <br/>
 <input type="radio" value="replace" id="items-edit-tags-replace" name="items-edit-tags-mode" onchange="app.itemsEditionTagsModeSet('replace');"/>
 <label for="items-edit-tags-replace"> Remplacer par ces mot-clés</label>
</div>

<div class="items-edit-selection flex-1"> 
 
     <h3>Type</h3>
     <div class="h4">
      <div class="items-edition-input">
       <input type="radio" id="item-vue-checkbox" class="item-vue" name="item-vue" value="checkbox" onchange="app.itemsVueChange(this.value);"/>
       <div class="items-edition-input-hover"></div>
       <label for="item-mode-libre"> Checkbox</label> <small>L'item est validé ou non. </small>
     </div>  
     <div class="items-edition-input">
      <input type="radio" id="item-vue-cursor" class="item-vue" name="item-vue" value="cursor" checked="checked" onchange="app.itemsVueChange(this.value);"/>
      <div class="items-edition-input-hover"></div>
      <label for="item-mode-auto"> Curseur</label>  <small>Plusieurs niveaux de validations. </small>
    </div> 
  </div>


<div class="items-cursor-mode items-edit-selection text-center">
  <br/>
  <br/>
  Nombre de couleurs
  <select id="item_value_max" onchange="app.previewItemsSlider(this.value);">
    <option value="1">2</option>
    <option value="2">3</option>
    <option value="3">4</option>
    <option value="4">5</option>
    <option value="5">6</option>
    <option value="6">7</option>
    <option value="7">8</option>
    <option value="8">9</option>
    <option value="9">10</option> 
  </select>
  <br/>
</div>
<div id="item-preview" class="items-cursor-mode items-edit-selection text-center"></div>
<div class="flex-1 items-cursor-mode">   
  <h3>Synthèse</h3>
  
  <div class="h4">
   <input type="radio" id="item-mode-libre" name="item-mode" value="libre"  onchange="app.itemsMode=this.value;"/>
   <label for="item-mode-libre"> Fixe</label> <small>La progression est fixée manuellement.</small>
   <br/>
   <input type="radio" id="item-mode-auto" name="item-mode" value="auto"  checked="checked" onchange="app.itemsMode=this.value;"/>
   <label for="item-mode-auto"> Automatique</label> <small>La progression dépend du nombre d'évaluations, de leur date. </small>
 </div>
</div>
</div>

</div>
<div class="flex-1 aside items-edit-selection text-center">
 <div class="h3 text-center">Feedbacks</div> 
<div id="item-descriptions"></div>
</div>

</div>

<hr/>

<div class="pull-left" id="item-form-delete"></div>
<div class="pull-right">
  <div class="btn btn-primary connexion-requise item_save_btn" onclick="$(this).button('loading');" data-loading-text="Enregistrement...">
    <span class="glyphicon glyphicon-save"></span> Enregistrer
  </div>
  <div class="btn btn-default" onclick="app.itemsFormClose();">
    <span class="glyphicon glyphicon-remove"></span>
  </div>
</div>
<br/>

</div>