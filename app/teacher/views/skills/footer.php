<div id="items-footer" class="flex-1">
	<div id="items-selection-btn" class="flex-1 pull-left"> 
    <span id="items-selected-nb"></span> 
    <div class="btn-group">
     <div class="btn btn-default" onclick="app.itemsFilteredBySelectionToggleStatue();" title="Filtrer">
       <span class="glyphicon glyphicon glyphicon-filter"></span>
     </div>
   </div>
   <div class="btn-group">
    <div class="btn btn-default item_view item_view_edit" id="items-selection-edit" onclick="app.toggleItemView('selection','edit');" title="Permettre l'auto-évaluation.">
      <span class="glyphicon glyphicon glyphicon-pencil"></span>
    </div>
    <div class="btn btn-default item_view" id="items-selection-view" onclick="app.toggleItemView('selection','view');" title="Montrer l'item aux élèves.">
      <span class="glyphicon glyphicon glyphicon-eye-open"></span>
    </div>
  </div>
  <div class="btn-group">
    <div class="btn btn-default item_admin" id="items-selection-share" onclick="app.toggleItemShare('selection');" title="Partager ces items.">
      <span class="glyphicon glyphicon-education"></span>
    </div>
    <span class="btn btn-default item_admin" onclick="app.renderEditItem('selection');$('#app').goTo();" title="Éditer ces items.">
      <span class="glyphicon glyphicon-cog"></span>
    </span>
  </div> 
  <div class="btn-group">
    <span class="btn btn-default" onclick="app.skillsItemsPrint();" title="Imprimer une liste de ces items.">
      <span class="glyphicon glyphicon-print"></span>
    </span>
    <span class="btn btn-default item_admin" onclick="app.skillsItemsExport();" title="Exporter ces items.">
      <span class="glyphicon glyphicon-export"></span>
    </span>
  </div>  
  <div class="btn btn-default" onclick="app.itemsUnselectAll();">Annuler</div>    
</div>
<div class="pull-right" id="items-students-selection-bloc">
  <span id="items-footer-texte"></span>
  <div class="btn-group">
    <div class="btn" id="items-footer-lock-btn" onclick="app.cptElevesSelectedLockToggle();"></div>
  </div>
  <div class="btn-group">
   <div class="btn btn-default" onclick="app.cptElevesUnselectAll();">Annuler</div>
 </div>
</div>  
</div>