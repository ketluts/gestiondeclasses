 <h4>Mes filtres</h4>
 <div class="flex-rows">
   <span class="btn itemsFiltersResetBtn" title="Réinitialiser les filtres" onclick="app.itemsResetFilters();">
    <span class="glyphicon glyphicon-refresh"></span>
  </span>
  <div class="flex-3">
    <select id="itemsFiltersList" class="form-control" onchange="app.itemsFiltersLoadFilter(this.value);">       </select>
  </div>
  <div class="flex-2">
    <span class="btn-group" >
      <span class="btn btn-default itemsFiltersSaveResetBtn" title="Enregistrer ce filtre" onclick="app.show('skills-filters-save-bloc');">
        <span class="glyphicon glyphicon-save"></span>
      </span> 
      <span class="btn btn-danger itemsFiltersSaveBtn" id="itemsFiltersDeleteBtn" title="Supprimer ce filtre" onclick="$(this).button('loading');app.itemsFiltersDeleteFilter();" data-loading-text="...">
       <span class="glyphicon glyphicon-trash"></span>
     </span>
   </span>
 </div> 
</div>
<!-- ################################# -->
<div id="skills-filters-save-bloc" class="well well-sm text-center">
 <input class="form-control itemsFiltersSaveBtn" placeholder="Nom du filtre" id="itemsFiltersSaveName"/>
 <div class="btn-group">
  <div class="btn btn-default btn-xs" onclick="app.hide('skills-filters-save-bloc');">
    Annuler
  </div> 
  <div class="btn btn-primary btn-xs itemsFiltersSaveBtn" id="itemsFiltersSaveBtn" onclick="$(this).button('loading');app.itemsFiltersSave();" data-loading-text="...">
   <span class="glyphicon glyphicon-save"></span> Enregistrer
 </div> 
</div>  
</div>
<div>
  <hr/>
  <h4>Par niveaux d'acquisition</h4>
  <div class="text-center">
   <div id="item_filters_acquisition_slider"></div>
   <br/>
   <br/>
   <select id="itemsFiltersByLevelRated" class="form-control" onchange="app.itemsFiltersSave(true);"> 
    <option value="all" selected>Items évalués ou non</option>
    <option value="rated">Seulement les items évalués</option>
  </select>
</div>
<div class="flex-rows">
  <div class="flex-1">
    <h5>Période</h5> 
      <select class="form-control" id="item_filters_periode"  onchange="app.itemsFiltersDateUpdate('periode',this.value);"></select>
    </div>
  </div>
  <div>
    <div class="flex-rows">        
     <div class="flex-1">
      <h5>Évalués du...</h5>
        <input class="form-control skills-filters-datepicker" id="item_filters_date_start" onchange="app.itemsFiltersDateUpdate('dates');" type="text" placeholder="JJ/MM/AAAA">
      </div>
      <div class="flex-1">
        <h5>au...</h5> <input class="form-control skills-filters-datepicker" id="item_filters_date_end" onchange="app.itemsFiltersDateUpdate();" type="text" placeholder="JJ/MM/AAAA"/>
      </div>
    </div> 
  </div>
</div>
<!-- ################################# -->
<div>
  <hr/>
  <div class="flex-1">
    <h4>Par cycle</h4>
    <select class="form-control" id="item_filters_cycle"  onchange="app.itemsFiltersCycleUpdate();">
      <option value="0">Tous les cycles</option>
      <option value="1">Cycle 1</option>
      <option value="2">Cycle 2</option>
      <option value="3">Cycle 3</option>
      <option value="4">Cycle 4</option>
    </select>
  </div>
</div>
<!-- ################################# -->
<div>
  <hr/>
  <div class="pull-right">
    <span class="btn-group">
     <div class="btn btn-default items-filters-selection-type btn-sm" id="items-filters-et" onclick="app.itemsSetSelectionType('et');">ET</div>
     <div class="btn btn-default items-filters-selection-type btn-sm" id="items-filters-ou" onclick="app.itemsSetSelectionType('ou');">OU</div>
   </span>
 </div>
 <h4>Par mot-clés <span class="btn btn-xs btn-primary"  id="items-filters-tags-group-btn" onclick="app.itemsTagsFamilyAdd();">
  <span class="glyphicon glyphicon-tags"></span>
Grouper</span>
<br/>
<input type="text" id="item-recherche" name="item-recherche" class="form-control" placeholder="Recherche" onkeyup="app.itemsFiltersSearch(this.value);" />
<small>Doit contenir</small>
</h4>
<div id="item_filters_tags_list" class="text-left flex-columns"></div>
<h4>
  <small>À exclure</small>
</h4>
<div id="item_filters_tags_exclude_list" class="text-left flex-columns"></div>
</div>