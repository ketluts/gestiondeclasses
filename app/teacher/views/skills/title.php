<div id="items-titre-bloc" class="text-right">
  <div class="btn-group pull-left item-eleve-view item-classe-view">
    <span class="btn btn-default" onclick="app.itemsPreviousView();"><span class="glyphicon glyphicon-fast-backward"></span></span>
    <span class="btn btn-default" onclick="app.itemsStudentPrev();"><span class="glyphicon glyphicon-chevron-left"></span></span>
    <span class="btn btn-default" onclick="app.itemsStudentNext();"><span class="glyphicon glyphicon-chevron-right"></span></span>
  </div>
  <div class="pull-left btn-group">
   <div  class="btn btn-default item-classe-view item-classe-list-view" onclick="app.itemsClassroomTabToggleView();" title="Afficher le tableau"><span class="glyphicon glyphicon-th"></span>       </div>
   <div  class="btn btn-default item-classe-view item-classe-tab-view" onclick="app.itemsClassroomTabToggleView();" title="Afficher la liste"><span class="glyphicon glyphicon-th-list"></span>     
   </div>
   <div  class="btn btn-default item-classe-view" onclick="app.itemsGenerateGroupsGo();" title="Générer des groupes"><span class="glyphicon flaticon-teamwork"> </span>     
   </div>
   <div  class="btn btn-default item-eleve-view" onclick="app.itemsStudentGraphToggleView();" title="Afficher le graphique"><span class="glyphicon glyphicon-stats"></span>     
   </div>
   <div  class="btn btn-default item-eleve-view" onclick="app.itemsStudentActivitiesToggleView();" title="Liste des activités"><span class="glyphicon flaticon-puzzle26"></span>      
   </div>
   
   <div  class="btn btn-default item-classe-view" onclick="app.itemsClassroomHelpGraphToggle();" title="Élèves ayant besoin d'aide"><span class="glyphicon glyphicon-warning-sign"></span>   
   </div>
<div  class="btn btn-default item-classe-view item-eleve-view" onclick="app.feedbacksInit();" title="Générateur de feedbacks"><span class="glyphicon flaticon-rocket60"></span>   
   </div>


 </div>
 <div class="pull-left form-inline"> 
  <input class="form-control item-classe-list-view item-eleve-view" list="list_progressions_activites" type="text" id="cpt-eval-activite" placeholder="Activité évaluée" />
  <input class="form-control item-classe-list-view item-eleve-view" type="text" id="cpt-eval-date" placeholder="JJ/MM/AAAA" /> 
  <input class="form-control item-classe-list-view item-eleve-view" type="text" id="cpt-eval-comment" placeholder="Commentaire" />  
</div>
<div class="btn-group pull-right">
  <span class="btn btn-default" id="skills-aside-openBtn" onclick="app.viewToggle('itemsAsideFiltres');$('#app').goTo();"><span class="glyphicon glyphicon-filter"></span></span>
</div>
<div class="btn-group pull-left">
  <div class="btn btn-default connexion-requise item-all-view" onclick="app.renderItemForm();">
    <span class="glyphicon glyphicon-plus"></span>
    <span class="screen-small">Ajouter un item</span>
  </div>  
</div>  
<div class="btn-group pull-left">
 <div title="Importer des résultats" class="btn btn-default item-all-view" onclick="app.viewToggle('items_import');$('#app').goTo();">
   <span class="glyphicon glyphicon-import"></span>
   <span class="screen-medium">Importer des items</span>
 </div>
</div>
<div class="btn-group pull-left">
 <div title="Importer des résultats" class="btn btn-default item-all-view" onclick="app.itemsResultsImportShow();">
   <span class="glyphicon glyphicon-import"></span>
   <span class="screen-medium">Importer des résultats</span>
 </div>
</div>
<div class="btn-group pull-right item-classe-list-view item-eleve-view">
  <select class="form-control" onchange="app.toggleSkillsAvisMode(this.value);" id="items-avis-mode">
    <option value="me">Mes avis</option>
    <option value="all">Tous les avis</option>
    <option value="students">Avis des élèves</option>
  </select>
</div>
<div class=" btn-group item-eleve-view item-classe-view pull-right" >
  <select id="items-symboly-select" class="form-control" onchange="app.itemsSelectSymbole(this.value);">
    <option value="square">&#9634;</option>
    <option value="star1">&#9734;</option>
    <option value="star2">&#9734;&#9734;</option>  
    <option value="star3">&#9734;&#9734;&#9734;</option>
  </select>
</div>
</div>