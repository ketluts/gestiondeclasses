<div id="items_import" class="well well-sm">
 <div class="btn btn-default btn-close" onclick="app.hide('items_import');">
   <span class="glyphicon glyphicon-remove"></span>
 </div>
 <div class="h3">
  Importer des items<br/>
  <small>Fichier JSON généré depuis un autre compte.</small>
</div>
<input id="skillsItemsImportFile" type="file" onchange="app.skillsItemsImportInit();"/>
</div>