<table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th>Date</th>
      <th>Feedbacks</th>
      <th>Code</th>
      <th>Options</th>

    </tr>
  </thead>
  <tbody>
    {{#each feedbacks}}
    <tr>
      <th>        
        {{feedback_date}}      
      </th>
      <th>        

       {{feedback_nbStudents}} / {{feedback_nbItems}}
       <br/>
       {{feedback_description}}
     </th>
     <th>        
      {{feedback_code}}      
    </th>  
    <th>        
      <div class="btn-group  pull-left">
        <button class="btn btn-xs btn-default" title="Visualiser" onclick="app.feedbacksRender({{feedback_id}});"><span class="glyphicon glyphicon-eye-open"></span></button>  
        <button class="btn btn-xs btn-default" title="Évaluer" onclick="app.feedbacksEvaluate({{feedback_id}});"><span class="glyphicon glyphicon-edit"></span></button>  
        <button class="btn btn-xs btn-default" title="Imprimer" onclick="app.feedbacksPrint({{feedback_id}});"><span class="glyphicon glyphicon-print"></span></button> 
      </div>
      <button class="btn btn-xs btn-default pull-right" title="Supprimer" onclick="app.feedbacksDelete({{feedback_id}});"><span class="glyphicon glyphicon-remove"></span></button>     
    </th>
  </tr>
  {{/each}}
</tbody>

</table>