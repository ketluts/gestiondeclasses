<div id="feedbacks" class="well well-sm flex-colums item-classe-view">
	<div class="h2">Liste des feedbacks</div>
	  <div class="btn btn-default" id="item-form-close" onclick="app.feedbacksInit();">
  <span class="glyphicon glyphicon-remove"></span>
</div>
<div class="flex-rows">
<div id="feedbacks-liste" class="flex-4"></div>
<div id="feedbacks-options" class="flex-1 text-center aside">
<div id="feedbacks-options-nbItems"></div>
<div id="feedbacks-options-nbEleves"></div>

	<button class="btn btn-primary" onclick="app.feedbacksGenerate();"><span class="glyphicon flaticon-rocket60"></span> Générer<br/> un feedback</button>

</div>
</div>

<div class="flex-rows">

  <div id="feedbacks-preview" class="flex-3"></div>

<div class="flex-1 text-center aside ">
	<div id="feedback-save">
 <textarea id="feedback-description" name="feedback-description" type="text" class="form-control" placeholder="Description (facultative)"></textarea>
<br/>
<button class="btn btn-primary" onclick="app.feedbacksSave();"><span class="glyphicon glyphicon-save"></span> Enregistrer</button>
</div>
</div>
</div>
</div>