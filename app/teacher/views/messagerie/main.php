<div id="template_messagerie" class="template box flex-1">
  <div class="flex-columns">
    <div class="toolbar">            
    <div class="pull-left">     
      <span class="btn btn-default" onclick="app.renderMessageForm();"><span class="glyphicon glyphicon-plus"></span> Nouvelle discussion</span>
    </div>   
    <div class="pull-right" id="discussion-selected-bloc">     
      Pour la sélection (<span id="discussion-selected-nb"></span>) <span class="btn btn-default btn-sm" onclick="app.delDiscussion('selection',false);"><span class="glyphicon glyphicon-trash"></span></span>
    </div>    
  </div>

<div id="message_form" class="well well-sm">
   <div  class="col-sm-8">
    <div id="message_destinataires">
      <div id="destinataires_liste" class="well well-sm"></div>
    </div>
    <input type="text" class="form-control"  id="message_subject" placeholder="Sujet" />
    <textarea class="form-control"  id="message_content" placeholder="Votre message" ></textarea> 
      </div>
    <div id="destinataires_select"  class="col-sm-4">
    </div>
    <div class="col-sm-12 text-center">
      <br/>
      <div class="btn-group">
        <span class="btn btn-default" onclick="app.hide('message_form');">Annuler</span>
        <span id="message_send_btn" class="btn btn-primary connexion-requise" onclick="$(this).button('loading');app.discussionAdd();" data-loading-text="Envoie en cours..."><span class="glyphicon glyphicon-send"></span> Envoyer</span>
      </div>
    </div>
  </div>
<div class="flex-rows">
    

  <div id="messages-liste" class="flex-2 messages_tab">
    
</div>
 <div id="messages-view" class="flex-2 aside">
     
     <div id="messages-view-subject" class="col-sm-12 text-center">
     </div>
     <br/>
     <div id="messages-view-toolbar" class="col-sm-12">
     </div>
     <div class=" well well-sm collapse" id="messages-view-participants">
      <h4>Liste des participants</h4>
      <div id="messages-view-participants-liste">
      </div>
    </div>
    <div id="messages-view-text" class="col-sm-12">
    </div>
  </div>

  </div>

  </div>
  
</div>

<template id="template-messagerie-destinatairesForm">
  <ul>
    <li>
      <button class="btn btn-default btn-sm" onclick="app.destinataireAdd('all_users','all_users','Tout le personnel');">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </button>
      <a data-toggle="collapse" href="#destinataires-users-list" role="button" aria-expanded="false" aria-controls="destinataires-users-list">Tout le personnel ({{users_length}})</a>
      <ul class="collapse" id="destinataires-users-list">
        {{#users}}
        <li>
          <button class="btn btn-default btn-sm" onclick="app.destinataireAdd('{{user_id}}','user','{{user_pseudo}}');">
            <span class="glyphicon glyphicon-chevron-left"></span>
          </button> {{user_pseudo}}
        </li>
        {{/users}}
      </ul>
    </li>
  </ul>
  <ul>
   {{#classes}}
   <li>
    <button class="btn btn-default btn-sm" onclick="app.destinataireAdd('{{classe_id}}','classe','{{classroom_name}}');">
      <span class="glyphicon glyphicon-chevron-left"></span>
    </button>
    <a data-toggle="collapse" href="#destinataires-students-list-{{classe_id}}" role="button" aria-expanded="false" aria-controls="destinataires-students-list-{{classe_id}}">{{classroom_name}} ({{classroom_nb_students}})</a>
    <ul class="collapse" id="destinataires-students-list-{{classe_id}}">
      {{#eleves}}
      <li>
        <button class="btn btn-default btn-sm" onclick="app.destinataireAdd('{{.}}','eleve','{{eleve_nom}}  {{eleve_prenom}}');">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </button> {{eleve_nom}}  {{eleve_prenom}}
      </li>
      {{/eleves}}
    </ul>
  </li>
  {{/classes}}
</ul>
</template>