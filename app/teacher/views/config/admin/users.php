<div id="config-users" class="box template_config template config_admin_bloc text-center">  
	<button class="btn btn-default btn-close" onclick="$('#app').goTo();">
		<span class="glyphicon glyphicon-chevron-up"></span>
	</button>
	<button class="btn btn-default config-toolbar" onclick="$('#config-import-bloc').css('display','block').goTo();"><span class="glyphicon glyphicon-plus"></span> Ajouter un utilisateur</button>   
	<span class="h3">Utilisateurs</span>
	<br/> 
	<div id="config_users" class="text-left"></div>
</div>