<div class="btn-group-vertical">
	<div class="btn btn-default" onclick="$('#config-periodes').goTo();"><span class="glyphicon glyphicon-calendar"> </span>  Années scolaires</div>
	<div class="btn btn-default" onclick="$('#config-school-disciplines').goTo();"><span class="glyphicon glyphicon-book"> </span> Disciplines</div>
	<div class="btn btn-default" onclick="$('#config-school-events').goTo();">Événements</div>
	<div class="btn btn-default" onclick="$('#config-plugins').goTo();"><span class="glyphicon glyphicon-cog"> </span> Plugins</div>
</div>
<hr/>
<div class="h4"><span class="glyphicon glyphicon-user"> </span> Nouvelles inscriptions</div>
<select id="config_subscriptions" class="form-control" onchange="app.serverConfigUPD({'subscriptions':this.value});" >
	<option value="1">Oui</option>
	<option value="0">Non</option>
</select>
<hr/>
<div class="h4"><span class="glyphicon glyphicon-lock"> </span> Sécurité</div>
<div class="text-right">
	<div class="input-group">
		<div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
		<input class="form-control" type="password" id="etablissement_old_passe" placeholder="Ancien mot de passe" />
	</div>
	<br/>
	<div class="input-group">
		<div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
		<input class="form-control" type="password" id="etablissement_nouveau_passe" placeholder="Nouveau mot de passe" />
	</div>
	<div class="input-group">
		<div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
		<input class="form-control" type="password" id="etablissement_nouveau_passe2" placeholder="Nouveau mot de passe (encore)" />
	</div>
	<br/>
	<div class="btn btn-default btn-sm" onclick="app.etablissementPasswordUPD();"><span class="glyphicon glyphicon-save"></span> Changer le mot de passe</div>
</div>
<hr/>
	<div class="btn-group-vertical">
	<div class="btn btn-danger" onclick="$('#config_school_delete').css('display','block').goTo();">Supprimer cet établissement</div>
</div>



