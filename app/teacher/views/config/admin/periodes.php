<div class="box config_admin_bloc template_config template text-center" id="config-periodes"> 
 <button class="btn btn-default btn-close" onclick="$('#app').goTo();">
  <span class="glyphicon glyphicon-chevron-up"></span>
</button>
 <span class="h3">Années scolaires et périodes</span>
 <br/><br/>
<div class="well well-sm text-left" id="periodes-millesime-form">
  <div id="millesime-form">  
    <div class="form-group">
      <label for="millesime_name">Nom de l'année scolaire</label>
      <input class="form-control" type="text" id="millesime_name" placeholder="Année scolaire AAAA/AAAA" />
    </div>
    <div class="form-group">
      <label for="millesime_start">Date de début</label>
      <input class="form-control" type="text" data-date-format="dd/mm/yyyy" id="millesime_start" placeholder="JJ/MM/AAAA" />
    </div>
    <div class="form-group">
     <label for="millesime_end">Date de fin</label>
     <input class="form-control" type="text" data-date-format="dd/mm/yyyy" id="millesime_end" placeholder="JJ/MM/AAAA" />
   </div>
   <div class="text-center">
     <div class="btn-group">
       <span class="btn btn-default" onclick="app.hide('periodes-millesime-form');">
        Annuler
      </span>
      <span  class="btn btn-primary connexion-requise" onclick="$(this).button('loading');app.addMillesime();" id="btn_save_millesime" data-loading-text="Enregistrement..."><span class="glyphicon glyphicon-save"></span> Enregistrer</span>
    </div>
  </div>
</div>
</div>
<div class="flex-rows">
  <div class="flex-4 flex-rows">
    <div id="periodes-liste" class="flex-1 text-left"></div>  
    <div id="periodes-edition-bloc" class="flex-1 text-left"></div>
  </div>
  <div class="flex-1 aside text-center">
    <button  class="btn btn-default connexion-requise" onclick="app.millesimeAddForm();"><span class="glyphicon glyphicon-plus"></span> Ajouter une année</button>
    <hr/>
    <span class="h3">Semaine <span id="config_week"></span></span>
    <br/>
    <br/>
    <span class="btn btn-default btn-sm connexion-requise" onclick="app.toggleWeeks();"><span class="glyphicon glyphicon-refresh"></span> 
    Inverser
  </span>
</div>
</div>
</div>

<template id="template-periodes">
  {{#millesimes}}
  <div class="panel panel-default">
    <div class="panel-heading">
     <span class="h4">{{millesime_name}}</span>
     <span>{{millesime_start}} - {{millesime_end}}</span>
     <br/>
     <span class="btn btn-default btn-sm connexion-requise" onclick="app.periodeAddForm({{millesime_id}});">
      <span class="glyphicon glyphicon-plus"></span> Ajouter une période
    </span>
    <div class="btn btn-default btn-sm" onclick="app.periodeEditionInit({{millesime_id}});">
     <span class="glyphicon glyphicon-cog"></span>
   </div>
 </div>
 <div class="panel-body">
  {{#millesime_periodes}}
  <div class="flex-rows config-periode">
    <span class="flex-1 valign">
     {{periode_titre}}
   </span>       
   <span >
    <span class="btn btn-default btn-sm connexion-requise" id="periode-{{periode_id}}-active-btn" data-loading-text="..." onclick="$(this).button('loading');app.setActivePeriode('{{periode_id}}');"> 
     <span class="glyphicon glyphicon-play"></span>
   </span>
   <span class="btn-group">
     <div class="btn {{periode_lockStyle}} btn-sm connexion-requise" onclick="app.periodeToggleLock('{{periode_id}}','{{periode_lock}}');">
       <span class="glyphicon glyphicon-lock "></span>
     </div>

     <div class="btn btn-default btn-sm connexion-requise" onclick="app.periodeEditionInit({{periode_id}});">
       <span class="glyphicon glyphicon-cog"></span> 
     </div>
   </span>
 </span>
</div>
{{/millesime_periodes}}
</div>
</div>
{{/millesimes}}
</template>

<template id="template-periodes-edition">
 {{#periode}}
 <div class="form-group">
  <label for="periode-edition-name">Nom de la période</label>
  <input class="form-control" type="text" id="periode-edition-name" value="{{periode_titre}}" />
</div>
<div class="form-group">
  <label for="periode-edition-start">Date de début</label>
  <input class="form-control periode-edition-date" type="text" id="periode-edition-start" value="{{periode_start_str}}" />
</div>
<div class="form-group">
  <label for="periode-edition-end">Date de fin</label>
  <input class="form-control periode-edition-date" type="text" id="periode-edition-end" value="{{periode_end_str}}" />
</div>
<div class="text-right">
 <div class="btn-group">
   <span class="btn btn-default" onclick="app.hide('periodes-edition-bloc');">
    Annuler
  </span>
  <span  class="btn btn-primary connexion-requise" onclick="$(this).button('loading');app.periodeUpdate({{periode_id}},'{{periode_parent}}','{{action}}');" id="btn_save_periode_edition" data-loading-text="Enregistrement..."><span class="glyphicon glyphicon-save"></span> Enregistrer</span>
</div>
</div>
<hr/>
{{#edit}}
<h3>Supprimer {{^periode_parent}} cette année scolaire{{/periode_parent}} {{#periode_parent}} cette période{{/periode_parent}}</h3>  
<h4>Les éléments suivants seront supprimés :</h4>
<ul>    
  {{^periode_parent}}
  <li>Les périodes</li>
  <li>Les classes</li>
  <li>Les groupes</li>
  <li>Les sociogrammes</li>
  {{/periode_parent}}
  <li>Les contrôles et les notes</li>
  <li>Les événements</li>
  <li>Les appréciations</li>    
</ul>
<div class="text-right">
 <span class="btn btn-danger" id="btn_delete_periode" onclick="$(this).button('loading');app.delPeriode({{periode_id}});" data-loading-text="Suppression...">
  <span class="glyphicon glyphicon-trash"></span>    Supprimer
</span>     
</div>
{{/edit}}
{{/periode}}
</template>