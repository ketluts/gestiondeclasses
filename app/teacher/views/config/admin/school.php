<div class="box config_admin_bloc template_config template" id="config-securite">
  <button class="btn btn-default btn-close" onclick="$('#app').goTo();">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </button> 
  <div class="h3 text-center">Sécurité</div>
  <br/><br/>
  <div class="flex-rows">
    <div class="flex-3 flex-columns">
      <div class="flex-rows">
        <div class="flex-1">
          <div class="h4"><span class="glyphicon glyphicon-user"> </span> Nouvelles inscriptions</div>
          <select id="config_subscriptions" class="form-control" onchange="app.serverConfigUPD({'subscriptions':this.value});" >
            <option value="1">Oui</option>
            <option value="0">Non</option>
          </select>
        </div>
        <div class="flex-1">
         <div class="text-right">
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
            <input class="form-control" type="password" id="etablissement_old_passe" placeholder="Ancien mot de passe" />
          </div>
          <br/>
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
            <input class="form-control" type="password" id="etablissement_nouveau_passe" placeholder="Nouveau mot de passe" />
          </div>
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
            <input class="form-control" type="password" id="etablissement_nouveau_passe2" placeholder="Nouveau mot de passe (encore)" />
          </div>
          <br/>
          <div class="btn btn-default btn-sm" onclick="app.etablissementPasswordUPD();"><span class="glyphicon glyphicon-save"></span> Changer le mot de passe</div>
        </div>
      </div>
    </div>
    <div id="config_school_delete">
      <hr/>
      <div class="h2">Suppression de l'établissement</div>
      <div class="h3">Attention !</div>
      <ul>
        <li>Si plusieurs utilisateurs se connectent à cet établissement, toutes leurs données en plus des vôtres vont être supprimées.</li>
        <li>Soyez sûr que cela ne posera de problème à personne.</li>
      </ul>
      <div class="text-center">
        <button class="btn btn-default" onclick="$('#config_school_delete').css('display','none');">Annuler</button>
        <button class="btn btn-danger" data-loading-text="Suppression en cours..." id="config_school_delete_btn" onclick="app.etablissementDEL();">Supprimer vraiment cet établissement !</button>
      </div>
    </div>
  </div>
  <div class="flex-1 aside text-center">
    <div class="btn btn-danger" onclick="$('#config_school_delete').css('display','block').goTo();">Supprimer cet établissement</div>
  </div>
</div>  
</div>