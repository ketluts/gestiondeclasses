<table id="config-disciplines-table" class="table">
  <thead>
    <tr>
      <th>
        Discipline
      </th>
      <th>
      </th>
    </tr>
  </thead>
  <tbody>
    {{#disciplines}}
    {{#isVisible}}
    <tr>
      <td>
        {{discipline_name}}    
      </td>
      <td>
        <span class="btn btn-sm btn-default" onclick="app.deleteDiscipline({{discipline_id}});"><span class="glyphicon glyphicon-remove"></span></span>
      </td>
    </tr>
    {{/isVisible}}
    {{/disciplines}}
  </tbody>
</table>