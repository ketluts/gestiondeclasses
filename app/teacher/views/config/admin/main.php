<div class="box template_config template config_admin_bloc">
	<div>	
		<div class="btn btn-default btn" onclick="$('#config-users').goTo();">
			<span class="glyphicon glyphicon-user"> </span> <br/>
			Utilisateurs
		</div>
		<div class="btn btn-default btn" onclick="$('#config-periodes').goTo();">
			<span class="glyphicon glyphicon-calendar"> </span> <br/>
			Années scolaires
		</div>
		<div class="btn btn-default btn" onclick="$('#config-school-disciplines').goTo();">
			<span class="glyphicon glyphicon-book"> </span> <br/>
			Disciplines
		</div>
		<div class="btn btn-default btn" onclick="$('#config-school-events').goTo();">
			<span class="glyphicon glyphicon-pushpin"></span><br/>
			Événements
		</div>
		<div class="btn btn-default btn" onclick="$('#config-plugins').goTo();">
			<span class="glyphicon glyphicon-cog"> </span><br/>
			Plugins
		</div>
		<div class="btn btn-default btn" onclick="$('#config-securite').goTo();">
			<span class="glyphicon glyphicon-lock"> </span><br/>
			Sécurité
		</div>
	</div>
	<hr/>
	<div class="flex-rows">
		<div class="flex-1">
			<div class="h3">Statistiques</div>
			<div id='config-stats'></div>  
		</div>
		<div class="flex-1">
			<div class="input-group">
				<div class="input-group-addon"> <span class="glyphicon glyphicon-link"></span> Lien professeur</div>
				<input type="text" class="form-control" id="connexion-link-prof" value="">
			</div>
			<div class="input-group">
				<div class="input-group-addon"> <span class="glyphicon glyphicon-link"></span> Lien élève</div>
				<input type="text" class="form-control" id="connexion-link-eleve" value="">
			</div>
		</div>
	</div>	
</div>
<?php require_once('users.php'); ?>
<?php require_once('import.php'); ?>  
<?php require_once('organizers.php'); ?>
<?php require_once('disciplines.php'); ?> 
<?php require_once('periodes.php'); ?> 
<?php require_once('events.php'); ?>
<?php require_once('school.php'); ?>
<?php require_once('plugins.php'); ?>
<template id='template-stats'>
	<div class="col-sm-12 col-lg-12">Classes <span class='badge'>{{stats.classes}}</span></div>
	<div class="col-sm-12 col-lg-12">Élèves <span class='badge'>{{stats.eleves}}</span> / G <span class='badge'>{{stats.garcons}}</span> - F <span class='badge'>{{stats.filles}}</span></div>
	<div class="col-sm-12 col-lg-12">Évènements <span class='badge'>{{stats.events}}</span></div>
	<div class="col-sm-12 col-lg-12">Notes <span class='badge'>{{stats.notes}}</span></div>
	<div class="col-sm-12 col-lg-12">Pièces <span class='badge'>{{stats.etoiles}}</span></div>
	<div class="col-sm-12 col-lg-12">Items <span class='badge'>{{stats.items}}</span></div>
</template>  