<div class="box config_admin_bloc template_config template text-center" id="config-plugins">
	<button class="btn btn-default btn-close" onclick="$('#app').goTo();">
		<span class="glyphicon glyphicon-chevron-up"></span>
	</button>
	<span class="h3">Plugins</span>
	<br/><br/>
	<div class="flex-rows text-left">
		<div class="flex-4 flex-columns">
			<div id="config-plugins-descriptions">
			</div>
		</div>
		<div class="flex-1 aside">
		</div>
	</div> 
</div>