<div class="box config_admin_bloc template_config template text-center" id="config-school-events"> 
  <span class="btn btn-default btn-close" onclick="$('#app').goTo();">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </span>
   <span class="h3">Légendes des événements</span>
<br/><br/>
  <div class="flex-rows text-left">
    <div class="flex-1">
      Les événements sans légende n'apparaîtront pas sur le compte des élèves.
        <div id="legends">
        </div>
        <br/>
        <span  class="btn btn-primary" onclick="app.saveLegends();$(this).button('loading');" id="btn_save_legends" data-loading-text="Enregistrement..."><span class="glyphicon glyphicon-save"></span> Enregistrer</span>
      </div>
      <div class="flex-1"></div>
    </div>
  </div>