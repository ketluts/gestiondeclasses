<div class="box config_admin_bloc template_config template text-center" id="config-school-disciplines"> 
  <span class="btn btn-default btn-close" onclick="$('#app').goTo();">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </span>
  <button class="btn btn-default config-toolbar" onclick="app.viewToggle('config-disciplines-form');"><span class="glyphicon glyphicon-plus"></span> Ajouter une discipline</button>   
  <span class="h3">Disciplines</span>
  <div class="flex-rows text-left">
    <div class="flex-4">   
      <div id="config-disciplines-form" class="well well-sm text-center">
        <input class="form-control" type="text" id="discipline-name" placeholder="Nom de la discipline" />
        <br/>
        <div class="btn-group"> 
          <button class="btn btn-default" onclick="app.viewToggle('config-disciplines-form');">Annuler</button>  
          <span  class="btn btn-primary connexion-requise" onclick="$(this).button('loading');app.addDiscipline();" id="btn_save_discipline" data-loading-text="Enregistrement..."><span class="glyphicon glyphicon-save"></span> Enregistrer</span></div>
        </div>
        <div id="config-disciplines-liste"></div>
      </div>
      <div class="flex-1 aside"></div>
    </div>
  </div>
  <script id="template-disciplines" type="text-template">
    <?php require_once('template-disciplines.php'); ?> 
  </script>  
  <template id="template-config-disciplines-select">
   {{#disciplines}}
   <option value="{{discipline_id}}">{{discipline_name}}</option>  
   {{/disciplines}}
 </template>