<div id="config-import-bloc" class="box template_config template config_admin_bloc">  
 <button class="btn btn-default btn-close" onclick="app.hide('config-import-bloc');$('#app').goTo();"><span class="glyphicon glyphicon-remove"></span></button>
 <div class="col-md-6">
   <h3>Nouvel utilisateur</h3>
   <input type="text" class="form-control" id="config-new-user-nom" placeholder="NOM"/>
   <input type="text" class="form-control" id="config-new-user-prenom" placeholder="Prénom"/>
   <input type="text" class="form-control" id="config-new-user-pseudo" placeholder="Pseudo"/>
   <input type="text" class="form-control" id="config-new-user-mail" placeholder="Mail"/>
   <div class="text-right">
    <button class="btn btn-primary" onclick="app.configImportUserAdd();"><span class="glyphicon glyphicon-plus"></span> Préparer pour l'ajout</button>
  </div>
</div>
<div class="col-md-6 aside">
  <h3>Importer depuis un tableur<br/>
    <small>Colonne 1 : NOM  / Colonne 2 : Prénom <br/> Colonne 3 : Pseudo / Colonne 4 : Mail</small>
  </h3>
  <textarea id="config-import-tableur" class="form-control" placeholder="Copier/Coller depuis le tableur."></textarea>
  <div class="text-right">
    <button class="btn btn-primary" id="config-import-tableur-btn" onclick="$(this).button('loading');app.configImportUserInit();" data-loading-text="Import en cours..."><span class="glyphicon glyphicon-import"></span> Importer</button>
  </div>
</div>
<div class="col-md-12">
  <h3>
    <small>Vous pourrez vérifier des données avant d'enregistrer.</small>
  </h3>
  <hr/>
  <div id="config-import-users-bloc"></div>
  <br/>
  <div class="well well-sm">
    Le <span class="bold">pseudo par défaut</span> est, selon les informations données :
    <br/>
<ul>
  <li>la première lettre du prénom et le nom (John DOE -> jdoe)</li>
  <li>le nom en minuscule</li>
</ul>
<br/>
  Le <span class="bold">mot de passe par défaut</span> est, selon les informations données :
    <br/>
<ul>
  <li>le pseudo en minuscule</li>
  <li>la première lettre du prénom et le nom (John DOE -> jdoe)</li>
  <li>le nom en minuscule</li>
</ul>

</div>

 <div class="text-right">
  <div class="btn-group">
    <button class="btn btn-default" onclick="app.configImportCancel();"><span>Annuler</span></button>
    <button class="btn btn-primary" id="config-import-tableur-btn-save" onclick="$(this).button('loading');app.configImportUserSave();" data-loading-text="Enregistrement..."><span class="glyphicon glyphicon-save"></span> Enregistrer</button>
  </div>
</div>
</div>
</div>