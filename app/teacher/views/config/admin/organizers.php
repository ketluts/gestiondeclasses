<div id="config_user_view" class="box template_config template config_admin_bloc">
  <button class="btn btn-default btn-close" onclick="app.hide('config_user_view');$('#app').goTo();"><span class="glyphicon glyphicon-remove"></span></button>
  <div  class="h2" id="config_user_view_pseudo"></div>
  <br/>
  <div class="h4">Cahiers de texte</div>
  Sélectionnez une classe <select id="config_user_view_cdt_select" onchange="app.configUserCdtView(this.value);"></select>
  <hr/>
  <div class="flex-rows">
    <div  id="config_user_view_cdt"  class="flex-3"></div>
    <div  class="flex-1 aside text-center" id="cdt_visas">
     <button id="cdt_visas_btn" class="btn btn-default connexion-requise" onclick="app.cdtVisaAdd();"><span class="glyphicon flaticon-verified9"></span> Viser ce cahier</button>
     <br/>
     <div id="cdt_visas_liste"></div>
   </div>
 </div>
</div>