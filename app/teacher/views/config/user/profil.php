<div class="config_user box template_config template">
  <div class="flex-rows">  
    <div class="flex-1 flex-rows">
      <div class="flex-1">
        <h3>Informations</h3>
        <div class="input-group">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-chevron-right"></span>
          </div>
          <input class="form-control" onkeypress="app.show('btn_save_config');" id="user_nom" placeholder="NOM"/>
        </div>       
        <div class="input-group">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-chevron-right"></span>
          </div>
          <input class="form-control" onkeypress="app.show('btn_save_config');" id="user_prenom" placeholder="Prénom"/>
        </div>
        <br/>
        <div class="input-group">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-education"></span>
          </div>
          <select class="form-control" onchange="app.show('btn_save_config');" id="matiere">
          </select>
        </div> 
        <div class="bg-info" id="ifNoDisciplinesInfo"></div>      
        <div class="input-group">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-envelope"></span>
          </div>
          <input class="form-control" onkeypress="app.show('btn_save_config');" id="user_mail" placeholder="Adresse mail"/>
        </div>
      </div>
      <div class="flex-1 flex-columns" id="config_avatar_block">
        <div id="config_avatar"></div>
        <div class="h3" id="config_pseudo"></div>
      </div>
    </div>
    <div class="flex-1 aside">
      <h3>Sécurité</h3>
      <div class="input-group">
        <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
        <input class="form-control" type="password" id="user_old_passe" placeholder="Ancien mot de passe"/>
      </div>
      <br/>
      <div class="input-group">
        <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
        <input class="form-control" onkeypress="app.show('btn_save_config');" type="password" id="user_new_passe" placeholder="Nouveau mot de passe"/>
      </div>
      <div class="input-group">
        <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
        <input class="form-control" type="password" id="user_new_passe_bis" placeholder="Nouveau mot de passe (encore)"/>
      </div>
    </div>
  </div>
  <button class="btn btn-primary config_btn" id="btn_save_config" onclick="app.userUPD();$(this).button('loading');" data-loading-text="Enregistrement...">
    <span class="glyphicon glyphicon-save"></span> Enregistrer
  </button>  
</div>


