<div class="config_user box  template_config template"> 
  <button class="btn btn-default btn-close" onclick="$('#app').goTo();">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </button>
  <div class="col-md-6">
   <div class="h3">Affichage des classes
    <br/>
    <small>Sélectionnez les classes à afficher</small>
    <br/>
    <div class="btn-group">
      <button class="btn btn-default btn-sm" onclick="app.configClassesSelectAll();">
        <span class="glyphicon glyphicon glyphicon-ok "></span>
        <span class="screen-small">Tout sélectionner</span>
      </button>
      <button class="btn btn-default btn-sm" onclick="app.configClassesUnselectAll();">
        <span class="glyphicon glyphicon glyphicon-remove "></span>
        <span class="screen-small">Tout retirer</span>
      </button>
    </div>
  </div>     
  <div id="config_classes_list"></div> 
</div>
<div class="col-md-6 aside">
 <h3>Affichage de l'agenda</h3>
 Afficher les weekends : <select id="config_weekends" class="form-control" onchange="app.configSetWeekends(this.value);" >
   <option value="true">Oui</option>
   <option value="false">Non</option>
 </select>
 <br/>
 Horaire de début : <select id="config_organizer_min_time" class="form-control" onchange="app.configSetOrganizerTime();" >
   <option value="00:00:00">00h00</option>
   <option value="01:00:00">01h00</option>
   <option value="02:00:00">02h00</option>
   <option value="03:00:00">03h00</option>
   <option value="04:00:00">04h00</option>
   <option value="05:00:00">05h00</option>
   <option value="06:00:00">06h00</option>
   <option value="07:00:00">07h00</option>
   <option value="08:00:00">08h00</option>
 </select>
 <br/>
 Horaire de fin : <select id="config_organizer_max_time" class="form-control" onchange="app.configSetOrganizerTime();" >
   <option value="16:00:00">16h00</option>
   <option value="17:00:00">17h00</option>
   <option value="18:00:00">18h00</option>
   <option value="19:00:00">19h00</option>
   <option value="20:00:00">20h00</option>
   <option value="21:00:00">21h00</option>
   <option value="22:00:00">22h00</option>
   <option value="23:00:00">23h00</option>
   <option value="24:00:00">24h00</option>
 </select>
 <br/>
 Précision : <select id="config_organizer_slotDuration" class="form-control" onchange="app.configSetOrganizerSlotDuration(this.value);" >
   <option value="00:15:00">00h15</option>
   <option value="00:30:00">00h30</option>
   <option value="01:00:00">1h00</option>
 </select>
 <br/>
 <h3>Affichage des événements<br/>
  <small>Sélectionnez les événements à afficher</small></h3>            
  <div id="events_list"></div>
  <hr/>
  <h3 id="config_alertes">Création d'alertes
    <br/>
    <small>Les événements barrés ne déclencheront pas d'alertes.</small>
  </h3>
  <div class="btn-toolbar text-left"  role="toolbar">
    <div class="btn-group">
      <button class="btn btn-primary" onclick="app.renderAlertesForm();">
        <span class="glyphicon glyphicon-plus"></span> Ajouter une alerte
      </button>
    </div> 
  </div>
  <div id="alertes" class="flex-columns"></div>
  <div id="alertesForm">
    <div class="flex-rows">
      <div id="alerteEventsWatched" class="flex-3"></div>
      <div class="flex-1">     
        x
        <select id="alerteEventsMax">
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option> 
          <option value="6">6</option>
          <option value="7">7</option>
          <option value="8">8</option>
          <option value="9">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>  
        </select>      
        <span class="glyphicon glyphicon-arrow-right glyphicon-2x"></span>
      </div>
      <div id="alerteEventTrigerred" class="flex-3"></div>
    </div>
    <div class="btn-group config_btn">
      <button class="btn btn-default" onclick="app.hide('alertesForm');$('#config_alertes').goTo();">Annuler</button> 
      <button class="btn btn-primary" onclick="app.addAlerte();"><span class="glyphicon glyphicon-save"></span> Enregistrer</button>
    </div>
  </div>
</div>
</div>