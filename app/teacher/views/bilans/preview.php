<div id="bilans-student-preview">
</div>
<template id="bilans-preview">
  <meta charset="UTF-8" />
  <style>
  .nobreak {
    page-break-after: always;
  }
  .h2{ 
   margin-bottom: 5px;     
   font-size: 12pt;      
   font-weight: bold;
 }
 .h4{
  margin-bottom: 5px;
  font-weight: bold;
  font-size: 10pt;  
}
.bilans-preview-data table,.bilans-preview-data td,.bilans-preview-data th{  
  border: solid 1px #000000;
}
.bilans-preview-data  td,.bilans-preview-data th{
  padding: 4px;
  text-align: center;  
}
.bilans-preview-data table{
 width: auto !important;
 border-collapse: collapse;
 font-size: 8pt;
 border: 1.5px solid black;
}
.bilans-preview-data .bilans-tests-table-moyenne,.bilans-preview-data th{
  background-color: #F0F0F0;
}
.bilans-header{
 font-weight: bold;
 text-align: center;
 font-size: 16pt; 
}
.cdt_title{
  font-weight: bold;
}
.cdt_item{
  border-left: 4px solid;
  padding-left: 5px;
}
.cdt_date{
  padding-bottom: 5px;
  padding-top: 5px;
}
.bilans-title{  
  margin-top: 10px;
  margin-bottom: 5px;
  background-color: #E3E3E3;
  font-weight: bold;
} 
.bilans-item-name{
  text-align: left !important;
}
.student_card{
  border:1px dashed black;
  width: 405px;
  margin: 5px;
  display: inline-block;
  white-space: nowrap;
  padding: 5px;
  overflow: hidden;
  font-size: 12pt;
  font-family: "Liberation", serif !important;
}
</style>
</template>
<div id="bilans-temp">
</div>