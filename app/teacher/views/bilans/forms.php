 <div id="bilans-form">
   <div class="well well-sm">
     <label class="switch">
      <input type="checkbox" id="bilansFormsNotes">
      <span class="slider round"></span>
    </label>
    <span class="h3">Notes <small>Relevés par trimestre</small></span>
  </div> 
  <div class="well well-sm">
    <label class="switch">
      <input type="checkbox"  id="bilansFormsSkills">
      <span class="slider round"></span>
    </label>
    <span class="h3">Compétences <small>Relevé des acquis</small></span>
    <br/>
    <br/>
    <div class="h4">Mode</div>
    <div>
      <input type="radio" onclick="app.itemsSyntheseSetMode('categories');" name="bilanItemsSyntheseMode" class="itemsSyntheseMode_categories" id="bilanItemsSyntheseMode_categories" checked="checked"><label for="bilanItemsSyntheseMode_categories">Par catégories</label>
      <br/>
      <input type="radio" onclick="app.itemsSyntheseSetMode('sous_categories');" name="bilanItemsSyntheseMode" class="itemsSyntheseMode_sous_categories" id="bilanItemsSyntheseMode_sous_categories"><label for="bilanItemsSyntheseMode_sous_categories">Par sous-catégories</label>
      <br/>
      <input type="radio" onclick="app.itemsSyntheseSetMode('tags');" name="bilanItemsSyntheseMode" class="itemsSyntheseMode_tags" id="bilanItemsSyntheseMode_tags"><label for="bilanItemsSyntheseMode_tags">Par mot-clés</label>
    </div>
  </div>
  <div class="well well-sm">
   <label class="switch">
    <input type="checkbox" id="bilansFormsEvents">
    <span class="slider round"></span>
  </label>
  <span class="h3">Cahiers de texte / Événements</span>
  <br/>
  <br/>
  <div class="h4">Inclure les dates</div>
  Du <input type="text" data-date-format="dd/mm/yyyy" id="agenda-export-date-start" name="agenda-export-date-start" placeholder="JJ/MM/AAAA" />
  au <input type="text" data-date-format="dd/mm/yyyy" id="agenda-export-date-end" name="agenda-export-date-end" placeholder="JJ/MM/AAAA" />
  <br/>
  <br/>
  <div class="h4">Inclure les événements</div>
  <div class="btn btn-default export-event-icon export-event-icon-glyphicon-blackboard" id='agenda-export-glyphicon-blackboard' onclick="app.agendaExportTypeToggle('glyphicon-blackboard');"><span class="glyphicon glyphicon-blackboard"></span> En classe</div>
  <div class="btn btn-default export-event-icon export-event-icon-glyphicon-home" id='agenda-export-glyphicon-home' onclick="app.agendaExportTypeToggle('glyphicon-home');"><span class="glyphicon glyphicon-home"></span> Devoirs</div>
  <div class="btn btn-default export-event-icon export-event-icon-glyphicon-comment" id='agenda-export-glyphicon-comment' onclick="app.agendaExportTypeToggle('glyphicon-comment');"><span class="glyphicon glyphicon-comment"></span> Commentaire</div>
</div>
<div class="well well-sm">
     <label class="switch">
      <input type="checkbox" id="bilansFormsCodes">
      <span class="slider round"></span>
    </label>
    <span class="h3">Codes <small>À distribuer aux élèves</small></span>
  </div> 
<form id="export-pdf-form" method="post">
  <input type="text" name="export-pdf-data" id="export-pdf-data">       
  <input type="text" name="export-pdf-classe" id="export-pdf-classe">      
  <input type="text" name="export-pdf-titre" id="export-pdf-titre">
</form>
</div>