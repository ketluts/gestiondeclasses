<div id="template_bilans" class="template box flex-rows"> 
 <div class="flex-1 flex-columns" id="bilans-classes"></div>
 <div class="flex-5 flex-columns" id="bilans-main">    
   <div class="flex-1 flex-rows">   
    <div class="flex-1">
     <div class="h4">Classes (<span id="bilans-classrooms-count">0</span>)</div>
     <div id="bilans-classrooms"></div>
     <hr/>
     <div class="h4">Élèves (<span id="bilans-students-count">0</span>)</div>
       <div id="bilans-students"></div>
    </div>  
    <div class="flex-2 aside">
      <?php
        require_once('forms.php');
        require_once('preview.php');
      ?>
    </div>
    <div class="flex-1 aside flex-columns">
      <span class="btn btn-primary" onclick="app.bilansCreate();"><span class="glyphicon glyphicon-print"></span> Générer le PDF</span>
    </div>
  </div>   
</div>
</div>

<template id="template-code-eleve"> 
  {{#eleves}} 
<div class="student_card">
  <div style="display:inline-block; width:85%;">
   <span class="h5"><strong>Élève</strong> : {{eleve_prenom}} {{eleve_nom}}</span><br/> 
<span class="h5"><strong>URL</strong> : {{../appUrl}}student/</span><br/>
<span class="h5"><strong>Établissement</strong> : {{../school}}</span><br/>
<span class="h5"><strong>Code élève</strong> : {{eleve_token}}</span>
</div>
<div style=" display:inline-block; width:15%; text-align:center;">
  <img src="{{../serveur}}/lib/monsterid/monsterid.php?seed={{eleve_id}}&size=60" /><br/>  
  </div>
  </div>
{{/eleves}} 
</template>