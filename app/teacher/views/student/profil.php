  <div class="flex-rows">  
    <div class="flex-1 text-center">    
 <img id="student-picture" src='assets/favicons/apple-touch-icon-72x72.png'  />
  <div class="h5">
          <span class="glyphicon glyphicon-lock"></span> 
          <span id="eleve_token"></span>  
        </div>
    </div> 
    <div class="flex-columns flex-4">
      <span id="eleve-nom"></span>
      <div>
        <span id='eleve-age'></span>
        <span id='eleve-delegue'></span>
       </div>      
      <div id="eleve-cycle" class="admin"></div>
    </div>  
  </div>   
  <div id="eleve-niveau">
   <div id="eleve-jauge-notes"></div>
   <div id="eleve-jauge-acquis"></div> 
 </div>


<div class="flex-rows">

<div class="flex-1">
  <span class="h3">Sociogramme <button class="sociogrammeType btn btn-xs btn-default" onclick="app.sociogrammeToggleType();">
 Mode avancé</button> </span>
<br/><br/>
 <div id="student-sociogramme-form-simple" class="student-sociogramme-form panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">
      <span class="glyphicon flaticon-cell9 screen-small"> </span> Proche de
    </h3>
  </div>
  <div class="list-group">
   <select id="relation_1" onchange="app.saveRelations();" class="student-relations-select connexion-requise form-control">
   </select>
   <select id="relation_2" onchange="app.saveRelations();" class="student-relations-select connexion-requise form-control">
   </select>
 </div>
 <div class="panel-heading">
  <h3 class="panel-title"><span class="glyphicon flaticon-cell9 screen-small"> </span> A éloigner de</h3>
</div>
<div class="list-group">
 <select id="relation_3" onchange="app.saveRelations();" class="student-relations-select connexion-requise form-control">
 </select>
</div>
</div>

<div id="student-sociogramme-form-avance" class="student-sociogramme-form panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">
      <span class="glyphicon flaticon-cell9 screen-small"> </span> Avec qui aimerais-tu jouer ou travailler ?
    </h3>
  </div>
  <div class="list-group">
   <select id="relation_4" onchange="app.saveRelations();" class="student-relations-select connexion-requise form-control">
   </select>
 </div>
 <div class="panel-heading">
  <h3 class="panel-title">
    <span class="glyphicon flaticon-cell9 screen-small"> </span> Avec qui n’aimerais-tu pas jouer ou travailler ?
  </h3>
</div>
<div class="list-group">
 <select id="relation_5" onchange="app.saveRelations();" class="student-relations-select connexion-requise form-control">
 </select>
</div>
<div class="panel-heading">
  <h3 class="panel-title">
    <span class="glyphicon flaticon-cell9 screen-small"> </span> Par qui penses-tu être choisi ?
  </h3>
</div>
<div class="list-group">
 <select id="relation_6" onchange="app.saveRelations();" class="student-relations-select connexion-requise form-control">
 </select>
</div>
<div class="panel-heading">
  <h3 class="panel-title">
    <span class="glyphicon flaticon-cell9 screen-small"> </span> Par qui penses-tu avoir été rejeté ?
</h3>
</div>
<div class="list-group">
  <select id="relation_7" onchange="app.saveRelations();" class="student-relations-select connexion-requise form-control">
  </select>
</div>
</div>
</div>
<div class="flex-1 text-center aside">
<div id="eleve-intelligences"></div>
</div>
</div>







 


<template id="template-student-relations-form">
  <option value="false" selected>Choisir un élève</option>
  {{#eleves}} 
  <option value="{{eleve_id}}">{{renderNom}}</option>
  {{/eleves}} 
</template>