<div id='eleve_notes' class="box">   
  <div class="btn btn-default btn-close" onclick="$('#app').goTo();">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </div>
  <div class="flex-columns">
    <div class="h2 text-center">Notes</div>
    <div class="flex-rows">
     <div class="flex-4">
       <div id="note_eleve_edit" class="text-left"></div>
       <div id='eleve_notes_LST'></div>
     </div>
     <div class="flex-1 aside text-center">
     </div>
   </div>   
 </div>
</div>