<div class="box text-center" id="eleve_agenda_bloc"> 
 <div class="flex-rows">
  <div class="flex-4 flex-columns">
    <div class="toolbar">
      <div class="pull-left">
        <div class="btn-group">
          <div class="btn btn-default eleve--events-view" id="eleve-events-view-calendar"  onclick="app.eleveToggleViews('calendar');">
            <span class="glyphicon glyphicon-calendar"></span> Calendrier
          </div>
          <div class="btn btn-default eleve-events-view" id="eleve-events-view-liste" onclick="app.eleveToggleViews('liste');">
            <span class="glyphicon glyphicon-th-list"></span> Liste
          </div>
        </div>  
        <div class="btn-group" id="eleve-organizer-toolbar">  
         <div class="btn btn-default connexion-requise" onclick="app.agendaEditionInit();">
           <span class="glyphicon glyphicon-plus"></span> 
           <span class="screen-small">Nouvel événement</span>
         </div> 
       </div> 
     </div>  
     <div class="clearfix"></div>
   </div>
   <div class="well well-sm" id="eleve_event_edition">
     <div title="Coller" class="btn btn-primary btn-paste" id="eleve_agenda_paste" onclick="app.agendaEventPaste();">
       <span class="glyphicon glyphicon-copy"></span> Coller
     </div>
     <div class="btn btn-default btn-close" onclick="app.hide('eleve_event_edition');">
      <span class="glyphicon glyphicon-remove"></span>
    </div>
    <div class="h3">
      <div id="eleve_event_edition_bloc_title">Nouvel événement </div>
    </div>
    <div class="text-left">
      <input id="eleve_event_date" class="btn btn-default btn-sm"/>
      <div id="eleve-agenda-next" class="clearfix"></div>
    </div>
    <br/>
    <div class="input-group">
      <span class="input-group-btn">
        <input id="eleve_event_color" class="form-control" type="color"/> 
      </span>
      <input class="form-control" type="text" list="list_event_titre" id="eleve_event_title" placeholder="Titre de l'événement" onfocus="app.enableKeyboardShorcuts=false;" onblur="app.enableKeyboardShorcuts=true;"/>
    </div>
    <br/>    
    <div id="eleve_agenda_editor" class="editable-container"></div>
    <hr/>
    <div class="btn-group text-left" >
      <div class="btn btn-default event-icon event-icon-glyphicon-blackboard" onclick="app.setAgendaEventIcon('glyphicon-blackboard');"><span class="glyphicon glyphicon-blackboard"></span> En classe</div>
      <div class="btn btn-default event-icon event-icon-glyphicon-home" onclick="app.setAgendaEventIcon('glyphicon-home');"><span class="glyphicon glyphicon-home"></span> Devoirs</div>
      <div class="btn btn-default event-icon event-icon-glyphicon-comment" onclick="app.setAgendaEventIcon('glyphicon-comment');"><span class="glyphicon glyphicon-comment"></span> Commentaire</div>
    </div>
    <div id="eleve_event_btn" class="btn btn-primary pull-right pull-bottom connexion-requise event-btn" onclick="$(this).button('loading');app.agendaEventAdd();" data-loading-text="Enregistrement..."><span class="glyphicon glyphicon-save"></span> Enregistrer</div> 
  </div>
  <div id='eleve_calendar' class="eleve_view main">
    <div id='eleve_agenda'></div>
    <div id="eleve_calendar_events_compteur"></div>       
  </div>
  <div id='eleve_events' class="eleve_view">
    <div class="h3">Événements</div>
    <br/>
    <div class="flex-rows">
      <div id="eleveEventsLST" class="flex-2"></div>
      <div id="eleveEventsLST_details" class="flex-1"></div>
    </div>
  </div>
</div>
<div class="flex-1 aside text-center">
 <div class="btn btn-default pull-right" onclick="$('#app').goTo();">
  <span class="glyphicon glyphicon-chevron-up"></span>
</div>  
<div class="h3">
  <div id="eleve-agenda-date" class="agenda-date"></div>
  <small>Semaine <span id="eleve-agenda-semaine"></span></small>
</div>
<hr/>
<div class="btn-group">
  <div class="btn btn-default btn-md eleve-agenda-view" id="eleve-agenda-view-agendaDay" onclick=" $('#eleve_agenda').fullCalendar( 'changeView', 'agendaDay' )">1 j.</div>
  <div class="btn btn-default btn-md eleve-agenda-view" id="eleve-agenda-view-basicThreeDay" onclick=" $('#eleve_agenda').fullCalendar( 'changeView', 'basicThreeDay' )">3 j.</div>
  <div class="btn btn-default btn-md eleve-agenda-view" id="eleve-agenda-view-basicWeek" onclick=" $('#eleve_agenda').fullCalendar( 'changeView', 'basicWeek' )">Semaine</div>
  <div class="btn btn-default btn-md eleve-agenda-view" id="eleve-agenda-view-month" onclick=" $('#eleve_agenda').fullCalendar( 'changeView', 'month' )">Mois</div>
</div>
<div class="btn-group">
  <div class="btn btn-default btn-md" onclick=" $('#eleve_agenda').fullCalendar( 'prev')">
    <span class="fc-icon fc-icon-left-single-arrow"></span>
  </div>
  <div class="btn btn-default btn-md" onclick="app.agendaGoToday();">Aujourd'hui</div>
  <div class="btn btn-default btn-md" onclick=" $('#eleve_agenda').fullCalendar( 'next')">
    <span class="fc-icon fc-icon-right-single-arrow"></span>
  </div>
</div>
<div class="col-sm-12" id="eleve-legende">
  <hr/>  
  <div id="eleve_calendar_legende"></div>
</div>
</div>
</div>
</div>