<div class="template template_eleve">
<div class="flex-columns box">
 <?php require_once('toolbar.php'); ?>  
 <div class="flex-rows flex-1">
   <div class="flex-4 flex-rows">
    <div class="flex-2" id="eleve-infos">
      <?php require_once('profil.php'); ?>  
    </div>
    <div class="flex-1"> 
 <div class="h3">Mémo</div>
<textarea id="eleve_memo" class="form-control" onblur="app.studentSetMemo(this.value);"  oninput="app.updateTextareaHeight(this);"></textarea>

    </div>
   

    
  </div>
  <div class="flex-1 aside text-center">
    <?php require_once('aside.php'); ?>  
  </div>
</div>
</div>
<?php 
require_once('profilUPD.php');
require_once('organizer.php');
require_once('notes.php');
require_once('challenges.php'); 
?>
</div>