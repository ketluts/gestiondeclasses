<div id='eleve_infos' class="box"> 
 <h2>Informations</h2>    
 <div class="btn btn-default btn-close" onclick="app.viewToggle('eleve_infos');$('#app').goTo();">
   <span class="glyphicon glyphicon-remove"></span>
 </div>
 <div class="flex-rows">
   <div class="flex-3">
    <div class="flex-rows">
      <div class="flex-1 text-center">
       <div id="eleve_photo"></div>
     </div>
     <div class="flex-4">
      <form class="form-horizontal">
        <div class="form-group">
          <label for="eleve_nom" class="col-sm-3 control-label">NOM</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="eleve_nom" onkeypress="app.show('eleve_infos_save');" placeholder="DOE">
          </div>
        </div>
        <div class="form-group">
          <label for="eleve_prenom" class="col-sm-3 control-label">Prénom</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="eleve_prenom" onkeypress="app.show('eleve_infos_save');" placeholder="John">
          </div>
        </div>
        <div class="form-group">
          <label for="eleve_birthday" class="col-sm-3 control-label">Date de naissance</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="eleve_birthday" pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}" onkeypress="app.show('eleve_infos_save');" onchange="app.checkBirthdayPattern(this.value);" placeholder="JJ/MM/AAAA">
          </div>
        </div>
        <div class="form-group">
          <label for="eleve_genre" class="col-sm-3 control-label">Genre</label>
          <div class="col-sm-9">
           <select id="eleveGenreSelect" class="btn btn-default form-control connexion-requise" onchange="app.show('eleve_infos_save');" name="genre">
            <option value="null">-</option>
            <option value="G">G</option>
            <option value="F">F</option>
          </select>
        </div>
      </div>
      <hr/>
      <div class="form-group">
        <label for="eleve_statut" class="col-sm-3 control-label">Statut</label>
        <div class="col-sm-9">    
          <select id="eleveDelegueSelect" class="btn btn-default form-control connexion-requise" onchange="app.show('eleve_infos_save');" name="eleve_statut">
            <option value="true">Délégué.e</option>
            <option value="false">Non Délégué.e</option>
          </select>
        </div>
      </div>
      <hr/>
      <div class="form-group">
        <div class="col-sm-3 control-label">
          <div type="div" class="btn btn-sm btn-default" onclick="app.makeEleveToken();">
            <span class="glyphicon glyphicon-refresh"></span>
          </div>
        Mot de passe</div>
        <div class="col-sm-9">  
          <input type="text" class="form-control" id="eleve_info_token" onkeyup="app.show('eleve_infos_save');" placeholder="">
        </div>
      </div>
    </form>   
  </div>
</div>
</div>
<div class="flex-1 aside flex-columns text-center ">
  <div class="flex-1">
   <img id="student-edit-picture" src='assets/favicons/apple-touch-icon-72x72.png'  />
   <span class="glyphicon glyphicon-remove btn btn-xs btn-default" onclick="app.studentPictureSelectDelete();"></span>
   <input type="file" class="form-control" id="eleve-picture-input" accept="image/*" onchange="app.studentPictureSelect(this);app.show('eleve_infos_save');" />
 </div>
 <div id="eleve_infos_save">
   <span class="btn btn-default pull-left" onclick="app.renderEleveInfosEdit();">Annuler</span>
   <div class="btn btn-primary connexion-requise pull-right" onclick="app.eleveInfosUpdate();$(this).button('loading');" data-loading-text="Enregistrement..." id="eleve_infos_save_btn">
     <span class="glyphicon glyphicon-save"></span> 
     <span class="screen-small">Enregistrer</span>
   </div>      
 </div>
</div>
</div>
</div>