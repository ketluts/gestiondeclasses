<div class="btn-group-vertical">
 <div onclick="$('#eleve_agenda_bloc').goTo();" class="btn btn-default">
  <span class="glyphicon glyphicon-calendar"></span> 
  <span class="screen-small">Agenda</span>
</div>
<div class="btn btn-default" onclick="$('#eleve_notes').goTo();   ">
  <span class="glyphicon flaticon-book154"></span> 
  <span class="screen-small">Notes</span>
</div>    
<div class="btn btn-default" onclick="app.go('skills/student/'+app.currentEleve.eleve_id);">
  <span class="glyphicon glyphicon-tasks"></span> 
  <span class="screen-small">Compétences</span>
</div> 
</div>
<hr/>
 <div class="panel panel-default col-sm-12">
 <div class="panel-heading">
   <h3 class="panel-title">Pièces</h3>
 </div>
 <div class="list-group">
  <span id="eleve_etoiles_total">0</span> x <span class="etoile_on"></span>
  <br/>
  <span class="h5"><small> dont <span id="eleve_etoiles_utilisable">0</span> à utiliser.</small></span>
  <div id="eleve_etoiles_btn">
  </div>
</div>
</div>
<div class="btn-group-vertical">
 <div class="btn btn-danger admin connexion-requise" id="eleve-remove-btn" onclick="app.studentRemoveFromClassroom();">
 	<span class="glyphicon glyphicon-remove"></span> 
 	<span class="screen-small">Retirer de la classe</span>
 </div>
 <div class="btn btn-danger admin connexion-requise" onclick="app.studentDelete();">
 	<span class="glyphicon glyphicon-remove"></span> 
 	<span class="screen-small">Supprimer l'élève</span>
 </div>
</div>