<div id="home-calendar-aside">
    <div class="h3">
      <div id="home-agenda-date" class="agenda-date"></div>
      <small>Semaine <span id="home-agenda-semaine"></span></small>
    </div>
    <hr/>
    <div class="btn-group">
      <div class="btn btn-default btn-md home-agenda-view" id="home-agenda-view-agendaDay" onclick=" $('#home_agenda').fullCalendar( 'changeView', 'agendaDay' )">1 j.</div>
      <div class="btn btn-default btn-md home-agenda-view" id="home-agenda-view-basicThreeDay" onclick=" $('#home_agenda').fullCalendar( 'changeView', 'basicThreeDay' )">3 j.</div>
      <div class="btn btn-default btn-md home-agenda-view" id="home-agenda-view-basicWeek" onclick=" $('#home_agenda').fullCalendar( 'changeView', 'basicWeek' )">Semaine</div>
      <div class="btn btn-default btn-md home-agenda-view" id="home-agenda-view-month" onclick=" $('#home_agenda').fullCalendar( 'changeView', 'month' )">Mois</div>
    </div>
    <div class="btn-group">
      <div class="btn btn-default btn-md" onclick=" $('#home_agenda').fullCalendar( 'prev')"><span class="fc-icon fc-icon-left-single-arrow"></span></div>
      <div class="btn btn-default btn-md" onclick="app.agendaGoToday();">Aujourd'hui</div>
      <div class="btn btn-default btn-md" onclick=" $('#home_agenda').fullCalendar( 'next')"><span class="fc-icon fc-icon-right-single-arrow"></span></div>
    </div>
    <hr/>
    <div id="home_legende" class="clearfix"></div>
  </div>