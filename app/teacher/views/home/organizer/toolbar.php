 <div class="toolbar">
       <div class="pull-left">     
        <div class="btn-group">
         <div class="btn btn-default connexion-requise" onclick="app.agendaEditionInit();$('#home_agenda_bloc').goTo();"><span class="glyphicon glyphicon-plus"></span> <span class="screen-small">Nouvel événement</span></div>
       </div>  
       <div class="btn btn-default btn-md  agenda-events-details-btn" onclick="app.agendaEventDetailsToggle(!app.userConfig.agendaEventDetails);">
        <span class="glyphicon glyphicon-th-list"></span> <span class="screen-small">Détails</span>
      </div>
    </div> 
    <div class="clearfix"></div>
  </div>