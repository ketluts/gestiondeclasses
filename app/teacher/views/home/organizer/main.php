<div class="text-center box" id="home_agenda_bloc">
  <div class="btn btn-default btn-close" onclick="$('#app').goTo();">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </div>
  <div class="flex-rows"> 
    <div class="flex-4">
     <?php include('toolbar.php'); ?>
     <?php include('edit.php'); ?>
     <div id='home_agenda'></div>  
        </div>
   <div class="flex-1 aside">
    <?php include('aside.php'); ?>
  </div>
</div>
</div>