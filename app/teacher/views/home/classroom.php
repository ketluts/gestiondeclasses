<div  class="flex-1" id="home-classrooms"></div>

<template id="template-home-classes">
	<br/>
	{{#classes}}
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
		<ul class="list-group">
			{{#.}}
			<li class="list-group-item">
				<div class="btn btn-default home-classroom" onclick="app.go('classroom/{{classe_id}}');">
					<b>{{classe_nom}}</b>
				</div>
			</li>
			{{/.}}
		</ul>
	</div>
	{{/classes}}
	<div class="clearfix"></div>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
		<div class="btn btn-default home-classroom" onclick="app.go('classroom/-1');">
			<b>Tous les élèves</b>
		</div>      
	</div>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
		<div class="btn btn-default home-classroom" onclick="app.go('classroom/-2');">
			<b>Non classés</b>
		</div>      
	</div>
</template>