<div id="classe_add_form" class="well text-center">  
	<input type="text" class="form-control" placeholder="Nom de la classe" name="classe_nom" id="classe_nom" value=""/>
	<br/>
	<div class="btn-group">
		<span class="btn btn-default" onclick="app.hide('classe_add_form');$('#app').goTo();">Fermer</span>
		<div id="classe_save_btn" onclick="app.classeAdd();$(this).button('loading');" class="btn btn-primary"  data-loading-text="Enregistrement...">Enregistrer</div>
	</div>
</div>