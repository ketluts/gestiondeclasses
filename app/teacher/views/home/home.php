<div class="template_home template flex-columns"> 
 <div class="flex-rows box flex-1"> 
  <div class="flex-columns flex-4">
    <?php include('toolbar.php'); ?>    
    <?php include('classroomAddForm.php'); ?>
    <?php include('classroom.php'); ?>
    <?php include('edt.php'); ?>
  </div>
  <div id='home_aside' class="flex-1 text-center">
    <?php include('aside.php'); ?>
  </div>
    </div>
    <?php include('organizer/main.php'); ?>
</div>
