<div class="toolbar">
  <div class="pull-left">
   <div class="btn-group">
    <div id="home-btn-edt" class="btn btn-default  home-btn"  onclick="app.homeViews('edt');"><span class="glyphicon glyphicon-calendar"></span> <span class="screen-small">Emploi du temps</span></div>          
    <div id="home-btn-liste" class="btn btn-default  home-btn"  onclick="app.homeViews('liste');"><span class="glyphicon glyphicon-list"></span> <span class="screen-small">Classes</span></div>
  </div>
</div>
<div class="pull-right"> 
  <div class="btn-group">
   <div id="home-btn-classe" class="btn btn-danger connexion-requise admin"  onclick="app.renderClasseAdd();"><span class="glyphicon glyphicon-plus"></span> <span class="screen-small">Ajouter une classe</span>
   </div> 
 </div>
 <div class="btn-group">
  <div id='home-edt-edit-btn' onclick="app.homeEdtToggleView();" class='btn btn-default connexion-requise'>
   <span class='glyphicon glyphicon-cog'></span> 
 </div> 
</div>  
</div>
</div>