<div id="home_edt_bloc" class="flex-1 flex-rows">
 <div id="home-edt-edition" class="text-left flex-1">
  <div class="bold">Type</div>
  <input type="text" class="form-control" placeholder="Groupe, Vie de classe..." id="edt-edit-info"/>
  <br/>
  <div class="bold">Semaine</div>
  <div class="btn-group"> 
    <div class="btn home-week btn-sm" id="home-week-A" onclick="app.homeEDTCurrentWeekSet('A');">A</div>
    <div class="btn home-week btn-sm" id="home-week-AB" onclick="app.homeEDTCurrentWeekSet('AB');">A et B</div>
    <div class="btn home-week btn-sm" id="home-week-B" onclick="app.homeEDTCurrentWeekSet('B');">B</div>
  </div>
  <hr/>
  <div id="home-classe" onclick="app.homeEDTClasseBtn();"></div> 
</div>
<div id="home-calendar" class="flex-4"></div>
</div>