<!DOCTYPE html>
<html lang="fr">
<head>
  <title>GestionDeClasses</title>
  <meta name="author" content="Ketluts" />
  <meta name="description" content="GestionDeClasse vous permet d'encourager, d'évaluer, de communiquer et de suivre la progression de chaque classe et chaque élève d'un établissement à l'aide de différents outils : sociogramme, évaluation par compétences, cahier de texte, fiche de suivi, partage de documents, messagerie" />
  <meta name="keywords" content="classe, élèves, gestion, devoirs, travail, maison, affaires, fiche" />
  <meta http-equiv="Content-type" content="text/html; charset=utf-8">
  <meta name="viewport" content="initial-scale=1">

  <link rel="apple-touch-icon" sizes="57x57" href="assets/favicons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="assets/favicons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="assets/favicons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/favicons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="assets/favicons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="assets/favicons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="assets/favicons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="assets/favicons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="assets/favicons/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="assets/favicons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="assets/favicons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/favicons/favicon-16x16.png">
  <link rel="manifest" href="assets/favicons/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="assets/favicons/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <!-- ###LIBRAIRIES### --> 
  <link href="assets/lib/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
  <link href="assets/lib/educational-icons/flaticon.css" rel="stylesheet" type="text/css"/>
  <link href='assets/lib/jquery-ui-1.12.1.custom/jquery-ui.css'  rel='stylesheet' type='text/css'/>
  <link href="assets/lib/datepicker/css/datepicker.css"  rel="stylesheet" type="text/css"/>
  <link href='assets/lib/fullcalendar-3.0.1/fullcalendar.css'  rel='stylesheet' type='text/css'/> 
  <link href="assets/lib/sweetalert/sweetalert.css" rel="stylesheet" type="text/css"/>
  <link href="assets/lib/pace/pace.css" rel="stylesheet" type="text/css"/>
  <link href="assets/lib/DataTables/datatables.min.css" rel="stylesheet" type="text/css"/>
  <link href="assets/lib/quill1.3.6/quill.snow.css" rel="stylesheet" type="text/css"/>
  <link href="assets/lib/qtip/jquery.qtip.min.css" rel="stylesheet" type="text/css"/>
  <link href="assets/lib/checkbox.css" rel="stylesheet" type="text/css"/>
  <link href="assets/lib/fileicon.css" rel="stylesheet" type="text/css"/>

  <!-- ###PRINT### --> 
  <link href="assets/css/print.css" media="print"  rel="stylesheet" type="text/css"/>
  <!-- ###GESTION DE CLASSES### -->   
  <link href="assets/css/app.css?20191106" rel="stylesheet" type="text/css"/> 
  <link href="assets/css/main.css?20191106" rel="stylesheet" type="text/css"/>
  <link href="assets/css/header.css?20191106" rel="stylesheet" type="text/css"/>
  <link href="assets/css/home.css?20191106" rel="stylesheet" type="text/css"/>
  <link href="assets/css/classroom.css?20191106" rel="stylesheet" type="text/css"/>
  <link href="assets/css/student.css?20191106" rel="stylesheet" type="text/css"/>
  <link href="assets/css/skills.css?20191106" rel="stylesheet" type="text/css"/>
  <link href="assets/css/config.css?20191106" rel="stylesheet" type="text/css"/>
  <link href="assets/css/challenges.css?20191106" rel="stylesheet" type="text/css"/>
  <link href="assets/css/connexion.css?20191106" rel="stylesheet" type="text/css"/>
  <link href="assets/css/messagerie.css?20191106" rel="stylesheet" type="text/css"/>
  <link href="assets/css/organizer.css?20191106" rel="stylesheet" type="text/css"/>
  <link href="assets/css/mobile.css?20191106" rel="stylesheet" type="text/css"/>
  <link href="assets/css/shares.css?20191106" rel="stylesheet" type="text/css"/>
  <link href="assets/css/bilans.css?20191106" rel="stylesheet" type="text/css"/>
</head>
<body class="flex-columns">
  <div id="app" class="flex-columns">
    <div id="infos"></div>   
    <div id="main" class="flex-columns">
     <?php require_once('headers.php'); ?>