<table class="table challenges-classement-tab">
<thead>
<tr>
   <th class="text-center">Élèves</th>
  <th class="text-center"><span class="glyphicon flaticon-puzzle26"></span></th>
</tr>
</thead>
<tbody>
  {{#each eleves}}
{{#if (eleve_enable)}}
<tr>
  <td>{{eleve_nom_html}}</td>
  <td>{{eleve_pieces_html}}</td>
</tr>
 {{/if}}
{{/each}}
</tbody>
</table>