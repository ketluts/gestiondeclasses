  <div class="box template text-center" id="template_defis">
    <div class="flex-columns">      
     <div class="toolbar">
      <button class="btn btn-default pull-left" onclick="app.renderDefiForm();"><span class="glyphicon glyphicon-plus"></span> Créer un défi</button>   
    </div>
    <?php
    require_once('form.php');
    ?>
    <div class="flex-rows">     
     <div class="flex-1" id="challenges-classement-classes"></div>
     <div class="flex-1 aside" id="challenges-classement-eleves"></div>
   </div>
   <hr/>

   <div class="flex-rows">   
    <div class="flex-1">
     
      <div id="challenges-liste"></div>
    </div>
    <div class="flex-1 text-left aside">
      <div id="challenges-edit">         
       <div id="challenges-edit-form">
       </div>
     </div>
   </div>
 </div>
</div>
</div>

<template id="template-defis-liste">
  {{#each defis}}
  <div class="defi flex-rows">
    <div class="defi-icon flex-1">
      <img class="defi-img" src="assets/lib/educational-icons/svg/{{defi_icon}}.svg"/>
      <div>
       {{defi_etoiles_html}}
     </div>
   </div>
   <div class="defi-info flex-5">    
    <span class="defi_titre h3">{{defi_titre}}</span>
    <br/>
    <div class="defi_description h5">{{defi_description}}
      <br/>
      <i><small>Ajouté le {{defi_date_html}}</small></i>
    </div>    
  </div>
  <div class="defi-toolbar">
    <div class="btn-group">
      {{#defi_url}}
      <a href="{{../../serveur}}index.php?go=flux&e={{../../nom_etablissement}}&t={{../defi_token}}"  class="btn btn-default btn-sm" target="_blanck" title="Flux RSS du défi"><img src="assets/img/rss8.svg" width="12"/></a>
      {{/defi_url}}
      <a class="btn btn-default btn-sm" onclick="app.updDefi({{defi_id}});" title="Éditer ce défi"><span class="glyphicon glyphicon-cog"></span></a>
    </div>
    <div class="btn btn-default btn-sm" onclick="app.renderClassesDefi({{defi_id}});"><span class="glyphicon flaticon-puzzle26"></span></div>
  </div>  
</div>  
{{/each}}
</template>

<template id="template-defis-edition">
  {{#each defi}}
  <div class="flex-columns"> 
    <span class="h3">{{defi_titre}} <small>Edition</small></span>
    <hr/>
    <div class="input-group">
      <div class="input-group-addon">
        Titre
      </div>
      <input type="text" id="defi_{{defi_id}}_titre" class="form-control" value="{{defi_titre}}"/>
    </div>
    <br/>
    <div class="input-group">
      <div class="input-group-addon">
        Description
      </div>
      <input type="text" id="defi_{{defi_id}}_description" class="form-control" value="{{defi_description}}"/>
    </div>
    <br/>
    <div class="input-group">
      <div class="input-group-addon"><span class="glyphicon glyphicon-link"></span></div>
      <input type="text" id="defi_{{defi_id}}_url" class="form-control"  placeholder="http(s)://.... (facultative)" value="{{defi_url}}"/>
    </div>
    <hr/>
    <div class="flex-rows">
      <div class="flex-1 text-left">
        <div class="btn btn-danger defi_delete_btn" onclick="$(this).button('loading');app.delDefi({{defi_id}});" title="Supprimer ce défi" data-loading-text="Suppression..."><span class="glyphicon glyphicon-trash"></span></div>
      </div>
      <div class="flex-1 text-right">
        <div class="btn-group">
          <div class="btn btn-default" onclick="$('#challenges-edit-form').css('display','none');$('#challenges-edit-help').css('display','block');">Annuler</div> 
          <div class="btn btn-primary defi_edit_save" onclick="$(this).button('loading');app.defiEditSave({{defi_id}});" data-loading-text="Enregistrement...">Enregistrer</div>
        </div> 
      </div> 
    </div> 
  </div>
  {{/each}}
</template>
<script id="template-defis-classement-classes" type="text/x-handlebars">
 <?php
 require_once('template-defis-classement-classes.php');
 ?>
</script>

<script id="template-defis-classement-eleves" type="text/x-handlebars">
 <?php
 require_once('template-defis-classement-eleves.php');
 ?>
</script>