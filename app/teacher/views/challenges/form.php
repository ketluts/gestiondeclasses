<div id="defi_form" class="well well-sm">
  <div class="flex-rows">
    <div class="flex-1">
       <input id="defi_titre" class="form-control" placeholder="Titre du défi" onkeyup="app.renderApercuDefi();" /> 
       <br/>
       <textarea id="defi_description" class="form-control" placeholder="Description" onkeyup="app.renderApercuDefi();"></textarea>
       <br/>     
       <input id="defi_icon" value="education34" type="hidden"/> 
       <br/>
       <div id="defi_icons_LST"></div>
     </div>
     <div class="flex-1">
       <input type="hidden" id="defi_tags" class="form-control" placeholder="Mots clés séparés par virgules."/><label for="defi_etoiles">Nombre de pièces à gagner : </label>
       <select id="defi_etoiles" name="defi_etoiles" class="form-control" onchange="app.renderApercuDefi();">
         <option value="1" selected>1</option>
         <option value="2">2</option>
         <option value="3">3</option>
         <option value="4">4</option>
         <option value="5">5</option>
         <option value="6">6</option>
         <option value="7">7</option>
         <option value="8">8</option>
         <option value="9">9</option>
         <option value="10">10</option>
       </select>
       <br/>
       <label for="defi_url">Lien externe associé au défi</label>
       <div class="input-group">
         <div class="input-group-addon"><span class="glyphicon glyphicon-link"></span></div>
         <input id="defi_url" class="form-control" placeholder="http(s)://.... (facultative)"/> 
       </div>
       <br/>
       <h3>Aperçu</h3>
       <div class="well well-sm text-left defi row">
        <div id="apercu_defi_icon" class="col-sm-3 text-center"></div>
        <div class="col-md-9">
          <div class="defi_titre h3" id="apercu_defi_titre">Titre</div>
          <div class="defi_description h5" id="apercu_defi_description">Description</div>
        </div>
      </div>
      <br/>
      <div class="btn-group">
        <span class="btn btn-default" onclick="app.hide('defi_form');$('#app').goTo();">Annuler</span>
        <button class="btn btn-primary center-block" id="defi_save_btn" onclick="$(this).button('loading');app.addDefis();" data-loading-text="Enregistrement...">Enregistrer</button>
      </div>
    </div>    
  </div>     
  </div>