<table class="table challenges-classement-tab">
<thead>
<tr>
   <th class="text-center">Classes</th>
  <th class="text-center"><span class="glyphicon flaticon-puzzle26"></span></th>
</tr>
</thead>
<tbody>
  {{#each classes}}
  {{#if (classe_enable)}}
<tr>
  <td>{{classe_nom}}</td>
  <td>{{classe_pieces_html}}</td>
</tr>
 {{/if}}
{{/each}}
</tbody>
</table>