<div class="text-center template template_classe  box" id="classe-export">
	<div class="btn btn-default btn-close" onclick="app.viewToggle('classe-export');$('#app').goTo();">
		<span class="glyphicon glyphicon-remove"></span>
	</div>
	<div class="h2">Exporter les données<br/>
		<small>Sélectionnez en utilisant CTRL et MAJ</small>
	</div>
	<div id="student-selected-bloc">     
		Pour la sélection (<span id="student-selected-nb"></span>) <span class="btn btn-default btn-sm" onclick="app.studentsDelete();"><span class="glyphicon glyphicon-trash"></span></span>
	</div>  
	<div id="classe-export-div"></div>
</div>