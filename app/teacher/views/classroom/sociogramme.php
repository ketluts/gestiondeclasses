<div class="text-center template template_classe box"  id="sociogramme">
 <div class="btn btn-default btn-close" onclick="app.hide('sociogramme');$('#app').goTo();">
   <span class="glyphicon glyphicon-remove"></span>
 </div>
 <div class="flex-rows">  
  <div class="flex-3 flex-columns">   
    <div class="toolbar">
     <div class="pull-left">
       <div class="btn-group">  
        <div class="btn btn-default sociogramme-toolbar-btn" onclick="app.resetSociogramme();"><span class="glyphicon glyphicon-refresh"></span> Reset</div>
        <div class="btn btn-default sociogramme-toolbar-btn" id="sociogramme-toolbar-btn-save" onclick="$(this).button('loading');app.sociogrammeSave();" data-loading-text="Enregistrement..."><span class="glyphicon glyphicon-save"></span> Enregistrer</div> 
      </div>
      <div class="btn-group">  
       <a class="btn btn-default" href="#" target="_blank" rel="noreferrer" id="classroom-sociogramme-export" onclick="app.sociogrammePictureGet();"><span class="glyphicon glyphicon-picture"></span> Exporter</a> 
     </div>     
   </div> 
   <div class="pull-right">
    <button class="btn btn-default sociogrammeType" onclick="app.sociogrammeToggleType();">
      <span class="glyphicon glyphicon-stats"></span>
      Mode avancé
    </button> 
    <div class="btn-group">
      <div title="Mes avis" class="btn btn-default classroom-sociogramme-views-btn btn-sociogramme-userView" onclick="app.getClasseSociogramme({relationsView:'userView'});"><span class="glyphicon glyphicon-user"></span> <span class="screen-medium">Mes avis</span></div>
      <div title="Avis des élèves" class="btn btn-default classroom-sociogramme-views-btn btn-sociogramme-elevesView" onclick="app.getClasseSociogramme({relationsView:'elevesView'});"><span class="glyphicon glyphicon glyphicon-eye-open"></span> <span class="screen-medium">Avis des élèves</span></div>
    </div> 
  </div>
</div>   
<span class="text-left h3"><small>Cliquez et glissez pour déplacer les éléments.</small></span>
<div id="classroom-sociogramme">
 <canvas id="classroom-sociogramme-canvas" onmouseover="app.sociogrammeMouseOver();" onmouseout="app.sociogrammeMouseOut();" onmousedown="app.start_move();" onmouseup="app.end_move();"></canvas>
</div>
<div id="classroom-sociogramme-synthese">
</div>
</div>
<div class="flex-1 aside">
 <h2>Filtres</h2>
 <div class="flex-rows"> 
  <div class="input-group sociogramme-filtres flex-1">
    <div class="input-group-addon">
      <span class="glyphicon glyphicon-filter"></span>
    </div>
    <select class="btn btn-default form-control" onchange="app.setSociogrammeFiltre(this.value);" id="sociogrammeFiltres">
      <option value="null" selected>Aucun filtre</option>
      <option value="green">Verts</option>
      <option value="blue">Bleus</option>
      <option value="green/blue">Verts et Bleus</option>
      <option value="red">Rouges</option>
      <option value="black">Noirs</option>
    </select>
  </div>
</div>
<hr/>
<div class="classroom-sociogramme-option">
  Afficher les cercles
  <label class="switch">
    <input type="checkbox"  id="sociogrammeCircles" checked="checked" onchange="app.sociogrammeRenderInit();">
    <span class="slider round"></span>
  </label>
</div>
<div class="classroom-sociogramme-option">
 Inverser le sociogramme
 <label class="switch">
  <input type="checkbox"  id="sociogrammeReverse" onchange="app.setElevesPositions(app.sociogramme.eleves,app.sociogramme.rangs);app.sociogrammeRenderInit();">
  <span class="slider round"></span>
</label>
</div>
<div class="btn-group">
  <div class="btn btn-default" onclick="app.sociogrammeZoom(-0.05);">
    <span class="glyphicon glyphicon-zoom-out"></span>
  </div>
  <div class="btn btn-default" onclick="app.sociogrammeZoom(0.05);">
    <span class="glyphicon glyphicon-zoom-in"></span>
  </div>
</div>
<hr/>
<div id="sociogramme-save-bloc">
  <h3>Sauvegardes</h3>
  <div class="flex-rows" id="classroom-sociogramme-select-sauvegarde">
    <select class="btn btn-default flex-1" id="classroom-sociogramme-select-btn" onchange="app.classroomSociogrammeSelectChange(this.value);">
    </select>
    <span class="flex-1" id="classroom-sociogramme-delete">
    </span>
  </div>
</div>
</div>
</div> 
</div>