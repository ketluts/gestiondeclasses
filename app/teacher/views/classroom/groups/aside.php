  <h2>Générateur</h2>
  <div class="flex-rows">
    <select id="groupsGenerator-studentsNb" class="form-control flex-3" name="groupsGenerator-studentsNb">   
    </select>
    <div class="flex-1">
      <div  class="btn btn-default" onclick="app.getGroupes(0);"><span class="glyphicon glyphicon-repeat"></span></div>
    </div>
  </div>
  <div class="flex-rows">
    <select id="groupsGenerator-groupsNb" class="form-control flex-3" name="groupsGenerator-groupsNb">  
    </select>
    <div class="flex-1">
      <div  class="btn btn-default" onclick="app.getGroupes(1);"><span class="glyphicon glyphicon-repeat"></span></div>
    </div>      
  </div>
  <div class="form-inline text-left">
   <br/>
   <input type="checkbox" id="sociogroupes" onchange="app.classroomGroupsCheckboxsRules('sociogroupes');" /> <label for="sociogroupes">Sociogroupes</label>     <br/>
   <input type="checkbox" id="groupes_mixtes" onchange="app.classroomGroupsCheckboxsRules('mixite');"/> <label for="groupes_mixtes">Groupes mixtes</label>
   <br/>
   <input type="checkbox" id="groupes_de_niveaux" onchange="app.classroomGroupsCheckboxsRules('niveaux');" /> <label for="groupes_de_niveaux">Groupes de niveaux</label> 
   <select  id="groupes_de_niveaux_type" class="form-control" onchange="app.classroomGroupsCheckboxsRules(this.value);">
     <option value="homogenes" selected>Homogènes</option>
     <option value="heterogenes">Hétérogènes</option>
   </select>
   <select  id="groupes_de_niveaux_mode" class="form-control" onchange="app.classroomGroupsCheckboxsRules(this.value);">
     <option value="notes" selected>d'après les notes</option>
     <option value="competences">d'après les compétences</option>
   </select>
   <select id="classroomGroupesFiltersList" class="form-control">
    </select>
 </div>
 <hr/>
 <h3>Sauvegardes</h3>
 <div class="flex-rows">   
  <input type="text" class="flex-2 form-control"  id="classe-groupe-save-text" placeholder="Groupes A et B" />
  <div class="flex-1">
    <div id="classroom-groupes-doSave" class="btn btn-primary" onclick="$(this).button('loading');app.renderGroupSave();" data-loading-text="Enregistrement..."><span class="glyphicon glyphicon-save"></span> Enregistrer</div>
  </div>        
</div>  
<hr/>
<div class="flex-rows">  
  <select id="classroom-groupes-selectSave" class="flex-1 form-control" onchange="app.classroomGroupLoad(this.value);">
  </select>
  <span id="classroom-groupes-deleteSave"  class="flex-1">
  </span>
</div>