 <div class="classroom-students-groupe-toolbar">
     <div class="btn-group pull-left">
     <div class="input-group input-small">
        <div class="input-group-addon">
          <span class="glyphicon glyphicon-tint"></span>
        </div>
        <select class="btn btn-default form-control" onchange="app.renderGroupes(app.currentClasse.currentGroupes);" id="classroom-groupes-selectColor">
          <option value="null" selected>Aucunes</option>
          <option value="genres">Par genres</option>
          <option value="notes">Par niveaux (Notes)</option>
          <option value="acquis">Par niveaux (Acquis)</option>          
        </select>
      </div>
    </div> 
    <div class="pull-left btn-group">    
       <div class="btn btn-default" title="Cohésion des groupes" onclick="$('.classroom-groupes-cohesion').toggleClass('hide');">
        <span class="glyphicon flaticon-teamwork"></span> Cohésion
      </div>
      <div class="btn btn-default" onclick="app.getClasseSociogramme({mode:'groups'});">
        <span class="glyphicon flaticon-cell9"> </span> Sociogramme
      </div>       
    </div> 
     <div class="btn btn-default pull-left" onclick="app.classroomsGroupsPrint();">
        <span class="glyphicon glyphicon-print"> </span> 
      </div>    
    <div class="btn-group pull-right" id="classroomGroupsToolbar">
      <div title="Mes avis" class="btn btn-default classroom-sociogramme-views-btn btn-sociogramme-userView"  onclick="app.sociogramme.relationsView='userView';app.sociogrammeRelationsViewButtons();">
        <span class="glyphicon glyphicon-user"></span> 
        <span class="screen-medium">Mes avis</span>
      </div>
      <div title="Avis des élèves" class="btn btn-default classroom-sociogramme-views-btn btn-sociogramme-elevesView" onclick="app.sociogramme.relationsView='elevesView';app.sociogrammeRelationsViewButtons();">
        <span class="glyphicon glyphicon glyphicon-eye-open"></span> 
        <span class="screen-medium">Avis des élèves</span>
      </div>
    </div> 
    
  </div>