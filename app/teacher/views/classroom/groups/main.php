<div class="template template_classe box" id="generateurGroupes">
   <div class="btn btn-default btn-close" onclick="app.viewToggle('generateurGroupes');$('#app').goTo();">
     <span class="glyphicon glyphicon-remove"></span>
   </div>
 <div class="flex-rows">  
  <div class="flex-3 flex-columns"> 
  <?php 
    include('toolbar.php');
    ?>
  <span class="text-left h3"><small>Glissez/Déposez pour modifier les groupes.</small></span>
  <div id="classroom-groupes-liste"></div>
</div>  
<div class="flex-1 aside">
 <?php 
    include('aside.php');
    ?>
</div>
</div>
</div>
<template id="template-groups-export">
  <meta charset="UTF-8" />
  <style>
   .classroom-groupes-cohesion{
    display: none;
   }
   .h4{
    font-weight: bold;
   }
   .col-sm-4{
    margin-bottom: 20px;
    display: inline-block;
    border: 1px solid black;
    margin-right:1%;
    padding:10px;
    width: 40%;
   }
   .groupe-vide{
    display: none;
   }
</style>
</template>