<div class="template template_classe box" id="classe-config">
	<div class="btn btn-default btn-close" onclick="app.viewToggle('classe-config');$('#app').goTo();">
		<span class="glyphicon glyphicon-remove"></span>
	</div>
	<div class="h2 text-center ">Configuration de la classe</div>
  <div class="flex-rows">
   <div class="flex-4">
     Autoriser ces élèves à me contacter : <select id="messagerieEnable" class="connexion-requise" onchange="app.updMessagerieEnable(this.value);">
      <option value="true">Oui</option>
      <option value="false">Non</option>
    </select>
    <br/>
    Filtre pour les groupes de niveaux et jauges d'acquis : <select id="classroomJaugesFiltersList" class="connexion-requise" onchange="app.classroomFilterSet(this.value);">
    </select>
  </div>
  <div class="flex-1 aside">
   <div class="btn-group-vertical admin">  

    <div class="btn btn-danger connexion-requise" onclick="app.classeRename();">
      <span class="glyphicon glyphicon-pencil"> </span> 
      <span class="screen-small">Renommer cette classe</span>
    </div>
    <div id="new_classe_nom_form" class="classroom-renameForm">
      <br/>
      <input id="new_classe_nom" class="form-control" placeholder="Nouveau nom"/>
      <div class="btn-group">
        <div class="btn btn-default btn-sm" onclick="app.hide('new_classe_nom_form');">
          Annuler
        </div> 
        <div class="btn btn-primary btn-sm" id="new_classe_nom_btn" onclick="app.classeRename(true);$(this).button('loading');" data-loading-text="Enregistrement...">
          <span class="glyphicon glyphicon-save"></span> 
          <span class="screen-small">Enregistrer</span>
        </div>
      </div>  
    </div>   
    <div class="btn btn-danger connexion-requise" onclick="$('#classe-delete').css('display','block').goTo();">
      <span class="glyphicon glyphicon-trash"></span> 
      <span class="screen-small">Supprimer cette classe</span>
    </div>
  </div> 	
</div>
</div>
</div>