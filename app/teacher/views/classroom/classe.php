<div class="template template_classe box">
 <div class="flex-rows">
  <div class="flex-4 flex-columns">
    <?php 
    include('toolbar.php');
    ?>
    <div id="classroom-students"></div>  
  </div>
  <div class="flex-1 text-center aside">       
    <?php 
    include('aside.php');
    ?>
  </div>
</div>
</div>
<?php 
include('tests/main.php');
include('studentsADD.php');
include('organizer/main.php');
include('events.php');
include('groups/main.php');
include('sociogramme.php');
include('export.php');
include('intelligences.php');
include('delete.php');
include('config.php');
?>