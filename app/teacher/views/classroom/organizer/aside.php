  <div class="h3">
    <div id="classroom-agenda-date" class="agenda-date"></div>
    <small>Semaine <span id="classe-agenda-semaine"></span></small>
  </div>
  <hr/>
  <div class="btn-group">
    <div class="btn btn-default btn-md classe-agenda-view" id="classe-agenda-view-agendaDay" onclick=" $('#classe_agenda').fullCalendar( 'changeView', 'agendaDay' )">1 j.</div>
    <div class="btn btn-default btn-md classe-agenda-view" id="classe-agenda-view-basicThreeDay" onclick=" $('#classe_agenda').fullCalendar( 'changeView', 'basicThreeDay' )">3 j.</div>
    <div class="btn btn-default btn-md classe-agenda-view" id="classe-agenda-view-basicWeek" onclick=" $('#classe_agenda').fullCalendar( 'changeView', 'basicWeek' )">Semaine</div>
    <div class="btn btn-default btn-md classe-agenda-view" id="classe-agenda-view-month" onclick=" $('#classe_agenda').fullCalendar( 'changeView', 'month' )">Mois</div>
  </div>
  <div class="btn-group">
    <div class="btn btn-default btn-md" onclick=" $('#classe_agenda').fullCalendar( 'prev')"><span class="fc-icon fc-icon-left-single-arrow"></span></div>
    <div class="btn btn-default btn-md" onclick="app.agendaGoToday();">Aujourd'hui</div>
    <div class="btn btn-default btn-md" onclick=" $('#classe_agenda').fullCalendar( 'next')"><span class="fc-icon fc-icon-right-single-arrow"></span></div>
  </div>
  <hr/>
  <div id="classroom-usersLegend" class="clearfix"></div>