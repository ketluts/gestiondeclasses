<div class="text-center template template_classe box" id="classroom-organizer">
<div class="btn btn-default btn-close" onclick="$('#app').goTo();">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </div> 
  <div class="flex-rows">
   <div class="flex-4 flex-columns" >
    <?php
    include('toolbar.php');
    include('form.php');
    ?>  
    <div id='classe_agenda' class="flex-rows"></div>  
    <div id='classe_diagramme_barre'></div>       
</div>
<div class="flex-1 aside" >
  <?php
  include('aside.php');
  ?>
</div>
</div>
</div>