<div class="toolbar">         
  <div class="pull-left">
    <div class="btn-group">
      <div class="btn btn-default " onclick="app.go('home');">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </div>
    </div>
    <div class="btn-group">
      <div  class="btn btn-default" id="classe-liste-btn" onclick="app.classeElevesRender();">
        <span class="glyphicon glyphicon glyphicon-th-list"></span>
        <span class="screen-small">Liste</span>
      </div>
      <div  class="btn btn-default" id="classe-plan-btn" onclick="app.planDeClasse();">
        <span class="glyphicon glyphicon glyphicon-th"></span>
        <span class="screen-small">Plan</span>
      </div>
      <div class="btn btn-default" onclick="app.classeExportRender();$('#classe-export').goTo();">
        <span class="glyphicon glyphicon glyphicon-list-alt"></span>
        <span class="screen-small">Tab.</span>
      </div>
    </div>
    <div class="btn-group">
      <div  class="btn btn-default" onclick="app.classroomConfigInit();">
        <span class="glyphicon glyphicon glyphicon-cog"></span>
      </div>
    </div>
    <div class="btn-group classroomOnly">
     <div  class="btn btn-danger connexion-requise admin" onclick="app.classeElevesAddRender();">
       <span class="glyphicon glyphicon-plus"></span>
       <span class="screen-small">Ajouter des élèves</span>
     </div>
   </div>      
 </div>   
 <div class="pull-right">   
   <div class="btn btn-primary connexion-requise" id="classroom-plan-btnSave" onclick="app.planSave();$(this).button('loading');"  data-loading-text="Enregistrement...">
    <span class="glyphicon glyphicon-save"></span> Enregistrer
  </div>  
  <div id="classroom-students-toolbar">
    <div class="btn-group">
     <div  class="btn btn-default" onclick="app.classeSwitchPicture();" title="Afficher les photos.">
       <span class="glyphicon glyphicon-picture"></span>
     </div>
     <div  class="btn btn-default" onclick="app.classeSwitchCouleur();" title="Afficher les jauges.">
       <span class="glyphicon glyphicon-tint"></span>
     </div>
     <div class="btn btn-default" onclick="app.switchElevesSort();" title="Modifier le tri.">
      <span class="glyphicon glyphicon-sort"></span>
    </div>
  </div>  
  <div class="btn-group">
   <div class="input-group input-small">
    <div class="input-group-addon screen-small">
      <span class="glyphicon flaticon-teamwork"></span>
    </div>
    <select class="btn btn-default form-control" id="classroom-students-toolbar-selectGroup" onchange="app.classroomGroupeSelect(this.value);">
    </select>
  </div>
</div>     
</div>
</div>
</div>