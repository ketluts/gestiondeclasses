<div class="text-center template template_classe  box" id="classe-intelligences">
   <div class="btn btn-default btn-close" onclick="app.viewToggle('classe-intelligences');$('#app').goTo();">
     <span class="glyphicon glyphicon-remove"></span>
   </div>
 <div class="h2">Intelligences multiples<br/>
  <small>Profil de la classe</small>
</div>
<div class="flex-rows">  
  <div class="flex-4 flex-columns">  
    <div id="classe-intelligences-view" class="col-sm-12"></div>
  </div>
  <div class="flex-1 aside">
    <div class="h4">Test d'intelligences multiples</div>
    <select id="intelligencesTest" class="connexion-requise form-control" onchange="app.updIntelligencesTest(this.value);">
      <option value="true">Activé</option>
      <option value="false">Désactivé</option>
    </select>
  </div>
</div>
</div>