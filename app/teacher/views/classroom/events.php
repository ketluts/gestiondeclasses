<div class="text-center template template_classe box" id="classroomEventsStats">
	<div class="btn btn-default btn-close" onclick="app.hide('classroomEventsStats');$('#app').goTo();">
		<span class="glyphicon glyphicon-remove"></span>
	</div>	
	<div class="flex-rows">
		<div class="flex-4 flex-columns" >
			<div class="h2">Évènements</div>
			<div id='classroomEventsLST'></div> 
			<div id="classe_graph"></div>
		</div>
		<div class="flex-1 aside">  
		</div>
	</div>	
</div>