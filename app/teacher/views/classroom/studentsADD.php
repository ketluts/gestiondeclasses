<div class="template template_classe box text-center" id="classroom-addStudents">
  <div class="btn btn-default btn-close" onclick="app.viewToggle('classroom-addStudents');$('#app').goTo();">
    <span class="glyphicon glyphicon-remove"></span>
  </div>
  <div class="flex-colums">
    <div class="h2">Ajouter des élèves à cette classe</div> 
    <div class="flex-rows">
      <div class="flex-2 flex-columns text-center">
        <div class="toolbar">  
         <div class="btn-group pull-left">
           <div  class="btn btn-default" onclick="app.elevesAddInputRender();"><span class="glyphicon glyphicon-plus"></span> Nouvel élève</div>  
           <div onclick="$(this).button('loading');app.submitEleveForm();" id="classroom-addStudents-btn" class="btn btn-primary" data-loading-text="Enregistrement..."><span class="glyphicon glyphicon-save"></span> Enregistrer</div>
         </div>
       </div>
      <div id="classroom-addStudents-liste"></div>  
     </div> 
     <div class="flex-1 aside flex-columns text-center">  
      <div>  
        <h3>
          Importer depuis un tableur<br/>
          <small>
            Colonne 1 : NOM  / Colonne 2 : Prénom<br/> Colonne 3 : Genre (F ou G) / Colonne 4 : Date de naissance<br/>
            Vous pourrez vérifier des données avant d'enregistrer.
          </small>
        </h3>
        <textarea id="import-tableur" class="form-control" placeholder="Copier/Coller depuis le tableur."></textarea>
        <div class="btn btn-primary" id="import-tableur-btn" onclick="$(this).button('loading');app.importFromTableur();" data-loading-text="Import en cours...">
          <span class="glyphicon glyphicon-import"></span> Importer
        </div>
      </div>
      <div>
       <hr/>
       <h3>Choisir des élèves existants<br/>
        <small>Cliquez sur le nom de la classe pour afficher les élèves.</small>
      </h3>
      <div class="panel-group" id="classroom-addStudents-classrooms">
      </div>
    </div>
  </div>
</div>
</div>
</div>