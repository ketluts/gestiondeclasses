 <h3 class="screen-small">
   <span class="glyphicon flaticon-teacher35"></span>Au tableau...
 </h3>
  <div id="classroom-random" class="well h3">?</div>
  <div class="btn-group">
   <div class="btn btn-default" onclick="app.alert({title:'Réinitialiser le tirage au sort ?',type:'confirm'},app.razRandomEleve);">
     <span class="glyphicon glyphicon-refresh"></span>
     <span class="screen-small">Reset</span>
   </div> 
   <div  class="btn btn-primary" onclick="app.getRandomEleve();">Envoyer...</div>      
 </div>
<hr/>
<div class="btn-group-vertical">
 <div onclick="$('#classroom-organizer').goTo();" class="btn btn-default">
  <span class="glyphicon glyphicon-calendar"> </span>
  <span class="screen-small">Agenda</span>
  </div>
   <div onclick="app.show('classroomEventsStats');$('#classroomEventsStats').goTo();" class="btn btn-default">
  <span class="glyphicon glyphicon-bookmark"> </span>
  <span class="screen-small">Événements</span>
  </div>
<div class="btn btn-default classroomOnly" onclick="app.classeTestsRender();$('#controles').css('display','block');$('#controles').goTo();">
  <span class="glyphicon flaticon-book154"> </span>
  <span class="screen-small">Notes</span>
</div>
<div class="btn btn-default classroomOnly" onclick="app.go('skills/classroom/'+app.currentClasse.classe_id);">
  <span class="glyphicon glyphicon-tasks"> </span>
  <span class="screen-small">Compétences</span>
</div>   
</div>
<hr/>
<div class="btn-group-vertical">
  <div class="btn btn-default" onclick="$('#generateurGroupes').css('display','block').goTo();">
   <span class="glyphicon flaticon-teamwork"> </span>
   <span class="screen-small">Groupes</span>
 </div>
 <div class="btn btn-default" onclick="app.getClasseSociogramme({mode:'students'});">
  <span class="glyphicon flaticon-cell9"> </span> 
  <span class="screen-small">Sociogramme</span>
</div>
<div class="btn btn-default" onclick="app.getClasseIntelligences();">
  <span class="glyphicon flaticon-atom18"> </span>
  <span class="screen-small">Intelligences multiples</span>
</div>
</div>
<hr/>
<div class="h3">Mémo</div>
<textarea id="classe_memo" class="form-control" onblur="app.classroomSetMemo(this.value);" oninput="app.updateTextareaHeight(this);"></textarea>