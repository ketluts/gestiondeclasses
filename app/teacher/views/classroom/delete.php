<div class="text-left template template_classe  box" id="classe-delete">
    <div class="btn btn-default btn-close" onclick="app.viewToggle('classe-delete');$('#app').goTo();">
     <span class="glyphicon glyphicon-remove"></span>
   </div> 
    <h1>Suppression d'une classe</h1>

  <h3>Les éléments suivants seront supprimés (concernant cette classe)</h3>
  <ul>
    <li>Les groupes</li>
    <li>Les plans de classe</li>
    <li>Les contrôles, les notes et les pièces associées</li>
    <li>Les pièces associées aux défis</li>
    <li>Les commentaires</li>
  </ul>
  <h3>Les éléments suivants seront conservés</h3>
  <ul>
    <li>Les élèves et leurs compétences</li>
    <li>Les liens entres élèves (sociogramme)</li>
  </ul>
  <div class="col-md-12 text-center">
    <button class="btn btn-danger " onclick="app.classeDelete();"><span class="glyphicon glyphicon-remove"></span>  Supprimer vraiment cette classe !</button>
  </div>
</div>