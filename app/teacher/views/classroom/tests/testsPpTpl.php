<table class="table table-striped table-bordered export-tests" id="export-tests-{{periode_id}}">
  <thead>
    <tr>
      <th>Élèves</th>
      <th class='classroom-tests-table-moyenne'>Moyenne /20</th>
      {{#each disciplines}}
      <th class="classroom-tests-table-title">
        
          {{discipline_name}}
          <br/>            
          /20          
      </th>  
      {{/each}}
    </tr>
  </thead>
  <tbody>
    {{#each eleves}}
    <tr>      
     <td style="font-weight:bold;white-space:nowrap;">
       {{eleve_nom}} {{eleve_prenom}}
    </td>
    <td data-sort="" id="moyenne_{{this}}_{{../periode_id}}" class="classroom-tests-table-moyenne" style="font-weight:bold;">
    </td>
    {{#each ../disciplines}}
    <td id="note_{{../this}}_{{../../periode_id}}_{{discipline_id}}">
      -
    </td>
    {{/each}}      
  </tr>
  {{/each}}
</tbody>
<tfoot> 
  <tr>
    <th class="classroom-tests-table-moyenne">
      Moyenne
    </th>
    <th class="classroom-tests-table-moyenne">
     <span id="moyenneGenerale_{{periode_id}}"></span> <span class='classroom-tests-table-max'>/20</span>
   </th>
   {{#each disciplines}}
   <th id="moyenneDiscipline_{{discipline_id}}"></th>
   {{/each}}  
 </tr>
 {{^bilan}}
 <tr>
  <th></th>
  <th></th>
  {{#each disciplines}}
  <th id="boxplot_{{discipline_id}}"></th>
  {{/each}}  
</tr>
{{/bilan}}
</tfoot>
</table>