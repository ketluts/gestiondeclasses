<div class="text-center template template_classe box" id="controles">
 <div class="btn btn-default btn-close" onclick="app.viewToggle('controles');$('#app').goTo();">
   <span class="glyphicon glyphicon-remove"></span>
 </div>
 <div class="flex-column">
   <div class="h2">Notes</div>
   <div class="text-left">
    <div class="btn-group">    
      <div class="btn btn-default" onclick="app.show('classroom-tests-addForm');">
        <span class="glyphicon glyphicon-plus"></span>Ajouter une évaluation
      </div>
    </div>
  </div>

  <div id="classroom-tests-addForm" class="flex-rows well well-sm">
   <div class="flex-1">
    <div class="input-group">
      <div class="input-group-addon">Titre</div> 
      <input type="text" id="controle_titre" class="form-control" placeholder="Nouveau contrôle" value=""/>
    </div>
    <br/>
    <div class="input-group"> 
     <div class="input-group-addon">Noté sur...</div> 
     <input type="text" id="controle_note" class="form-control" value="20"/>
   </div>
   <br/>
   <div class="input-group"> 
    <div class="input-group-addon">Coefficient</div> 
    <input type="text" id="controle_coefficient" class="form-control" value="1"/> 
  </div>
</div>
<div class="flex-1">
  <div class="input-group">  
    <div class="input-group-addon">Catégorie</div> 
    <input id="controle_categorie" list="list_controles_categories" class="form-control" placeholder=""/> 
  </div>
  <br/>
  <div class="input-group"> 
    <div class="input-group-addon">Période</div>
    <select class="form-control select-periodes" id="controle_periode"></select>
  </div>
  <div class="input-group"> 
    <div class="input-group-addon">Date</div>
    <input id="controle_date" class="form-control" placeholder="JJ/MM/AAAA"/> 
  </div>
  <div class="btn-group">
    <button class="btn btn-default" onclick="app.viewToggle('classroom-tests-addForm');">Annuler</button> <button class="btn btn-primary connexion-requise" id="controle_save_btn" onclick="$(this).button('loading');app.addControle();" data-loading-text="Enregistrement...">Enregistrer</button>
  </div>
</div>
</div>
<div id="classeTestsExport"></div>
</div>
</div>
<template id="template-test-edition">
  {{#controle}}
  <div class="h2">Édition</div>
  <div class="input-group">
    <div class="input-group-addon">
      Titre
    </div>
    <input type="text" id="controle_{{controle_id}}_titre" class="form-control" value="{{controle_titre}}"/>
  </div>
  <br/>
  <div class="input-group">
    <div class="input-group-addon">
      Catégorie
    </div>
    <input type="text" id="controle_{{controle_id}}_categorie"  list="list_controles_categories" class="form-control" value="{{controle_categorie}}"/>
  </div>
  <br/>
  <div class="input-group">
    <div class="input-group-addon">
      Période
    </div>
    <select class="form-control" id="controle_{{controle_id}}_periode">
     
      {{#each ../periodes}}
      <option value="{{periode_id}}">{{periode_titre}}</option>
     {{/each}}
    </select>
  </div>
  <br/>
  <div class="input-group">
    <div class="input-group-addon">
     Date
    </div>
    <input type="text" id="controle_{{controle_id}}_date" class="form-control" value="{{../date}}" placeholder="JJ/MM/AAAA" />
  </div>
  <hr/>
  <div class="input-group">
    <div class="input-group-addon">
      Noté sur...
    </div>
    <input type="text" id="controle_{{controle_id}}_note" class="form-control" value="{{controle_note}}"/>
  </div>
  <br/>
  <div class="input-group">
    <div class="input-group-addon">
      Coefficient
    </div>
    <input type="text" id="controle_{{controle_id}}_coefficient" class="form-control" value="{{controle_coefficient}}"/>
  </div>
  <br/>  
  <br/>  
  <div class="pull-right btn-group">
    <button class="btn btn-default" onclick="$('#classeTestsEdit_{{controle_periode}}').css('display','none');$('#export-tests-{{controle_periode}}_bloc').goTo();">Fermer</button> 
    <button class="btn btn-primary connexion-requise controle_edition_save" onclick="app.editerControleSave('{{controle_id}}');$(this).button('loading');" data-loading-text="Enregistrement..."><span class="glyphicon glyphicon-save" ></span> Enregistrer</button>
  </div>
  <div class="pull-left">
    <button type="button" class="btn btn-danger" onclick="app.delControle({{controle_id}});"><span class="glyphicon glyphicon-trash"></span> Supprimer</button>
  </div>
  {{/controle}}
</template>

<script id="template-tests-render-user" type="text/x-handlebars">
  <?php
  include('testsUserTpl.php');
  ?>  
</script>
<script id="template-tests-render-pp" type="text/x-handlebars">
  <?php
  include('testsPpTpl.php');
  ?>  
</script>