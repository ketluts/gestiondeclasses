<table class="table table-striped table-bordered export-tests" id="export-tests-{{periode_id}}">
  <thead>
    <tr>
      <th>Élèves</th>
      <th class='classroom-tests-table-moyenne'>Moyenne /20</th>
      {{#each tests}}
      <th>
        <div class="classroom-tests-table-title">  
          {{controle_categorie}} - {{controle_titre}}  
         <div class="classroom-tests-table-subtitle">
            /{{controle_note}} - Coeff. {{controle_coefficient}}
         <br/> 
         {{controle_date}} 
        
         </div>
         
          
        </div>  
        {{^ ../bilan}}
        <div class="btn-group" style="width:50px;">   
          <button class="btn btn-default btn-xs" onclick="app.renderControleNotes({{controle_id}});" title="Attribuer des notes"><span class="glyphicon glyphicon-edit"></span></button>
          <button type="button" class="btn btn-default btn-xs" onclick="app.editerControle({{controle_id}});" title="Éditer ce controle"><span class="glyphicon glyphicon-cog"></span></button>
        </div>
        {{/ ../bilan}}
      </th>  
      {{/each}}
    </tr>
  </thead>
  <tbody>
    {{#each eleves}}
    <tr>
     <td style="font-weight:bold; white-space:nowrap;">
      {{eleve_nom}} {{eleve_prenom}}
    </td>
    <td data-sort="" id="moyenne_{{this}}_{{../periode_id}}_{{../discipline_id}}" class="classroom-tests-table-moyenne" style="font-weight:bold;">

    </td>
    {{#each ../tests}}
    <td id="note_{{../this}}_{{controle_id}}">
      -
    </td>
    {{/each}}      
  </tr>
  {{/each}}
</tbody>
<tfoot> 
  <tr>
    <th class="classroom-tests-table-moyenne">
      Moyenne
    </th>
    <th class="classroom-tests-table-moyenne">
     <span id="moyenneGenerale_{{periode_id}}_{{discipline_id}}"></span> <span class='classroom-tests-table-max'>/20</span>
   </th>
   {{#each tests}}
   <th id="moyenneTest_{{controle_id}}"></th>
   {{/each}}  
 </tr>
 {{^bilan}}
 <tr>
  <th></th>
  <th></th>
  {{#each tests}}
  <th id="boxplot_{{controle_id}}"></th>
  {{/each}}  
</tr>
{{/bilan}}
</tfoot>
</table>