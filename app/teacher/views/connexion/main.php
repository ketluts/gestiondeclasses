<div class="template_connexion box" id="connexion-header">
  <div class="connexion-toolbar"> 
    <a href="../student/" class="btn btn-primary connexion-requise btn-md">Élève <span class="glyphicon glyphicon-chevron-right"></span></a>
  </div>       
  <span class="h1">
   <span class="screen-medium">
    <a href="https://gestiondeclasses.net/" title="GestionDeClasses.net" target="_blank" rel="noreferrer"><img src='assets/favicons/apple-touch-icon-72x72.png'/></a>
  </span>
  GestionDeClasses
</span>
</div>
<div class="template template_connexion box">

  <div class="flex-rows flex-1">


    <div class="flex-1 flex-columns" id="connexion-menu">
      <div class="btn-group-vertical">

        <button class="btn btn-default" id="connexion-menu-nouvelEtablissement" onclick="app.renderCreateAccountForm('etablissement');"><span class='glyphicon glyphicon-plus'></span> Créer un établissement</button> 
        <button class="btn btn-default" onclick="app.renderCreateAccountForm('compte');"><span class='glyphicon glyphicon-share-alt'></span> Rejoindre un établissement</button>

        <br/>
        <button class="btn btn-default connexion-requise" onclick=" $('#connexion-password-recovery').css('display','block').goTo();"><span class='glyphicon glyphicon-lock'></span> Mot de passe perdu ?</button>
      </div>
    </div>

    <div class="flex-2 text-center well well-sm" id="connexion-user-form">    
      <span class="h3">Professeurs</span>
      <br/>
      <br/>
      <form onsubmit="return false">
        <div class="form-group" id="etablissement_form">
          <div class="input-group">
            <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
            <input class="form-control" list="list_schools" id="etablissement_nom" name="etablissement_nom" placeholder="Établissement"/>
          </div>
          <div class="input-group" id="etablissement_passe1_form">
           <div class="input-group-addon"><span class="glyphicon glyphicon-chevron-right"></span></div>
           <input class="form-control" id="etablissement_passe" name="etablissement_passe" placeholder="Mot de passe de l'établissement" type="password"/>
         </div>
         <div class="input-group" id="etablissement_passe2_form">
           <div class="input-group-addon"><span class="glyphicon glyphicon-chevron-right"></span></div>
           <input class="form-control" id="etablissement_passe2" name="etablissement_passe2" placeholder="Confirmez le mot de passe" type="password"/>
         </div>
       </div>
       <div class="form-group">
         <div class="input-group">       
           <div class="input-group-addon" id="prefix"></div>
           <input class="form-control" id="user_pseudo" name="user_pseudo" placeholder="Pseudo" />
         </div>
         <div class="input-group">
           <div class="input-group-addon"><span class="glyphicon glyphicon-chevron-right"></span></div>
           <input class="form-control" id="user_passe" name="user_passe" placeholder="Mot de passe" type="password"/>
         </div>
         <div class="input-group" id="user_passe2_form">
           <div class="input-group-addon"><span class="glyphicon glyphicon-chevron-right"></span></div>
           <input class="form-control" id="user_passe2" name="user_passe2" placeholder="Confirmez le mot de passe" type="password"/>
         </div>
       </div>
       <div id="connexion-btn-step1">
         <div class="form-group text-center">
          <button id="connexion-btn-user" class="btn btn-primary connexion-btn" onclick="$(this).button('loading');app.connexion();" data-loading-text="Connexion en cours..."><span class='glyphicon glyphicon-log-in'></span> Connexion</button>      
        </div>
      </div>
      <div id="connexion-btn-step2" class="text-center">
        <button class="btn btn-default pull-left" onclick="app.connexionRender();">Annuler</button> 
        <button class="btn btn-primary pull-right connexion-btn connexion-requise" onclick="$(this).button('loading');app.createAccount();" data-loading-text="Création du compte..."  data-loading-text="Création en cours..." ><span class='glyphicon glyphicon-plus'></span> Créer ce compte</button> 
      </div>
    </form>
  </div>
  <div class="flex-1 text-center">
<!--   <div class="well well-sm">
 <img src='assets/favicons/apple-touch-icon-72x72.png'  />
 <br/>
  <a class="btn btn-primary" href="../../old/app/">Ancienne version <span class="glyphicon glyphicon-chevron-right"></span></a>

</div>

-->

</div>
</div>

<div class="well well-sm h3 col-sm-12" id="connexion-password-recovery">
 <div class="btn btn-default btn-close" onclick="app.viewToggle('connexion-password-recovery');$('#app').goTo();">
   <span class="glyphicon glyphicon-remove"></span>
 </div>
 <div class="form col-md-6 text-center">
  <h3>Je ne connais pas mon code de récupération</h3>
  <div class="input-group">
    <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div> 
    <input type="text" class="form-control" list="list_schools" placeholder="Etablissement" id="retrieve_etablissement" value="" required/>
  </div>
  <div class="input-group">
    <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div> 
    <input type="text" class="form-control" placeholder="Pseudo" id="retrieve_pseudo" value="" required/>
  </div>
  <br/>
  <button onclick="app.passwordRetrieveGetCode();" class="btn btn-primary"><span class="glyphicon glyphicon-send"></span> Envoyer</button>
</div>
<div class="form  col-md-6 text-center aside">
  <h3>J'ai reçu mon code de récupération</h3>
  <div class="input-group">
    <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>  
    <input type="text" class="form-control" list="list_schools" placeholder="Etablissement" id="retrieve_etablissement2" value="" required/>
  </div>
  <div class="input-group">
    <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
    <input type="text" class="form-control" placeholder="Pseudo" id="retrieve_pseudo2" value="" required/>
  </div>
  <hr/>
  <div class="input-group">
   <div class="input-group-addon"><span class="glyphicon glyphicon-chevron-right"></span></div> 
   <input type="password" class="form-control" placeholder="Nouveau mot de passe" id="retrieve_passe" value="" required/>
 </div>
 <div class="input-group">
   <div class="input-group-addon"><span class="glyphicon glyphicon-chevron-right"></span></div> 
   <input type="password" class="form-control" placeholder="Confirmer le mot de passe" id="retrieve_passe2" value="" required/>
 </div>
 <div class="input-group">
   <div class="input-group-addon"><span class="glyphicon glyphicon-chevron-right"></span></div>
   <input type="password" class="form-control" placeholder="Code de récupération" id="retrieve_token" value="" required/>
 </div>
 <br/>
 <button onclick="app.passwordRetrieveNew();" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Valider</button>
</div>
</div>
<div class="clearfix"></div>
<div id="connexion-plugins-bloc"></div>
</div>