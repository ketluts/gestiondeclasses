 <div class="box flex-rows"  id="header">     
  <div id="home" class="flex-1">
       <div class="btn-group">
      <button  class="btn btn-primary" onclick="app.go('home');" title="Accueil">
        <span class="glyphicon flaticon-university7"></span>
      </button>
    </div>
    <div class="btn-group">     
     <button  class="btn btn-primary" onclick="app.go('skills');" title="Compétences">
      <span class="glyphicon glyphicon-tasks"></span>
    </button>
    <button  class="btn btn-primary connexion-requise" onclick="app.go('challenges');" title="Défis">
    <span class="glyphicon flaticon-puzzle26"></span>
    </button>
  </div>        
  <div class="btn-group">
   <button  class="btn btn-primary connexion-requise" onclick="app.go('shares');" title="Partages">
    <span class="glyphicon glyphicon-link"></span>
  </button>
  <button  class="btn btn-primary" onclick="app.go('mailbox');" title="Messagerie">
    <span class="glyphicon glyphicon-envelope"></span>  
    <span class="badge" id="messagerie-badge"></span>
  </button>
</div>
<div class="btn-group">
  <button  class="btn btn-primary" onclick="app.go('bilans');" title="Impressions">
    <span class="glyphicon glyphicon-print"></span>
  </button>
</div>
<div class="btn-group">
 <div  class="btn btn-danger btn-membre admin"  onclick="app.go('admin');" title="Administration"><span class="glyphicon glyphicon-cog"></span>
 </div>
</div>
<span id="nav">                                
</span>
</div>
<div id="titre" class="flex-1 h3"></div>  
<div class="flex-1 flex-rows" id="header-toolbar-right"> 
  <div class="flex-1 text-right" id="userInfos">
    <span class="h3" id="home-pseudo"></span>   
    <br/>
    <select class="form-control input-sm" id="home-matiere" onchange="app.userUPD({discipline:this.value});"></select>
  </div>
  <div id="userInfos-btn">
    <div class="btn button_pp btn-default btn-sm" title="Vue globale" onclick="app.togglePP();">
      <span class="glyphicon glyphicon-education"></span>
    </div>
    <div class="btn-group">
     <span onclick="app.go('user');" title="Configuration" class="btn btn-sm btn-default">
      <span class="glyphicon glyphicon-cog"></span>
    </span> 
    <span class='btn btn-sm btn-default' onclick='app.deconnexion();' title="Déconnexion"><span class='glyphicon glyphicon-off'></span>
  </span>
</div>
 <a href="https://gestiondeclasses.net" target="_blank" rel="noreferrer"><img id="header-logo" src='assets/favicons/apple-touch-icon-57x57.png' width='35px' title="GestionDeClasses.net"></a>
</div>
</div> 
</div>