/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.myTime=function(){
  var d = new Date();
  return d.getTime();
};
app.today=function(time){
  var date=moment.utc(time*1000);
  var h=date.hours();
  var m=date.minutes();
  var s=date.seconds();
  return time-s-m*60-h*3600;
}
// Returns the ISO week of the date.
app.getWeek=function(time) {
  var date = new Date(time);
  date.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  // January 4 is always in week 1.
  var week1 = new Date(date.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
    - 3 + (week1.getDay() + 6) % 7) / 7);
};
app.getWeekName=function(time) {
  time=time||app.myTime();
  var week=app.getWeek(time)*1+app.weeks*1;
  if (week % 2 === 0) {
    return "A";
  }
  return "B";
}