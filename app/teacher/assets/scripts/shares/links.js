/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
/*******************/
//LINKS
/*******************/
app.renderLinks=function(){
	var html=[];
	html.push('<table id="links-table">');
	html.push('<thead>');
	html.push('<tr>'); 
	html.push("<th><input type='checkbox' onchange='app.linksCheckAll(this.checked);' class='btn btn-control'/></th>"); 
	//html.push('<th></th>');
	html.push('<th>Site</th>');
	html.push('<th>Adresse</th>');
	html.push('<th>Date</th>');
	html.push('<th></th>');
	html.push('</tr>');
	html.push('</thead>');    
	html.push('<tbody>');
	for (var i = 0, lng=app.links.length ; i<lng; i++) {
		var link=app.links[i];
		html.push('<tr class="tab-row">');
		html.push('<td data-order="0">');
		html.push("<input type='checkbox' id='link_"+link.link_id+"' class='btn btn-control'/>");
		html.push('</td>');
		// html.push('<td data-order="0">');
		// if(link.link_favicon!=""){
		// 	html.push("<img src='"+link.link_favicon+"'  class='link_favicon'/>");
		// }	
		// html.push('</td>');		
		html.push('<td data-order="'+link.link_titre+'">');
		html.push(link.link_titre);		
		html.push('</td>');
		html.push('<td>');
		if(link.link_favicon!=""){
			html.push("<img src='"+link.link_favicon+"'  class='link_favicon'/>");
		}	
		html.push('<a href="'+link.link_url + '" target="_blank" rel="noreferrer">');
		html.push(link.link_url);
		html.push('</a> ');
		html.push('</td>');
		html.push('<td data-order="'+link.link_date+'">');
		html.push('le '+moment(parseInt(link.link_date)*1000).format('DD/MM/YY à HH[h]mm'));
		html.push('</td>');
		html.push('<td data-order="'+link.isShared+'">');
		html.push('<div class="btn-group" role="group" aria-label="...">');
		html.push('<a href="'+link.link_url + '" target="_blank" rel="noreferrer" title="Ouvrir" class="btn btn-default btn-sm pull-left">');
		html.push('<span class="glyphicon glyphicon-share-alt"></span>');
		html.push('</a> ');
		var text_shared="";
		if(link.isShared!=0){
			text_shared=link.isShared;
		}
		html.push('<span class="btn btn-default btn-sm pull-left" title="Partager" onclick="app.renderShare('+i+',\'link\');$(\'#app\').goTo();">');
		html.push('<img src="assets/img/share.svg"/>');
		html.push("<span class='badge badge_items'>"+text_shared+"</span>");
		html.push('</span>');
		var url=link.link_url;
		if(link.link_url.indexOf('#')>-1){
			url=link.link_url.substring(0,link.link_url.indexOf('#'));
		}		
		html.push('<a class="btn btn-default btn-sm pull-left" title="QR Code" href="'+app.serveur+'index.php?go=share&q=QRCode&share_text='+url+''+app.connexionParam+'">');
		html.push('<span class="glyphicon glyphicon-qrcode"></span>');
		html.push('</a>');
		html.push('</div>');
		html.push('<span class="btn btn-default btn-sm pull-right" title="Supprimer" onclick="app.delLink('+link.link_id+',false,\''+link.link_titre+'\');">');
		html.push('<span class="glyphicon glyphicon-trash"></span>');
		html.push('</span>');
		html.push('</td>');		
		html.push('</tr>');
	}
	html.push('</tbody>');
	html.push("</table>");
	document.getElementById('links-liste').innerHTML=html.join('');	
	$('#links-table').DataTable({
		stateSave: true,
		language:app.datatableFR,
		"order":[[3,'desc']]
	});
	$('#links-table').addClass('table table-striped ');
}
app.linksCheckAll=function(checked){
	for (var i = app.links.length - 1; i >= 0; i--) {
		var link=app.links[i];
		if(document.getElementById("link_"+link.link_id+"")){
			document.getElementById("link_"+link.link_id+"").checked=checked;
		}
	}
}
app.linksDeleteSelected=function(confirm){
	if (!app.checkConnection()) {return;}
	if(!confirm){
		app.alert({title:'Supprimer les liens sélectionnés ?',type:'confirm'},function(){app.linksDeleteSelected(true);});
		return;
	}
	for (var i =0,lng=app.links.length; i <lng; i++) {
		var link=app.links[i];
		if(document.getElementById("link_"+link.link_id+"")){

			if(document.getElementById("link_"+link.link_id+"").checked){
				app.delLink(link.link_id,true,link.link_titre);
			}
		}
	}
}
app.renderLinkForm=function(){
	if (!app.checkConnection()) {return;}
	$('#link_save_btn').button('reset');
	document.getElementById('link_titre').value="";
	document.getElementById('link_url').value="";
	document.getElementById('share_form').style.display = "none";
	document.getElementById('link_form').style.display = "block";
	document.getElementById('link_url').focus();
}
app.closeLinkForm=function(){
	document.getElementById('link_form').style.display = "none";
}
app.linkAdd=function(){
	if (!app.checkConnection()) {return;}
	var titre=document.getElementById('link_titre').value;
	var url=document.getElementById('link_url').value;
	$.post(app.serveur + "index.php?go=share&q=addLink"+app.connexionParam,{
		link_titre:titre,
		link_url:url,
		time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
	}, function(data) {
		app.render(data);   
		app.renderLinks();
		app.closeLinkForm();
	}
	);
}
app.delLink=function(link_id,confirm,fullname){
	if (!app.checkConnection()) {return;}
	if(!confirm){
		app.alert({title:'Supprimer le lien '+fullname+' ?',type:'confirm'},function(){app.delLink(link_id,true);});
		return;
	}
	$.post(app.serveur + "index.php?go=share&q=deleteLink"+app.connexionParam, {
		link_id:link_id,
		sessionParams:app.sessionParams
	},function(data) {
		app.render(data);  
		app.renderLinks(); 
	}
	);
}