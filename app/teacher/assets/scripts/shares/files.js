/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
/*******************/
//FILES
/*******************/
app.delFile=function(file_id,confirm,fullname){
	if (!app.checkConnection()) {return;}
	if(!confirm){
		app.alert({title:'Supprimer le fichier '+fullname+' ?',type:'confirm'},function(){app.delFile(file_id,true);});
		return;
	}
	$.post(app.serveur + "index.php?go=share&q=deleteFile"+app.connexionParam,{
		file_id:file_id,
		sessionParams:app.sessionParams
	}, function(data) {
		app.render(data);   
		app.renderFiles(); 
	}
	);
}
app.getFileById=function(file_id){
for (var i = 0, lng = app.files.length; i < lng; i++) {
		if(app.files[i].file_id==file_id){
			return app.files[i];
		}
	}
return false;
}
app.renderFiles=function(){
	var html=[];
	html.push('<table id="files-table"  width="100%">');
	html.push('<thead>');
	html.push('<tr>'); 
	html.push("<th><input type='checkbox' onchange='app.filesCheckAll(this.checked);' class='btn btn-control'/></th>"); 
	html.push('<th>Type</th>'); 
	html.push('<th>Nom du fichier</th>');
	html.push('<th>Poids</th>');
	html.push('<th>Date</th>');
	html.push('<th></th>');
	html.push('</tr>');
	html.push('</thead>');    
	html.push('<tbody>');
	for (var i = 0, lng=app.files.length ; i<lng; i++) {
		var file=app.files[i];
		if(file.file_visibility==0){continue;}
		html.push('<tr class="tab-row">');
		html.push('<td data-order="0">');
		html.push("<input type='checkbox' id='file_"+file.file_id+"' class='btn btn-control'/>");
		html.push('</td>');
		html.push('<td class="file_type">');
		html.push("<span class='' style='background-color:#" + app.getColor(file.file_type.toLowerCase()+'') + ";'>"+file.file_type.toUpperCase()+"</span>");
		html.push('</td>');
		html.push('<td>');
		html.push(file.file_fullname);
		html.push('</td>');
		html.push('<td data-order="'+file.file_size+'">');
		html.push(bytesToSize(file.file_size,2));
		html.push('</td>');
		html.push('<td data-order="'+file.file_date+'">');
		html.push('le '+moment(parseInt(file.file_date)*1000).format('DD/MM/YY à HH[h]mm'));
		html.push('</td>');
		html.push('<td data-order="'+file.isShared+'">');
		html.push('<div class="btn-group" role="group" aria-label="...">');
		html.push('<a href="'+app.serveur + "index.php?go=share&q=getFile&file_id="+file.file_id+app.connexionParam+'" title="Télécharger" class="btn btn-default btn-sm pull-left">');
		html.push('<span class="glyphicon glyphicon-cloud-download"></span>');
		html.push('</a> ');
// 		if(['doc','docx','ppt','pps','xls','odt'].indexOf(file.file_type)>=0){
// html.push('<a href="https://view.officeapps.live.com/op/view.aspx?src='+app.serveur + "index.php?go=share&q=getFile&file_id="+file.file_id+app.connexionParam+'" title="Ouvrir" class="btn btn-default btn-sm pull-left">');
// html.push('<span class="glyphicon glyphicon-share-alt"></span>');
// 		html.push('</a> ');
// }
		var text_shared="";
		if(file.isShared!=0){
			text_shared=file.isShared;
		}
		html.push('<span class="btn btn-default btn-sm pull-left" title="Partager" onclick="app.renderShare('+i+',\'file\');$(\'#app\').goTo();">');
		html.push('<img src="assets/img/share.svg"/>');
		html.push("<span class='badge badge_items'>"+text_shared+"</span>");
		html.push('</span>');

		html.push('</div>');
		html.push('<span class="btn btn-default btn-sm pull-right" title="Supprimer" onclick="app.delFile('+file.file_id+',false,\''+file.file_fullname+'\');">');
		html.push('<span class="glyphicon glyphicon-trash"></span>');
		html.push('</span>');
		html.push('</td>');		
		html.push('</tr>');
	}
	html.push('</tbody>');
	html.push("</table>");
	document.getElementById('files-liste').innerHTML=html.join('');	
	$('#files-table').DataTable({
		stateSave: true,
		language:app.datatableFR,
		"order":[[4,'desc']]
	});
	$('#files-table').addClass('table table-striped ');
	var valeur= Math.floor(app.quotaUsed*100/app.quotaByUser);
	document.getElementById('quota_used').innerHTML = bytesToSize(app.quotaUsed,2);
	document.getElementById('quota_total').innerHTML = bytesToSize(app.quotaByUser,2);
	app.quotaProgressClassInit();
	if(valeur>=90){
		$('#progress_quota').addClass('progress-bar-danger');
	}
	else if(valeur>=80){
		$('#progress_quota').addClass('progress-bar-warning');
	}
	else{
		$('#progress_quota').addClass('progress-bar-primary');	
	}
	$('#progress_quota').css('width', valeur+'%').attr('aria-valuenow', valeur); 	
}
app.filesCheckAll=function(checked){
	for (var i = app.files.length - 1; i >= 0; i--) {
		var file=app.files[i];
		if(document.getElementById("file_"+file.file_id+"")){
			document.getElementById("file_"+file.file_id+"").checked=checked;
		}
	}
}
app.filesDeleteSelected=function(confirm){
	if (!app.checkConnection()) {return;}
	if(!confirm){
		app.alert({title:'Supprimer les fichiers sélectionnés ?',type:'confirm'},function(){app.filesDeleteSelected(true);});
		return;
	}
	for (var i =0,lng=app.files.length; i <lng; i++) {
		var file=app.files[i];
		if(document.getElementById("file_"+file.file_id+"")){

			if(document.getElementById("file_"+file.file_id+"").checked){
				app.delFile(file.file_id,true,file.file_fullname);
			}
		}
	}
}
app.quotaProgressClassInit=function(){
	$('#progress_quota').removeClass('progress-bar-primary').removeClass('progress-bar-danger').removeClass('progress-bar-warning');
}
app.renderFilesForm=function(){
	if (!app.checkConnection()) {return;}
	$('#files_save_btn').button('reset');
	var input=document.querySelector('#files_input');
	input.value="";
	document.getElementById('files_temp').innerHTML="";
	document.getElementById('files_form').style.display = "block";
	document.getElementById('share_form').style.display = "none";
	input.addEventListener('change', function() {
		var html=[];
		html.push('<table id="files_table_temp"  width="100%">');
		html.push('<thead>');
		html.push('<tr>'); 
		html.push('<th>Type</th>'); 
		html.push('<th>Nom du fichier</th>');
		html.push('<th></th>');
		html.push('</tr>');
		html.push('</thead>');    
		html.push('<tbody>');
		var n=input.files.length
		for (var i=0,lng= n ; i <lng; i++) {
			if(i>9){n--; continue;}
			var file=input.files[i];
			imgType = file.name.split('.');
			imgType = imgType[imgType.length - 1];		
			if(file.size>app.upload_max_filesize){  app.alert({title:'Le fichier '+file.name+' est trop volumineux.'}); n--; continue;}
			if(file.size*1+app.quotaUsed*1>app.quotaByUser){ app.alert({title:'Vous n\'avez plus assez d\'espace de stockage.'}); n--; continue;}
			html.push('<tr class="tab-row" class="tab-row">');
			html.push('<td class="file_type">');
			html.push("<span class='' style='background-color:#" + app.getColor(imgType.toLowerCase()+'') + ";'>"+imgType.toUpperCase()+"</span>");
			html.push('</td>');
			html.push('<td>');
			html.push(''+file.name+' ('+bytesToSize(file.size,2)+')');
			html.push('</td>');
			html.push('<td>');
			html.push('<div class="progress" style="visibility:hidden;">\
				<div id="progress_'+i+'" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;">0%</div>\
				</div>');
			html.push('</td>');
			html.push('</tr>');
		}
		html.push('</tbody>');
		html.push("</table>");
		document.getElementById('files_temp').innerHTML="<hr/>"+html.join('');
		$('#files-table-temp').DataTable({
			stateSave: true,
			"dom": 't',
			language:app.datatableFR
		});
		$('#files_table_temp').addClass('table table-striped ');

		if(n==0){
			document.getElementById('files_temp').innerHTML="";
		}
	});
};
app.closeFilesForm=function(){
	app.filesUploadAbort();
	document.getElementById('files_form').style.display = "none";
}
app.addFiles=function(){
	app.filesToUpload=[];
	var input=document.querySelector('#files_input');
	var n=input.files.length;
	if(n==0){	
		$('#files_save_btn').button('reset');
		return;
	}
	app.nbFilesToUpload=n;
	app.nbFilesUploaded=0;	
	$('.progress').css('visibility', 'visible');
	for (var i = 0; i <n; i++) {
		if(i>9){ app.nbFilesToUpload--; continue;}
		var file=input.files[i];
		if(file.size>app.upload_max_filesize){ app.nbFilesToUpload--; app.checkUploadedFiles(); continue;}
		if(file.size*1+app.quotaUsed*1>app.quotaByUser){ app.nbFilesToUpload--; app.checkUploadedFiles(); continue;}
		var xhr= new XMLHttpRequest();
		xhr.open('POST', app.serveur + "index.php?go=share&q=addFile"+app.connexionParam);
		xhr.upload.progress=i;
		xhr.upload.addEventListener('progress', function(e) {
			var valeur= Math.floor(e.loaded*100/e.total);
			document.getElementById('progress_'+this.progress).innerHTML = valeur+"%";
			$('#progress_'+this.progress).css('width', valeur+'%').attr('aria-valuenow', valeur); 			
		}, false);
		xhr.addEventListener('load', function() {	
			app.nbFilesUploaded++;
			app.checkUploadedFiles();
		}, false);

		xhr.onreadystatechange=function() {
			if (xhr.readyState == 4 && (xhr.status == 200)) {
				app.render(xhr.responseText);
			}
		};
		var form = new FormData();
		form.append('file', file);
		form.append('time',app.myTime());
		form.append('sessionParams',app.sessionParams);		
		xhr.send(form);
		app.filesToUpload.push(xhr);
	}
}
app.filesUploadAbort=function(){
	for (var i = app.filesToUpload.length - 1; i >= 0; i--) {
		app.filesToUpload[i].abort();
	};
}
app.checkUploadedFiles=function(){
	if(app.nbFilesUploaded==app.nbFilesToUpload){
		app.getSharesItems();
		app.closeFilesForm();
	}
}

