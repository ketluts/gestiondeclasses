/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.renderPageShares=function(){
	
	if (!app.checkConnection()) {return;}
	app.viewClear();
	app.currentEleve=null;
	app.currentClasse=null;
	$(".template_shares").css("display","block");
	document.getElementById('share_form').style.display = "none";
	document.getElementById('link_form').style.display = "none";
	document.getElementById('titre').innerHTML = 'Mes partages';
	document.getElementById('upload_max_filesize').innerHTML=bytesToSize(app.upload_max_filesize,2);
	document.getElementById('files-liste').innerHTML=app.renderLoader();
	document.getElementById('links-liste').innerHTML=app.renderLoader();
	app.getSharesItems();
}
app.getSharesItems=function(){
	$.post(app.serveur + "index.php?go=share&q=getAll"+app.connexionParam,{
		sessionParams:app.sessionParams
	}, function(data) {
		app.render(data);   
		app.renderFiles(); 
		app.renderLinks(); 
	}
	);
}
/*******************/
//SHARES
/*******************/
app.renderShareClasses=function(){
	document.getElementById('share_classes').innerHTML="";
	var html=[];
	
	for (var i =0,lng= app.classes.length; i <lng; i++) {
		var classe=app.classes[i];
		if(app.userConfig.classesDisable.indexOf(classe.classe_id)!=-1){continue;}
		html.push('<div class="panel panel-default" id="share_'+classe.classe_id+'_block">\
			<div class="panel-heading filesShareEleve">\
			<h4 class="panel-title">\
			<a data-toggle="collapse" data-parent="#files_share_classes" href="#share_'+classe.classe_id+'">\
			'+app.cleanClasseName(classe.classe_nom)+'\
			</a>');
		if(classe.classe_id>=0){
			html.push('<span class="btn btn-default btn-sm pull-right share_button" onclick="app.shareAdd('+classe.classe_id+',\'classe\');">\
				<span class="glyphicon glyphicon-chevron-right"></span>\
				</span>');
		}
		html.push('</h4>\
			</div>\
			<div id="share_'+classe.classe_id+'" class="panel-collapse collapse ">\
			<ul class="list-group" id="share_'+classe.classe_id+'_body">');
		for (var j =0,llng=app.classes[i].eleves.length ; j <llng; j++) {
			var eleve=app.getEleveById(app.classes[i].eleves[j]);
			if(!eleve.eleve_prenom){
				eleve.eleve_prenom="";
			}
			html.push('<li class="list-group-item filesShareEleve">\
				'+app.renderEleveNom(eleve)+'\
				<span class="btn btn-default btn-sm pull-right share_button" onclick="app.shareAdd('+eleve.eleve_id+',\'eleve\');">\
				<span class="glyphicon glyphicon-chevron-right"></span>\
				</span>\
				</li>');
		}
		html.push('</ul>\
			</div>\
			</div>');  
	}; 
	document.getElementById('share_classes').innerHTML+=html.join('');
	for (var i = app.classes.length - 1; i >= 0; i--) {
		var classe=app.classes[i];
		if(document.getElementById('share_'+classe.classe_id+'_body') && document.getElementById('share_'+classe.classe_id+'_body').innerHTML==""){
			document.getElementById('share_'+classe.classe_id+'_block').style.display="none";
		}
	}
};
app.shareAdd=function(share_with,share_type){
	if (!app.checkConnection()) {return;}
	app.currentShareItem.isShared++;
	document.getElementById('share_liste').innerHTML=app.renderLoader();
	$(".share_button").addClass('disabled');
	$.post(app.serveur + "index.php?go=share&q=addShare"+app.connexionParam,{
		share_item_type:app.currentShareItem.type,
		share_item_id:app.currentShareItem.id,
		share_with:share_with,
		share_type:share_type,
		time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
	}, function(data) {
		app.render(data);   
		app.renderFiles();
		app.renderLinks();
		$(".share_button").removeClass('disabled');
	}
	);
}
app.shareDelete=function(share_id){
	if (!app.checkConnection()) {return;}
	app.currentShareItem.isShared--;
	document.getElementById('share_liste').innerHTML=app.renderLoader();
	$.post(app.serveur + "index.php?go=share&q=deleteShare&share_id="+share_id+app.connexionParam,{
		share_item_type:app.currentShareItem.type,
		share_item_id:app.currentShareItem.id,
sessionParams:app.sessionParams
	}, function(data) {
		app.render(data);   
		app.renderFiles();
		app.renderLinks();
	}
	);
};
app.closeShare=function(){
	document.getElementById('share_form').style.display = "none";
}
app.renderShare=function(item_num,item_type){
	if (!app.checkConnection()) {return;}
	document.getElementById('link_form').style.display = "none";
	document.getElementById('share_form').style.display = "block";
	document.getElementById('share_classes').innerHTML="";
	if(item_type=="file"){
		var file=app.files[item_num];
		app.currentShareItem=file;
		app.currentShareItem.id=file.file_id;
		app.currentShareItem.type="file";
		var share_name=file.file_fullname;
		var share_icon="<span class='' style='background-color:#" + app.getColor(file.file_type.toLowerCase()+'') + ";'>"+file.file_type.toUpperCase()+"</span>";
		var share_item_id=file.file_id;
	}
	else{
		var link=app.links[item_num];
		app.currentShareItem=link;
		app.currentShareItem.id=link.link_id;
		app.currentShareItem.type="link";
		var share_name=link.link_titre;
		var share_icon="";
		if(link.link_favicon!=""){
			share_icon="<img src='"+link.link_favicon+"' class='link_favicon' />";
		}		
		var share_item_id=link.link_id;
	}
	document.getElementById('share_name').innerHTML=share_name;
	document.getElementById('share_icon').innerHTML=share_icon;
	app.renderShareClasses();
	document.getElementById('share_liste').innerHTML=app.renderLoader();
	$.post(app.serveur + "index.php?go=share&q=getShares"+app.connexionParam,{
		share_item_type:item_type,
		share_item_id:share_item_id,
		sessionParams:app.sessionParams
	},function(data) {
		app.render(data);   
	}
	);
}
app.renderShareListe=function(shares){
	var html=[];
	html.push('<table id="file_share_table" width="100%" class="table table-striped">');
	html.push('<thead>');
	html.push('<tr>'); 
	html.push('<th>Partagé avec</th>');
	html.push('<th>Date</th>');
	html.push('<th></th>');
	html.push('</tr>');
	html.push('</thead>');    
	html.push('<tbody>');
	for (var i = shares.length - 1; i >= 0; i--) {
		var share=shares[i];
		var share_text="";
		//share.eleve_prenom=share.eleve_prenom||"";
		//share.eleve_nom=share.eleve_nom||"";
		if(share.share_type=='eleve'){
			var eleve=app.getEleveById(share.share_with);
			share_text=app.renderEleveNom(eleve);
		}
		else{
			var classe=app.getClasseById(share.share_with);
			share_text=app.cleanClasseName(classe.classe_nom);
		}
		html.push('<tr class="tab-row">');
		html.push('<td>');
		html.push(share_text);					
		html.push('</td>');
		html.push('<td data-order="'+share['share_date']+'">');
		html.push(moment(parseInt(share['share_date'])*1000).format('DD/MM/YY à HH[h]mm'));					
		html.push('</td>');
		html.push('<td class="text-right">');
		html.push("<button class='btn btn-danger btn-xs' onclick='app.shareDelete(\""+share.share_id+"\");'><span class='glyphicon glyphicon-trash'></span></button>");					
		html.push('</td>');
		html.push('</tr>');
	};
	html.push('</tbody>');
	html.push("</table>");
	document.getElementById('share_liste').innerHTML=html.join('');
	$('#file_share_table').DataTable({
		stateSave: true,
		"dom": 't',
		language:app.datatableFR
	});
}