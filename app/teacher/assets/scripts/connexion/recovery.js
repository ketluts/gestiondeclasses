/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.passwordRetrieveGetCode=function(){
	if (!app.checkConnection()) {return;}
	var etablissement=document.getElementById('retrieve_etablissement').value;
	var pseudo=document.getElementById('retrieve_pseudo').value;
	$.post(app.serveur + "index.php?go=users&q=passwordRecovery&action=getCode",
	{
		retrieve_etablissement:etablissement,
		retrieve_pseudo:pseudo
	}, function(data){
		app.render(data);       
	});
};
app.passwordRetrieveNew=function(){
	if (!app.checkConnection()) {return;}
	var etablissement=document.getElementById('retrieve_etablissement2').value;
	var pseudo=document.getElementById('retrieve_pseudo2').value;
	var passe=document.getElementById('retrieve_passe').value;
	var passe2=document.getElementById('retrieve_passe2').value;
	var token=document.getElementById('retrieve_token').value;
	$.post(app.serveur + "index.php?go=users&q=passwordRecovery&action=passwordUPD",{
		retrieve_etablissement:etablissement,
		retrieve_pseudo:pseudo,
		retrieve_passe:passe,
		retrieve_passe2:passe2,
		retrieve_token:token
	},app.render);
}