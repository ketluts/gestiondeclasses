/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.connexionRender=function(){
	app.viewClear();
	document.getElementById('connexion-header').style.display = "";  
	app.discussionTimer=null;
	$("#connexion-password-recovery").css('display','none');
	$(".btn-membre").css('display','inline-block');
	document.getElementById('connexion-btn-step1').style.display="block";
	document.getElementById('connexion-btn-step2').style.display="none";
	document.getElementById('etablissement_form').style.display = "block";
	document.getElementById('header').style.display = "none";
	$(".template_connexion").css('display','block');$('.connexion-btn').button('reset');
	document.getElementById('prefix').innerHTML='<span class="glyphicon glyphicon-user"></span>';
	document.getElementById('titre').innerHTML = "";
	$(document.body).trigger("sticky_kit:recalc");
	if(app.currentMode!="connexion"){
		document.getElementById("etablissement_nom").value="";
		document.getElementById("etablissement_passe").value="";
		document.getElementById("etablissement_passe2").value="";
		document.getElementById("user_pseudo").value="";
		document.getElementById("user_passe").value="";
		document.getElementById("user_passe2").value="";
	}
	app.currentMode="connexion";
	document.getElementById('etablissement_passe2_form').style.display="none";
	document.getElementById('etablissement_passe1_form').style.display="none";
	document.getElementById('user_passe2_form').style.display="none";
	document.getElementById('connexion-user-form').style.display="block";
	$('#connexion-eleve-form').removeClass('col-md-offset-3');

	if(QueryString.school){
		document.getElementById("etablissement_nom").value=QueryString.school;
	}
	if(app.etablissementsCRT==false){
		$('#connexion-menu-nouvelEtablissement').prop("disabled", true);
	}
	app.pluginsLauncher('connexionAfterRender');
}
app.renderCreateAccountForm=function(type){
	if(app.currentMode!="connexion"){
		app.connexionRender();
	}
	var pseudo=document.getElementById('user_pseudo').value;
	var passe=document.getElementById('user_passe').value;        
	document.getElementById('etablissement_passe1_form').style.display="";
	if(type=="etablissement"){
		document.getElementById('etablissement_passe2_form').style.display="";
	}else{
		document.getElementById('etablissement_passe2_form').style.display="none";
	}
	document.getElementById('user_passe2_form').style.display="";
	$('#connexion-btn-step1').css('display','none');
	$('#connexion-btn-step2').css('display','block');
	$('.connexion-btn').button('reset');
	document.getElementById('etablissement_nom').focus();
}