/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.connexion=function(){
  app.nom_etablissement=document.getElementById("etablissement_nom").value;
  app.pseudo_utilisateur=document.getElementById("user_pseudo").value;

  if(app.nom_etablissement==""){
   app.alert({title:'Il faut indiquer un nom d\'établissement.'});
   app.connexionRender();
   return false;
 }
$.post(app.serveur + "index.php?go=users&q=sessionInit",
  {   
    nom_etablissement:document.getElementById("etablissement_nom").value,
   pseudo_utilisateur:document.getElementById("user_pseudo").value,
   pass_utilisateur:document.getElementById("user_passe").value
  }, function(data) {
    app.render(data);  
    $('.connexion-btn').button('reset'); 
  }
  );
};
app.createAccount=function(){
  var etablissement=document.getElementById("etablissement_nom").value;
  var etablissement_password_1=document.getElementById("etablissement_passe").value;
  var etablissement_password_2=document.getElementById("etablissement_passe2").value;
  var pseudo=document.getElementById("user_pseudo").value;
  var password_1=document.getElementById("user_passe").value;
  var password_2=document.getElementById("user_passe2").value;
  var error=false;
  if(password_1=="" || etablissement_password_1==""){
   app.alert({title:'Les mots de passe ne doivent pas être vides.'});
   error=true;
 }
 if(password_1!=password_2){
   app.alert({title:'Les mots de passe utilisateur sont différents.'});
   error=true;
 }
 if(etablissement_password_1!=etablissement_password_2 && document.getElementById('etablissement_passe2_form').style.display!="none"){
   app.alert({title:'Les mots de passe établissement sont différents.'});
   error=true;
 }
 if(etablissement==""){
   app.alert({title:'Il faut choisir un nom d\'établissement.'});
   error=true;
 }  
 if(pseudo==""){
   app.alert({title:'Il faut choisir un pseudo.'});
   error=true;
 }  
 if(error){
  $('.connexion-btn').button('reset');
  return;
}

$.post(app.serveur + "index.php?go=users&q=create",{
  pseudo_utilisateur:pseudo,
  pass_utilisateur:password_1,
  nom_etablissement:etablissement,
  pass_etablissement:etablissement_password_1,
  create_account:true
}, function(data) {
  data=app.render(data);
  $('.connexion-btn').button('reset');
  if(data['statut']==true){   
   app.pluginsLauncher('createAccount');
   app.connexion();
 }
}
);
}

app.connexionFormEtablissementAutocompletion=function(etablissements_liste){
  var liste=etablissements_liste;
  var options = '';
  for(var i = 0,lng=liste.length; i < lng; i++)
    options += '<option value="'+liste[i]+'" />';
  document.getElementById('list_schools').innerHTML = options;
}

app.deconnexion=function(){  
  app.init();
  app.connexionParam = "";
  app.connexionRender();
}
app.checkConnection=function() {
 if (!app.isConnected) {
   app.alert({title:'Vous devez être connecté pour effectuer cette action.'});
   return false;
 }
 return true;
}