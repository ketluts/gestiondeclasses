/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.toggleClasseDisplay=function(classe_id){
  if(app.userConfig.classesDisable.indexOf(classe_id)==-1){
   app.userConfig.classesDisable.push(classe_id);
 }else{
  var newclassesDisable=[];
  for (var i = app.userConfig.classesDisable.length - 1; i >= 0; i--) {
    var id=app.userConfig.classesDisable[i];
    if(id!=classe_id){
      newclassesDisable.push(id);
    }
  };
  app.userConfig.classesDisable=newclassesDisable;
}
app.homeBuildClassesListes();
app.setColorClasses();
app.pushUserConfig();
app.renderConfigClasses();
};
app.configClassesSelectAll=function(){
  app.userConfig.classesDisable=[];
  app.homeBuildClassesListes();
  app.setColorClasses();  
  app.pushUserConfig(); 
  app.renderConfigClasses();
}
app.configClassesUnselectAll=function(){
  app.userConfig.classesDisable=[];
  for (var i = app.classes.length - 3; i >= 0; i--) {
    app.userConfig.classesDisable.push(app.classes[i].classe_id);
    app.homeBuildClassesListes();
  } app.setColorClasses();  
  app.pushUserConfig(); 
  app.renderConfigClasses();
}
app.renderConfigClasses=function(){
  var html_list=['<div class="col-md-6 config_classes_column">'];
  var currentFirstLetter = "";
  var k=0;
  for (var i =0,lng= app.classes.length ; i <lng; i++) {
    var classe=app.classes[i];
    if(classe.classe_id<0){continue;}
    var firstLetter = classe.classe_nom.charAt(0);
    if (firstLetter != currentFirstLetter) {
      if (i != 0) {
        html_list.push('</div>');  
        k++;  
        if (k % 2 == 0 && i!=lng-1) {
         html_list.push('<hr class="col-md-12"></hr>');
       }
       
       html_list.push('<div class="col-md-6 config_classes_column">');    
     }
     currentFirstLetter = firstLetter;
   }
   if(app.userConfig.classesDisable.indexOf(classe.classe_id)!=-1){
    html_list.push('<div class="btn btn-default app.userConfig.itemsAvisMode config-classroom" onclick="app.toggleClasseDisplay(\''+classe.classe_id+'\')" title="Activer cette classe">\
      '+app.cleanClasseName(classe.classe_nom)+'\
      </div><br/>'); 
  }
  else{

    html_list.push('<div class="btn btn-primary app.userConfig.itemsAvisMode config-classroom" onclick="app.toggleClasseDisplay(\''+classe.classe_id+'\')" title="Désactiver cette classe">\
      '+app.cleanClasseName(classe.classe_nom)+'\
      </div><br/>'); 
  }
  
};
html_list.push('</div>');
document.getElementById('config_classes_list').innerHTML = html_list.join('');
};