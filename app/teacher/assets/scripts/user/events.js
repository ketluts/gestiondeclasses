/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.delEventWhiteList=function(num){
  app.userConfig.eventsWhiteList.splice(num, 1);
  app.renderConfigEvents();  
  app.pushUserConfig();
}
app.addEventWhiteList=function(event){
 app.userConfig.eventsWhiteList.push(event);
 app.renderConfigEvents(); 
 app.pushUserConfig();
}
app.renderConfigEvents=function(){
  var html=[];
  for(var i=0, lng=app.eventsAvailable.length;i<lng;i++){ 
    var index=app.userConfig.eventsWhiteList.indexOf(app.eventsAvailable[i].type);
    var legende=app.legends[app.eventsAvailable[i].type]||"";
    if(index>=0){
      html.push('<div class="btn btn-primary app.userConfig.itemsAvisMode btn-config-event" data-toggle="tooltip" data-placement="top" title="'+legende+'" onclick="app.delEventWhiteList(\''+index+'\')">\
        <span class="glyphicon ' + app.eventsAvailable[i].type + '"></span>\
        </div>');
    }else{
     html.push('<div class="btn btn-default app.userConfig.itemsAvisMode btn-config-event"  data-toggle="tooltip" data-placement="top" title="'+legende+'" onclick="app.addEventWhiteList(\''+app.eventsAvailable[i].type+'\')" >\
      <span class="glyphicon ' + app.eventsAvailable[i].type + '"></span>\
      </div>'); 
   }
 }
 document.getElementById('events_list').innerHTML =html.join('');
 $('.btn-config-event').tooltip();
};