/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.configSetOrganizerSlotDuration=function(value){
  app.userConfig.organizerSlotDuration=value;
  app.pushUserConfig();
}
app.configSetWeekends=function(value){
 if(value=="false"){
  app.userConfig.agendaWeekEnds=false;
}
else{
 app.userConfig.agendaWeekEnds=true;
}
app.pushUserConfig();
}
app.configSetOrganizerTime=function(){
  var min=document.getElementById('config_organizer_min_time').value;
  var max=document.getElementById('config_organizer_max_time').value;
  app.userConfig.organizerMinTime=min;
  app.userConfig.organizerMaxTime=max;
  app.pushUserConfig();
}