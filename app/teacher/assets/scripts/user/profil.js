/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.userUPD=function(options){
 if (!app.checkConnection()) {return;}
options=options||{};
 var discipline_id=options.discipline||document.getElementById('matiere').value;
 var mail=document.getElementById('user_mail').value;
 var nom=document.getElementById('user_nom').value;
 var prenom=document.getElementById('user_prenom').value;
 var old_passe=document.getElementById('user_old_passe').value;
 var new_passe=document.getElementById('user_new_passe').value;
 var new_passe_bis=document.getElementById('user_new_passe_bis').value;
 app.userConfig.matiere=matiere;
 $.post(app.serveur + "index.php?go=users&q=update"+ app.connexionParam,{
 mail:mail,
  discipline_id:discipline_id,
  old_passe:old_passe,
  new_passe:new_passe,
  new_passe_bis:new_passe_bis,
  nom:nom,
  prenom:prenom,
  user_id:app.userConfig.userID,
sessionParams:app.sessionParams
}, function(data) {
 app.render(data);         
 $("#btn_save_config").button('reset');
 app.hide('btn_save_config');
 app.userInfosHeaderRender();
if(options.discipline){
   app.navigate(app.currentHash,true);
}


}
);
};
app.goRoleEdition=function(){
  app.go('user/role');  
}
app.configUserDisciplinesSelect=function(){
  var template = $('#template-config-disciplines-select').html();
  var rendered = Mustache.render(template, {disciplines:app.disciplines});
  document.getElementById('matiere').innerHTML=rendered; 
  document.getElementById('home-matiere').innerHTML=rendered; 
document.getElementById('matiere').value=app.userConfig.matiere;
 document.getElementById('home-matiere').value=app.userConfig.matiere;

}

app.userInfosHeaderRender=function(){
  app.configUserDisciplinesSelect();
  document.getElementById('home-pseudo').innerHTML=ucfirst(app.pseudo_utilisateur);
  document.getElementById('userInfos').style.borderColor="#"+app.getColor(ucfirst(app.pseudo_utilisateur));

}