/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.renderUserConfig=function(){  
  //app.configUserDisciplinesSelect();
  app.viewClear();
  app.currentClasse=null;
  app.currentEleve=null;
  app.currentView="config";
  $("#btn_save_matiere").button('reset');
  app.renderConfigEvents();
  app.renderConfigAlertes();
  app.renderConfigClasses();
  app.configAdminsRender();
  app.userConfigRenderForm();
  $(".config_user").css("display","block");
  if(!app.userConfig.admin){
    document.getElementById('config_no_admin_bloc').style.display = "block";
  } 
  app.hide('btn_save_config');
  document.getElementById('config_weekends').value=app.userConfig.agendaWeekEnds;
  document.getElementById('config_organizer_min_time').value=app.userConfig.organizerMinTime;
  document.getElementById('config_organizer_max_time').value=app.userConfig.organizerMaxTime;
  document.getElementById('config_organizer_slotDuration').value=app.userConfig.organizerSlotDuration;
  if(app.disciplines.length==1){
    $('#ifNoDisciplinesInfo').html('<span class="glyphicon glyphicon-info-sign"></span> Aucune discipline n\'a été définie par l\'administrateur.');
  }else{
   $('#ifNoDisciplinesInfo').html('');
 }
};
app.userConfigRenderForm=function(){
 if(!app.userConfig.matiere){
  app.userConfig.matiere=-1;
}
document.getElementById('titre').innerHTML = "Configuration";
document.getElementById('matiere').value = app.userConfig.matiere;
document.getElementById('user_mail').value = app.userConfig.mail||"";
document.getElementById('user_prenom').value = app.userConfig.prenom||"";
document.getElementById('user_nom').value = app.userConfig.nom||"";
document.getElementById('user_old_passe').value ="";
document.getElementById('user_new_passe').value ="";
document.getElementById('user_new_passe_bis').value = "";
document.getElementById('config_pseudo').innerHTML = ucfirst(app.pseudo_utilisateur);
document.getElementById('config_avatar').innerHTML='<img src="'+app.serveur+'/lib/monsterid/monsterid.php?seed='+app.pseudo_utilisateur+'&size=100"/>';
document.getElementById('config_pseudo').style.borderColor="#"+app.getColor(ucfirst(app.pseudo_utilisateur));
}
app.configAdminsRender=function(){
  var html=['<ul>'];
  for (var i = app.serveurOptions.admins.length - 1; i >= 0; i--) {
    var user=app.getUserById(app.serveurOptions.admins[i]);
    html.push('<li><strong>'+user.user_pseudo+'</strong></li>');
  }
  html.push('</ul>');
  document.getElementById('config_admins').innerHTML=html.join('');
}
app.configUserInit=function(data){
  var config=jsonParse(data);
  app.userConfig.eventsWhiteList=config.eventsWhiteList||[];
  app.userConfig.sorting=config.sorting||0;
  app.userConfig.version=config.version||20191106;
  app.userConfig.coloration=config.coloration||1;
  app.userConfig.classroomStudentsPicture=config.classroomStudentsPicture||0;
  app.userConfig.edt=config.edt||[];    
  app.userConfig.homeCurrentView=config.homeCurrentView||"liste";
  app.userConfig.ppView=config.ppView||false;
  app.userConfig.agendaCurrentView=config.agendaCurrentView||"basicThreeDay";
  app.userConfig.agendaWeekEnds=config.agendaWeekEnds||false;
  app.userConfig.organizerMinTime=config.organizerMinTime||'00:00:00';
  app.userConfig.organizerMaxTime=config.organizerMaxTime||'24:00:00';
  app.userConfig.organizerSlotDuration=config.organizerSlotDuration||'00:30:00';
  app.userConfig.eleveAgendaCurrentView=config.eleveAgendaCurrentView||"basicThreeDay";
  app.userConfig.classesDisable=config.classesDisable||[];
  app.userConfig.randomEleves=config.randomEleves||[];
  app.userConfig.alerts=config.alerts||[];   
  app.userConfig.memo=config.memo||"";   
  app.userConfig.messagesSort=config.messagesSort||1;  
  app.userConfig.itemsAvisMode=config.itemsAvisMode||false;
  app.userConfig.itemsTagsFamilies=config.itemsTagsFamilies||[];
  app.userConfig.currentEleveView=config.currentEleveView||"calendar";
  app.userConfig.sociogramme_type=config.sociogramme_type||"simple";
  app.userConfig.agendaEventDetails=config.agendaEventDetails;
  app.userConfig.itemsSyntheseMode=config.itemsSyntheseMode;
  app.userConfig.itemsOrder=config.itemsOrder;
  app.userConfig.itemsCurrentFilter=config.itemsCurrentFilter||clone(app.itemsDefaultFilter);
  app.userConfig.jaugesFilters=config.jaugesFilters||[];
  app.togglePP(config.ppView,false);
  app.buildEdtTime();
  app.agendaEventDetailsToggle(app.userConfig.agendaEventDetails,false);
  app.itemsSyntheseSetMode(app.userConfig.itemsSyntheseMode,false);
  app.sociogrammeSetType(app.userConfig.sociogramme_type);
}
app.pushUserConfig=function(){
  if(!app.isConnected){return;}
  $.post(app.serveur + "index.php?go=users&q=update"+app.connexionParam,{
    user_config:JSON.stringify(app.userConfig),
    user_id:app.userConfig.userID,
    sessionParams:app.sessionParams
  }, function(data) {
    app.render(data);       
  }
  );
}

app.setMemo=function(memo_type,memo_type_id,memo_data){  
  if (!app.checkConnection()) {return;} 
  //On met à jour les données
  $.post(app.serveur + "index.php?go=memos&q=add"+ app.connexionParam,
  {
    memo_type_id:memo_type_id,
    memo_type:memo_type,
    memo_data:memo_data,
sessionParams:app.sessionParams
  },
  function(data) {
    app.render(data);     
  });
}

app.getMemo=function(memo_type,memo_type_id){
var memos=app.userConfig.memos||[];
for (var i = memos.length - 1; i >= 0; i--) {
  var memo=memos[i];
if(memo.memo_type_id==memo_type_id && memo.memo_type==memo_type){
  return memo.memo_data;
}
}
return "";
}