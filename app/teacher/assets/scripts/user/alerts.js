/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.renderAlertesForm=function(){
  document.getElementById("alertesForm").style.display="block";
  $("#alertesForm").goTo();
  var newAlertWatched=app.newAlertWatched.join('~');
  var html=[];
  for(var i=0, lng=app.eventsAvailable.length;i<lng;i++){
//  if(i!=0){ html.push(' ou ');}
var style="btn-default";
if(newAlertWatched.indexOf(app.eventsAvailable[i].type)>=0){
 style="btn-primary";
}
html.push('<div class="btn '+style+' app.userConfig.itemsAvisMode" id="config-alerte-btn-'+app.eventsAvailable[i].type+'" onclick="app.switchAlertWatched(\''+app.eventsAvailable[i].type+'\');"><span class="glyphicon '+app.eventsAvailable[i].type+'"></span></div>');
}
document.getElementById("alerteEventsWatched").innerHTML=html.join('');
var html=[];
for(var i=0, lng=app.eventsAvailable.length;i<lng;i++){
  var style="btn-default";
  if(app.eventsAvailable[i].type==app.newAlertTrigerred){
   style="btn-primary";
 }
 html.push('<div class="btn '+style+' app.userConfig.itemsAvisMode" onclick="app.addAlertTrigerred(\''+app.eventsAvailable[i].type+'\');"><span class="glyphicon '+app.eventsAvailable[i].type+'"></span></div>');
}
document.getElementById("alerteEventTrigerred").innerHTML=html.join('');
for(var i=0, lng=app.userConfig.alerts.length;i<lng;i++){
  var alerte=app.userConfig.alerts[i];
  for(var j=0, llng=alerte.events_watched.length;j<llng;j++){
   document.getElementById('config-alerte-btn-'+alerte.events_watched[j]).disabled=true;
 }
}
}
app.switchAlertWatched=function(event_type){
  var n=0;
  for(var i=0, lng=app.newAlertWatched.length;i<lng;i++){
    if(app.newAlertWatched[i]==event_type){
      app.newAlertWatched.splice(i, 1);
      n++;
    }
  }
  if(n==0){
    app.newAlertWatched.push(event_type);
  }
  app.renderAlertesForm();
};
app.addAlertTrigerred=function(event_type){
  app.newAlertTrigerred=event_type;
  app.renderAlertesForm();
};
app.addAlerte=function(){
  var max=document.getElementById("alerteEventsMax").value;
  if(app.newAlertWatched.length==0 || app.newAlertTrigerred==""){
    app.alert({title:"Il faut choisir les événements à surveiller et l'événement à déclencher."});
    return;
  }
  var alert={
    events_watched:app.newAlertWatched,
    events_max:max,
    events_trigerred:app.newAlertTrigerred,
    texte:""
  };
  app.userConfig.alerts.push(alert);
  app.renderConfigAlertes();
  app.newAlertWatched=[];
  app.newAlertTrigerred="";
  document.getElementById("alertesForm").style.display="none";
  app.pushUserConfig();
};
app.delAlerte=function(num){
  app.userConfig.alerts.splice(num, 1);
  app.renderConfigAlertes();
  
  app.pushUserConfig();
};
app.renderConfigAlertes=function(){
 document.getElementById('alertes').innerHTML="";
 var html=[];
 for(var i=0, lng=app.userConfig.alerts.length;i<lng;i++){
  var alerte=app.userConfig.alerts[i];
  html.push('<div class="flex-rows well well-sm config_alerte">');
  html.push('<div class="flex-1">');
  for(var j=0, llng=alerte.events_watched.length;j<llng;j++){
    if(j!=0){ html.push(' ou ');}
    html.push(' <div class="btn btn-default app.userConfig.itemsAvisMode"><span class="glyphicon ' + alerte.events_watched[j] + '"></span></div>');
  }
  html.push(' x'+alerte.events_max+' <span class="glyphicon glyphicon-arrow-right"></span> ');
  html.push(' <div class="btn btn-default app.userConfig.itemsAvisMode"><span class="glyphicon ' + alerte.events_trigerred + '"></span></div>');
  html.push('</div>');
  html.push('<div>');
  html.push('<div class="btn btn-danger btn-xs" onclick="app.delAlerte('+i+')"><span class="glyphicon glyphicon-trash"></span> Supprimer</div>');
  html.push('</div>');  
  html.push('</div>');
}
document.getElementById("alertes").innerHTML=html.join('');
};