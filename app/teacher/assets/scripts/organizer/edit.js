/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.agendaEventAddFromEDT=function(classe_id,edt_num){
  app.agendaEditionInit();
  $('#home_agenda_bloc').goTo();
  document.getElementById('home_event_classe').value=classe_id;
  app.agendaFiveNext(classe_id,false,edt_num);
}
app.angendaCopyEvent=function(event_id){
  var view=app.currentView;
  if(app.agendaEventCopy==event_id){
    app.agendaEventCopy=null;
  }else{
   app.agendaEventCopy=event_id;
 } 
 $('.'+view+'_event_copy_btn').removeClass('btn-primary').addClass('btn-default');
 $('#'+view+'_event_'+event_id+'_copy_btn').removeClass('btn-default').addClass('btn-primary');
 document.getElementById('classe_agenda_paste').style.display = "block";
}
app.agendaEventPaste=function(){
  var view=app.currentView;
  if(app.agendaEventCopy==null){
   document.getElementById(view+'_agenda_paste').style.display = "none";
   return;
 }
 var event=app.getEventById(app.agendaEventCopy);
 if(!event){
   document.getElementById(view+'_agenda_paste').style.display = "none";
   return;
 }
 app.setAgendaEventIcon(event.event_icon);
 document.getElementById(view+'_event_edition').style.display = "block";
 document.getElementById(view+'_event_title').value=event.event_title;
 document.getElementById(''+view+'_event_color').value=event.event_color||"#E3E3E3";$('#'+view+'_event_btn').button('reset');   
 app.agendaEditor[''+view+''].pasteHTML(event.event_data);

 document.getElementById(view+'_event_edition_bloc_title').innerHTML="Éditer un événement"
 $('#'+view+'_event_edition').goTo();
}
app.agendaEventEdit=function(event_id){
 app.agendaCurrentEventEdit=event_id;
 var event=app.getEventById(event_id);
 var view=app.currentView;
 if(app.agendaEventCopy!=null){
   document.getElementById(''+view+'_agenda_paste').style.display = "block";
 }else{
   document.getElementById(''+view+'_agenda_paste').style.display = "none";
 }
 app.setAgendaEventIcon(event.event_icon);
 document.getElementById(''+view+'_event_edition').style.display = "block";
 document.getElementById(''+view+'_event_title').value=event.event_title;
 document.getElementById(''+view+'_event_color').value=event.event_color||"#E3E3E3";
 if(view=="home"){
  document.getElementById('home_event_classe').value=event.event_type_id;
}
$('#'+view+'_event_btn').button('reset');  
app.agendaEditor[view].pasteHTML(event.event_data);
document.getElementById(''+view+'_event_date').style.display="none";
document.getElementById(''+view+'_event_edition_bloc_title').innerHTML="Éditer un événement"
$('#'+view+'_event_edition').goTo();
}
app.addGroupeEvent=function(args){
  var groupe=app.currentClasse.currentGroupes[args.groupe];
  for (var i = groupe.length - 1; i >= 0; i--) {
    var eleve=groupe[i];
    app.agendaAddQuickEvent(args.event_type,eleve,true);
  };
}
app.agendaEventUpdate=function(event,refresh){
  if (!app.checkConnection()) {
    return;
  }
  event.sessionParams=app.sessionParams;
  $.post(app.serveur + "index.php?go=agenda&q=update"+ app.connexionParam,event, function(data) {
    app.render(data);   
    if(refresh!==false){
      app.agendaRender();
    }
    if(app.currentView=="eleve"){
     app.renderEleveEvents();
    }else if(app.currentView=="classe"){
   app.renderClassroomEvents();
    }     
  });
}

app.setAllEventsStateChecked=function(confirm){
  if(!confirm){
    app.alert({title:'Barrer tous les événements ?',type:'confirm'},function(){app.setAllEventsStateChecked(true);});
    return;
  }  
  for (var i = app.agenda.length - 1; i >= 0; i--) {
    var oEvent=app.agenda[i];
    if((oEvent.event_user != app.userConfig.userID)
      || (app.currentClasse.eleves.indexOf(oEvent.event_type_id) <0 &&  oEvent.event_type=="eleve") 
      || (oEvent.event_type_id!=app.currentClasse.classe_id && oEvent.event_type=="classe")
      || oEvent.event_state!="enable"){
      continue;
  }
  app.agendaEventUpdate({
    event_id:oEvent.event_id,
    event_state:"disable"
  });
};
}
app.agendaEventDelete=function(event_id,confirm){
  if (!app.checkConnection()) {
    return;
  }
  if(!confirm){
    app.alert({title:'Supprimer cet élément ?',type:'confirm'},function(){app.agendaEventDelete(event_id,true);});
    return;
  }
  $.post(app.serveur + "index.php?go=agenda&q=deleteEvent"+ app.connexionParam,{
    event_id:event_id,
sessionParams:app.sessionParams
  }, function(data) {
    app.render(data);    
    app.agendaRender();   
if(app.currentView=="eleve"){
  app.renderEleveEvents();
}
if(app.currentView=="classe"){
app.renderClassroomEvents();
}
  });

}
app.agendaAddQuickEvent=function(event_icon,eleve_id,checkAlert){
 if (!app.checkConnection()) {
  return;
}
if(document.getElementById('event_'+event_icon + '_' + eleve_id + '')){
 document.getElementById('event_'+event_icon + '_' + eleve_id + '').disabled="disabled"; 
}
 setTimeout(function(){
  var elem=document.getElementById('event_'+event_icon + '_' + eleve_id + '');
  if(elem){
    elem.disabled="";
  }
},2000);

var event_start=app.agendaNewEventStart;
var event_end=app.agendaNewEventEnd;
var allDay=app.agendaNewEventAllDay;
var now=$('#classe_agenda').fullCalendar( 'getCalendar').moment();
if(!event_start){
  event_start=now;   
}
if(!event_start || !event_start.isValid()){ 
  allDay="true";
  event_start=now;
}
var event_visibility=true;
var event_data=app.legends[event_icon]||"";
$.post(app.serveur + "index.php?go=agenda&q=addEvent"+app.connexionParam,{
 event_title:"",
 event_data:event_data,
 event_icon:event_icon,
 event_start:event_start.unix(),
 event_end:event_start.unix()*1+3600,
 event_color:"#e3e3e3",
 event_allDay:allDay,
 event_type:"eleve",
 event_visibility:event_visibility,
 event_periode:app.active_periode.periode_id,
 event_type_id:eleve_id,
 event_discipline:app.userConfig.matiere,
 event_date:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
}, function(data) {
  app.render(data);     
  app.agendaRender();
   app.renderClassroomEvents();
  if(checkAlert){
    var text="ème";
    var countEvent=app.countEvents(eleve_id,event_icon);
    if(countEvent==1){text="ère";}
    document.getElementById('eleve_'+eleve_id).innerHTML='<span class="glyphicon ' + event_icon + '"></span> : '+countEvent+text+' fois';
    app.checkAlerts(eleve_id);
  }
  else{
    document.getElementById('eleve_'+eleve_id).innerHTML='1 <span class="glyphicon ' + event_icon + '"></span>';
  }

}
);
}
app.agendaEventAdd=function(agenda){
  if (!app.checkConnection()) {
    return;
  } 
  var data="";
  var event_type_id=null;
  var event_type="";
  var view=app.currentView;
  var data=$("#"+view+"_agenda_editor > .ql-editor").html();

  if(app.currentView=="classe"){   
    event_type_id=app.currentClasse.classe_id;
    event_type="classe";
  }
  else if(app.currentView=="eleve"){ 
   event_type="eleve";
   event_type_id=app.currentEleve.eleve_id;
 }
 else{
  event_type="classe";
  var classe_id=document.getElementById('home_event_classe').value;
  if(classe_id=="-1"){
   $('#'+view+'_event_btn').button('reset');  
   app.alert({title:'Il faut choisir une classe.'});
   return;
 }
 event_type_id=classe_id;
}
if(data=="" && document.getElementById(view+'_event_title').value==""){
 $('#'+view+'_event_btn').button('reset');  
 app.alert({title:'Il manque un titre ou du texte.'});
 return;
}
if(!app.agendaCurrentEventEdit){
  var event_start=app.agendaNewEventStart;
  var event_end=app.agendaNewEventEnd;

  var allDay=false;
  if(!event_start || !event_start.isValid() || !event_end || !event_end.isValid()){ 
    allDay=true; 
    var date=$('#'+view+'_agenda').fullCalendar( 'getDate'); 
    event_start=date;
    event_end=date;
  }
  if(app.agendaNewEventAllDay){
    allDay=true;
  }
}
var event_visibility=true;
if(app.agendaNewEventIcon=="glyphicon-comment"){
  event_visibility=false;
}
if(!app.agendaCurrentEventEdit){
  $.post(app.serveur + "index.php?go=agenda&q=addEvent"+app.connexionParam,{
   event_title:document.getElementById(view+'_event_title').value,
   event_data:data,
   event_icon:app.agendaNewEventIcon,
   event_start:event_start.unix(),
   event_end:event_end.unix(),
   event_color:document.getElementById(view+'_event_color').value,
   event_allDay:allDay,
   event_type:event_type,
   event_visibility:event_visibility,
    event_periode:app.active_periode.periode_id,
   event_type_id:event_type_id,   
 event_discipline:app.userConfig.matiere,
   event_date:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
 }, function(data) {
  app.render(data);   
  
  $('#'+view+'_event_btn').button('reset'); 
  app.agendaEditor[view].pasteHTML('');
  app.agendaRender();
  $('#'+view+'_agenda').fullCalendar( 'unselect' );
}
);
}
else{
 $.post(app.serveur + "index.php?go=agenda&q=update"+app.connexionParam,{
  event_id:app.agendaCurrentEventEdit,
  event_title:document.getElementById(view+'_event_title').value,
  event_data:data,
  event_icon:app.agendaNewEventIcon,
  event_color:document.getElementById(view+'_event_color').value,
  event_state:"enable",
  event_visibility:event_visibility,
  event_date:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
}, function(data) {
  app.render(data);   
  app.hide(view+'_event_edition');
  app.agendaRender();  
}
);
}
}
app.agendaEditionInit=function(){
  app.agendaNewEventStart=null;
  var view=app.currentView;
  if(app.agendaEventCopy!=null){
   document.getElementById(''+view+'_agenda_paste').style.display = "block";
 }else{
   document.getElementById(''+view+'_agenda_paste').style.display = "none";
 }
 document.getElementById(''+view+'_event_color').value="#E3E3E3";
 app.agendaCurrentEventEdit=null;
 document.getElementById(''+view+'_event_edition').style.display = "block";
 document.getElementById(''+view+'_event_title').value="";
 $('#'+view+'_event_date').datepicker({
  format:'dd/mm/yyyy'
}).on('changeDate', function(ev){
  app.agendaNewEventStart=moment(ev.date.valueOf());
  app.agendaNewEventEnd=moment(ev.date.valueOf());
  app.agendaNewEventAllDay=true;
  var classe_id=-1;
  if(app.currentView=="classe" || (app.currentView=="eleve" && app.currentClasse)){
    classe_id=app.currentClasse.classe_id;
  }else{
    classe_id=document.getElementById('home_event_classe').value;
  }
  app.agendaFiveNext(classe_id,moment(ev.date.valueOf()));
});
$('#'+view+'_event_btn').button('reset');  
app.agendaEditor[view].pasteHTML(''); 
document.getElementById(''+view+'_event_date').value="";
$('#'+view+'_event_date').attr('placeholder','Sélectionner une date').css('display','inline-block').addClass('btn-default').removeClass('btn-primary');
document.getElementById(''+view+'_event_edition_bloc_title').innerHTML="Nouvel événement";  
app.setAgendaEventIcon('glyphicon-blackboard');
if(view=="classe" || view=="eleve"){
  app.agendaFiveNext(app.currentClasse.classe_id);
}
}
app.checkAlerts=function(eleve_id){
  if(app.timers[eleve_id]){clearTimeout(app.timers[eleve_id]);}
  app.timers[eleve_id]=setTimeout(function(){
   document.getElementById('eleve_'+eleve_id).innerHTML=app.renderClasseEleveName(eleve_id);
 }, 3000);
  for(var i=0,lng=app.userConfig.alerts.length;i<lng;i++){
    var alerte=app.userConfig.alerts[i];
    var events_watched=alerte.events_watched.join("~");
    var n=0;
    for (var j = 0, llng = app.agenda.length; j < llng; j++) {
      var oEvent = app.agenda[j];
      if(alerte.events_watched.indexOf(oEvent.event_icon)>=0 && oEvent.event_state=="enable" && oEvent.event_type=="eleve" && oEvent.event_type_id==eleve_id && oEvent.event_user==app.userConfig.userID){
        n++;
      }
    }
    if(n%alerte.events_max==0 && n>0){
     for (var j = 0, llng = app.agenda.length; j < llng; j++) {
      var oEvent = app.agenda[j];
      if(events_watched.indexOf(oEvent.event_icon)>=0 && oEvent.event_state=="enable" && oEvent.event_type=="eleve" && oEvent.event_type_id==eleve_id && oEvent.event_user==app.userConfig.userID){
        app.agendaEventUpdate({
          event_id:oEvent.event_id,
          event_state:"disable"
        });
      }
    }    
    $("#eleve_"+eleve_id).addClass('btn-danger');
    if(alerte.events_trigerred!=""){  
     app.agendaAddQuickEvent(alerte.events_trigerred,eleve_id,true);       
   }
 }
}
};