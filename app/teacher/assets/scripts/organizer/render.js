/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.agendaRender=function(){
  var prefix="classe";
  var view=app.currentView;  
  var agenda=app.agenda;
  if(app.currentView=="eleve"){
    prefix="eleve";
  }else if(app.currentView=="home"){
    prefix="home"
  }
  $('#'+view+'_agenda').fullCalendar( 'removeEventSource',  app.agendaEventsSource );
  app.agendaEventsSource=[];


  for (var i =0, lng= agenda.length; i <lng; i++) {
    var event=agenda[i];
    var user_pseudo=app.getUserById(event.event_user).user_pseudo;
    if(app.currentView=="eleve"){
     if ((event.event_user !==  app.userConfig.userID && !app.userConfig.ppView)       
      || (event.event_type=="eleve" && app.currentEleve.eleve_id!=event.event_type_id)
      || event.event_type=="classe"
     // || (event.event_discipline!=app.userConfig.matiere && event.event_discipline!=-1 && app.userConfig.matiere!=-1)
      ) {
      continue;
  }
}
else if(app.currentView=="classe"){
 if ((event.event_user !==  app.userConfig.userID && !app.userConfig.ppView) 
  || (event.event_type=="classe" && event.event_type_id!=app.currentClasse.classe_id)
  || (event.event_type=="eleve" && app.currentClasse.eleves.indexOf(event.event_type_id)<0)
//  || (event.event_discipline!=app.userConfig.matiere && event.event_discipline!=-1 && app.userConfig.matiere!=-1)
  ) { 
  continue;
}
}else{
  if ((event.event_user !==  app.userConfig.userID) 
    || (event.event_type!="classe")
   //  || (event.event_discipline!=app.userConfig.matiere && event.event_discipline!=-1 && app.userConfig.matiere!=-1)
    ) { 
    continue;
}
}
var event_class="event_disable";
var event_state_text="enable";
var event_state_btn="primary";
var event_data=event.event_data;
if(event.event_state=="enable"){
  event_class="";
  event_state_text="disable";
  event_state_btn="default";
}
var button_sup="";
var button_edit="";
var button_visibility="";
var button_checked="";
var button_copy_class="btn-default";
if(event.event_id==app.agendaEventCopy){
  button_copy_class="btn-primary";
}
var button_copy="";
if(event.event_type=="classe" && event.event_lock!=1){
  button_copy='<div title="Copier" class="'+view+'_event_copy_btn btn '+button_copy_class+' btn-xs connexion-requise" onclick="event.cancelBubble=true; app.currentClasseCalendarDate=$(\'#classe_agenda\').fullCalendar( \'getDate\' );app.angendaCopyEvent(\''+event.event_id+'\');" id="'+view+'_event_'+event.event_id+'_copy_btn">\
  <span class="glyphicon glyphicon-duplicate"></span>\
  </div>';
}
if(event.event_user == app.userConfig.userID && event.event_lock!=1){
  button_edit='<div title="Éditer cet événement." class="btn btn-default btn-xs connexion-requise" onclick="event.cancelBubble=true; app.currentClasseCalendarDate=$(\'#classe_agenda\').fullCalendar( \'getDate\' ); app.agendaEventEdit(\''+event.event_id+'\');">\
  <span class="glyphicon glyphicon-pencil"></span>\
  </div>';
  button_sup='<div title="Supprimer cet événement." class="btn btn-default btn-xs connexion-requise" onclick="event.cancelBubble=true; app.currentClasseCalendarDate=$(\'#classe_agenda\').fullCalendar( \'getDate\' );app.agendaEventDelete(\''+event.event_id+'\');">\
  <span class="glyphicon glyphicon-trash"></span>\
  </div>';
  button_visibility='<div title="Cliquer pour rendre visible aux élèves" class="btn btn-default btn-xs connexion-requise" onclick="event.cancelBubble=true; app.currentClasseCalendarDate=$(\'#classe_agenda\').fullCalendar( \'getDate\' );app.agendaEventUpdate({event_id:'+event.event_id+',event_visibility:true});">\
  <span class="glyphicon glyphicon-eye-close"></span>\
  </div> ';
  if(event.event_visibility==="true"){
    button_visibility='<div title="Cliquer pour masquer aux élèves" class="btn btn-primary btn-xs connexion-requise" onclick="event.cancelBubble=true; app.currentClasseCalendarDate=$(\'#classe_agenda\').fullCalendar( \'getDate\' );app.agendaEventUpdate({event_id:'+event.event_id+',event_visibility:false});">\
    <span class="glyphicon glyphicon-eye-open"></span>\
    </div> ';
  }
  button_checked='<div title="Barrer cet événement." class="btn btn-'+event_state_btn+' btn-xs connexion-requise" onclick="event.cancelBubble=true; app.currentClasseCalendarDate=$(\'#classe_agenda\').fullCalendar( \'getDate\' );app.currentClasseCalendarDate=$(\'#classe_agenda\').fullCalendar( \'getDate\' );app.agendaEventUpdate({event_id:'+event.event_id+',event_state:\''+event_state_text+'\'});">\
  <span class="glyphicon glyphicon-font barre"></span>\
  </div> ';
}
app.legendsUsers.push(event.event_user);
var title=ucfirst(app.organizerEventTitleRender(event.event_title));
var icon="";
if(event.event_icon){
  icon='<span class="glyphicon '+event.event_icon+'"></span>';
}
if(event.event_type=="eleve"){
  if(app.currentView=="classe"){
    title=app.renderEleveNom(app.getEleveById(event.event_type_id));
    if(event.event_title!=""){
      title+=" -";
    }
    title+=" "+ucfirst(event.event_title)+"";
  }
}
if(app.currentView=="home"){
  var classe=app.getClasseById(event.event_type_id);
  if(!classe){
    continue;
  }
  title=classe.classe_nom+" - "+title;
}
if(event.event_icon=="glyphicon-home"){
  if(title==""){
    title="Devoirs";
  }
}
var event_color="";
if(event_data==""){
  event_data="&nbsp;";
}

if(app.currentView=="home"){ 
   event_color=classe.color;
 }
 else{

if(app.userConfig.ppView){
  event_color="#" + app.getColor(user_pseudo);
}else{
 
  if(event.event_color!="#e3e3e3"){
    event_color=event.event_color;
  }else{   
    if(app.currentClasse){
       event_color=app.currentClasse.color;
     }else{
       event_color="#e3e3e3";
     }
  }
}
 }

var eventDataStyle="display:block;";
if(!app.userConfig.agendaEventDetails){
  eventDataStyle="display:none;";
}
app.legendsUsers.push(event.event_user);
var oEvent={
  title: '<div class="calendar-event-title '+event_class+'">'+icon+' '+title+'</div><span class="'+event_class+'"><span class="event-data" id="'+view+'-event-data-'+event.event_id+'" style="'+eventDataStyle+'">'+ucfirst(event_data)+'</span></span><div class="calendar-event-toolbar"><div class="btn-group">'+button_copy+button_edit+button_checked+'</div><div class="btn-group">'+button_visibility+button_sup+'</div></div>',
  start: event.event_start*1000,
  end: event.event_end*1000,
  textColor:"#000000",               
  borderColor:event_color,
  event_id:event.event_id,
  event_type:'agenda',
  event_state:event.event_state,
  event_data:event.event_data,
  event_color:event.event_color,
  event_icon:event.event_icon
};
if(oEvent.event_icon=="glyphicon-blackboard"){
  oEvent.backgroundColor=app.agendaEventBlackBoardBackGround;
  oEvent.start+=1;
}
else if(oEvent.event_icon=="glyphicon-home"){
  oEvent.backgroundColor=app.agendaEventHomeBackGround;
}
else{
  oEvent.backgroundColor=app.agendaEventCommentBackGround;
}
if(event.event_allDay=="true"){
  oEvent.allDay=true;
}else{
  oEvent.allDay=false;
}
app.agendaEventsSource.push(oEvent);
};
$('#'+view+'_agenda').fullCalendar( 'addEventSource',  app.agendaEventsSource );
if(view=="classe"){
  if (app.colorCalendarBy == "user") {
    app.renderLegendsUsers('classroom-usersLegend');
  }
  app.classeDiagrammeBarre();
}
}