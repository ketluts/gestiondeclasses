/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.agendaGoToday=function(){
  var view=app.currentView;
  var agendaView= $('#'+view+'_agenda').fullCalendar( 'getView');
  $('#'+view+'_agenda').fullCalendar( 'today');
  if(agendaView.name=="basicThreeDay"){
    $('#'+view+'_agenda').fullCalendar( 'incrementDate', { days:-1});
  } 
}
app.setAgendaEventIcon=function(type_event,div_id){
 var view=app.currentView;
 app.agendaNewEventIcon=type_event; 
 $('.event-icon').removeClass('btn-primary').addClass('btn-default'); 
 $('.event-icon-'+type_event).addClass('btn-primary'); 
}

app.countEvents=function(eleve_id,event_type){
 var n=0;
 for (var i = 0, lng = app.agenda.length; i < lng; i++) {
  var oEvent = app.agenda[i];
  if(oEvent.event_icon==event_type && oEvent.event_type_id==eleve_id && oEvent.event_type=="eleve" && oEvent.event_user==app.userConfig.userID && oEvent.event_state != "disable"){
    n++;
  }
}
return n;
}
app.getEventById=function(event_id){
  for (var i = app.agenda.length - 1; i >= 0; i--) {
   if(app.agenda[i].event_id==event_id){
    return app.agenda[i];
  }
}
return false;
}
app.buildAgendaDatalist=function(){
  var titres=[];
  for (var i = app.agenda.length - 1; i >= 0; i--) {
    var event=app.agenda[i];
    if(event.event_user!=app.userConfig.userID){continue;}
    if(!event['event_title'] || event['event_title']=="Visa"){continue;}
    titres=titres.concat(event['event_title']);
  };
  app.agendaTitreDatalist=arrayUnique(titres.map(function(value){if(value){return value.trim();}})).sort();
  //On génére le Datalist pour les titres d'événements
  var options='';
  for(var i = 0,lng=app.agendaTitreDatalist.length; i < lng; i++){
    options += '<option value="'+app.agendaTitreDatalist[i]+'" />';
  }
  document.getElementById('list_event_titre').innerHTML = options;
};
app.agendaEventDetailsToggle=function(value,enableSave){
  if(value){
    $(".agenda-events-details-btn").removeClass('btn-default').addClass('btn-primary');
    $(".event-data").css('display','block');
  }
  else{
    $(".agenda-events-details-btn").removeClass('btn-primary').addClass('btn-default');
    $(".event-data").css('display','none');
  }
  app.userConfig.agendaEventDetails=value;

  if(enableSave!==false){
    
    app.pushUserConfig();
  }
}
app.agendaOneEventDetailsToggle=function(event_id){
  var div=$('#'+app.currentView+'-event-data-'+event_id);
  if(div.css('display')=='block'){  
    div.css('display','none');
  }
  else{
   div.css('display','block');
 }
}
app.agendaFiveNext=function(classe_id,date,edt_num){
  date=date||moment().unix()*1000;
  var toSelect=null;
  var edtToSelect=0;
  if(edt_num>=0){
    edtToSelect=app.userConfig.edt[edt_num].start*1000;
  }
  var view=app.currentView;
  var edt=app.userConfig.edt||[];
  var edtDays=[];
  edtDays['AB']={
    liste:[],
    cours:[]
  };
  edtDays['A']={
    liste:[],
    cours:[]
  };
  edtDays['B']={
    liste:[],
    cours:[]
  };
  edt=app.orderBy(edt,"start","ASC");
  for (var i = edt.length - 1; i >= 0; i--) {
    if(edt[i].dataClasseId!=classe_id){
      continue;
    }
    edtDays[edt[i].dataWeek].liste.push(moment.unix(edt[i].start).days());
    edtDays[edt[i].dataWeek].cours.push(edt[i].start*1000);
  }
  var next=[];
  var day=moment(date);
  day.subtract(1, 'd');
  for (var i = 0; i <31; i++) {
   day.add(1, 'd');
   var week=app.getWeekName(day);
   for (var j = edtDays[week].liste.length - 1; j >= 0; j--) {
    if(edtDays[week].liste[j]==day.day()){
     var h=day.hours();
     var m=day.minutes();
     var s=day.seconds();
     var ms=day.milliseconds();
     if(edtDays[week].cours[j]==edtToSelect && toSelect==null){
      toSelect=next.length;
    }
    var cours=moment(edtDays[week].cours[j]);
    var nMoment=moment((day.unix()-s-m*60-h*3600)*1000+cours.hours()*3600000+cours.minutes()*60000);
    next.push(nMoment.unix());
  }
}
for (var j = edtDays['AB'].liste.length - 1; j >= 0; j--) {
  if(edtDays['AB'].liste[j]==day.day()){
    var h=day.hours();
    var m=day.minutes();
    var s=day.seconds();
    var ms=day.milliseconds();
    if(edtDays['AB'].cours[j]==edtToSelect && toSelect==null){
      toSelect=next.length;
    }
    var cours=moment(edtDays['AB'].cours[j]);
    var nMoment=moment((day.unix()-s-m*60-h*3600)*1000+cours.hours()*3600000+cours.minutes()*60000);
    next.push(nMoment.unix());
  }
}
}
if(toSelect==null){
  toSelect=0;
}
if(next.length>0){
  var html=[];
  var n=Math.min(next.length,app.agendaNextDaysNb);
  next.sort();
  for (var i =0, lng= n; i<lng; i++) {
    var day=next[i]; 
    html.push('<div class="btn btn-default agenda-next-day btn-sm" id="'+view+'-agenda-next-day-'+i+'" onclick="app.agendaSelectNextDay('+day+','+i+');">');
    html.push(moment(day*1000).format('ddd DD MMM - HH[h]mm'));
    html.push('</div>');
  }
  if(toSelect>=n){
    var day=next[toSelect]; 
    html.push('<div class="btn btn-default agenda-next-day btn-sm" id="'+view+'-agenda-next-day-'+toSelect+'" onclick="app.agendaSelectNextDay('+day+','+toSelect+');">');
    html.push(moment(day*1000).format('ddd DD MMM - HH[h]mm'));
    html.push('</div>');
  }
  document.getElementById(view+'-agenda-next').innerHTML=html.join('');
  if(!app.agendaNewEventStart){
    app.agendaSelectNextDay(next[toSelect],toSelect);
  }
}
else{
  document.getElementById(view+'-agenda-next').innerHTML="";
}
}
app.agendaSelectNextDay=function(day,num){
  var view=app.currentView;
  $('.agenda-next-day').removeClass('btn-primary').addClass('btn-default');
  $('#'+view+'-agenda-next-day-'+num+'').removeClass('btn-default').addClass('btn-primary');
  $('#'+view+'_event_date').removeClass('btn-primary').addClass('btn-default').html('Sélectionner une date');
  app.agendaNewEventAllDay=false;
  day=moment(day*1000);
  app.agendaNewEventStart=day;
  app.agendaNewEventEnd=day;
  if(!day.hasTime()){
   app.agendaNewEventAllDay=true;
 }
}
app.organizerEventTitleRender=function(title){
  return title.replace(/\*/g,"");
}
