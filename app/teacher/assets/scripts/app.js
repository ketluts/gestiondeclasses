/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
/*******/
/* Run */
/*******/
$(document).ready(function() {
  app.timerPing=setTimeout(app.ping, 0);
  app.init();
  app.keyboardInit();
  app.touchInit(); 
  app.mouseInit();
  app.pluginsLauncher('appInit');
  app.navigationInit(); 
  app.connexionRender();    
});
app.load=function() {
  //On vérifie les paramètres de connexion
  // app.connexionParam est déprécié, on utilise app.sessionParams en POST pour identifier la session en cours
  app.connexionParam = "&nom_etablissement=" + app.nom_etablissement + "&pseudo_utilisateur=" + app.pseudo_utilisateur + "&sessionID=" + app.sessionID + "&version=" + app.version;
 // app.connexionParam="";
  app.sessionParams=JSON.stringify({
    nom_etablissement:app.nom_etablissement,
    pseudo_utilisateur:app.pseudo_utilisateur,
    sessionID:app.sessionID,
    version:app.version
  });

   //Chargement
   $.ajax({url: app.serveur + "index.php?go=loader" + app.connexionParam,
    timeout: 60000,
    method:"POST",
    data:{ 
      time:Math.floor(app.myTime()/1000),
      sessionParams:app.sessionParams
    }
  }).done(function(data){
    app.render(data);   
    app.navigate(app.urlHash);
    app.getDiscussions();
   
  });
};

app.ping=function() {
 clearTimeout(app.timerPing);
 $.ajax({type: "HEAD",cache:false,url: app.serveur + "ping.php",
  timeout: 10000
})
 .done(function(data) {
  if (!app.isConnected) {
    app.isConnected = true;
    $.ajax({url: app.serveur + "index.php?go=loader" + app.connexionParam,
      timeout: 5000,
      method:"POST",
      data:{ 
        time:Math.floor(app.myTime()/1000),
        sessionParams:app.sessionParams
      }
    })
    $(".connexion-requise").prop("disabled", false);
  }
}
).fail(
function() {
  app.isConnected = false;
  $(".connexion-requise").prop("disabled", true);
}
);
app.timerPing=setTimeout(app.ping, 15000);
};
app.pluginsLauncher=function(callback){
var plugins=app.plugins;
   for (var i = plugins.length - 1; i >= 0; i--) {
   var plugin=plugins[i];
   if(!plugin.enabled){continue;}
   if(plugin[callback]){
    (plugin[callback])();
  }
}
}
