/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

Handlebars.registerHelper('eleve_nom', function() {
	var eleve=app.getEleveById(this);
	return eleve.eleve_nom.toUpperCase();
});
Handlebars.registerHelper('eleve_prenom', function() {
	var eleve=app.getEleveById(this);
	return ucfirst(eleve.eleve_prenom);
});
Handlebars.registerHelper('eleve_id', function() {
	var eleve=app.getEleveById(this);
	return eleve.eleve_id;
});
Handlebars.registerHelper('eleve_token', function() {
	var eleve=app.getEleveById(this);
	return eleve.eleve_token;
});
Handlebars.registerHelper('classroom_name', function() {	
	return this.classe_nom.replace(/~/g,'');
});
Handlebars.registerHelper('classroom_nb_students', function() {	
	var n=this.eleves.length;
	return n+" élève"+app.pluralize(n,'s');
});
Handlebars.registerHelper('controle_date', function() {	
	var date=this.controle_date;
		return moment.unix(date).format('DD MMM YYYY');
});
Handlebars.registerHelper('feedback_date', function() {	
	var date=this.feedback_date;
		return moment.unix(date).format('DD MMM YYYY - HH:mm');
});
Handlebars.registerHelper('feedback_nbItems', function() {	

	return this.nbItems+' item'+app.pluralize(this.nbItems,'s');
});
Handlebars.registerHelper('feedback_nbStudents', function() {	
	return this.nbStudents+' élève'+app.pluralize(this.nbStudents,'s');
});
