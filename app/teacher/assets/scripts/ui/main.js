/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.uiInit=function(){
//On initialise l'affichage
app.loadUi++;
app.agendaEditor=[];
app.agendaEditor['classe']=new Quill('#classe_agenda_editor', {
  modules: {  
    toolbar: app.toolbarOptions
  },
  styles: false,
  theme: 'snow'
});   
app.agendaEditor['eleve'] = new Quill('#eleve_agenda_editor', {
  modules: {   
   toolbar: app.toolbarOptions
 },
 styles: false,
 theme: 'snow'
}); 
app.agendaEditor['home'] = new Quill('#home_agenda_editor', {
  modules: {   
    toolbar: app.toolbarOptions    
  },
  styles: false,
  theme: 'snow'
});  
$('#agenda-export-date-start').datepicker({
  format:'dd/mm/yyyy'
});
$('#agenda-export-date-end').datepicker({
  format:'dd/mm/yyyy'
});
$('#millesime_start').datepicker({
  format:'dd/mm/yyyy'
});
$('#millesime_end').datepicker({
  format:'dd/mm/yyyy'
});

$("#item_filters_acquisition_slider").slider({
  min:0,
  max:100,
  step:0.01,
  values:[0,100],
  range:true
}); 

if(app.hebergeur.mail!=""){
$('#footer-hebergeur').html(' | Hébergé par <a href="mailto:'+app.hebergeur.mail+'">'+app.hebergeur.nom+'</a>');
}
$('#footer-version').html(app.version);

app.hide('classroomEventsStats');

$.get(app.serveur + "index.php?go=school&q=getAll", function(data) {
  app.render(data);         
}
);  
}
app.updateTextareaHeight=function(input) {
    input.style.height = 'auto';
    input.style.height = input.scrollHeight+'px';
}