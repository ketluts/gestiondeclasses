/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.togglePP=function(forced_view,enableSave) {
 app.legendsUsers=[];
 if(forced_view==true){
  app.userConfig.ppView=false;
}
else if(forced_view===false){
 app.userConfig.ppView=true;
}
if (app.userConfig.ppView) {
  app.userConfig.ppView = false;
}
else{
  app.userConfig.ppView = true;
} 
if (app.userConfig.ppView) {
  $(".button_pp").removeClass('btn-default').addClass('btn-primary');
} else {  
  $(".button_pp").removeClass('btn-primary').addClass('btn-default'); 
}
if(enableSave!==false){
 app.pushUserConfig();
 app.navigate(app.currentHash,true);
}
};