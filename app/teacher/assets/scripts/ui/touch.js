/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.handleStart=function(evt){	
	var pos = $('#classroom-sociogramme-canvas').offset();
	var touches = evt.changedTouches;
	mouse.x=touches[0].pageX-pos.left;
	mouse.y=touches[0].pageY-pos.top;
	app.start_move();
	if(app.preventTouch){
		evt.preventDefault();	
	}
}
app.handleMove=function(evt){	
	var touches = evt.changedTouches;
	app.movemouse(touches[0]);	
	if(app.preventTouch){
		evt.preventDefault();	
	}
}
app.handleEnd=function(evt){
	app.end_move();
	if(app.preventTouch){
		evt.preventDefault();	
	}
}
app.touchInit=function(){
	var el = document.getElementById("classroom-sociogramme-canvas");
	el.addEventListener("touchstart", app.handleStart, false);
	el.addEventListener("touchend", app.handleEnd, false);
	el.addEventListener("touchleave", app.handleEnd, false);
	el.addEventListener("touchmove", app.handleMove, false);
}