/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.bilansItemsClassroomSkillsTab=function(classe_id){
	var classe=app.getClasseById(classe_id);
	var eleves=classe.eleves;
	app.itemsStudentsProgressionsBuild(eleves);
	//var millesime=app.getPeriodeById(app.active_periode.periode_parent);
	var html=[];
	var html_head=[];
	html.push('<div class="nobreak">');
	var date=moment().format('DD MMM YYYY');  
	html.push('<div class="h4 text-center">Relevé des acquis - '+date+'</div>');
	html.push("<table class='table table-striped table-bordered'>");
	html.push("<tbody>");
	for (var i =0,lng=eleves.length ; i<lng; i++) {
		var eleve_id=eleves[i];
		var eleve=app.getEleveById(eleve_id);
		var progressionsByTag=app.itemsStudentGetProgressionByTags(eleve_id);
		var values=progressionsByTag.values;
		if(i==0){
			html_head.push("<thead>");
			html_head.push('<tr>');  
			var tags=progressionsByTag.tags;
			html_head.push("<th>Élèves</th>"); 
			for (var k =0,lllng= tags.length ; k<lllng; k++) {
				var tag=tags[k];	
				html_head.push("<th>");
				//html_head.push('<div class="classroom-tests-table-title">');  
				html_head.push(tag);
				//html_head.push("</div>");
				html_head.push("</th>");
			}
			html_head.push('</tr>');
			html_head.push("</thead>");
		}
		html.push("<tr>");
		html.push('<td style="font-weight:bold;">');
		html.push(eleve.eleve_nom.toUpperCase()+" ");
		html.push(ucfirst(eleve.eleve_prenom));
		html.push('</td>');
		for (var j =0,llng= values.length; j<llng; j++) {
			var progression=values[j];
			html.push("<td>");
			//html.push('<div class="pie">'+Math.round(progression)+'%</div>');
			if(progression==-1){
				html.push('-');
			}else{
				html.push(progression);	
			}
			html.push("</td>");
		}
		html.push("</tr>");
	}
	html.push("</tbody>");
	html=html.concat(html_head);
	html.push("</table>");
	html.push('</div>');
	return html.join('');
	// [].forEach.call(document.getElementsByClassName("pie"), function (pie) {
	// 	var p = parseFloat(pie.textContent);
	// 	var NS = "http://www.w3.org/2000/svg";
	// 	var svg = document.createElementNS(NS, "svg");
	// 	var circle = document.createElementNS(NS, "circle");
	// 	var title = document.createElementNS(NS, "title");
	// 	circle.setAttribute("r", 16);
	// 	circle.setAttribute("cx", 16);
	// 	circle.setAttribute("cy", 16);

	// 	circle.setAttribute("stroke-dasharray", p + " 100");
	// 	svg.setAttribute("viewBox", "0 0 32 32");
	// 	title.textContent = pie.textContent;
	// 	pie.textContent = '';
	// 	svg.appendChild(title);
	// 	svg.appendChild(circle);
	// 	pie.appendChild(svg);
	// });
}
app.bilansItemsSyntheseSetMode=function(mode){
	app.userConfig.itemsSyntheseMode=mode;
	app.bilansItemsClassroomSkillsTab();
}
app.bilansItemsStudentSkillsTab=function(eleve_id){
	var html=[];
	var	eleve=app.getEleveById(eleve_id);		
	var date=moment().format('DD MMM YYYY');  
	html.push('<div class="nobreak">');	
	html.push('<div class="h4 text-center">Relevé des acquis - '+date+'</div>');		
	for (var c=app.itemsCategories.length-1; c >=0; c--) {
		var is_render=false;
		temp_html=[];
		var categorie=app.itemsCategories[c];
		temp_html.push('<div class=" bilans-title">');
		temp_html.push(ucfirst(categorie['name']));
		temp_html.push('</div>');
		for (var i =0, llng=categorie['sous_categories'].length;i<llng; i++) {
			is_sous_cat_render=false;
			temp_sous_cat_html=[];
			var sous_categorie=categorie['sous_categories'][i];
			if(sous_categorie['sous_categorie_name']!="Non classés"){
				temp_sous_cat_html.push('<div>');
				temp_sous_cat_html.push(ucfirst(sous_categorie['sous_categorie_name']));
				temp_sous_cat_html.push('</div>');	
			}			
			temp_sous_cat_html.push('<table>');
			for (var j = 0,lllng=sous_categorie['list'].length ; j<lllng; j++) {
				var item=sous_categorie['list'][j];	
				if(!app.itemsIsVisible(item)){
					continue;
				}

				var value="-";
				if(eleve.progression[item.item_id]){
					value=eleve.progression[item.item_id]['value'];
				}
				if(value<0){continue;}			

				is_render=true;
				is_sous_cat_render=true;
				temp_sous_cat_html.push('<tr>');				
				temp_sous_cat_html.push('<td style="width:20px;">');
				if(item['item_vue']=="cursor"){
					var color=app.itemsProgressionStyleGet(value,item.item_id).color;
					if(item['item_vue']=="cursor"){
						temp_sous_cat_html.push('<div style="width:15px;height:15px;background-color:'+color+';"></div>');
					}						
				}
				else{
					if(value>0){
						temp_sous_cat_html.push('<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB5UlEQVQ4jWNgoAA0rFMWI1/zWhWZxnWqj8nUrKbUtE71Vf0ala8ka65ZoarZuF7tdf0a1f+1K5QjSNJcu0bFsHG96tuOrUq/a1cpryNN8yoVy/q1yu+nHlT4X7tS+UX5TCV+uGTDEj2LpqVG/9s2q1hi01y1XM65brXShzkn5P/Xr1D8WLpIwR5D84Mfvf/bN2l/bd2s5Izi59VyPtUrFD7MPy3/v2uL/JeKRQoTUUxvWmr0f8Mtvf9LLyj+v/ap5n/DWuUPDauVrRgYGBhqVytEVC9T+Ljhlt7/mUfk/5XOl78T2sDAhmJA1lRRnsKZck8Wn1P8tuyi/P/dD53+Vy9VeN+0UnNu1RKI5iXnlf6XzpX/mDtDTgtrABX0CwgUzZZ7tPi8ys9llxT+b7il9795leb/Dbf0/i+/pPi/fqX856JZ8iV4QzmtR0qkaKbcsyXn1X4vu6Tw//////+XXZT/P3mP8p/iWfInGBgYGPEaAPWORPFM+ZdzT6v/XXZJ4f+SCxr/i2fJv8/ok5EmqBlhiJRs8Uy5NwvOaP+rXiL/uXC6XBjRmqFAUteW179whtyXsCLRPAYGBlMGBgZjBgYGHQYGBiUGBgZJQgZIMzAwKKmachuoGrFpMjAwiDEwMAjgUgwA4GrGcSAe8CYAAAAASUVORK5CYII=" />');	
					}
				}
				temp_sous_cat_html.push('</td>');
				temp_sous_cat_html.push('<td class="bilans-item-name">');
				temp_sous_cat_html.push(item['item_name']);
				temp_sous_cat_html.push('</td>');
				temp_sous_cat_html.push('</tr>');
			}
			temp_sous_cat_html.push('</table>');
			if(is_sous_cat_render){
				temp_html=temp_html.concat(temp_sous_cat_html);
			}
		}
		if(is_render){
			html=html.concat(temp_html);
		}
	}
	html.push('</div>');
	return html.join('');
}