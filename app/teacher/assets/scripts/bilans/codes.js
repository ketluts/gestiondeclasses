/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.bilansClassroomCodes=function(classe_id){
  var classe=app.getClasseById(classe_id);
  var source   = document.getElementById("template-code-eleve").innerHTML;
  var template = Handlebars.compile(source);
  var context = {
   eleves:classe.eleves,
   serveur:app.serveur,
   school:app.nom_etablissement,
   appUrl:app.url
 };
 return template(context);
}

app.bilansStudentCode=function(eleve_id){
  var source   = document.getElementById("template-code-eleve").innerHTML;
  var template = Handlebars.compile(source);
  var context = {
   eleves:[eleve_id],
   serveur:app.serveur,
   school:app.nom_etablissement,
   appUrl:app.url
 };
 return template(context);
}