/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.bilansClassroomOrganizer=function(classe_id){
  var exportStart=document.getElementById('agenda-export-date-start').value;
  var exportEnd=document.getElementById('agenda-export-date-end').value;
  if(exportStart!="" && exportEnd!=""){
    var start=moment(exportStart, "DD/MM/YYYY").unix();
    var end=moment(exportEnd, "DD/MM/YYYY").unix();
  }  
  //On range les événements par classe
  var agenda=[];
  for (var i = app.agenda.length - 1; i >= 0; i--) {
    var event=app.agenda[i];
    if(event.event_type!='classe' 
      || (event.event_type_id!=classe_id && classe_id!=-1)
      || event.event_user!=app.userConfig.userID){
      continue;
  }
  if(exportStart!="" && exportEnd!=""){
    if(event.event_start<start 
      || event.event_start>end){
      continue;
  }
}
if(app.agendaExportType.indexOf(event.event_icon)<0){
  continue;
}
agenda.push(event);

}
  //On génére l'agenda pour chaque classe
  var html=[];
  var agenda_classe=agenda;
  var classe=app.getClasseById(classe_id);
  var currentDate="";
  var currentMonth="";
  html.push('<div class="nobreak">');
  var date=moment().format('DD MMM YYYY');  
  html.push('<div class="h4 text-center">Cahier de texte - '+date+'</div>');
  if(i!=agenda.length - 1){
    html.push('<div class="pdf-agenda-classe">');
  }
  else{
    html.push('<div>');
  }
  agenda_classe=app.orderBy(agenda_classe,'event_start','DESC');
  for (var j =0,lng= agenda_classe.length; j <lng; j++) {
    var event=agenda_classe[j];
    var date=moment(event.event_start*1000).format("DD MMM YYYY");
    var month=moment(event.event_start*1000).format("MMMM YYYY");
    var color=classe.color;
    var titre="";
    var bgColor="#ffffff";
    if(event.event_icon=="glyphicon-home"){
      bgColor="#F0F0F0";
      titre="Devoirs - "
    }
    if(month!=currentMonth){
     html.push('<h3 class="cdt_date">');
     html.push(ucfirst(month));
     html.push('</h3>');
     currentMonth=month;
   }
   if(date!=currentDate){      
    html.push('<h4 class="cdt_date">');
    html.push(date);
    html.push('</h4>');
    currentDate=date;
  }
  html.push('<div class="cdt_item"  style="background-color:'+bgColor+';border-color:'+color+';">');
  html.push('<span class="cdt_title">');
  html.push(titre+event.event_title);
  html.push('</span>');
  html.push(event.event_data);
  html.push('</div>');
}
html.push('</div>');
html.push('</div>');
return html.join('');
}
app.bilansStudentEvents=function(eleve_id) {  
  var eleve=app.getEleveById(eleve_id);
  var html = [];
  //var date=moment().format('DD MMM YYYY');  
  var millesime=app.getPeriodeById(app.active_periode.periode_parent);
  html.push('<div class="nobreak">');
 // html.push('<h2>'+app.renderEleveNom(eleve)+' - Événements - '+date+'</h2>');
   html.push('<div class="h4 text-center">Relevé des événements - '+millesime.periode_titre+'</div>');
  html.push('<table class="table">');
  html.push('<tr>');
  html.push('<td>');
  html.push('</td>');
  for (var i =0,lng= app.periodes.length ; i <lng; i++) {
    var periode=app.periodes[i];
    if(periode.periode_type=="m" || periode.periode_parent!=app.active_periode.periode_parent){continue;}
    html.push('<td>');
    html.push(periode.periode_titre);
    html.push('</td>');
  }
  html.push('</tr>');
  for (var k = 0, lng = app.eventsAvailable.length; k < lng; k++) {
    var event_type=app.eventsAvailable[k].type;
    if(app.userConfig.eventsWhiteList.indexOf(event_type)<0){continue;}
    var legende=app.legends[event_type]||"";
    html.push('<tr id="bilan-student-events-'+event_type+'" class="student-events-row">');
    html.push('<td class="text-left">');
    html.push('<span class="glyphicon ' + event_type + '"></span> ');   
    html.push(legende);
    html.push('</td>');
    for (var i =0,llng= app.periodes.length ; i <llng; i++) {
      var periode=app.periodes[i];
        if(periode.periode_type=="m" || periode.periode_parent!=app.active_periode.periode_parent){continue;}
      html.push('<td class="h5">');
      html.push('<span id="bilan-eleveEvents_'+periode.periode_id+'_' + event_type + '_counter_enable">0</span> <small>/<span id="bilan-eleveEvents_'+periode.periode_id+'_' + event_type + '_counter_total">0</span></small>');
      html.push('</td>');
    }    
    html.push('</tr>');
  }
  html.push('</table>');
  html.push('</div>');
  document.getElementById('bilans-student-preview').innerHTML = html.join('');
  app.eleveEventsList=[];
  var tabEventsCounter=[];
  for (var i = app.agenda.length - 1; i >= 0; i--) {
    var oEvent = app.agenda[i];
    if(app.userConfig.eventsWhiteList.indexOf(oEvent.event_icon)<0){continue;}
    if (oEvent.event_type_id != eleve_id || oEvent.event_type!="eleve") {continue;}
    if(oEvent.event_user != app.userConfig.userID && !app.userConfig.ppView){continue;}
    app.eleveEventsList.push(oEvent);
    var periode=oEvent.event_periode;
      //if(periode.periode_type=="m" || periode.periode_parent!=app.active_periode.periode_parent){continue;}
    tabEventsCounter[periode]=tabEventsCounter[periode]||[];
    if (oEvent.event_state == "disable") {
    }else{
      if(tabEventsCounter[periode][oEvent.event_icon+'_enable']){
        tabEventsCounter[periode][oEvent.event_icon+'_enable']++;
      }else{
        tabEventsCounter[periode][oEvent.event_icon+'_enable']=1;
      }
    }     
    if(tabEventsCounter[periode][oEvent.event_icon+'_total']){
      tabEventsCounter[periode][oEvent.event_icon+'_total']++;
    }else{
      tabEventsCounter[periode][oEvent.event_icon+'_total']=1;
    }
  }
  for (var k = 0, lng = app.eventsAvailable.length; k < lng; k++) {
    var event_type=app.eventsAvailable[k].type;
    var is_visible=false;
    if(app.userConfig.eventsWhiteList.indexOf(event_type)<0){continue;}
    for (var i = app.periodes.length - 1; i >= 0; i--) {
      var periode=app.periodes[i];  
        if(periode.periode_type=="m" || periode.periode_parent!=app.active_periode.periode_parent){continue;}
      tabEventsCounter[periode.periode_id]=tabEventsCounter[periode.periode_id]||[];
      var total=tabEventsCounter[periode.periode_id][event_type+'_total'];
      if(total){
        is_visible=true;
        document.getElementById('bilan-eleveEvents_'+periode.periode_id+'_' + event_type + '_counter_total').innerHTML=total;
        document.getElementById('bilan-eleveEvents_'+periode.periode_id+'_' + event_type + '_counter_enable').innerHTML=tabEventsCounter[periode.periode_id][event_type+'_enable']||0;
      } 
    }
    if(!is_visible){
      $('#bilan-student-events-'+event_type+'').css('display','none');
    }else{        
      $('#bilan-student-events-'+event_type+'').css('display','');
    } 
  }
  return document.getElementById('bilans-student-preview').innerHTML;
};
app.agendaExportTypeToggle=function(type){
  var types=app.agendaExportType;
  if(types.indexOf(type)<0){
    app.agendaExportType.push(type);
  }
  else{
    app.agendaExportType.splice(app.agendaExportType.indexOf(type), 1);
  }
  app.agendaExportRenderTypes();
}
app.agendaExportRenderTypes=function(){
  var types=['glyphicon-blackboard','glyphicon-home',"glyphicon-comment"];
  for (var i = types.length - 1; i >= 0; i--) {
    if(app.agendaExportType.indexOf(types[i])<0){
      $('#agenda-export-'+types[i]).addClass('btn-default').removeClass('btn-primary');
    }else{
      $('#agenda-export-'+types[i]).removeClass('btn-default').addClass('btn-primary');
    }
  }
}