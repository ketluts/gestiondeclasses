/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.bilansClassroomTests=function(classe_id){
	document.getElementById('classeTestsExport').innerHTML="";
	app.currentClasse=app.getClasseById(classe_id);
	var html=app.classeTestsRender(classe_id);
	document.getElementById('bilans-temp').innerHTML=html;
	app.testsRenderNotes(classe_id);
	app.currentClasse=null;
	html=document.getElementById('bilans-temp').innerHTML;
	document.getElementById('bilans-temp').innerHTML="";
	return html;
}
app.bilansStudentTests=function(eleve_id){
	return app.studentTestsRender(eleve_id,true);
};