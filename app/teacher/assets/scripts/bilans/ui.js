/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.renderBilansClasses=function(){
	document.getElementById('bilans-classes').innerHTML="";
	var html=[];	
	for (var i =0,lng= app.classes.length; i <lng; i++) {
		var classe=app.classes[i];
		if(app.userConfig.classesDisable.indexOf(classe.classe_id)!=-1){continue;}
		html.push('<div class="panel panel-default" id="bilans_classe_'+classe.classe_id+'_block">\
			<div class="panel-heading bilansClasseItem">\
			<h4 class="panel-title">\
			<a data-toggle="collapse" href="#bilans_classe_'+classe.classe_id+'">\
			'+app.cleanClasseName(classe.classe_nom)+'\
			</a>');
		if(classe.classe_id>=0){
			html.push('<span class="btn btn-default btn-sm pull-right share_button" onclick="app.bilansAdd('+classe.classe_id+',\'classe\');">\
				<span class="glyphicon glyphicon-chevron-right"></span>\
				</span>');
		}
		html.push('</h4>\
			</div>\
			<div id="bilans_classe_'+classe.classe_id+'" class="panel-collapse collapse ">\
			<ul class="list-group" id="bilans_classe_'+classe.classe_id+'_body">');
		html.push('<li class="list-group-item bilansClasseItem">\
			<span class="bold">Tous les élèves</span>\
			<span class="btn btn-default btn-sm pull-right share_button" onclick="app.bilansAddAll('+classe.classe_id+');">\
			<span class="glyphicon glyphicon-chevron-right"></span>\
			</span>\
			</li>');

		for (var j =0,llng=app.classes[i].eleves.length ; j <llng; j++) {
			var eleve=app.getEleveById(app.classes[i].eleves[j]);
			if(!eleve.eleve_prenom){
				eleve.eleve_prenom="";
			}
			html.push('<li class="list-group-item bilansClasseItem">\
				'+app.renderEleveNom(eleve)+'\
				<span class="btn btn-default btn-sm pull-right share_button" onclick="app.bilansAdd('+eleve.eleve_id+',\'eleve\');">\
				<span class="glyphicon glyphicon-chevron-right"></span>\
				</span>\
				</li>');
		}
		html.push('</ul>\
			</div>\
			</div>');  
	}; 
	document.getElementById('bilans-classes').innerHTML+=html.join('');
	for (var i = app.classes.length - 1; i >= 0; i--) {
		var classe=app.classes[i];
		if(document.getElementById('bilans_classe_'+classe.classe_id+'_body') && document.getElementById('bilans_classe_'+classe.classe_id+'_body').innerHTML==""){
			document.getElementById('bilans_classe_'+classe.classe_id+'_block').style.display="none";
		}
	}
};
app.bilansAddAll=function(classe_id){
	var classe=app.getClasseById(classe_id);

	for (var i = classe.eleves.length - 1; i >= 0; i--) {
		app.bilansAdd(classe.eleves[i],'eleve');
	}
}
app.bilansAdd=function(id,type){
	for (var i = app.bilansStudentsList.length - 1; i >= 0; i--) {
		var elem=app.bilansStudentsList[i];
		if(elem.type==type && elem.id==id){
			return;
		}
	}
	app.bilansStudentsList.push({
		id:id,
		type:type
	});
	app.bilansStudentsListRender();
}
app.bilansDelete=function(num){
	app.bilansStudentsList.splice(num,1);
	app.bilansStudentsListRender();
}
app.bilansStudentsListRender=function(){	
	app.bilansStudentsList=app.orderBy(app.bilansStudentsList,'type','DESC');
	var students_count=0;
	var classrooms_count=0;
	var html_students=[];
	var html_classrooms=[];
	for (var i = app.bilansStudentsList.length - 1; i >= 0; i--) {
		var html=[];
		var elem=app.bilansStudentsList[i];
		html.push('<span class="bilans-person">');
		if(elem.type=="eleve"){
			var eleve=app.getEleveById(elem.id);
			
			students_count++;
			html.push('<span class="btn btn-default btn-xs" onclick="app.bilansDelete('+i+');">');
			html.push('<span class="glyphicon glyphicon-remove"></span>');
			html.push('</span>');	
			html.push(app.renderEleveNom(eleve));
			html.push('</span>');	
			html_students=html_students.concat(html);
		}else{
			var classe=app.getClasseById(elem.id);
			classrooms_count++;
			html.push('<span class="btn btn-default btn-xs" onclick="app.bilansDelete('+i+');">');
			html.push('<span class="glyphicon glyphicon-remove"></span>');
			html.push('</span>');				
			html.push(classe.classe_nom);
			html.push('</span>');	
			html_classrooms=html_classrooms.concat(html);
		}		
	}
	$('#bilans-students').html(html_students.join(''));
	$('#bilans-classrooms').html(html_classrooms.join(''));
	$('#bilans-classrooms-count').html(classrooms_count);
	$('#bilans-students-count').html(students_count);
}