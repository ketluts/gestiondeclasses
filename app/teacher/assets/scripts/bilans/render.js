/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.bilansCreate=function(){
	var css=document.getElementById('bilans-preview').innerHTML;
	var html="";
	if(document.getElementById('bilansFormsSkills').checked){
		app.itemsCategoriesBuild();
	}
	for (var i = app.bilansStudentsList.length - 1; i >= 0; i--) {
		var elem=app.bilansStudentsList[i];
		html+='<page class="bilans-preview-data">'; 
		if(elem.type=='classe'){			
			if(document.getElementById('bilansFormsNotes').checked){
				var notes=app.bilansClassroomTests(elem.id);
				if(notes!=""){
					html+='<div class="nobreak">';	
					html+=app.bilansHeaderClassroom(elem.id);
					html+=notes;
					html+='</div>';
				}	
			}
			if(document.getElementById('bilansFormsSkills').checked){					
				var skills=app.bilansItemsClassroomSkillsTab(elem.id);
				if(skills!=''){
					html+='<div class="nobreak">';
					html+=app.bilansHeaderClassroom(elem.id);
					html+=skills;
					html+='</div>';	
				}				
			}
			if(document.getElementById('bilansFormsEvents').checked){
				html+=app.bilansHeaderClassroom(elem.id);				
				if(organizer!=''){
					html+='<div class="nobreak">';
					var organizer=app.bilansClassroomOrganizer(elem.id);
					html+=organizer;
					html+='</div>';
				}				
			}
			if(document.getElementById('bilansFormsCodes').checked){			
				var codes=app.bilansClassroomCodes(elem.id);
				//alert(codes);
				if(codes!=""){
					html+='<div class="nobreak">';	
					html+=app.bilansHeaderClassroom(elem.id);
					html+=codes;
					html+='</div>';
				}	
			}
		}
		else{
			if(document.getElementById('bilansFormsCodes').checked){
				var codes=app.bilansStudentCode(elem.id);
				if(codes!=''){
					html+='<div class="nobreak">';
					html+=app.bilansHeaderStudent(elem.id);
					html+=codes;
					html+='</div>';
				}				
			}
			if(document.getElementById('bilansFormsNotes').checked){
				var notes=app.bilansStudentTests(elem.id);
				if(notes!=""){
					html+='<div class="nobreak">';	
					html+=app.bilansHeaderStudent(elem.id);
					html+=notes;
					html+='</div>';
				}	
			}
			if(document.getElementById('bilansFormsSkills').checked){
				app.itemsStudentsProgressionsBuild([elem.id]);
				var skills=app.bilansItemsStudentSkillsTab(elem.id);
				if(skills!=''){
					html+='<div class="nobreak">';
					html+=app.bilansHeaderStudent(elem.id);
					html+=skills;
					html+='</div>';	
				}				
			}
			if(document.getElementById('bilansFormsEvents').checked){
				var events=app.bilansStudentEvents(elem.id);
				if(events!=''){
					html+='<div class="nobreak">';
					html+=app.bilansHeaderStudent(elem.id);
					html+=events;
					html+='</div>';
				}				
			}
		}
		html+='</page>';
	}
	//document.getElementById('bilans-temp').innerHTML=html;
	//return;
	document.getElementById('export-pdf-data').value=css+html;
	document.getElementById('export-pdf-titre').value='bilans_'+moment().format('DD_MMM_YYYY');  
	document.forms["export-pdf-form"].action=app.serveur + "index.php?go=pdf&q=a4" + app.connexionParam;
	document.forms["export-pdf-form"].submit();
}
app.bilansHeaderClassroom=function(classe_id){
	var classe=app.getClasseById(classe_id);
	var html=[];
	html.push('<div class="bilans-header">');
	html.push('<div class="bilans-header-logo">');
	html.push('<img src="'+app.serveur+'/views/logo.png" width="40"/> GestionDeClasses');
	html.push('</div>');
	html.push('</div>');
	html.push('<div class="h2">'+classe.classe_nom+'</div>');
	
	return html.join('');
}
app.bilansHeaderStudent=function(eleve_id){
	var eleve=app.getEleveById(eleve_id);
	var html=[];
	html.push('<div class="bilans-header">');
	html.push('<div class="bilans-header-logo">');
	html.push('<img src="'+app.serveur+'/views/logo.png" width="40"/> GestionDeClasses');
	html.push('</div>');
	html.push('</div>');
	html.push('<div class="h2">'+app.renderEleveNom(eleve)+'</div>');
	return html.join('');
}