/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.bilansInit=function(){	
	if (!app.checkConnection()) {return;}
	app.viewClear();
	app.currentView="bilans";
	document.getElementById('template_bilans').style.display = "flex";
	document.getElementById('titre').innerHTML = 'Mes bilans';
	app.renderBilansClasses();
	app.bilansStudentsList=[];
	app.bilansStudentsListRender();
	app.agendaExportType=['glyphicon-blackboard','glyphicon-home'];
	app.agendaExportRenderTypes();
	if(app.itemsCurrentClasse){
		app.bilansAdd(app.itemsCurrentClasse,'classe');
		document.getElementById('bilansFormsSkills').checked="checked";
	}
	if(app.itemsCurrentEleve){
		app.bilansAdd(app.itemsCurrentEleve,'eleve');
		document.getElementById('bilansFormsSkills').checked="checked";
	}
};
