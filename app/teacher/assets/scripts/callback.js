/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.render=function(data) {
  var data = jsonParse(data); 
  /*---------------
-----------------
------APP-------
-----------------
---------------*/ 
if (data['info'] && data['info'] != "") {
  app.renderNoty(data['info']);
}
if(data['statut']==false){
 app.deconnexion();
 return;
}
if(data['sessionID']){
app.sessionID=data['sessionID'];
app.load();

}
if(data['alertes']){
 for (var i = data['alertes'].length - 1; i >= 0; i--) {
  app.alert({title:data['alertes'][i]});
}
}
if (data['titre']) {
  document.getElementById('titre').innerHTML = data['titre'];
}
 /*---------------
-----------------
------CONFIG------
-----------------
---------------*/
if(data['options']){
  app.upload_max_filesize=data['options']['upload_max_filesize'];
  app.quotaByUser=data['options']['quotaByUser'];
  app.weeks=data['options']['weeks'];
  app.serveurOptions=data['options'];
  document.getElementById('home-week').innerHTML =app.getWeekName();
  for (var i = app.plugins.length - 1; i >= 0; i--) {
    var plugin= app.plugins[i];
    if(app.serveurOptions.plugins && app.serveurOptions.plugins.indexOf(plugin.name)>=0){
      plugin.enabled=true;
    }
   } 
     app.pluginsLauncher('appLoaded');
}
if (data['legends']) {
  app.setLegends(data['legends']);
}
if (data['disciplines']) {

 app.disciplines = data['disciplines'];
  app.buildDisciplinesIndex();
}
if (data['periodes']) {
  app.periodes = data['periodes'];

  for (var i = app.periodes.length - 1; i >= 0; i--) {
    if(app.periodes[i].periode_active==1){
      app.active_periode=app.periodes[i];        
    }
  };
  app.testsformPeriodesRender();
  app.skillsFiltersPeriodesRender();
  app.homeRenderActivePeriode();
  app.homeRenderPeriodesView();
}

  /*---------------
-----------------
------USER------
-----------------
---------------*/
 if (data['userMemos']) { 
  app.userConfig.memos=data['userMemos'];
}
if (data['user']) {  
 if (data['user']['matiere']) {    
  app.userConfig.matiere=data['user']['matiere'];
}
  if (data['user']['user_config']) {
    app.configUserInit(data['user']['user_config']);
  }

  if (data['user']['user_id']) {
   app.userConfig.userID=data['user']['user_id'];
 }
 
if (data['user']['mail']) { 
 app.userConfig.mail=data['user']['mail'];
}
if (data['user']['nom']) { 
 app.userConfig.nom=data['user']['nom'];
}

if (data['user']['prenom']) { 
 app.userConfig.prenom=data['user']['prenom'];
}
if(data['user']['admin']){
  app.userConfig.admin=data['user']['admin'];
  $('.admin').css('display','');
}
else if(data['user']['admin']==false){
  app.userConfig.admin=data['user']['admin'];
  $('.admin').css('display','none');
}
app.userConfigRenderForm();
app.userInfosHeaderRender();
}
/*---------------
-----------------
------DATA------
-----------------
---------------*/

if (data['classes']) {
 app.loadClasses(data['classes']);   
}
if (data['eleves']) {
  app.eleves=data['eleves'];
  app.buildElevesIndex();
}
if (data['shares']) {
  app.renderShareListe(data['shares']);
}
if (data['filters']) {
 app.itemsFiltersBuildData(data['filters']);
 app.itemsFiltersLoadFilter(false,app.userConfig.itemsCurrentFilter);
}
if (data['defis']) {
  app.defis=data['defis'];
}
if(data['elevesByClasses']){ 
  app.elevesByClasses=data['elevesByClasses'];
  app.buildClassroomStudent();
}
if (data['userGroupes']) { 
  app.userConfig.groupes=data['userGroupes'];
  app.buildClassroomGroups();
}
if (data['userPlans']) { 
  app.userConfig.plans=data['userPlans'];
  app.buildClassroomPlans();
}
if (data['userSociogrammes']) {
 app.userConfig.sociogrammes=data['userSociogrammes'];
 app.buildClassroomSociogrammes();
}
if (data['agenda']) {
  app.agenda=data['agenda'];
  app.buildAgendaDatalist();
}
if (data['items']) {
  app.items=data['items'];
  app.buildItemsIndex();
  app.buildItemsDatalist();  
  app.renderItemsFilters();
}
if (data['relations']) {
  app.relations=data['relations'];
}
if (data['etoiles']) {
  app.etoiles=data['etoiles'];
}
if (data['progressions']) {
  app.itemsProgressions=data['progressions'];
   app.buildProgressionsIndex();
  app.buildProgressionsDatalist();
}
if (data['controles']) {
  app.controles=data['controles'];
  app.buildTestsIndex();
  if(app.currentView=="classe"){   
    app.classeTestsRender();
  }
}
if (data['notes']) {
  app.notes=data['notes'];
  app.notesByUserBuild();
}
if (data['files']) {
  app.quotaUsed=data['files']['quotaUsed'];
  app.files=data['files']['liste'];
}
if (data['links']) {
 app.links=data['links'];
}
if (data['messages']) {
  if(data['messages'].length>0){
   app.messages=app.messages.concat(data['messages']);
   app.messagerieInitData();
 }
}
//if (data['eleves_sans_classe']) {
// app.eleves_sans_classe=data['eleves_sans_classe'];
//  if(app.getClasseById(-2)){
//   app.getClasseById(-2).eleves=data['eleves_sans_classe'];
// }
//}
if (data['picture']) {
app.currentEleve.eleve_picture=data['picture'];
app.studentPictureSet();
}
if (data['feedbacks']) {
  app.feedbacks=data['feedbacks'];
  app.buildFeedbacksIndex();
}  
/*---------------
-----------------
------CONNEXION------
-----------------
---------------*/
if (data['users']) {
  app.users=data['users'];
  app.buildUsersIndex();
}        
if (data['etablissementsLST']) {
  app.connexionFormEtablissementAutocompletion(data['etablissementsLST']);
  if(data['etablissementsCRT']==false){
     app.etablissementsCRT=false;
    $('#connexion-menu-nouvelEtablissement').prop("disabled", true);
  }
}   

return data;   
};