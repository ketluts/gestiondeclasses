/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.itemsToolbarButtonsSet=function(item_id){
	if(app.itemsCurrentEleve!=null){
		if(app.itemsItemToolbarVisibleBtn[item_id] && app.myIndexOf(app.itemsItemToolbarVisibleBtn[item_id],app.itemsCurrentEleve)>=0){
			$('#item_view_'+item_id+'').removeClass('btn-default').addClass('btn-primary').attr('data-value',true);
		}
		if(app.itemsItemToolbarAutoEvalBtn[item_id] && app.myIndexOf(app.itemsItemToolbarAutoEvalBtn[item_id],app.itemsCurrentEleve)>=0){
			$('#item_view_'+item_id+'').removeClass('btn-default').addClass('btn-primary').attr('data-value',true);
			$('#item_view_'+item_id+'_edit').removeClass('btn-default').addClass('btn-primary').attr('data-value',true);
		}
		return;
	}
	if(app.itemsCurrentClasse){
		var classe=app.getClasseById(app.itemsCurrentClasse);
		var eleves=classe.eleves;
		var item=app.getItemById(item_id);
		if(item['item_user']==app.userConfig.userID){
			document.getElementById('item_view_'+item_id+'').style.display="block";
			document.getElementById('item_view_'+item_id+'_edit').style.display="block";
			if(app.itemsItemToolbarVisibleBtn[item['item_id']] && app.itemsItemToolbarVisibleBtn[item_id].length>0){ 
				//On gère le bouton view			
				$('#item_view_'+item_id+'').removeClass('btn-default').addClass('btn-primary');
				document.getElementById('item_view_'+item_id+'').innerHTML='<span class="glyphicon glyphicon glyphicon-eye-open"></span>  <span class="badge badge_items">'+app.itemsItemToolbarVisibleBtn[item_id].length+'/'+eleves.length+'</span>';
				if(app.itemsItemToolbarVisibleBtn[item['item_id']].length<eleves.length){
					$('#item_view_'+item_id+'').attr('data-value',false);
				}else{
					$('#item_view_'+item_id+'').attr('data-value',true);
				}
			}
			else{
				$('#item_view_'+item_id+'').attr('data-value',false);
				document.getElementById('item_view_'+item_id+'').innerHTML='<span class="glyphicon glyphicon glyphicon-eye-open"></span> ';
			}
			if(app.itemsItemToolbarAutoEvalBtn[item['item_id']] && app.itemsItemToolbarAutoEvalBtn[item['item_id']].length>0){ 
				//On gère le bouton edit			
				$('#item_view_'+item['item_id']+'_edit').removeClass('btn-default').addClass('btn-primary');
				document.getElementById('item_view_'+item_id+'_edit').innerHTML='<span class="glyphicon glyphicon glyphicon-pencil"></span>  <span class="badge badge_items">'+app.itemsItemToolbarAutoEvalBtn[item_id].length+'/'+eleves.length+'</span>';
				if(app.itemsItemToolbarAutoEvalBtn[item_id].length<eleves.length){
					$('#item_view_'+item_id+'_edit').attr('data-value',false);
				}else{
					$('#item_view_'+item_id+'_edit').attr('data-value',true);
				}
			}
			else{
				$('#item_view_'+item['item_id']+'_edit').attr('data-value',false);
				document.getElementById('item_view_'+item_id+'_edit').innerHTML='<span class="glyphicon glyphicon glyphicon-pencil"></span> ';
			}
		}		
	}
	
}
app.itemsProgressionSlideStyleRender=function(event,ui){
	var item_id=$(ui.handle).parent().attr('data-id');
	var style=app.itemsProgressionStyleGet(ui.value,item_id);
	$('#item_'+item_id+'_slider_style').css('width',style.width+"%").css('background-color',style.color);
	if(style.description!="<p><br></p>"){
		$('#item_'+item_id+'_current_description').css('display','block').html("<span class='h4'><small>"+style.description+"</small></span>");
	}
}
app.itemsProgressionStyleGet=function(value,item_id){
	var item=app.getItemById(item_id);
	var max=item['item_value_max'];
	var color="";
	var descriptions=JSON.parse(item['item_descriptions']); 
	var intValue=max;
	var description="";

	if(value>=0){
		if(value!=max){
			intValue=Math.floor(value*1/(max/(max*1+1)));
		}
		color=app.getColorAffine(intValue,item['item_colors']);	
		description=descriptions[intValue];				
	}
	return {
		"width":Math.floor(value*100/max),
		"color":color,
		"description":description
	};
}
app.itemsSelectSymbole=function(symbole){
		app.itemsCurrentSymbole=symbole;
	}