/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.buildProgressionsIndex=function(){	
	app.progressionsIndex=[];
	for (var i = 0, lng = app.itemsProgressions.length; i < lng; i++) {
		app.progressionsIndex[app.itemsProgressions[i].rei_id]=app.itemsProgressions[i];
	}
}
app.getProgressionById=function(progression_id){
	return app.progressionsIndex[progression_id];
}
app.itemCheckboxChange=function(item_id){
	if(!app.checkConnection()
		||(!app.itemsCurrentClasse && !app.itemsCurrentEleve)
		){
		return false;
}	
var eleves=[];
if(app.itemsCurrentEleve!=null){
	eleves=[app.itemsCurrentEleve];
}
else{
	var classe=app.getClasseById(app.itemsCurrentClasse);
	for (var e = classe.eleves.length - 1; e >= 0; e--) {
		var eleve_id=classe.eleves[e];
		if(document.getElementById('cpt_eleve_'+eleve_id+'_checkbox').checked){
			eleves.push(eleve_id);
		}
	}
	if(eleves.length==0){
		app.itemsRenderProgressionByClasse(app.itemsCurrentClasse);
		return;
	}
	if(app.cptElevesSelectedLock){
		app.alert({title:'L\'évaluation multiple est vérrouillée.',confirmButtonText:"Fermer"},function(){});
		app.itemsRenderProgressionByClasse(app.itemsCurrentClasse);
		return;
	}
}
var url="";
if(document.getElementById('item_'+item_id+'_checkbox_mode').checked){	
	url=app.serveur + "index.php?go=items&q=addProgression"+app.connexionParam;
}
else{
	url=app.serveur + "index.php?go=items&q=uncheck"+app.connexionParam;
}
var activite=document.getElementById('cpt-eval-activite').value;
if(activite){
	activite=ucfirst(activite);
}



for (var i = eleves.length - 1; i >= 0; i--) {
	var eleve_id=eleves[i];
var progression={
rei_id:-1,
rei_eleve:eleve_id,
rei_item:item_id,
rei_value:1,
rei_date:Math.floor(app.myTime()/1000),
rei_user:app.userConfig.userID,
rei_user_type:"user",
rei_activite:activite,
rei_comment:ucfirst(document.getElementById('cpt-eval-comment').value),
rei_symbole:app.itemsCurrentSymbole,
rei_periode:null
};

app.itemsProgressions.push(progression);

}

 app.buildProgressionsIndex();
  app.buildProgressionsDatalist();
	


(function(eleves){$.post(url,
{
	items:JSON.stringify([item_id]),
	item_id:item_id,
	eleves:JSON.stringify(eleves),
	progression:1,
	symbole:app.itemsCurrentSymbole,
	activite:activite,
	time:Math.floor(app.myTime()/1000),
	sessionParams:app.sessionParams
}, function(data) {
	app.render(data);	
	app.itemsProgressionUpdateCallback(item_id,eleves);
}
);})(eleves);



}

app.itemsProgressionAdd=function(event,ui){
	var eleves=[];
	var item_id=$(ui.handle).parent().attr('data-id');
	var max=$(ui.handle).parent().attr('data-max');
	if(app.itemsCurrentEleve!=null){ 
		eleves=[app.itemsCurrentEleve];		
	}else{
		var classe=app.getClasseById(app.itemsCurrentClasse);
		for (var e = classe.eleves.length - 1; e >= 0; e--) {
			var eleve_id=classe.eleves[e];
			if(document.getElementById('cpt_eleve_'+eleve_id+'_checkbox').checked){
				eleves.push(eleve_id);
			}
		}
		if(eleves.length==0){
			app.itemsRenderProgressionByClasse(app.itemsCurrentClasse);
			return;
		}
		if(app.cptElevesSelectedLock){
			app.alert({title:'L\'évaluation multiple est vérrouillée.',confirmButtonText:"Fermer"},function(){});
			app.itemsRenderProgressionByClasse(app.itemsCurrentClasse);
			return;
		}	
	}
	if(ui.value!=max){
		ui.value=Math.floor(ui.value*1/(max/(max*1+1)));
	}	

	$("#item_"+item_id+"_slider").slider( "disable" );
var date=moment(document.getElementById('cpt-eval-date').value, "DD/MM/YYYY").unix();

for (var i = eleves.length - 1; i >= 0; i--) {
	var eleve_id=eleves[i];
var progression={
rei_id:-1,
rei_eleve:eleve_id,
rei_item:item_id,
rei_value:ui.value,
rei_date:date||Math.floor(app.myTime()/1000),
rei_user:app.userConfig.userID,
rei_user_type:"user",
rei_activite:ucfirst(document.getElementById('cpt-eval-activite').value),
rei_comment:ucfirst(document.getElementById('cpt-eval-comment').value),
rei_symbole:app.itemsCurrentSymbole,
rei_periode:null
};

app.itemsProgressions.push(progression);

}

 app.buildProgressionsIndex();
  app.buildProgressionsDatalist();
	

	(function(eleves){
		$.post(app.serveur + "index.php?go=items&q=addProgression"+app.connexionParam,
		{
			items:JSON.stringify([item_id]),
			eleves:JSON.stringify(eleves),
			progression:ui.value,
			symbole:app.itemsCurrentSymbole,
			activite:ucfirst(document.getElementById('cpt-eval-activite').value),
			comment:ucfirst(document.getElementById('cpt-eval-comment').value),
			time:date||Math.floor(app.myTime()/1000),
			sessionParams:app.sessionParams
		}, function(data) {
			$("#item_"+item_id+"_slider").slider( "enable" );

			app.render(data);
			app.itemsProgressionUpdateCallback(item_id,eleves);						
		}
		);	})(eleves);
	}
	app.delProgression=function(progression_id,item_id){
		if (!app.checkConnection()) {return;}

		$('#progression_'+progression_id).css('display','none');

		(function(eleve_id){$.post(app.serveur + "index.php?go=items&q=delProgression"+app.connexionParam,
		{
			progression_id:progression_id,
			sessionParams:app.sessionParams
		}, function(data) {
			app.render(data);   
			app.itemsProgressionUpdateCallback(item_id,[app.itemsCurrentEleve]);
		}
		);})(app.itemsCurrentEleve);
	}
	app.progressionUpdate=function(progression_id,item_id){
		if (!app.checkConnection()) {
			$('.progression-update-btn').button('reset');
			return;}
			var activite=ucfirst(document.getElementById('progression_'+progression_id+'_edit_activite').value);
			var date=moment(document.getElementById('progression_'+progression_id+'_edit_date').value, "DD/MM/YYYY").unix();


			(function(eleve_id){$.post(app.serveur + "index.php?go=items&q=updateProgression"+app.connexionParam,
			{
				progression_id:progression_id,
				progression_activite:activite,
				progression_date:date||Math.floor(app.myTime()/1000),
				sessionParams:app.sessionParams

			}, function(data) {
				$('.progression-update-btn').button('reset');
				app.render(data);   
				app.itemsProgressionUpdateCallback(item_id,[eleve_id]);
			}
			);})(app.itemsCurrentEleve);
		}
		app.itemsProgressionUpdateCallback=function(item_id,eleves){
			if(eleves){
				app.itemsStudentsProgressionsBuild(eleves);
			}

			if(app.itemsCurrentEleve!=null && (eleves.indexOf(app.itemsCurrentEleve)!=-1 || !eleves)){
			app.itemsRenderProgressionByItemAndEleve(item_id);
			app.itemsSetFilters();			
			app.itemsStudentActivitiesRender();
		}else if(app.itemsCurrentClasse!=null){
			var classe=app.getClasseById(app.itemsCurrentClasse);				
			classe.progressions=app.itemsStudentsProgressionsBuild(classe.eleves);
			app.itemsRenderProgressionByItemAndClassroom(item_id);
			app.itemsSetFilters();
			app.itemsClassroomSkillsTab();
		}	
		if(document.getElementById('item_'+item_id+'_details_block').style.display=="block"){
			app.itemsRenderDetails(item_id,true);
		}
	}
	
	
	app.itemsStudentsProgressionsBuild=function(eleves_id_liste){
		var progressions=app.itemsProgressions;
		app.itemsItemToolbarVisibleBtn=[];
		app.itemsItemToolbarAutoEvalBtn=[];
		var classroomProgression=[];
		var progressionByEleveAndUser=[];
		var progressionClasseByUser={
			itemsList:[],
			itemsIds:[]
		};
		var itemList=[];
		for (var i = eleves_id_liste.length - 1; i >= 0; i--) {
			app.getEleveById(eleves_id_liste[i]).progression=[];
			progressionByEleveAndUser[eleves_id_liste[i]]={
				itemsList:[],
				itemsIds:[]
			};
		}
		for (var i = progressions.length - 1; i >= 0; i--) {
			var progression=progressions[i];
			//$('#progression_'+progression['rei_id']).css('display','none');
			if(progression['rei_user_type']=="user" && progression['rei_user']!=app.userConfig.userID && app.userConfig.itemsAvisMode=="me"){
				continue;
			}	
			if(app.myIndexOf(eleves_id_liste,progression['rei_eleve']) == -1){continue;}

			if((app.userConfig.itemsAvisMode!="students" && progression['rei_user_type']=="eleve") || (app.userConfig.itemsAvisMode=="students" && progression['rei_user_type']=="user")) {
				continue;
			}
			if(progression.rei_date<app.itemFiltersDateStart || progression.rei_date>app.itemFiltersDateEnd){
				continue;
			}
		//Si la valeur vaut -1, cela signifie que l'item est visible pour l'élève et on en tient pas compte dans le calcul
		//Si la valeur vaut -2, cela signifie que l'item est visible et peut s'auto évaluer et on en tient pas compte dans le calcul
		if(app.itemsGetProgressionValue(progression.rei_id)=='-1'){
			(app.itemsItemToolbarVisibleBtn[progression['rei_item']]=app.itemsItemToolbarVisibleBtn[progression['rei_item']]||[]).push(progression['rei_eleve']);
			continue;
		}
		if(app.itemsGetProgressionValue(progression.rei_id)=='-2'){
			(app.itemsItemToolbarAutoEvalBtn[progression['rei_item']]=app.itemsItemToolbarAutoEvalBtn[progression['rei_item']]||[]).push(progression['rei_eleve']);
			(app.itemsItemToolbarVisibleBtn[progression['rei_item']] = app.itemsItemToolbarVisibleBtn[progression['rei_item']]|| []).push(progression['rei_eleve']);
			continue;
		}		
		var eleve=app.getEleveById(progression['rei_eleve']);
		if(!eleve){continue;}
		//$('#progression_'+progression['rei_id']).css('display','block');		
		var item_id=progression['rei_item'];
		var user_id=progression['rei_user'];		
		if(itemList.indexOf(item_id)<0){
			itemList.push(item_id);
		}
		var item=app.getItemById(item_id);
		if(item.item_vue!="cursor" && app.itemsGetProgressionValue(progression.rei_id)>1){
			progression['rei_value']=1;
		}	
		//ELEVES PROGRESSIONS CONSTRUCTIONS
		var indexItemId=progressionByEleveAndUser[eleve.eleve_id].itemsIds.indexOf(item_id);
		if(indexItemId<0){
			progressionByEleveAndUser[eleve.eleve_id].itemsList.push({
				liste:[[]],
				userIds:[user_id],
				item_id:item_id
			});
			progressionByEleveAndUser[eleve.eleve_id].itemsIds.push(item_id);
			indexItemId=progressionByEleveAndUser[eleve.eleve_id].itemsIds.length-1;
		}		
		var indexUser=progressionByEleveAndUser[eleve.eleve_id].itemsList[indexItemId].userIds.indexOf(user_id);
		if(indexUser==-1){
			progressionByEleveAndUser[eleve.eleve_id].itemsList[indexItemId].userIds.push(user_id);
			indexUser=progressionByEleveAndUser[eleve.eleve_id].itemsList[indexItemId].userIds.length-1;
			progressionByEleveAndUser[eleve.eleve_id].itemsList[indexItemId].liste[indexUser]=[];
		}
		progressionByEleveAndUser[eleve.eleve_id].itemsList[indexItemId].liste[indexUser].push(progression);
		
		if(!eleve.progression[item_id]){
			eleve.progression[item_id]={
				"value":0,
				"nb":0
			};
		}
		eleve.progression[item_id].nb++;
		//CLASSE PROGRESSIONS CONSTRUCTIONS
		var indexItemId=progressionClasseByUser.itemsIds.indexOf(item_id);
		if(indexItemId<0){
			progressionClasseByUser.itemsList.push({
				liste:[[progression]],
				userIds:[user_id],
				item_id:item_id
			});
			progressionClasseByUser.itemsIds.push(item_id);
			indexItemId=progressionClasseByUser.itemsIds.length-1;
		}
		var indexUser=progressionClasseByUser.itemsList[indexItemId].userIds.indexOf(user_id);
		if(indexUser==-1){
			progressionClasseByUser.itemsList[indexItemId].userIds.push(user_id);
			indexUser=progressionClasseByUser.itemsList[indexItemId].userIds.length-1;
			progressionClasseByUser.itemsList[indexItemId].liste[indexUser]=[];
		}
		progressionClasseByUser.itemsList[indexItemId].liste[indexUser].push(progression);
		if(!classroomProgression[item_id]){
			classroomProgression[item_id]={
				"value":[],
				"nb":0
			};			
		}
	};
	for (var i = eleves_id_liste.length - 1; i >= 0; i--) {
		var eleve=app.getEleveById(eleves_id_liste[i]);
		//alert(app.cptElevesSelectedNum);
		
		var items=progressionByEleveAndUser[eleves_id_liste[i]].itemsList;
		for (var j = items.length - 1; j >= 0; j--) {
			var oitem=items[j];
			var item=app.getItemById(oitem.item_id);
			var progressions=[];
			for (var k = oitem.liste.length - 1; k >= 0; k--) {
				var n=oitem.liste[k].length;
				if(n){
					progressions.push(app.itemsProgressionsSynthese(oitem.liste[k],item.item_mode));
				}				
			}
			n=progressions.length;
			var value=0;
			if(n){
				value=progressions.reduce(app.add,0)/n;
			}
			eleve.progression[item.item_id].value=value;
//alert(app.cptElevesSelectedNum);
		if(document.getElementById('cpt_eleve_'+eleves_id_liste[i]+'_checkbox') && app.cptElevesSelectedNum>0){
		if(document.getElementById('cpt_eleve_'+eleves_id_liste[i]+'_checkbox').checked){
			classroomProgression[oitem.item_id].value.push(value);
		}
	}else{
		classroomProgression[oitem.item_id].value.push(value);
	}
			
		}
	}
	for (var i = itemList.length - 1; i >= 0; i--) { 
		var item_id=itemList[i];
		var n=classroomProgression[item_id].value.length;
		var value=-1;
		if(n){
			value=classroomProgression[item_id].value.reduce(app.add,0)/n;
		}
		
		classroomProgression[item_id].value=value;
		classroomProgression[item_id].nb=n;
	}
	return classroomProgression;
}
app.itemsProgressionsSynthese=function(progressions,mode){ 
	progressions=app.orderBy(progressions,'rei_date','DESC');
	if(progressions.length==0){
		return false;
	}
	if(mode=="auto"){
		var total=0;
		var coeff={
			"square":1,
			"star1":1,
			"star2":1,
			"star3":1
		};
		var somme=0;	
		var current_date=null;	
		for (var i =0,lng= progressions.length; i <lng; i++) {
			var progression=progressions[i];

			if(!current_date){
				current_date=progression.rei_date;
			}
			progression.rei_symbole=progression.rei_symbole||"square";
			// if(current_date-progression.rei_date>2592000){
			// 	coeff[progression.rei_symbole]=Math.max(0,coeff[progression.rei_symbole]-0.3);
			// 	current_date=progression.rei_date;
			// }
			var c=coeff[progression.rei_symbole];
			somme+=app.itemsGetProgressionValue(progression.rei_id)*c*app.itemsCyclesCoeffs[progression.rei_symbole];
			total+=c*app.itemsCyclesCoeffs[progression.rei_symbole];
			
		}
		return somme/total;
	}
	else{					
		return app.itemsGetProgressionValue(progressions[0].rei_id);
	}
}
app.itemsGetProgressionValue=function(progression_id){
	var progression=app.getProgressionById(progression_id);
	//return progression.rei_value;
	if(!progression.rei_symbole || progression.rei_symbole=="square"){
		return progression.rei_value;
	}
	if(progression.rei_value<0){
		return progression.rei_value;
	}
	var eleve=app.getEleveById(progression.rei_eleve);
	if(!eleve.eleve_cycle || eleve.eleve_cycle=="false"){
		return progression.rei_value;
	}
// var item=app.getItemById(progression.rei_item);
// if(item.item_cycle<0){
// 	return progression.rei_value;
// }
var level=eleve.eleve_cycle[1];
// if(item.item_cycle>eleve.eleve_cycle[0]){
// return progression.rei_value;	
// }
// if(item.item_cycle<eleve.eleve_cycle[0]){
// level="F";	
// }
return progression.rei_value*app.itemsCyclesCoeffsMalus[progression.rei_symbole][level];
}