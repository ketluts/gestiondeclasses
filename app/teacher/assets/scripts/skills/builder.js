/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.buildItemsIndex=function(){	
	app.itemsIndex=[];
	for (var i = 0, lng = app.items.length; i < lng; i++) {
		app.itemsIndex[app.items[i].item_id]=app.items[i];
	}
}
app.getItemById=function(item_id){
	return app.itemsIndex[item_id];
}
app.buildItemsDatalist=function(){
	var tags=[];
	var categories=[];
	var sous_categories=[];
	app.itemsSetOrder();
	var items=app.items;
	for (var i = app.items.length - 1; i >= 0; i--) {
		if(!app.items[i]['item_tags'] && !app.items[i]['item_categorie'] && !app.items[i]['item_sous_categorie']){continue;}
		if(app.items[i].item_user!=app.userConfig.userID && !app.userConfig.ppView){
			continue;
		}
		app.items[i]['item_tags']=app.items[i]['item_tags']||"";
		var categorie=app.items[i]['item_categorie']||"";
		var sous_categorie=app.items[i]['item_sous_categorie']||"";
		tags=tags.concat(app.items[i]['item_tags'].split(',')).concat(categorie).concat(sous_categorie);
		categories=categories.concat(app.items[i]['item_categorie']);
		sous_categories=sous_categories.concat(app.items[i]['item_sous_categorie']);
	};
	app.itemsTagsDatalist=arrayUnique(tags.map(function(value){if(value){return value.trim();}})).sort();
	//On nettoie les filtres selectionnés par l'utilisateur et qui n'existent plus
	var new_filters=[];
	var new_exclude_filters=[];
	for (var i = app.itemsCurrentFilter.tags.include.length - 1; i >= 0; i--) {
		if(app.itemsTagsDatalist.indexOf(app.itemsCurrentFilter.tags.include[i])>=0){
			new_filters.push(app.itemsCurrentFilter.tags.include[i]);
		};	
	};
	for (var i = app.itemsCurrentFilter.tags.exclude.length - 1; i >= 0; i--) {
		if(app.itemsTagsDatalist.indexOf(app.itemsCurrentFilter.tags.exclude[i])>=0){
			new_exclude_filters.push(app.itemsCurrentFilter.tags.exclude[i]);
		};	
	};
	app.itemsCurrentFilter.tags.include=new_filters;
	app.itemsCurrentFilter.tags.exclude=new_exclude_filters;
	//On prépare l'index des tags en incluant des configurations particulières
	app.itemsTagsIndex=[];
	for (var i =0,lng= app.itemsTagsDatalist.length ; i <lng; i++) {
		var tag={
			value:app.itemsTagsDatalist[i],
			order:Infinity,
			color:null,
			img:null
		};
		for (var j = app.itemsTagsConfig.length - 1; j >= 0; j--) {
			var config=app.itemsTagsConfig[j];
			if(config.value==tag.value){
				tag.order=config.order,
				tag.color=config.color,
				tag.img=config.img
			}
		}
		app.itemsTagsIndex.push(tag);
	}
	var list=[];
	var families=[];
	app.pluginsLauncher('appTagsFamiliesInit');
	for (var i = app.userConfig.itemsTagsFamilies.length - 1; i >= 0; i--) {
		var family=app.userConfig.itemsTagsFamilies[i];
		var newFamily={};
		if(family.position==undefined){
			family.position=Infinity;
		}		
		newFamily.position=family.position;		
		newFamily.num=i;
		newFamily.list=[];		
		newFamily.removable=family.removable;
		for (var j = family.list.length - 1; j >= 0; j--) {
			var value=family.list[j];
			for (var k = app.itemsTagsIndex.length - 1; k >= 0; k--) {
				var tag=app.itemsTagsIndex[k];
				if(tag.value==value){
					newFamily.list.push(tag);
				}
			}
		}
		newFamily.list.sort(function(a,b){
			if(a.order!=Infinity || b.order!=Infinity){
				return a.order-b.order;
			}
			return (a.value).localeCompare(b.value);
		})
		families.push(newFamily);
		list=list.concat(family.list);
	}	
	var alone=[];
	for (var i =0,lng= app.itemsTagsDatalist.length ; i <lng; i++) {
		var value=app.itemsTagsDatalist[i];
		if(value=="" || !value){
			continue;
		};
		if(list.indexOf(value)>=0){
			continue;
		}
		var tag={
			value:value,
			order:Infinity,
			color:null,
			img:null
		};
		for (var j = app.itemsTagsConfig.length - 1; j >= 0; j--) {
			var config=app.itemsTagsConfig[j];
			if(config.value==tag.value){ 
				tag.order=config.order,
				tag.color=config.color,
				tag.img=config.img
			}
		}

		alone.push(tag);
	}
	families.push({
		list:alone,
		position:Infinity,
		removable:true
	});
	families=app.orderBy(families,'position','ASC');
	app.itemsTagsFamilies=families;
	app.itemsCategoriesDatalist=arrayUnique(categories.map(function(value){if(value){return value.trim();}})).sort();
	app.itemsSousCategoriesDatalist=arrayUnique(sous_categories.map(function(value){if(value){return value.trim();}})).sort();
	//On génére le Datalist pour les catégories. Celui pour les Tags est générer lors de la création d'un item
	var options='';
	for(var i = 0,lng=app.itemsCategoriesDatalist.length; i < lng; i++){
		options += '<option value="'+app.itemsCategoriesDatalist[i]+'" />';
	}
	document.getElementById('list_item_categories').innerHTML = options;
	//On génére le Datalist pour les sous_catégories.
	var options='';
	for(var i = 0,lng=app.itemsSousCategoriesDatalist.length; i < lng; i++){
		options += '<option value="'+app.itemsSousCategoriesDatalist[i]+'" />';
	}
	document.getElementById('list_item_sous_categories').innerHTML = options;
};
