/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/
app.buildFeedbacksIndex=function(){ 
  app.feedbacksIndex=[];
  for (var i = 0, lng = app.feedbacks.length; i < lng; i++) {
    var feedback=app.feedbacks[i];
    var data=JSON.parse(feedback.feedback_data);
    feedback.nbItems=data.itemsIds.length;
    feedback.nbStudents=data.studentsIds.length;
    app.feedbacksIndex[feedback.feedback_id]=feedback;
  }
}
app.getFeedbackById=function(feedback_id){
  return app.feedbacksIndex[feedback_id];
}
app.itemsFeedbacksDrop=function(ev,feedback_num){
  ev.preventDefault();
  console.log('File(s) dropped !'); 

  if (ev.dataTransfer.items) {
    // Use DataTransferItemList interface to access the file(s)
    for (var i = 0; i < ev.dataTransfer.items.length; i++) {
      // If dropped items aren't files, reject them
      if (ev.dataTransfer.items[i].kind === 'file') {
        var file = ev.dataTransfer.items[i].getAsFile();
        console.log('... file[' + i + '].name = ' + file.name);
        app.itemsFeedbacksLoadFile(file,feedback_num);
      }
    }
  } else {
    // Use DataTransfer interface to access the file(s)
    for (var i = 0; i < ev.dataTransfer.files.length; i++) {
      console.log('... file[' + i + '].name = ' + ev.dataTransfer.files[i].name);
      app.itemsFeedbacksLoadFile(ev.dataTransfer.files[i],feedback_num);
    }
  }


}

app.itemsFeedbacksDragOverHandler=function(ev){
 console.log('File(s) in drop zone'); 
  // Prevent default behavior (Prevent file from being opened)
  ev.preventDefault();

}

app.itemsFeedbacksLoadFile=function(file,feedback_num){

  var xhr= new XMLHttpRequest();
  xhr.open('POST', app.serveur + "index.php?go=items&q=addFeedbackFile&visibility=0");

  xhr.upload.addEventListener('progress', function(e) {
    var valeur= Math.floor(e.loaded*100/e.total);
    console.log(valeur+'%');
$('#item-feedbacks-'+feedback_num+'-loading').html('<img src="assets/img/ajax-loader.gif" width="50" title="Chargement en cours..."/>');
  }, false);
  xhr.addEventListener('load', function() {     
$('#item-feedbacks-'+feedback_num+'-loading').html("");

  }, false);

  xhr.onreadystatechange=function() {
    if (xhr.readyState == 4 && (xhr.status == 200)) {
      app.render(xhr.responseText);
      app.renderEditItem(app.itemsToEdit['item_id']);
    }
  };


  var form = new FormData();
  form.append('file', file);
  form.append('time',app.myTime());
  form.append('sessionParams',app.sessionParams);   
  form.append('item_id',app.itemsToEdit['item_id']); 
  form.append('feedback_num',feedback_num);   

  xhr.send(form);
  app.filesToUpload.push(xhr);


}

app.itemsFeedbacksFileDelete=function(file_id){


  if (!app.checkConnection()) {return;}

  $.post(app.serveur + "index.php?go=items&q=deleteFeedbackFile",{
    file_id:file_id,
    sessionParams:app.sessionParams
  }, function(data) {
    app.render(data);   
    app.renderEditItem(app.itemsToEdit['item_id']);
  }
  );

}

app.feedbacksInit=function(){
  var elem=document.getElementById('feedbacks');
  if(elem.style.display!="none"){
    elem.style.display="none";
    return false;
  }else{
    elem.style.display="";  
    app.feedbacksListe();

    return true;
  }
}


app.feedbacksGenerate=function(){
 
if(app.itemsCurrentEleve!==null){
    var eleves=[app.itemsCurrentEleve]; 
}
else{
 var classe=app.getClasseById(app.itemsCurrentClasse);
  var eleves=classe.eleves;
}


  var items=[];
  var activitiesByEleves=[];
  var itemsIds=[];
  var studentsIds=[];
  //On liste les items concernés
  for (var i =0, lng=app.items.length;i<lng; i++) {
    var item=app.items[i];  
    if(!app.itemsIsVisible(item)){
      continue;
    }
    items.push(item['item_id']);
  }
  for (var j =0 ; j<eleves.length; j++) {
    var eleve=app.getEleveById(eleves[j]);
    if(!eleve){continue;}
    if(app.cptElevesSelectedNum>0 && !document.getElementById('cpt_eleve_'+eleve.eleve_id+'_checkbox').checked){
      continue;
    }


    for (var i = 0; i<items.length; i++) {
      var item_id=items[i];
      var item=app.getItemById(item_id);
      feedback_num=-1;
      if(eleve.progression && eleve.progression[item_id]){
        feedback_num=Math.round(eleve.progression[item_id].value);
      }
      var activity_file=app.activityGet(eleve.eleve_id,item_id,feedback_num);
      if(activity_file!=-1){
        itemsIds.push(item_id);
        studentsIds.push(eleve.eleve_id);
        activitiesByEleves.push({
          eleve_id:eleve.eleve_id,
          eleve_progression:eleve.progression[item_id].value,
          item_id:item_id,
          activity_file:activity_file
        });     
      }
    } 
  }

  
  var feedback={
    feedback_code:strAlea(5),
    feedback_data:{
      liste:activitiesByEleves,
      itemsIds:arrayUnique(itemsIds),
      studentsIds:arrayUnique(studentsIds)
    },
  };
  app.currentFeedbacks=feedback;
  app.feedbacksRender();
  app.show('feedback-save');
}
app.activityGet=function(eleve_id,item_id,feedback_num){
  var item=app.getItemById(item_id);
  var itemFeedbacks=[];
  var feedbacks=app.feedbacks;

  if(item['item_feedbacks']){
    itemFeedbacks=JSON.parse(item['item_feedbacks']);

  }
  var activityNum=0;


  var activity=-1;
  if(itemFeedbacks[feedback_num]){
    var feedbacksCodes=[];

    for (let i = feedbacks.length - 1; i >= 0; i--) {
      let feedback=feedbacks[i];
      let data=JSON.parse(feedback.feedback_data);
      if(data.studentsIds.indexOf(eleve_id)<0){
        continue;
      }
      if(data.itemsIds.indexOf(item_id)<0){
        continue;
      }
      feedbacksCodes.push(feedback.feedback_code);
    }
    var progressions=app.itemsProgressions;

    for (var i = progressions.length - 1; i >= 0; i--) {
      let progression=progressions[i];
      if(progression['rei_eleve']!=eleve_id){continue;}
      if(progression['rei_item']!=item_id){continue;}

      if(feedbacksCodes.indexOf(progression['rei_activite'])>=0){
        activityNum++;
      }

    }

    var activities=itemFeedbacks[feedback_num];
    if(activityNum>activities.length-1){
      activity=-2;
    }else{
      activity=activities[activityNum];  
    }
    
  }
  return activity;
}

app.feedbacksRender=function(feedback_id,toPrint){
  app.hide('feedback-save');
  var data=[];
  if(!feedback_id){
    var feedback=app.currentFeedbacks;
    data=feedback.feedback_data;
  }else{
    var feedback=app.getFeedbackById(feedback_id);
    data=JSON.parse(feedback.feedback_data);
  }  

  var eleves=[]; 
  var html=[];

  var feedbacksByEleve=[];
  for (var i = data.liste.length - 1; i >= 0; i--) {
    eleves.push(data.liste[i].eleve_id);
    feedbacksByEleve[data.liste[i].eleve_id]=feedbacksByEleve[data.liste[i].eleve_id]||[];
    feedbacksByEleve[data.liste[i].eleve_id].push(data.liste[i]);
  }

  eleves=arrayUnique(eleves);
  html.push('<hr/>');

  html.push('<div class="h3">Feedback - '+ucfirst(feedback.feedback_code)+'</div>');
html.push('<div class="h4">'+data.studentsIds.length+' élève'+app.pluralize(data.studentsIds.length,'s')+" / "+data.itemsIds.length+' item'+app.pluralize(data.itemsIds.length,'s')+'</div>');




  for (var i = eleves.length - 1; i >= 0; i--) {
    var eleve=app.getEleveById(eleves[i]);
    if(!eleve){continue;}
    if(feedbacksByEleve[eleve.eleve_id]){
      html.push('<div class="feedback-student" >'+app.renderEleveNom(eleve)+"</div>");
      for (var j = feedbacksByEleve[eleve.eleve_id].length - 1; j >= 0; j--) {
        var activity=feedbacksByEleve[eleve.eleve_id][j];
        
        var item=app.getItemById(activity.item_id);
        if(!item){continue;}
        var style=app.itemsProgressionStyleGet(activity.eleve_progression,item['item_id']);
        html.push('<div class="feedback-wrapper">');
        html.push('<div class="feedback-item">');
        html.push('<span class="progression-square" style="background-color:'+style.color+';"></span>');
        html.push(item.item_name);
        html.push('</div>');
        if(activity.activity_file==-2){
          html.push("Liste d'exercices épuisée.");
        }else{
          html.push('<u>Pour progresser</u> : ');
          html.push(app.activitySelector(activity));
        }
        html.push('</div>');
      }
      html.push('<hr/>');
    }
  }
  if(!toPrint){
   $('#feedbacks-preview').html(html.join('')); 
 }else{
  return html.join();
 }
  
}


app.activitySelector=function(activity){
  var html=[];
  var file=app.getFileById(activity.activity_file)
  if(['jpg','jpeg','png','gif'].indexOf(file.file_type)>=0){
    html.push('<br/>');
    html.push('<img class="feedback-img" src="'+app.serveur + "index.php?go=share&q=getImage&file_id="+file.file_id+"&sessionParams="+encodeURIComponent(app.sessionParams)+'"/>');
  }else{
    html.push("<a href='"+app.serveur + "index.php?go=share&q=getFile&file_id="+file.file_id+"&sessionParams="+encodeURIComponent(app.sessionParams)+"'>"+file.file_fullname+'</a>');
  }
  return html.join('');
}

app.feedbacksSave=function(){
  if (!app.checkConnection()) {
    return;
  }
  if(!app.currentFeedbacks){
    return;
  }
  var feedback={
    feedback_description:document.getElementById('feedback-description').value||"",
    feedback_data:app.currentFeedbacks.feedback_data,
    feedback_code:app.currentFeedbacks.feedback_code
  };


  $.post(app.serveur + "index.php?go=feedbacks&q=add",{
    feedback:JSON.stringify(feedback),
    sessionParams:app.sessionParams,
    time:Math.floor(app.myTime()/1000)

  }, function(data) {

    app.render(data); 
    app.feedbacksListe();

  }
  );
}


app.feedbacksDelete=function(feedback_id,confirm){
  if (!app.checkConnection()) {
    return;
  }
if(!confirm){
  var feedback=app.getFeedbackById(feedback_id);
      app.alert({title:'Supprimer le feedback '+feedback.feedback_code+' ?',type:'confirm'},function(){app.feedbacksDelete(feedback_id,true);});
      return;
    }


  $.post(app.serveur + "index.php?go=feedbacks&q=delete",{
    feedback_id:feedback_id,
    sessionParams:app.sessionParams

  }, function(data) {
    app.render(data); 
    app.feedbacksListe();

  }
  );
}

app.feedbacksListe=function(){
  var feedbacks=app.feedbacks;
  var html=[];
  var source   = document.getElementById("template-feedbacks").innerHTML;
  var template = Handlebars.compile(source);
  var context = {
    feedbacks:feedbacks
  };      
  html.push(template(context)); 

if(feedbacks.length==0){
  html.push('Pour utiliser ce générateur, les sections <strong>Feedbacks</strong> des items doivent être renseignées.');
}
  $('#feedbacks-liste').html(html.join(''));
  app.currentFeedbacks=null;
  $('#feedbacks-preview').html("");
    app.hide('feedback-save');
}


app.feedbacksEvaluate=function(feedback_id){
  var feedback=app.getFeedbackById(feedback_id);
  if(!feedback){return;}
  var data=JSON.parse(feedback.feedback_data);
  var items=data.itemsIds;
  for (var i = items.length - 1; i >= 0; i--) {
    var item=app.getItemById(items[i]);
    if(item){
     document.getElementById('item_'+items[i]+'_checkbox').checked=true; 
   }  
 }
 app.itemsSelectedUpdate();
 app.itemsFilteredBySelectionStatue=false;
 app.itemsFilteredBySelectionToggleStatue();

 var eleves=data.studentsIds;
 for (var i = eleves.length - 1; i >= 0; i--) {
  var eleve=app.getEleveById(eleves[i]);
  if(eleve){
    document.getElementById('cpt_eleve_'+eleves[i]+'_checkbox').checked=true; 
  }
}

app.cptElevesSelectedUPD();
$('#cpt-eval-activite').prop('value',feedback.feedback_code);
app.feedbacksInit();
}

app.feedbacksPrint=function(feedback_id){
  if (!app.checkConnection()) {
    return;
  }



 // $.post(app.serveur + "index.php?go=feedbacks&q=print",{
 //    feedback_id:feedback_id,
 //    sessionParams:app.sessionParams

 //  }, function(data) {
 //    app.render(data); 

 //  }
 //  );








document.forms["export-pdf-form"].action=app.serveur + "index.php?go=feedbacks&q=print&feedback_id="+feedback_id + app.connexionParam;
   document.forms["export-pdf-form"].submit();

}