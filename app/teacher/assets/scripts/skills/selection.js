/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

//##############
//ITEMS SELECTION
//##############
var app=app || {};
app.itemsSelectedUpdate=function(){
	var items=app.items;
	app.itemsSelection=[];
	app.itemsShareNb=0;
	app.itemsViewNb=0;
	app.itemsEditNb=0;
	for (var i = items.length - 1; i >= 0; i--) {
		var item=items[i];
		if(document.getElementById('item_'+item['item_id']+'_checkbox').checked){
			app.itemsSelection.push(item['item_id']);
			if(item['item_public']=="true"){
				app.itemsShareNb++;
			}
			if($('#item_view_'+item['item_id']+'').attr('data-value')=='true'){
				app.itemsViewNb++;
			}
			if($('#item_view_'+item['item_id']+'_edit').attr('data-value')=='true'){
				app.itemsEditNb++;
			}
		}
	}
	var n=app.itemsSelection.length;
	document.getElementById('items-selected-nb').innerHTML=n+" item"+app.pluralize(n,'s');

	var m=app.itemsSelection.length/2;
	if(app.itemsShareNb>m){
		$('#items-selection-share').removeClass('btn-default').addClass('btn-primary');
	}
	else{
		$('#items-selection-share').removeClass('btn-primary').addClass('btn-default');
	}
	if(app.itemsViewNb>m){
		$('#items-selection-view').removeClass('btn-default').addClass('btn-primary');
	}
	else{
		$('#items-selection-view').removeClass('btn-primary').addClass('btn-default');
	}
	if(app.itemsEditNb>m){
		$('#items-selection-edit').removeClass('btn-default').addClass('btn-primary');
	}
	else{
		$('#items-selection-edit').removeClass('btn-primary').addClass('btn-default');
	}
	if(app.itemsSelection.length>0){
		document.getElementById('items-selection-btn').style.display="block";
		$('#items-footer').css('display','block');
	}
	else{
		document.getElementById('items-selection-btn').style.display="none";
		app.skillsFooterClose();
	}
}
app.itemsUnselectAll=function(){
	var items=app.items;
	for (var i = items.length - 1; i >= 0; i--) {
		var item=items[i];
		document.getElementById('item_'+item['item_id']+'_checkbox').checked=false;
	}
	document.getElementById('items-selection-btn').style.display="none";
	app.skillsFooterClose();
	app.itemsSelection=[];
	app.itemsCurrentFilter.selection=[];
	app.itemsFiltersSave(true);
	app.itemsSetFilters();
};
app.getCategorieById=function(categorie_id){
	for (var i = app.itemsCategories.length - 1; i >= 0; i--) {
		if(app.itemsCategories[i]['categorie_id']==categorie_id){
			return app.itemsCategories[i];
		}
	}
};
app.getSousCategorieById=function(categorie,sous_categorie_id){
	for (var i = categorie.sous_categories.length - 1; i >= 0; i--) {
		if(categorie.sous_categories[i]['sous_categorie_id']==sous_categorie_id){
			return categorie.sous_categories[i];
		}
	}
};
app.itemsAutoSelect=function(categorie_id,sous_categorie_id,mode){
	var categorie=app.getCategorieById(categorie_id);
	var sous_categorie=app.getSousCategorieById(categorie,sous_categorie_id);
	var items=app.items;

	for (var i = items.length - 1; i >= 0; i--) {
		var item=items[i];
		if(!item.isVisible){
			continue;
		}	
		var item_categorie=	item['item_categorie'];
			if(item['item_cycle']!=-1){
				item_categorie="Cycle "+item['item_cycle']+' - '+item['item_categorie'];
			}
		if(app.itemsSelection.indexOf(item['item_id'])>=0 || (mode=='categorie' &&  item_categorie==categorie.categorie_name) || (mode=='sous_categorie' &&  item['item_sous_categorie']==sous_categorie.sous_categorie_name && item['item_categorie']==categorie.categorie_name)){
		
			document.getElementById('item_'+item['item_id']+'_checkbox').checked=true;
		}
	}
	app.itemsSelectedUpdate();	
}
app.itemsSelectionSet=function(){	
	for (var i = app.itemsSelection.length - 1; i >= 0; i--) {
		var item_id=app.itemsSelection[i];
		if(document.getElementById('item_'+item_id+'_checkbox')){
			document.getElementById('item_'+item_id+'_checkbox').checked=true;
		}
	}
	app.itemsSelectedUpdate();	
}