/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.itemsPreviousView=function(){
	if(app.itemsCurrentEleve!=null){
		if(app.itemsCurrentClasse!=null){
			app.go('skills/classroom/'+app.itemsCurrentClasse);
			return;
		}
	}
	app.go('skills');
}
app.itemsStudentPrev=function(){
	if(app.itemsCurrentEleve!=null){
		if(app.itemsCurrentClasse==null){return;}
		var classe=app.getClasseById(app.itemsCurrentClasse);
		var eleves=classe.eleves;
		var num=eleves.indexOf(""+app.itemsCurrentEleve+"");
		if(num==0){
			return;     
		}else{
			var prev=num-1;
		}    
		app.go('skills/student/'+eleves[prev]);
		return;
	}else{
		if(app.itemsCurrentClasse!=null){
			var num=app.getClasseById(app.itemsCurrentClasse).num;
			if(num==0){				
				return;
			}else{
				var previous_classroom_id=app.classes[num-1].classe_id;

			}
			app.itemsToggleClasse(previous_classroom_id);
		}
	}
}
app.itemsStudentNext=function(){
	if(app.itemsCurrentEleve){

		if(app.itemsCurrentClasse==null){return;}
		var classe=app.getClasseById(app.itemsCurrentClasse);
		var eleves=classe.eleves;
		var num=eleves.indexOf(""+app.itemsCurrentEleve+"");
		if(num==eleves.length-1){
			return;
		}  
		var next=num*1+1;
		app.go('skills/student/'+eleves[next]);
		return;
	}
	else{
		if(app.itemsCurrentClasse!=null){
			var num=app.getClasseById(app.itemsCurrentClasse).num;
			if(num>=app.classes.length){
				return;
			}else{
				var next_classroom_id=app.classes[num+1].classe_id;

			}
			app.itemsToggleClasse(next_classroom_id);
		}
	}
}