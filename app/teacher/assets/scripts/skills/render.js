/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
//##############
//ITEMS RENDER
//##############
app.itemsCategoriesBuild=function(){
	var items=app.items;
	app.itemsCategories=[];
	var categories_name=[];
	for (var i =0, lng= items.length; i<lng; i++) {
		var item=items[i];
		item['item_num']=i;		
		item['is_visible']=true;
		item['item_categorie']=ucfirst(item['item_categorie'])||"Non classés";
		item['item_sous_categorie']=ucfirst(item['item_sous_categorie'])||"Non classés";
		item['item_tags_render']=app.itemsTagsRender(item['item_tags']);
		if(item['item_user']==app.userConfig.userID || app.userConfig.admin){
			item.item_admin=true;
		}
		item.item_public_style="btn-default";
		if(item['item_public']=="true"){
			item.item_public_style="btn-primary";
		}
		var categorie=item['item_categorie'];

		if(item['item_cycle']>0){
			categorie="Cycle "+item.item_cycle+" - "+item['item_categorie'];
		}

		if(item['item_vue']=="cursor"){
			var html=[];
			var max=item["item_value_max"];
			for (var k = max; k>=0; k--) {
				var color=app.getColorAffine(k,item['item_colors']);
				var width=(k*1+1)*100/(max*1+1);
				html.push('<div class="slider-style item-background" style="background-color:'+color+';width:'+width+'%;"></div>');
			}
			item['item_cursor_style']=html.join('');
			item.cursor=true;
		}else{
			item.box=true;
		}
		var index=categories_name.indexOf(categorie);
		if(index<0){
			//Si la catégorie n'existe pas
			var visibility="";
			if(item['item_sous_categorie']=='Non classés'){
				visibility="hide";
			}
			var new_categorie={
				name:categorie,
				categorie_name:categorie,
				categorie_id:categories_name.length,
				sous_categories_liste:[item['item_sous_categorie']],
				sous_categories:[{
					sous_categorie_name:item['item_sous_categorie'],
					sous_categorie_id:0,
					list:[item],
					visibility:visibility
				}]
			};
			item['item_categorie_id']=categories_name.length;
			app.itemsCategories.push(new_categorie);
			categories_name.push(categorie);
			item['item_sous_categorie_id']=0;
		}
		else{
			item['item_categorie_id']=index;
			//Si la catégorie existe		
			var index_sous_categorie=app.itemsCategories[index]['sous_categories_liste'].indexOf(item['item_sous_categorie']);
			if(index_sous_categorie<0){
				//Si la sous catégorie n'exite pas 
				var new_sous_categorie={
					sous_categorie_name:item['item_sous_categorie'],
					list:[item],
					sous_categorie_id:app.itemsCategories[index]['sous_categories_liste'].length
				};					
				item['item_sous_categorie_id']=app.itemsCategories[index]['sous_categories_liste'].length;		
				app.itemsCategories[index]['sous_categories'].push(new_sous_categorie);

				//app.itemsCategories[index]['sous_categories']=app.orderBy(app.itemsCategories[index]['sous_categories'],'sous_categorie_name','ASC');
				app.itemsCategories[index]['sous_categories_liste'].push(item['item_sous_categorie']);
				
			}else{
				//Si la sous catégorie exite
				item['item_sous_categorie_id']=index_sous_categorie;
				app.itemsCategories[index]['sous_categories'][index_sous_categorie]['list'].push(item);
				item['item_sous_categorie_num']=index_sous_categorie;
			}		
		}	
	};
	app.itemsCategories=app.orderBy(app.itemsCategories,'categorie_name','ASC');
	for (var i = app.itemsCategories.length - 1; i >= 0; i--) {
		app.itemsCategories[i]['sous_categories']=app.orderBy(app.itemsCategories[i]['sous_categories'],'sous_categorie_name','ASC');
	}
}
app.renderItems=function(){
	//Affichage des boutons
	//##############
	var html=[];
	document.getElementById('itemsNothingToShow').style.display='none';
	$(".itemsEleve").removeClass('itemsEleveSelected');		
	$(".itemsClasse").removeClass('itemsClasseSelected');
	document.getElementById('titre').innerHTML = "Liste des items";

	if(app.itemsImport){
		document.getElementById('items-liste').innerHTML=app.renderLoader();
		return;
	}
	//##############
	//Trie des items par catégories et sous-catégories
	//##############
	app.itemsCategoriesBuild();
	//##############
	//On affiche par categories et par sous-catégories
	//##############	
	var template = $('#template-items-liste').html();
	var rendered = Mustache.render(template, {categories:app.itemsCategories});
	document.getElementById('items-liste').innerHTML=rendered; 
	$('.items-sortable').sortable({
		handle: ".item-handle",
		update: function( event, ui ) {
			app.userConfig.itemsOrder=[];
			$( ".items-sortable" ).each(function(){
				var sortedIDs=$( this ).sortable( "toArray", { attribute: "data-id" } );
				for (var i =0,lng= sortedIDs.length; i <lng; i++) {
					app.userConfig.itemsOrder.push({
						id:sortedIDs[i],
						order:i
					})
				}
				
			});
			app.pushUserConfig();
			app.itemsSetOrder();
		},
		cancel: ".ui-sort-disabled"
	});
	//##############
	//Message par défaut si aucun item existe
	//##############
	if(app.items.length==0){
		var ppViewStr="";
		if(app.userConfig.ppView){
			ppViewStr=" et aucun utilisateur n'en a partagé"
		}
		html.push('<div class="well well-sm">Vous n\'avez pas encore créé d\'item'+ppViewStr+'.<br/><br/> <div class="btn btn-default connexion-requise btn-sm" onclick="app.renderItemForm();"><span class="glyphicon glyphicon-plus"></span> Ajouter un item</div>\
			ou utilisez le mode <div class="btn btn-default btn-sm" onclick="app.togglePP();"><span class="glyphicon glyphicon-education"></span> Tous les items</div> pour utiliser les items créés par les autres membres de l\'établissement.</div>');
	}else{
		if(app.itemsCategories.length==0){
			document.getElementById('itemsNothingToShow').style.display='block';
		}	
	}
	$('.itemSlider').slider({
		min:0,
		max:1,
		step:0.01,
		value:0
	});

$('.check-box').prop('disabled', true);
}
app.itemsSetOrder=function(){
	if(!app.userConfig.itemsOrder || app.userConfig.itemsOrder.length==0){
		app.items=app.orderBy(app.items,'item_name','ASC');
		return;
	}
	var orderIndex=[];
	for (var i = app.userConfig.itemsOrder.length - 1; i >= 0; i--) {
		var order=app.userConfig.itemsOrder[i];
		if(order){
			orderIndex[order.id]=order.order;
		}		
	}
	app.items.sort(function(a,b){
		if(a.item_categorie!=b.item_categorie || a.item_sous_categorie!=b.item_sous_categorie){
			return b.item_name.localeCompare(a.item_name);
		}
		if(orderIndex[a.item_id]!=undefined && orderIndex[b.item_id]!=undefined){
			return orderIndex[a.item_id]-orderIndex[b.item_id];
		}else{
			return b.item_name.localeCompare(a.item_name);
		}
	});
}
app.itemsTagsRender=function(tags){
	var tags_split=tags.split(',');
	var new_tags=[];
	for (var i = tags_split.length - 1; i >= 0; i--) {
		var tag=tags_split[i].trim();
		if(tag.length!=0){
			new_tags.push(tag);
		}
	}
	new_tags.sort();
	return new_tags.join(', ');
}
app.itemsRenderDetails=function(item_id,show){	
	if(app.itemsCurrentEleve==null && app.itemsCurrentClasse==null){
		document.getElementById('item_'+item_id+'_details_block').style.display="none";
		return false;
	}
	var progressions=app.itemsProgressions;
	var item=app.getItemById(item_id);
	var max=item['item_value_max'];
	var descriptions=JSON.parse(item['item_descriptions']);	
	if(app.itemsCurrentEleve!==null){
		//Vue élève
		//TODO SEPARER CETTE PARTIE###########################################
		var eleve_items_values=[];
		for (var i = progressions.length - 1; i >= 0; i--) {
			var progression=progressions[i];
			if(progression['rei_user_type']=="user" && progression['rei_user']!=app.userConfig.userID  && app.userConfig.itemsAvisMode=="me"){
				continue;
			}		
			if(progression['rei_eleve']==app.itemsCurrentEleve){
				//Si la valeur vaut -1, cela signifie que l'item est visible pour l'élève et on en tient pas compte dans le calcul
				//Si la valeur vaut -2, cela signifie que l'item est visible et peut s'auto évaluer et on en tient pas compte dans le calcul
				if(app.itemsGetProgressionValue(progression.rei_id)=='-1' 
					|| app.itemsGetProgressionValue(progression.rei_id)=='-2'
					|| (app.userConfig.itemsAvisMode!="students" && progression['rei_user_type']=="eleve") 
					|| (app.userConfig.itemsAvisMode=="students" && progression['rei_user_type']=="user")){
					continue;
			}
			if(progression.rei_date<app.itemFiltersDateStart || progression.rei_date>app.itemFiltersDateEnd){
				continue;
			}
			if(item.item_vue!="cursor"){
				progression['rei_value']=Math.max(progression['rei_value'],1);
			}	
			//#######################
			var pColor="";			
			if(item['item_vue']=="checkbox"){
				pColor="#34b93d";
			}else{
				if(item['item_colors']){
					pColor=app.getColorAffine(progression.rei_value,item['item_colors']);
				}
				else{
					pColor=app.colorByMoyenne((progression.rei_value/max)*20);
				}	
			}
			progression.color=pColor;
			//#######################
			var userStr="";
			var user=app.getUserById(progression.rei_user);
			if(progression['rei_user_type']=="user"){
				if(app.userConfig.itemsAvisMode=="all"){
					userStr=" par <strong>"+user.user_pseudo+"</strong>";
				}				
			}else{
				userStr=" par <strong>"+app.renderEleveNom(app.getEleveById(app.itemsCurrentEleve))+"</strong>";
			}
			progression.userStr=userStr;
			//#######################
			progression.admin=false;
			if(progression['rei_user']==app.userConfig.userID && progression['rei_id']!=-1){
				progression.admin=true;
			}



			//#######################
			progression.rei_symbole=progression.rei_symbole||"square";
			//#######################
			if(!eleve_items_values[progression['rei_item']]){
				eleve_items_values[progression['rei_item']]={
					value:0,
					nb:0,
					liste:[]
				};
			}
			eleve_items_values[progression['rei_item']]['value']+=app.itemsGetProgressionValue(progression.rei_id)*1;
			eleve_items_values[progression['rei_item']]['nb']++;
			eleve_items_values[progression['rei_item']]['liste'].push(progression);
		}		
	};
  //TODO SEPARER CETTE PARTIE###########################################





  var liste=[];
  if(eleve_items_values[item['item_id']]){
  	liste=eleve_items_values[item['item_id']]['liste'];
  }
  if(liste.length==0){
  	document.getElementById('item_'+item_id+'_details_block').style.display="none";
  	return;
  }
		//On affiche la liste des progressions
		var template = $('#template-items-student-progress').html();
		var rendered = Mustache.render(template, {
			progress:liste,
			"progressDate": function () {
				return moment(parseInt(this.rei_date)*1000).format('DD/MM/YY');
			},
			"commentStr":function(){
				if(this.comment){
					return "<i>"+this.rei_comment+"</i>";
				}				
			}
		});
		document.getElementById('item_'+item['item_id']+'_details').innerHTML=rendered; 
	}
	else if(app.itemsCurrentClasse!=null){
		//if(app.itemsCurrentClasse!=null){
			var tab_eleves_ids=app.getClasseById(app.itemsCurrentClasse).eleves;
		// }
		// else{
		// 	//On créé la liste des élèves
		// 	var tab_eleves_ids=[];	
		// 	for (var i = app.eleves.length - 1; i >= 0; i--) {			
		// 		tab_eleves_ids.push(app.eleves[i].eleve_id);		
		// 	};
		// }
		//#######################
		var elevesByValues=[];
		for (var k = progressions.length - 1; k >= 0; k--) {
			var progression=progressions[k];
			if(app.itemsGetProgressionValue(progression.rei_id)<0
				||(progression['rei_user_type']=="user" && progression['rei_user']!=app.userConfig.userID  && app.userConfig.itemsAvisMode=="me")
				||((app.userConfig.itemsAvisMode!="students" && progression['rei_user_type']=="eleve") || (app.userConfig.itemsAvisMode=="students" && progression['rei_user_type']=="user"))) {
				continue;
		}
		if(progression.rei_date<app.itemFiltersDateStart|| progression.rei_date>app.itemFiltersDateEnd){
			continue;
		}

if(document.getElementById('cpt_eleve_'+progression['rei_eleve']+'_checkbox') && app.cptElevesSelectedNum>0){
		if(!document.getElementById('cpt_eleve_'+progression['rei_eleve']+'_checkbox').checked){
			continue;
		}
	}



		if(tab_eleves_ids.indexOf(progression['rei_eleve'])>=0 && progression['rei_item']==item_id){
			if(!elevesByValues[progression['rei_eleve']]){
				elevesByValues[progression['rei_eleve']]=[];
			}
			if(item.item_vue!="cursor" && app.itemsGetProgressionValue(progression.rei_id)>1){
				app.itemsGetProgressionValue(progression.rei_id)=1;
			}
			elevesByValues[progression['rei_eleve']].push(app.itemsGetProgressionValue(progression.rei_id));
		}	
	}
		//#######################
		var tab_values=[];
		for (var i =0 ; i <=max+1; i++) {
			tab_values[i]=[];
		};
		//#######################
		for (var i =0, lng= tab_eleves_ids.length ; i <lng; i++) {
var eleve_id=tab_eleves_ids[i];

if(document.getElementById('cpt_eleve_'+eleve_id+'_checkbox') && app.cptElevesSelectedNum>0){
		if(!document.getElementById('cpt_eleve_'+eleve_id+'_checkbox').checked){
			continue;
		}
	}

			
			if(!elevesByValues[eleve_id]){
				tab_values[max*1+1].push(eleve_id);
			}else{
				var sum=0;
				for (var j = elevesByValues[eleve_id].length - 1; j >= 0; j--) {
					sum+=elevesByValues[eleve_id][j]*1;
				};
				var value=Math.round((sum/elevesByValues[eleve_id].length));
				tab_values[value].push(eleve_id);
			}
		};
	//#######################RENDER
	var html=[];
	for (var i =0 ; i <=max*1+1; i++) {
		html.push('<div class="item-details">');
		html.push('<div>'); 
		var description="";
		var color="";
		if(i==max*1+1){
			if(app.cptElevesSelectedNum==0){
			html.push('<hr/>');	
	}
			//html.push('<span class="h5">Non évalués</span>');	
		}else{
			if(item['item_vue']=="checkbox" && i==0){
				continue;
			}				
			if(item['item_vue']=="checkbox"){
				color="#34b93d";
				description="";
			}else{
				color=app.getColorAffine(i,item['item_colors']);
				description=descriptions[i];
			}
			html.push('<span class="items-square" style="color:'+color+';"></span>');
			var p=Math.round((tab_values[i].length/tab_eleves_ids.length)*100,2);
		html.push(' - <span class="items-tooltip" data-toggle="tooltip" data-placement="top" title="'+p+'%"><span class="h5">'+tab_values[i].length+'<small>/'+tab_eleves_ids.length+'</small></span></span>');
		html.push('<div>');
		html.push('<span class="h4"><small>'+description+'</small></span>'); 
		html.push('</div>');	
		}
			
		



		html.push('</div>');
		html.push('<div>'); 
		if(tab_values[i].length!=0){
			for (var j = tab_values[i].length - 1; j >= 0; j--) {
				var eleve=app.getEleveById(tab_values[i][j]);
				html.push('<a class="btn btn-default btn-sm" href="#skills/student/'+eleve.eleve_id+'">');
				html.push(app.renderEleveNom(eleve)); 
				html.push('</a>'); 
			};
		}		
		html.push('</div>');
		html.push('</div>');
	};
	$('#item_'+item['item_id']+'_current_description').css('display','none');
	document.getElementById('item_'+item['item_id']+'_details').innerHTML=html.join('');
} 
var elem=document.getElementById('item_'+item_id+'_details_block');
if(show){
	elem.style.display="block";
}
else{
	if(elem.style.display!="none"){
		elem.style.display="none";
		return false;
	}else{
		elem.style.display="block";
	}	
}
}
app.renderItemsClasses=function(){	
var template = $('#template-items-classes-select').html();
	var rendered = Mustache.render(template, {
		classes:app.classes,
		"classeNom":function(){
			return app.cleanClasseName(this.classe_nom)
		},
		"isVisible":function(){
			if(!this.visible || this.eleves.length==0){
				return false;
			}
			return true;
		}
	});
	document.getElementById('items-classes-select').innerHTML=rendered; 
};
app.renderItemsClassesEleves=function(classe_id){	
	var classe=app.getClasseById(classe_id);
var template = $('#template-items-classes').html();
	var rendered = Mustache.render(template, {
		eleves:classe.eleves,
		classe_id:classe_id,
		"eleveNom": function () {
			return app.renderEleveNom(app.getEleveById(this));
		}
	});
	document.getElementById('items-classes').innerHTML=rendered; 
};
app.itemsCategoriesProgressRender=function(){
	var categories_progression=[];
	var categorie_id=[];

	for (var i = app.items.length - 1; i >= 0; i--) {
		var item=app.items[i];
		if(!app.itemsIsVisible(item)){
			continue;
		}
		if(item.item_progression_in_percent=="-" || item.item_progression==null){continue;}
		var index=categorie_id.indexOf(item.item_categorie_id);
		if(index<0){
			categories_progression.push({
				id:item.item_categorie_id,
				liste:[item.item_progression_in_percent]
			});
			categorie_id.push(item.item_categorie_id);					
		}
		else{
			categories_progression[index].liste.push(item.item_progression_in_percent);	
		}	
	}
		//On affiche la progression de la catégorie
		for (var i = categories_progression.length- 1; i >= 0; i--) {
			var lng=categories_progression[i].liste.length;
			
			if(categories_progression[i].id!=undefined){
				var value=categories_progression[i].liste.reduce(app.add,0)/lng;
				document.getElementById('categorie_'+categories_progression[i].id+'_progression').innerHTML="<small title='"+Math.round(value*20)/100+"/20'> "+Math.round(value*100)/100+"%</small>";	
			}
		}
	}
	app.skillsFooterClose=function(){
		if(document.getElementById('items-selection-btn').style.display=="none" && document.getElementById('items-students-selection-bloc').style.display=="none"){
			$('#items-footer').css('display','none');
		}
	};