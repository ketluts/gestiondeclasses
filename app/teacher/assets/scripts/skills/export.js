/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.skillsItemsExport=function(){
	var items=[];
	for (var i = app.items.length - 1; i >= 0; i--) {
		var item=app.items[i];
		if(!app.itemsIsVisible(item)){
			continue;
		}
		items.push(item);
		//TODO nettoyer l'item pour garder l'essentiel
	}
	var file = new File([JSON.stringify(items)], "items.json", {type: "application/json;charset=utf-8"});
	saveAs(file);
}

app.skillsItemsPrint=function(){
	var itemsToPrint=[];
	for (var i =0, lng= app.items.length; i<lng; i++) {
		var item=app.items[i];
		if(document.getElementById('item_'+item['item_id']+'_checkbox') && document.getElementById('item_'+item['item_id']+'_checkbox').checked){
			itemsToPrint.push(item);
		}
	}
	var html=[];
	html.push(' <meta charset="UTF-8" />');
	html.push('<table style="border:1px solid;border-collapse: collapse;width:100%">');
	for (var i = itemsToPrint.length - 1; i >= 0; i--) {
		var item=itemsToPrint[i];
		html.push('<tr style="border:1px solid;">');
		html.push('<td style="border:1px solid;width:20px;padding:5px;text-align:center;">');
		//html.push('&nbsp;');
		html.push('<div style="width:20px;height:20px;border:1px solid;border-radius:2px;"></div>');
		html.push('</td>');
		html.push('<td style="border:1px solid;padding:5px;">');
		html.push(ucfirst(item.item_name));
		html.push('</td>');
		html.push('</tr>');
	}
	html.push('</table>');
document.getElementById('export-pdf-data').value=html.join('');
document.getElementById('export-pdf-titre').value='items';  
document.forms["export-pdf-form"].action=app.serveur + "index.php?go=pdf&q=a4" + app.connexionParam;
document.forms["export-pdf-form"].submit();

	
}