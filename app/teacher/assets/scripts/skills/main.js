/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.skillsViewInit=function(){
	
	app.viewClear();
	app.currentView="competences";		
	app.itemsCurrentEleve=null;
	app.itemsCurrentClasse=null;
	app.itemsToEdit=null;

	app.hide('item_form');
	app.hide('items_results_import');
	app.hide('items_import');
	app.hide('itemsNothingToShow');
	app.hide('items-student-graph-block');
	app.hide('items-classroom-tab-block');
	app.hide('feedbacks');
	$('#items-footer').css('display','none');
	app.show('template_skills');
	$('#items-students-selection-bloc').css('display','none');

	document.getElementById('items-liste').innerHTML=app.renderLoader();
	document.getElementById('cpt-eval-activite').value="";
	document.getElementById('cpt-eval-comment').value="";		
	document.getElementById('cpt-eval-date').value="";	
	document.getElementById('itemsFiltersSaveName').value="";
	document.getElementById('items-avis-mode').value=app.userConfig.itemsAvisMode;
	// $("#items-titre-bloc").stick_in_parent({
	// 	sticky_class:"competences_sticky",
	// 	offset_top:62
	// });	
	app.renderItemsClasses();
	document.getElementById('items-classes-select').value="-3";
	app.cptElevesSelectedLock=false;
	app.cptElevesSelectedLockToggle();	
	app.renderItems();	
	app.itemsFiltersLoadFilter(false,app.userConfig.itemsCurrentFilter);
};

//##############
//ITEMS TOOLBAR
//##############
app.toggleSkillsAvisMode=function(mode){
	app.userConfig.itemsAvisMode=mode;
	document.getElementById('items-avis-mode').value=app.userConfig.itemsAvisMode;
	app.navigate(app.currentHash,true);
	$('.item_details').each(function(){		
		if($(this).css('display')!='none'){
			app.itemsProgressionUpdateCallback($(this).attr('data-id'));
		}
	});
	app.pushUserConfig();
};


app.itemsProgressionsInit=function(){
	app.itemsCurrentEleve=null;
	app.itemsCurrentClasse=null;	
	document.getElementById('titre').innerHTML="Mes items";
	document.getElementById('items-classes-select').value="-3";
	$('#items-classes').css('display','none');
	$('.item-details').css('display','none');
	//app.show('items-classes-container');
	app.show('items-liste');
	$('#app').goTo();
	$('.item-classe-list-view').css('display','');
	$(".items-classe-bloc").css("display","block");
	$('.item-toolbar-right').css("display","inline-block");	
	$('.item-classe-view').css("display","none");
	$('.item-all-view').css("display","block");		
	app.hide('items-classroom-tab-block');
	app.hide('items-classroom-graph-block');
	app.hide('items-student-graph-block');		
	$('.item-classe-tab-view').css('display','none');
	$(".items_classe_eleves_container").css('display','none');
	$('.cpt_classe_checkbox').css("display","none");
	$('.item-eleve-view').css("display","none");
	$(".item_view").css("display","none");

	$(document.body).trigger("sticky_kit:recalc");
	$(".itemsEleve").removeClass('itemsEleveSelected');
	$(".itemsClasse").removeClass('itemsClasseSelected').css("display","block");

	$('.item_checkbox_details').html('');	
	$(".categories_progression").html('');

	$('.slider-progress-style').css('background-color','rgba(255,255,255,0)');
	$('.itemSlider').off('slidechange')
	.off('slide')
	.slider({
		min:0,
		max:1,
		step:0.01,
		value:0
	});
	$('.check-box').prop('checked', false);
	$(".itemSlider").slider( "disable" );	
	$('.items-tooltip').tooltip();
	app.itemsSetFilters();
}
