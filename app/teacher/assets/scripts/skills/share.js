/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
//##############
//ITEMS SHARE
//##############
app.toggleItemShare=function(item_id){
	var itemsToUpdate=[];
	if (!app.checkConnection()) {return;}
	var is_public=false;
	if(item_id=='selection'){
		itemsToUpdate=app.itemsSelection;
		if(app.itemsShareNb<=app.itemsSelection.length/2){
			is_public=true;
			$('#items-selection-share').removeClass('btn-default').addClass('btn-primary');
		}
		else{
			$('#items-selection-share').removeClass('btn-primary').addClass('btn-default');
		}
		for (var i = app.items.length - 1; i >= 0; i--) {
			var item=app.items[i];
			if(app.itemsSelection.indexOf(item['item_id'])>=0){
				$('#item_view_'+item['item_id']+'_share').removeClass('btn-primary').addClass('btn-default');
				if(is_public){		
					$('#item_view_'+item['item_id']+'_share').removeClass('btn-default').addClass('btn-primary');
				}
			}
			item.item_public=is_public;
		}
	}else{
		var item=app.getItemById(item_id);
		itemsToUpdate=[item_id];
		if(item['item_public']){
			$('#item_view_'+item_id+'_share').removeClass('btn-primary').addClass('btn-default');
		}
		else{
			is_public=true;
			$('#item_view_'+item_id+'_share').removeClass('btn-default').addClass('btn-primary');
		}

		item.item_public=is_public;
	}	
	app.itemsSelectedUpdate();
	$.post(app.serveur + "index.php?go=items&q=updateItems"+app.connexionParam,
	{	
		item_id:JSON.stringify(itemsToUpdate),
		item_public:is_public,
sessionParams:app.sessionParams
	}, function(data) {		
		app.render(data);   
	}
	);
};
app.toggleItemView=function(item_id,mode){
	if (!app.checkConnection()) {return;}
	var itemsToUpdate=[];
	var value=0;
	if(item_id=="selection"){
		itemsToUpdate=app.itemsSelection;
		if(mode=='view'){
			if(app.itemsViewNb<=app.itemsSelection.length/2){
				value=-1;
			}else{
				value=-3;
			}
		}
		else{
			if(app.itemsEditNb<=app.itemsSelection.length/2 && mode=='edit'){
				value=-2;
			}
			else{
				value=-1;
			}
		}
	}else{
		itemsToUpdate.push(item_id);
		if(mode=='view'){
			var view=$('#item_view_'+item_id+'').attr('data-value');
			if(view=='true'){
				value=-3;
			}
			else{
				value=-1;
			}
		}else{
			var edit=$('#item_view_'+item_id+'_edit').attr('data-value');
			if(edit=='true'){
				value=-1;
			}else{
				value=-2;
			}
		}
	}
	var eleves=[];
	if(app.itemsCurrentEleve!=null){
		eleves.push(app.itemsCurrentEleve);	
	}
	else if(app.itemsCurrentClasse!=null){
		
		eleves=app.getClasseById(app.itemsCurrentClasse).eleves;	
	}
	for (var i = itemsToUpdate.length - 1; i >= 0; i--) {
		var item_id=itemsToUpdate[i];
		if(value==-3){
			$('#item_view_'+item_id+'').attr('data-value','false').removeClass('btn-primary').addClass('btn-default');
			$('#item_view_'+item_id+'_edit').attr('data-value','false').removeClass('btn-primary').addClass('btn-default');
			document.getElementById('item_view_'+item_id+'').innerHTML='<span class="glyphicon glyphicon-eye-open"></span>';
			document.getElementById('item_view_'+item_id+'_edit').innerHTML='<span class="glyphicon glyphicon-pencil"></span>';	
		}
		else if(value==-1){
			$('#item_view_'+item_id+'_edit').attr('data-value','false').removeClass('btn-primary').addClass('btn-default');
			document.getElementById('item_view_'+item_id+'_edit').innerHTML='<span class="glyphicon glyphicon-pencil"></span>';	
			$('#item_view_'+item_id+'').attr('data-value','true').removeClass('btn-default').addClass('btn-primary');
			if(app.itemsCurrentClasse!=null && app.itemsCurrentEleve==null){
				document.getElementById('item_view_'+item_id+'').innerHTML='<span class="glyphicon glyphicon-eye-open"></span> <span class="badge badge_items">'+eleves.length+'/'+eleves.length+'</span>';
			}
		}
		else{
			$('#item_view_'+item_id+'_edit').attr('data-value','true').removeClass('btn-default').addClass('btn-primary');
			$('#item_view_'+item_id+'').attr('data-value','true').removeClass('btn-default').addClass('btn-primary');
			if(app.itemsCurrentClasse!=null && app.itemsCurrentEleve==null){
				document.getElementById('item_view_'+item_id+'_edit').innerHTML='<span class="glyphicon  glyphicon-pencil"></span>  <span class="badge badge_items">'+eleves.length+'/'+eleves.length+'</span>';
				document.getElementById('item_view_'+item_id+'').innerHTML='<span class="glyphicon glyphicon-eye-open"></span> <span class="badge badge_items">'+eleves.length+'/'+eleves.length+'</span>';
			}
		}
	}
	app.itemsSelectedUpdate();
	$.post(app.serveur + "index.php?go=items&q=addProgression"+app.connexionParam,
	{
		items:JSON.stringify(itemsToUpdate),
		eleves:JSON.stringify(eleves),
		progression:value,
		time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
	}, function(data) {
		app.render(data);		
	}
	);
}