/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.itemsToggleClasse=function(classe_id){
	if(classe_id==-3){
		app.go('skills');
		return;
	}
	app.go('skills/classroom/'+classe_id);
	
}
app.cptElevesUnselectAll=function(){
	$('.cpt_eleve_checkbox').prop('checked', false);
	$('.cpt_classe_checkbox').prop('checked', false);
	app.cptElevesSelectedUPD();
	app.cptElevesSelectedLock=false;
	app.cptElevesSelectedLockToggle();
}
app.cptElevesSelectedUPD=function(){
	if(app.itemsCurrentClasse==null){
		$('#items-students-selection-bloc').css('display','none');
		app.skillsFooterClose();
		return;
	}
	var eleves=[];
	var classe=app.getClasseById(app.itemsCurrentClasse);
	for (var i = classe.eleves.length - 1; i >= 0; i--) {
		var eleve_id=classe.eleves[i];
		var eleve=app.getEleveById(eleve_id);
		if(document.getElementById('cpt_eleve_'+eleve_id+'_checkbox').checked){
			eleves.push(eleve_id);
		}
	}
	var n=eleves.length;
	app.cptElevesSelectedNum=n;
	if(n>0){
		$('#items-footer-texte').html(n+" élève"+app.pluralize(n,'s'));
		$('#items-footer').css('display','block');
		$('#items-students-selection-bloc').css('display','block');
	}
	else{

		$('#items-students-selection-bloc').css('display','none');
		app.skillsFooterClose();
	}
	app.itemsRenderProgressionByClasse(app.itemsCurrentClasse);
}
app.cptElevesSelectedLockToggle=function(confirm){
	if(!app.cptElevesSelectedLock){		
		app.cptElevesSelectedLock=true
		$('.check-box').prop('disabled', true);
		$( ".itemSlider" ).slider( "disable" );	
		$('#items-footer-lock-btn').html('<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>').prop('title',"L'évaluation multiple est vérrouillée.").removeClass('btn-success').addClass('btn-danger');
	}
	else{
		if(!confirm){
			app.alert({title:'Attention, l\'évaluation multiple va être dévérrouillée.',type:'confirm'},function(){app.cptElevesSelectedLockToggle(true);});
			return;
		}
		app.cptElevesSelectedLock=false;
		$('.check-box').prop('disabled', false);
		$( ".itemSlider" ).slider( "enable" );	
		$('#items-footer-lock-btn').html('<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>').prop('title',"Attention ! L'évaluation multiple est dévérrouillée.").addClass('btn-success').removeClass('btn-danger');
	}
}
app.cptElevesSelectedAll=function(classe_id){
	if(!document.getElementById('cpt_classe_'+classe_id+'_checkbox').checked){		
		app.cptElevesUnselectAll();
		return;
	}
	var classe=app.getClasseById(classe_id);
	for (var i = classe.eleves.length - 1; i >= 0; i--) {
		var eleve_id=classe.eleves[i];	
		$('#cpt_eleve_'+eleve_id+'_checkbox').prop('checked', true);
	}
	app.cptElevesSelectedUPD();
	app.itemsRenderProgressionByClasse(classe_id);
}
app.itemsSyntheseSetMode=function(mode,save){
	app.userConfig.itemsSyntheseMode=mode;
	if(save!=false){app.pushUserConfig();}
	$(".itemsSyntheseMode_"+mode).prop( "checked", true );
	if(app.currentView=="competences"){
		app.itemsClassroomSkillsGraph();
		app.itemsClassroomSkillsTab();
	}  
}
app.itemsGenerateGroupsGo=function(){
	app.go('classroom/'+app.itemsCurrentClasse);
	setTimeout(
		function(){
			document.getElementById("classroom-groupes-selectColor").value='acquis';
			document.getElementById('generateurGroupes').style.display = 'block';			
			document.getElementById('groupes_de_niveaux').checked="checked";
			app.classroomGroupsCheckboxsRules('niveaux');
			document.getElementById('groupes_de_niveaux_type').value="homogenes";
			document.getElementById('groupes_de_niveaux_mode').value="competences";
			document.getElementById('groupsGenerator-studentsNb').value=4;
			app.getGroupes(0);
			$('#generateurGroupes').goTo();
		},1000);	
}