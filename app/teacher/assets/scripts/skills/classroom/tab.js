/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.itemsClassroomTabToggleView=function(enable){
	if(enable===false || (app.itemsClassroomTab && enable!==true)){
		app.itemsClassroomTab=false;
		app.show('items-classes-container');
		app.hide('items-classroom-tab-block');
		app.show('items-liste');
		$('.item-classe-list-view').css('display','');
		$('.item-classe-tab-view').css('display','none');
		//$(document.body).trigger("sticky_kit:recalc");
		return;
	}
	app.itemsClassroomTab=true;
	app.hide('items-classes-container');
	app.hide('items-liste');
	app.show('items-classroom-tab-block');
	$('.item-classe-list-view').css('display','none');
	$('.item-classe-tab-view').css('display','');
}
app.itemsClassroomSkillsTab=function(){
	var classe=app.getClasseById(app.itemsCurrentClasse);
	var eleves=classe.eleves;
	var html=[];
	var html_head=[];
	html.push("<table class='table table-striped table-bordered' id='items-classroom-tab-table'>");
	html.push("<tbody>");
	for (var i =0,lng=eleves.length ; i<lng; i++) {
		var eleve_id=eleves[i];
		var eleve=app.getEleveById(eleve_id);
		var progressionsByTag=app.itemsStudentGetProgressionByTags(eleve_id);
		var values=progressionsByTag.values;
		if(i==0){
			html_head.push("<thead>");
			html_head.push('<tr>');  
			var tags=progressionsByTag.tags;
			html_head.push("<th>");
			html_head.push("</th>");
			for (var k =0,lllng= tags.length ; k<lllng; k++) {
				var tag=tags[k];	
				html_head.push("<th>");
				html_head.push('<div class="classroom-tests-table-title">');  
				html_head.push(tag);
				html_head.push("</div>");
				html_head.push("</th>");
			}
			html_head.push('</tr>');
			html_head.push("</thead>");
		}
		html.push("<tr>");
		html.push("<td>");
		html.push(app.renderEleveNom(eleve)); 
		html.push("</td>");
		for (var j =0,llng= values.length; j<llng; j++) {
			var progression=values[j];
			html.push("<td>");
			//html.push('<div class="pie">'+Math.round(progression)+'%</div>');
			if(progression==-1){
				html.push('-');
			}else{
				html.push(progression);	
			}
			
			html.push("</td>");
		}
		html.push("</tr>");
	}
	html.push("</tbody>");
	html=html.concat(html_head);
	html.push("</table>");
	document.getElementById('items-classroom-tab').innerHTML=html.join('');

	// [].forEach.call(document.getElementsByClassName("pie"), function (pie) {
	// 	var p = parseFloat(pie.textContent);
	// 	var NS = "http://www.w3.org/2000/svg";
	// 	var svg = document.createElementNS(NS, "svg");
	// 	var circle = document.createElementNS(NS, "circle");
	// 	var title = document.createElementNS(NS, "title");
	// 	circle.setAttribute("r", 16);
	// 	circle.setAttribute("cx", 16);
	// 	circle.setAttribute("cy", 16);

	// 	circle.setAttribute("stroke-dasharray", p + " 100");
	// 	svg.setAttribute("viewBox", "0 0 32 32");
	// 	title.textContent = pie.textContent;
	// 	pie.textContent = '';
	// 	svg.appendChild(title);
	// 	svg.appendChild(circle);
	// 	pie.appendChild(svg);
	// });
	$('#app').goTo();
}