/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.itemsClassroomSkillsGraph=function(){
	var classe=app.getClasseById(app.itemsCurrentClasse);
	var eleves=classe.eleves;
	var categories=[];	
	var series=[];
	var data=[];
	var colors=[
	'#e5e5e5',
	'#cccccc',
	'#b2b2b2',
	'#999999'
	];
	for (var i =0,lng=eleves.length ; i<lng; i++) {
		var eleve_id=eleves[i];
		var eleve=app.getEleveById(eleve_id);
		var progressionsByTag=app.itemsStudentGetProgressionByTags(eleve_id);
		var values=progressionsByTag.values;
		var tags=progressionsByTag.tags;		
		if(i==0){

			for (var k =0,lllng= tags.length ; k<lllng; k++) {
				var tag=tags[k];
				data.push({
					id: tag,
					name: tag,
						color: colors[k%4],
						value:1
					});				
			}
		}
		for (var j =0,llng= values.length; j<llng; j++) {
			var progression=values[j];	
			if(progression>50 || progression<0){continue;}
			data.push({
				id: eleve.eleve_id+'_'+j,
				name: app.renderEleveNom(eleve),
				parent: tags[j],
				value:Math.pow(((100-progression)/10)-5,3)
			});	
		}
	}
	$('#classroom_competences_graph').highcharts({
		series: [{
			type: "treemap",
			layoutAlgorithm: 'squarified',
			alternateStartingDirection: true,
			borderColor:"#000000",
			levels: [{
				level: 1,
				layoutAlgorithm: 'strip',
				dataLabels: {
					enabled: true,
					align: 'left',
					verticalAlign: 'top',
					style: {
						fontSize: '15px',
						fontWeight: 'bold'
					}
				}
			}],
			data: data
		}],
		title: {
			text: 'Élèves ayant besoin d\'aide'
		}
	});
	$('#app').goTo();
}

app.itemsClassroomHelpGraphToggle=function(){
	var elem=document.getElementById('items-classroom-graph-block');
	if(elem.style.display!="none"){
		elem.style.display="none";
		return false;
	}else{
		elem.style.display="block";    
		app.itemsClassroomSkillsGraph();
		return true;
	}
}