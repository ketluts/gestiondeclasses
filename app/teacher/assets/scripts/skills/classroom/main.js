/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
//##############
//ITEMS CLASSE VIEW
//##############
app.itemsRenderClassesInit=function(classe_id){
	app.itemsCurrentEleve=null;
	var classe=app.getClasseById(classe_id);
	var classe_nom=classe.classe_nom;
	document.getElementById('titre').innerHTML = "<a href='#classroom/"+classe.classe_id+"' class='btn app.userConfig.itemsAvisMode btn-default'>"+app.cleanClasseName(classe_nom)+"</a>";	
	if(app.itemsCurrentClasse!=classe_id){
		$('.item_details').css('display','none');
		app.itemsCurrentClasse=classe_id;
	}
	app.cptElevesSelectedNum=0;
	document.getElementById('items-classes-select').value=classe.classe_id;
	$('.item-all-view').css("display","none");	
	$('.item-eleve-view').css("display","none");
	$('.item-classe-view').css("display","");
	$('#items-classes').css('display','');
	$('#items-titre-bloc').css('display','');
	$('#feedbacks').css("display","none");
	$(document.body).trigger("sticky_kit:recalc");
	$(".itemsEleve").removeClass('itemsEleveSelected');		
	$(".itemsClasse").removeClass('itemsClasseSelected');
	$('.cpt_classe_checkbox').css("display","none");
	app.hide('items-classroom-graph-block');
	$('.item-toolbar-right').css("display","none");	
	$("#cpt_classe_"+classe_id+"_checkbox").css("display","block");
	$('#items_classe_'+classe_id+'').addClass('itemsClasseSelected');		
	$('#items_classe_'+classe_id+'_eleves_container').css("display","block");
	$(".items-classe-bloc").css("display","none");
	$("#items_classe_"+classe_id+"_bloc").css("display","block");
	$(".categories_progression").html('');	
	$(".item_view").css("display","block").addClass('btn-default').removeClass('btn-primary');
	app.hide('items-student-graph-block');	
	app.hide('items-activity');
	app.itemsClassroomTabToggleView(app.itemsClassroomTab);
	app.renderItemsClassesEleves(classe_id);
}
app.itemsRenderProgressionByClasse=function(classe_id){
	var classe=app.getClasseById(classe_id);
	if(app.itemsCurrentClasse!=classe_id || app.itemsCurrentEleve!=null){
	app.itemsRenderClassesInit(classe_id);	
}
	var eleves=classe.eleves;	
	var classeProgression=app.itemsStudentsProgressionsBuild(eleves);
	classe.progressions=classeProgression;
	//Affichage des résultats sur les slides par categorie et par item
	for (var c =0,lng=app.itemsCategories.length; c <lng; c++) {
		var categorie=app.itemsCategories[c];
		for (var i =0, llng=categorie['sous_categories'].length;i<llng; i++) {
			var sous_categorie=categorie['sous_categories'][i];
			for (var j = 0,lllng=sous_categorie['list'].length ; j<lllng; j++) {
				var item=sous_categorie['list'][j];	
				app.itemsRenderProgressionByItemAndClassroom(item.item_id);
			}
		}
	}
	$( ".itemSlider" ).slider( "disable" );	
	$('.check-box').prop('disabled', true);
	$('.items-tooltip').tooltip();
	app.itemsSetFilters();
	app.itemsClassroomSkillsTab();
};	
app.itemsRenderProgressionByItemAndClassroom=function(item_id){
	var item=app.getItemById(item_id);
	var classe=app.getClasseById(app.itemsCurrentClasse);
	var eleves=classe.eleves;
	var classeProgression=classe.progressions;
	app.itemsToolbarButtonsSet(item['item_id']);
	$('#item_'+item['item_id']+'_checkbox_details').html("");
	var max=item['item_value_max'];
	var html=[];
	html.push("<div class='btn btn-default btn-sm' style='position:relative;' onclick=\"app.itemsRenderDetails("+item['item_id']+");\"><span class='glyphicon glyphicon-th-list'></span>");
	html.push("</div>");
	document.getElementById('item_'+item['item_id']+'_btn').innerHTML=html.join('');
	document.getElementById('item_'+item['item_id']+'_btn').style.visibility="visible";
	item['item_progression']=-1;
	item['item_progression_in_percent']="-";
	if(!classeProgression[item['item_id']]){
		var slide=$("#item_"+item['item_id']+"_slider")
		.off('slidechange')
		.off('slide')
		.slider( "option", "min", 0)
		.slider( "option", "max", max)
		.slider("option",'step',0.01)
		.slider('value',0)
		.on( "slidechange",app.itemsProgressionAdd)
		.on( "slide",app.itemsProgressionSlideStyleRender);
		$('#item_'+item['item_id']+'_slider_style').css('background-color',""); 
		return;
	}				
	if(item['item_user']!=app.userConfig.userID && !app.userConfig.ppView){
		return;
	}	
	var progression=app.itemsClassroomProgressionByItem(classe.classe_id,item['item_id']);
	item['item_progression_in_percent']=progression.string;
	item['item_progression']=progression['value']*100/max;
	if(item['item_vue']=="cursor"){ 
		//Cursor
		$("#item_"+item['item_id']+"_slider")
		.slider( "option", "min", 0)
		.slider( "option", "max", max)
		.slider("option",'step',0.01)
		.off('slidechange')
		.off('slide')
		.slider('value',progression.slideValue)
		.on( "slidechange",app.itemsProgressionAdd)
		.on( "slide",app.itemsProgressionSlideStyleRender);
		var style=app.itemsProgressionStyleGet(progression.value,item['item_id']);
		$('#item_'+item['item_id']+'_slider_style').css('width',Math.floor(progression.slideValue*100/max)+"%");				
		$('#item_'+item['item_id']+'_slider_style').css('background-color', style.color); 
	}
	else{
		//Checkbox
		if(progression.value>=1){
			document.getElementById('item_'+item['item_id']+'_checkbox_mode').checked="checked";
		}else{
			document.getElementById('item_'+item['item_id']+'_checkbox_mode').checked="";
		}
		var nb=classeProgression[item['item_id']]['nb'];
		if(nb<0){
			nb=0;
		}				
		$('#item_'+item['item_id']+'_checkbox_details').html("<span class='h4'>"+nb+"<small>/"+eleves.length+"</small></span>").prop('title',Math.round(progression.string*100)/100+"%");
		$('#item_'+item['item_id']+'_checkbox_mode').prop("disabled", false);
	}
	if(document.getElementById('item_'+item_id+'_details_block').style.display!="none"){
		app.itemsRenderDetails(item_id,true);
	}
}

app.itemsClassroomProgressionByItem=function(classe_id,item_id){
	var classe=app.getClasseById(classe_id),
	item=app.getItemById(item_id),
	max=item['item_value_max'],
	slideValue=-1,
	value_in_percent="-",
	value=-1,
	progressions=classe.progressions;
	if(progressions[item['item_id']]){	
		value=progressions[item['item_id']]['value'];
		if(value!=-1){									
			slideValue=Math.round(Math.max(value,max*0.08)*100)/100;
		}	
		value_in_percent=Math.min(Math.max(value*100/max,0),100);
	}
	return {
		"value":Math.min(value,max),
		"slideValue":Math.min(slideValue,max),
		"string":value_in_percent,
		'nb_evaluations':progressions[item['item_id']]['nb']
	};
}