/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
//##############
//ITEMS CREATE/EDIT
//##############
app.renderItemForm=function(){
	if (!app.checkConnection()) {return;}
	app.itemsToEdit=null;	
	app.itemEditionTags=[];
	$('.item_save_btn').button('reset');
	document.getElementById('item-form-titre').innerHTML = "Création d'un item";
	document.getElementById('item_form').style.display = "block";
	document.getElementById('item_name').value="";
	document.getElementById('item_cycle').value=-1;
	document.getElementById('item_color').value="#E3E3E3";
	document.getElementById('item_tags').value="";
	document.getElementById('item_categorie').value="";
	document.getElementById('items-edition-tags-mode').style.display='none';
	$('.items-edit-selection').css('display','');
	document.getElementById('item_sous_categorie').value="";
	document.getElementById('item_value_max').value="3";
	$("#item_value_max").prop("disabled", false);
	document.getElementById('item-form-delete').innerHTML="";	
	app.itemsEditionTagsListRender();
	app.setItemsTagsDatalist();
	app.itemsVueChange(app.itemsVue);
	$(".item-vue").prop("disabled", false);
	$("#item_form input").prop("disabled", false);	
	$(".items-edition-input>div").hide();
	$('.item_save_btn').off('click');
	$('.item_save_btn').click(function() {app.addItem(); return false; });
};
app.previewItemsSlider=function(value){
	//Aperçu des couleurs
	var html=[];
	html.push('<div class="btn-group">');
	for (var i = 0; i <= value; i++) {
		var color="";
		if(app.itemsToEdit){	
			color=app.getColorAffine(i,app.itemsToEdit['item_colors']);
		}else{
			color=app.colorByMoyenne((i/value)*20);
		}
		html.push('<input type="color" class="btn btn-default btn-xs" value="'+color+'" id="item-color-'+i+'" onchange="app.itemDescriptionColorUpdate('+i+',this.value)"/>');
	}
	html.push('</div>');
	html.push('<h4><small>Cliquez pour changer la couleur</small></h4>');
	document.getElementById('item-preview').innerHTML=html.join('');


	//Aperçu des descriptions
	html=[];
	for (var i = 0; i <= value; i++) {
		html.push('<div id="item-description-'+i+'-wrapper" class="item-description-wrapper">');	
		html.push('<div type="text" class="editable-container" id="item-description-'+i+'-editeur"></div>\
			<textarea type="text" value="" id="item-description-'+i+'" style="display:none;"></textarea>\
			');
		
		if(app.itemsToEdit){
			html.push('<div id="item-feedbacks-dropzone-'+i+'" class="flex-rows item-dropzone" ondrop="app.itemsFeedbacksDrop(event,'+i+');" ondragover="app.itemsFeedbacksDragOverHandler(event);">');	
			html.push('<div id="item-feedbacks-'+i+'-liste" class="item-feedbacks-liste">');
			html.push('Glissez ici des exercices.');
			html.push('</div>');
			html.push('<div id="item-feedbacks-'+i+'-loading" class="item-feedbacks-loader flex-1">');
			html.push('</div>');
			html.push('</div>');
		}
		html.push('</div>');
	}
	document.getElementById('item-descriptions').innerHTML=html.join('');

	app.editors=[];
	for (var i = 0; i<=value; i++) {
		var color="";
		if(app.itemsToEdit){
			color=app.getColorAffine(i,app.itemsToEdit['item_colors']);
		}else{
			color=app.colorByMoyenne((i/value)*20);
		}
		if(value==0){
			color="#009900";
		}
		app.editors[i] = new Quill('#item-description-'+i+'-editeur', {
			modules: {  
				toolbar: app.toolbarOptions
			},
			styles: false,
			theme: 'snow'
		});

		var feedbacks=[];
		if(app.itemsToEdit && app.itemsToEdit['item_feedbacks']){
			feedbacks=JSON.parse(app.itemsToEdit['item_feedbacks']);
		}
		(function(num){			
			if(app.itemsToEdit && app.itemsToEdit['item_descriptions']){
				$("#item-description-"+num+"-editeur > .ql-editor").html(JSON.parse(app.itemsToEdit['item_descriptions'])[num]);
			}
			var files=[];
			feedbacks[num]=feedbacks[num]||[];
			for (var j=0; j <feedbacks[num].length; j++) {
				var file_id=feedbacks[num][j];
				files.push(app.getFileById(file_id));
			}
			if(files.length>0){
html=[];
files=app.orderBy(files,'file_id','DESC');
				for (var i = files.length - 1; i >= 0; i--) {
					var file=files[i];
html.push('<div>');

 // if(['jpg','jpeg','png','gif'].indexOf(file.file_type)>=0){
   
 //    html.push('<img height="50" src="'+app.serveur + "index.php?go=share&q=getImage&file_id="+file.file_id+"&sessionParams="+encodeURIComponent(app.sessionParams)+'"/>');
 //  }else{
 	html.push('<span class="btn btn-xs btn-default" onclick="app.itemsFeedbacksFileDelete('+file.file_id+');"><span class="glyphicon glyphicon-remove"></span></span>');
  	html.push('<span class="file-icon file-icon-sm" data-type="'+file.file_type+'"></span> ');

    html.push("<a href='"+app.serveur + "index.php?go=share&q=getFile&file_id="+file.file_id+"&sessionParams="+encodeURIComponent(app.sessionParams)+"'>"+file.file_fullname+'</a>');
 
 // }
html.push('</div>');
				}




				// var template = $('#template-items-feedback-liste').html();
				// var rendered = Mustache.render(template, {
				// 	files:files
				// });
				
				 $('#item-feedbacks-'+num+'-liste').html(html.join('')); 



			}
			
		})(i);
		$('#item-description-'+i+'-wrapper').css('border-left','3px solid '+color);
	};

	if(app.itemsToEdit){	
		if(app.itemsToEdit['item_mode']=="false"){
			app.itemsToEdit['item_mode']="auto";
		}
		document.getElementById('item-mode-'+app.itemsToEdit['item_mode']).checked=true;
	}
}
app.itemDescriptionColorUpdate=function(value,color){
	$('#item-description-'+value+'-wrapper').css('border-left','3px solid '+color);
}
app.itemsFormClose=function(){
	app.hide('item_form');
	if(app.itemsToEdit){
		window.scrollTo(0,app.itemsToEdit.scrollY);
	}
}
app.addItem=function(){	
	if (!app.checkConnection()) {return;}
	var value=document.getElementById('item_value_max').value;
	var name=document.getElementById('item_name').value||null;
	var item_color=document.getElementById('item_color').value||null;
	var categorie=document.getElementById('item_categorie').value||null;
	var cycle=document.getElementById('item_cycle').value*1||-1;
	var sous_categorie=document.getElementById('item_sous_categorie').value||null;
	var tags=arrayUnique(app.itemEditionTags.map(function(value){return ucfirst(value.trim().toLowerCase());})).sort().join(', ');
	var item_mode=app.itemsMode||"auto";
	var item_vue=app.itemsVue||"cursor";
	var colors=[];
	var descriptions=[];
	if(app.itemsVue=="checkbox"){
		colors=['#009900'];
		descriptions=[$("#item-description-0-editeur > .ql-editor").html()];
		item_mode='libre';
		value=1;
	}else{
		for (var i = 0; i<=value; i++) {
			colors.push(document.getElementById('item-color-'+i+'').value);
			descriptions.push($("#item-description-"+i+"-editeur > .ql-editor").html());
		};
	}
	if(name==""){
		app.alert({title:'Choisissez un nom pour cet item.',type:'confirm'});
		$('.item_save_btn').button('reset');
		return;
	}
	var items=[{
		item_name:name,
		item_value_max:value,
		item_mode:item_mode,
		item_cycle:cycle,
		item_vue:item_vue,
		item_colors:JSON.stringify(colors),
		item_color:item_color,
		item_descriptions:JSON.stringify(descriptions),	
		item_tags:tags,
		item_categorie:categorie,
		item_sous_categorie:sous_categorie
	}];
	$.post(app.serveur + "index.php?go=items&q=add"+app.connexionParam,
	{
		items:JSON.stringify(items),
		time:Math.floor(app.myTime()/1000),
		sessionParams:app.sessionParams
	}, function(data) {
		$('.item_save_btn').button('reset');
		app.render(data);   
		app.renderItems(); 
		app.itemsProgressionsInit();
		app.itemsSelectionSet();
		app.itemsFormClose();
	}
	);
}
app.updateItem=function(selection){	
	if (!app.checkConnection()) {return;}
	var colors=[],
	value=document.getElementById('item_value_max').value,
	item_color="null",descriptions=[],
	item_mode=false,
	item_vue=false,
	item_cycle=-1,
	name="null",
	tags="",
	categorie="null",
	sous_categorie="null";
	if($('#item_name').prop('disabled')==false){
		name=document.getElementById('item_name').value;
		if(name==""){
			app.alert({title:'Choisissez un nom pour cet item.',type:'confirm'});
			$('.item_save_btn').button('reset');
			return;
		}
	}
	if($('#item_color').prop('disabled')!=true){
		item_color=document.getElementById('item_color').value;
	}
	if($('#item_categorie').prop('disabled')!=true){
		categorie=document.getElementById('item_categorie').value;
	}
	if($('#item_sous_categorie').prop('disabled')!=true){
		sous_categorie=document.getElementById('item_sous_categorie').value;
	}
	if($('#item_tags').prop('disabled')!=true){
		tags=arrayUnique(app.itemEditionTags.map(function(value){return ucfirst(value.trim());})).sort().join(', ');
	}
	if($('#item_cycle').prop('disabled')!=true){
		item_cycle=document.getElementById('item_cycle').value;
	}
	if($('#item-vue-cursor').prop('disabled')!=true || $('#item-vue-checkbox').prop('disabled')!=true){
		item_mode=app.itemsMode;
		item_vue=app.itemsVue;
		if(app.itemsVue=="checkbox"){
			colors=['#009900'];
			descriptions=[$("#item-description-0-editeur > .ql-editor").html()];
			item_mode='libre';
			value=1;
		}else{
			for (var i = 0; i<=value; i++) {
				colors.push(document.getElementById('item-color-'+i+'').value);
				descriptions.push($("#item-description-"+i+"-editeur > .ql-editor").html());
			};
		}
	}	
	//Préparation de la liste des items
	var id=[];
	if(app.itemsToEdit){
		id.push(app.itemsToEdit['item_id']);
	}
	else if(selection){
		id=app.itemsSelection;
	}
	$.post(app.serveur + "index.php?go=items&q=updateItems"+app.connexionParam,
	{
		item_name:name,
		item_id:JSON.stringify(id),
		item_value_max:value,
		item_mode:item_mode,
		item_cycle:item_cycle,
		item_vue:item_vue,
		item_colors:JSON.stringify(colors),
		item_color:item_color,
		item_descriptions:JSON.stringify(descriptions),	
		item_tags:tags,
		item_categorie:categorie,
		item_sous_categorie:sous_categorie,		
		time:Math.floor(app.myTime()/1000),
		selection:selection,
		tagsMode:app.itemsEditionTagsMode,
		sessionParams:app.sessionParams
	}, function(data) {
		$('.item_save_btn').button('reset');
		app.render(data);   
		app.renderItems(); 
		app.itemsProgressionsInit();
		app.itemsSelectionSet();
		app.itemsSelectedUpdate();
		app.itemsSetFilters();
		if(app.itemsToEdit || selection){	
			app.itemsFormClose();
		}	
	}
	);
}
app.renderEditItem=function(item_id){
	if (!app.checkConnection()) {return;}
	document.getElementById('item_form').style.display = "block";
	$('.item_save_btn').off('click').button('reset');	
	app.setItemsTagsDatalist();
	app.itemEditionTags=[];
	if(item_id=="selection"){
		app.itemsToEdit=null;
		document.getElementById('item-form-titre').innerHTML = "Édition de la sélection ("+app.itemsSelection.length+" item"+app.pluralize(app.itemsSelection.length,'s')+")";	
		document.getElementById('item-form-delete').innerHTML='<button id="item-form-delete-btn" class="btn btn-danger" data-loading-text="Suppression..." onclick="app.delItem(\'selection\');$(this).button(\'loading\');"><span class="glyphicon glyphicon-trash"></span> Supprimer</button>';
		document.getElementById('items-edition-tags-mode').style.display='';
		$("#item_name").prop("disabled", true).prop('value','');
		$("#item_tags").prop("disabled", true).prop('value','');
		$("#item_cycle").prop("disabled", true).prop('value',-1);
		$("#item_categorie").prop("disabled", true).prop('value','');
		$("#item_sous_categorie").prop("disabled", true).prop('value','');
		$("#item_color").prop("disabled", true).prop('value',"#E3E3E3");
		//Si les items ont des modes différents
		$('#item-vue-cursor').prop("disabled", true);
		$('#item-vue-checkbox').prop("disabled", true).prop('checked', true);
		app.itemsVueChange('checkbox');
		document.getElementById('items-edit-tags-add').checked=true;
		app.itemsEditionTagsMode="add";
		$('.item_save_btn').click(function(){app.updateItem('selection'); return false; });
		$(".items-edition-input>div").show();
		$(".items-edition-input>div").click(function (evt) {
			$(this).hide().prev("input[disabled]").prop("disabled", false).focus();
			$(this).hide().prev("select[disabled]").prop("disabled", false).focus();
		});
		app.itemsEditionTagsListRender(app.itemEditionTags);
	}
	else{
		app.itemsToEdit=app.getItemById(item_id);
		var tags=app.itemsToEdit['item_tags'];
		var tags_split=tags.split(',');
		var new_tags=[];
		for (var i = tags_split.length - 1; i >= 0; i--) {
			var tag=tags_split[i].trim();
			if(tag.length!=0){
				new_tags.push(tag);
			}
		}
		app.itemEditionTags=new_tags;
		app.itemsEditionTagsMode="replace";
		$(".items-edition-input>div").hide().prev("input[disabled]").prop("disabled", false);
		app.itemsToEdit.scrollY=window.scrollY;
		$('.items-edit-selection').css('display','');
		document.getElementById('item-form-titre').innerHTML = "Édition d'un item";	
		document.getElementById('item_name').value=app.itemsToEdit['item_name'];
		document.getElementById('item_color').value=app.itemsToEdit['item_color'];
		document.getElementById('item_cycle').value=app.itemsToEdit['item_cycle'];
		document.getElementById('item_categorie').value=app.itemsToEdit['item_categorie'];
		document.getElementById('item_sous_categorie').value=app.itemsToEdit['item_sous_categorie'];
		document.getElementById('item_value_max').value=app.itemsToEdit['item_value_max'];	
		$('#item-vue-'+app.itemsToEdit['item_vue']).prop('checked', true);
		document.getElementById('item-form-delete').innerHTML='<button id="item-form-delete-btn" class="btn btn-danger" data-loading-text="Suppression..." onclick="app.delItem('+item_id+');$(this).button(\'loading\');"><span class="glyphicon glyphicon-trash"></span> Supprimer</button>';
		$('.item_save_btn').click(function() {app.updateItem(); return false; });
		app.itemsVueChange(app.itemsToEdit['item_vue']);
		app.itemsEditionTagsListRender();
	}
}
app.renderCloneItem=function(item_id){
	app.renderEditItem(item_id);
	app.itemsToEdit=null;
	document.getElementById('item-form-titre').innerHTML = "Création d'un item";
	$('.item_save_btn').click(function() {app.addItem(); return false; });
}
app.itemsEditionTagsListRender=function(tags){
	tags=tags||app.itemEditionTags;
	tags.sort();
	var html=[];
	for (var i =0,lng= tags.length; i<lng; i++) {
		html.push('<span class="item-edition-tags">');
		html.push(tags[i]);
		html.push('<span class="btn btn-default btn-xs" onclick="app.itemsEditionTagRemove(\''+tags[i].replace(/'/g, "\\'")+'\');">');
		html.push('<span class="glyphicon glyphicon-remove">');	
		html.push('</span>');
		html.push('</span>');
		html.push('</span>');
	}
	document.getElementById('items-tags-list').innerHTML=html.join('');
}
app.itemsEditionTagAdd=function(){
	app.itemEditionTagsCurrent=[];
	if(app.itemEditionTags!=[]){
		app.itemEditionTagsCurrent=clone(app.itemEditionTags);
	}
	var tags=document.getElementById('item_tags').value;
	var tags_split=tags.split(',');
	for (var i = tags_split.length - 1; i >= 0; i--) {
		var tag=tags_split[i].trim();
		if(tag.length!=0 && app.itemEditionTagsCurrent.indexOf(tag)<0){
			app.itemEditionTagsCurrent.push(tag);
		}
	}
	app.itemsEditionTagsListRender(app.itemEditionTagsCurrent);
}
app.itemsEditionTagRemove=function(toDelete){
	app.itemEditionTags.splice(app.itemEditionTags.indexOf(toDelete), 1);
	app.itemsEditionTagsListRender(app.itemEditionTags);
}
app.delItem=function(item_id,confirm){
	if (!app.checkConnection()) {return;}
	var items=[];
	if(item_id=="selection" && app.itemsSelection.length==1){
		item_id=app.itemsSelection[0];
	}
	if(item_id=="selection"){
		if(!confirm){
			app.alert({title:'Supprimer ces items et les évaluations associées ?',type:'confirm'},function(){app.delItem('selection',true);},function(){$('#item-form-delete-btn').button('reset');});
			return;
		}
		items=app.itemsSelection;
	}else{
		if(!confirm){
			app.alert({title:'Supprimer cet item et les évaluations associées ?',type:'confirm'},function(){app.delItem(item_id,true);},function(){$('#item-form-delete-btn').button('reset');});
			return;
		}
		items.push(item_id);
	}
	$.post(app.serveur + "index.php?go=items&q=del"+app.connexionParam,
	{
		items:JSON.stringify(items),
		sessionParams:app.sessionParams
	}, function(data) {
		app.render(data);   
		app.renderItems(); 
		app.itemsProgressionsInit();
		app.itemsFormClose();
		app.itemsSelectionSet();
		app.itemsSelectedUpdate();
		app.itemsSetFilters();		
	}
	);
}
app.setItemsTagsDatalist=function(){ 
	var tags=document.getElementById('item_tags').value;
	var tags_split=tags.split(',');
	var tags_tab=[];
	for (var i =0, lng= tags_split.length - 1; i < lng; i++) {
		tags_tab.push(tags_split[i].trim());
	};
	var new_tags=[];
	if(tags_split.length<=1){new_tags=app.itemsTagsDatalist;}
	else{
		for (var i = app.itemsTagsDatalist.length - 1; i >= 0; i--) {
			if(tags_tab.indexOf(app.itemsTagsDatalist[i])>=0){continue;}
			new_tags.push(tags_tab.concat(app.itemsTagsDatalist[i]).join(', '));
		};
	}
	var options='';
	for(var i = 0,lng=new_tags.length; i < lng; i++){
		options += '<option value="'+new_tags[i]+'" />';
	}
	document.getElementById('list_item_tags').innerHTML = options;
}
app.buildProgressionsDatalist=function(){
	var activites=[];
	for (var i = app.itemsProgressions.length - 1; i >= 0; i--) {
		var progression=app.itemsProgressions[i];
		if((progression['rei_user']!=app.userConfig.userID && !app.userConfig.ppView)
			||(app.itemsGetProgressionValue(progression.rei_id)<0)
			||(progression['rei_user_type']=="eleve")
			||(!progression.rei_activite || progression.rei_activite=="")){
			continue;
	}
	activites=activites.concat(progression.rei_activite);				
};
app.progressionActivitesDatalist=arrayUnique(activites.map(function(value){if(value){return value.trim();}})).sort();
var options=[];
for(var i = 0,lng=app.progressionActivitesDatalist.length; i < lng; i++){
	options.push('<option value="'+app.progressionActivitesDatalist[i]+'">'+app.progressionActivitesDatalist[i]+'</option>');
}
document.getElementById('list_progressions_activites').innerHTML = options.join('');

}
app.itemsEditionTagsModeSet=function(mode){
	app.itemsEditionTagsMode=mode;
}
app.itemsVueChange=function(vue){
	app.itemsVue=vue;
	$('#item-vue-'+vue).prop('checked', true);
	if(vue=="checkbox"){
		app.previewItemsSlider(0);
		$(".items-cursor-mode").css('display','none');
	}
	else{
		$(".items-cursor-mode").css('display','');		
		app.previewItemsSlider(document.getElementById('item_value_max').value);
	}
}

