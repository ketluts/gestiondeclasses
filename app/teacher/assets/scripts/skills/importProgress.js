/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.itemsResultsImportShow=function(){
	app.viewToggle('items_results_import');
	$('#items-results-import-save-btn').css('display','none');
	document.getElementById('items-results-import-tableur').value="";
	$('#items-results-import-selection').html('');
	document.getElementById('items-results-import-apercu').innerHTML="";
}
app.itemsResultsImportInit=function(){
	var data=document.getElementById('items-results-import-tableur').value;
	var lignes=data.split('\n');
	app.itemsResultsImport=[];
	var n=0;
	for (var i = lignes.length - 1; i >= 0; i--) {
		var ligne=lignes[i].split('\t');
		if(ligne.length>n){
			n=ligne.length;
		}
		if(ligne[0]){
			var eleve=app.getEleveById(ligne[0]);
			if(eleve){
				app.itemsResultsImport.push(ligne);
				ligne[0]=eleve;
			}
		}
	};
	app.itemsResultsImportNb=n-1;
	app.itemsResultsImportSelection();
	app.itemsResultsImportApercu();
	$('#items-results-import-save-btn').css('display','block').button('reset');
}
app.itemsResultsImportApercu=function(){
	var html=[];
	html.push('<hr/>');
	html.push('<table id="itemsResultsImportTable">');
	html.push('<thead>');
	html.push('<tr>'); 
	html.push("<th class='flex-1'>Élève</th>"); 
	for (var i = 0; i<app.itemsResultsImportNb; i++) {
		html.push("<th class='flex-1 text-center'>#"+(i+1)+"</th>"); 
	}
	html.push('</tr>');
	html.push('</thead>');  
	for (var i = app.itemsResultsImport.length - 1; i >= 0; i--) {
		var ligne=app.itemsResultsImport[i];
		if(ligne.length==0){continue;}
		html.push('<tr>');
		for (var j =0,lng= ligne.length ; j<lng; j++) {
			var value=ligne[j];
			if(!value && j==0){
				continue;
			}
			if(!value){
				value=-1;
			}
			if(j==0){
				html.push('<td>');
				html.push(app.renderEleveNom(value));
				html.push('</td>');
			}
			else{
				html.push("<td class='text-center'>");
				var item_id=document.getElementById('items_selection_'+(j-1)+'').value;
				if(item_id==-1){
					html.push(value);	
				}
				else{
					var item=app.getItemById(item_id);
					var color=app.getColorAffine(value*1,item['item_colors']);
					html.push('<span class="items-box" style="background-color:'+color+';"></span>');	
				}				
				html.push('</td>');
			}
		}
		html.push('</tr>');
	}	
	html.push('</table>');
	document.getElementById('items-results-import-apercu').innerHTML=html.join('');
	$('#itemsResultsImportTable').dataTable({ 
		"language":app.datatableFR,
		"paging": false,
		dom: 't',
	});
	$('#itemsResultsImportTable').addClass('table table-striped ');

	$('#items-results-import-tableur-btn').button('reset');
}
app.itemsResultsImportSelection=function(){
	//CONSTRUCTION DE LA LISTE DES ITEMS
	var items_html=[];
	items_html.push('<option value="-1">');
	items_html.push("Choisir l'item évalué");
	items_html.push('</option>');
	for (var i =0, lng=app.items.length; i<lng; i++) {
		var item=app.items[i];		
		if(!item.isVisible){
			continue;
		}
		items_html.push('<option value="'+item.item_id+'">');
		items_html.push(item.item_name);
		items_html.push('</option>');
	}
	var html=[];
	html.push('<hr/>');
	for (var i = 0; i<app.itemsResultsImportNb; i++) {
		html.push('<div>');
		html.push('#'+(i+1));
		html.push('<select class="form-control" id="items_selection_'+i+'" onchange="app.itemsResultsImportApercu();">');
		html.push(items_html.join(''));
		html.push('</select>');
		html.push('</div>');
	}
	$('#items-results-import-selection').html(html.join('')).goTo();
}
app.itemsResultsImportSave=function(confirm){	
	if (!app.checkConnection()) {
		return;
	}
	$('#items-results-import-save-btn').button('loading');
	app.itemsResultsImportRequetesNb=0;
	var items=[];
	var itemsIdsTab=[];
	//On prepare la liste des items à mettre à jour
	for (var i = 0; i<app.itemsResultsImportNb; i++) {
		var item_id=document.getElementById('items_selection_'+i+'').value;
		if(item_id==-1){
			continue;
		}
		var index=itemsIdsTab.indexOf(item_id);
		if(index<0){
			var item=app.getItemById(item_id);
			items.push({
				item_id:item_id,
				item_vue:item.item_vue,
				liste:[],
				requetes:[],
				requetesValues:[]
			});
			itemsIdsTab.push(item_id);
		}
	}
	if(items.length==0){
		app.alert({title:'Il faut sélectionner les items évalués.'});
		$('#items-results-import-selection').goTo();
		$('#items-results-import-save-btn').button('reset');		
		return;
	}
	if(!confirm){
		app.alert({title:'Enregistrer tous ces résultats ?',type:'confirm'},function(){app.itemsResultsImportSave(true);});
		return;		
	}
	//On range les résultats par items	
	for (var i = app.itemsResultsImport.length - 1; i >= 0; i--) {
		var ligne=app.itemsResultsImport[i];
		if(ligne.length==0){continue;}
		for (var j =1,lng= ligne.length ; j<lng; j++) {
			var value=ligne[j];
			var item_id=document.getElementById('items_selection_'+(j-1)+'').value;
			if(item_id==-1){
				continue;
			}
			var index=itemsIdsTab.indexOf(item_id);
			if(items[index].item_vue=="checkbox" && value>0){
				value=1;
			}
			items[index].liste.push({
				eleve_id:ligne[0].eleve_id,
				value:value
			});
		}		
	}	
	//On prepare les requetes par items
	for (var i = items.length - 1; i >= 0; i--) {
		var item=items[i];
		for (var j = item.liste.length - 1; j >= 0; j--) {
			var result=item.liste[j];
			if(result.value==-1 || !result.value){
				continue;
			}
			var index=item.requetesValues.indexOf(result.value);
			if(index<0){
				item.requetes.push({
					value:result.value,
					eleves:[result.eleve_id]
				});
				item.requetesValues.push(result.value);
			}
			else{
				item.requetes[index].eleves.push(result.eleve_id);
			}
		}
	}
	//On envoie les requetes
	//app.skillsRenderReset();
	var n=0;
	for (var i = items.length - 1; i >= 0; i--) {
		var item=items[i];
		for (var j = item.requetes.length - 1; j >= 0; j--) {
			var requete=item.requetes[j];
			app.itemsResultsImportRequetesNb++;
			n++;
			$.post(app.serveur + "index.php?go=items&q=addProgression"+app.connexionParam,
			{
				items:JSON.stringify([item.item_id]),
				eleves:JSON.stringify(requete.eleves),
				progression:requete.value,
				activite:"",
				time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
			}, function(data) {
				app.render(data);
				app.itemsResultsImportRequetesNb--
				if(app.itemsResultsImportRequetesNb==0){
					$('#items-results-import-save-btn').button('reset');
					app.hide('items_results_import');
				}							
			}			
			);			
		}
	}
	if(n==0){
		if(app.itemsResultsImport.length>0){	
			app.alert({title:'Il n\'y a aucun résultat à enregistrer.'});		
		}else{
			app.alert({title:'Il faut copier/coller les résultats depuis un tableur.'});
			$('#items-results-import-tableur').goTo();
		}
		$('#items-results-import-save-btn').button('reset');		
	}
}