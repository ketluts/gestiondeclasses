/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.itemsRenderStudentCycle=function(eleve){
	var html=[];
	if(eleve.eleve_cycle && eleve.eleve_cycle!="false"){
//html.push('<br/>');
html.push('<span class="items-student-cycle">');

html.push(" Cycle "+eleve.eleve_cycle[0]);
if(eleve.eleve_cycle[1]=="D"){
	html.push(" - Début");
}else if(eleve.eleve_cycle[1]=="M"){
	html.push(" - Milieu");
}else{
	html.push(" - Fin");
}

html.push('</span>');
}
return html.join('');
}
app.itemsStudentGraphToggleView=function(enable){
	if(enable===false || (app.itemsStudentGraph && enable!==true)){
		app.itemsStudentGraph=false;
		app.hide('items-student-graph-block');
		return;
	}
	app.itemsStudentGraph=true;
	app.show('items-student-graph-block');
	$('#app').goTo();
}
app.itemsStudentActivitiesToggleView=function(){
	var display=$("#items-activity").css('display');
	if(display=='block'){
		app.hide('items-activity');
	}else{
		app.show('items-activity');
		$('#app').goTo();
	}
}
