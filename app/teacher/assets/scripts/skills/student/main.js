/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
//##############
//ITEMS ELEVE VIEW
//##############
app.itemsRenderProgressionByEleve=function(eleve_id){
	var eleve=app.getEleveById(eleve_id);	
	document.getElementById('titre').innerHTML = "<a href='#student/"+eleve_id+"' class='btn btn-default'>"+app.renderEleveNom(eleve)+app.itemsRenderStudentCycle(eleve)+"</a>";	
	$("#items-titre-bloc").css("display","");
	if(app.itemsCurrentEleve!=eleve_id){
		$('.item_details').css('display','none');
		app.itemsCurrentEleve=eleve_id;	
	}	
	$('.item-toolbar-right').css("display","none");	
	$('.item-all-view').css("display","none");	
	$('.item-classe-view').css("display","none");	
	$('.item-eleve-view').css("display","");

	$(".itemsEleve").removeClass('itemsEleveSelected');
	$('.items_eleve_'+eleve_id+'').addClass('itemsEleveSelected');
	$(".itemsClasse").removeClass('itemsClasseSelected');
	$(".item_view").css("display","block").addClass('btn-default').removeClass('btn-primary').attr('data-value',false);
	$(".categories_progression").html('');
	$('.item_checkbox_details').html('');
	$('.item_description').css('display','none');

	app.itemsStudentsProgressionsBuild([eleve_id]);
	
	app.itemsStudentGraphToggleView(app.itemsStudentGraph);

	//Affichage des résultats sur les slides par categorie et par item
	for (var c =0,lng=app.itemsCategories.length; c <lng; c++) {
		var categorie=app.itemsCategories[c];
		for (var i =0, llng=categorie['sous_categories'].length;i<llng; i++) {
			var sous_categorie=categorie['sous_categories'][i];
			for (var j = 0,lllng=sous_categorie['list'].length ; j<lllng; j++) {
				var item=sous_categorie['list'][j];	
				app.itemsRenderProgressionByItemAndEleve(item.item_id);
			}
		}
	}
	$( ".itemSlider" ).slider((app.checkConnection())?"enable":"disable");
	app.itemsSetFilters();	

	$(document.body).trigger("sticky_kit:recalc");
	app.itemsStudentActivitiesRender(app.itemsStudentActivities);
};


app.itemsStudentGetProgressionByTags=function(eleve_id){
	var eleve=app.getEleveById(eleve_id);
	var tags=[];
	var data=[];
	var data_progression=[];
	var data_id=[];
	for (var i = app.items.length - 1; i >= 0; i--) {
		var item=app.items[i];	
			if(!app.itemsIsVisible(item)){
				continue;
			}
		var progression='-';

		if(eleve.progression[item.item_id]){
			progression=Math.min(eleve.progression[item.item_id]['value']*100/item.item_value_max,100);	
		}

		var tags=[];
		if(app.userConfig.itemsSyntheseMode=="tags"){
			if(app.itemsCurrentFilter.tags.include.length>0){
				tags=item.item_tags.split(',').map(function(value){if(value){return value.trim();}});
				tags.push(item.item_categorie);
				tags.push(item.item_sous_categorie);
			}
		}else if(app.userConfig.itemsSyntheseMode=="categories"){
			tags.push(item.item_categorie);
		}
		else{
			tags.push(item.item_sous_categorie);	
		}
		tags=arrayUnique(tags);

		for (var j = tags.length - 1; j >= 0; j--) {
			var tag=tags[j];
			if(app.userConfig.itemsSyntheseMode=="tags"){
				if(app.itemsCurrentFilter.tags.include.indexOf(tag)<0 && app.itemsCurrentFilter.tags.include.length>0){continue;}
			}
			var index=data_id.indexOf(tag);
			if(index<0){
				var nData={
					name:tag,
					liste:[]
				};
				if(progression!="-"){
					nData.liste.push(progression);
				}
				data_progression.push(nData);
				data_id.push(tag);					
			}
			else{
				if(progression!="-"){
					data_progression[index].liste.push(progression);	
				}				
			}
		}
	}

	data_progression=app.orderBy(data_progression,'name','DESC');
	tags=[];
	for (var i = data_progression.length- 1; i >= 0; i--) {		
		var lng=data_progression[i].liste.length;
		var value=-1;
		if(lng!=0){
			value=Math.round(100*data_progression[i].liste.reduce(app.add,0)/lng)/100;
		}
		tags.push(data_progression[i].name);
		data.push(value);
	}
	return {tags:tags,values:data};
}