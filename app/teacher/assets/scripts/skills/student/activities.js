/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.studentGetTeamMate=function(eleve_id){
	var teammate=[];
	for (var i = app.classes.length - 3; i >= 0; i--) {
		var classe=app.classes[i];
		if(classe.eleves.indexOf(eleve_id)>=0){
			teammate=teammate.concat(classe.eleves);
		}
	}
	return arrayUnique(teammate);
}
app.itemsStudentActivitiesRender=function(){
	var activites=[];
	var teammate=app.studentGetTeamMate(app.itemsCurrentEleve);
	var activitesNames=[];
	for (var i = app.itemsProgressions.length - 1; i >= 0; i--) {
		var progression=app.itemsProgressions[i];
		if(progression['rei_user_type']=="user" && progression['rei_user']!=app.userConfig.userID && app.userConfig.itemsAvisMode=="me"){
			continue;
		}		
		if(teammate.indexOf(progression['rei_eleve'])<0){
			continue;
		};

				//Si la valeur vaut -1, cela signifie que l'item est visible pour l'élève et on en tient pas compte dans le calcul
				//Si la valeur vaut -2, cela signifie que l'item est visible et peut s'auto évaluer et on en tient pas compte dans le calcul
				if(app.itemsGetProgressionValue(progression.rei_id)=='-1' 
					|| app.itemsGetProgressionValue(progression.rei_id)=='-2'
					|| (app.userConfig.itemsAvisMode!="students" && progression['rei_user_type']=="eleve") 
					|| (app.userConfig.itemsAvisMode=="students" && progression['rei_user_type']=="user")){
					continue;
			}
			if(progression.rei_date<app.itemFiltersDateStart || progression.rei_date>app.itemFiltersDateEnd){
				continue;
			}
			var item=app.getItemById(progression['rei_item']);
			if(item.item_vue!="cursor"){
				progression['rei_value']=Math.max(progression['rei_value'],1);
			}	
			if(!progression['rei_activite'] || progression['rei_activite']==""){
				progression.rei_activite="Activité non précisée";
			}
			if(!activites[progression['rei_activite']]){
				activites[progression['rei_activite']]={
					value:0,
					nb:0,
					liste:[]
				};
				activitesNames.push(progression['rei_activite']);
			}
			if(progression['rei_eleve']==app.itemsCurrentEleve){
			//#######################
			var pColor="";			
			if(item['item_vue']=="checkbox"){
				pColor="#34b93d";
			}else{
				if(item['item_colors']){
					pColor=app.getColorAffine(progression.rei_value,item['item_colors']);
				}
				else{
					pColor=app.colorByMoyenne((progression.rei_value/max)*20);
				}	
			}
			progression.color=pColor;
						
			//#######################
			progression.skillStr=item['item_name'].substring(0,3);
			//#######################
			progression.rei_symbole=progression.rei_symbole||"square";
			//#######################
			activites[progression['rei_activite']]['nb']++;
			activites[progression['rei_activite']]['liste'].push(progression);
		}					
	};

	var html=[];
	html.push('<table class="table" id="items-activity-table">');
	html.push('<thead>');
	html.push('<tr>');  html.push("<th>Activités</th>"); 
	html.push('<th>Acquis</th>');
	html.push('</tr>');
	html.push('</thead>');    
	html.push('<tbody>');
	for (var i = activitesNames.length - 1; i >= 0; i--) {
		var activiteName= activitesNames[i];
		html.push('<tr>');
		html.push('<td>');
		html.push('<span class="btn btn-default" onclick="app.skillsActivitiesAutoFilter(\''+hex_md5(activiteName)+'\');">');
		html.push(activiteName);
		html.push('</span>');
		html.push('</td>');
		html.push('<td>');
		activites[activiteName].liste.sort(app.skillsActivitiesProgressionsSort);
		for (var j = activites[activiteName].liste.length - 1; j >= 0; j--) {
			var progression=activites[activiteName].liste[j];
			var title=app.getItemById(progression.rei_item).item_name;
			html.push('<span class="skills-activite-progression">');
			html.push(progression.skillStr);
			html.push('<span class="items-'+progression.rei_symbole+'" title="'+title+'" style="color:'+progression.color+';"></span>');
			html.push('</span>');
		}
		html.push('</td>');  
		html.push('</tr>');
	};
	html.push('</tbody>');
	html.push("</table>");
	html.push("<hr/>");
	document.getElementById("items-activity").innerHTML=html.join('');

	$('#items-activity-table').DataTable({
		stateSave: true,
		language:app.datatableFR,
		 paging: false,
		dom: 't',
		"order":[[0,'asc']]
	});



}
app.skillsActivitiesProgressionsSort=function(a,b){

	var aDate=moment.unix(a.rei_date);
	var aDateStr=aDate.month()+''+aDate.year();
	var bDate=moment.unix(b.rei_date);
	var bDateStr=bDate.month()+''+bDate.year();			

	if(aDateStr!=bDateStr){
		return b.rei_date-a.rei_date;
	}
	else{
		return (b.skillStr).localeCompare(a.skillStr);
	}

}

app.skillsActivitiesAutoFilter=function(activity){
	//var items=[];
	app.itemsUnselectAll();
var activityName="";
	for (var i = app.itemsProgressions.length - 1; i >= 0; i--) {
		var progression=app.itemsProgressions[i];
		if(progression['rei_user_type']=="user" && progression['rei_user']!=app.userConfig.userID  && app.userConfig.itemsAvisMode=="me"){
			continue;
		}
		if(app.itemsGetProgressionValue(progression.rei_id)=='-1' 
			|| app.itemsGetProgressionValue(progression.rei_id)=='-2'
			|| (app.userConfig.itemsAvisMode!="students" && progression['rei_user_type']=="eleve") 
			|| (app.userConfig.itemsAvisMode=="students" && progression['rei_user_type']=="user")){
			continue;
	}
	if(hex_md5(progression.rei_activite)!=activity){
		continue;
	}

activityName=progression.rei_activite;
var item=app.getItemById(progression.rei_item);
document.getElementById('item_'+item['item_id']+'_checkbox').checked=true;



}

document.getElementById('cpt-eval-activite').value=activityName;

app.itemsSelectedUpdate();	
app.itemsFilteredBySelectionToggleStatue();


}