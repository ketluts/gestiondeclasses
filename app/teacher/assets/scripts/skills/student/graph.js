/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.itemsStudentSkillsGraph=function(){
	var progressionsByTag=app.itemsStudentGetProgressionByTags(app.itemsCurrentEleve);
	var tags=progressionsByTag.tags;
	var data=progressionsByTag.values;

	$('#items-student-diagramme').highcharts({
		chart: {
			//polar: true,
			type: 'bar'
		},

		title: {
			text: '',
			style:"display:none"
		},

		pane: {
			size: '80%'
		},

		xAxis: {
			categories: tags//,
			//tickmarkPlacement: 'on',
			//lineWidth: 0
		},

		yAxis: {
			//gridLineInterpolation: 'polygon',
			lineWidth: 0,
			min: 0,
			max:100,
			plotLines: [{
				value: 25,
				color: 'black',
				width: 1,
				zIndex:10
			},
			{
				value: 50,
				color: 'black',
				width: 1,
				zIndex:10
			},
			{
				value: 75,
				color: 'black',
				width: 1,
				zIndex:10
			}]
		},
		series: [{
			name: 'Niveau de maîtrise',
			data: data//,
			//pointPlacement: 'on'
		}
		]
	});
}