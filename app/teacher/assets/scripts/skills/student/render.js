/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.itemsRenderProgressionByItemAndEleve=function(item_id){
	var item=app.getItemById(item_id);
	var eleve=app.getEleveById(app.itemsCurrentEleve);
	//Affichage des boutons d'édition
	item['item_progression']=null;
	item['item_progression_in_percent']="-";	
	if(item['item_user']==app.userConfig.userID){
		document.getElementById('item_view_'+item['item_id']+'').innerHTML='<span class="glyphicon glyphicon glyphicon-eye-open"></span> ';
		document.getElementById('item_view_'+item['item_id']+'_edit').innerHTML='<span class="glyphicon glyphicon glyphicon-pencil"></span> ';
		app.itemsToolbarButtonsSet(item['item_id']);
	}else{
		if(!app.userConfig.ppView){
			return;
		}
	}		
	var max=item['item_value_max'];	
	var progression=app.itemsStudentProgressionByItem(eleve.eleve_id,item['item_id']);
	item['item_progression']=progression['value']*100/max;
	item['item_progression_in_percent']=progression['string'];
	var nb_evaluations=progression['nb_evaluations'];
	if(item['item_vue']=="cursor"){
		//Curseur
		var step=0.01;
		var min=0;
		var slideValue=progression.slideValue;
		$("#item_"+item['item_id']+"_slider .ui-slider-handle").unbind('keydown'); 
		var slide=$("#item_"+item['item_id']+"_slider")
		.slider( "option", "min", min)
		.slider("option","max", max)
		.slider("option",'step',step)
		.off('slidechange')
		.off('slide')
		.slider("value",slideValue)
		.on( "slidechange",app.itemsProgressionAdd)
		.on( "slide",app.itemsProgressionSlideStyleRender);
		var style=app.itemsProgressionStyleGet(progression['value'],item['item_id']);
		$('#item_'+item['item_id']+'_slider_style').css('width',Math.floor(slideValue*100/max)+"%");				
		$('#item_'+item['item_id']+'_slider_style').css('background-color', style.color); 
		if(style.description!="<p><br></p>"){
			$('#item_'+item['item_id']+'_current_description').css('display','block').html("<span class='h4'><small>"+style.description+"</small></span>");
		}					
	}
	else{
		//Checkbox
		if(progression['value']>0.5){
			document.getElementById('item_'+item['item_id']+'_checkbox_mode').checked="checked";
		}else{
			document.getElementById('item_'+item['item_id']+'_checkbox_mode').checked="";
		}
		$('#item_'+item['item_id']+'_checkbox_mode').prop("disabled", false);
	}
	var html=[];
	html.push("<div class='btn btn-default btn-sm' style='position:relative;' title='"+nb_evaluations+" évaluation"+app.pluralize(nb_evaluations,"s")+"' onclick=\"app.itemsRenderDetails("+item['item_id']+");\"><span class='glyphicon glyphicon-th-list'></span>");
	html.push("<span class='badge badge_items'>"+nb_evaluations+"</span>");
	html.push("</div>");
	document.getElementById('item_'+item['item_id']+'_btn').innerHTML=html.join('');

	if(nb_evaluations>0){
		$('#item_'+item['item_id']+'_btn').css('visibility','visible');
	}else{
		$('#item_'+item['item_id']+'_btn').css('visibility','hidden');
	}

if(document.getElementById('item_'+item_id+'_details_block').style.display=="block"){
			app.itemsRenderDetails(item_id,true);
		}

}


app.itemsStudentProgressionByItem=function(eleve_id,item_id){
	var eleve=app.getEleveById(eleve_id);	
	var item=app.getItemById(item_id);
	var mode=item['item_mode'];
	var max=item['item_value_max'];
	var value=-1;
	var value_in_percent="-";
	var n=0;	
	if(eleve.progression[item_id]){
		n=eleve.progression[item_id]['nb'];
		value=eleve.progression[item_id]['value'];
		value_in_percent=Math.min(value*100/max,100);	
	}
	var slideValue=value;
	if(n!=0){
		slideValue=Math.max(value,max*0.08);
	}
	return {
		"value":Math.min(value,max),
		"slideValue":Math.min(slideValue,max),
		"string":value_in_percent,
		'nb_evaluations':n
	};
}