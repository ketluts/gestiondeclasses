/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.skillsItemsImportInit=function(){
	var input =document.getElementById('skillsItemsImportFile');
	var extension = input.files[0].name.split('.').pop().toLowerCase();
	if(extension=="json"){
		app.skillsItemsImportJSON(input);
	}
}
app.skillsItemsImportJSON=function(input){
	var reader = new FileReader();
	reader.addEventListener('load', function() {
		var items_import = jsonParse(reader.result); 
		var items=[];
		//On prépare la liste des items
		for (var i = items_import.length - 1; i >= 0; i--) {
			var item=items_import[i];
			if(
				!item.item_name || 
				!item.item_value_max || 
				!item.item_color || 
				!item.item_categorie || 
				!item.item_mode || 
				!item.item_vue || 
				!item.item_colors ||
				!item.item_descriptions
				)
			{
				continue;
			}
			items.push(item);
		}
		app.skillsItemsImport(items);
	});
	reader.readAsText(input.files[0]);
}
app.skillsItemsImport=function(items,confirm){
	if(!app.checkConnection()){
		return false;
	}	
	var n=items.length;
	if(!n){
		return false;
	}
	
	if(!confirm){
		app.alert({title:'Importer ce'+app.pluralize(n,'s '+n,'t')+' item'+app.pluralize(n,'s')+' ?',type:'confirm'},function(){app.skillsItemsImport(items,true);});
		return false;
	}
	document.getElementById('items-liste').innerHTML=app.renderLoader();
	app.itemsFormClose();
	app.itemsImport=true;
	$.post(app.serveur + "index.php?go=items&q=add"+app.connexionParam,
	{
		items:JSON.stringify(items),
		time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
	}, function(data) {
		$('.item_save_btn').button('reset');
		app.itemsImport=false;
		app.render(data);   
		app.renderItems(); 
		app.itemsProgressionsInit();		
	}
	);
}