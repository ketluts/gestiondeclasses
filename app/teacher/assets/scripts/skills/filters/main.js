/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
//##############
//ITEMS FILTERS
//##############
app.getFilterById=function(filter_id){
	  var filters=app.filters;
  for (var i = 0, lng = filters.length; i < lng; i++) {
    if (filters[i].filter_id == filter_id) {
     filters[i].num=i;     
      return filters[i];
    }
  }
  return false;
}
app.itemsSetFilters=function(){
	$('.item-box').css('display','');
	var k=0;
	for (var c =0,llng= app.itemsCategories.length; c<llng; c++) {
		var categorie=app.itemsCategories[c];
		var is_categorie_visible=false;
		for (var i =0, lng=categorie['sous_categories'].length;i<lng; i++) {
			var sous_categorie=categorie['sous_categories'][i];
			var is_sous_categorie_visible=false;
			for (var j =0, lllng=sous_categorie['list'].length;j<lllng; j++) {
				var item=sous_categorie['list'][j];

				document.getElementById('item_'+item['item_id']+'_bloc').style.display='block';
				
				item.isVisible=app.itemsIsVisible(item);
				
				if(!item.isVisible){
					document.getElementById('item_'+item['item_id']+'_bloc').style.display='none';
				}
				else{
					is_sous_categorie_visible=true;					
				}	

			}
			if(is_sous_categorie_visible==false){
				document.getElementById('items_sous_categorie_'+categorie.categorie_id+'_'+sous_categorie.sous_categorie_id).style.display='none';
			}
			else{			
				document.getElementById('items_sous_categorie_'+categorie.categorie_id+'_'+sous_categorie.sous_categorie_id).style.display='block';
				is_categorie_visible=true;
			}
		}
		if(is_categorie_visible==false){
			document.getElementById('items_categorie_'+categorie.categorie_id+'').style.display='none';
			k++;
		}
		else{
			document.getElementById('items_categorie_'+categorie.categorie_id+'').style.display='block';
		}
	}
	if(k==app.itemsCategories.length && app.items.length>0){
		document.getElementById('itemsNothingToShow').style.display='block';
	}
	else{
		document.getElementById('itemsNothingToShow').style.display='none';	
	}
	app.itemsCategoriesProgressRender();
	if(app.itemsCurrentEleve!=null){
		app.itemsStudentSkillsGraph();
	}
	if(app.itemsCurrentClasse!=null && app.itemsClassroomTab){
		app.itemsClassroomSkillsTab();
	}
}
app.itemsIsVisible=function(item,filter_id){
	var filter =app.itemsCurrentFilter;
	if(filter_id!=null){
		filter=app.getFilterById(filter_id)||app.itemsCurrentFilter;
	}
	var itemsNbFiltres=app.itemsNbFiltres;
	
	var min=filter.niveaux.min;
	var max=filter.niveaux.max;

	var is_visible=0;
	if(item['item_progression']!=null && item['item_progression']>=0){	
		if(item['item_progression']<=max && item['item_progression']>=min){
			is_visible++;						
		}	
	}else{
		if(filter.filterRated=="all"){
			is_visible++;
		}
	}	
	
	if(filter.selection.indexOf(item['item_id'])>=0 || filter.selection.length==0){
		is_visible++;
	}

	if(app.isFilteredByTags(item,filter_id)){
		is_visible++;
	}
	var recherche=filter.search;
	if(item['item_name'].toLowerCase().indexOf(recherche.toLowerCase())>=0){
		is_visible++;	
	}
	if(item['item_user']==app.userConfig.userID || app.userConfig.ppView){
		is_visible++;
	}
	if(item['item_cycle']==filter.cycle || filter.cycle==0){
		is_visible++;
	}
	if(is_visible<itemsNbFiltres){
		return false;
	}

	return true;
}

app.isFilteredByTags=function(item,filter_id){
	var filter =app.itemsCurrentFilter;
	if(filter_id!=null){
		filter=app.getFilterById(filter_id)||app.itemsCurrentFilter;
	}
	var tags=item.item_tags;
	var n=filter.tags.include.length;
	var m=filter.tags.exclude.length;
	if(n==0 && m==0){
		return true;
	}
	
	var k=0;
	tags=tags.split(',');
	tags.push(item.item_categorie);
	tags.push(item.item_sous_categorie);
	tags=arrayUnique(tags.map(function(value){if(value){return value.trim();}})).sort();

if(!tags && n>0){
		return false;
	}
	
	for (var i = tags.length - 1; i >= 0; i--) {
		if(filter.tags.exclude.indexOf(tags[i])>=0){
			return false;	
		}
		if(filter.tags.include.indexOf(tags[i])>=0){
			k++;
		}
	}
	if(k>0){
		if(filter.tags.mode=="et"){
			if(k==n){
				return true;
			}
			return false;
		}
		return true;
	}
	if(m>0 && n==0){
		return true;
	}
	return false;
}