/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.itemsFiltersSave=function(auto){ 
	if(!app.checkConnection() && !auto){
		return false;
	}	
	var name=app.trim(document.getElementById('itemsFiltersSaveName').value);
	if(name==""){
		name=moment(app.myTime()).format('DD/MM/YY - HH[h]mm');
	}	
	app.itemsCurrentFilter.name=name;
	var periode=app.getPeriodeById($('#item_filters_periode').val());
	if(periode){
		app.itemsCurrentFilter.periode=periode;
		app.itemFiltersDateStart=periode.periode_start;
		app.itemFiltersDateEnd=periode.periode_end*1+3600*24;
	}
	else{
		app.itemsCurrentFilter.periode=-1;

		var dateStart=moment(app.trim(document.getElementById('item_filters_date_start').value),"DD/MM/YYYY");
		var dateEnd=moment(app.trim(document.getElementById('item_filters_date_end').value),"DD/MM/YYYY");
		if(dateStart && dateEnd){
			app.itemFiltersDateStart=dateStart.unix();
			app.itemFiltersDateEnd=dateEnd.unix()*1+3600*24;
			app.itemsCurrentFilter.dateStart=app.itemFiltersDateStart;
			app.itemsCurrentFilter.dateEnd=app.itemFiltersDateEnd;
		}else{
			app.itemFiltersDateStart=0;
			app.itemFiltersDateEnd=Infinity;
		}	
	}
	app.itemsCurrentFilter.filterRated=document.getElementById('itemsFiltersByLevelRated').value;	
	if(auto){
		app.userConfig.itemsCurrentFilter=app.itemsCurrentFilter;
		if(app.isConnected){
			app.pushUserConfig();
		}		
		app.buildItemsDatalist();
		app.renderItemsFilters();
		app.itemsSetFilters();	
		app.itemsRenderFiltersBtn();
		document.getElementById('itemsFiltersList').value=-1;
		return;
	}
	$.post(app.serveur + "index.php?go=filters&q=add"+app.connexionParam,
	{
		filter:JSON.stringify(app.itemsCurrentFilter).replace(/'/g, "\\'"),
		time:Math.floor(app.myTime()/1000),
		sessionParams:app.sessionParams
	}, function(data) {
		$('#itemsFiltersSaveBtn').button('reset');
		app.render(data);   
		app.renderItemsFilters();
		document.getElementById('itemsFiltersSaveName').value="";
		app.hide('skills-filters-save-bloc');
		for (var i = app.filters.length - 1; i >= 0; i--) {	
			if(app.filters[i].name==name){
				document.getElementById('itemsFiltersList').value=app.filters[i].filter_id;	
			}
			$('#itemsFiltersDeleteBtn').css('display','');
		}
	}
	);	
};
app.itemsFiltersLoadFilter=function(filter_id,filter){
	if(!filter){
		filter=app.getFilterById(filter_id);
	}

	


	$('#itemsFiltersDeleteBtn').css('display','');
	if(!filter){
		$('#itemsFiltersDeleteBtn').css('display','none');
		filter=clone(app.itemsDefaultFilter);
		app.itemsFiltersSave(true);
	}

	if(filter.filter_id==null){
		$('#itemsFiltersDeleteBtn').css('display','none');
	}


	var periode=app.getPeriodeById(filter.periode);
	if(periode){
		app.itemFiltersDateStart=periode.periode_start;
		app.itemFiltersDateEnd=periode.periode_end;
		$('#item_filters_periode').val(filter.periode);
	}
	else{
		document.getElementById('item_filters_date_start').value="";
		document.getElementById('item_filters_date_end').value="";
		app.itemFiltersDateStart=0;
		app.itemFiltersDateEnd=Infinity;
		if(app.itemsCurrentFilter.dateStart){
			document.getElementById('item_filters_date_start').value=moment.unix(app.itemsCurrentFilter.dateStart).format('DD/MM/YYYY');	
			app.itemFiltersDateStart=app.itemsCurrentFilter.dateStart;
		}
		if(app.itemsCurrentFilter.dateEnd){
			document.getElementById('item_filters_date_end').value=moment.unix(app.itemsCurrentFilter.dateEnd).format('DD/MM/YYYY');	
			app.itemFiltersDateEnd=app.itemsCurrentFilter.dateEnd;
		}		
	}
	app.itemsCurrentFilter.tags=filter.tags;
	app.itemsCurrentFilter.niveaux=filter.niveaux;		
	app.itemsCurrentFilter.filterRated=filter.filterRated||"all";
	app.itemsCurrentFilter.search=filter.search;
	app.itemsCurrentFilter.selection=filter.selection;
	app.itemsCurrentFilter.cycle=filter.cycle||0;
	app.itemsSelection=filter.selection;
	app.itemsCurrentFilter.periode=filter.periode||-1;
	if(app.currentView=='competences'){
		app.renderItemsFilters();
		app.itemsRenderFiltersBtn();
		app.itemsSelectionSet();
		app.itemsSelectedUpdate();
		app.itemsSetFilters(); 		
	}	 



};

app.itemsFiltersDeleteFilter=function(){
	if(!app.checkConnection()){
		return false;
	}
	var filter_id=document.getElementById('itemsFiltersList').value;
	if(filter_id==-1){
		return;
	}	
	var ids=[filter_id];
	$.post(app.serveur + "index.php?go=filters&q=delete"+app.connexionParam,
	{
		filters:JSON.stringify(ids),
		sessionParams:app.sessionParams
	}, function(data) {
		app.render(data);   
		$('#itemsFiltersDeleteBtn').button('reset');
		app.itemsResetFilters();
	}
	);
}