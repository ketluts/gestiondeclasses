/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.itemsFilteredBySelectionToggleStatue=function(){
	if(app.itemsFilteredBySelectionStatue){
		app.itemsFilteredBySelectionStatue=false;
		app.itemsCurrentFilter.selection=[];
	}else{
		app.itemsFilteredBySelectionStatue=true;
		app.itemsCurrentFilter.selection=app.itemsSelection;	
	}
	app.itemsFiltersSave(true);
}
//##############
//Filters's form
//##############
app.renderItemsFilters=function(){
	var html=[];
	var html_exclude=[];
	for (var i =0,lng=app.itemsTagsFamilies.length ; i <lng; i++) {
		var family=app.itemsTagsFamilies[i];
		var sortable="ui-sort-enable";
		if(i==lng-1){
			sortable="ui-sort-disabled";
		}
		html.push('<div class="tags-family '+sortable+'" data-num="'+family.num+'" >');	
		html_exclude.push('<div class="tags-family">');	
		for (var j =0,llng=family.list.length ; j <llng; j++) {
			var tag=family.list[j];
			var index=app.itemsTagsDatalist.indexOf(tag.value);
			if(index<0){continue;}
			var style="btn-default";
			if(app.itemsCurrentFilter.tags.include.indexOf(tag.value)>=0){
				style="btn-primary";
			};
			html.push('<span class="btn '+style+' btn-sm" onclick="app.toggleItemFilter('+index+');">');
			html.push(app.renderTag(tag));	
			html.push('</span>');	
			style="btn-default";
			if(app.itemsCurrentFilter.tags.exclude.indexOf(tag.value)>=0){
				style="btn-danger";
			};
			html_exclude.push('<span class="btn '+style+' btn-sm" onclick="app.toggleItemExcludeFilter('+index+');">');
			html_exclude.push(app.renderTag(tag));	
			html_exclude.push('</span>');
		}

		if(i<lng-1){
			if(family.removable!==false){
				html.push('<div class="tags-family-toolbar btn btn-xs btn-default" onclick="app.itemsTagsFamilyDelete('+family.num+');"><span class="glyphicon glyphicon-remove"></span></div>');
			}
			html.push('<span class="handle"></span>');
		}
		html.push('</div>');	
		html_exclude.push('</div>');	
	}
	document.getElementById('item_filters_tags_list').innerHTML=html.join('');
	document.getElementById('item_filters_tags_exclude_list').innerHTML=html_exclude.join('');
	if(app.itemsTagsDatalist.length<=0){
		document.getElementById('item_filters_tags_list').innerHTML='Aucun mot-clé à filtrer.';	
	}
	app.itemsRenderSelectionType();
	$('#itemsFiltersSaveBtn').button('reset');
	$('#itemsFiltersDeleteBtn').button('reset');
	$('#item_filters_tags_list').sortable({
		handle: ".handle",
		update: function( event, ui ) {
			var order=$( "#item_filters_tags_list" ).sortable( "toArray", { attribute: "data-num" } );
			for (var i = order.length - 1; i >= 0; i--) {
				if(app.userConfig.itemsTagsFamilies[order[i]]){
					app.userConfig.itemsTagsFamilies[order[i]].position=i;	
				}	
			}
			app.itemsFiltersSave(true);
		},
		cancel: ".ui-sort-disabled"
	});
	$( "#item_filters_acquisition_slider" ).off('slidechange')
	.slider( "values", [ app.itemsCurrentFilter.niveaux.min, app.itemsCurrentFilter.niveaux.max ] )
	.on('slidechange',app.itemsFiltersSliderUPD);
	document.getElementById('itemsFiltersByLevelRated').value=app.itemsCurrentFilter.filterRated;
	$('#item-recherche').val(app.itemsCurrentFilter.search);
	document.getElementById('item_filters_cycle').value=app.itemsCurrentFilter.cycle;
	document.getElementById('item_filters_periode').value=app.itemsCurrentFilter.periode.periode_id||-1;	
};
app.renderTag=function(tag){
	var html=[];
	if(tag.color){
		html.push('<span class="tags-color" style="background-color:'+tag.color+'"></span>')
	}
	html.push(tag.value)
	return html.join('');
}
app.itemsSetSelectionType=function(type){
	app.itemsCurrentFilter.tags.mode=type;
	app.itemsFiltersSave(true);
	app.itemsRenderSelectionType();
};
app.itemsRenderSelectionType=function(){
	$(".items-filters-selection-type").removeClass('btn-primary').removeClass('btn-default');
	if(app.itemsCurrentFilter.tags.mode=="et"){
		$("#items-filters-et").addClass('btn-primary');
		$("#items-filters-ou").addClass('btn-default');
	}else{
		$("#items-filters-ou").addClass('btn-primary');
		$("#items-filters-et").addClass('btn-default');
	}
};
app.toggleItemFilter=function(n){
	var value=app.itemsTagsDatalist[n];
	var index=app.itemsCurrentFilter.tags.include.indexOf(value);
	if(index<0){
		app.itemsCurrentFilter.tags.include.push(value);
	}else{		
		app.itemsCurrentFilter.tags.include.splice(index, 1);
	}
	index=app.itemsCurrentFilter.tags.exclude.indexOf(value);
	if(index>=0){
		app.itemsCurrentFilter.tags.exclude.splice(index, 1);
	}
	app.itemsFiltersSave(true);

	if(app.currentView=="competences" && app.userConfig.itemsSyntheseMode=="tags" && app.itemsCurrentClasse){
		app.itemsClassroomSkillsGraph();
		app.itemsClassroomSkillsTab();
	} 
};
app.toggleItemExcludeFilter=function(n){
	var value=app.itemsTagsDatalist[n];
	var index=app.itemsCurrentFilter.tags.exclude.indexOf(value);	
	if(index<0){
		app.itemsCurrentFilter.tags.exclude.push(value);
	}else{
		app.itemsCurrentFilter.tags.exclude.splice(index, 1);
	}
	index=app.itemsCurrentFilter.tags.include.indexOf(value);
	if(index>=0){
		app.itemsCurrentFilter.tags.include.splice(index, 1);
	}
	app.itemsFiltersSave(true);	
};
app.itemsResetFilters=function(){
	app.itemsFiltersLoadFilter(false,clone(app.itemsDefaultFilter));
	app.itemsFiltersSave(true);
};
app.itemsRenderFiltersBtn=function(){
	var nb=app.itemsCurrentFilter.tags.include.length*1+app.itemsCurrentFilter.tags.exclude.length*1;
	var show=false;
	if(nb>0){
		show=true;				
	}
	var min=app.itemsCurrentFilter.niveaux.min;
	var max=app.itemsCurrentFilter.niveaux.max;
	if(min>0 || max<100){	
		show=true;
	}
	if(document.getElementById('item_filters_periode').value!=-1){
		show=true;
	}
	if(document.getElementById('item_filters_cycle').value!=0){
		show=true;
	}
	$(".itemsFiltersResetBtn").removeClass('btn-default').removeClass('btn-primary');
	if(show==true){
		$(".itemsFiltersResetBtn").addClass('btn-primary');
	}
	else{
		$(".itemsFiltersResetBtn").addClass('btn-default');
	}
	if(app.itemsCurrentFilter.tags.include.length>0){
		$('#items-filters-tags-group-btn').css('visibility','visible');
	}else{
		$('#items-filters-tags-group-btn').css('visibility','hidden');
	}
};
app.itemsFiltersDateUpdate=function(mode,value){
	app.itemsFiltersSave(true);
	$('.item_checkbox_details').html('');

if(mode=="periode"){
var periode=app.getPeriodeById(value);

if(periode){ 
 document.getElementById('item_filters_date_start').value=moment.unix(periode.periode_start).format('DD/MM/YYYY');
 document.getElementById('item_filters_date_end').value=moment.unix(periode.periode_end).format('DD/MM/YYYY');
}

if(value==-1){	
document.getElementById('item_filters_date_start').value="";
document.getElementById('item_filters_date_end').value="";

}

}
	if(app.itemsCurrentEleve!=null){
		app.itemsRenderProgressionByEleve(app.itemsCurrentEleve);
	}else if(app.itemsCurrentClasse!=null){
		app.itemsRenderProgressionByClasse(app.itemsCurrentClasse);
	}	
};
app.itemsFiltersCycleUpdate=function(){
app.itemsCurrentFilter.cycle=document.getElementById('item_filters_cycle').value;
	app.itemsFiltersSave(true);	
};
app.itemsTagsFamilyAdd=function(){
	var family={
		list:app.itemsCurrentFilter.tags.include,
		position:Infinity
	};
	app.userConfig.itemsTagsFamilies.push(family);
	app.itemsCurrentFilter.tags.include=[];
	app.itemsCurrentFilter.tags.exclude=[];
	app.itemsFiltersSave(true);
}
app.itemsTagsFamilyDelete=function(family_num){
	app.userConfig.itemsTagsFamilies.splice(family_num, 1);
	app.itemsCurrentFilter.tags.include=[];
	app.itemsCurrentFilter.tags.exclude=[];
	app.itemsFiltersSave(true);
}
app.itemsFiltersSliderUPD=function(event, ui){	
	app.itemsCurrentFilter.niveaux.min=ui.values[0];
	app.itemsCurrentFilter.niveaux.max=ui.values[1];
	
	if(ui.values[0]>0 || ui.values[1]<100){
		$(".categories_progression").css('display','none'); 
	}else{
		$(".categories_progression").css('display','');
	}
	app.itemsFiltersSave(true)
}
app.itemsFiltersBuildData=function(data){
	var filters=[];
	for (var i = data.length - 1; i >= 0; i--) {
		var filter=jsonParse(data[i].filter_data.replace(/\'/g, '"'));
		filter.filter_id=data[i].filter_id;
		filters.push(filter);
	}
	app.filters=filters;
	var options=[];
	options.push('<option value=\"-1\">');
	options.push('Aucun filtre');
	options.push('</option>');
	for (var i = app.filters.length - 1; i >= 0; i--) {
		var filter=app.filters[i];
		options.push('<option value=\"' + filter.filter_id+ '\">');
		options.push(filter.name);
		options.push('</option>');
	};
	document.getElementById('itemsFiltersList').innerHTML=options.join('');
	document.getElementById('classroomJaugesFiltersList').innerHTML=options.join('');
	document.getElementById('classroomGroupesFiltersList').innerHTML=options.join('');
};
app.itemsFiltersSearch=function(value){
	app.itemsCurrentFilter.search=value;
	app.itemsFiltersSave(true);
}
app.skillsFiltersPeriodesRender=function(){
  var html=[];
  var millesimes_ids=[];
  var millesimes=[];
  var periodes=clone(app.periodes);
  for (var i =0, lng=periodes.length; i <lng; i++) {
    var millesime=periodes[i];
    if(millesime.periode_type=="m"){
      millesimes_ids.push(millesime.periode_id);
      millesime.periodes=[];
      millesimes.push(millesime);
    }
  };
  for (var i =0, lng=periodes.length; i <lng; i++) {
    var periode=periodes[i];
    if(periode.periode_type=="p"){
      var index=millesimes_ids.indexOf(periode.periode_parent);
      if(index>=0){
        millesimes[index].periodes.push(periode);
      }
    }
  };



  millesimes=app.orderBy(millesimes,'periode_start','DESC');
  html.push('<option value="-1">Toutes</option>');
  for (var i =0, lng=millesimes.length; i <lng; i++) {
    var millesime=millesimes[i];
    html.push('<option value="'+millesime.periode_id+'">'+millesime.periode_titre+'</option>');
    if(!millesime.periodes){continue;}
    millesime.periode=app.orderBy(millesime.periodes,'periode_start','ASC');
    for (var j =0, llng=millesime.periodes.length; j <llng; j++) {
     var periode=millesime.periodes[j];
     html.push('<option value="'+periode.periode_id+'">'+periode.periode_titre+'</option>');
   }
 }
 $('#item_filters_periode').html(html.join('')).val(-1);
}