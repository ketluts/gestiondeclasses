/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};

app.buildDisciplinesIndex=function(){
app.disciplines.push({
discipline_id:-1,
discipline_name:"Aucune discipline définie"
})
}

app.getDisciplineById=function(discipline_id){
  for (var i = app.disciplines.length - 1; i >= 0; i--) {
    if(app.disciplines[i].discipline_id==discipline_id){
      return app.disciplines[i];
    }
  };
  return false;
};
app.renderDisciplines=function(){
var template = $('#template-disciplines').html();
var rendered = Mustache.render(template, {
  disciplines:app.disciplines,
  "isVisible":function(){
    if(this.discipline_id==-1){
      return false;
    }
    return true;
    }
}
  );
document.getElementById('config-disciplines-liste').innerHTML=rendered; 
$('#config-disciplines-table').dataTable({
  "language":app.datatableFR,
  paging: false,
  searching: false
}).addClass('table table-striped ');


};

app.addDiscipline=function(){
 if (!app.checkConnection()) {
  $('#btn_save_discipline').button('reset');
  return;
}
var discipline_name=document.getElementById('discipline-name').value;


if(!discipline_name || discipline_name.trim()==""){
  app.alert({title:'Il faut choisir un nom de discipline.'});
  $('#btn_save_discipline').button('reset');
  return;
}

$.post(app.serveur + "index.php?go=disciplines&q=addDiscipline" + app.connexionParam,
{
  sessionParams:app.sessionParams,
  discipline_name:discipline_name,
sessionParams:app.sessionParams
}, function(data){
 $('#btn_save_discipline').button('reset');
 app.hide('config-disciplines-form');
 app.render(data);
 document.getElementById('discipline-name').value="";
app.renderDisciplines();
});
};

app.deleteDiscipline=function(discipline_id,confirm) {
  if (!app.checkConnection()) {
   $('#btn_save_discipline').button('reset');
    return;
  }

var n=0;//Nombre de professeurs de cette discipline
for (var i = app.users.length - 1; i >= 0; i--) {
  if(app.users[i].user_matiere==discipline_id){
    n++;
  }
}

 if(!confirm){
    app.alert({title:'Cette discipline concerne '+n+' utilisateur'+app.pluralize(n,'s')+'. Voulez-vous vraiment supprimer cette discipline ?',type:'confirm'},function(){app.deleteDiscipline(discipline_id,true);},function(){
    $('#btn_save_discipline').button('reset');
    });     
    return;
  }


  $.post(app.serveur + "index.php?go=disciplines&q=deleteDiscipline"+ app.connexionParam,
    {
      discipline_id:discipline_id,
      sessionParams:app.sessionParams
    },function(data){
  $('#btn_save_discipline').button('reset');
 app.render(data);
app.renderDisciplines();

  });
};
