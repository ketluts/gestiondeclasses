/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.configImportUserInit=function(){
  app.configUsersImport=[];
  
  var tab=document.getElementById('config-import-tableur').value;
  var split=tab.split('\n');
  for (var i = split.length - 1; i >= 0; i--) {
    var user=split[i].split('\t');
    if(user.join('')==""){continue;}

    app.configUsersImport.push(user); 

  };

  app.configImportUserListe();
  $('#config-import-tableur-btn').button('reset');

}
app.configImportUserAdd=function(){
  var user=[
  document.getElementById('config-new-user-nom').value,
  document.getElementById('config-new-user-prenom').value,
  document.getElementById('config-new-user-pseudo').value,
  document.getElementById('config-new-user-mail').value
  ];

  app.configUsersImport.push(user);
  app.configImportUserListe();

}
app.configImportUserDelete=function(num){
  app.configUsersImport.splice(num, 1);
  app.configImportUserListe();
}
app.configImportCancel=function(){
  app.configUsersImport=[];
  app.configImportUserListe();
}

app.configImportUserListe=function(){

  var html=[];

  html.push('<div class="h3">Aperçu</div>');
  html.push('<table id="config-import-users-table" class="col-md-12 ">');
  html.push('<thead>');
  html.push('<tr>'); 
  html.push("<th class='col-md-2'>NOM</th>"); 
  html.push("<th class='col-md-2'>Prénom</th>");  
  html.push("<th class='col-md-2'>Pseudo</th>"); 
  html.push("<th class='col-md-2'>Mail</th>");
  html.push("<th class='col-md-4'></th>");
  html.push('</tr>');
  html.push('</thead>');    
  html.push('<tbody>');
  for (var i =0,lng=app.configUsersImport.length; i<lng; i++) {
   var user=app.configUsersImport[i];
   html.push('<tr class="tab-row">');
   html.push('<td>');
   html.push(ucfirst(user[0]));
   html.push('</td>');
   html.push('<td>');
   html.push(ucfirst(user[1]));
   html.push('</td>');
   html.push('<td>');
   html.push(ucfirst(user[2]));
   html.push('</td>');
   html.push('<td>');
   html.push(ucfirst(user[3]));
   html.push('</td>'); 
   html.push('<td class="text-right">');
   html.push('<div class="btn btn-default" onclick="event.cancelBubble=true;app.configImportUserDelete('+i+');">\
     <span class="glyphicon glyphicon-remove"></span>\
     </div>');
   html.push('</td>');  
   html.push('</tr>');
 };
 html.push('</tbody>');
 html.push("</table>");
 document.getElementById('config-import-users-bloc').innerHTML=html.join('');
 $('#config-import-users-bloc').css('display','');
 $('#config-import-users-table').dataTable({
  "language":app.datatableFR,
  paging: false,
  searching: false
});
 $('#config-import-users-table').addClass('table table-striped ');


}
app.configImportUserSave=function(){
  if (!app.checkConnection()) {
   $("#config-import-tableur-btn-save").button('reset');
   return;
 }

 
 $.post(app.serveur + "index.php?go=users&q=addUsers"+ app.connexionParam,{
  users:JSON.stringify(app.configUsersImport),
sessionParams:app.sessionParams
}, function(data) {
 $("#config-import-tableur-btn-save").button('reset');
 app.render(data); 
 app.hide('config-import-bloc');
 $('#app').goTo();
 app.configUsersRender();
}
);
}
app.buildUsersIndex=function(){ 
  app.usersIndex=[];
  for (var i = 0, lng = app.users.length; i < lng; i++) {
    app.users[i].user_pseudo=ucfirst(app.users[i].user_pseudo);
    app.usersIndex[app.users[i].user_id]=app.users[i];
  }
}
app.getUserById=function(user_id){
  return app.usersIndex[user_id]||false;  
}
app.delUser=function(user_id,confirm){
  if (!app.checkConnection()) {return;}
  if(!confirm){
    app.alert({title:'Supprimer cet utilisateur et TOUTES ses données ?',type:'confirm'},function(){app.delUser(user_id,true);});
    return;
  }
  $.post(app.serveur + "index.php?go=users&q=delete"+ app.connexionParam,{
    user_id:user_id,
    sessionParams:app.sessionParams
  }, function(data) {
    app.render(data); 
    app.configUsersRender();         
  });
};
app.configUsersRender=function(){
  var html=[];
  var users=app.users;

  users=app.orderBy(users,"user_pseudo",'ASC');
  var html=[];
  html.push('<table id="config_users_tableau">');
  html.push('<thead>');
  html.push('<tr>'); 
  html.push("<th class='col-md-2'></th>");
  html.push("<th class='col-md-2'>NOM</th>"); 
  html.push("<th class='col-md-2'>Prénom</th>");  
  html.push("<th class='col-md-2'>Pseudo</th>");     
  html.push("<th class='col-md-2'>Mail</th>");   
  html.push("<th class='col-md-2'>Discipline</th>"); 
  html.push("<th class='col-md-2'>Status</th>");
  html.push('<th class="text-right col-md-4">Options</th>');
  html.push('</tr>');
  html.push('</thead>');    
  html.push('<tbody>');
  for (var i =0,lng=users.length; i<lng; i++) {
   var user=users[i];

   html.push('<tr class="tab-row">');


   html.push('<td>');

   html.push('<div onclick="app.configUserView('+user['user_id']+');" title="Voir les cahiers de texte" class="btn btn-default">\
    <span class="glyphicon glyphicon-calendar"> </span>\
    </div>');

   html.push('</td>');

   html.push('<td>');
   html.push(ucfirst(user['user_nom']));
   html.push('</td>');
   html.push('<td>');
   html.push(ucfirst(user['user_prenom']));
   html.push('</td>');
   html.push('<td>');
   html.push(user['user_pseudo']);
   html.push('</td>');
   html.push('<td>');
   if(user['user_mail']){
     html.push("<a href='mailto:"+user['user_mail']+"'>"+user['user_mail']+"</a>");      
   }
   html.push('</td>');
   html.push('<td>');
   html.push('<select class="form-control config-users-discipline" onchange="app.configUserSetDiscipline(\''+user['user_id']+'\',this.value);" data-value="'+user['user_matiere']+'">');
   var template = $('#template-config-disciplines-select').html();
   var rendered = Mustache.render(template, {disciplines:app.disciplines});
   html.push(rendered); 

   html.push('</select>');

   html.push('</td>');

   html.push('<td data-order="'+user['user_type']+'">');
   if(user['user_id']==app.userConfig.userID){

     if(user['user_type']=="admin"){
      html.push('Administrateur');
    }else{
      html.push('Utilisateur');
    }
  }else{
   html.push('<select class="form-control connexion-requise config-users-statut" data-value="'+user['user_type']+'" onchange="app.configUserSetStatut(\''+user['user_id']+'\',this.value);">');
   html.push('<option value="user" >Utilisateur</option>');
   html.push('<option value="admin" >Administrateur</option>');
   html.push('</select>');
 }
 html.push('</td>');

 html.push('<td class="text-right">');
 html.push('<div class="btn btn-default btn-sm" onclick="event.cancelBubble=true;app.delUser('+user['user_id']+');">\
   <span class="glyphicon glyphicon-remove"></span>\
   </div>');
 html.push('</td>');  
 html.push('</tr>');
};
html.push('</tbody>');
html.push("</table>");
document.getElementById('config_users').innerHTML=html.join('');

$('.config-users-discipline').each(function(){
  var elem=$(this);
  elem.val(elem.attr('data-value'));
});
$('.config-users-statut').each(function(){
  var elem=$(this);
  elem.val(elem.attr('data-value'));
});
$('#config_users_tableau').dataTable({
  stateSave: true,
  "language":app.datatableFR,
  "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
  "pageLength": 10
});
$('#config_users_tableau').addClass('table table-striped ');
}

app.configUserSetStatut=function(user_id,statut){
 $.post(app.serveur + "index.php?go=users&q=update"+ app.connexionParam,{
  user_id:user_id,
  statut:statut,
sessionParams:app.sessionParams
}, function(data) {
 app.render(data);         
}
);
}
app.configUserSetDiscipline=function(user_id,discipline_id){
 $.post(app.serveur + "index.php?go=users&q=update"+ app.connexionParam,{
  user_id:user_id,
  discipline_id:discipline_id,
sessionParams:app.sessionParams
}, function(data) {
 app.render(data);
 app.navigate(app.currentHash,true);        
}
);
}
app.configUserView=function(user_id){
  var user=app.getUserById(user_id);
  app.configCurrentUser=user;
  document.getElementById('config_user_view').style.display="block";
  
  document.getElementById('config_user_view_pseudo').innerHTML=user.user_pseudo+ "<br/><small>"+user.user_nom+" "+user.user_prenom+"</small>";
  document.getElementById('config_user_view_pseudo').style.borderColor="#"+app.getColor(user.user_pseudo);
  document.getElementById("config_user_view_cdt").innerHTML="";

  var user_classes=[];
  for (var i = app.agenda.length - 1; i >= 0; i--) {
    var event=app.agenda[i];
    if(event.event_type=='classe' && user_classes.indexOf(event.event_type_id)<0 && event.event_user==user.user_id){
      //continue si mauvais milesime
      user_classes.push(event.event_type_id);
    }
  }
  document.getElementById('cdt_visas').style.display="none";
  document.getElementById('config_user_view_cdt_select').innerHTML="<option>Aucune données</option>";
  var html=['<option value="-1">-</option>'];
  for (var i = user_classes.length - 1; i >= 0; i--) {
    var classe=app.getClasseById(user_classes[i]);

    //continue si mauvais milesime
    html.push('<option value="'+classe.classe_id+'">'+classe.classe_nom+'</option>');
  }
  if(html.length>0){
    document.getElementById('config_user_view_cdt_select').innerHTML=html.join('');
  }
  $('#config_user_view_pseudo').goTo();
}