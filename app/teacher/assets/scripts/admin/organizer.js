/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
//###########
//Admin - Cahier de textes
app.configUserCdtView=function(classe_id){
  var html=[];
  var currentDate="";
  var currentMonth="";
  var display=false;
  document.getElementById("cdt_visas_liste").innerHTML="";
  app.cdtCurrentClasse=classe_id;
  var agenda=app.orderBy(app.agenda,'event_start','DESC');
  var classe=app.getClasseById(classe_id)
  for (var i =0, lng= agenda.length ; i <lng; i++) {
    var event=agenda[i];
    if(event.event_type!='classe' || event.event_type_id!=classe_id || event.event_user!=app.configCurrentUser.user_id){
      continue;
    }
    if(event.event_icon!="glyphicon-home" && event.event_icon!="glyphicon-blackboard"){
      continue;
    }
    if(event.event_start*1000>app.myTime()){
      continue;
    }
    var date=moment(event.event_start*1000).format("[Le] DD/MM/YYYY");
    var month=moment(event.event_start*1000).format("MMMM");
    var bgColor="#ffffff";
    if(event.event_icon=="glyphicon-home"){
      bgColor=app.agendaEventCommentBackGround;
    }
    if(month!=currentMonth){
      html.push('<div class="h3 cdt_date">');
      html.push(ucfirst(month));
      html.push('</div>');
      currentMonth=month;
    }
    if(date!=currentDate){
      html.push('<div class="h4 cdt_date">');
      html.push(date);
      html.push('</div>');
      currentDate=date;
    }
    html.push('<div class="cdt_item cdt_devoirs" style="background-color:'+bgColor+';border-color:'+classe.color+';">');
    html.push('<span class="cdt_title">');
    html.push('<span class="glyphicon '+event.event_icon+'"></span> ');
    html.push(event.event_title);
    html.push('</span>');
    html.push('<span class="cdt_data">');
    html.push(event.event_data);
    html.push('</span>');
    html.push('</div>');
    display=true;
  }
  if(display){
   document.getElementById("config_user_view_cdt").innerHTML=html.join('');
   document.getElementById('cdt_visas').style.display="block";
   app.cdtVisaRender();
 }else{
   document.getElementById("config_user_view_cdt").innerHTML="";
   document.getElementById('cdt_visas').style.display="none";
 }
}
app.cdtVisaAdd=function(){
  if (!app.checkConnection()) {
    return;
  }
  $.post(app.serveur + "index.php?go=agenda&q=addVisa"+app.connexionParam,{
   event_title:"Visa",
   event_data:"Cahier de texte visé par "+ucfirst(app.pseudo_utilisateur),
   event_icon:"flaticon-verified9",
   event_start:Math.floor(app.myTime()/1000),
   event_end:Math.floor(app.myTime()/1000),
   event_color:"#ff0000",
   event_allDay:false,
   event_type:"classe",
   event_visibility:false,
   event_user:app.configCurrentUser.user_id,
   event_type_id:app.cdtCurrentClasse,
   event_lock:1,
   event_periode:app.active_periode.periode_id,
   event_date:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
 }, function(data) {
  app.render(data);   
  app.cdtVisaRender();
}
);
}
app.cdtVisaRender=function(){
  var html=[];
  for (var i = app.agenda.length - 1; i >= 0; i--) {
    var event=app.agenda[i];
    if(event.event_icon!="flaticon-verified9" 
      || event.event_lock!=1 
      || event.event_user!=app.configCurrentUser.user_id 
      || event.event_type!="classe" 
      || event.event_type_id!=app.cdtCurrentClasse){
      continue;
  }
  html.push("<div class='cdt_visa_item'>");
  html.push("Le "+moment(event.event_date*1000).format('DD/MM/YYYY'));
  html.push("<div class='btn btn-xs btn-primary pull-right' onclick='app.cdtVisaDelete("+event.event_id+");'><span class='glyphicon glyphicon-remove'></span></div>");
  html.push("</div>");
}
document.getElementById("cdt_visas_liste").innerHTML=html.join('');
}
app.cdtVisaDelete=function(event_id,confirm){
  if (!app.checkConnection()) {
    return;
  }
  if(!confirm){
    app.alert({title:'Supprimer ce visa ?',type:'confirm'},function(){app.cdtVisaDelete(event_id,true);});
    return;
  }
  $.post(app.serveur + "index.php?go=agenda&q=deleteVisa"+ app.connexionParam,{
    event_id:event_id,
sessionParams:app.sessionParams
  }, function(data) {
    app.render(data);    
    app.cdtVisaRender();
  });
}