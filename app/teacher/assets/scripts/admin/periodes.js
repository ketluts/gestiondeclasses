/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.getPeriodeById=function(periode_id){
  for (var i = app.periodes.length - 1; i >= 0; i--) {
    if(app.periodes[i].periode_id==periode_id){
      return app.periodes[i];
    }
  };
  return false;
};
app.renderPeriodes=function(){
  var periodes=app.periodes;
  if(periodes.length==0){
   document.getElementById('periodes-liste').innerHTML ="Aucune période n'a été définie";
   return;
 }

 var active_periode=null;

 var millesimesIds=[];
 var millesimes=[];

 app.periodes=app.orderBy(app.periodes,"periode_id","ASC");
//alert(dump(app.periodes));
 for (var j = 0, lng = app.periodes.length; j < lng; j++) {
  var periode = app.periodes[j];
  var type=periode.periode_type;
 //  alert(dump(periode));
  if(type=="m"){
    millesimes.push({
      millesime_id:periode.periode_id,
      millesime_name:periode.periode_titre,
      millesime_start:moment(periode.periode_start*1000).format('DD/MM/YYYY'),
      millesime_end:moment(periode.periode_end*1000).format('DD/MM/YYYY'),
      millesime_active:periode.periode_active,
      millesime_periodes:[]
    });
    millesimesIds.push(periode.periode_id);
  }else{
//alert(dump(millesimesIds));
    var index=millesimesIds.indexOf(periode.periode_parent);
    //if(!index){continue;}
    var periode={
      periode_titre:periode.periode_titre,
      periode_id:periode.periode_id,
      periode_active:periode.periode_active,
      periode_selected:"",
      periode_lock:periode.periode_lock,
      periode_lockStyle:"btn-default"
    };
    if(periode.periode_active==1){
      active_periode=periode.periode_id;
    }
    if(periode.periode_lock==1){
      periode.periode_lockStyle="btn-primary";
    }
    if(!millesimes[index]){continue;}
    
    millesimes[index].millesime_periodes.push(periode);
  }

}

var template = $('#template-periodes').html();
var rendered = Mustache.render(template, {millesimes:millesimes});
document.getElementById('periodes-liste').innerHTML=rendered; 
$('.millesime-periodeForm-date').datepicker({
  format:'dd/mm/yyyy'
});
$('#periode-'+active_periode+'-active-btn').removeClass('btn-default').addClass('btn-primary');
};

app.millesimeAddForm=function(){
  app.show('periodes-millesime-form');
  app.hide('periodes-edition-bloc');
    document.getElementById('millesime_name').focus();
}

app.addMillesime=function(){
 if (!app.checkConnection()) {
  $('#btn_save_millesime').button('reset');
  return;
}
var millesime_name=document.getElementById('millesime_name').value;
var start=document.getElementById('millesime_start').value;
var end=document.getElementById('millesime_end').value;

if(start!="" && end!=""){
  var millesime_start=moment(start, "DD/MM/YYYY").unix();
  var millesime_end=moment(end, "DD/MM/YYYY").unix();
}

if(!millesime_start){
  app.alert({title:'Il faut choisir la date de début.'});
  $('#btn_save_millesime').button('reset');
  return;
}
if(!millesime_end){
  app.alert({title:'Il faut choisir la date de fin.'});
  $('#btn_save_millesime').button('reset');
  return;
}
if(!millesime_name || millesime_name.trim()==""){
  millesime_name = moment(start, "DD/MM/YYYY").format('YYYY')+"/"+moment(end, "DD/MM/YYYY").format('YYYY');
}

$.post(app.serveur + "index.php?go=periodes&q=addMillesime" + app.connexionParam,{
  millesime_start:millesime_start,
  millesime_end:millesime_end,
  millesime_titre:millesime_name,
  sessionParams:app.sessionParams
}, function(data){
 $('#btn_save_millesime').button('reset');
 app.hide('periodes-millesime-form');
 app.render(data);
 app.renderPeriodes();
});
};

app.delMillesime=function(millesime_id,confirm) {
  if (!app.checkConnection()) {
    $('#btn_delete_periode').button('reset');
    return;
  }
 if(!confirm){
    app.alert({title:'Voulez-vous vraiment supprimer cette année scolaire ?',type:'confirm'},function(){app.delMillesime(millesime_id,true);},function(){
      $('#btn_delete_periode').button('reset');
    });     
    return;
  }


  $.post(app.serveur + "index.php?go=periodes&q=deleteMillesime"+ app.connexionParam,
    {
      millesime_id:millesime_id,
      sessionParams:app.sessionParams
    },function(data){
    app.render(data);
     app.hide('periodes-edition-bloc');
    app.renderPeriodes();

  });
};

app.periodeAddForm=function(millesime_id){
  app.show('periodes-edition-bloc');
 var template = $('#template-periodes-edition').html();
 var rendered = Mustache.render(template, {
  periode:{
    periode_id:-1,
    periode_parent:millesime_id
  },
  action:"add"
});
 document.getElementById('periodes-edition-bloc').innerHTML=rendered; 
 $('.periode-edition-date').datepicker({
  format:'dd/mm/yyyy'
});

  document.getElementById('periode-edition-name').focus();
   $('#periodes-edition-bloc').goTo();
}

app.delPeriode=function(periode_id,confirm) {
  var periode=app.getPeriodeById(periode_id);
  if(!periode.periode_parent){
app.delMillesime(periode_id);
    return;
  }
  if (!app.checkConnection()) {
    $('#btn_delete_periode').button('reset');
    return;
  }

 if(!confirm){
    app.alert({title:'Voulez-vous vraiment supprimer cette période ?',type:'confirm'},function(){app.delPeriode(periode_id,true);},function(){
      $('#btn_delete_periode').button('reset');
    });     
    return;
  }


  $.post(app.serveur + "index.php?go=periodes&q=deletePeriode" + app.connexionParam,
    {
      periode_id:periode_id,
      sessionParams:app.sessionParams
    },function(data){
   $('#btn_delete_periode').button('reset');
    app.render(data);
     app.hide('periodes-edition-bloc');
    app.renderPeriodes();

  });
};
app.setActivePeriode=function(periode_id){
 if (!app.checkConnection()) {
  return;
}
$.post(app.serveur + "index.php?go=periodes&q=setPeriode" + app.connexionParam,
  {
     periode_id:periode_id,
      sessionParams:app.sessionParams
  },function(data){
  app.init();
  app.render(data);  
  app.renderPeriodes();
});
};
app.periodeToggleLock=function(periode_id,periode_lock){
 if (!app.checkConnection()) {
  return;
}
var lock=(periode_lock*1+1)%2;
$.post(app.serveur + "index.php?go=periodes&q=lockPeriode"+ app.connexionParam,{
  periode_id:periode_id,
      sessionParams:app.sessionParams,
  periode_lock:lock,
  time:Math.floor(app.myTime()/1000)
}, function(data){
  app.render(data);
  app.renderPeriodes();
});
};


app.periodeEditionInit=function(periode_id){
  var periode=app.getPeriodeById(periode_id);
  periode.periode_start_str=moment(periode.periode_start*1000).format('DD/MM/YYYY');
  periode.periode_end_str=moment(periode.periode_end*1000).format('DD/MM/YYYY');
  app.show('periodes-edition-bloc');
  var template = $('#template-periodes-edition').html();
  var rendered = Mustache.render(template, {
    periode:periode,
    action:"upd",
    edit:true
  });
  document.getElementById('periodes-edition-bloc').innerHTML=rendered; 
  $('.periode-edition-date').datepicker({
    format:'dd/mm/yyyy'
  });
  $('#periodes-edition-bloc').goTo();
}

app.periodeUpdate=function(periode_id,millesime_id,action){
 if (!app.checkConnection()) {
  $('#btn_save_periode_edition').button('reset');
  return;
}

var periode_name=document.getElementById('periode-edition-name').value;

if(!periode_name || periode_name.trim()==''){
  app.alert({title:'Il faut choisir le nom de la période.'});
  $('#btn_save_periode_edition').button('reset');
  return;
}

var start=document.getElementById('periode-edition-start').value;
var end=document.getElementById('periode-edition-end').value;

if(start!="" && end!=""){
  var periode_start=moment(start, "DD/MM/YYYY").unix();
  var periode_end=moment(end, "DD/MM/YYYY").unix();
}

if(!periode_start){
  app.alert({title:'Il faut choisir la date de début.'});
  $('#btn_save_periode_edition').button('reset');
  return;
}
if(!periode_end){
  app.alert({title:'Il faut choisir la date de fin.'});
  $('#btn_save_periode_edition').button('reset');
  return;
}
$.post(app.serveur + "index.php?go=periodes&q="+action+"Periode"+ app.connexionParam,{
  periode_titre:periode_name,
  periode_id:periode_id,
  millesime_id:millesime_id,
  periode_start:periode_start,
  periode_end:periode_end,
  sessionParams:app.sessionParams
}, function(data){
 $('#btn_save_periode_edition').button('reset');
 app.hide('periodes-edition-bloc');
 app.render(data);
 app.renderPeriodes();
});

}