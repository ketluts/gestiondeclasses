/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.renderAdminConfig=function(){  
  
  app.viewClear();
  app.currentClasse=null;
  app.currentEleve=null;
  app.currentView="config";
  document.getElementById('config_no_admin_bloc').style.display = "none";

  app.configAdminsRender();

  document.getElementById('titre').innerHTML = "Administration";
  document.getElementById('etablissement_old_passe').value="";
  document.getElementById('etablissement_nouveau_passe').value="";
  document.getElementById('etablissement_nouveau_passe2').value="";
  document.getElementById('config-import-tableur').value="";
  $('#config-import-tableur-btn-save').button('reset');  
  $('#config-import-users-bloc').css('display','none');
  $('#config-import-tableur-btn').button('reset');  
  document.getElementById('config-import-users-bloc').innerHTML="";

  if(app.userConfig.admin){
    $(".config_admin_bloc").css("display","block");
    app.renderPeriodes();
    app.renderLegends();
    app.configUsersRender();
    app.renderDisciplines();
    document.getElementById('config_week').innerHTML=app.getWeekName();
    
    document.getElementById('config_subscriptions').value=app.serveurOptions.subscriptions;
    app.configPluginsListeRender();
  }else{
   document.getElementById('config_no_admin_bloc').style.display = "block";
 }
 app.doStats();

 app.hide('config_school_delete'); 
 document.getElementById('config-import-bloc').style.display = "none";

 document.getElementById('config_user_view').style.display="none";
 document.getElementById('connexion-link-prof').value=app.url+"teacher/?school="+app.nom_etablissement;
 document.getElementById('connexion-link-eleve').value=app.url+"student/?school="+app.nom_etablissement;
};
//#########
//OPTIONS
//#########
app.serverConfigUPD=function(options) {
  if (!app.checkConnection()) {
    return;
  }   
  $.post(app.serveur + "index.php?go=school&q=update" + app.connexionParam, {
    options:JSON.stringify(options),
sessionParams:app.sessionParams
  },
  app.render
  );
};
app.toggleWeeks=function(){
  app.weeks=(app.weeks*1+1)%2;
  document.getElementById('config_week').innerHTML=app.getWeekName();
  app.serverConfigUPD({'weeks':app.weeks});
  document.getElementById('home-week').innerHTML =app.getWeekName();
}
app.configUpdatePlugin=function(name,value){


  //app.serverConfigUPD({'plugins':this.value});
  var pluginsEnabled=[];
  for (var i = app.plugins.length - 1; i >= 0; i--) {
    var plugin=app.plugins[i];
    if(plugin.name==name){
      plugin.enabled=value;
      if(plugin.enabled){
        pluginsEnabled.push(plugin.name);
   //app.pluginsLauncher('onEnabled');
   (plugin['onEnabled'])();
 }
}


}

app.serverConfigUPD({'plugins':JSON.stringify(pluginsEnabled)});
return true;
}
app.configPluginsListeRender=function(){
  var html=[];
  for (var i = app.plugins.length - 1; i >= 0; i--) {
    var plugin=app.plugins[i];
    if(!plugin.display){
      continue;
    }
  //alert(plugin.name+" "+plugin.enabled)
  var checked="";
  if(plugin.enabled){
    checked="checked";
  }

  html.push('<div class="well well-sm plugin-desciption">');
  html.push('<label class="switch">');

  html.push('<input type="checkbox" onclick="return app.configUpdatePlugin(\''+plugin.name+'\',this.checked);" '+checked+'/>');
  html.push('<span class="slider round"></span>');
  html.push('</label>');


  html.push('<h4>');
  html.push(plugin.name);
  html.push('</h4>');

  if(plugin.description){
    html.push('<hr/>');
    html.push(plugin.description);
  }

  html.push('</div>');
}
document.getElementById('config-plugins-descriptions').innerHTML=html.join('');

}

app.doStats=function(){  
  app.stats.classes=app.classes.length-2;
  app.stats.eleves=app.eleves.length;
  app.stats.events=  app.agenda.length;
  app.stats.notes=  app.notes.length;
  app.stats.etoiles= app.etoiles.length;
  app.stats.filles=0;
  app.stats.garcons=0;
  for (var i = app.eleves.length - 1; i >= 0; i--) {
    var eleve=app.eleves[i];
    if(eleve.eleve_genre=="F"){
      app.stats.filles++;
    }
    if(eleve.eleve_genre=="G"){
      app.stats.garcons++;
    }
  }
  app.stats.items=app.items.length;

  if(document.getElementById('config-stats')){
   var template = $('#template-stats').html();
   var rendered = Mustache.render(template, {stats:app.stats});
   document.getElementById('config-stats').innerHTML=rendered; 
 }
};
//#########
//PASSWORD
//#########
app.etablissementPasswordUPD=function(confirm){
  if(!confirm){
    app.alert({title:'Attention, en continuant les autres utilisateurs seront déconnectés !',type:'confirm'},function(){app.etablissementPasswordUPD(true);});
    return;
  }
  var old_passe=document.getElementById('etablissement_old_passe').value;
  var passe=document.getElementById('etablissement_nouveau_passe').value;
  var passe2=document.getElementById('etablissement_nouveau_passe2').value;
  if (!app.checkConnection()) {return;}
  $.post(app.serveur + "index.php?go=school&q=update"+app.connexionParam,{
    old_passe:old_passe,
    nouveau_passe:passe,
    nouveau_passe2:passe2,
sessionParams:app.sessionParams
  }, function(data){
    app.render(data);       
  });
};
//#########
//RESETS
//#########
app.etablissementDEL=function(confirm) {
  if (!app.checkConnection()) {
    return;
  }
  if(!confirm){
    app.alert({title:'Voulez-vous vraiment supprimer cette établissement ?',type:'confirm'},function(){app.etablissementDEL(true);},function(){
      $('#config_school_delete_btn').button('reset');
    });
    
    return;
  }
  $.post(app.serveur + "index.php?go=school&q=delete"+ app.connexionParam,{
    sessionParams:app.sessionParams
  }, app.render);
};
