/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.renderLegends=function(){
  var html=[];
  for (var i = app.eventsAvailable.length - 1; i >= 0; i--) {
    var oEvent=app.eventsAvailable[i];
    var legend=app.legends[oEvent.type]||"";
    html.push('<div class="input-group">');
    html.push('<div class="input-group-addon"><span class="glyphicon '+oEvent.type+'"></span></div>');
    html.push('<input class="form-control" id="'+oEvent.type+'_legend" placeholder="ex : '+oEvent.placeholder+'" type="text" value="'+legend+'"/>');
    html.push('</div>');              
  };
  document.getElementById('legends').innerHTML = html.join('');
}
app.saveLegends=function(){
  var legends=[];
  for (var i = app.eventsAvailable.length - 1; i >= 0; i--) {
    var oEvent=app.eventsAvailable[i];
    var legend=document.getElementById(oEvent.type+'_legend').value;
    if(legend==""){continue;}
    legends.push([oEvent.type,legend]);
  }
  
  $.post(app.serveur + "index.php?go=school&q=update"+ app.connexionParam,
  {
    legends:JSON.stringify(legends),
    sessionParams:app.sessionParams
  },function(data) {
   $("#btn_save_legends").button('reset');
   app.render(data); 
   app.renderConfigEvents();
 }
 );
};
app.setLegends=function(legends){
  app.legends=[];
  //for (let i = app.eventsAvailable.length - 1; i >= 0; i--) {
   //var oEvent=app.eventsAvailable[i];
   //var legend=legends[i];
  // app.legends[oEvent.type]=oEvent.legend;

 for (let i = legends.length - 1; i >= 0; i--) {
  var legend=legends[i];
//if(oEvent.type==legend.legend_event){
  app.legends[legend.legend_event]=legend.legend_text;
//}
};


 //}



 //if(legends.length==0){return;}

};