/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

//##########
//ADD MESSAGE
//##########
app.discussionAdd=function(){
	if (!app.checkConnection()) {return;}
	if(app.destinataires.length==0){
		app.alert({title:"Il faut sélectionner des participants."});
		app.destinatairesForm();
		$('#message_send_btn').button('reset');  
		return;
	}
	clearTimeout(app.discussionTimer);
	var message_subject=app.trim(document.getElementById('message_subject').value);
	var message_content=app.trim(document.getElementById('message_content').value);
	if(message_subject=="" || message_content==""){
		app.alert({title:"Il faut entrer un sujet et un message."});
		$('#message_send_btn').button('reset');  
		return;
	}
	$.post(app.serveur + "index.php?go=messagerie&q=newMessage"+app.connexionParam,{
		message_subject:message_subject,
		message_content:message_content,
		message_destinataires: JSON.stringify(app.destinataires),
		time:Math.floor(app.myTime()/1000),
		syncId:app.userConfig.messagesSyncId,
		sessionParams:app.sessionParams
	}, function(data) {
		app.render(data);   
		app.discussionTimer=setTimeout(app.getDiscussions,app.discussionRefreshTime);
		
		app.hide('message_form');
		app.messagesRender();
	}
	);
}
app.destinataireAdd=function(destinataire_id,destinataire_type,destinataire_nom){
	//On evite les doublons.
	for (var i =0,lng= app.destinataires.length; i<lng; i++) {
		var destinataire=app.destinataires[i];
		if(destinataire.destinataire_id==destinataire_id && destinataire.destinataire_type==destinataire_type){
			return;
		}
	}
	//On ajoute le destinataire.
	app.destinataires.push({
		destinataire_id:destinataire_id,
		destinataire_type:destinataire_type,
		destinataire_nom:destinataire_nom
	});
	app.destinatairesRender();
}
app.destinataireDel=function(destinataire_id,destinataire_type){
	var temp_destinataires=[];
	for (var i =0,lng= app.destinataires.length; i<lng; i++) {
		var destinataire=app.destinataires[i];
		if(destinataire.destinataire_id!=destinataire_id || destinataire.destinataire_type!=destinataire_type){
			temp_destinataires.push(destinataire);
		}
	}
	app.destinataires=temp_destinataires;
	app.destinatairesRender();
}
app.destinatairesForm=function(){
	var classes=app.classes;
	var source   = document.getElementById("template-messagerie-destinatairesForm").innerHTML;
	var template = Handlebars.compile(source);
	var context = {
		classes:classes,
		users:app.users,
		users_length:app.users.length+" personne"+app.pluralize(app.users.length,'s')
	};
	document.getElementById('destinataires_select').innerHTML=template(context);

	// document.getElementById('destinataires_select').innerHTML="";
	// var html=[];
	// //On liste les utilisateurs
	// if(app.users.length>1){
	// 	html.push('<div class="panel panel-default">\
	// 		<div class="panel-heading destinataires_select_btn">\
	// 		<h4 class="panel-title">\
	// 		<a data-toggle="collapse" href="#destinataire_users">\
	// 		Utilisateurs\
	// 		</a>\
	// 		<button class="btn btn-default btn-sm" onclick="app.destinataireAdd(\'all_users\',\'all_users\',\'Tous les utilisateurs\');">\
	// 		<span class="glyphicon glyphicon-chevron-left"></span>\
	// 		</button>\
	// 		</h4>\
	// 		</div>\
	// 		<div class="panel-collapse collapse " id="destinataire_users">\
	// 		<ul class="list-group">');	
	// 	for (var j =0,llng=app.users.length ; j <llng; j++) {
	// 		var user=app.users[j];
	// 		if(user.user_id==app.userConfig.userID){continue;}
	// 		html.push('<li class="list-group-item destinataires_select_btn">\
	// 			'+user.user_pseudo+'\
	// 			<button class="btn btn-default btn-sm" onclick="app.destinataireAdd('+user.user_id+',\'user\',\''+user.user_pseudo+'\');">\
	// 			<span class="glyphicon glyphicon-chevron-left"></span>\
	// 			</button>\
	// 			</li>');
	// 	}
	// 	html.push('</ul>\
	// 		</div>\
	// 		</div>');  
	// }
	// //On liste les classes et les élèves
	// for (var i =0,lng= app.classes.length; i <lng; i++) {
	// 	var classe=app.classes[i];
	// 	if(app.userConfig.classesDisable.indexOf(classe.classe_id)!=-1){continue;}
	// 	html.push('<div class="panel panel-default" id="destinataire_classe_'+classe.classe_id+'_block">\
	// 		<div class="panel-heading destinataires_select_btn">\
	// 		<h4 class="panel-title">\
	// 		<a data-toggle="collapse" data-parent="#destinataire_classes" href="#destinataire_classe_'+classe.classe_id+'">\
	// 		'+app.cleanClasseName(classe.classe_nom)+'\
	// 		</a>\
	// 		<button class="btn btn-default btn-sm" onclick="app.destinataireAdd('+classe.classe_id+',\'classe\',\''+classe.classe_nom+'\');">\
	// 		<span class="glyphicon glyphicon-chevron-left"></span>\
	// 		</button>\
	// 		</h4>\
	// 		</div>\
	// 		<div id="destinataire_classe_'+classe.classe_id+'" class="panel-collapse collapse ">\
	// 		<ul class="list-group" id="destinataire_classe_'+classe.classe_id+'_body">');
	// 	for (var j =0,llng=app.classes[i].eleves.length ; j <llng; j++) {
	// 		var eleve=app.getEleveById(app.classes[i].eleves[j]);
	// 		if(!eleve.eleve_prenom){
	// 			eleve.eleve_prenom="";
	// 		}
	// 		html.push('<li class="list-group-item destinataires_select_btn">\
	// 			'+app.renderEleveNom(eleve)+'\
	// 			<button class="btn btn-default btn-sm" onclick="app.destinataireAdd('+eleve.eleve_id+',\'eleve\',\''+app.renderEleveNom(eleve)+'\');">\
	// 			<span class="glyphicon glyphicon-chevron-left"></span>\
	// 			</button>\
	// 			</li>');
	// 	}
	// 	html.push('</ul>\
	// 		</div>\
	// 		</div>');  
	// }; 
	// document.getElementById('destinataires_select').innerHTML+=html.join('');
	// for (var i = app.classes.length - 1; i >= 0; i--) {
	// 	var classe=app.classes[i];
	// 	if(document.getElementById('destinataire_classe_'+classe.classe_id+'_body') && document.getElementById('destinataire_classe_'+classe.classe_id+'_body').innerHTML==""){
	// 		document.getElementById('destinataire_classe_'+classe.classe_id+'_block').style.display="none";
	// 	}
	// }
}