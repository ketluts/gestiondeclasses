/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.getMessages=function(){
	
	app.viewClear();	
	document.getElementById('template_messagerie').style.display = "block";
	document.getElementById('titre').innerHTML = 'Mes discussions';
	document.getElementById('messages-view').style.display = "none";
	document.getElementById('message_form').style.display = "none";
	$('#discussion-selected-bloc').css('display','none');
	app.messagesRender();
	app.discussionRefreshTime=5000;
	clearTimeout(app.discussionTimer);
	app.currentDiscussion=null;
	app.currentView="messagerie";

	if(app.currentEleve){
		app.renderMessageForm();
		app.destinataireAdd(app.currentEleve.eleve_id,'eleve',''+app.renderEleveNom(app.currentEleve)+'');
		document.getElementById('message_subject').focus();
	}
};

app.getDiscussions=function(){
	clearTimeout(app.discussionTimer);
	if(app.isConnected){
		$.post(app.serveur + "index.php?go=messagerie&q=get"+app.connexionParam,{
syncId:app.userConfig.messagesSyncId,
			sessionParams:app.sessionParams
		}, function(data) {
			app.render(data);   	 
		}
		);
	}
	app.discussionTimer=setTimeout(app.getDiscussions,app.discussionRefreshTime);
}
app.messagerieInitData=function(){
	var syncId=0;
	var n=app.messages.length;
	app.messagesIndex=[];
	for (var i = 0, lng=n ; i<lng; i++) {
		var message=app.messages[i];
		if(message.message_id*1>syncId*1){
			syncId=message.message_id;
		}
		app.messagesIndex[message.message_id]=i;
	}	
	app.userConfig.messagesSyncId=syncId;
	app.renderBadgeMessages();
	if(app.currentView=="messagerie" && n>0){
		app.messagesRender();
		if(app.currentDiscussion!=null){
			app.renderDiscussionMessages();
		}
	}
}
app.getMessageById=function(message_id) { //alert(dump(app.messagesIndex));
	return app.messages[app.messagesIndex[message_id]];
};
app.updMessagerieEnable=function(value){
	//Apparaître dans la liste des destinataires des élèves d'une classe
	if (!app.checkConnection()) {return;}
	$.post(app.serveur + "index.php?go=classe&q=update"+app.connexionParam,{
classe_id:app.currentClasse.classe_id,
messagerie:value,
		sessionParams:app.sessionParams
	}, function(data) {
		app.render(data);  
	}
	);
}

//##########
//DISCUSSION
//##########
app.answerAdd=function(){
	if (!app.checkConnection()) {return;}
	var message_content=app.trim(document.getElementById('answer-text').value);
	if(message_content==""){
		app.alert({title:"Il faut entrer un message."});
		$('#answer_send_btn').button('reset');  
		return;
	}
	clearTimeout(app.discussionTimer);
	$.post(app.serveur + "index.php?go=messagerie&q=newAnswer"+app.connexionParam,{
		syncId:app.userConfig.messagesSyncId,
		message_content:message_content,
		message_parent_message: app.currentDiscussion,
		time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
	}, function(data) {
		app.render(data);   
		
		app.discussionTimer=setTimeout(app.getDiscussions,app.discussionRefreshTime);
		$('#answer_send_btn').button('reset');  
		document.getElementById('answer-text').value="";
	}
	);
}
app.delDiscussion=function(message_id,confirm){
	if (!app.checkConnection()) {return;}
	app.messagesDeleted=[];
	if(message_id!="selection"){
		var message=app.getMessageById(message_id);
		if(!confirm){
			app.alert({title:'Quitter la discussion «'+ucfirst(message.message_subject)+'» ?',type:'confirm'},function(){app.delDiscussion(message_id,true);});
			return;
		}
		app.messagesDeleted.push(message.message_id);
	}else{
		if(!confirm){
			app.alert({title:'Quitter les discussions sélectionnées ?',type:'confirm'},function(){app.delDiscussion(message_id,true);});
			return;
		}
		for (var i = 0, lng=app.messages.length ; i<lng; i++){
			var message=app.messages[i];
			if(message.message_parent_message!="-1"){continue;}
			if(document.getElementById("discussion_"+message.message_id+"")){			
				if(document.getElementById("discussion_"+message.message_id+"").checked){
					app.messagesDeleted.push(message.message_id);
				}
			}
		}
	}	
	var liste=[];
	for (var i = app.messages.length - 1; i >= 0; i--) {
		var message=app.messages[i];
		if(app.messagesDeleted.indexOf(message.message_id)>-1 || app.messagesDeleted.indexOf(message.message_parent_message)>-1){
			continue;
		}		
		liste.push(message);
	};
	app.messages=liste;
	if(app.messagesDeleted.indexOf(app.currentDiscussion)>=0){
		app.closeDiscussion();
	}
	app.messagerieInitData();
	$.post(app.serveur + "index.php?go=messagerie&q=delete"+app.connexionParam,{
		ids:JSON.stringify(app.messagesDeleted),
		time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
	}, function(data) {
		app.render(data); 
		app.messagesRender();		
	}
	);
}
app.closeDiscussion=function(){
	$('#messages-view').css('display','none');
	app.currentDiscussion=null;
}
app.lockDiscussion=function(discussion_id,lock,discussion_num){
	if (!app.checkConnection()) {return;}
	app.messages[discussion_num].rm_lock=lock;
	app.messagesRender(); 	
	if(app.currentDiscussion==discussion_id){
		app.renderDiscussionMessages();
	}
	$.post(app.serveur + "index.php?go=messagerie&q=update"+app.connexionParam,{
		discussion_id:discussion_id,
		discussion_lock:lock,
		syncId:app.userConfig.messagesSyncId,
		time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
	}, function(data) {
		app.render(data);  
	}
	);
}
//##########
//TOOLS
//##########
app.messagesSort=function(){
	app.userConfig.messagesSort=(app.userConfig.messagesSort*1+1)%2;
	
	app.pushUserConfig();
	app.renderDiscussion(app.currentDiscussion);
	app.renderDiscussionMessages();
}