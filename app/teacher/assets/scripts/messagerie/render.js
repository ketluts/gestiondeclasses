/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.messagesRender=function(){
	app.messages=app.orderBy(app.messages,"message_date",'ASC');
	var checkedCurrentDiscussion=false;
	var html=[];
	html.push('<table id="messages-table">');
	html.push('<thead>');
	html.push('<tr>');
	html.push('<th class="hide">ID</th>');
	html.push("<th><input type='checkbox' onchange='app.discussionsCheckAll(this.checked);' class='btn btn-control'/></th>"); 
	html.push('<th><span class="glyphicon glyphicon-chevron-right"></span> Sujet</th>');
	html.push('<th><span class="glyphicon flaticon-teamwork"></span> Participants</th>');
	html.push('<th><span class="glyphicon glyphicon-time"></span> Date</th>');
	html.push('<th></th>');
	html.push('</tr>');
	html.push('</thead>');
	html.push('<tbody>');
	for (var i = 0, lng=app.messages.length ; i<lng; i++){
		var message=app.messages[i];
		if(message.message_parent_message!="-1"){continue;}
		if(message.message_id==app.currentDiscussion){
			checkedCurrentDiscussion=true;
		}
		var lock=message.rm_lock;
		html.push('<tr class="tab-row">');
		html.push('<td data-order="0" class="hide">');
		html.push(message.message_id);
		html.push('</td>');
		html.push('<td data-order="0">');
		html.push("<input type='checkbox' id='discussion_"+message.message_id+"' class='btn btn-control' onchange='app.discussionSelectedCount();'/>");
		html.push('</td>');
		html.push('<td class="message_subject col-sm-7" id="message_subject_'+message.message_id+'" data-order="'+message.message_subject+'" onclick="app.renderDiscussion('+message.message_id+');">');
		html.push(ucfirst(message.message_subject));
		html.push('</td>');
		html.push('<td class="col-sm-2" id="message_'+message.message_id+'_participants" onclick="app.renderDiscussion('+message.message_id+');">');
		html.push('</td>');
		html.push('<td class="col-sm-2" id="message_date_'+message.message_id+'" data-order="'+message.message_date*1000+'" onclick="app.renderDiscussion('+message.message_id+');">');
		html.push('le '+moment(parseInt(message.message_date)*1000).format('DD/MM/YY à HH[h]mm'));
		html.push('</td>');
		html.push('<td class="col-sm-1">');
		if(message.message_author==app.userConfig.userID){
			if(lock==1){
				html.push(' <span class="btn btn-default btn-sm connexion-requise" title="Autoriser les réponses." onclick="app.lockDiscussion('+message.message_id+',0,'+i+');">');
				html.push('<span class="glyphicon glyphicon-comment"></span>');
				html.push('</span>');
			}else{
				html.push(' <span class="btn btn-primary btn-sm connexion-requise" title="Bloquer les réponses." onclick="app.lockDiscussion('+message.message_id+',1,'+i+');">');
				html.push('<span class="glyphicon glyphicon-comment"></span>');
				html.push('</span>');
			}
		}
		html.push('<span class="btn btn-default btn-sm pull-right connexion-requise" title="Quitter la discussion." onclick="app.delDiscussion('+message.message_id+',false);">');
		html.push('<span class="glyphicon glyphicon-trash"></span>');
		html.push('</span>');
		html.push('</td>');		
		html.push('</tr>');
	}
	html.push('</tbody>');
	html.push("</table>");
	document.getElementById('messages-liste').innerHTML=html.join('');	
	$('#messages-table').addClass('table table-striped ');
	if(!checkedCurrentDiscussion){
		app.closeDiscussion();
	}	
	app.renderBadgeMessages();
	app.renderParticipants();
}
app.discussionsCheckAll=function(checked){
	for (var i = 0, lng=app.messages.length ; i<lng; i++){
		var message=app.messages[i];
		if(message.message_parent_message!="-1"){continue;}
		if(document.getElementById("discussion_"+message.message_id+"")){
			document.getElementById("discussion_"+message.message_id+"").checked=checked;
			
		}
	}
	app.discussionSelectedCount();	
}
app.discussionSelectedCount=function(){
	var n=0;
	for (var i = 0, lng=app.messages.length ; i<lng; i++){
		var message=app.messages[i];
		if(message.message_parent_message!="-1"){continue;}
		if(document.getElementById("discussion_"+message.message_id+"")){			
			if(document.getElementById("discussion_"+message.message_id+"").checked){
				n++;
			}
		}
	}
	if(n>0){
		$('#discussion-selected-bloc').css('display','block');
		$('#discussion-selected-nb').html(n);
	}else{
		$('#discussion-selected-bloc').css('display','none');
	}
}
app.renderBadgeMessages=function(){	
	var n=0;
	for (var i = app.messages.length - 1; i >= 0; i--) {
		var message=app.messages[i];
		if((message.message_author==app.userConfig.userID && message.message_author_type=="user") || message.message_id<=message.rm_read){
			message.state=true;
		}else{
			n++;
			message.state=false;
		}
	};
	if(n==0){
		document.getElementById('messagerie-badge').style.display="none";
	}else{
		document.getElementById('messagerie-badge').style.display="inline-block";
		document.getElementById('messagerie-badge').innerHTML=n;
	}
}
app.destinatairesRender=function(){
	var html=[];
	for (var i =0,lng= app.destinataires.length; i<lng; i++) {
		var destinataire=app.destinataires[i];
		html.push('<span class="well well-sm legende destinataire" style="border-color:#' + app.getColor(destinataire.destinataire_type+'_1337') + ';">');
		html.push(destinataire.destinataire_nom);
		html.push(' <button class="btn btn-xs btn-default pull-right" onclick="app.destinataireDel(\''+destinataire.destinataire_id+'\',\''+destinataire.destinataire_type+'\');">');
		html.push('<span class="glyphicon glyphicon-trash"></span>');
		html.push('</button>');
		html.push('</span>');
	};
	if(lng==0){
		document.getElementById('destinataires_liste').innerHTML="Aucun destinataire.";
	}else{
		document.getElementById('destinataires_liste').innerHTML=html.join('');
	}
}
app.renderParticipants=function(){
	for (var i =0,lng= app.messages.length; i <lng; i++) {
		var message=app.messages[i];
		if(message.message_parent_message!="-1" && message.message_subject!="~lock"){
			document.getElementById('message_date_'+message.message_parent_message).innerHTML='le '+moment(parseInt(message.message_date)*1000).format('DD/MM/YY à HH[h]mm');
			$('#message_date_'+message.message_parent_message).attr('data-order',-1*parseInt(message.message_date)*1000);
		}
		var html=[];
		if(message.state==false){
			if(message.message_parent_message=="-1"){
				document.getElementById('message_subject_'+message.message_id+'').style.fontWeight ="bold";
			}else{
				if(document.getElementById('message_subject_'+message.message_parent_message+'')){
					document.getElementById('message_subject_'+message.message_parent_message+'').style.fontWeight ="bold";
				}
			}			
		}
		if(message.message_parent_message=="-1"){
			var author='Moi';
			for (var j = message.participants.length - 1; j >= 0; j--) {
				var participant=message.participants[j];
				if(participant.user_pseudo){
					author=ucfirst(participant.user_pseudo);
				}
				else{
					participant.eleve_prenom=participant.eleve_prenom||" ";
					participant.eleve_nom=participant.eleve_nom||" ";
					author=ucfirst(participant.eleve_prenom)+" "+participant.eleve_nom.toUpperCase();
				}
				html.push(author);
			};
			html=html.join(', ');
			document.getElementById('message_'+message.message_id+'_participants').title=html;
			if(html.length>40){
				html=html.substr(0,37)+"...";
			}
			document.getElementById('message_'+message.message_id+'_participants').innerHTML=html;
		}
	};
	$('#messages-table').DataTable({
		stateSave: true,
		language:app.datatableFR,
		"order":[[4,'desc']]
	});
}
app.renderParticipantsListe=function(discussion_num){
	var discussion=app.messages[discussion_num];
	var html=[];
	for (var i = discussion.participants.length - 1; i >= 0; i--) {
		var participant=discussion.participants[i];
		if(participant.user_pseudo){
			var author=participant.user_pseudo;
		}
		else{
			participant.eleve_prenom=participant.eleve_prenom||" ";
			author=participant.eleve_prenom+" "+participant.eleve_nom.toUpperCase();
		}
		html.push('<span class="well well-sm legende destinataire" style="border-color:#' + app.getColor(ucfirst(author)) + ';">');
		html.push(ucfirst(author));
		html.push('</span>');
	};
	document.getElementById('messages-view-participants-liste').innerHTML=html.join('');
}
app.renderMessageForm=function(){
	if (!app.checkConnection()) {return;}
	app.destinataires=[];
	$('#message_send_btn').button('reset');
	document.getElementById('message_subject').value="";
	document.getElementById('message_content').value="";
	document.getElementById('message_form').style.display = "block";
	document.getElementById('destinataires_select').innerHTML="";
	document.getElementById('destinataires_liste').innerHTML="Aucun participant.";
	app.destinatairesForm();
}

app.renderDiscussion=function(discussion_id){	
	app.currentDiscussion=discussion_id;
	document.getElementById('messages-view').style.display = "block";	
	var form='<div>\
	<textarea id="answer-text" class="col-sm-12 form-control" placeholder="Votre réponse"></textarea>\
	<div class="btn-group pull-right">\
	<button id="answer_send_btn" class="btn btn-primary connexion-requise" onclick="$(this).button(\'loading\');app.answerAdd();" data-loading-text="Envoie en cours..."><span class="glyphicon glyphicon-send"> </span> Envoyer</button>\
	</div>\
	</div>';
	var html=[];
	if(app.userConfig.messagesSort==1){
		html.push(form);
	}
	html.push('<div id="messages-view-liste" class="col-sm-12 flex-container"></div>');	
	if(app.userConfig.messagesSort!=1){
		html.push('<hr/>');
		html.push(form);	
	}
	document.getElementById('messages-view-text').innerHTML=html.join('');	
	app.renderDiscussionMessages();
}
app.renderDiscussionMessages=function(){	
	document.getElementById('message_subject_'+app.currentDiscussion+'').style.fontWeight ="normal";
	var lock=null;
	var participants_nb=0;
	//var parent_id=0;
	var markAsRead=false;
	var html=[];
	var discussion_id=app.currentDiscussion;
	var readIdMax=0;
	for (var i =0,lng= app.messages.length ; i <lng ; i++) {
		var message=app.messages[i];
		if(message.message_id!=discussion_id && message.message_parent_message!=discussion_id){
			continue;
		}
		if(message.state==false){
			markAsRead=true;
			message.state=true;
			if(message.message_id>readIdMax){
				readIdMax=message.message_id;
			}
			message.rm_read=message.message_id;
		}
		//Si le message est celui d'origine on retient l'indice
		if(message.message_id==discussion_id){
			document.getElementById('messages-view-subject').innerHTML="<span class='h2'>"+ucfirst(message.message_subject)+"</span>";
			participants_nb=message.participants.length;
			var parent_num=i;
		}		
		//On garde l'indice du dernier message qui contient le blocage ou pas des réponses
		lock=i;
		var message_class="bulle-left";
		var message_author="";
		if(message.user_pseudo){
			message_author=message.user_pseudo;
		}
		else if(message.eleve_nom){
			if(message.eleve_prenom){
				message_author=message.eleve_prenom;	
			}
			message_author+=" "+message.eleve_nom.toUpperCase();
		}
		if(message.message_author==app.userConfig.userID){
			message_class="bulle-left message_highlight";
		}
		var flex_order=i;
		if(app.userConfig.messagesSort==1){
			flex_order=lng-i;
		}
		html.push('<div class="bulle '+message_class+'" style="order:'+flex_order+'; border-color:#'+ app.getColor(ucfirst(message_author))+';">');
		html.push('<div class="bulle_author h4">');
		html.push("<small>");
		html.push(ucfirst(message_author));
		html.push(" - ");
		html.push(''+moment(parseInt(message.message_date)*1000).format('DD/MM/YY à HH[h]mm'));
		html.push("</small>");
		html.push('</div>');
		html.push('<span class="bulle_text">');
		html.push(message.message_content);
		html.push('</span>');
		html.push('</div>');
	};
	document.getElementById('messages-view-liste').innerHTML=html.join('');
	//On désactive si les réponses sont bloquées
	if(lock!==null){
		if(app.messages[lock].rm_lock=="1"){
			document.getElementById('messages-view-subject').innerHTML+="<br/><span class='h3'> <small>(Les réponses sont bloquées)</small></span>";
			if(message.message_author!=app.userConfig.userID || message.message_author_type!='user'){
				document.getElementById('answer_send_btn').disabled="disabled";
				document.getElementById('answer-text').value="Les réponses sont bloquées."
				document.getElementById('answer-text').disabled="disabled";	
			}	
		}else{
			if(document.getElementById('answer_send_btn').disabled=="disabled"){
				document.getElementById('answer-text').value="";
				document.getElementById('answer_send_btn').disabled="";
				document.getElementById('answer-text').removeAttribute("disabled");
			}
		}
	}
	//On marque les messages comme lus
	app.renderBadgeMessages();
	if(markAsRead && app.isConnected){
		$.post(app.serveur + "index.php?go=messagerie&q=update"+app.connexionParam,{
			discussion_id:discussion_id,
			discussion_read:readIdMax,
			syncId:app.userConfig.messagesSyncId,
			sessionParams:app.sessionParams
		}, function(data) {
			app.render(data);  
		}
		);
	}
	//On affiche les participants
	var html=[];
	html.push('<div class="btn-group">');
	html.push("<button class='btn btn-default btn-md' data-toggle=\"collapse\" data-target=\"#messages-view-participants\" aria-expanded=\"false\" aria-controls=\"messages-view-participants\">Participant"+app.pluralize(participants_nb,'s')+" <span class='badge'>"+participants_nb+"</span> </button>");
	html.push('<button class="btn btn-default btn-md" onclick="app.messagesSort();"><span class="glyphicon glyphicon-sort"></span></button>');
	html.push('</div>');
	html.push('<button class="btn btn-default pull-right" onclick="app.closeDiscussion();$(\'#app\').goTo();">\
       <span class="glyphicon glyphicon-remove"></span> Fermer\
     </button>');
	
	document.getElementById('messages-view-toolbar').innerHTML=html.join('');
	app.renderParticipantsListe(parent_num);
}