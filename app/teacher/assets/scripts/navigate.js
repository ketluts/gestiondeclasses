/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

 (function($) {
  $.fn.goTo=function() {
   $('html, body').animate({
     scrollTop: ($(this).offset().top*1-60) + 'px'
   }, 'fast');
   return this; 
 }
})(jQuery);

app.go=function(hash){
 if(hash==app.currentHash){
  $('#app').goTo();
  return;
}
window.location.hash=hash;
}
app.navigationInit=function(){
  app.urlHash=window.location.hash.split('#')[1];
  window.onhashchange=function(){
   app.navigate();
 }
}
app.navigate=function(hash,force){
 hash=hash||window.location.hash.split('#')[1];
 if(hash==app.currentHash && !force){$('#app').goTo(); return;}
 app.currentHash=hash;
 if(!hash){
  app.home();
  return;
}
var dir=hash.split('/');
if(dir.length==0){
  app.home();
  return;
}

if(app.routes[dir[0]]){
  app.routes[dir[0]](dir);
}else{
  app.home();
}
$(document.body).trigger("sticky_kit:recalc");
return;

}
app.routes={
  home:function(dir){
    app.home();  
  },
  classroom:function(dir){
   var id=dir[1];
   if(id==undefined){app.go('home');}
   app.classeRender(id);
   $('#app').goTo();
 },
 student:function(dir){
  var id=dir[1];
  if(id==undefined){app.go('home');}
  app.renderEleve(id);
},
skills:function(dir){
  if(app.currentView!="competences"){
   app.skillsViewInit();
 }   
 var view=dir[1];
 var id=dir[2];
 if(view=="classroom"){
  if(id==undefined){app.go('home');}      
  app.itemsRenderProgressionByClasse(id);
}else if(view=="student"){
  if(id==undefined){app.go('home');}
  app.itemsRenderProgressionByEleve(id);
}else{     
  app.itemsProgressionsInit();
}      
},
challenges:function(){
  app.getDefis();
},
mailbox:function(){
  app.getMessages();
},
shares:function(){
  app.renderPageShares();
},
admin:function(){
  app.renderAdminConfig();
},
user:function(dir){
var view=dir[1]; 
  app.renderUserConfig();
},
bilans:function(){
  app.bilansInit();
}
};
var QueryString = function () {
  // This function is anonymous, is executed immediately and 
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
          query_string[pair[0]] = decodeURIComponent(pair[1]);
        // If second entry with this name
      } else if (typeof query_string[pair[0]] === "string") {
        var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
        query_string[pair[0]] = arr;
        // If third or later entry with this name
      } else {
        query_string[pair[0]].push(decodeURIComponent(pair[1]));
      }
    } 
    return query_string;
  }();