/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.defiActivationParClasse=function(defi_id,classe_id){
	if (!app.checkConnection()) {
		$('#defi_activation_btn').button('reset');
		return;
	}
	var etoiles=[];
	var classe=app.getClasseById(classe_id);
	var eleves=classe.eleves;
	for (var i = eleves.length - 1; i >= 0; i--) {
		var eleve=eleves[i];
		if(document.getElementById('etoile_'+defi_id+'_'+eleve+'').value!=-1){
			continue;
		}
		var temp_etoile=[];
		temp_etoile.push(eleve);
		temp_etoile.push(0);
		etoiles.push(temp_etoile);
	};
	$.post(app.serveur + "index.php?go=etoiles&q=add"+app.connexionParam, {
		etoiles:JSON.stringify(etoiles),
		etoile_type:'defi',
		defi_id:defi_id,
		time:Math.floor(app.myTime()/1000),
		sessionParams:app.sessionParams
	},
	function(data) { 
		app.render(data);  
		app.renderDefiEtoilesForm(app.defisCurrentClasse,defi_id);
	}
	);
};
app.defisSetEtoileByEleve=function(eleve_id,defi_id,etoile_value){
	if (!app.checkConnection()) {return;}
	var etoiles=[];
	var temp_etoile=[];
	temp_etoile.push(eleve_id);
	temp_etoile.push(etoile_value);
	etoiles.push(temp_etoile);
	var defi=app.getDefiById(defi_id);
	if(etoile_value<-1 || etoile_value>defi.defi_etoiles){
		return;
	}
	$.post(app.serveur + "index.php?go=etoiles&q=add"+app.connexionParam, {
		etoiles:JSON.stringify(etoiles),
		etoile_type:'defi',
		defi_id:defi_id,		
		time:Math.floor(app.myTime()/1000),
		sessionParams:app.sessionParams
	},
	function(data) { 
		app.render(data);  
		app.renderDefiEtoilesForm(app.defisCurrentClasse,defi_id);
		app.defisClassementParClasse();
	}
	);
};
app.getEtoiles=function(classe_id,defi_id){
	if (!app.checkConnection()) {return;}
	if(classe_id=="null"){
		document.getElementById('defi_classe').innerHTML="";
		return;
	}
	document.getElementById('defi_classe').innerHTML=app.renderLoader();
	app.renderDefiEtoilesForm(classe_id,defi_id);
};
app.renderDefiEtoilesForm=function(classe_id,defi_id){
	app.defisCurrentClasse=classe_id;
	var classe=app.getClasseById(classe_id);
	var etoiles=[[],[]];
	var modification_time=0;
	var defi=app.getDefiById(defi_id);
	for (var i = app.etoiles.length - 1; i >= 0; i--) {
		var etoile=app.etoiles[i];
		if(classe.eleves.indexOf(etoile.etoile_eleve)<0){continue;}
		if(etoile.etoile_defi==defi.defi_id && etoile.etoile_type=="defi"){
			etoiles[0].push(etoile.etoile_eleve);
			etoiles[1].push(etoile.etoile_value);
			if(etoile.etoile_date>modification_time){
				modification_time=etoile.etoile_date;
			}
		}
	};
	var defi_activation_btn=false;
	var html=[];
	html.push('<table class="table" style="margin:auto;">');
	html.push('<tr id="defi_activation_tr">');
	html.push('<td>');
	html.push('</td>');
	html.push('<td class="text-center">');
	html.push("<button class='btn btn-default' id='defi_activation_btn' data-loading-text='Activation...' onclick='app.defiActivationParClasse("+defi.defi_id+","+classe_id+");$(this).button(\"loading\");'>Activer pour tous</button>");
	html.push('</td>');
	html.push('</tr>');
	var eleves=classe.eleves;
	for (var i=0, lng= eleves.length ; i< lng; i++) {
		var eleve=app.getEleveById(eleves[i]);
		var currentValue=-1;
		var selected="";
		var id=eleve.eleve_id;
		if(etoiles[0] && etoiles[0].indexOf(eleve.eleve_id)!=-1){
			currentValue=etoiles[1][etoiles[0].indexOf(eleve.eleve_id)];
		}	
		html.push('<tr>');
		html.push('<td>');
		html.push(app.renderEleveNom(eleve));
		html.push('</td>');
		html.push('<td class="text-center">');
		html.push('<input type="hidden" value="'+currentValue+'" id="etoile_'+defi.defi_id+'_'+eleve.eleve_id+'"/>');
		if(currentValue>=0){
			html.push('<div class="flex-rows">');
			html.push("<button class='btn btn-default btn-xs' onclick='app.defisSetEtoileByEleve("+eleve.eleve_id+","+defi.defi_id+","+(currentValue*1-1)+");'><span class='glyphicon glyphicon-minus'></span></button>");
			html.push('<div class="flex-1">');
			html.push(app.renderEtoiles(currentValue,defi.defi_etoiles,0));
			html.push('</div>');
			html.push("<button class='btn btn-default btn-xs' onclick='app.defisSetEtoileByEleve("+eleve.eleve_id+","+defi.defi_id+","+(currentValue*1+1)+");'><span class='glyphicon glyphicon-plus'></span></button>");
			html.push('</div>');
		}else{
			defi_activation_btn=true;
			html.push("<button class='btn btn-default' onclick='app.defisSetEtoileByEleve("+eleve.eleve_id+","+defi.defi_id+",0);'>Activer le défi</button>");
		}
		html.push('</td>');
		html.push('</tr>');
	}
	html.push('</table>');
	document.getElementById('defi_classe').innerHTML=html.join('');
	if(!defi_activation_btn){
		$('#defi_activation_tr').css('display','none');
	}
};
app.renderEtoiles=function(nb_etoiles,max,used){
	var html=[];
	var used=used||0;
	var max=max||nb_etoiles;
	for (var i = 0; i<max; i++) {
		if(i<used){
			html.push('<img class="etoile-icon" src="assets/img/etoile_use.png" width="20" alt="Pièce utilisée"/>');
		}
		else if(i<nb_etoiles){
			html.push('<img class="etoile-icon" src="assets/img/etoile_on.png" width="20" alt="Pièce brillante"/>');
		}
		else{
			html.push('<img class="etoile-icon" src="assets/img/etoile_off.png" width="20" alt="Pièce éteinte"/>');
		}
	};
	return html.join('');
};