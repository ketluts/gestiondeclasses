/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.getDefis=function(){	
	if (!app.checkConnection()) {return;}
	app.viewClear();
	document.getElementById('challenges-liste').innerHTML=app.renderLoader();
	document.getElementById('template_defis').style.display = "block";
	document.getElementById('defi_form').style.display = "none";
	document.getElementById('titre').innerHTML = 'Mes défis';
	app.renderDefis();
	app.defisClassementParClasse();
};
app.renderDefis=function(){
	var defis=app.defis;
	document.getElementById('defi_form').style.display = "none";
	$('#challenges-edit-form').css('display','none');
	var source   = document.getElementById("template-defis-liste").innerHTML;
	var template = Handlebars.compile(source);
	var context = {
		nom_etablissement:app.nom_etablissement,
		serveur:app.serveur,
		defis:defis
	};		
	Handlebars.registerHelper('defi_date_html', function() {		
		return moment(parseInt(this.defi_date)*1000).format('DD/MM/YYYY');
	});
	Handlebars.registerHelper('defi_etoiles_html', function() {	
		return new Handlebars.SafeString(app.renderEtoiles(this.defi_etoiles));		
	});
	document.getElementById('challenges-liste').innerHTML=template(context);
};
app.renderClassesDefi=function(defi_id){
	$('#challenges-edit-form').css('display','block');
	$('#challenges-edit').goTo();
	var defi=app.getDefiById(defi_id);
	var html=[];
	html.push('<div class="btn btn-default btn-close" onclick="$(\'#challenges-edit-form\').css(\'display\',\'none\');">\
		<span class="glyphicon glyphicon-remove"></span>\
		</div>');
	html.push("<span class='h3'>"+defi.defi_titre+" <small>Attribuer des pièces</small></span><br/><br/>");
	//html.push('<div id="defi_date_'+defi.defi_id+'"></div><br/>');
	html.push('<select onchange="app.getEtoiles(this.value,'+defi_id+');" class="form-control">');
	html.push('<option value="null">Sélectionnez une classe</option>');
	for (var i=0, lng= app.classes.length ; i< lng; i++) {
		var classe=app.classes[i];
		if(app.userConfig.classesDisable.indexOf(classe.classe_id)!=-1){continue;}
		html.push('<option value="'+classe.classe_id+'">');
		html.push(app.cleanClasseName(classe.classe_nom));
		html.push('</option>');
	};
	html.push('</select><br/>');
	html.push('<div id="defi_classe">');
	html.push('</div>');	
	document.getElementById('challenges-edit-form').innerHTML=html.join('');
};
