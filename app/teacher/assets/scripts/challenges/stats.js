/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.defisClassementParClasse=function(){
	var piecesParEleve=[];
	for (var i = 0; i < app.etoiles.length; i++) {
		var etoile=app.etoiles[i];
		piecesParEleve[etoile.etoile_eleve]=piecesParEleve[etoile.etoile_eleve]||0;
		piecesParEleve[etoile.etoile_eleve]+=etoile.etoile_value*1;
	}
	var piecesParClasse=[];
	for (var i = 0; i < app.classes.length; i++) {
		var classe=app.classes[i];
		if(classe.classe_id<0){continue;}
		var eleves=classe.eleves;
		for (var j = 0; j < eleves.length; j++) {
			var eleve_id=eleves[j];
			if(piecesParEleve[eleve_id]){
				piecesParClasse[classe.classe_id]=piecesParClasse[classe.classe_id]||0;
				piecesParClasse[classe.classe_id]+=piecesParEleve[eleve_id]*1;
			}
		}
	}
	var source   = document.getElementById("template-defis-classement-classes").innerHTML;
	var template = Handlebars.compile(source);
	var context = {
		classes:app.classes
	};	
	Handlebars.registerHelper('classe_pieces_html', function() {	
		var pieces=0;
		if(piecesParClasse[this.classe_id]){
			pieces=piecesParClasse[this.classe_id];
		}
		return pieces;		
	});
	Handlebars.registerHelper('classe_enable', function() {			
		if(this.classe_id<0){return false;}
		var pieces=0;
		if(piecesParClasse[this.classe_id]){
			pieces=piecesParClasse[this.classe_id];
		}
		if(pieces<=0){
			return false;
		}	
		return true;
	});
	document.getElementById('challenges-classement-classes').innerHTML=template(context);

	var source=document.getElementById("template-defis-classement-eleves").innerHTML;
	var template = Handlebars.compile(source);
	var context = {
		eleves:app.eleves
	};	
	Handlebars.registerHelper('eleve_pieces_html', function() {	
		var pieces=0;
		if(piecesParEleve[this.eleve_id]){
			pieces=piecesParEleve[this.eleve_id];
		}
		return pieces;		
	});
	Handlebars.registerHelper('eleve_nom_html', function() {			
		return new Handlebars.SafeString(app.renderEleveNom(this));		
	});
	Handlebars.registerHelper('eleve_enable', function() {			
		var pieces=0;
		if(piecesParEleve[this.eleve_id]){
			pieces=piecesParEleve[this.eleve_id];
		}
		if(pieces<=0){
			return false;
		}
		return true;		
	});
	document.getElementById('challenges-classement-eleves').innerHTML=template(context);
	$('.challenges-classement-tab').dataTable({
		"language":app.datatableFR,
		searching: false,
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
		"order": [[ 1, "desc" ]],
		"pageLength": 3,
		"dom": 'tp'
	});
}
