/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.setDefiIcon=function(icon){
	document.getElementById('defi_icon').value=icon;
	app.renderApercuDefi();
}
app.addDefis=function(){
	if (!app.checkConnection()) {return;}	
	var defi_titre=document.getElementById('defi_titre').value;
	var defi_description=document.getElementById('defi_description').value;
	var defi_tags=document.getElementById('defi_tags').value;
	var defi_icon=document.getElementById('defi_icon').value;
	var defi_etoiles=document.getElementById('defi_etoiles').value;
	var defi_url=document.getElementById('defi_url').value;
	$.post(app.serveur + "index.php?go=defis&q=add"+app.connexionParam,
	{
		defi_titre:defi_titre,
		defi_description:defi_description,
		defi_tags:defi_tags,
		defi_etoiles:defi_etoiles,
		defi_icon:defi_icon,
		defi_url:defi_url,
		time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
	}, function(data) {
		$('#defi_save_btn').button('reset');
		app.render(data);   
		app.renderDefis(); 
	}
	);
};
app.delDefi=function(defi_id,confirm){
	if (!app.checkConnection()) {return;} 
	if(!confirm){
		$('.defi_delete_btn').button('reset');
		app.alert({title:'Supprimer ce défi ?',type:'confirm'},function(){app.delDefi(defi_id,true);});
		return;
	}
	$('.defi_delete_btn').button('loading');
	$.post(app.serveur + "index.php?go=defis&q=delete"+app.connexionParam,{
		defi_id:defi_id,
sessionParams:app.sessionParams
}, function(data) {
		app.render(data);  
		app.renderDefis(); 
		app.defisClassementParClasse();
	}
	);
};
app.updDefi=function(defi_id){
	if (!app.checkConnection()) {return;}
	$('#challenges-edit-form').css('display','block');
	$('#challenges-edit').goTo();	
	var defi=app.getDefiById(defi_id);
	var source   = document.getElementById("template-defis-edition").innerHTML;
	var template = Handlebars.compile(source);
	var context = {
		defi:[defi]
	};		
	document.getElementById('challenges-edit-form').innerHTML=template(context);
	$('#challenges-edit-help').css('display','none');
};
app.defiEditSave=function(defi_id){
	if (!app.checkConnection()) {return;}
	var titre=document.getElementById('defi_'+defi_id+'_titre').value;
	var description=document.getElementById('defi_'+defi_id+'_description').value;
	var url=document.getElementById('defi_'+defi_id+'_url').value;
	$.post(app.serveur + "index.php?go=defis&q=update"+app.connexionParam,{
		defi_id:defi_id,
		defi_titre:titre,
		defi_description:description,
		defi_url:url,
sessionParams:app.sessionParams
}, function(data) {
		app.render(data);  
		app.renderDefis(); 
		document.getElementById('challenges-edit-form').innerHTML="";
		$('#challenges-edit-help').css('display','block');
	}
	);
};
app.renderDefiForm=function(){
	if (!app.checkConnection()) {return;}
	$('#defi_save_btn').button('reset');
	document.getElementById('defi_form').style.display = "block";
	document.getElementById('defi_titre').value="";
	document.getElementById('defi_description').value="";
	document.getElementById('defi_tags').value="";
	document.getElementById('defi_etoiles').value="1";
	document.getElementById('apercu_defi_icon').innerHTML='<img src="assets/lib/educational-icons/svg/education34.svg"/><br/><span class="etoile_on"></span>';
	var html=[];
	for (var i = app.defisIcons.length - 1; i >= 0; i--) {
		html.push('<div class="btn btn-default btn-ms defi_icon_form" onclick="app.setDefiIcon(\''+app.defisIcons[i]+'\');"><span class=" glyphicon flaticon-'+app.defisIcons[i]+'"></span></div>');
	};
	document.getElementById('defi_icons_LST').innerHTML=html.join('');
};
app.renderApercuDefi=function(){
	var defi_titre=document.getElementById('defi_titre').value;
	var defi_description=document.getElementById('defi_description').value;
	var defi_tags=document.getElementById('defi_tags').value;
	var defi_icon=document.getElementById('defi_icon').value;
	var defi_etoiles=document.getElementById('defi_etoiles').value;
	if(defi_titre==""){defi_titre="Titre";}
	if(defi_description==""){defi_description="Description";}
	document.getElementById('apercu_defi_titre').innerHTML=defi_titre;	
	document.getElementById('apercu_defi_description').innerHTML=defi_description;
	document.getElementById('apercu_defi_icon').innerHTML="<img src='assets/lib/educational-icons/svg/"+defi_icon+".svg'/><br/>"+app.renderEtoiles(defi_etoiles);
};