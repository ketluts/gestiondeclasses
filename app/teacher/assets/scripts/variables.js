/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};

app.nom_etablissement="";
app.pseudo_utilisateur="";
app.etablissementsCRT=true;
app.loadUi=0;

app.itemsTagsConfig=[];
app.logo="<img src='assets/favicons/apple-touch-icon-57x57.png' width='40px'>";
app.plugins=app.plugins||[];
//**********
// Connexion
//*********
app.connexionParam = "";

app.init=function(){  
//**********
// App
//*********
app.version = "v20191106";
app.currentMode="";
app.platform="web";
app.sessionID=null;
app.isConnected =true;
app.urlHash="";
app.mouse={
  x:0,
  y:0 
};
app.timerPing=null;
app.active_periode=null;
app.periodes=[];
app.update = [];
app.users=[];
app.usersIndex=[];
app.stats={
  classes:0,
  eleves:0,
  events:0,
  notes:0,
  etoiles:0  
};
app.pi=Math.PI;
app.cacheColor=[];
app.legends=[];
clearTimeout(app.discussionTimer);
app.discussionTimer=null;
app.discussionRefreshTime=60000;
app.enableKeyboardShorcuts=true;
app.editors=[];

//**********
// UI
//*********
app.currentHash="";
app.colorCalendarBy = "user";
app.currentView="";
app.currentDiscussion=null;
app.homeCurrentEditionWeek="AB";
app.zindex=10;
app.zindexBlock=false;
app.newAlertWatched=[];
app.newAlertTrigerred="";
app.weeks=0;
//**********
// Home
//*********
app.homeClassesListe=[];
//**********
// Classe
//*********
app.elevesByClasses=[];
app.classes = [];
app.currentClasse = null;
app.classeTimeOut=null;
app.currentClasseView="liste";
//**********
// Eleves
//*********
//app.eleves_sans_classe=[];
app.elevesIndex=[];
app.eleves=[];
app.currentClasseCalendarDate=null;
app.currentEleve = null;
app.notesEventsSource=[];
app.etoilesEventsSource=[];
app.eleveEventsList=[];
app.eleveEventsListCurrent="";
//**********
// Bilans
//*********
app.bilansStudentsList=[];


//**********
// Notes
//*********
app.controles=[];
app.notes=[];
app.controlesCategoriesDatalist=[];
app.myNotes=[];
//**********
// Config
//*********
app.configCurrentUser=null;
app.cdtCurrentClasse=null;
app.configUsersImport=[];
app.disciplines=[];
app.disciplinesIndex=[];
//**********
// Defis
//*********
app.etoiles=[];
app.defis=[];
app.defisCurrentClasse=null;
//**********
// Items
//*********
app.items=[];
app.itemsIndex=[];
app.itemsCurrentClasse=null;
app.itemsCurrentEleve=null;
app.itemsProgressions=[];
app.itemsMode="auto";
app.itemsTagsDatalist=[];
app.itemsUsersDatalist=[];
app.itemsCategoriesDatalist=[];
app.itemsSousCategoriesDatalist=[];
app.progressionActivitesDatalist=[];
app.itemsToEdit=null;
app.itemsCategories=[];
app.itemsVue="cursor";
app.itemsSelection=[];
app.itemsEditionTagsMode="add";
app.itemsNbFiltres=6;
app.cptElevesSelectedLock=true;
app.cptElevesSelectedNum=0;
app.itemsResultsImport=[];
app.itemsResultsImportNb=0;
app.categories_progression=[];
app.itemEditionTags=[];
app.itemEditionTagsCurrent=[];
app.itemsItemToolbarVisibleBtn=[];
app.itemsItemToolbarAutoEvalBtn=[];
app.itemsTagsFamilies=[];
app.itemsStudentGraph=false;
app.currentFeedbacks=null;
app.feedbacks=[];
app.feedbacksIndex=[];
//app.itemsStudentActivities=false;
app.itemsClassroomTab=false;
app.itemsImport=false;
app.itemsCurrentSymbole='square';
app.itemsDefaultFilter={
  name:"current",
  periode:-1,
  cycle:0,
  filterRated:"all",
  tags:{
    include:[],
    exclude:[],
    mode:"tags"
  },
  niveaux:{
    min:0,
    max:100
  },
  search:"",
  selection:[]
};
app.itemsCurrentFilter=clone(app.itemsDefaultFilter);
app.cycles=[
{
  "nom":"Cycle 1 - Début",
  "code":"1D"
},
{
  "nom":"Cycle 1 - Milieu",
  "code":"1M"
},
{
  "nom":"Cycle 1 - Fin",
  "code":"1F"
},
{
  "nom":"Cycle 2 - Début",
  "code":"2D"
},
{
  "nom":"Cycle 2 - Milieu",
  "code":"2M"
},
{
  "nom":"Cycle 2 - Fin",
  "code":"2F"
},
{
  "nom":"Cycle 3 - Début",
  "code":"3D"
},
{
  "nom":"Cycle 3 - Milieu",
  "code":"3M"
},
{
  "nom":"Cycle 3 - Fin",
  "code":"3F"
},
{
  "nom":"Cycle 4 - Début",
  "code":"4D"
},
{
  "nom":"Cycle 4 - Milieu",
  "code":"4M"
},
{
  "nom":"Cycle 4 - Fin",
  "code":"4F"
}
];
app.itemsCyclesCoeffsMalus={
  "star1":{
    "D":1,
    "M":0.8,
    "F":0.4
  },
  "star2":{
    "D":1,
    "M":1,
    "F":0.8
  },
  "star3":{
    "D":1,
    "M":1,
    "F":1
  }
};
app.itemsCyclesCoeffs={
  "square":1,
  "star1":1,
  "star2":1,
  "star3":1
};
//**********
// Random Eleve
//*********
app.currentEleves=[];
app.randomEleve={};
app.randomEleve.n=0;
app.randomEleve.k=0;
app.randomEleve.enable=true;
app.randomEleve.eleves=[];
app.randomEleve.selectInGroupe=0;
//**********
// Sociogramme
//*********
app.relations=[];
app.sociogramme={
  width:($(window).width())*0.75,
  current:null,
  cote:900,
  zoom:1,
  centerX:450,
  centerY:450,
  centres:[],
  vue:"",
  mode:"students",
  relationsView:"userView",
  eleves:[],
  rangs:[],
  relations:[],
  enableDraw:true,
  enable_move:false,
  selectedEleve:null,
  selectedCentre:null,
  step:null,
  filtre:null,
  ctx:null
};
app.preventTouch=false;

//**********
// Files
//*********
app.files=[];
app.currentShareItem=null;
app.quotaUsed=0;
app.quotaByUser=0;
app.upload_max_filesize=0;
app.nbFilesToUpload=[];
app.nbFilesUploaded=0;
app.filesToUpload=[];
//**********
// Filters
//*********
app.filters=[];
//**********
// Links
//*********
app.links=[];
//**********
// Messagerie
//*********
app.messages=[];
app.destinataires=[];
app.messagesIndex=[];
app.messagesDeleted=[];
//**********
// Agenda
//*********
app.agenda=[];
app.agendaNewEventIcon="";
app.agendaNewEventStart=null;
app.agendaNewEventEnd=null;
app.agendaEventsSource=[];
app.agendaCurrentEventEdit=null;
app.agendaNewEventAllDay=false;
app.agendaEventCopy=null;
app.agendaExportType=[];
app.agendaTitreDatalist=[];
app.homeAgendaTooltip=null;
app.classeAgendaTooltip=null;
app.eleveAgendaTooltip=null;
app.pubicAgendaTooltip=null;
app.edtEditionMode=false;
app.agendaNextDaysNb=4;
app.edtMinTime=null;
app.edtMaxTime=null;
app.agendaEventHomeBackGround="#cbcbcb";
app.agendaEventCommentBackGround="#b8b8b8";
app.agendaEventBlackBoardBackGround="#ffffff";
//**********
// Events
//*********
app.eventsAvailable = [
{type:"glyphicon-briefcase",placeholder:"Oubli"},
{type:"glyphicon-pencil",placeholder:"Devoirs non faits"},
{type:"glyphicon-thumbs-up",placeholder:"Bonne attitude"},
{type:"glyphicon-thumbs-down",placeholder:"Mauvaise attitude"},
{type:"glyphicon-user",placeholder:"Absence"},
{type:"glyphicon-time",placeholder:"Retard"},
{type:"glyphicon-file",placeholder:"Punition"},
{type:"glyphicon-plus",placeholder:"Bonus"},
{type:"glyphicon-minus",placeholder:"Malus"},
{type:"glyphicon-earphone",placeholder:"Appel"},
{type:"glyphicon-share",placeholder:"Exclusion de cours"},
{type:"glyphicon-pushpin",placeholder:"Retenue"},
{type:"glyphicon-home",placeholder:"DM"},
{type:"glyphicon-ok",placeholder:"Validation"},
{type:"glyphicon-comment",placeholder:"Bavardages"}
];
app.timers=[];
//**********
// Datatable
//*********
app.datatableFR={    "sProcessing":     "Traitement en cours...",    "sSearch":         "",    "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",    "sInfo":           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",    "sInfoEmpty":      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",    "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",    "sInfoPostFix":    "",    "sLoadingRecords": "Chargement en cours...",    "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",    "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",    "oPaginate": {        "sFirst":      "Premier",        "sPrevious":   "Pr&eacute;c&eacute;dent",        "sNext":       "Suivant",        "sLast":       "Dernier"    },    "oAria": {        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"    }};
//**********
// User
//*********
app.users=[];
app.userConfig={
  eventsWhiteList:[
  'glyphicon-briefcase',
  'glyphicon-pencil',
  'glyphicon-thumbs-up',
  'glyphicon-thumbs-down'
  ],
  sorting:0,
  coloration:0,
  version:app.version,
  userID:0,
  ppView : false,
  matiere:-1,
  messagesSyncId:0,
  messagesSort:0,
  classesDisable:[],
  randomEleves:[],
  homeCurrentView:'liste',
  agendaCurrentView:"basicThreeDay",
  eleveAgendaCurrentView:"basicThreeDay",
  classeEventsView:"calendar",
  alerts:[],
  edt:[],
  currentEleveView:"eleve_calendar",
  itemsAvisMode:'me',
  agendaWeekEnds:false,
  agendaEventDetails:true,
  itemsFilterCurrent:{},
  itemsTagsFamilies:[],
  itemsSyntheseMode:'categories',
  itemsOrder:[],
  sociogrammes:[],
  sociogramme_type:'simple',
  groupes:[],
  plans:[],
  memos:[],
  jaugesFilters:[]
};

//Init UI
$('.btn').prop("disabled", false);
if(app.loadUi==0)
{
app.uiInit();
}

}
//###########
//Intelligences multiples
app.intelligences=[
{
  name:"Verbale et linguistique"
},
{
  name:"Spatiale et visuelle"
},
{
  name:"Corporelle et kinesthésique"
},
{
  name:"Naturaliste"
},
{
  name:"Intra-personnelle"
},
{
  name:"Logico-mathématique"
},
{
  name:"Inter-personnelle"
},
{
  name:"Musicale et rythmique"
}
];
app.defisIcons=[
'a13',            
'concentric',     
'marker12',      
'saturn11',
'abacus6',        
//'danger4',        
'maths5',        
'scale11',
'alarm27',        
'diploma7',       
'maths6',        
'school46',
//'apple54',        
'dna9',           
'medal45',       
'school47',
'ascending7',     
'documents11',    
'microscope12',  
'science15',
'atom18',         
'draft1',         
'missing1',      
'search50',
'attach3',        
'earth180',       
'mouse35',       
'semicircular2',
'award4',         
'ebook10',        
'music196',      
'send4',
'backpack5',      
//'education34',    
'musical81',     
'soccer34',
'bad7',           
'educational24',  
//'new47',         
'soccer35',
'ballpoint2',     
'educational25',  
'notepad10',     
'statistics4',
'basketball32',   
'envelope33',     
'notes16',       
'student49',
'bell30',         
'eyeglasses13',   
'online15',      
'student50',
'book154',        
'find3',          
'online16',      
'student51',
'boy31',          
'first31',        
'opened13',      
'student52',
'briefcase38',    
'football147',    
'opened14',      
'synchronization',
'brush19',        
//'frog4',          
//'owl11',         
'syringe20',
'bus25',          
'gear25',         
'painter11',     
'talk8',
'calculator44',   
'gear26',         
'pen48',         
'teacher35',
'calendar135',    
'girl19',         
'pencil66',      
'teamwork',
'calligraphy16',  
'good2',          
'pendrive6',     
'telescope7',
'cell9',          
//'good',           
'person277',     
'test20',
'cells1',         
'graduate20',     
'persons8',      
'think2',
'chair10',        
'idea13',         
'photo146',      
'thoughts',
'chalkboard',     
'infinite4',      
'physics3',      
'three108',
'checklist1',     
'lab2',           
'piano15',       
//'thumb30',
'chemistry11',    
'lamp14',         
'pin34',         
'time12',
'circular151',    
'landscape6',     
'poetry',        
//'tree100',
'circular152',    
'laptop97',       
'puzzle26',      
'trophy17',
'circular153',    
'law2',           
'question43',    
'trumpet12',
//'code26',         
'leaves9',        
'rating',        
'university7',
'comedy2',        
'letter40',       
//'religious4',    
'verified9',
'compass64',      
'line37',         
'rocket60',      
'video132',
'compass65',      
'male208',        
'ruler14',       
'write12',
'computer146',    
'map77',          
'sandwich',      
'writing19']
;

app.toolbarOptions = [
  [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
  //['blockquote', 'code-block'],
  //[{ 'header': 1 }, { 'header': 2 }],               // custom button values
  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
 // [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
  [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
  //[{ 'direction': 'rtl' }],                         // text direction
  //[{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
  // [{ 'font': [] }],
  [{ 'align': [] }],
  ['clean']                                         // remove formatting button
  ];