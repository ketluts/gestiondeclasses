/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.trim=function(myString)
{
  return myString.replace(/^\s+/g,'').replace(/\s+$/g,'');
} 
function dump(obj) {
  var out = '';
  for (var i in obj) {
    if(typeof obj[i]===typeof {}){
      out+=dump(obj[i])+ "\n";
    }else{
     out += i + ": " + obj[i] + "\n";
   }
 }
 return out;   
}
function hashCode(str) { 
  str += '00000';
  str=hex_md5(str);
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }

  return Math.abs(hash);
}
function intToRGB(i) {
  var color= ((i >> 24) & 0xFF).toString(16) +
  ((i >> 16) & 0xFF).toString(16) +
  ((i >> 8) & 0xFF).toString(16);
  while(color.length<6){
    color="0"+color;
  }
  return color;
}
function arrayUnique(array) {
  var a = array.concat();
  for(var i=0; i<a.length; ++i) {
    for(var j=i+1; j<a.length; ++j) {
      if(a[i] === a[j])
        a.splice(j--, 1);
    }
  }
  return a;
} 
function ucfirst(str)
{
  if(str=="" || !str){return "";}
  return str.charAt(0).toUpperCase() + str.substr(1);
}
/**
 * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
 * @param obj1
 * @param obj2
 * @returns obj3 a new object based on obj1 and obj2
 */
 // function merge_options(obj1,obj2){
 //  var obj3 = {};
 //  for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
 //    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
 //      return obj3;
//  }
function clone(obj) {
  var copy;
  // Handle the 3 simple types, and null or undefined
  if (null == obj || "object" != typeof obj) return obj;
  // Handle Date
  if (obj instanceof Date) {
    copy = new Date();
    copy.setTime(obj.getTime());
    return copy;
  }
  // Handle Array
  if (obj instanceof Array) {
    copy = [];
    for (var i = 0, len = obj.length; i < len; i++) {
      copy[i] = clone(obj[i]);
    }
    return copy;
  }
  // Handle Object
  if (obj instanceof Object) {
    copy = {};
    for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
    }
    return copy;
  }
  throw new Error("Unable to copy obj! Its type isn't supported.");
}
function getMaxOfArray(numArray) {
  return Math.max.apply(null, numArray);
}
function getMinOfArray(numArray) {
  return Math.min.apply(null, numArray);
}  
app.orderBy=function(array,orderBy,order){
  app.orderByParam=orderBy;
  if(order=="ASC"){
    return array.sort(function(a,b){
      if(a[app.orderByParam]>b[app.orderByParam]) return 1;
      if(a[app.orderByParam]<b[app.orderByParam]) return -1;
      return 0;
    });
  }
  return array.sort(function(a,b){
    if(a[app.orderByParam]>b[app.orderByParam]) return -1;
    if(a[app.orderByParam]<b[app.orderByParam]) return 1;
    return 0;
  });
}

/**
* Convert number of bytes into human readable format
*
* @param integer bytes     Number of bytes to convert
* @param integer precision Number of digits after the decimal separator
* @return string
*/
function bytesToSize(bytes, precision)
{  
  var kilobyte = 1024;
  var megabyte = kilobyte * 1024;
  var gigabyte = megabyte * 1024;
  var terabyte = gigabyte * 1024;
  if ((bytes >= 0) && (bytes < kilobyte)) {
    return bytes + ' B';
  } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
    return (bytes / kilobyte).toFixed(precision) + ' KB';
  } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
    return (bytes / megabyte).toFixed(precision) + ' MB';
  } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
    return (bytes / gigabyte).toFixed(precision) + ' GB';
  } else if (bytes >= terabyte) {
    return (bytes / terabyte).toFixed(precision) + ' TB';
  } else {
    return bytes + ' B';
  }
}
app.add=function(a,b) {
  return a*1 + b*1;
};   
app.distanceEuclidienne=function(v1,v2){
  var sum = 0;
  for (var n = 0, lng=v1.length; n < lng; n++) {
    var d=v1[n] - v2[n];
    sum += d*d;
  }
  return sum;
}
app.vectorMiddle=function(v1,v2){
  var m=[];
  for (var n = 0, lng=v1.length; n < lng; n++) {
    m.push((v1[n] + v2[n])/2);
  }
  return m;
}
app.toolsVectorsAverage=function(vectors){
  var v=[];
  var n=vectors[0].length;
  if(!n){return v};
  for (var i = vectors.length - 1; i >= 0; i--) {
    var vector=vectors[i];
    for (var j = vector.length - 1; j >= 0; j--) {
      v[j]=v[j]||0;
      v[j]+=vector[j]*1;
    }
  }
  for (var i = v.length - 1; i >= 0; i--) {
    if(v[i].length>1){
      v[i].reduce(app.add,0)/n;
    }
  }
  return v;
}
app.myIndexOf=function (arr,o) {    
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] == o) {
      return i;
    }
  }
  return -1;
}

app.moyenneTableau=function(arr) {
  var somme = 0;
  for (var i = 0, j = arr.length; i < j; i++) {
    somme += arr[i];
  }
  return somme / arr.length;
}

function strAlea(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}