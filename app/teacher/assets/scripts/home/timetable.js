/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
//##############
//HOME - EMPLOI DU TEMPS
//##############
app.homeEDT=function(){
	$('#home-classrooms').css('display','none');
	$('#home_edt_bloc').css('display','flex');
	document.getElementById('home-edt-edit-btn').style.display = "block";
	$('#app').goTo();
	var minTime=app.edtMinTime||"00:00:00";
	var maxTime=app.edtMaxTime||"24:00:00";
	var height="auto"; 	
	if(app.edtMaxTime=="00:00:00"){
		app.edtMaxTime="18:00:00";
	}
	if(minTime=="24:00:00"){
		minTime="08:00:00";
	}
	if(app.edtEditionMode){
		minTime="00:00:00";
		maxTime="24:00:00";
		height="600";
	}
	$('#home-calendar').fullCalendar('destroy');
	$('#home-calendar').fullCalendar({
		views: {
			month: { 
				titleFormat: 'MM/YYYY'
			},
			day: { 
				titleFormat: 'D/MM/YYYY'
			}
		},
		header:{
			right:"",
			left:"",
			center:""
		},
		timezone:'local',
		editable: true,
		defaultView:'agendaWeek',
		firstDay: 1,
		timeFormat: 'HH:mm',
		columnFormat:'dddd',
		slotLabelFormat:"HH:mm",
		slotEventOverlap:false,
		minTime:minTime,
		maxTime:maxTime,
		height:height,
		droppable:true,
		allDaySlot: false,
		defaultTimedEventDuration:"01:00:00",
		eventBackgroundColor:"#F5F5F5",
		dragRevertDuration:0,
		displayEventTime:false,		
		slotDuration:app.userConfig.organizerSlotDuration,
		eventOrder:'dataWeek',
		weekends:app.userConfig.agendaWeekEnds,
		eventTextColor:"black",
		eventReceive:function(event){
			event.dataWeek=app.homeCurrentEditionWeek;
			event.info=document.getElementById('edt-edit-info').value;
			$('#home-calendar').fullCalendar( 'updateEvent', event );
			app.homeEDTSave();
			app.homeMyEDTRender();
		},
		eventResize:function( event, jsEvent, ui, view ){
			app.homeEDTSave();
		},
		eventDrop:function( event, jsEvent, ui, view ){
			app.homeEDTSave();
		},
		eventClick:function(event){
			app.go('classroom/'+event.dataClasseId);
		},
		eventAfterAllRender:function( view){
			if(app.edtEditionMode){ 	
				$('.edt-edit-btn').css('display','block');
				$('.edt-run-btn').css('display','none');		

			}else{
				$('.edt-run-btn').css('display','block');
				$('.edt-edit-btn').css('display','none');
			}
		}
	}
	);
	$('#home-calendar').fullCalendar( 'gotoDate', moment(604800000) );
	app.homeEDTClassesRender();
	app.homeEDTCurrentWeekSet('AB');
	app.homeMyEDTRender();
}
app.homeMyEDTRender=function(week){
	week=week||app.getWeekName();
	$('#home-calendar').fullCalendar('removeEvents');
	var edt=app.userConfig.edt||[];
	var source=[];
	
	if(edt.length==0){
		app.show('home-edt-edition','inline-block');
	}
	for (var i = edt.length - 1; i >= 0; i--) {
		var event=edt[i];
		var classe=app.classes[app.getClasseById(event.dataClasseId).num];
		if(!classe){
			continue;
		}
		if(!app.edtEditionMode && event.dataWeek!="AB" && event.dataWeek!=week){
			continue;
		}
		var textWeek="";
		if(event.dataWeek!="AB" && app.edtEditionMode){
			textWeek="Sem. "+event.dataWeek;
		}
		var id=Math.random();
		var info="";
		if(event.info){
			info="- "+event.info;
		}
		var oEvent={
			title:'<div class="calendar-event-title">'+classe.classe_nom+' '+info+'</div>\
			<div class="h6" style="">'+textWeek+'</div>\
			<div class="edt-event-toolbar">\
			<div class="btn btn-default btn-xs edt-run-btn" style="display:none;" onclick="event.cancelBubble=true; app.agendaEventAddFromEDT('+event.dataClasseId+','+i+');"><span class="glyphicon glyphicon-calendar"></span></div>\
			<div class="btn btn-default btn-xs edt-edit-btn" style="display:none;" onclick="event.cancelBubble=true; $(\'#home-calendar\').fullCalendar(\'removeEvents\', '+id+');app.homeEDTSave();"><span class="glyphicon glyphicon-remove"></span></div>\
			</div>',
			start: event.start*1000,
			end: event.end*1000,
			id:id,
			dataWeek:event.dataWeek,
			dataClasseId:event.dataClasseId,
			stick: true, 
			info:event.info,
     		// maintain when user navigates (see docs on the renderEvent method)
     		borderColor:classe.color
     	};
     	source.push(oEvent);
     }
     $('#home-calendar').fullCalendar( 'addEventSource',  source);
 }

 app.homeEDTClassesRender=function(){
 	var html = [];
 	for (var i = 0, lng = app.classes.length; i < lng; i++) {
 		if(app.classes[i].classe_id<0){continue;}
 		if(app.userConfig.classesDisable.indexOf(app.classes[i].classe_id)>=0){continue;}
 		html.push('<div class="btn btn-default eleve home-edt-fc-event"  data-num="'+i+'" data-id="'+app.classes[i]['classe_id']+'"  style="border-color:'+app.classes[i].color+';">' + app.classes[i]['classe_nom'] + '</div>');
 	}
 	document.getElementById("home-classe").innerHTML=html.join('');
 	$('#home-classe .home-edt-fc-event').each(function() {
      	// store data so the calendar knows to render an event upon drop
      	$(this).data('event', {
      		// use the element's text as the event title
      		title: $.trim($(this).text()), 
        	// maintain when user navigates (see docs on the renderEvent method)
        	stick: true, 
        	dataClasseId:$(this).attr('data-id')
        });
      	// make the event draggable using jQuery UI
      	$(this).draggable({
      	// will cause the event to go back to its
      	revert: true,
        // original position after the drag      
        revertDuration: 0,  
        stack: "#home-calendar"
    });
      });
 }
 app.homeEDTSave=function(){
 	if (!app.checkConnection()) {
 		return;
 	}
 	var events=$('#home-calendar').fullCalendar('clientEvents');
 	var myEDT=[];
 	for (var i = events.length - 1; i >= 0; i--) {
 		var event=events[i];
 		var end=null;
 		if(event.end){
 			end=event.end.unix()
 		}
 		myEDT.push(
 		{
    start:event.start.unix(), // a start time (10am in this example)
    end:end, // an end time (6pm in this example)
    dataWeek:event.dataWeek,
    dataClasseId:event.dataClasseId,
    info:event.info  
}
);
 	}
 	app.userConfig.edt=myEDT; 	
 	app.pushUserConfig();
 }
 app.homeEDTCurrentWeekSet=function(week){
 	app.homeCurrentEditionWeek=week;
 	$(".home-week").removeClass('btn-primary').addClass('btn-default');  
 	$("#home-week-"+week).addClass('btn-primary');  
 }
 app.homeTrashAlert=function(){
 	app.alert({title:'Glissez un élément ici pour le supprimer.'});
 }
 app.homeEDTClasseBtn=function(){
 	app.alert({title:'Glissez cet élément pour l\'ajouter.'});
 }
 app.homeEdtToggleView=function(){
 	var div=$('#home-edt-edition');

 	if(div.css('display')=="block"){
 		div.css('display','none');
 		app.buildEdtTime();
 		app.edtEditionMode=false;
 	}else{
 		div.css('display','block');
 		app.edtEditionMode=true;
 	}
 	app.homeEDT();
 }
 app.buildEdtTime=function(){
 	app.edtMinTime="24:00:00";
 	app.edtMaxTime="18:00:00";
 	var end="";
 	app.userConfig.edt=app.userConfig.edt||[];
 	for (var i = app.userConfig.edt.length - 1; i >= 0; i--) {
 		var start=moment(app.userConfig.edt[i].start*1000).format('HH:mm:ss');
 		if(!app.userConfig.edt[i].end){
 			end=moment(app.userConfig.edt[i].start*1000+3600000).format('HH:mm:ss');
 		}
 		else{
 			end=moment(app.userConfig.edt[i].end*1000).format('HH:mm:ss');
 		}
 		if(start<app.edtMinTime){
 			app.edtMinTime=start;
 		}
 		if(end>app.edtMaxTime){
 			app.edtMaxTime=end;
 		}
 	}
 }