/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.homeClasses=function(){
  $('#app').goTo();
  $('#home_edt_bloc').css('display','none');
  $('#home-classrooms').css('display','block');
  document.getElementById('home-edt-edit-btn').style.display = "none";
  var template = $('#template-home-classes').html();
  var rendered = Mustache.render(template, {classes:app.homeClassesListe});
  document.getElementById('home-classrooms').innerHTML=rendered;
}
app.renderClasseAdd=function(){
  if (!app.checkConnection()) {return;}
  $('#classe_save_btn').button('reset');
  document.getElementById('classe_add_form').style.display = "block";
  document.getElementById('classe_nom').value="";
  document.getElementById('classe_nom').focus();
};
app.classeAdd=function() {
  if (!app.checkConnection()) {
    return;
  }
  var classeNom = document.getElementById('classe_nom').value;
  $.post(app.serveur + "index.php?go=classe&q=add"+ app.connexionParam,{
classe_nom:classeNom,
    sessionParams:app.sessionParams
  },function(data){
    app.render(data);
    app.renderClasseAdd();
    app.home();
    if(app.userConfig.homeCurrentView=="edt"){
      app.viewToggle('home-edt-edition','inline-block');
    }  
  } );  
};