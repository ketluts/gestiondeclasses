/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.home=function(view){
  app.currentView="home";  
  if(app.userConfig.version && app.userConfig.version!=app.version){
    app.alert({title:"L'application a été mise à jour.",text:'',confirmButtonText:"Fermer"},function(){});
    app.userConfig.version=app.version;    
    app.pushUserConfig(); 
  } 
  //Init app
  clearTimeout(app.classeTimeOut);   
  //Init render
  app.viewClear();
  app.currentClasse=null;
  app.currentEleve=null;
  document.getElementById('home_event_edition').style.display = "none";
  
  document.getElementById('titre').innerHTML = "Mes classes";
  
  $(".template_home").css("display","flex");
  document.getElementById('classe_add_form').style.display = "none";
  $('#home-btn-classe').button('reset');
  document.getElementById('home_event_edition').style.display = "none";  

  app.currentView="home";

document.getElementById('home-today').innerHTML = moment().format('D MMMM YYYY'); 

  //Affichage de la vue   
  view=view||app.userConfig.homeCurrentView;
  app.homeViews(app.userConfig.homeCurrentView); 
  //Affichage de l'agenda 
  app.homeAgendaRender();
  app.agendaGoToday();
  document.getElementById('home-memo').value=app.getMemo('home',0)||'';
  app.pluginsLauncher('homeAfterRender');
}
app.homeViews=function(view){
  if(view=="edt"){
    app.homeEDT();
  }
  else{
   app.homeClasses();
 }
 $(".home-btn").removeClass('btn-primary').addClass('btn-default');
 $("#home-btn-"+view).addClass('btn-primary');
 if(view!=app.userConfig.homeCurrentView){
  app.userConfig.homeCurrentView=view;  
  app.pushUserConfig();
} 
}