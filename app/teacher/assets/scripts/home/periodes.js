/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.homeRenderActivePeriode=function(){    
  var html="";  
  if(app.active_periode!==null){
    html="<small>"+app.active_periode.periode_titre+"</small>";
  }
  document.getElementById('home-periode').innerHTML=html;
};
app.homeRenderPeriodesView=function(){    
  var periodes_bloc=[];
  var width=100;
  var colors=[
  '#e5e5e5',
  '#cccccc',
  '#b2b2b2',
  '#999999'
  ];
  var color=0;
  var millesime=app.getPeriodeById(app.active_periode.periode_parent);
  var a=width/(millesime.periode_end-millesime.periode_start);
  var b=-a*millesime.periode_start;

  for (var i = app.periodes.length - 1; i >= 0; i--) {
   var periode=app.periodes[i];
   if(periode.periode_parent!=millesime.periode_id){continue;}
   var start=a*periode.periode_start+b;
   var end=a*periode.periode_end+b;
   var bloc={
    x:start,
    width:end-start,
    title:periode.periode_titre,
    color:colors[color],
    active:periode.periode_active
  };
  periodes_bloc.push(bloc);
  color=(color*1+1)%4;
}
//NOW
var now={
  x:a*Math.floor(app.myTime()/1000)+b
};
html=[];
for (var i = periodes_bloc.length - 1; i >= 0; i--) {
  var bloc=periodes_bloc[i];
  var border="";
  if(bloc.active==1){
    border="border-bottom:1px solid black;";
  } 
  html.push('<span class="home-periodes-views-bloc" style="'+border+'left:'+bloc.x+'%;width:'+bloc.width+'%;background-color:'+bloc.color+';" title="'+bloc.title+'"></span>');
}
html.push('<span class="home-periodes-views-bloc" style="left:'+now.x+'%;width:2px;background-color:#000000;" title="Aujourd\'hui"></span>');
$('#home-periodes-views').html(html.join(''));
};