/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
//##############
//HOME - AGENDA
//##############
app.homeAgendaRender=function(){
	$('#home_agenda').fullCalendar('destroy');
	$('#home_agenda').fullCalendar({
		lang: 'fr',
		views: {
			month: { 
				titleFormat: 'MM/YYYY'
			},
			day: { 
				titleFormat: 'D/MM/YYYY'
			}
		},
		timezone:'local',
		editable: true,
		selectable:true,
		selectHelper:true,
		nowIndicator:true,
		unselectAuto:true,
		unselectCancel:"#home_event_edition",
		defaultView: app.userConfig.agendaCurrentView,
		allDaySlot: true,
		firstDay: 1,
		minTime:app.userConfig.organizerMinTime,
		maxTime:app.userConfig.organizerMaxTime,
		height:"auto",
		//eventOrder:'event_order',  
		timeFormat: 'HH:mm',
		slotLabelFormat:"HH:mm",
		eventBackgroundColor:"#F5F5F5",
		eventBorderColor:"#E3E3E3",
		defaultTimedEventDuration:"01:00:00",
		eventTextColor:"black",
		scrollTime:"07:30:00",
		slotDuration:app.userConfig.organizerSlotDuration,
		slotEventOverlap:false,
		contentHeight:550,
		weekends:app.userConfig.agendaWeekEnds,
		views: {
			basicThreeDay: {
				type: 'basic',
				duration: { days: 3 },
				buttonText: '3 jours'
			}
		},
		header: {
			left: '',
			center: '',
			right: ''
		}, 
		dayClick: function() { 
			//app.homeAgendaTooltip.hide() 
		},
		eventResizeStart: function() { 
			//app.homeAgendaTooltip.hide()
		},
		eventDragStart: function() { 
			//app.homeAgendaTooltip.hide() 
		},
		select:function(start, end, jsEvent, view,resource){
			if(document.getElementById('home_event_edition').style.display=='none'){
				app.agendaEditionInit();
			}

			$('.agenda-next-day').removeClass('btn-primary').addClass('btn-default');
			app.agendaCurrentEventEdit
			app.agendaNewEventAllDay=false;
			$('#home_agenda_bloc').goTo();
			app.agendaNewEventStart=start;
			app.agendaNewEventEnd=end;
			if(!start.hasTime()){
				app.agendaNewEventAllDay=true;
			}
			var html="";
			if(view.name=="agendaDay"){
				if(app.agendaNewEventAllDay){
					html=start.format("ddd DD MMM");
				}
				else{
					html="de "+start.format("HH[h]mm")+" à "+end.format("HH[h]mm");
				}
			}
			else{
				if( end.unix()-start.unix()<=86400){
					html=start.format("ddd DD MMM");
				}
				else{
					html="du "+start.format("DD MMM YYYY")+" au "+end.format("DD MMM YYYY");
				}
			} 
			document.getElementById('home_event_date').value=html;
			$('#home_event_date').css('display','inline-block').removeClass('btn-default').addClass('btn-primary');
		},
		viewRender:function( view, element ){
			app.agendaNewEventStart=null;
			app.agendaNewEventEnd=null;
 			//app.homeAgendaTooltip.hide() ;
 			$(".home-agenda-view").removeClass('btn-primary').addClass('btn-default');    
 			$("#home-agenda-view-"+view.name).addClass('btn-primary');  
 			if(app.userConfig.agendaCurrentView!=view.name){    
 				app.userConfig.agendaCurrentView=view.name;
 				
 				app.pushUserConfig(); 
 			}
 			var week=app.getWeekName(  $('#home_agenda').fullCalendar( 'getDate' ).unix()*1000);
 			document.getElementById('home-agenda-semaine').innerHTML=week;
 			
 			app.homeMyEDTRender(week);

 			$('#home_agenda').fullCalendar( 'unselect' );

 			var format="";
 			if(view.name=="month"){
 				format="MMMM YYYY";
 				document.getElementById('home-agenda-date').innerHTML= $('#home_agenda').fullCalendar( 'getDate' ).format(format);
 			}
 			else if(view.name=="basicThreeDay"){
 				format="DD MMM YYYY";
 				document.getElementById('home-agenda-date').innerHTML= $('#home_agenda').fullCalendar( 'getDate' ).add({day:1}).format(format);
 			}
 			else{
 				format="DD MMM YYYY";
 				document.getElementById('home-agenda-date').innerHTML= $('#home_agenda').fullCalendar( 'getDate' ).format(format);
 			}
 			app.agendaEventDetailsToggle(app.userConfig.agendaEventDetails,false);

 		},
 		eventMouseover:function( event, jsEvent, view ) {
 			this.style.zIndex=5;
 		},
 		eventMouseout:function( event, jsEvent, view ) {
 			this.style.zIndex=1;
 		},
 		eventDrop:function( event, jsEvent, ui, view ) {
 			event.event_type=event.event_type||null;
 			var oEvent={
 				event_id:event.event_id,
 				event_start:event.start.unix()
 			}
 			if(!event.start.hasTime()){
 				oEvent.event_allDay=true;
 			}
 			else{
 				oEvent.event_allDay=false;
 			}

 			if(!event.end || !event.end.hasTime()){
 				oEvent.event_end=event.start.unix();
 			}else{
 				oEvent.event_end=event.end.unix();     
 			}
 			app.currentHomeCalendarDate=$('#home_agenda').fullCalendar( 'getDate' );
 			if(event.event_type=="agenda"){
 				app.agendaEventUpdate(oEvent,false);
 			}
 		},
 		eventResize:function( event, jsEvent, ui, view ) {
 			event.event_type=event.event_type||null;
 			var oEvent={
 				event_id:event.event_id,
 				event_start:event.start.unix()
 			}
 			if(!event.start.hasTime()){
 				oEvent.event_allDay=true;
 			}
 			else{
 				oEvent.event_allDay=false;
 			}
 			if(event.end){
 				oEvent.event_end=event.end.unix();
 			}
 			app.currentHomeCalendarDate=$('#home_agenda').fullCalendar( 'getDate' );
 			if(event.event_type=="agenda"){
 				app.agendaEventUpdate(oEvent,false);
 			}
 		},
 		eventClick:function( event, jsEvent, view ) { 
 			app.agendaOneEventDetailsToggle(event.event_id);
 		}
 	}); 
$('#home_agenda').fullCalendar( 'incrementDate', { days:-1});
app.agendaRender();
app.homeAgendaEditorClasses();
}
app.homeAgendaEditorClasses=function(){ 
	//A faire un même temps que l'affichage des classes
	var html=[];
	app.classes=app.orderBy(app.classes,"classe_nom",'ASC');
	html.push('<option value="-1">Sélectionnez une classe</option>');
	var k = 0;
	for (var i = 0, lng = app.classes.length; i < lng; i++) {
		if(app.classes[i].classe_id<0){continue;}
		if(app.userConfig.classesDisable.indexOf(app.classes[i].classe_id)>=0){continue;}
		html.push('<option value="'+app.classes[i].classe_id+'">');
		html.push(app.classes[i].classe_nom);
		html.push('</option>');
	}
	document.getElementById('home_event_classe').innerHTML=html.join('');
}