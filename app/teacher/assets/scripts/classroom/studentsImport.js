/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

 var app=app || {}; 
  /*---------------
-----------------
------Ajouter des élèves------
-----------------
---------------*/ 
app.elevesAddInputRender=function(options) {
  if (!app.checkConnection()) {
    return;
  }
  options=options||{};
  var div=document.getElementById("classroom-addStudents-liste");
  var n=document.getElementsByClassName('input_eleve').length;
  var new_eleve= document.createElement("div");
  new_eleve.className="input_eleve";
  new_eleve.id="new_eleve_"+n;
  var html=[];
  html.push('<div class="classroom-addStudents-liste-student col-sm-12">');
  html.push('<form class="form-horizontal col-sm-11">');
  html.push('<div class="col-sm-5">');
 //Colonne de droite

 html.push('<div class="form-group">');
 html.push('<label class="col-sm-3 control-label">NOM</label>');
 if(options.eleve_nom=="null"){options.eleve_nom="";}
 var value = options.eleve_nom||"";
 html.push('<div class="col-sm-9"><input type="text" class="form-control" name="eleves_noms[]" placeholder="DOE" id="input_eleve_nom_'+n+'" value="'+value+'"/></div>');
 html.push('</div>');

 html.push('<div class="form-group">');
 html.push('<label class="col-sm-3 control-label">Prénom</label>');
 if(options.eleve_nom=="null"){options.eleve_prenom="";}
 var value = options.eleve_prenom||"";
 html.push('<div class="col-sm-9"><input type="text" class="form-control" name="eleves_prenoms[]" placeholder="John" id="input_eleve_prenom_'+n+'" value="'+value+'"/></div>');
 html.push('</div>');

 html.push('</div>');
 html.push('<div class="col-sm-7">');
 //Colonne de gauche
 //Genre
 html.push('<div class="form-group">');
 html.push('<label class="col-sm-6 control-label">Genre</label>');
 html.push('<div class="col-sm-6"><select class="btn btn-default form-control" name="eleves_genres[]" id="input_eleve_genre_'+n+'">');
 html.push('<option value="null">-</option>');
 html.push('<option value="G">G</option>');
 html.push('<option value="F">F</option>');           
 html.push('</select></div>');
 html.push('</div>'); 
//Date de naissance
html.push('<div class="form-group">');
html.push('<label class="col-sm-6 control-label">Date de naissance</label>');
if(options.eleve_birthday=="null"){options.eleve_birthday="";}
var value = options.eleve_birthday||"";
html.push('<div class="col-sm-6"><input type="text" class="form-control" onchange="app.checkBirthdayPattern(this.value);" name="eleves_birthdays[]" placeholder="JJ/MM/AAAA" id="input_eleve_birthday_'+n+'" value="'+value+'"/></div>');
html.push('</div>');
//Fin : Colonne de gauche
html.push('</div>');
html.push('</form>');
html.push('<div class="col-sm-1">');
html.push('<button type="button" id="input_eleve_btn_'+n+'" class="btn btn-default" onclick="app.elevesAddInputRemove('+n+');" >');
html.push("<span class='glyphicon glyphicon-trash'></span>");
html.push('</button>');
html.push('</div>');
html.push('</div>');
new_eleve.innerHTML=html.join('');
div.appendChild(new_eleve);
//document.getElementById('input_eleve_nom_'+n+'').focus();
document.getElementById('input_eleve_genre_'+n+'').value=options.eleve_genre||null;
var input_id = document.createElement("input");
input_id.name = "eleves_ids[]";
input_id.value = options.eleve_id||"";
input_id.type = "hidden";
input_id.id = "input_eleve_id_"+n;
div.appendChild(input_id);
if(options.eleve_id){
  document.getElementById('input_eleve_nom_'+n+'').disabled = true;
  document.getElementById('input_eleve_prenom_'+n+'').disabled = true;
  document.getElementById('input_eleve_id_'+n+'').disabled = true;
  document.getElementById('input_eleve_genre_'+n+'').disabled=true;
  document.getElementById('input_eleve_birthday_'+n+'').disabled=true;
}
};
app.elevesAddInputRemove=function(id){ 
  document.getElementById("input_eleve_nom_"+id).value="";
  document.getElementById("input_eleve_prenom_"+id).value="";
  document.getElementById("input_eleve_id_"+id).value="";
  document.getElementById("input_eleve_genre_"+id).value="";
  document.getElementById("input_eleve_birthday_"+id).value="";
  document.getElementById("new_eleve_"+id).style.display="none";
};
// app.closeAddEleves=function(){
//   document.getElementById('classroom-addStudents').style.display = "none";
// };
app.classeElevesAddRender=function(){
 document.getElementById('classroom-addStudents').style.display = "block";
 document.getElementById('classroom-addStudents-classrooms').innerHTML="";
 document.getElementById("classroom-addStudents-liste").innerHTML="";
 $("#classroom-addStudents").goTo();
 app.elevesAddInputRender();
 var html=[];
for (var i =0, lng=app.classes.length; i<lng; i++) {
 var classe=app.classes[i];
 if(classe.classe_nom==app.currentClasse.classe_nom){continue;}
 html.push('<div class="panel panel-default" id="collapse_'+classe.classe_id+'_block">\
  <div class="panel-heading">\
  <h4 class="panel-title">\
  <a data-toggle="collapse" data-parent="#classroom-addStudents-classrooms" href="#collapse_'+classe.classe_id+'">\
  '+app.cleanClasseName(classe.classe_nom)+'\
  </a>\
  </h4>\
  </div>\
  <div id="collapse_'+classe.classe_id+'" class="panel-collapse collapse ">\
  <div class="panel-body" id="collapse_'+classe.classe_id+'_body">');

 for (var j =0,llng=app.classes[i].eleves.length ; j <llng; j++) {
  var eleve=app.getEleveById(app.classes[i].eleves[j]);
  if(!eleve.eleve_prenom){
    eleve.eleve_prenom="";
  }
      //TODO Améliorer l'utilisation de la fonction escape
      html.push('<div class="classroom-addStudents-classrooms-student">\
        <button class="btn btn-default btn-sm" onclick=\'app.elevesAddInputRender({eleve_nom:"'+escape(eleve.eleve_nom)+'",eleve_prenom:"'+escape(eleve.eleve_prenom)+'",eleve_id:"'+eleve.eleve_id+'",eleve_genre:"'+eleve.eleve_genre+'",eleve_birthday:"'+eleve.eleve_birthday+'"});\'>\
        <span class="glyphicon glyphicon-chevron-left"></span>\
        </button>\
        '+app.renderEleveNom(eleve)+'\
        </div>');
    }
    html.push('</div>\
      </div>\
      </div>');  
  }; 
  // if(app.eleves_sans_classe.length!=0){
  //   app.classes=effacerLigne(app.classes,app.classes.length - 1);
  // }
  document.getElementById('classroom-addStudents-classrooms').innerHTML+=html.join('');
  for (var i = app.classes.length - 1; i >= 0; i--) {
    if(app.classes[i].disable){continue;}
    var classe=app.classes[i];
    if(classe.classe_nom==app.currentClasse.classe_nom){continue;}
    if(document.getElementById('collapse_'+classe.classe_id+'_body') && document.getElementById('collapse_'+classe.classe_id+'_body').innerHTML==""){
      document.getElementById('collapse_'+classe.classe_id+'_block').style.display="none";
    }
  }
};
app.submitEleveForm=function() {
    
  var nouveauxElevesNoms = document.getElementsByName("eleves_noms[]");
  var nouveauxElevesPrenoms = document.getElementsByName("eleves_prenoms[]");
  var nouveauxElevesIds = document.getElementsByName("eleves_ids[]");
  var nouveauxElevesGenres = document.getElementsByName("eleves_genres[]");
  var nouveauxElevesBirthdays = document.getElementsByName("eleves_birthdays[]");
  var eleves=[];
  for (var i = 0, lng = nouveauxElevesNoms.length; i < lng; i++) {
    if(!app.checkBirthdayPattern(nouveauxElevesBirthdays[i].value)){
      $('#classroom-addStudents-btn').button('reset');
      return;
    }
    var eleve={
      eleve_nom:nouveauxElevesNoms[i].value,
      eleve_prenom:nouveauxElevesPrenoms[i].value,
      eleve_id:nouveauxElevesIds[i].value,
      eleve_genre:nouveauxElevesGenres[i].value,
      eleve_birthday:nouveauxElevesBirthdays[i].value
    };
    eleves.push(eleve);
  }
  $.post(app.serveur + "index.php?go=eleves&q=add"+ app.connexionParam, {
   id:app.currentClasse.classe_id,
    eleves: JSON.stringify(eleves),
sessionParams:app.sessionParams
}, function(data) {
    $('#classroom-addStudents-btn').button('reset');
    app.render(data);
    //app.navigate('classroom/'+app.currentClasse.classe_id);
    app.classeRender(app.currentClasse.classe_id);
   // app.classroomInit(app.currentClasse.num);
  });
};
app.importFromTableur=function(){
  
  var tab=document.getElementById('import-tableur').value;
  var split=tab.split('\n');
  for (var i = split.length - 1; i >= 0; i--) {
   var eleve_tab=split[i];
   var eleve=eleve_tab.split('\t');
   app.elevesAddInputRender({eleve_nom:eleve[0],eleve_prenom:eleve[1],eleve_id:false,eleve_genre:eleve[2],eleve_birthday:eleve[3]});
 };
 $('#import-tableur-btn').button('reset');
}