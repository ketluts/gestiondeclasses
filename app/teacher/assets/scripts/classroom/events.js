/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.classeEventsGraphRender=function(){
  var events=app.agenda;
  
  events.sort(function(a,b) { return parseFloat(a.event_start) - parseFloat(b.event_start) } );
  
  var series=[];
  var day=0;
  var tabEventsType=[];
  var n=0;
  for (var i =0, lng= events.length; i <lng; i++) {
    var oEvent=events[i];    
    if ((oEvent.event_user !==  app.userConfig.userID && !app.userConfig.ppView)  
      || (oEvent.event_type=="classe")
      || (oEvent.event_type=="eleve" && app.currentClasse.eleves.indexOf(oEvent.event_type_id)<0)
      ) { 
      continue;
  }
  if (oEvent.event_user == app.userConfig.userID || app.userConfig.ppView) {
    var event_day=app.today(oEvent.event_start)*1000;
    var legende=app.legends[oEvent.event_icon]||"";
    var name='<span class="glyphicon '+oEvent.event_icon+'"></span> '+legende;
    var index=tabEventsType.indexOf(name);
    var d = new Date();
    if(index==-1){
     index=tabEventsType.length;
     series[index]={};
     series[index].type="line";
     series[index].name=name;  
     series[index].visible=false;  
     series[index].pointStart= Date.UTC(d.getFullYear(), 8, 1);
     series[index].data=[];
     series[index].total=0;  
     series[index].day=event_day;    
     tabEventsType.push(name);   
     n++; 
   }
   if(n==1){
     series[index].visible=true;
   }
   if(series[index].day!=event_day){
     var temp=[series[index].day*1,series[index].total*1];
     series[index].data.push(temp);
     series[index].day=event_day;
   }
   series[index].total++;
 }
};
for (var i =0, lng= series.length; i <lng; i++) {
  var serie=series[i];
  var temp=[serie.day*1,serie.total*1];
  serie.data.push(temp);
}
$('#classe_graph').highcharts({
  chart: {
    zoomType: 'x'
  },
  title: {
    text: ''
  },
  subtitle: {
    text: document.ontouchstart === undefined ?
    'Clique et déplace pour zoomer.' :
    'Pince pour zoomer.'
  },
  xAxis: {
    type: 'datetime',
            minRange: 86400000 // one days
          },
          yAxis: {
            title: {
              text: ''
            }
          },        
          legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'middle',
            borderWidth: 0,
            useHTML:true
          },      
          series: series
        });
};
  /*---------------
-----------------
------Events------
-----------------
---------------*/
app.renderClassroomEventsInit=function() {
  var html = [];
  html.push('<table class="table">');
  html.push('<tr>');
  html.push('<td>');
  html.push('</td>');
  for (var i =0,lng= app.periodes.length ; i <lng; i++) {
    var periode=app.periodes[i];
    if(periode.periode_type=="m" || periode.periode_parent!=app.active_periode.periode_parent){continue;}
    html.push('<td>');
    html.push(periode.periode_titre);
    html.push('</td>');
  }
  html.push('<td>');
  html.push('</td>');
  html.push('</tr>');
  for (var k = 0, lng = app.eventsAvailable.length; k < lng; k++) {
    var event_type=app.eventsAvailable[k].type;
    if(app.userConfig.eventsWhiteList.indexOf(event_type)<0){continue;}
    var legende=app.legends[event_type]||"";
    html.push('<tr id="classroom-events-'+event_type+'" class="student-events-row">');
    html.push('<td class="text-left">');
    html.push('<span class="glyphicon ' + event_type + '"></span> ');   
    html.push(legende);
    html.push('</td>');
    for (var i =0,llng= app.periodes.length ; i <llng; i++) {
      var periode=app.periodes[i];
      if(periode.periode_type=="m" || periode.periode_parent!=app.active_periode.periode_parent){continue;}
      html.push('<td class="h5">');
      html.push('<span id="classroomEvents_'+periode.periode_id+'_' + event_type + '_counter_enable" class="classroomEvents_counter_enable">0</span> <small>/<span id="classroomEvents_'+periode.periode_id+'_' + event_type + '_counter_total" class="classroomEvents_counter_total">0</span></small>');
      html.push('</td>');
    }
    html.push('<td>');
    html.push('<div class="btn-group eleveEventsListBtn">');
   html.push('<div class="btn btn-default btn-xs" title="Tout barrer" onclick="app.classroomEventsCheckAll(\''+event_type+'\');"><span class="glyphicon glyphicon-font barre"></span></div>');
   html.push('<div class="btn btn-default btn-xs" title="Tout supprimer" onclick="app.classroomEventDeleteAll(\''+event_type+'\');"><span class="glyphicon glyphicon-trash"></span></div>');
    html.push('</div>');
    html.push('</td>');
    html.push('</tr>');
  }

  html.push('</table>');
  document.getElementById('classroomEventsLST').innerHTML = html.join('');
};
app.renderClassroomEvents=function() {  
  $('.classroomEvents_counter_enable').html('0');
  $('.classroomEvents_counter_total').html('0');
  var html=[];
  var tabEventsCounter=[];
  for (var i = app.agenda.length - 1; i >= 0; i--) {
    var oEvent = app.agenda[i];
    if(app.userConfig.eventsWhiteList.indexOf(oEvent.event_icon)<0){continue;}
    if (app.currentClasse.eleves.indexOf(oEvent.event_type_id)<0 || oEvent.event_type!="eleve") {continue;}
    if(oEvent.event_user != app.userConfig.userID && !app.userConfig.ppView){continue;}
    var periode=oEvent.event_periode||-1;
    tabEventsCounter[periode]=tabEventsCounter[periode]||[];
    if (oEvent.event_state == "disable") {
    }else{
      if(tabEventsCounter[periode][oEvent.event_icon+'_enable']){
        tabEventsCounter[periode][oEvent.event_icon+'_enable']++;
      }else{
        tabEventsCounter[periode][oEvent.event_icon+'_enable']=1;
      }
    }     
    if(tabEventsCounter[periode][oEvent.event_icon+'_total']){
      tabEventsCounter[periode][oEvent.event_icon+'_total']++;
    }else{
      tabEventsCounter[periode][oEvent.event_icon+'_total']=1;
    }
  }
  for (var k = 0, lng = app.eventsAvailable.length; k < lng; k++) {
    var event_type=app.eventsAvailable[k].type;
    var is_visible=false;
    if(app.userConfig.eventsWhiteList.indexOf(event_type)<0){continue;}
    for (var i = app.periodes.length - 1; i >= -1; i--) {
      var periode=app.periodes[i]||{
        periode_id:-1,
        periode_titre:"Non classés"
      };  
      tabEventsCounter[periode.periode_id]=tabEventsCounter[periode.periode_id]||[];
      var total=tabEventsCounter[periode.periode_id][event_type+'_total'];
      if(total){
        is_visible=true;
        document.getElementById('classroomEvents_'+periode.periode_id+'_' + event_type + '_counter_total').innerHTML=total||0;
        document.getElementById('classroomEvents_'+periode.periode_id+'_' + event_type + '_counter_enable').innerHTML=tabEventsCounter[periode.periode_id][event_type+'_enable']||0;
      }
    } 
  }
};
app.classroomEventsCheckAll=function(event_type,confirm){
  if(!confirm){
    app.alert({title:'Barrer tous les éléments de ce type ?',type:'confirm'},function(){app.classroomEventsCheckAll(event_type,true);});
    return;
  }
 // var eleve=app.currentEleve;
  for (var i = 0, lng = app.agenda.length; i < lng; i++) {
    var oEvent = app.agenda[i];   
    if(oEvent.event_icon!=event_type){continue;}

    // if(app.userConfig.eventsWhiteList.indexOf(oEvent.event_icon)<0){continue;}
    if (app.currentClasse.eleves.indexOf(oEvent.event_type_id)<0 || oEvent.event_type!="eleve") {continue;}
   // if(oEvent.event_user != app.userConfig.userID && !app.userConfig.ppView){continue;}



    //if (app.currentClasse.eleves.indexOf(oEvent.event_type_id)<0 || oEvent.event_type!="eleve") {continue;}
    if(oEvent.event_user != app.userConfig.userID ){continue;}
  //  if (oEvent.event_type_id == eleve.eleve_id && oEvent.event_type=="eleve" && oEvent.event_user == app.userConfig.userID) {
    app.agendaEventUpdate({
      event_id:oEvent.event_id,
      event_state:"disable"
    });
    //}
  }
}


app.classroomEventDeleteAll=function(event_type,confirm){
  if(!confirm){
    app.alert({title:'Supprimer tous les éléments de ce type ?',type:'confirm'},function(){app.classroomEventDeleteAll(event_type,true);});
    return;
  }

  for (var i = 0, lng = app.agenda.length; i < lng; i++) {
    var oEvent = app.agenda[i]; 

    if(oEvent.event_icon!=event_type){continue;}
if (app.currentClasse.eleves.indexOf(oEvent.event_type_id)<0 || oEvent.event_type!="eleve") {continue;}
  if(oEvent.event_user != app.userConfig.userID ){continue;}

   // if (oEvent.event_type_id == eleve.eleve_id && oEvent.event_type=="eleve" && oEvent.event_user == app.userConfig.userID) {
      app.agendaEventDelete(oEvent.event_id,true);
   // }
  }
}