/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

 var app=app || {}; 
  /*---------------
-----------------
------Exporter la liste des élèves------
-----------------
---------------*/ 
app.classeExportRender=function(){
 document.getElementById("classe-export").style.display="block";
 var options=[['false','-'],['D','Début'],['M','Milieu'],['F','Fin']];
 var html=[];
 html.push('<table id="classroom-export-table">');
 html.push('<thead>');
 html.push('<tr>');  
 html.push("<th class='text-left'><input type='checkbox' onchange='app.studentsCheckAll(this.checked);' class='btn btn-control'/></th>"); 
 html.push("<th>ID</th>"); 
 html.push("<th>Classe</th>"); 
 html.push("<th>NOM</th>"); 
 html.push('<th>Prénom</th>'); 
 if(app.userConfig.admin){
 html.push('<th>Cycle<br/>'); 
}
 html.push('</th>'); 
 html.push('<th>Genre</th>');
 html.push('<th>Date de naissance</th>');
 html.push('<th>Code élève</th>');
 html.push('</tr>');
 html.push('</thead>');    
 html.push('<tbody>');
 for (var i = app.currentClasse.eleves.length - 1; i >= 0; i--) {
   var eleve= app.getEleveById(app.currentClasse.eleves[i]);
   html.push('<tr class="text-left">');
   html.push("<td><input id='student_"+eleve.eleve_id+"_checkbox' type='checkbox' onchange='app.studentsSelectedCount();' class='btn btn-control'/></td>"); 
   html.push('<td>');
   html.push(eleve.eleve_id);
   html.push('</td>');
   html.push('<td>');
   html.push(app.cleanClasseName(app.currentClasse.classe_nom));
   html.push('</td>');
   html.push('<td>');
   html.push(eleve.eleve_nom.toUpperCase());
   html.push('</td>');
   html.push('<td>');
   html.push(ucfirst(eleve.eleve_prenom));
   html.push('</td>');

if(app.userConfig.admin){
   html.push('<td>');
   html.push('<select  onchange="app.studentSetCycle('+eleve.eleve_id+',this.value);">');
   html.push('<option value="false">-</option>');
   for (var j =0,llng= app.cycles.length ; j< llng; j++) {

    var selected="";
    if(eleve.eleve_cycle==app.cycles[j]['code']){
      selected="selected";
    }
    html.push('<option value="'+app.cycles[j]['code']+'" '+selected+'>'+app.cycles[j]['nom']+'</option>');
  }
  html.push('</select>');

  html.push('</td>');  
  } 
  html.push('<td>');
  html.push(eleve.eleve_genre);
  html.push('</td>');
  html.push('<td>');
  html.push(eleve.eleve_birthday);
  html.push('</td>');
  html.push('<td>');
  html.push(eleve.eleve_token);
  html.push('</td>');
  html.push('</tr>');
};
html.push('</tbody>');
html.push("</table>");
document.getElementById("classe-export-div").innerHTML=html.join('');

$('#classroom-export-table').dataTable({
  dom: '<"flex-rows"<"flex-3"<"text-center"<"pull-left"l><"pull-right"p>>ti><"flex-1 aside"<B><f><"#classroom-export-aside">>>',
  colReorder: true,
  buttons: [
  {
    extend: 'copy',
    text: 'Copier'
  },
  {
    extend: 'csv',
    text: 'CSV'
  },
  {
    extend: 'excel',
    text: 'Excel'
  },
  {
    extend: 'pdf',
    text: 'PDF'              
  }

  ],
  stateSave: true,
  "language":app.datatableFR,
 //  "oLanguage": {
 //   "sSearch": "",
 //    "lengthMenu": "Display _MENU_ records per page",
 // },
 // select: {
 //   style: 'os',
 //   items: 'cell'
 // },
 "lengthMenu": [[-1,10, 25, 50], ["Tous",10, 25, 50]],
 "order":[[3,'asc']],

});
$('#classroom-export-table').addClass('table table-striped ');
//$('.dataTables_filter input').addClass('search-query');
$('.dataTables_filter input').attr('placeholder', 'Recherche').removeClass('input-sm');
app.studentsSelectedCount();

if(app.userConfig.admin){
  html=[];
  html.push('<hr/>');
  html.push('<div class="h4">Pour la classe</div>');
  html.push('Cycle : <select  onchange="app.classroomSetCycle(this.value);">');
  html.push('<option value="false">Définir le cycle</option>');
  for (var j =0,llng= app.cycles.length ; j< llng; j++) {
   html.push('<option value="'+app.cycles[j]['code']+'">'+app.cycles[j]['nom']+'</option>');
 }
 html.push('</select>');
 document.getElementById('classroom-export-aside').innerHTML=html.join('');

}
}

app.studentsCheckAll=function(checked){
  for (var i = 0, lng=app.currentClasse.eleves.length ; i<lng; i++){
   var eleve_id= app.currentClasse.eleves[i];
   //console.log(eleve_id);
   if(document.getElementById("student_"+eleve_id+"_checkbox")){ 
    document.getElementById("student_"+eleve_id+"_checkbox").checked=checked;
  }
     // $("#student_"+eleve_id+"_checkbox").prop('checked', true);
   }
   app.studentsSelectedCount();  
 }
 app.studentsSelectedCount=function(){
  var n=0;
  for (var i = 0, lng=app.currentClasse.eleves.length ; i<lng; i++){
   var eleve= app.getEleveById(app.currentClasse.eleves[i]);
      //  if(message.message_parent_message!="-1"){continue;}
      if(document.getElementById("student_"+eleve.eleve_id+"_checkbox")){     
        if(document.getElementById("student_"+eleve.eleve_id+"_checkbox").checked){
          n++;
        }
      }
    }

    if(n>0){
      $('#student-selected-bloc').css('display','block');
      $('#student-selected-nb').html(n);
    }else{
      $('#student-selected-bloc').css('display','none');
    }
  }