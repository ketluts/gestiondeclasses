/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {}; 
 /*---------------
-----------------
------Tirage au sort------
-----------------
---------------*/ 
app.classroomRandomInit=function(){
  var groupes=app.currentClasse.currentGroupes;

  for (var j =0, n= groupes.length; j <n; j++) {
    var groupe=groupes[j];
      app.randomEleve.eleves[j]=[];
    for (var i = 0, lng = groupe.length; i < lng; i++) {
     var eleve = app.getEleveById(groupe[i]);  
     if(!eleve){continue;}

     if(app.userConfig.randomEleves.indexOf(eleve.eleve_id)==-1){
     // if(!app.randomEleve.eleves[j]){app.randomEleve.eleves[j]=[];}
      app.randomEleve.eleves[j].push(eleve.eleve_id);
      btn_class="btn-primary";
    }else{
      btn_class="btn-default";
    }
    $("#eleve_"+eleve.eleve_id).removeClass("btn-default").removeClass("btn-primary").addClass(""+btn_class+"");
  }
}

}
app.renderRandomEleve=function(nom,id){  
  var html="";
  for(var j=0;j<app.randomEleve.k;j++){
    html+=nom[j];
  }
  for(var i=app.randomEleve.k, lng=nom.length;i<lng;i++){
    var alea=Math.random();
    html+=String.fromCharCode(Math.floor(alea*65+(1-alea)*122));
  }
  document.getElementById("classroom-random").innerHTML=html;  
  app.randomEleve.n++;
  if(app.randomEleve.n==3){
    app.randomEleve.k++;
    app.randomEleve.n=0;
  }
  if(app.randomEleve.k<=nom.length){
    setTimeout(function(){
      app.renderRandomEleve(nom,id);
    },10);
  }else{
    $("#eleve_"+id).removeClass("btn-primary");
    $("#eleve_"+id).addClass("btn-default");
    app.randomEleve.enable=true;
  }
};
app.getRandomEleve=function(){  
  if(app.randomEleve.enable==false || app.currentClasse.loading){
    return;
  }
  app.randomEleve.enable=false;
  app.randomEleve.n=0;
  app.randomEleve.k=0;
app.randomEleve.eleves[app.randomEleve.selectInGroupe]=app.randomEleve.eleves[app.randomEleve.selectInGroupe]||[];
//On réinitialise le groupe vidé
  if(app.randomEleve.eleves && app.randomEleve.eleves[app.randomEleve.selectInGroupe].length==0){
    var eleves=[];
    if( app.currentClasse.currentGroupesNum===false){
      for (var i = app.currentEleves.length - 1; i >= 0; i--) {
        eleves.push(app.currentEleves[i].eleve_id);
      //  alert(app.currentEleves[i].eleve_id);
        app.randomEleveDel(app.currentEleves[i].eleve_id);
        $("#eleve_"+app.currentEleves[i].eleve_id).addClass("btn-primary");
        $("#eleve_"+app.currentEleves[i].eleve_id).removeClass("btn-default");
      };
    }else{
     eleves=jsonParse(app.currentClasse.groupes[app.currentClasse.currentGroupesNum].groupe_data)[app.randomEleve.selectInGroupe];
     for (var i = eleves.length - 1; i >= 0; i--) {

      //var eleve=app.currentClasse.eleves[app.idToNum(eleves[i],app.currentClasse.eleves)];
      app.randomEleveDel(eleves[i]);
      $("#eleve_"+eleves[i]).addClass("btn-primary");
      $("#eleve_"+eleves[i]).removeClass("btn-default");
    };
  }  
  app.randomEleve.eleves[app.randomEleve.selectInGroupe]=clone(eleves);
}  
var alea= Math.floor(Math.random()*(app.randomEleve.eleves[app.randomEleve.selectInGroupe].length)); 
var eleve=app.getEleveById(app.randomEleve.eleves[app.randomEleve.selectInGroupe][alea]);
app.renderRandomEleve(app.renderEleveNom(eleve,true),eleve.eleve_id); 
app.randomEleveAdd(eleve.eleve_id);
app.randomEleve.eleves[app.randomEleve.selectInGroupe].splice(alea,1);
app.randomEleve.selectInGroupe=(app.randomEleve.selectInGroupe*1+1)%app.currentClasse.currentGroupesNb;
setTimeout(function(){app.pushUserConfig();},100);
};
app.razRandomEleve=function(){  
 for (var i = app.currentEleves.length - 1; i >= 0; i--) {
  app.randomEleveDel(app.currentEleves[i].eleve_id);
  $("#eleve_"+app.currentEleves[i].eleve_id).addClass("btn-primary").removeClass("btn-default");
};  
app.classroomRandomInit();
document.getElementById("classroom-random").innerHTML = "?";  
app.pushUserConfig();
};
app.randomEleveDel=function(eleve_id){
  var newRandomEleves=[];
  for (var i = app.userConfig.randomEleves.length - 1; i >= 0; i--) {
    var id=app.userConfig.randomEleves[i];
    if(id!=eleve_id){
      newRandomEleves.push(id);
    }
  };
  app.userConfig.randomEleves=newRandomEleves;  
};
app.randomEleveAdd=function(eleve_id){
  app.userConfig.randomEleves.push(eleve_id);  
}