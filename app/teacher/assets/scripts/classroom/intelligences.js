/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

 var app=app || {}; 
app.updIntelligencesTest=function(value){
  //Activer le test d'intelligences multiples pour la classe
  if (!app.checkConnection()) {return;}
  $.post(app.serveur + "index.php?go=classe&q=update"+app.connexionParam,
    {
      intelligences:value,
classe_id:app.currentClasse.classe_id,
      sessionParams:app.sessionParams
    }, function(data) {
    app.render(data);  
  }
  );
  if(value=="true"){
    app.alert({title:"Les élèves de cette classe ont maintenant accès au test sur leur page personnelle."});

  }
}
app.getClasseIntelligences=function(){
  $('#classe-intelligences').css('display','block').goTo();

  var results=[0,0,0,0,0,0,0,0];

  for (var i =0,llng= app.currentClasse.eleves.length; i<llng; i++) {
    var eleve_intelligences=app.getEleveById(app.currentClasse.eleves[i]).eleve_intelligences;
//alert(dump(results));
if(!eleve_intelligences){
  scores=[];
}else{
  scores=jsonParse(eleve_intelligences);
}
for (var j =0,lng=scores.length; j<lng; j++) {
  results[j]=results[j]*1+scores[j];
}
//alert(dump(results));
}
var categories=[];

for (var i =0; i<8; i++) {
  categories.push(app.intelligences[i].name);  
}
$('#classe-intelligences-view').highcharts({

  chart: {
    polar: true,
    type: 'line'
  },

  title: {
    text: '',
    style:"display:none"
  },

  pane: {
    size: '80%'
  },
  xAxis: {
    categories: categories,
    tickmarkPlacement: 'on',
    lineWidth: 0
  },
  yAxis: {
    gridLineInterpolation: 'polygon',
    lineWidth: 0,
    min: 0
  },
  series: [{
    name: 'Profil',
    data: results,
    pointPlacement: 'on'
  }
  ]
});
}