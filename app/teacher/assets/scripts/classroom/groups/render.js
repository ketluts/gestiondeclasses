/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.renderGroupes=function(groupes){
  var eleves_id=[];
  var coloration=document.getElementById("classroom-groupes-selectColor").value;
  var temp_groupes=[];
  var max=0;
  for(var i=0, lng=groupes.length;i<lng;i++){
   var llng=groupes[i].length;
   if(llng>max){max=llng;}
   if(llng!=0){
     temp_groupes.push(groupes[i]);
   }
 }
 groupes=temp_groupes;
 var nbGroupes=groupes.length;
 var html = [];
 for(i=0;i<nbGroupes;i++){
  var cohesion=app.classroomGroupsCohesionScore(groupes[i]);
  html.push('<div class="col-sm-4" ondragover="event.preventDefault();" ondrop="app.classroomGroupsItemDrop(event,'+i+');">');
  html.push('<div class="panel panel-default" >');
  var nomGroupe=i*1+1;
  html.push('<div class="panel-heading h4">Groupe '+nomGroupe+'');
  html.push('<span class="classroom-groupes-cohesion hide" title="Cohésion du groupe"> <span class="glyphicon flaticon-teamwork"></span><br/><span class="classroom-groupes-cohesion-value">'+cohesion+'%</span></span>');
  html.push('</div>');
  for(var j=0,lng=max;j<lng;j++){
    if(groupes[i][j]){
      var eleve=app.getEleveById(groupes[i][j]);
      if(!eleve){
        continue;
      }
      var color="";
      switch(coloration){
        case 'genres':
        if(eleve.eleve_genre=="F"){
          color="#F76F8E";
        }
        else if(eleve.eleve_genre=="G"){
          color="#3ABEFF";
        }
        break;
        case 'notes':
        color=app.colorByMoyenne(eleve.jaugeNotes); 
        break;
        case 'acquis':
       // alert(eleve.jaugeAcquis);
        color=app.colorByMoyenne(eleve.jaugeAcquis); 
       // alert(eleve.jaugeAcquis+" "+color);
        break;
      }      
      eleves_id.push(eleve.eleve_id);
      html.push('<div style="background-color:'+color+'" draggable="true" ondragstart="event.dataTransfer.setData(\'text/plain\', \''+j+'~'+i+'\');" class="panel-body">');
      html.push('<span  class="movable">'+app.renderEleveNom(eleve)+'</span>')
    }else{
      html.push('<div class="panel-body">');
      html.push("...");       
    }       
    html.push('</div>');
  }
  html.push("</div>");
  html.push("</div>");
}
//On affiche le nouveau groupe
var nomGroupe=groupes.length*1+1;
html.push('<div class="col-sm-4 groupe-vide"  ondragover="event.preventDefault();" ondrop="app.classroomGroupsItemDrop(event,\'new_groupe\');">');
html.push('<div class="panel panel-default" >');
html.push('<div class="panel-heading h4">Groupe '+nomGroupe+'</div>');
html.push('<div class="panel-body">');
html.push("...");  
html.push('</div>');
html.push("</div>");
html.push("</div>");
//On affiche les élèves non classés dans un groupe
var n=0;
html.push('<div class="col-sm-4"  ondragover="event.preventDefault();" ondrop="app.classroomGroupsItemDrop(event,\'nc\');">');
html.push('<div class="panel panel-default" >');
html.push('<div class="panel-heading h4">Non classé(s)</div>');
for(var j=0;j<app.currentClasse.eleves.length;j++){
  if(eleves_id.indexOf(app.currentClasse.eleves[j])>=0){continue;}
  n++;
  var eleve=app.getEleveById(app.currentClasse.eleves[j]);
  var color="";
  switch(coloration){
        case 'genres':
        if(eleve.eleve_genre=="F"){
          color="#F76F8E";
        }
        else if(eleve.eleve_genre=="G"){
          color="#3ABEFF";
        }
        break;
        case 'notes':
        color=app.colorByMoyenne(eleve.jaugeNotes); 
        break;
        case 'acquis':
        color=app.colorByMoyenne(eleve.jaugeAcquis); 
        break;
      }   
html.push('<div  style="background-color:'+color+'" draggable="true" ondragstart="event.dataTransfer.setData(\'text/plain\', \''+eleve.eleve_id+'~nc\')" class="panel-body">');
html.push('<span class="movable">'+app.renderEleveNom(eleve)+'</span>')
html.push('</div>');
}
if(n==0){
 html.push('<div class="panel-body">');
 html.push("...");  
 html.push('</div>');
}
html.push("</div>");
html.push("</div>");
app.currentClasse.currentGroupes=groupes;
document.getElementById("classroom-groupes-liste").innerHTML=html.join('');
};