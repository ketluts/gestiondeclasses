/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.getGroupsBySkills=function(nbGroupes,fixeNbGroupes){
  var studentsSegments=[];
  var studentsCoordonnees=[];
  var items=[];
  var eleves=app.currentClasse.eleves;
 //eleves.sort(function(){ return Math.round(Math.random())});
  var maxStudentsInGroup=Math.floor(eleves.length/nbGroupes);
  var type=document.getElementById('groupes_de_niveaux_type').value;
  document.getElementById("classroom-groupes-selectColor").value='acquis';
  var ids=[];

//On liste les items concernés
for (var i =0, lng=app.items.length;i<lng; i++) {
  var item=app.items[i];  

var filter_id=document.getElementById('classroomGroupesFiltersList').value;
// if(filter_id!=-1){
//   var filter=app.getFilterById(filter_id);
// }else{
//   var filter=app.itemsCurrentFilter;
// }

  if(!app.itemsIsVisible(item,filter_id)){
    continue;
  }
  items.push(item['item_id']);
}

if(items.length==0){
  return [];
}

//On prepare les résultats de chaque élève
//app.itemsProgressions=app.orderBy(app.itemsProgressions,'rei_date','DESC');
for (var i = app.itemsProgressions.length - 1; i >= 0; i--) {
  var progression=app.itemsProgressions[i];
if(items.indexOf(progression['rei_item'])<0){
  continue;
}

  if(progression['rei_user_type']=="user" && progression['rei_user']!=app.userConfig.userID && (!app.userConfig.ppView || (app.userConfig.ppView && app.userConfig.app.itemsAvisMode=="me"))){
    continue;
  }
  if(app.itemsGetProgressionValue(progression.rei_id)<0){
    continue;
  }
  if((app.userConfig.itemsAvisMode!="students" && progression['rei_user_type']=="eleve") || (app.userConfig.itemsAvisMode=="students" && progression['rei_user_type']=="user")) {
    continue;
  }   
  if(eleves.indexOf(progression['rei_eleve'])<0){
    continue;
  }

  if(!studentsCoordonnees[progression['rei_eleve']]){
    studentsCoordonnees[progression['rei_eleve']]=[];
  }

  var index=items.indexOf(progression['rei_item']);
  if(!studentsCoordonnees[progression['rei_eleve']][index]){
   studentsCoordonnees[progression['rei_eleve']][index]=[];
 }
   studentsCoordonnees[progression['rei_eleve']][index].push(progression);
 };


//On calcul les coordoonnées de chaque élève dans l'espace des compétences
for (var i = eleves.length - 1; i >= 0; i--) {
  var eleve_id=eleves[i];
  if(!studentsCoordonnees[eleve_id]){
   // studentsCoordonnees[eleve_id]=[];
    ids.push(eleve_id);
    continue;
  }
  for (var j = items.length - 1; j >= 0; j--) {
    if(!studentsCoordonnees[eleve_id][j]){
      studentsCoordonnees[eleve_id][j]=-1;
    }else{
      studentsCoordonnees[eleve_id][j]=app.itemsProgressionsSynthese(studentsCoordonnees[eleve_id][j],app.getItemById(items[j]).item_mode)/app.getItemById(items[j]).item_value_max;
    }
    
  }
}

  for (var i =eleves.length - 1; i >= 0; i--) {   
    var eleve=app.getEleveById(eleves[i]);
    eleve.jaugeAcquis=-1;
     var studentsCoordonneesNotNull=[];
    if(studentsCoordonnees[eleve.eleve_id]){
  
for (var j = studentsCoordonnees[eleve.eleve_id].length - 1; j >= 0; j--) {
  if(studentsCoordonnees[eleve.eleve_id][j]!=-1){
  //  alert(studentsCoordonnees[eleve.eleve_id][j]);
    studentsCoordonneesNotNull.push(20*studentsCoordonnees[eleve.eleve_id][j]);
  }
}



      var n=studentsCoordonneesNotNull.length; 
if(n!=0){
  eleve.jaugeAcquis=Math.round((studentsCoordonneesNotNull.reduce(app.add,0)/n)*100)/100; 
}    

    }
  };





if(fixeNbGroupes==0){
  nbGroupes=Math.floor((eleves.length-ids.length)/maxStudentsInGroup)+1;
}else{
  maxStudentsInGroup=Math.floor((eleves.length-ids.length)/nbGroupes)+1;
}

//On calcul les distances entre chaque élève
for (var i = eleves.length - 1; i >= 0; i--) {
  var eleve1_id=eleves[i];
  // if(!studentsSegments[i]){
  //   studentsSegments[i]=[];
  // }
  for (var j = eleves.length - 1; j >= 0; j--) {
    var eleve2_id=eleves[j];
    if(i==j){
      //studentsSegments[i][j]="nd";
      continue;
    }

     if(!studentsCoordonnees[eleve1_id]){
      continue;
    }
    if(!studentsCoordonnees[eleve2_id]){
      continue;
    }
    //if(typeof studentsSegments[i][j]=='undefined'){
      studentsSegments.push({
        from:i,
        to:j,
        value:app.distanceEuclidienne(studentsCoordonnees[eleve1_id],studentsCoordonnees[eleve2_id])//,
        //coord:app.vectorMiddle(studentsCoordonnees[eleve1_id],studentsCoordonnees[eleve2_id])
      });
    //}
  }
}
if(type=="homogenes"){
  studentsSegments=app.orderBy(studentsSegments,'value','DESC');
}else{
  studentsSegments=app.orderBy(studentsSegments,'value','ASC');
}


//On créé le bon nombre de groupes en prenant des élèves éloignés les uns des autres.
var groupes=[];
for (var i =0,lng= studentsSegments.length; i<lng; i++) { 
  var segment=studentsSegments[i];
  if(ids.indexOf(eleves[segment.from])<0 || ids.indexOf(eleves[segment.to])<0){
    if(groupes.length>=nbGroupes){
      break;
    }
    groupes.push({
      list:[eleves[segment.from]],
      coord:studentsCoordonnees[eleves[segment.from]]
    });
    ids.push(eleves[segment.from]);
    if(groupes.length>=nbGroupes){
      break;
    }
    groupes.push({
      list:[eleves[segment.to]],
      coord:studentsCoordonnees[eleves[segment.to]]
    });
    ids.push(eleves[segment.to]);

  }


}


for (var k = eleves.length - 1; k >= 0; k--) {
  var eleve_id=eleves[k];
  if(ids.indexOf(eleve_id)>=0){continue;}
  var scores=[];
  for (var i = groupes.length - 1; i >= 0; i--) {
    var groupe=groupes[i];   
    scores.push({
     groupe_num:i,
     value:app.distanceEuclidienne(studentsCoordonnees[eleve_id],groupe.coord)
   });
  }

  if(type=="homogenes"){
   scores=app.orderBy(scores,'value','ASC');
 }
 else{
   scores=app.orderBy(scores,'value','DESC');
 }

 var groupe_num=-1;
 var somme=0;
 for (var i =0,lng= scores.length; i <lng; i++) {
  somme+=scores[i].value;
}
if(scores.length!=0){
  score_moyen=somme/scores.length;
}else{
  score_moyen=0;
}
for (var i =0,lng= scores.length; i <lng; i++) {
  var score=scores[i];
  if(groupes[score.groupe_num].list.length>=maxStudentsInGroup){
    continue;
  }
  else{
 groupe_num=score.groupe_num; 
      break; 

    if(type=="homogenes"){
     if(score.value<=score_moyen){
      groupe_num=score.groupe_num; 
      break; 
    } 
  }else{
    if(score.value>=score_moyen){
      groupe_num=score.groupe_num; 
      break; 
    } 
  }
}
}

if(groupe_num==-1){
  continue;
}

if(groupes[groupe_num].list.length<maxStudentsInGroup){
 groupes[groupe_num].list.push(eleve_id);
 ids.push(eleve_id);
}

var new_groupes=[];
for (let  m= groupes.length - 1; m >= 0; m--) {
  new_groupes.push(groupes[m].list);
}
app.renderGroupes(new_groupes);
var eleve=app.getEleveById(eleve_id);
//alert(score.value+" "+score_moyen+" "+eleve.eleve_prenom);

}





var new_groupes=[];
for (var i = groupes.length - 1; i >= 0; i--) {
  new_groupes.push(groupes[i].list);
}
new_groupes=app.orderBy(new_groupes,'length','DESC');
return new_groupes;

}