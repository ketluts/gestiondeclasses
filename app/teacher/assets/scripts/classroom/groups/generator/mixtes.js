/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.getGroupMixite=function(groupe,other_groupe,nb_filles_max,nb_boys_max){
  var nb_fille=0;
  var nb_boys=0;
  for (var i = groupe.length - 1; i >= 0; i--) {
    var eleve=groupe[i];
    if(eleve.eleve_genre=="F"){
      nb_fille++;
    }
    if(eleve.eleve_genre=="G"){
      nb_boys++;
    }
  }
  for (var i = other_groupe.length - 1; i >= 0; i--) {
    var eleve=other_groupe[i];
    if(eleve.eleve_genre=="F"){
      nb_fille++;
    }
    if(eleve.eleve_genre=="G"){
      nb_boys++;
    }
  }
  if(nb_fille*100/nb_filles_max>100 || nb_boys*100/nb_boys_max>100){return 0;}
  return 1;
}