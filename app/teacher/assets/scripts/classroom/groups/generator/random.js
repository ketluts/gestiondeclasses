/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.getGroupsRandom=function(nbGroupes){
  //GROUPES ALEATOIRES
  var mixite=false;
  if(document.getElementById('groupes_mixtes').checked){
    mixite=true;    
    document.getElementById("classroom-groupes-selectColor").value="genres";
  }
  var eleves=clone(app.currentClasse.eleves);
  eleves.sort(function(){ return Math.round(Math.random())});
  var groupes=[];
  if(mixite){     
    var tab_girls=[];
    var tab_boys=[];
    var tab_others=[];
    for(var i=0,lng=eleves.length;i<lng;i++){
      var eleve=app.getEleveById(eleves[i]);
      if(eleve.eleve_genre=="F"){
        tab_girls.push(eleve);
      }
      else if(eleve.eleve_genre=="G"){
        tab_boys.push(eleve);
      }
      else{
        tab_others.push(eleve);
      }
    }
    tab_girls.sort(function(){ return Math.round(Math.random())});
    tab_boys.sort(function(){ return Math.round(Math.random())});
    tab_others.sort(function(){ return Math.round(Math.random())});
    var tab_eleves=tab_girls.concat(tab_boys).concat(tab_others);
    for(var i=0,lng=tab_eleves.length;i<lng;i++){
     var eleve=tab_eleves[i];
     var k=i%nbGroupes;
     if(!groupes[k]){groupes[k]=[];}
     groupes[k].push(eleve.eleve_id);
   }
 }else{   
   for(var i=0,lng=eleves.length;i<lng;i++){
     var eleve=app.getEleveById(eleves[i]);
     var k=i%nbGroupes;
     if(!groupes[k]){groupes[k]=[];}
     groupes[k].push(eleve.eleve_id);
   }
 }
 return groupes;
}