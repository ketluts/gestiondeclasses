/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.getGroupsByScore=function(nbGroupes,type){
  if(document.getElementById('groupes_de_niveaux_mode').value=="competences"){
    return app.getGroupsBySkills(nbGroupes,type);   
  }
  var groupes_de_niveaux_type=document.getElementById('groupes_de_niveaux_type').value;
  document.getElementById("classroom-groupes-selectColor").value="notes";
  var eleves=[];
  for (var i = app.currentClasse.eleves.length - 1; i >= 0; i--) {
    eleves.push(app.getEleveById(app.currentClasse.eleves[i]));
  };
  eleves.sort(function(a,b) { return parseFloat(a.jaugeNotes) - parseFloat(b.jaugeNotes) } );
  //On génère les groupes homogènes ou hétérogènes
  var groupes=[];
  var n=0;
  var j=0;
  for (var i =0,lng= eleves.length ; i<lng; i++) {
   var eleve=eleves[i];
   if(eleve.jaugeNotes==-1){j++; continue;}
   if(groupes_de_niveaux_type=="heterogenes"){
    n=(i+j)%nbGroupes;
  }else{
   if((i-j)%(Math.round(lng/nbGroupes))==0 && (i-j)!=0){n++;}
 }
 if(!groupes[n]){groupes[n]=[];}
 groupes[n].push(eleve.eleve_id);
}
  //Si la case sociogroupes est cochée, on réorganise les groupes pour tenir compte du sociogramme
  if(document.getElementById('sociogroupes').checked && groupes_de_niveaux_type=="heterogenes"){
    var relations=app.getSociogrammeRelations();
    //On récupère les groupes hétérogènes par ligne (on suppose que les élèves d'une même ligne sont d'un niveau similaire)
    var temp_groupes=[];
    var new_groupes=[];
    for (var i = groupes.length - 1; i >= 0; i--) {
      var groupe=groupes[i];
      new_groupes[i]=[];
      for (var j = groupe.length - 1; j >= 0; j--) {
       var eleve= groupe[j];
       if(!temp_groupes[j]){temp_groupes[j]=[];}
       temp_groupes[j].push(eleve);
     };
   };
  //On reforme les groupes ligne par ligne en plaçant les élèves dans les groupes qui leurs correspondent le mieux
  var k=1;
  for (var i = temp_groupes.length - 1; i >= 0; i--) {
    var groupe=temp_groupes[i];
    var tab_positions=[];
    for (var j = groupe.length - 1; j >= 0; j--) {
      var eleve=groupe[j];
      //On créé par élève un tableau de scores par groupe disponible
      var scores=[];
      for (var n =0,lng= new_groupes.length ; n<lng; n++) {
        scores[n]=app.getGroupeScore({eleve_id:eleve},new_groupes[n],relations);      
      };
      var max=getMaxOfArray(scores);
      var position=scores.indexOf(max);
      tab_positions.push([eleve,max,scores]);
    };  
    //On place les élèves dans le groupe qui lui correspond le mieux en commençant par le score le plus élevé
    tab_positions.sort(function(a,b) { return a[1] - b[1] } );
    while(tab_positions.length>0){
      var max=getMaxOfArray(tab_positions[0][2]);
      var position=tab_positions[0][2].indexOf(max);
      if(new_groupes[position].length==k){
        tab_positions[0][2][position]=-10000000;  
      }
      else{
        new_groupes[position].push({eleve_id:tab_positions[0][0]}); 
        tab_positions.splice(0, 1);
      }
    };
    k++;
  };
    //On met en forme les groupes pour ne garder que l'id de l'élève
    groupes=[];
    for (var i =0,lng= new_groupes.length; i < lng; i++) {
      if(!groupes[i]){groupes[i]=[];}
      var groupe=new_groupes[i];
      for (var j = groupe.length - 1; j >= 0; j--) {
       var eleve=groupe[j];
       if(!groupes[i][j]){
        groupes[i][j]=[];
      }
      groupes[i][j]=eleve.eleve_id;
    }
  }
}
return groupes;
}