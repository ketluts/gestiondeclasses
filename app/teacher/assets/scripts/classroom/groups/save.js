/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.renderGroupSave=function(){
  var groupe_data=JSON.stringify(app.currentClasse.currentGroupes);
  var name=document.getElementById('classe-groupe-save-text').value;
  $.post(app.serveur + "index.php?go=groupes&q=add"+app.connexionParam,
  {
    groupe_name:name,
    classe_id:app.currentClasse.classe_id,
    groupe_data:groupe_data,
    time:Math.floor(app.myTime()/1000),
    sessionParams:app.sessionParams
  },
  function(data) {
   app.render(data);
   app.renderClasseGroupesSelect();
    if(!app.currentClasse.currentGroupesNum){
      app.currentClasse.currentGroupesNum=0;
    }
   document.getElementById('classroom-students-toolbar-selectGroup').value=app.currentClasse.currentGroupesNum;
   app.classroomGroupsSelectSaveRender(true);
   app.classroomGroupsDeleteBtnRender(app.currentClasse.groupes.length-1);
   $('#classroom-groupes-doSave').button('reset');
 }
 );
}
app.classroomGroupLoad=function(value){
  if(value==-1){
    app.classroomGroupsDeleteBtnRender();
    return;
  }
  app.renderGroupes(jsonParse(app.currentClasse.groupes[value].groupe_data));
  app.classroomGroupsDeleteBtnRender(value);
}
app.classroomGroupDelete=function(groupe_num,confirm){
  if(!confirm){
    app.alert({title:'Supprimer "'+app.currentClasse.groupes[groupe_num].groupe_name+'" ?',type:'confirm'},function(){app.classroomGroupDelete(groupe_num,true);});
    return;
  }
  if(app.currentClasse.currentGroupesNum==groupe_num){
    app.currentClasse.currentGroupesNum=null;
    app.classeElevesRender();
  }
  $.post(app.serveur + "index.php?go=groupes&q=delete"+app.connexionParam,
  {
    classe_id:app.currentClasse.classe_id,
    groupe_id:app.currentClasse.groupes[groupe_num].groupe_id,
    sessionParams:app.sessionParams
  }, function(data) {
    document.getElementById("classroom-groupes-liste").innerHTML='';
    app.currentClasse.currentGroupes=[];
    app.render(data);  
    app.classroomGroupsSelectSaveRender();
    app.renderClasseGroupesSelect();
    if(!app.currentClasse.currentGroupesNum){
      app.currentClasse.currentGroupesNum=0;
    }
    document.getElementById('classroom-students-toolbar-selectGroup').value=app.currentClasse.currentGroupesNum;
    app.classroomGroupsDeleteBtnRender();
  }
  );
}
app.classroomGroupsSelectSaveRender=function(select_last){
  var groupes=app.currentClasse.groupes;
  var lng=groupes.length;
  var html=[];
  if(lng==0){
    html.push('<option value="-1">Aucune sauvegarde</option>');
    html.push('</select>');
    document.getElementById('classroom-groupes-selectSave').innerHTML=html.join('');
    app.classroomGroupsDeleteBtnRender();
    return;
  }
  html.push('<option value="-1">Charger des groupes</option>');
  var selected='';
  for (var i = 0; i <lng; i++) {
    if(!groupes[i].groupe_id){
      continue;
    }
    if(i==lng-1 && select_last){
      selected="selected";
    }
    html.push('<option value="'+i+'" '+selected+'>');
    html.push(groupes[i].groupe_name);
    html.push('</option>');
  };
  document.getElementById('classroom-groupes-selectSave').innerHTML=html.join('');
}
app.classroomGroupsDeleteBtnRender=function(groupe_num){
  if(!groupe_num){
    document.getElementById('classroom-groupes-deleteSave').innerHTML='<div class="btn btn-danger" disabled="disabled"><span class="glyphicon glyphicon-trash"></span></div>';
    return;
  }
  var groupe_id=app.currentClasse.groupes[groupe_num].groupe_id;
  if(groupe_id!==null){
    var html=[];
    html.push('<div class="btn btn-danger" onclick="app.classroomGroupDelete('+groupe_num+');"><span class="glyphicon glyphicon-trash"></span></div>');
  }
  document.getElementById('classroom-groupes-deleteSave').innerHTML=html.join('');
}