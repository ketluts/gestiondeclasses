/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.classroomGroupsCheckboxsRules=function(checkbox){
  if(document.getElementById('groupes_de_niveaux').checked && checkbox=="niveaux"){
    document.getElementById('groupes_mixtes').checked=false;
  }
  if(document.getElementById('groupes_de_niveaux').checked && checkbox=="mixite"){
    document.getElementById('groupes_de_niveaux').checked=false;
  }
  if(document.getElementById('groupes_de_niveaux_type').value=="homogenes" && document.getElementById('groupes_de_niveaux').checked && document.getElementById('sociogroupes').checked){
    document.getElementById('groupes_de_niveaux_type').value="heterogenes";
    document.getElementById('groupes_mixtes').checked=false;
  }
  if(document.getElementById('groupes_de_niveaux_type').value=="homogenes" && document.getElementById('groupes_de_niveaux').checked && checkbox=="sociogroupes"){
    document.getElementById('groupes_de_niveaux_type').value="heterogenes";
    document.getElementById('groupes_mixtes').checked=false;
  }
  app.classroomGroupsToolbarRender();
}
app.classroomGroupsFormRender=function() {
 var classe=app.currentClasse;
 if (!classe.eleves) {
  return;
}
var lng = classe.eleves.length;
var html = [];
for (var j = 2; j < Math.round(lng / 2) + 1; j++) {
  html.push('<option value=\"' + j + '\"');
  if (j == 3) {
    html.push(' selected');
  }
  html.push('>Créer des groupes de ' + j + '</option>');
}
document.getElementById('groupsGenerator-studentsNb').innerHTML = html.join('');
var html = [];
for (var j = 2; j < Math.round(lng / 2) + 1; j++) {
  html.push('<option value=\"' + j + '\"');
  if (j == 2) {
    html.push(' selected');
  }
  html.push('> Créer ' + j + ' groupes</option>');
}
document.getElementById('groupsGenerator-groupsNb').innerHTML = html.join('');
};
app.classroomGroupsCohesionScore=function(groupe){
  var relations=app.getSociogrammeRelations();
  var relationsNb=0;
  var relationsMax=0;
  for (var j = relations.length - 1; j >= 0; j--) {
    var relation=relations[j];  
    if(app.currentClasse.eleves.indexOf(relation.relation_to)<0){
      continue;
    }
    if(groupe.indexOf(relation.relation_from)>=0){
     var value=app.getSociogrammeRelationValue(relation.relation_type);
     relationsMax++;
     var index=groupe.indexOf(relation.relation_to);
     if(index>=0 && value>0){
      relationsNb++;
    }
    else if(index<0 && value<0){
      relationsNb++;
    }
  }
};
if(relationsMax!=0){
 return Math.round(relationsNb/relationsMax*100);
}
else{
  return "--";
}
}
app.classroomGroupsToolbarRender=function(){
  if(document.getElementById('sociogroupes').checked){
    $('#classroomGroupsToolbar').css('display','block');
  }else{  
    $('#classroomGroupsToolbar').css('display','none');
  }
}
app.classroomsGroupsPrint=function(){
  var css=document.getElementById("template-groups-export").innerHTML;
  var classeName=app.cleanClasseName(app.currentClasse.classe_nom);
  var html=[]
  html.push('<h1>');
  html.push(classeName);
  html.push('</h1>');
  html.push(document.getElementById("classroom-groupes-liste").innerHTML);
  document.getElementById('export-pdf-data').value=css+html.join('');
  document.getElementById('export-pdf-titre').value=''+classeName+'_groupes_'+moment().format('DD_MMM_YYYY');  
  document.forms["export-pdf-form"].action=app.serveur + "index.php?go=pdf&q=a4" + app.connexionParam;
  document.forms["export-pdf-form"].submit();
}
app.classroomGroupsItemDrop=function(e,groupe_num){
  e.preventDefault();
  if (e.stopPropagation) e.stopPropagation(); 
  var eleve = e.dataTransfer.getData('text/plain').split("~");
  if(groupe_num!=="nc"){
    if(groupe_num=="new_groupe"){
     groupe_num=app.currentClasse.currentGroupes.length;
     app.currentClasse.currentGroupes.push([]);
   }
   if(eleve[1]!=="nc"){
    app.currentClasse.currentGroupes[groupe_num].push(clone(app.currentClasse.currentGroupes[eleve[1]][eleve[0]]));
    app.currentClasse.currentGroupes[eleve[1]].splice(eleve[0], 1);
  }else{
    app.currentClasse.currentGroupes[groupe_num].push(eleve[0]);
  }
}else{
  app.currentClasse.currentGroupes[eleve[1]].splice(eleve[0], 1);
}
app.renderGroupes(app.currentClasse.currentGroupes);
}