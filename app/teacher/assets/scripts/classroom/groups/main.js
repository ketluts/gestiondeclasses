/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.getGroupes=function(type) {  
  //type=0 : nbEleveParGroupe
  //type=1 : nbGroupes
  var nbGroupes=document.getElementById('groupsGenerator-groupsNb').value;
  var nbGroupes = 0;
  if (type == 0) {
    nbGroupes = document.getElementById('groupsGenerator-studentsNb').value;
    nbGroupes = Math.floor(app.currentClasse.eleves.length / document.getElementById('groupsGenerator-studentsNb').value);
  }
  else{
    nbGroupes = Math.floor(app.currentClasse.eleves.length / document.getElementById('groupsGenerator-groupsNb').value);
    nbGroupes = document.getElementById('groupsGenerator-groupsNb').value;
  }
  app.classroomGroupsDeleteBtnRender();
  //GENERATION
  app.currentClasse.currentGroupes=[];  
  if(document.getElementById('sociogroupes').checked && !document.getElementById('groupes_de_niveaux').checked){
    //SOCIOGROUPES
    app.currentClasse.currentGroupes=app.getGroupsByRelations(nbGroupes);    
  }
  else if(document.getElementById('groupes_de_niveaux').checked){
     //GROUPES DE NIVEAUX
     app.currentClasse.currentGroupes=app.getGroupsByScore(nbGroupes,type);
   }
   else{
    //GROUPES ALEATOIRES
    app.currentClasse.currentGroupes=app.getGroupsRandom(nbGroupes);
  }  
  app.renderGroupes(app.currentClasse.currentGroupes);
};