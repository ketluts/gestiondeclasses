/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.loadClasses=function(classes){  
  app.classes=classes;
  //Classe fictive : Tous les élèves
  var n_classe={
    classe_nom:"~Tous les élèves~",
    classe_id:'-1',
    controles:[],
    eleves:[],
    sociogrammes:[],
    groupes:[],
    classe_destinataires:[],
    classe_intelligences:false
  } 
  app.classes.push(n_classe);
   //Classe fictive : Elèves non classés
   var n_classe={
    classe_nom:"~Élèves non classés~",
    classe_id:'-2',
    controles:[],
    eleves:[],
    sociogrammes:[],
    groupes:[],
    classe_destinataires:[],
    classe_intelligences:false
  }   
  app.classes.push(n_classe);
  app.classes=app.orderBy(app.classes,'classe_nom','ASC');
  app.buildClassroomStudent();
  app.buildClassroomGroups();
  app.buildClassroomPlans();
  app.buildClassroomSociogrammes();
  app.setColorClasses();
  app.homeBuildClassesListes();
}

app.homeBuildClassesListes=function(){
 var currentFirstLetter = "";
 var classes=[];
 var family=[];

 for (var i = 0, lng=app.classes.length ; i < lng; i++) {
  var classe=app.classes[i];
  if(classe.classe_id<0){continue;}
  if(app.userConfig.classesDisable.indexOf(classe.classe_id)>=0){continue;}

  var firstLetter = classe.classe_nom.charAt(0);
  if (firstLetter != currentFirstLetter) {
   currentFirstLetter = firstLetter;
   if(family.length>0){classes.push(family);}   
   family=[classe]; 
 }else{
  family.push(classe);
}
}
classes.push(family);
app.homeClassesListe=classes;
//alert(app.homeClassesListe.length);
}
app.setColorClasses=function(){
  for (var i = 0, lng = app.userConfig.classesDisable.length; i < lng; i++) {
    if(!app.getClasseById(app.userConfig.classesDisable[i])){
      app.toggleClasseDisplay(app.userConfig.classesDisable[i]);
    }
  }
  var k=0;
  var nb=app.classes.length-app.userConfig.classesDisable.length*1+1;

  for (var i = 0, lng = app.classes.length; i < lng; i++) {
    var classe=app.classes[i];
    var visible=app.userConfig.classesDisable.indexOf(classe.classe_id);
    classe.visible=true;
    if(visible>=0){
      classe.visible=false;
    } 
    if(visible>=0 || app.classes[i].classe_id<0){
      continue;
    }
    var color="#ffffff";
    var p=(k/nb)*100;
    var fctAffineNum=Math.floor(p/(100/6));
    var n=Math.round((p*100/((fctAffineNum+1)*(100/6)))/100*255);
    if(fctAffineNum%2==1){
      n=255-n;
    }
    n=n.toString(16);
    if(n.length<2){
      n="0"+n;
    }
    if(fctAffineNum==0){
      color="#ff00"+n;
    }
    else if(fctAffineNum==1){
      color="#"+n+"00ff";
    }
    else if(fctAffineNum==2){
      color="#00"+n+"ff";
    }
    else if(fctAffineNum==3){
      color="#00ff"+n;
    }
    else if(fctAffineNum==4){
      color="#"+n+"ff00";
    }
    else if(fctAffineNum==5){
      color="#ff"+n+"00";
    }
    classe.color=color;
    k++;
  }  
}
app.buildClassroomStudent=function(){
  var data=app.elevesByClasses;
  var all=[];
  for (var i = app.classes.length - 1; i >= 0; i--) {
    app.classes[i].eleves=[];
  }
  for (var i =data.length - 1; i >= 0; i--) {
    var classe=app.getClasseById(data[i]['rec_classe']);
    if(!classe){
      continue;
    }
    classe.eleves.push(data[i]['rec_eleve']);
    all.push(data[i]['rec_eleve']);
 }   

 var non_classes=[];
 for (var i = app.eleves.length - 1; i >= 0; i--) {
if(all.indexOf(app.eleves[i].eleve_id)<0){
app.classes[app.classes.length-2].eleves.push(app.eleves[i].eleve_id);
}
}
//app.classes[app.classes.length-2].eleves=arrayUnique(all);



for (var i = app.classes.length - 1; i >= 0; i--) {
 app.sortEleves(app.classes[i].classe_id);
}
}
app.buildClassroomGroups=function(){
  var data=app.userConfig.groupes;
  for (var i = app.classes.length - 1; i >= 0; i--) {
    var classe=app.classes[i];
    classe.groupes=[];

var girls=[];
var boys=[];

var eleves=classe.eleves;

for (let i = eleves.length - 1; i >= 0; i--) {
 var eleve=app.getEleveById(eleves[i]);

if(eleve.eleve_genre=="F"){
  girls.push(eleves[i]);
}
else if(eleve.eleve_genre=="G"){
  boys.push(eleves[i]);
}

}

classe.groupes.push({
groupe_name:"Classe entière ("+eleves.length+")",
groupe_data:JSON.stringify([eleves])
});
classe.groupes.push({
groupe_name:"Filles/Garçons",
groupe_data:JSON.stringify([girls,boys])
});
classe.groupes.push({
groupe_name:"Filles ("+girls.length+")",
groupe_data:JSON.stringify([girls])
});
classe.groupes.push({
groupe_name:"Garçons ("+boys.length+")",
groupe_data:JSON.stringify([boys])
});


  }
  for (var i =data.length - 1; i >= 0; i--) {
    var classe=app.getClasseById(data[i].groupe_classe);
    if(!classe){
      continue;
    }
    classe.groupes.push(data[i]);
  }
}
app.buildClassroomPlans=function(){
 var data=app.userConfig.plans;
 for (var i = app.classes.length - 1; i >= 0; i--) {
  app.classes[i].plans=[];
}
for (var i =data.length - 1; i >= 0; i--) {
  var classe=app.getClasseById(data[i]['plan_classe']);
  if(!classe){
    continue;
  }
  classe.pdc=jsonParse(data[i]['plan_data']);
}
}
app.buildClassroomSociogrammes=function(){
  var data=app.userConfig.sociogrammes;    
  for (var i = app.classes.length - 1; i >= 0; i--) {
    app.classes[i].sociogrammes=[];
  }
  for (var i = data.length - 1; i >= 0; i--) {
    var classe=app.getClasseById(data[i].sociogramme_classe);
    if(!classe){
      continue;
    }
    classe.sociogrammes.push(data[i])
  }
  if(app.currentView=="classe"){
   app.renderClasseSociogrammesSelectSave();
   app.renderClasseSociogrammeDeleteButton(-1);
 }
}