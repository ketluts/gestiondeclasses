/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.renderControleNotes=function(controle_id){
	//$('#controle_classes_form_'+controle_num+'').remove();
	var classe=app.currentClasse;  
	var controle=app.getTestById(controle_id);
  	//On recherche le controle précédent de la même catégorie
  	var previous_controles=app.getPreviousControles(controle);
  	controle.previous_controles=previous_controles;
	//On prépare les notes par élèves
	var temp_notes=[];
	for (var i = app.notes.length - 1; i >= 0; i--) {
		var note=app.notes[i];
		if(note.note_controle==controle_id){
			temp_notes.push(note);
		}
	};
	var notes=[];
	notes[0]=[];
	notes[1]=[];
	notes[2]=[];//Seulement les valeurs numériques
	for (var i =0, lng= temp_notes.length; i <lng; i++) {
		notes[0].push(temp_notes[i].note_eleve);
		notes[1].push(temp_notes[i].note_value); 
		if(!isNaN(temp_notes[i].note_value)){
			notes[2].push(temp_notes[i].note_value);  
		} 
	};
	var html=[];
	var temp_html=[];
	html.push('<div class="h2">'+controle.controle_titre);
	html.push('<br/>');
	html.push('<small>'+controle.controle_categorie+'</small>');
	html.push('</div>');
	html.push('<input class="form-control" style="display:inline-block;" onkeyup="app.controleNotesImport('+controle_id+');" id="controles-notes-import-'+controle_id+'" placeholder="Copier/Coller une colonne d\'un tableur."/>');
	html.push('<br/>');
	html.push('<br/>');
	html.push('<table class="classroom-tests-notes">');  
	html.push('<thead>');
	html.push('<tr>');
	html.push("<td></td>");
	html.push("<td>Élèves</td>");
	html.push('<td>Notes /'+controle.controle_note+'</td>');
	html.push('<td colspan="3">Progression</td>');
	html.push('</tr>');
	html.push('</thead>');
	for (var i=0, lng= classe.eleves.length ; i< lng; i++) {
		var currentValue="";
		var eleve=app.getEleveById(classe.eleves[i]);
		var  selected="";
		var id=eleve.eleve_id;  
		if(notes[0]){
			var index=notes[0].indexOf(eleve.eleve_id);
		}   
		if(notes[0] && index!=-1){
			currentValue=notes[1][index];
		}    
		html.push('<tr>');
		html.push('<td>');
		html.push('<input type="checkbox" value="" id="note_'+controle_id+'_'+eleve.eleve_id+'_checkbox" onclick="app.getProgressionByGroupes('+controle_id+');"/>');
		html.push('</td>');
		html.push('<td>');
		html.push(app.renderEleveNom(eleve));
		html.push('</td>');
		html.push('<td class="classroom-tests-notes-value">\
			<input type="text" class="note_'+controle_id+'_'+eleve.eleve_id+'_input" onkeyup="app.controlesNextNoteInput(event,\'note_'+controle_id+'_'+(i*1+1)+'_input\');" value="'+currentValue+'" id="note_'+controle_id+'_'+eleve.eleve_id+'_input" onchange="app.renderNotesCouleur('+controle_id+');app.renderProgression('+controle_id+');"/>\
			</td>');
		html.push('<td>');
		html.push('<span id="note_'+controle_id+'_'+eleve.eleve_id+'_progression"></span>');
		html.push('</td>');
		html.push('<td>');
		html.push('<span id="note_'+controle_id+'_'+eleve.eleve_id+'_etoiles"></span>');
		html.push('</td>');
		html.push('<td>');
		html.push('<input type="hidden" id="note_'+controle_id+'_'+eleve.eleve_id+'_etoiles_value" value=""/>');
		html.push('</td>');
		html.push('</tr>');
	}
	html.push('</table>');   
	html.push('<div id="controle_'+controle_id+'_groupe_progression" class=""></div>');  
	html.push('<div class="pull-right btn-group">');
	html.push('<button class="btn btn-default" onclick="$(\'#classeTestsEdit_'+controle.controle_periode+'\').css(\'display\',\'none\');$(\'#export-tests-'+controle.controle_periode+'_bloc\').goTo();">Fermer</button> ');
	html.push('<button class="btn btn-primary classroom-tests-notes-value_save" onclick="$(this).button(\'loading\');app.controleNotesSave('+controle_id+');" data-loading-text="Enregistrement..."><span class="glyphicon glyphicon-save" ></span> Enregistrer</button>');
	html.push('</div>');

	$('#classeTestsEdit_'+controle.controle_periode).html(html.join('')).css('display','').goTo();
	app.renderNotesCouleur(controle_id);
	app.renderProgression(controle_id);
	app.getProgressionByGroupes(controle_id);
};
app.controleNotesSave=function(controle_id){
	if(!app.checkConnection()){
		$('.classroom-tests-notes-value_save').button('reset');
		return false;
	}
		var controle=app.getTestById(controle_id);
	var notes=[];
	var etoiles=[];
	app.renderProgression(controle_id);
	var eleves=app.currentClasse.eleves;
	for (var i = eleves.length - 1; i >= 0; i--) {
		var eleve=app.getEleveById(eleves[i]);
		var temp_note=[];
		temp_note.push(eleve.eleve_id);
		temp_note.push(document.getElementById('note_'+controle_id+'_'+eleve.eleve_id+'_input').value);
		notes.push(temp_note);
		var temp_etoile=[];
		var etoile_value=document.getElementById('note_'+controle_id+'_'+eleve.eleve_id+'_etoiles_value').value;
		temp_etoile.push(eleve.eleve_id);
		temp_etoile.push(etoile_value);
		if(etoile_value>0){ 
			etoiles.push(temp_etoile);
		}
	};
	$.post(app.serveur + "index.php?go=etoiles&q=add"+app.connexionParam,
	{
		classe_id:app.currentClasse.classe_id,
		etoiles:JSON.stringify(etoiles),
		defi_id:controle.controle_id,
		etoile_type:'progression',
		app_views:'classe',
		time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
	},
	function(data) {
		app.render(data);  
	}
	);
	$.post(app.serveur + "index.php?go=notes&q=add"+app.connexionParam,
	{
		notes:JSON.stringify(notes),
		controle_id:controle.controle_id,
		classe_id:app.currentClasse.classe_id,
sessionParams:app.sessionParams
	},
	function(data) {
		app.render(data);   
		app.renderControleNotes(controle_id);
		//app.classeTestsRender();
		app.testsRenderNotes(app.currentClasse.classe_id);
		$('.classroom-tests-notes-value_save').button('reset');
		$('#controles').goTo();
	}
	);
};
 app.getControleMoyenne=function(controle_id){
  	var notes=[];
  	for (var i = app.notes.length - 1; i >= 0; i--) {
  		var note=app.notes[i];
  		if(note.note_controle==controle_id){
  			notes.push(note);
  		}
  	};
  	var notesTotal=0;
  	var k=0;
  	for (var i = notes.length - 1; i >= 0; i--) {
  		if(!isNaN(notes[i].note_value)){
  			notesTotal+=notes[i].note_value*1;
  			k++;
  		}
  	}
  	if(k!=0){
  		return Math.round((notesTotal/k)*100)/100;
  	}
  	return 0;
  }