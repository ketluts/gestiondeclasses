/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.getTestById=function(test_id) {
	return app.controles[app.testsIndex[test_id]];
};
app.buildTestsIndex=function(){
	app.testsIndex=[];
	var categories=[];
	for (var i = 0, lng = app.controles.length; i < lng; i++) {
		var controle=app.controles[i];
		app.testsIndex[controle.controle_id]=i;
		if(!controle['controle_categorie']){continue;}
		var categories=categories.concat(controle['controle_categorie']);
	}
	app.controlesCategoriesDatalist=arrayUnique(categories.map(function(value){
		if(value){
			return value.trim();
		}
	})).sort();
  	//On génére le Datalist pour les catégories des tests
  	var options='';
  	for(var i = 0,lng=app.controlesCategoriesDatalist.length; i < lng; i++){
  		options += '<option value="'+app.controlesCategoriesDatalist[i]+'" />';
  	}
  	document.getElementById('list_controles_categories').innerHTML = options;
  }
  app.notesByUserBuild=function(){
	app.myNotes=[];
	for (var i = app.notes.length - 1; i >= 0; i--) {
		var note=app.notes[i];
		var controle=app.getTestById(note.note_controle);
		if(controle.controle_user!=app.userConfig.userID){
			continue;
		}
		app.myNotes.push(note);
	}
}