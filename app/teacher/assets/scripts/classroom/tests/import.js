/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

 app.controleNotesImport=function(input_id){
	var data=document.getElementById('controles-notes-import-'+input_id+'').value;
	var notes=data.split(' ');
	for (var i =0,lng= notes.length; i <lng; i++) {
		var note=notes[i];
		if(app.currentClasse.eleves[i]){
			document.getElementById('note_'+input_id+'_'+app.currentClasse.eleves[i]+'_input').value=note;
		}		
	};
	app.renderNotesCouleur(input_id);
	app.renderProgression(input_id);
}