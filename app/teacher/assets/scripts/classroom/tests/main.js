/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
//##############
//TESTS RENDER
//##############
app.classeTestsRender=function(classe_id){	
	var classe=app.currentClasse;
	var html=[];
	var millesime=app.getPeriodeById(app.active_periode.periode_parent);
	for (var i = app.periodes.length - 1; i >= 0; i--) {		
		var periode=app.periodes[i];
		if(periode.periode_type!="p" || periode.periode_parent!=millesime.periode_id){continue;}
		var html_temp=app.testsRenderByPeriode(periode.periode_id);
		if(html_temp=="" && periode.periode_id>app.active_periode.periode_id){
			continue;
		}
		if(html.length!=0){

			html.push('<hr/>');
		}
		html.push('<div class="flex-rows nobreak" id="export-tests-'+periode.periode_id+'_bloc">');  
		html.push('<div class="flex-3 classroom-tests-table">');  
		html.push('<div class="h4 text-center">Relevé de notes - '+millesime.periode_titre+' - '+periode.periode_titre+'</div>');
		html.push(html_temp);
		html.push("</div>");
		html.push('<div class="flex-2 aside" style="display:none;"  id="classeTestsEdit_'+periode.periode_id+'">');
		html.push("</div>");
		html.push("</div>");
	}
	if(app.currentView=="bilans"){
		return html.join('');
	}

	document.getElementById('classeTestsExport').innerHTML=html.join('');
	app.testsRenderNotes(classe.classe_id);
	$('.export-tests').dataTable({
		"ordering": false,
		"paging":   false,
		dom: 'Bt',

		buttons: [
		{
			extend: 'csv',
			text: 'CSV'
		},
		{
			extend: 'excel',
			text: 'Excel'
		} 
		],
		stateSave: true,
		"language":app.datatableFR
	});
	document.getElementById('controle_date').value=moment().format('DD/MM/YYYY');
}
app.testsRenderByPeriode=function(periode_id){
	var bilan=false;
	app.currentView=="bilans" ? bilan=true:false;

	var html=[];
	
	var periode=app.getPeriodeById(periode_id);
	var classe=app.currentClasse;
	var eleves=classe.eleves;

	if(app.userConfig.ppView){
		var tests=app.getTestsByPeriodeAndDiscipline(periode_id,false);
		if(tests.length==0){
			return "";
		}
		var source   = document.getElementById("template-tests-render-pp").innerHTML;
		var template = Handlebars.compile(source);
		var context = {
			periode_id:periode_id,
			disciplines:app.disciplines,
			eleves:eleves,
			tests:tests,
			bilan:bilan
		};		
		html.push(template(context)); 
	}else{

		for (var i = app.disciplines.length - 1; i >= 0; i--) {
			var discipline=app.disciplines[i];
			if(discipline.discipline_id==app.userConfig.matiere){
		
		
			var tests=app.getTestsByPeriodeAndDiscipline(periode_id,discipline.discipline_id);
			if(tests.length==0){
				//continue;
			}
			var source   = document.getElementById("template-tests-render-user").innerHTML;
			var template = Handlebars.compile(source);
			var context = {
				periode_id:periode_id,
				discipline_id:discipline.discipline_id,
				eleves:eleves,
				tests:tests,
				bilan:bilan
			};			
			html.push(template(context)); 
			}
		}	




	}
	return html.join('');
}
app.getTestsByPeriodeAndDiscipline=function(periode_id,discipline_id){
	var tests=[];
	for (var i = 0; i < app.controles.length; i++) {
		var test=app.controles[i];

		if(!test.controle_discipline){
			test.controle_discipline=-1;
		}
		
		if(test.controle_periode!=periode_id){
			continue;
		}

		if(test.controle_classe!=app.currentClasse.classe_id){
			continue;
		}
		
		
		//alert('1');
		if(test.controle_discipline!=discipline_id && discipline_id!==false){
			continue;
		}
		//alert('2');
		//alert(app.userConfig.matiere);
		// if(test.controle_discipline!=app.userConfig.matiere && discipline_id!==false){
		// 	continue;
		// }
		//alert('3');
		if(test.controle_user!=app.userConfig.userID && app.userConfig.ppView){
			continue;
		}
		//alert('4');
		tests.push(test);
	}
return tests.sort(function(a,b){
      if(a['controle_categorie']>b['controle_categorie']) return 1;
      if(a['controle_categorie']<b['controle_categorie']) return -1;
      if(a['controle_date']>b['controle_date']) return 1;
      if(a['controle_date']<b['controle_date']) return -1;
      return 0;
    });
}
app.testsRenderNotes=function(classe_id){
	var millesime=app.getPeriodeById(app.active_periode.periode_parent);
	var classe=app.getClasseById(classe_id);
	if(app.userConfig.ppView){
		for (var i = app.periodes.length - 1; i >= 0; i--) {		
			var periode=app.periodes[i];
			var periode_id=periode.periode_id;
			if(periode.periode_type!="p" || periode.periode_parent!=millesime.periode_id){continue;}
			var notesParEleveEtDiscipline=[];
			var moyennesParEleve=[];
			var notesParDiscipline=[];
			var disciplinesIDs=[];
			var moyennes=[];
			for (var j = 0; j < app.notes.length; j++) {
				var note=app.notes[j];						
				var controle=app.getTestById(note.note_controle);
				if(controle.controle_periode!=periode_id){
					continue;
				}
				if(controle.controle_classe!=classe_id){
					continue;
				}
				notesParEleveEtDiscipline[note.note_eleve]=notesParEleveEtDiscipline[note.note_eleve]||[];
				notesParEleveEtDiscipline[note.note_eleve][controle.controle_discipline]=notesParEleveEtDiscipline[note.note_eleve][controle.controle_discipline]||[];
				notesParEleveEtDiscipline[note.note_eleve][controle.controle_discipline].push(note);
				disciplinesIDs.push(controle.controle_discipline);
			}
			disciplinesIDs=arrayUnique(disciplinesIDs);
			for (var j = 0; j < classe.eleves.length; j++) {
				var eleve_id=classe.eleves[j];
				moyennesParEleve[eleve_id]=[];
				notesParEleveEtDiscipline[eleve_id]=notesParEleveEtDiscipline[eleve_id]||[];
				for (var k = disciplinesIDs.length - 1; k >= 0; k--) {
					var discipline_id=disciplinesIDs[k];
					var notes=notesParEleveEtDiscipline[eleve_id][discipline_id]||false;
					var div=$('#note_'+eleve_id+'_'+periode_id+'_'+discipline_id);
					var moyenne=app.moyenne(notes);
					notesParDiscipline[discipline_id]=notesParDiscipline[discipline_id]||[];
					notesParDiscipline[discipline_id].push(moyenne);
					div.html(moyenne).css('color',app.noteColor(moyenne,20));					
					moyennesParEleve[eleve_id].push(moyenne);
				}
				var div=$('#moyenne_'+eleve_id+'_'+periode_id);
				var moyenne=app.testSetMoyenne(div,moyennesParEleve[eleve_id]);
				moyennes.push(moyenne);
			}
			for (var j = 0; j < disciplinesIDs.length; j++) {
				var discipline_id=disciplinesIDs[j];
				var div=$('#moyenneDiscipline_'+discipline_id);				
				var moyenne=app.testSetMoyenne(div,notesParDiscipline[discipline_id]);
				var values=notesParDiscipline[discipline_id].filter(function(num){
					return !isNaN(num);
				}).sort(function(a,b){return a - b});
				app.boxplot(discipline_id,{
					min:values[0],
					max:values[values.length-1],
					median:app.median(values),
					q1:app.q1(values),
					q3:app.q1(values),
					moyenne:moyenne,
					max_box:20
				});
			}
			var div=$('#moyenneGenerale_'+periode_id);
			app.testSetMoyenne(div,moyennes);
		}
	}else{
		for (var i = app.periodes.length - 1; i >= 0; i--) {		
			var periode=app.periodes[i];
			var periode_id=periode.periode_id;
			if(periode.periode_type!="p" || periode.periode_parent!=millesime.periode_id){continue;}
			var moyennes=[];
			for (var j = app.disciplines.length - 1; j >= 0; j--) {
				var discipline=app.disciplines[j];
				var discipline_id=discipline.discipline_id;
				var notesParEleve=[];
				var notesParTests=[];
				var testsIDs=[];
				
				for (var k = 0; k < app.notes.length; k++) {
					var note=app.notes[k];
					var controle=app.getTestById(note.note_controle);
					if(controle.controle_periode!=periode_id){
						continue;
					}
					if(controle.controle_discipline!=discipline_id){
						continue;
					}
					if(controle.controle_classe!=classe_id){
						continue;
					}
					var div=$('#note_'+note.note_eleve+'_'+controle.controle_id);
					if(div){
						notesParEleve[note.note_eleve]=notesParEleve[note.note_eleve]||[];
						notesParEleve[note.note_eleve].push(note);
						notesParTests[note.note_controle]=notesParTests[note.note_controle]||[];
						notesParTests[note.note_controle].push(note);
						testsIDs.push(note.note_controle);
						div.html(note.note_value).css('color',app.noteColor(note.note_value,controle.controle_note));
					}
				}
				for (var k = 0; k < classe.eleves.length; k++) {
					var eleve_id=classe.eleves[k];
					var div=$('#moyenne_'+eleve_id+'_'+periode_id+'_'+discipline_id);
					notesParEleve[eleve_id]=notesParEleve[eleve_id]||false;
					var moyenne=app.moyenne(notesParEleve[eleve_id]);
					moyennes.push(moyenne);
					div.html(moyenne).css('color',app.noteColor(moyenne,20));
				}
				testsIDs=arrayUnique(testsIDs);
				for (var k = 0; k < testsIDs.length; k++) {
					var test_id=testsIDs[k];
					var div=$('#moyenneTest_'+test_id);
					var moyenne=app.moyenne(notesParTests[test_id]);
					div.html(moyenne).css('color',app.noteColor(moyenne,20));
					app.boxplot('test',test_id,notesParTests[test_id]);
					var values=notesParTests[test_id].filter(function(num){
						return !isNaN(num.note_value);
					}).map(function(note){
						return note.note_value*1+note.note_bonus*1;
					}).sort(function(a,b){return a - b});
					app.boxplot(test_id,{
						min:values[0],
						max:values[values.length-1],
						median:app.median(values),
						q1:app.q1(values),
						q3:app.q1(values),
						moyenne:moyenne,
						max_box:app.getTestById(test_id).controle_note
					});
				}
			}
			var moyenne='-';
			var n=moyennes.length;
			if(n>0){
				var moyenne=Math.round((moyennes.reduce(app.add,0)/n)*100)/100;	
			}
			var div=$('#moyenneGenerale_'+periode_id+'_'+discipline_id);
			div.html(moyenne).css('color',app.noteColor(moyenne,20));
		}
	}	
}
app.testSetMoyenne=function(elem,values){
	var moyenne='-';
	values=values.filter(function(num){
		return !isNaN(num);
	});
	var n=values.length;
	if(n>0){
		var moyenne=Math.round((values.reduce(app.add,0)/n)*100)/100;	
	}
	elem.html(moyenne).css('color',app.noteColor(moyenne,20));
	return moyenne;
}
app.renderNotesCouleur=function(controle_id){
	var controle=app.getTestById(controle_id);
	var note_max=controle.controle_note;
	var eleves=app.currentClasse.eleves;
	for (var i = eleves.length - 1; i >= 0; i--) {
		var eleve=app.getEleveById(eleves[i]);
		var note_input=document.getElementById('note_'+controle_id+'_'+eleve.eleve_id+'_input');
		var note=note_input.value;
		note_input.style.color=app.noteColor(note,note_max);		
	};
};
app.noteColor=function(value,max){
	var middle=max/2;
	if(value=="" || isNaN(value)){
		return "#000000";
	}
	if(value>=middle){
		return "#12660e";
	}else{
		return "#ff0000";
	}
}
app.moyenne=function(notes){
	if(!notes){
		return "-";
	}
	/*
	note={
		note_value:...
		note_bonus:...
		controle_coefficient:...
		controle_note:...
	}
	*/
	var somme_notes=0;
	var somme_max=0;
	for (var i = notes.length - 1; i >= 0; i--) {
		var note=notes[i];
		var controle=app.getTestById(note.note_controle);
		if(isNaN(note.note_value)){continue;}
		somme_notes=somme_notes*1+(note.note_value*1+note.note_bonus*1)*controle.controle_coefficient*1;
		somme_max=somme_max*1+controle.controle_note*controle.controle_coefficient*1;
	};
	if(somme_max==0){return 0;}
	return Math.round((somme_notes*20/somme_max)*100)/100;
};