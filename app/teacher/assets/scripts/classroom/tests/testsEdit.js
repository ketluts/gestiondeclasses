/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
//##############
//TESTS EDIT
//##############
app.editerControle=function(controle_id){
	var controle=app.getTestById(controle_id);
	var periodes=[];
	var millesime=app.getPeriodeById(app.active_periode.periode_parent);
	for (var i =0, lng=app.periodes.length; i <lng; i++) {
		var periode=app.periodes[i];
		if(periode.periode_type!="p" || periode.periode_parent!=millesime.periode_id){continue;}
		periodes.push(periode);
	};

	var source   = document.getElementById("template-test-edition").innerHTML;
	var template = Handlebars.compile(source);
	var context = {
		controle:controle,
		periodes:periodes,
		date:moment.unix(controle.controle_date).format('DD/MM/YYYY')
	};		
	
	$('#classeTestsEdit_'+controle.controle_periode).html(template(context)).css('display','').goTo();
	$('#controle_'+controle_id+'_periode').val(controle.controle_periode);
};
app.editerControleSave=function(controle_id){
	if(!app.checkConnection()){return false;}
	var controle=app.getTestById(controle_id);
	var controle_titre=document.getElementById('controle_'+controle_id+'_titre').value;
	var controle_categorie=document.getElementById('controle_'+controle_id+'_categorie').value;
	var controle_coefficient=document.getElementById('controle_'+controle_id+'_coefficient').value;
	var controle_periode=document.getElementById('controle_'+controle_id+'_periode').value;
	var controle_note=document.getElementById('controle_'+controle_id+'_note').value;
	controle.controle_periode=controle_periode;
	var controle_date=moment(document.getElementById('controle_'+controle_id+'_date').value,"DD/MM/YYYY").unix()||Math.floor(app.myTime()/1000);
	$.post(app.serveur + "index.php?go=controles&q=update"+app.connexionParam,
	{ 
		controle_titre:controle_titre,
		controle_coefficient:controle_coefficient,
		controle_categorie:controle_categorie,
		controle_periode:controle_periode,
		controle_id:controle.controle_id,
		controle_note:controle_note,
		controle_date:controle_date,
		classe_id:controle.controle_classe,
		sessionParams:app.sessionParams
	},
	function(data) {
		$('.controle_edition_save').button('reset'); 
		app.render(data);  
		$('#controles').goTo();
	}
	);
};
app.addControle=function(){
	if (!app.checkConnection()) {
		return;
	}
	var controle={};
	controle.controle_note=document.getElementById('controle_note').value;
	controle.controle_titre=document.getElementById('controle_titre').value;
	controle.controle_categorie=document.getElementById('controle_categorie').value;
	controle.controle_coefficient=document.getElementById('controle_coefficient').value;
	controle.controle_periode=document.getElementById('controle_periode').value;
	controle.controle_date=moment(document.getElementById('controle_date').value,"DD/MM/YYYY").unix()||Math.floor(app.myTime()/1000);
	
	$.post(app.serveur + "index.php?go=controles&q=add"+app.connexionParam,
	{
		classe_id:app.currentClasse.classe_id,
		controle:JSON.stringify(controle),
		time:Math.floor(app.myTime()/1000),
		sessionParams:app.sessionParams
	},
	function(data) {
		$("#controle_save_btn").button('reset');
		$('#classroom-tests-addForm').css('display','none'); 
		app.render(data);  
		$('#controles').goTo();
	}
	);
};
app.delControle=function(controle_id,confirm){
	if (!app.checkConnection()) {return;}    
	if(!confirm){
		app.alert({title:'Supprimer ce controle ?',type:'confirm'},function(){app.delControle(controle_id,true);});
		return;
	}
	$.post(app.serveur + "index.php?go=controles&q=delete"+app.connexionParam,{
		controle_id:controle_id,
		classe_id:app.currentClasse.classe_id,
		sessionParams:app.sessionParams
	}, function(data) {
		app.render(data);   
	}
	);
};
app.testsformPeriodesRender=function(){
	var html=[];
	var millesime=app.getPeriodeById(app.active_periode.periode_parent);
	for (var i =0, lng=app.periodes.length; i <lng; i++) {
		var periode=app.periodes[i];
		if(periode.periode_type!="p" || periode.periode_parent!=millesime.periode_id){continue;}
		html.push('<option value="'+periode.periode_id+'">'+periode.periode_titre+'</option>');
	};
	$('.select-periodes').html(html.join('')).val(app.active_periode.periode_id);
}