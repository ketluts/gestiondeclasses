/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.boxplot=function(elem_id,values){
	
$("#boxplot_"+elem_id).highcharts({
		plotOptions: {
			boxplot: {
				color:'black'
			}            
		},
		tooltip: {
			enabled: false
		},
		chart: {
			type: 'boxplot',
			height:150,
			width:80
		},
		title: {
			text: ''
		},
		legend: {
			enabled: false
		},
		xAxis: {
			categories: ['1'],
			title: {
				text: ''
			}
		},
		yAxis: {
			title: {
				text: ''
			},
			min:0,
			max:values.max_box,
			plotLines: [{
				value: values.moyenne,
				color: 'red',
				width: 1,
				zIndex:10
			}]
		},
		series: series=[{
			name: 'Observations',
			data: [
			[values.min,values.q1,values.median,values.q3,values.max]
			]
		}]
	});
};
app.median=function(notes){
	if (notes.length % 2 === 0) {
		// There are an even number of elements in the array; the median
		// is the average of the middle two
		return (notes[notes.length / 2 - 1]*1 + notes[notes.length / 2]*1) / 2;
	} 
	else {
		// There are an odd number of elements in the array; the median
		// is the middle one
		return notes[(notes.length - 1) / 2];
	}
};
app.q1=function(notes) {
	// The first quartile is the median of the lower half of the numbers
	return app.median(notes.slice(0, Math.floor(notes.length / 2)));
};
app.q3=function(notes) {
	// The third quartile is the median of the upper half of the numbers
	return app.median(notes.slice(Math.ceil(notes.length / 2)));
};