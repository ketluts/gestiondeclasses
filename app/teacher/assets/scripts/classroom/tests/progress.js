/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.renderProgression=function(controle_id){
  	var eleves=app.currentClasse.eleves;
  	var controle=app.getTestById(controle_id);
  	var notes=[];
  	notes[0]=[];
  	notes[1]=[];
  	notes[2]=[];
  	for (var i = eleves.length - 1; i >= 0; i--) {
  		var eleve=app.getEleveById(eleves[i]);	
  		var note_value=document.getElementById('note_'+controle_id+'_'+eleve.eleve_id+'_input').value;
  		if(note_value!="" && !isNaN(note_value)){
  			notes[0].push(eleve.eleve_id);
  			notes[1].push(note_value);   
  			notes[2].push(app.getProgressionByEleve(eleve.eleve_id,controle,note_value));
  		}		
  	};	
  	var progression='-';
  	var note="";
  	for (var i=0, lng= eleves.length ; i< lng; i++) {
  		note="";
  		progression='-';
  		var eleve=app.getEleveById(eleves[i]);
  		if(notes[0]){
  			var index=notes[0].indexOf(eleve.eleve_id);
  		}   
  		if(notes[0] && index!=-1){
  			note=notes[1][index];
  			progression=notes[2][index][0];
  		}   
		var previous_note="";
		if(notes[2][index]){
			previous_note="Note précédente : "+notes[2][index][1];
		}
		var etoiles=0;
		var progression_html="<span class='previous_note' data-toggle='tooltip' data-placement='top' title='"+previous_note+"'>"+progression+" %</span>";
		var etoiles_html="";
		if(progression=='-'){
			etoiles_html='-';
			progression_html='-';
		}else{
			etoiles=app.getEtoilesByProgression(progression);
			etoiles_html=app.renderEtoiles(etoiles,3);	
		}
		document.getElementById('note_'+controle_id+'_'+eleve.eleve_id+'_etoiles').innerHTML=etoiles_html;
		document.getElementById('note_'+controle_id+'_'+eleve.eleve_id+'_etoiles_value').value=etoiles;
		document.getElementById('note_'+controle_id+'_'+eleve.eleve_id+'_progression').innerHTML=progression_html;
		document.getElementById('note_'+controle_id+'_'+eleve.eleve_id+'_checkbox').value=progression;
	}
	$('.previous_note').tooltip();
};
app.getEtoilesByProgression=function(progression){
	if(progression<0){
		return 0;
	}
	else if(progression<30){
		return 1;
	}
	else if(progression<60){
		return 2;
	} 
	else if(progression<=100){
		return 3;
	} 
}
app.getProgressionByEleve=function(eleve_id,controle,value){
	var  score_progression='-';
	//On recherche la note précédente de la même catégorie
	var previous_controles=controle.previous_controles;
	if(previous_controles.length==0){return score_progression;}
	var previous_controles_ids=[];	
	for (var i = previous_controles.length - 1; i >= 0; i--) {
		previous_controles_ids.push(previous_controles[i].controle_id);
	};
	var previous_notes=[];
	for (var i = app.notes.length - 1; i >= 0; i--) {
		var previous_note=app.notes[i];
		var index=previous_controles_ids.indexOf(previous_note.note_controle);
		if(index>=0 && previous_note.note_eleve==eleve_id){
			previous_notes.push(previous_note);
		}
	}; 
	previous_notes=app.orderBy(previous_notes,'controle_date','DESC');
	if(previous_notes.length==0){return score_progression;}
	var previous_note=previous_notes[0];
	var previous_controle=previous_controles[previous_controles_ids.indexOf(previous_note.note_controle)];
	var pcOldNote=previous_note.note_value*100/previous_controle.controle_note;
	var pcNote=value*100/controle.controle_note;
	if(pcNote>pcOldNote){
		var diff=100-pcOldNote;
		score_progression=Math.round((pcNote-pcOldNote)*100/diff,3);
	}
	else if(pcNote<pcOldNote){
		score_progression=Math.round(100-(pcNote*100/pcOldNote),3)*(-1);
	}
	else{
		if(value==controle.controle_note){
			score_progression=100;
		}
		else{
			score_progression=0;
		}
	}
	return [score_progression,previous_note.note_value+'/'+previous_controle.controle_note];
}
app.getPreviousControles=function(controle){

	var previous_controles=[];
	for (var i = app.controles.length - 1; i >= 0; i--) {
		var other_controle=app.controles[i];
		if(other_controle['controle_classe']!=controle.controle_classe){
			continue;
		}
		if(controle.controle_categorie!=other_controle.controle_categorie){continue;}
		if(controle.controle_discipline!=other_controle.controle_discipline){continue;}
		if(other_controle.controle_date<controle.controle_date){
			previous_controles.push(other_controle);
		}  
	}
	return previous_controles;
}
app.controlesNextNoteInput=function(event,div_id){
	if(event.keyCode==13){
		$('.'+div_id).focus().select();
	}
}
app.getProgressionByGroupes=function(controle_id){
	var n=0;
	var k=0;
	var controle=app.getTestById(controle_id);
	var eleves=app.currentEleves;
	var controle_id=controle.controle_id;
	var somme=0;
	var somme_classe=0;
	for (var i = eleves.length - 1; i >= 0; i--) {
		var eleve=eleves[i];
		var ckeckbox=document.getElementById('note_'+controle_id+'_'+eleve.eleve_id+'_checkbox');
		var note=document.getElementById('note_'+controle_id+'_'+eleve.eleve_id+'_input').value;
		var value=ckeckbox.value;
		if(note!="" && !isNaN(note)){     
			if(value!='-'){
				k++;
				somme_classe=somme_classe*1+value*1;
			}
		}
		if(ckeckbox.checked){
			if(value!='-'){
				n++;
				somme=somme*1+value*1;
			}
		}
	};
	var div=document.getElementById('controle_'+controle_id+'_groupe_progression');
	if(n>0){
		div.innerHTML="<hr>Le groupe sélectionné ("+n+" élève"+app.pluralize(n,"s")+") a une progression moyenne de <strong>"+Math.round(somme/n)+"</strong> points.<br/>"+app.renderEtoiles(app.getEtoilesByProgression(Math.round(somme/n)),3)+"<hr>";
	}
	else{
		div.innerHTML="<hr>La progression moyenne de la classe est de <strong>"+Math.round(somme_classe/k)+"</strong> points.<hr>";
	}
};