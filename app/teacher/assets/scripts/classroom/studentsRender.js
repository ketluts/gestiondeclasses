/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.classroomGroupeSelect=function(num){
  app.classeElevesRender();
  if(num==-1){
    app.currentClasse.currentGroupesNum=false;
    app.classeElevesRender();
  }
  else if(num==-2){
    app.classeElevesRender({genre:'F'});
  }
  else if(num==-3){
    app.classeElevesRender({genre:'G'});
  }
  else{
    app.classeElevesRender({groupesNum:num});
  }

}
app.classeElevesRender=function(args) {
  var classe=app.currentClasse;
  app.currentClasseView="liste";
  document.getElementById('classroom-students-toolbar').style.display="";  
  document.getElementById('classroom-plan-btnSave').style.display="none"; 
  $("#classe-liste-btn").addClass('btn-primary').removeClass('btn-default');  
  $("#classe-plan-btn").removeClass('btn-primary').addClass('btn-default'); 
  var eleves_dom=document.getElementById('classroom-students'); 
  eleves_dom.innerHTML = "";
  eleves_dom.style.overflow=""; 
  eleves_dom.style.backgroundImage="";
  
  var eleves=classe.eleves;
  //Si il n'y a pas d'élèves à afficher
  if(!eleves || eleves.length==0){
    if(classe.loading==true){
     eleves_dom.innerHTML = app.renderLoader();
   }
   else{
    eleves_dom.innerHTML = '<div  class="classroomOnly btn btn-default connexion-requise" onclick="app.classeElevesAddRender();">\
    <span class="glyphicon glyphicon-plus"></span>\
    <span>Ajouter des élèves</span>\
    </div>';
  }
  return;
}
  //##################################



var groupes=[];

  args=args||{};
  app.randomEleve.eleves=[];  
  app.currentEleves=[];


  app.randomEleve.selectInGroupe=0;

  if(args.groupesNum===false || args.groupesNum===undefined){
   groupes[0]=eleves;
     app.currentClasse.currentGroupesNb=1;
 }
 else{
   app.currentClasse.currentGroupesNum=args.groupesNum;
   var groupes_data=jsonParse(app.currentClasse.groupes[args.groupesNum].groupe_data);
   groupes=groupes_data;
   app.currentClasse.currentGroupesNb=groupes_data.length;
 }


  //ON BOUCLE SUR CHAQUE GROUPE
  app.currentClasse.currentGroupes=clone(groupes);
  var html = []; 
  var btn_class="";
  for (var j =0, n= groupes.length; j <n; j++) {
    var groupe=groupes[j];
    html.push('<div class="classroom-students-groupe flex-columns">');
    if(n>1){
      html.push('<div class="classroom-students-groupe-header">');
    //On affiche les boutons pour le groupe 
    var nb=j+1;
    html.push('<div class="pull-left h3">Groupe '+nb+'</div>');
    html.push('<div class="btn-group pull-left">');
    for (var k = 0, llng = app.userConfig.eventsWhiteList.length; k < llng; k++) {
      var type_event=app.userConfig.eventsWhiteList[k];
      var legende=app.legends[type_event]||"";    
      html.push('<div class="btn btn-default btn-sm classroom-student-toolbar-btn" title="'+legende+'" onclick="app.addGroupeEvent({event_type:\''+ type_event + '\',groupe:\''+j+'\'})">');
      html.push('<span class="glyphicon ' + type_event + '"></span>');
      html.push('</div>'); 
    }
    html.push('</div>'); 
    html.push('</div>');
  }
  html.push('<div class="classroom-students-groupe-body">');
  for (var i = 0, lng = groupe.length; i < lng; i++) {
   var eleve = app.getEleveById(groupe[i]);  
   if(!eleve){continue;}
   //if(args.genre && eleve.eleve_genre!=args.genre){continue;}
   app.currentEleves.push(eleve);
   var color_notes="null";
   var niveau_notes_height=0;
   var color_acquis="null";
   var niveau_acquis_height=0;
   if(app.userConfig.coloration==1 || app.userConfig.coloration==3){
    color_notes=app.colorByMoyenne(eleve.jaugeNotes);
    if(eleve.jaugeNotes){
     niveau_notes_height=(eleve.jaugeNotes/20)*46;
   }
   color_acquis=app.colorByMoyenne(eleve.jaugeAcquis);
   if(eleve.jaugeNotes){
     niveau_acquis_height=(eleve.jaugeAcquis/20)*46;
   }
 } 
 html.push('<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 text-center classroom-student">');
 html.push('<div id="eleve_' + eleve.eleve_id + '" onclick="app.go(\'student/'+ eleve.eleve_id + '\');" title="Voir la fiche de cet élève" class="btn classroom-student-name">');
 html.push('<img class="classroom-student-picture" src="'+app.studentPictureGet(eleve.eleve_id)+'"/>');
 html.push(app.renderClasseEleveName(eleve.eleve_id));
 html.push('<div class="classroom-student-jauge">');
 html.push('<div class="classroom-student-jauge-notes" style="background-color:'+color_notes+';height:'+niveau_notes_height+'px;">');
 html.push('</div>');
 html.push('<div class="classroom-student-jauge-acquis" style="background-color:'+color_acquis+';height:'+niveau_acquis_height+'px;">');
 html.push('</div>');
 html.push('</div>');
 html.push('</div>');
 html.push('<div class="btn-group eleve-events-btn classroom-student-toolbar">');
 for (var k = 0, llng = app.userConfig.eventsWhiteList.length; k < llng; k++) {
  var type_event=app.userConfig.eventsWhiteList[k];
  var legende=app.legends[type_event]||"";
  html.push('<button class="btn btn-default btn-sm classroom-student-toolbar-btn" id="event_' + type_event + '_' + eleve.eleve_id + '" data-toggle="tooltip" data-placement="top" title="'+legende+'"');
  html.push('onclick="app.agendaAddQuickEvent(\''+ type_event + '\',\'' + eleve.eleve_id + '\',true);">');
  html.push('<span class="classroom-student-badge" id="badge_' + type_event + '_' + eleve.eleve_id + '"></span>\
    <span class="glyphicon ' + type_event + '"></span>'); 
  html.push('</button>');  
}
html.push('</div>');
html.push('<div class="btn-group eleve-events-btn classroom-student-toolbar">');
html.push('<button class="btn btn-default btn-sm classroom-student-toolbar-btn" title="Événement"');
html.push('onclick="app.studentOrganizerOpen('+eleve.eleve_id+');">');
html.push('<span class="glyphicon glyphicon glyphicon-calendar"></span>'); 
html.push('</button>');  
html.push('</div>');
html.push('</div>');
}
html.push('</div>');
html.push('</div>');
};
eleves_dom.innerHTML = html.join('');
eleves_dom.style.height="auto";
$('.classroom-student-jauge').css('display','none');
if(app.userConfig.coloration==1 || app.userConfig.coloration==3){
 $('.classroom-student-jauge').css('display','block');
} 
$('.classroom-student-picture').css('display','none');
if(app.userConfig.classroomStudentsPicture){
 $('.classroom-student-picture').css('display','block');
} 
app.classeEventsRender();
app.classroomRandomInit();
};
app.renderClasseGroupesSelect=function(){
  var groupes=app.currentClasse.groupes;
  var lng=groupes.length;
  var html=[];
  //html.push('<option value="-1" id="classe-all"></option>');
  //html.push('<option value="-2" id="classe-nb-girls"></option>');
 // html.push('<option value="-3" id="classe-nb-boys"></option>');
  for (var i = 0; i <lng; i++) {
    html.push('<option value="'+i+'">');
    html.push(groupes[i].groupe_name);
    html.push('</option>');
  };
  document.getElementById('classroom-students-toolbar-selectGroup').innerHTML=html.join('');
  //app.classeCompositionRender();
}

app.classeSwitchCouleur=function(){
  app.userConfig.coloration=(app.userConfig.coloration+1)%4;  
  app.pushUserConfig();
  $('.classroom-student-jauge').css('display','none');
  if(app.userConfig.coloration==1 || app.userConfig.coloration==3){
   $('.classroom-student-jauge').css('display','block');
 }
}

app.classeSwitchPicture=function(){
  app.userConfig.classroomStudentsPicture=(app.userConfig.classroomStudentsPicture+1)%2; 
  app.pushUserConfig();
  $('.classroom-student-picture').css('display','none');
  if(app.userConfig.classroomStudentsPicture){
   $('.classroom-student-picture').css('display','block');
 } 
}

app.switchElevesSort=function(){
  app.userConfig.sorting=(app.userConfig.sorting*1+1)%8;  
  app.pushUserConfig();
  app.sortEleves(app.currentClasse.classe_id);
  app.classeElevesRender();
}
app.sortEleves=function(classe_id){
  var classe=app.getClasseById(classe_id);
var eleves=classe.eleves;
  var sorting=app.userConfig.sorting;
  if(sorting==0){
  eleves=app.orderElevesBy(eleves,'eleve_prenom','ASC');
 }
 else if(sorting==1){
 eleves=app.orderElevesBy(eleves,'eleve_prenom','DESC');
}
else if(sorting==2){
eleves=app.orderElevesBy(eleves,'eleve_nom','ASC');
}
else if(sorting==3){
eleves=app.orderElevesBy(eleves,'eleve_nom','DESC');
}
else if(sorting==4){
 eleves=app.orderElevesBy(eleves,'eleve_nom','ASC');
}
else if(sorting==5){
 eleves=app.orderElevesBy(eleves,'eleve_nom','DESC');
}
else if(sorting==6){
eleves=app.orderElevesBy(eleves,'eleve_prenom','ASC');
}
else if(sorting==7){
eleves=app.orderElevesBy(eleves,'eleve_prenom','DESC');
}
return(eleves);
};
app.orderElevesBy=function(array,orderBy,order){
  app.orderByParam=orderBy;
  if(order=="ASC"){
    return array.sort(function(a,b){
     a=app.getEleveById(a);
     b=app.getEleveById(b);
     if(a[app.orderByParam]>b[app.orderByParam]) return 1;
     if(a[app.orderByParam]<b[app.orderByParam]) return -1;
     return 0;
   });
  }
  return array.sort(function(a,b){
   a=app.getEleveById(a);
   b=app.getEleveById(b);
   if(a[app.orderByParam]>b[app.orderByParam]) return -1;
   if(a[app.orderByParam]<b[app.orderByParam]) return 1;
   return 0;
 });
}
app.renderClasseEleveName=function(eleve_id){
  var eleve=app.getEleveById(eleve_id);
  var html=[];  
  if(eleve.eleve_statut=="delegue"){
    html.push('<span class="glyphicon flaticon-medal45 classroom-student-name-badge"></span>');
  }  
  var nom=eleve.eleve_nom;
  var prenom="";
  if(eleve.eleve_prenom){
    prenom=eleve.eleve_prenom;
  }
  if(!nom){
    return ucfirst(prenom);
  }
  if(nom && !prenom){
    return nom.toUpperCase();
  }
  if(app.userConfig.sorting<4){
    html.push('<div class="classroom-student-name-l1">'+ucfirst(prenom)+'</div><div class="classroom-student-name-l2">'+nom.toUpperCase()+'</div>');
  }else{
   html.push('<div class="classroom-student-name-l1">'+nom.toUpperCase()+'</div><div class="classroom-student-name-l2">'+ucfirst(prenom)+'</div>');
 } 
 return html.join('');
};