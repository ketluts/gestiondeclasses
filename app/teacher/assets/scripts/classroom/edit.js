/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.classeRename=function(new_classe_nom){
  if (!app.checkConnection()) {
    return;
  }
  var classe=app.currentClasse;
  if(!new_classe_nom){
    document.getElementById('new_classe_nom_form').style.display = "block";
    document.getElementById('new_classe_nom').focus();
    $('#new_classe_nom_btn').button('reset');
  }
  else{
    $.post(app.serveur + "index.php?go=classe&q=update"+app.connexionParam,
    {
      classe_nom:document.getElementById('new_classe_nom').value,
      classe_id:classe.classe_id,
      sessionParams:app.sessionParams
    }, function(data) {
      app.render(data); 
      app.go('home');
    }
    );
  }
};
app.classeDelete=function(confirm) {
  if (!app.checkConnection()) {
    return;
  }
  if(!confirm){
    app.alert({title:'Voulez-vous vraiment supprimer cette classe ?',type:'confirm'},function(){app.classeDelete(true);});
    return;
  }
  $.post(app.serveur + "index.php?go=classe&q=delete" + app.connexionParam,
  {
    classe_id:app.currentClasse.classe_id,
    sessionParams:app.sessionParams
  }, function(data){
    app.render(data);  
    app.go('home');  
  });
};


app.classroomSetCycle=function(cycle,confirm){
  if (!app.checkConnection()) {return;}

  if(!confirm){
    app.alert({title:'Modifier le cycle de tous ces élèves ?',type:'confirm'},function(){app.classroomSetCycle(cycle,true);});
    return;
  }

// var students=[];
// for (var i = app.currentClasse.eleves.length - 1; i >= 0; i--) {
//  students.push(app.currentClasse.eleves[i].eleve_id);
// }

//On met à jour les données
$.post(app.serveur + "index.php?go=eleves&q=updateStudents"+ app.connexionParam,
{
  students:JSON.stringify(app.currentClasse.eleves),
  student_cycle:cycle,
sessionParams:app.sessionParams
  
},
function(data) {
  app.render(data);   
  app.setElevesJauges(app.currentClasse.eleves);
  if(app.currentClasseView=="liste"){
    app.classeElevesRender();
  }
  else{
    app.planDeClasse();
  }  
  app.classeExportRender();

});


}