/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

//#######################
//SOCIOGRAMME DRAW
//#######################
app.sociogrammeRenderInit=function(){
	if(!app.sociogramme.enableDraw){return;}
	var canvas=document.getElementById('classroom-sociogramme-canvas');
	app.sociogramme.enableDraw=false;
	canvas.height=app.sociogramme.cote;
	canvas.width=app.sociogramme.cote;
	app.sociogramme.ctx=canvas.getContext("2d");
	var ctx=app.sociogramme.ctx;	
	ctx.fillStyle = "#ffffff";
	ctx.fillRect(0, 0, canvas.width, canvas.height);	
	if(app.sociogramme.mode=="students"){
		if(document.getElementById('sociogrammeCircles').checked){
			app.sociogrammeRenderRangs();
		}
	}
	else{
		app.sociogrammeRenderGroups();
	}
	app.sociogrammeRenderStudents();	
	app.sociogramme.enableDraw=true;
	if(app.sociogramme.enable_move){
		setTimeout(function(){app.sociogrammeRenderInit();},10);
	}
}
app.sociogrammeRenderRangs=function(){
	var rangs=app.sociogramme.rangs;
	var ctx=app.sociogramme.ctx;
	var rang_max=0;
	if(rangs.length>0){
		rang_max=rangs[0].value;
	}	
	for (var i = rangs.length - 1; i >= 0; i--) {
		if(document.getElementById('sociogrammeReverse').checked){
			var r=(rangs.length -i+1)*app.sociogramme.step*app.sociogramme.zoom;
		}else{
			var r=(i+1)*app.sociogramme.step*app.sociogramme.zoom;
		}	
		ctx.beginPath();
		ctx.arc(app.sociogramme.centerX, app.sociogramme.centerY, r-1, 0, app.pi*2, true);    
		if(rangs[i].value==0){
			ctx.lineWidth = 4;
		}
		else{
			ctx.lineWidth = 1;
		}
		ctx.strokeStyle = '#003300';
		ctx.stroke();		
		//Affichage des graduations
		ctx.fillStyle = 'black';
		ctx.font = '12pt Arial';
		ctx.fillText(rang_max-i,app.sociogramme.centerX , app.sociogramme.centerY-r-5);
	} 
}
app.sociogrammeRenderGroups=function(){
	var groupes=app.currentClasse.currentGroupes;
	var ctx=app.sociogramme.ctx;	
	for (var i = groupes.length - 1; i >= 0; i--) {
		var centre=app.sociogramme.centres[i];
		var coord=app.sociogrammeGetReduceCoord(centre[0],centre[1]);
		var x=coord.x||0;
		var y=coord.y||0;
		var theta=2*app.pi/groupes.length;
		var r=Math.max(Math.sin(theta/2)*300*0.6*app.sociogramme.zoom,1);
		ctx.beginPath();
		ctx.arc(x, y, r-1, 0, app.pi*2, true);    
		ctx.lineWidth = 1.5;
		ctx.strokeStyle = '#003300';
		ctx.stroke();
		ctx.beginPath();
		ctx.moveTo(x-4, y-4);
		ctx.lineTo(x+4, y+4);
		ctx.moveTo(x-4, y+4);
		ctx.lineTo(x+4, y-4);
		ctx.lineWidth = 2;
		ctx.strokeStyle = '#003300';
		ctx.stroke();		
	}
}
app.sociogrammeRenderStudents=function(){
	var eleves=app.sociogramme.eleves;
	var relations=app.sociogramme.relations;
	var drawed=[];
	var ctx=app.sociogramme.ctx;	
	for (var i = relations.length - 1; i >= 0; i--) {
		var relation=relations[i];   
		if(relations.indexOf(relation.relation_id)!=-1){continue;}	
		drawed.push(relation.relation_id);
		var eleve_1=app.getEleveById(relation.relation_from);
		var eleve_2=app.getEleveById(relation.relation_to);
		if(!eleve_1 || !eleve_2){continue;}
		var value_1=relation.relation_type;
		var color_1=app.getSociogrammeColorLink(relation);
		var value_2=0;
		var other_relation=app.getRelation(relation.relation_to,relation.relation_from,relations);
		var color_2="transparent";
		if(other_relation!=false){
			value_2=other_relation[0].relation_type;
			relations[other_relation[1]].drawed=true;
			var color_2=app.getSociogrammeColorLink(other_relation[0]);
		};		
		if(color_1=="transparent" && color_2=="transparent"){continue;}	
		//Coordonnées de l'élève 1
		var coord=app.sociogrammeGetReduceCoord(eleve_1.sociogramme[app.sociogramme.vue].x,eleve_1.sociogramme[app.sociogramme.vue].y);
		var x1=coord.x;
		var y1=coord.y;
		//Coordonnées de l'élève 1
		var coord=app.sociogrammeGetReduceCoord(eleve_2.sociogramme[app.sociogramme.vue].x,eleve_2.sociogramme[app.sociogramme.vue].y);
		var x2=coord.x;
		var y2=coord.y;
		var grad= ctx.createLinearGradient(x1, y1, x2, y2);
		grad.addColorStop(0, color_1);
		grad.addColorStop(1, color_2);
		ctx.strokeStyle = grad;
		ctx.beginPath();
		ctx.moveTo(x1,y1);
		ctx.lineTo(x2,y2);
		ctx.lineWidth = 2;
		if(value_1==value_2){
			ctx.lineWidth = 4;
		}
		ctx.stroke();
	}
	ctx.font = '12pt Arial';
	for (var i = eleves.length - 1; i >= 0; i--) {
		var eleve=app.getEleveById(eleves[i]);		
		if(eleve.eleve_prenom==""){
			var nom=eleve.eleve_nom;
		}else{
			var nom=eleve.eleve_prenom+" "+ucfirst(eleve.eleve_nom.substr(0,1))+".";
		}
		var textWidth = ctx.measureText (nom);
		ctx.beginPath();
		var coord=app.sociogrammeGetReduceCoord(eleve.sociogramme[app.sociogramme.vue].x,eleve.sociogramme[app.sociogramme.vue].y);
		var x1=coord.x;
		var y1=coord.y;
		var x=x1-5-ctx.measureText(nom).width/2;
		var y=y1-25;
		ctx.rect(x, y, textWidth.width+10, 35);
		eleve.width=textWidth.width+10;
		eleve.height=35;
		ctx.lineWidth = 1;
		ctx.fillStyle = 'white';
		ctx.fill();
		ctx.strokeStyle = 'black';
		ctx.stroke();
		ctx.fillStyle = 'black';
		x=x1-ctx.measureText(nom).width/2;
		y=y1;		
		ctx.fillText(nom,x , y);
	}
}
app.setElevesPositions=function(eleves,rangs){
	var tmpRangs=[];
	for (var i = rangs.length - 1; i >= 0; i--) {	
		var rang =rangs[i];
		var index=tmpRangs.indexOf(rang.value);    
		if(index==-1){				
			tmpRangs.push(rang.value);
		}
	}
	tmpRangs.sort(function(a,b){
		if(a.value>b.value)
			return -1;
		return 1;
	});
	var theta=2*app.pi/eleves.length;
	for (var i = eleves.length - 1; i >= 0; i--) {		
		var eleve=app.getEleveById(eleves[i]);
		if(!eleve.sociogramme){
			eleve.sociogramme=[];
		}
		if(!eleve.sociogramme[app.sociogramme.vue]){
			eleve.sociogramme[app.sociogramme.vue]={
				x:0,
				y:0
			};
		}
		if(document.getElementById('sociogrammeReverse').checked){
			var r=(tmpRangs.length-tmpRangs.indexOf(eleve.rang)+1)*app.sociogramme.step;
		}else{
			var r=(tmpRangs.indexOf(eleve.rang)+1)*app.sociogramme.step;
		}

		var angle=i*theta;
		eleve.sociogramme[app.sociogramme.vue].x=Math.floor(r*Math.cos(angle)+app.sociogramme.centerX);
		eleve.sociogramme[app.sociogramme.vue].y=Math.floor(r*Math.sin(angle)+app.sociogramme.centerY);
	};
	app.sociogramme.mode="students";
}
app.setElevesPositionsByGroupes=function(){	
	app.sociogramme.vue="groupes";
	app.sociogramme.centres=[];
	var groupes=app.currentClasse.currentGroupes;
	var n=groupes.length;
	if(n==0){return;}
	var canvas=document.getElementById('classroom-sociogramme-canvas');
	var ctx = canvas.getContext("2d");
	var theta=2*app.pi/n;
	var r=Math.sin(theta/2)*300*0.6;
	for (var j = 0, lng=groupes.length; j <lng; j++) {
		var angle=j*theta+theta*0.4;
		var new_centre_x=Math.floor(300*Math.cos(angle)+app.sociogramme.centerX);
		var new_centre_y=Math.floor(300*Math.sin(angle)+app.sociogramme.centerY);
		app.sociogramme.centres.push([new_centre_x,new_centre_y,0,0]);
		var groupe=groupes[j];		
		for (var i = 0, llng=groupe.length; i <llng; i++) {
			var eleve=app.getEleveById(groupe[i]);
			angle=(app.pi/3)+(2*app.pi/groupe.length)*i;
			if(!eleve.sociogramme){
				eleve.sociogramme=[];
			}
			if(!eleve.sociogramme[app.sociogramme.vue]){
				eleve.sociogramme[app.sociogramme.vue]={
					x:0,
					y:0
				};
			}
			eleve.sociogramme[app.sociogramme.vue].x=Math.floor(r*Math.cos(angle)+new_centre_x);
			eleve.sociogramme[app.sociogramme.vue].y=Math.floor(r*Math.sin(angle)+new_centre_y);
			eleve.groupe=j;			
		}
	}
};
app.sociogrammeGetReduceCoord=function(x,y){
	x-=app.sociogramme.centerX;
	y-=app.sociogramme.centerY;
	var r=Math.sqrt(x*x+y*y)*app.sociogramme.zoom;
	var theta=Math.atan2(y, x);
	x=Math.floor(r*Math.cos(theta)+app.sociogramme.centerX);
	y=Math.floor(r*Math.sin(theta)+app.sociogramme.centerY);
	return {"x":x,"y":y};
}
app.getSociogrammeColorLink=function(relation){
	var type=relation.relation_type;
	if(app.sociogramme.filtre!=false){
		if(app.sociogramme.filtre=="green" && type!=2 && type!=6){return "transparent";}
		if(app.sociogramme.filtre=="blue" && type!=1 && type!=4){return "transparent";}
		if(app.sociogramme.filtre=="red" && type!=3 && type!=5){return "transparent";}
		if(app.sociogramme.filtre=="green/blue" && type!=1 && type!=4 && type!=2 && type!=6){return "transparent";}
		if(app.sociogramme.filtre=="black" && type!=7){return "transparent";}
	}
	if(app.userConfig.sociogramme_type=="simple" && type>3){
		return "transparent";	
	}
	if(app.userConfig.sociogramme_type!="simple" && type<=3){
		return "transparent";	
	}
	if(type==0){return "transparent";}
	if(type==3 || type==5){return "red";}
	if(type==1 || type==4){return "blue";}
	if(type==2 || type==6){return "green";}
	if(type==7){return "black";}
	return "transparent";
}
app.sociogrammeSyntheseTable=function(){
	var eleves=app.sociogramme.eleves;
	var relations=app.sociogramme.relations;
	var studentsPoints=[];
	for (var i = relations.length - 1; i >= 0; i--) {
		var relation=relations[i]; 
		if(relation.relation_type<4){continue;}
		studentsPoints[relation.relation_to]=studentsPoints[relation.relation_to]||[];
		studentsPoints[relation.relation_to]['total']=studentsPoints[relation.relation_to]['total']||0;
		studentsPoints[relation.relation_to][relation.relation_type]=studentsPoints[relation.relation_to][relation.relation_type]||0;
		studentsPoints[relation.relation_to][relation.relation_type]++;
		studentsPoints[relation.relation_to]['total']++;
	}
	html=[];
	html.push('<table class="table" id="classroom-sociogramme-synthese-tab">');
	html.push('<thead>');
	html.push(' <tr>');
	html.push('<th>Élèves</th>');
	html.push('<th class="sociogramme_synth_blue">Choix positifs</th>');
	html.push('<th class="sociogramme_synth_red">Choix négatifs</th>');
	html.push('<th class="sociogramme_synth_green">Perceptions positives</th>');
	html.push('<th class="sociogramme_synth_black">Perceptions négatifs</th>');
	html.push('<th>Poids relatifs</th>');
	html.push('</tr>');
	html.push('</thead>');
	html.push('<tbody>');
	for (var i = 0; i < eleves.length; i++) {
		var eleve=app.getEleveById(eleves[i]);
		if(!studentsPoints[eleve.eleve_id]){continue;}
		html.push('<tr>');
		html.push('<td>');
		html.push(app.renderEleveNom(eleve));
		html.push('</td>');
		html.push('<td class="sociogramme_synth_blue">'+(studentsPoints[eleve.eleve_id][4]||0)+'</td>');
		html.push('<td class="sociogramme_synth_red">'+(studentsPoints[eleve.eleve_id][5]||0)+'</td>');
		html.push('<td class="sociogramme_synth_green">'+(studentsPoints[eleve.eleve_id][6]||0)+'</td>');
		html.push('<td class="sociogramme_synth_black">'+(studentsPoints[eleve.eleve_id][7]||0)+'</td>');
		html.push('<td>'+(studentsPoints[eleve.eleve_id]['total']||0)+'</td>');
		html.push('</tr>');
	}
	html.push('</tbody>');
	html.push('</table>');
	document.getElementById('classroom-sociogramme-synthese').innerHTML=html.join(''); 
	$('#classroom-sociogramme-synthese-tab').dataTable({
		"paging":   false,
		dom: 't',		
		stateSave: true,
		"language":app.datatableFR
	});
}