/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

//#######################
//SOCIOGRAMME MOUSE
//#######################
app.sociogrammeMouseOver=function(){
	app.sociogramme.over=true;
}
app.sociogrammeMouseOut=function(){
	app.sociogramme.over=false;
}
app.start_move=function(){
	for (var i = app.sociogramme.eleves.length - 1; i >= 0; i--) {
		var eleve=app.getEleveById(app.sociogramme.eleves[i]);
		var coord=app.sociogrammeGetReduceCoord(eleve.sociogramme[app.sociogramme.vue].x,eleve.sociogramme[app.sociogramme.vue].y);
		var x=coord.x;
		var y=coord.y;
		var x_min=x-1-eleve.width/2;
		var x_max=x*1+eleve.width*1-1-eleve.width/2;
		var y_min=y-25;
		var y_max=y*1+eleve.height*1-25;
		if(app.mouse.x>x_min && app.mouse.x<x_max && app.mouse.y>y_min && app.mouse.y<y_max){
			app.sociogramme.selectedEleve=i;	
			app.sociogramme.enable_move=true;
			app.preventTouch=true;
			continue;
		}
	};
	for (var i = app.sociogramme.centres.length - 1; i >= 0; i--) {
		var centre=	app.sociogramme.centres[i];
		var coord=app.sociogrammeGetReduceCoord(centre[0],centre[1]);
		var x=coord.x;
		var y=coord.y;
		if(app.mouse.x>x-15 && app.mouse.x<x+15 && app.mouse.y>y-15 && app.mouse.y<y+15){
			app.sociogramme.selectedCentre=i;	
			app.sociogramme.enable_move=true;			
			app.preventTouch=true;
			continue;
		}
	};
	app.sociogrammeRenderInit();
}
app.movemouse=function(e)
{
	if(app.currentView!="classe"){
		return;
	}
	var pos = $('#classroom-sociogramme-canvas').offset();	
	var new_mousex = e.pageX-pos.left;
	var new_mousey = e.pageY-pos.top;   
	var diffX=app.mouse.x-new_mousex;
	var diffY=app.mouse.y-new_mousey;
	app.mouse.x=new_mousex;
	app.mouse.y=new_mousey;
	if(app.sociogramme.enable_move){
		if(app.sociogramme.selectedEleve>=0 && app.sociogramme.selectedEleve!=null){
			var eleve=app.getEleveById(app.sociogramme.eleves[app.sociogramme.selectedEleve]);
			eleve.sociogramme[app.sociogramme.vue].x-=diffX/app.sociogramme.zoom;
			eleve.sociogramme[app.sociogramme.vue].y-=diffY/app.sociogramme.zoom;
		}		
		if(app.sociogramme.selectedCentre>=0 && app.sociogramme.selectedCentre!=null){	
			var centre=app.sociogramme.centres[app.sociogramme.selectedCentre];	
			centre[0]-=diffX/app.sociogramme.zoom;
			centre[1]-=diffY/app.sociogramme.zoom;
			for (var i = app.sociogramme.eleves.length - 1; i >= 0; i--) {
				var eleve=app.getEleveById(app.sociogramme.eleves[i]);
				if(eleve.groupe==app.sociogramme.selectedCentre){
					eleve.sociogramme[app.sociogramme.vue].x-=diffX/app.sociogramme.zoom;
					eleve.sociogramme[app.sociogramme.vue].y-=diffY/app.sociogramme.zoom;
				}
			}
		}
		app.preventTouch=true;
	}
	if(app.sociogramme.over){
		var cursor=false;
		for (var i = app.sociogramme.eleves.length - 1; i >= 0; i--) {
			var eleve=app.getEleveById(app.sociogramme.eleves[i]);
			var coord=app.sociogrammeGetReduceCoord(eleve.sociogramme[app.sociogramme.vue].x,eleve.sociogramme[app.sociogramme.vue].y);
			var x1=coord.x;
			var y1=coord.y;
			var x_min=x1-1-eleve.width/2;
			var x_max=x1*1+eleve.width*1-1-eleve.width/2;
			var y_min=y1-25;
			var y_max=y1*1+eleve.height*1-25;
			if(app.mouse.x>x_min && app.mouse.x<x_max && app.mouse.y>y_min && app.mouse.y<y_max){
				cursor=true;
			}
		};
		for (var i = app.sociogramme.centres.length - 1; i >= 0; i--) {
			var centre=	app.sociogramme.centres[i];
			var coord=app.sociogrammeGetReduceCoord(centre[0],centre[1]);
			var x=coord.x;
			var y=coord.y;
			if(app.mouse.x>x-15 && app.mouse.x<x+15 && app.mouse.y>y-15 && app.mouse.y<y+15){
				cursor=true;
			}
		};
		if(cursor){
			document.getElementById('classroom-sociogramme-canvas').style.cursor="pointer";
		}else{
			document.getElementById('classroom-sociogramme-canvas').style.cursor="default";
		}
	}
}
app.end_move=function(){
	app.preventTouch=false;
	app.sociogramme.enable_move=false;
	app.sociogramme.selectedEleve=null;	
	app.sociogramme.selectedCentre=-1;
}