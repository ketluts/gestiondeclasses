/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.sociogrammeRelationsViewButtons=function(){
	$('.classroom-sociogramme-views-btn').addClass('btn-default').removeClass('btn-primary');
	$('.btn-sociogramme-'+app.sociogramme.relationsView).removeClass('btn-default').addClass('btn-primary');	
}
app.resetSociogramme=function(confirm){
	if(!confirm){
		app.alert({title:'Réinitialiser votre sociogramme ?',type:'confirm'},function(){app.resetSociogramme(true);});
		return;
	}	
	for (var i = app.sociogramme.eleves.length - 1; i >= 0; i--) {
		var eleve=app.getEleveById(app.sociogramme.eleves[i]);
		eleve.sociogramme[app.sociogramme.vue].x=null;
		eleve.sociogramme[app.sociogramme.vue].y=null;
	};
	app.sociogramme.filtre=false;	
	app.getClasseSociogramme({mode:app.sociogramme.mode});
}
app.sociogrammePictureGet=function(){
	var canvas=document.getElementById('classroom-sociogramme-canvas');
	var dataURL = canvas.toDataURL();
	document.getElementById('classroom-sociogramme-export').href=dataURL;
	document.getElementById('classroom-sociogramme-export').download="sociogramme-"+app.cleanClasseName(app.currentClasse.classe_nom)+"-"+moment().format('DD/MM/YYYY-HH[h]mm')+".png";
}
app.setSociogrammeFiltre=function(value){
	app.sociogramme.filtre=value;
	app.sociogrammeRenderInit();
};
app.sociogrammeSetType=function(type){
	$('.student-sociogramme-form').css('display','none');
	$('#student-sociogramme-form-'+type).css('display','');
	app.userConfig.sociogramme_type=type;
	if(type=="simple"){
		$('.sociogrammeType').removeClass('btn-primary').addClass('btn-default');
	}else{
		$('.sociogrammeType').removeClass('btn-default').addClass('btn-primary');
	}
	if( app.currentView=="classe"){
		app.getClasseSociogramme({mode:app.sociogramme.mode});
	}
}
app.sociogrammeToggleType=function(){
	var type="simple";
	if(app.userConfig.sociogramme_type=="simple"){
		type="avance";
	}
	app.sociogrammeSetType(type);
	app.pushUserConfig();
}
app.sociogrammeZoom=function(zoom){
	app.sociogramme.zoom=Math.max(0.1,app.sociogramme.zoom+zoom);
	app.sociogrammeRenderInit();
}