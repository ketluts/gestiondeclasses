/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

//#######################
//SOCIOGRAMME SAVE
//#######################
app.sociogrammeSave=function(){
	if(!app.checkConnection()){return false;}
	var eleves_positions=[];
	for (var i = app.sociogramme.eleves.length - 1; i >= 0; i--) {
		var eleve=app.getEleveById(app.sociogramme.eleves[i]);

		eleves_positions.push({
			eleve_id:eleve.eleve_id,
			x:eleve.sociogramme[app.sociogramme.vue].x,
			y:eleve.sociogramme[app.sociogramme.vue].y
		});
	};
	var save={};
	save.relations=app.sociogramme.relations;
	save.eleves=eleves_positions;
	save.sociogramme_type=app.userConfig.sociogramme_type;
	save.zoom=app.sociogramme.zoom;
	$.post(app.serveur + "index.php?go=sociogramme&q=add"+ app.connexionParam,{
		data:JSON.stringify(save),
		classe_id:app.currentClasse.classe_id,
		time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
	},function(data){
		$('#sociogramme-toolbar-btn-save').button('reset');
		app.render(data);
		app.renderClasseSociogrammesSelectSave(true);
		app.renderClasseSociogrammeDeleteButton(app.currentClasse.sociogrammes.length-1);
	});	
}
app.renderClasseSociogrammesSelectSave=function(select_last){
	var sociogrammes=app.currentClasse.sociogrammes;
	var lng=sociogrammes.length;
	var html=[];
	if(lng==0){
		document.getElementById('sociogramme-save-bloc').style.display='none';
		html.push('<option value="-1">Aucune sauvegarde</option>');
		
		document.getElementById('classroom-sociogramme-select-btn').innerHTML=html.join('');
		return;
	}
	document.getElementById('sociogramme-save-bloc').style.display='block';
	html.push('<option value="-1">Par popularité</option>');
	var selected='';
	for (var i = 0; i <lng; i++) {
		if(i==lng-1 && select_last){
			selected="selected";
		}
		html.push('<option value="'+i+'" '+selected+'>');
		html.push(moment(parseInt(sociogrammes[i].sociogramme_date)*1000).format('[le] DD/MM/YY [à] HH[h]mm'));
		html.push('</option>');
	};	
	document.getElementById('classroom-sociogramme-select-btn').innerHTML=html.join('');
}
app.sociogrammeLoadSave=function(sociogramme_num){
	app.sociogramme.vue="save";
	if(sociogramme_num==-1){
		app.getClasseSociogramme({mode:"students"});
		app.resetSociogramme(true);
		return;
	}
	app.sociogramme.eleves=app.currentClasse.eleves;
	var save=jsonParse(app.currentClasse.sociogrammes[sociogramme_num].sociogramme_data);
	for (var i = save.eleves.length - 1; i >= 0; i--) {
		var eleve=app.getEleveById(save.eleves[i].eleve_id);
		if(!eleve.sociogramme){
			eleve.sociogramme=[];
		}
		eleve.sociogramme[app.sociogramme.vue]={
			x:save.eleves[i].x,
			y:save.eleves[i].y
		};		
	};
	app.sociogrammeSetType(save.sociogramme_type||'simple');
	app.sociogramme.relations=save.relations;
	app.sociogramme.zoom=save.zoom||app.sociogramme.zoom;
	app.setElevesRang(app.sociogramme.eleves,app.sociogramme.relations);
	app.sociogramme.rangs=app.getRangs(app.sociogramme.eleves);
	app.setElevesPositions(app.sociogramme.eleves,app.sociogramme.rangs);
	app.sociogrammeRenderInit({mode:"students"});
	app.sociogramme.current=sociogramme_num;
	app.renderClasseSociogrammeDeleteButton(sociogramme_num);
}
app.renderClasseSociogrammeDeleteButton=function(sociogramme_num){
	if(sociogramme_num<0){
		document.getElementById('classroom-sociogramme-delete').innerHTML='<button class="btn btn-danger" disabled="disabled"><span class="glyphicon glyphicon-trash"></span> Supprimer</button>';
		return;
	}
	var sociogramme_id=app.currentClasse.sociogrammes[sociogramme_num].sociogramme_id;
	if(sociogramme_id!==null){
		var html=[];
		html.push('<button class="btn btn-danger" onclick="app.delSociogramme('+sociogramme_id+');"><span class="glyphicon glyphicon-trash"></span> Supprimer</button>');
	}
	document.getElementById('classroom-sociogramme-delete').innerHTML=html.join('');
}
app.delSociogramme=function(sociogramme_id,confirm){
	if(!confirm){
		app.alert({title:'Supprimer cette sauvegarde ?',type:'confirm'},function(){app.delSociogramme(sociogramme_id,true);});
		return;
	}
	$.post(app.serveur + "index.php?go=sociogramme&q=delete"+app.currentClasse.classe_id+app.connexionParam,
		{
			classe_id:app.currentClasse.classe_id,
		sociogramme_id:sociogramme_id,
      sessionParams:app.sessionParams
		}, function(data) {
		app.render(data);  
		app.sociogrammeLoadSave(-1);
		app.renderClasseSociogrammesSelectSave();
		app.renderClasseSociogrammeDeleteButton(-1);
	}
	);
}