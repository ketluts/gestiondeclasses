/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.getClasseSociogramme=function(options){
	options.mode=options.mode||app.sociogramme.mode;
	options.relationsView=options.relationsView||app.sociogramme.relationsView;
	app.sociogramme.mode=options.mode;
	app.sociogramme.relationsView=options.relationsView;
	document.getElementById('classroom-sociogramme-synthese').innerHTML="";
	app.sociogrammeRelationsViewButtons();
	if(app.sociogramme.mode=="students"){
		app.sociogramme.vue="classe_"+app.currentClasse.classe_id;
	}
	app.sociogramme.relations=app.getSociogrammeRelations();
	app.sociogramme.eleves=app.currentClasse.eleves;
	if(app.sociogramme.mode=="students"){	
		app.setElevesRang(app.sociogramme.eleves,app.sociogramme.relations);
		app.sociogramme.rangs=app.getRangs(app.sociogramme.eleves);
		app.setElevesPositions(app.sociogramme.eleves,app.sociogramme.rangs);
	}
	else{
		app.setElevesPositionsByGroupes();
	}	
	app.sociogrammeRenderInit();
	if(app.userConfig.sociogramme_type!="simple"){
		app.sociogrammeSyntheseTable();
	}
	document.getElementById('sociogramme').style.display = "block";
	$('#sociogramme').goTo();
}

//#######################
//SOCIOGRAMME RANGS
//#######################
app.setElevesRang=function(eleves,relations){
	for (var i = eleves.length - 1; i >= 0; i--) {
		var eleve=app.getEleveById(eleves[i]);
		eleve.rang=0;
	}
	for (var i = relations.length - 1; i >= 0; i--) {
		var relation=relations[i];
		var eleve=app.getEleveById(relation.relation_to);
		if(app.userConfig.sociogramme_type=="simple"){
			if(relation.relation_type==1){
				eleve.rang+=2;
			}
			if(relation.relation_type==2){
				eleve.rang+=1;
			}
			if(relation.relation_type==3){
				eleve.rang-=1;
			}
		}
		else{
			if(relation.relation_type==4){
				eleve.rang+=1;
			}
			if(relation.relation_type==5){
				eleve.rang-=1;
			}
		}
	}
}
app.getRangs=function(eleves){
	var rangs=[];
	var tmpRangs=[];
	for (var i = eleves.length - 1; i >= 0; i--) {
		var eleve=app.getEleveById(eleves[i]);
		var index=tmpRangs.indexOf(eleve.rang);    
		if(index==-1){
			var rang={value:eleve.rang, eleves:[i]};
			rangs.push(rang);
			tmpRangs.push(eleve.rang);
		}else{
			rangs[index].eleves.push(i);
		}
	}
	rangs.sort(function(a,b){
		if(a.value>b.value)
			return -1;
		return 1;
	});
	app.sociogramme.step=(app.sociogramme.cote-120)/(2*rangs.length);
	return rangs;
}
app.getSociogrammeRelations=function(){
	if(app.sociogramme.relationsView=="elevesView"){
		var relations=clone(app.relations);
		var tempRelations=[];
		for (var i = app.relations.length - 1; i >= 0; i--) {
			var relation=app.relations[i];
			if(app.currentClasse.eleves.indexOf(relation.relation_from)<0 || app.currentClasse.eleves.indexOf(relation.relation_to)<0){continue;}
			if(relation.relation_user.indexOf('eleve_')>=0){
				tempRelations.push(relation);
			}
		};
	}
	else{
		//userView
		var tempRelations=[];
		for (var i = app.relations.length - 1; i >= 0; i--) {
			var relation=app.relations[i];
			if(app.currentClasse.eleves.indexOf(relation.relation_from)<0 || app.currentClasse.eleves.indexOf(relation.relation_to)<0){continue;}
			if(relation.relation_user==app.userConfig.userID){
				tempRelations.push(relation);
			}
		};
	}
	relations=tempRelations;
	tempRelations=[];
	for (var i in relations) {
		var relation=relations[i];
		tempRelations.push({
			relation_from:relation.relation_from,
			relation_to:relation.relation_to,
			relation_type:relation.relation_type
		});
	}
	relations=tempRelations;
	return relations;
}
app.getRelation=function(from,to,relations){
	for (var i = relations.length - 1; i >= 0; i--) {
		var relation=relations[i];
		if(relation.relation_from==from && relation.relation_to==to)
			return [relation,i];
	}
	return false;
}
app.getSociogrammeRelationValue=function(relation_type){

if(relation_type==1){
  return 2;
}
if(relation_type==2){
  return 1;
}
if(relation_type==3){
  return -2;
}
if(relation_type==4){
  return 1;
}
if(relation_type==5){
  return -2;
}
return 0;

}