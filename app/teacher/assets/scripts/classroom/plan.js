/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.zIndexUp=function(id){
  if(app.zindexBlock){return;}
  document.getElementById(id).style.zIndex=app.zindex;
  app.zindex++;
}
app.dragRenderEleve=function(div_id,eleve_id){
  if(document.getElementById(div_id).style.zIndex<app.zindex-1){
    app.zIndexUp(div_id);
    return;
  }
  app.go('student/'+eleve_id);
}
app.planDeClasse=function(){
  var html = [];
  var yMax=0;
  document.getElementById('classroom-students').style.backgroundImage="url('assets/img/grid.png')";
  document.getElementById('classroom-students-toolbar').style.display="none"; 
  if(app.currentClasse.pdcModif){
    document.getElementById('classroom-plan-btnSave').style.display="block"; 
  }
  $("#classe-plan-btn").addClass('btn-primary').removeClass('btn-default');  
  $("#classe-liste-btn").removeClass('btn-primary').addClass('btn-default');  
  app.currentClasseView="plan";
  for (var i = app.currentClasse.eleves.length - 1; i >= 0; i--) {
   var eleve= app.getEleveById(app.currentClasse.eleves[i]);
   if(!eleve){continue;}
   var color=null;
   var niveau_height=0;

   if(app.userConfig.coloration==1 || app.userConfig.coloration==3){
    color=app.colorByMoyenne(eleve.jaugeNotes);
    if(eleve.jaugeNotes){
     niveau_height=(eleve.jaugeNotes/20)*46;
   }
 } 
   //if(!eleve.isChecked){//TODO
    // if(app.userConfig.randomEleves.indexOf(eleve.eleve_id)==-1){
    //   if(!app.randomEleve.eleves[i]){app.randomEleve.eleves[i]=[];}
    //   app.randomEleve.eleves[i].push(eleve.eleve_id);
    //   btn_class="btn-primary";
    // }else{
    //   btn_class="btn-default";
    // }
    var x=0;
    var y=0;
    if(eleve.pdcX){
      x=eleve.pdcX+"px";
    }else{
     x=((i%6)*100/6)+"%";
   }
   if(eleve.pdcY){
    y=eleve.pdcY+"px";
    if(eleve.pdcY> yMax){yMax=eleve.pdcY;}
  }else{
    y=(Math.floor(i/6)*100)+"px";
  }
  html.push('<div style="top:'+y+'; left:'+x+';" class="text-center classroom-student onPlan" id="eleve_drag_' + eleve.eleve_id + '" onmousedown="app.zindexBlock=true;" onmouseup="app.zindexBlock=false;" onmouseover="app.zIndexUp(\'eleve_drag_' + eleve.eleve_id + '\');" onclick="app.zIndexUp(\'eleve_drag_' + eleve.eleve_id + '\');">\
    <div id="eleve_' + eleve.eleve_id + '"  onclick="app.dragRenderEleve(\'eleve_drag_' + eleve.eleve_id + '\', ' + eleve.eleve_id + ');" title="Voir la fiche de cet élève" class="btn classroom-student-name">');
  html.push(app.renderClasseEleveName(eleve.eleve_id));
  html.push('<div class="classroom-student-jauge onPlan">');
  html.push('<div class="classroom-student-jauge-notes" style="background-color:'+color+';height:'+niveau_height+'px;">');
  
  html.push('</div>');
  html.push('</div>');
  html.push('</div>');

  html.push('<div class="btn-group eleve-events-btn">');
  for (var k = 0, llng = app.userConfig.eventsWhiteList.length; k < llng; k++) {
    var type_event=app.userConfig.eventsWhiteList[k];
    var legende=app.legends[type_event]||"";
    html.push('<span class="btn btn-default btn-sm btn-eleve-event" id="event_' + type_event + '_' + eleve.eleve_id + '" title="'+legende+'" onclick="app.agendaAddQuickEvent(\''+ type_event + '\',\'' + eleve.eleve_id + '\',true);">');
    html.push('<span class="classroom-student-badge" id="badge_' + type_event + '_' + eleve.eleve_id + '"></span>');
    html.push('<span class="glyphicon ' + type_event + '"></span>');
    html.push('</span>');
  }
  html.push('</div>');
  html.push('</div>');
};
$('#classroom-students').css('overflow','auto')
.css('height',(yMax+150)+"px")
.html(html.join(''));

if(yMax==0){
  app.alert({title:"Glissez et déposez pour modifier le plan de classe."});
}
if(app.userConfig.coloration==1 || app.userConfig.coloration==3){
 $('.classroom-student-jauge').css('display','block');
} else{
  $('.classroom-student-jauge').css('display','none');
}
$('.classroom-student.onPlan').draggable({
  stop: function( event, ui ) {
    var eleve_id=ui.helper.attr('id').replace("eleve_drag_","");
    var eleve=app.getEleveById(eleve_id);
    eleve.pdcX=ui.position.left;
    eleve.pdcY=ui.position.top;
    document.getElementById('classroom-plan-btnSave').style.display="block"; 
    app.currentClasse.pdcModif=true;
   // app.setPlanHeight();
  }
  ,snap:true
  ,snapMode:"both"
  ,grid : [10 , 10]
  ,snapTolerance: 5
});
app.classroomRandomInit();
app.classeEventsRender();
}
app.planSave=function(){
 if (!app.checkConnection()) {
  return;
}
var data=[];
var x=0;
var y=0;
for (var i = app.currentClasse.eleves.length - 1; i >= 0; i--) {
 var eleve= app.getEleveById(app.currentClasse.eleves[i]);
 if(eleve.pdcX){
  x=eleve.pdcX;
}else{
 x=null;
}
if(eleve.pdcY){
  y=eleve.pdcY;
}else{
  y=null;
}
data.push({
  id:eleve.eleve_id,
  x:Math.floor(parseInt(x)),
  y:Math.floor(parseInt(y))
});
}
$.post(app.serveur + "index.php?go=plans&q=add"+app.connexionParam,{
  classe_id:app.currentClasse.classe_id,
  data:JSON.stringify(data),
sessionParams:app.sessionParams
}, function(data) {
  app.render(data); 
  document.getElementById('classroom-plan-btnSave').style.display="none"; 
  app.currentClasse.pdcModif=false;
  $('#classroom-plan-btnSave').button('reset');
}
);
}
app.setPlanHeight=function(){
  var yMax=0;
  for (var i = app.currentClasse.eleves.length - 1; i >= 0; i--) {
   var eleve= app.currentClasse.eleves[i];
   if(eleve.pdcY && eleve.pdcY> yMax){yMax=eleve.pdcY;}
 }
 document.getElementById('classroom-students').style.height=(yMax+150)+"px";
}