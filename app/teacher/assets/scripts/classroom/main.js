/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.getClasseById=function(classe_id) {
  var classes=app.classes;
  for (var i = 0, lng = classes.length; i < lng; i++) {
    if (classes[i].classe_id == classe_id) {
      classes[i].num=i;     
      return classes[i];
    }
  }
  return false;
};
app.classeRender=function(classe_id) {  
 var classe=app.getClasseById(classe_id)
 if(!classe){
  app.go('home');
  return;
}
   //Init view
   app.viewClear();
   app.currentView="classe";
   app.currentEleve=null;   
  app.currentClasse=classe;
   $(".template_classe").css("display","block");
   document.getElementById('classroom-students').innerHTML=app.renderLoader();
  //Sociogramme
  document.getElementById('sociogramme').style.display = "none";
  $('#sociogramme-toolbar-btn-save').button('reset'); 
  app.sociogrammeRelationsViewButtons();

  document.getElementById('generateurGroupes').style.display = "none";
  document.getElementById('classroom-groupes-liste').innerHTML = "";
  document.getElementById('new_classe_nom_form').style.display = "none";
  document.getElementById('classe-export').style.display = "none";
  document.getElementById('classe-config').style.display = "none";
  document.getElementById('controles').style.display = "none";
  document.getElementById('classroom-addStudents').style.display = "none";
  document.getElementById('classroom-addStudents-liste').innerHTML = "";
  document.getElementById('classroom-plan-btnSave').style.display="none";   
  document.getElementById('classe-intelligences').style.display="none";  
  document.getElementById('classe-delete').style.display="none";     
  document.getElementById("classroom-random").innerHTML = "?"; 
  //$(".classroom-students-groupe-toolbar").css("display","none"); 
  $('#classroom-addStudents-btn').button('reset');
  document.getElementById('import-tableur').value="";
  $('#import-tableur-btn').button('reset');  
  //Agenda
  document.getElementById('classe_event_edition').style.display = "none";
  app.zindex=10;
  //Groupes
  document.getElementById('classe-groupe-save-text').value="";
  app.currentClasse.currentGroupes=[];
  app.currentClasse.currentGroupesNb=0;
  app.currentClasse.currentGroupesNum=false;  
  app.classroomGroupsDeleteBtnRender();
  app.classroomGroupsToolbarRender();
  app.classroomGroupsFormRender();
  app.renderClasseGroupesSelect();
  app.classroomGroupsSelectSaveRender(); 

  //Init render
  document.getElementById('new_classe_nom').value= classe.classe_nom;
  document.getElementById('titre').innerHTML = app.cleanClasseName(classe.classe_nom);
  app.setElevesJauges(app.currentClasse.eleves);
  if(classe.pdc){
   for (var i = classe.pdc.length - 1; i >= 0; i--) {
    var position=classe.pdc[i];
    var eleve=app.getEleveById(position['id']);
    if(!eleve){continue;}
    eleve.pdcX=position['x'];
    eleve.pdcY=position['y'];
  };
}
  //Render   
  if(classe.classe_destinataires && classe.classe_destinataires.indexOf(app.userConfig.userID)>=0){
    document.getElementById('messagerieEnable').value=true;
  }
  else{
    document.getElementById('messagerieEnable').value=false;
  }
  if(classe.classe_intelligences && classe.classe_intelligences=="true"){
   document.getElementById('intelligencesTest').value=true;
 }
 else{
   document.getElementById('intelligencesTest').value=false;
 }
 if(classe.classe_id<0){
  $('.classroomOnly').css('display','none');
}  else{
  $('.classroomOnly').css('display','');
}
app.classeTimeOut=setTimeout(function(){
  if(app.currentClasseView=="liste"){
    app.classeElevesRender();
  }
  else{
    app.planDeClasse();
  }  

  app.classeCalendarRender();
  app.agendaGoToday();
  app.agendaRender();
  app.renderLegendsUsers('classroom-usersLegend');
  app.getGroupes(0);
},500);
app.renderClasseSociogrammesSelectSave();
app.renderClasseSociogrammeDeleteButton(-1);
 
app.renderClassroomEventsInit();
app.renderClassroomEvents();
app.classeEventsGraphRender();
document.getElementById('classe_memo').value=app.getMemo('classroom',classe.classe_id)||null
document.getElementById('classe_memo').style.height = 'auto';
    document.getElementById('classe_memo').style.height = document.getElementById('classe_memo').scrollHeight+'px';;
};
app.cleanClasseName=function(classe_name){
  return classe_name.replace(/~/g,'');
}

app.classroomSociogrammeSelectChange=function(n){
 if(n==-1){
  app.renderClasseSociogrammeDeletediv(-1);
  app.sociogrammeLoadSave(n);
  return;
}
 app.sociogrammeLoadSave(n);
 app.renderClasseSociogrammeDeletediv(n);
}
app.classroomSetMemo=function(value){ 
  app.setMemo('classroom',app.currentClasse.classe_id,value);
}