/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.setElevesJauges=function(eleves){
	//COMPETENCES
	app.itemsStudentsProgressionsBuild(eleves);
	var valuesByEleve=[];
	for (var i = app.items.length - 1; i >= 0; i--) {
		var item=app.items[i];
		var filter_id=null;
		if(app.currentClasse){			
		 filter_id=app.userConfig.jaugesFilters[app.currentClasse.classe_id];	
	}		
		if(!app.itemsIsVisible(item,filter_id)){
			continue;
		}
		for (var j = eleves.length - 1; j >= 0; j--) {
			var eleve=app.getEleveById(eleves[j]);
			if(eleve.progression && eleve.progression[item.item_id]){
				valuesByEleve[eleve.eleve_id]=valuesByEleve[eleve.eleve_id]||[];
				var value=eleve.progression[item.item_id].value;
				valuesByEleve[eleve.eleve_id].push(Math.min(Math.max(value*20/item.item_value_max,0.5),100));
			}
		}
	}
	for (var i =eleves.length - 1; i >= 0; i--) {		
		var eleve=app.getEleveById(eleves[i]);
		eleve.jaugeAcquis=-1;
		if(valuesByEleve[eleve.eleve_id]){
			var n=valuesByEleve[eleve.eleve_id].length;
			eleve.jaugeAcquis=Math.round((valuesByEleve[eleve.eleve_id].reduce(app.add,0)/n)*100)/100;
		}
	};
	

	//NOTES
	var notes=[];
	for (var i =0,lng= app.notes.length; i <lng; i++) {
		var note=app.notes[i];
		var controle=app.getTestById(note.note_controle);
		if(!controle){
			continue;
		}
		if(controle.controle_user!=app.userConfig.userID || eleves.indexOf(note.note_eleve)<0 || isNaN(note.note_value)){
			continue;
		}
		if(!notes[note.note_eleve]){
			notes[note.note_eleve]=[];
		}
		notes[note.note_eleve].push(note);
	}
	for (var i =eleves.length - 1; i >= 0; i--) {		
		var eleve=app.getEleveById(eleves[i]);
		if(!eleve){
			continue;
		}
		if(notes[eleve.eleve_id]){
			var moyenne=app.moyenne(notes[eleve.eleve_id]);			
			eleve.jaugeNotes=moyenne;
		}
		else{
			eleve.jaugeNotes=-1;
		}
	};
	return eleves;
}

