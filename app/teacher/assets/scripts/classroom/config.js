/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.classroomConfigInit=function(){
$('#classe-config').css('display','block').goTo();
var value=app.userConfig.jaugesFilters[app.currentClasse.classe_id]||-1;
document.getElementById('classroomJaugesFiltersList').value=value;

}
app.classroomFilterSet=function(value){
app.userConfig.jaugesFilters[app.currentClasse.classe_id]=value;
app.pushUserConfig();
app.setElevesJauges(app.currentClasse.eleves);
if(app.currentClasseView=="liste"){
    app.classeElevesRender();
  }
  else{
    app.planDeClasse();
  }  
}