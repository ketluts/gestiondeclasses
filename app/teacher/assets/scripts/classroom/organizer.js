/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.classeCalendarRender=function(){
 document.getElementById('classe_agenda').style.display = "flex";
 var classe=app.currentClasse;
 $('#classe_agenda').fullCalendar('destroy');
 $('#classe_agenda').fullCalendar({
   lang: 'fr',
   views: {
    month: { 
      titleFormat: 'MM/YYYY'
    },
    day: { 
      titleFormat: 'D/MM/YYYY'
    }
  },
  timezone:'local',
  editable: true,
  selectable:true,
  selectHelper:true,
  nowIndicator:true,
  unselectAuto:true,
  unselectCancel:"#classe_event_edition",
  weekends:app.userConfig.agendaWeekEnds,
//minTime: '08:00',
//maxTime: '23:00',
defaultView: app.userConfig.agendaCurrentView,
allDaySlot: true,
firstDay: 1,
minTime:app.userConfig.organizerMinTime,
maxTime:app.userConfig.organizerMaxTime,
height:"auto",
eventOrder:'event_order',  
timeFormat: 'HH:mm',
slotLabelFormat:"HH:mm",
slotDuration:app.userConfig.organizerSlotDuration,
eventBackgroundColor:"#F5F5F5",
eventBorderColor:"#E3E3E3",
defaultTimedEventDuration:"01:00:00",
eventTextColor:"black",
scrollTime:"07:30:00",
slotEventOverlap:false,
contentHeight:550,
views: {
  basicThreeDay: {
    type: 'basic',
    duration: { days: 3 },
    buttonText: '3 jours'
  }
},
header: {
 left: '',
 center: '',
 right: ''
},
eventClick:function( event, jsEvent, view ) { 
  app.agendaOneEventDetailsToggle(event.event_id);
},
select:function(start, end, jsEvent, view,resource){
  if(document.getElementById('classe_event_edition').style.display=='none'){
    app.agendaEditionInit();
  }
  $('.agenda-next-day').removeClass('btn-primary').addClass('btn-default');
  app.agendaNewEventAllDay=false;
  $('#events').goTo();
  app.agendaNewEventStart=start;
  app.agendaNewEventEnd=end;
  if(!start.hasTime()){
   app.agendaNewEventAllDay=true;
 }
 var html="";
 if(view.name=="agendaDay"){
  if(app.agendaNewEventAllDay){
   html=start.format("ddd DD MMM");
 }
 else{
  html="de "+start.format("HH[h]mm")+" à "+end.format("HH[h]mm");
}
}
else{
  if( end.unix()-start.unix()<=86400){
    html=start.format("ddd DD MMM");
  }
  else{
    html="du "+start.format("DD MMM YYYY")+" au "+end.format("DD MMM YYYY");
  }
}
$('#classe_event_date').html(html).css('display','inline-block').removeClass('btn-default').addClass('btn-primary');
},
viewRender:function( view, element ){
  app.agendaNewEventStart=null;
  app.agendaNewEventEnd=null;
  $(".classe-agenda-view").removeClass('btn-primary').addClass('btn-default');    
  $("#classe-agenda-view-"+view.name).addClass('btn-primary');  
  if(app.userConfig.agendaCurrentView!=view.name){    
   app.userConfig.agendaCurrentView=view.name;
   
   app.pushUserConfig(); 
 }
 document.getElementById('classe-agenda-semaine').innerHTML=app.getWeekName(  $('#classe_agenda').fullCalendar( 'getDate' ).unix()*1000);
 $('#classe_agenda').fullCalendar( 'unselect' );

 var format="";
 if(view.name=="month"){
  format="MMMM YYYY";
  document.getElementById('classroom-agenda-date').innerHTML= $('#classe_agenda').fullCalendar( 'getDate' ).format(format);
}
else if(view.name=="basicThreeDay"){
  format="DD MMM YYYY";
  document.getElementById('classroom-agenda-date').innerHTML= $('#classe_agenda').fullCalendar( 'getDate' ).add({day:1}).format(format);
}
else{
  format="DD MMM YYYY";
  document.getElementById('classroom-agenda-date').innerHTML= $('#classe_agenda').fullCalendar( 'getDate' ).format(format);
}
app.agendaEventDetailsToggle(app.userConfig.agendaEventDetails,false);
},
eventMouseover:function( event, jsEvent, view ) {
 this.style.zIndex=5;
},
eventMouseout:function( event, jsEvent, view ) {
  this.style.zIndex=1;
},
eventDrop:function( event, jsEvent, ui, view ) {
  event.event_type=event.event_type||null;
  var oEvent={
    event_id:event.event_id,
    event_start:event.start.unix()
  }

  if(!event.start.hasTime()){
    oEvent.event_allDay=true;
  }
  else{
    oEvent.event_allDay=false;
  }

  if(!event.end || !event.end.hasTime()){
    oEvent.event_end=event.start.unix();
  }else{
    oEvent.event_end=event.end.unix();     
  }

  app.currentClasseCalendarDate=$('#classe_agenda').fullCalendar( 'getDate' );
  if(event.event_type=="agenda"){
    app.agendaEventUpdate(oEvent,false);
  }
  else if(event.event_type=="event"){
  }
},
eventResize:function( event, jsEvent, ui, view ) {
  event.event_type=event.event_type||null;
  var oEvent={
    event_id:event.event_id,
    event_start:event.start.unix()
  }
  if(!event.start.hasTime()){
    oEvent.event_allDay=true;
  }
  else{
    oEvent.event_allDay=false;
  }
  if(event.end){
    oEvent.event_end=event.end.unix();
  }
  app.currentClasseCalendarDate=$('#classe_agenda').fullCalendar( 'getDate' );
  if(event.event_type=="agenda"){
    app.agendaEventUpdate(oEvent,false);
  }
},
eventAfterAllRender:function(view){

  app.classeEventsRender(); 
}
}); 
};
app.classeEventsRender=function() {
  var classe=app.currentClasse;
  for (var i = 0, lng = classe.eleves.length; i < lng; i++) {
   var eleve = classe.eleves[i];
   for (var k = 0, llng = app.eventsAvailable.length; k < llng; k++) {
    if(app.userConfig.eventsWhiteList.indexOf(app.eventsAvailable[k].type)<0){
      continue;
    }
    var oEvent=app.eventsAvailable[k];
    var count=app.countEvents(eleve,oEvent.type,true);
    var elem=document.getElementById('badge_' + oEvent.type + '_' + eleve + '');
    if(elem){
     if(count>0){
      elem.innerHTML=count;
      elem.style.display="inline-block";
    }else{
     elem.style.display="none";
   }    
 } 
}
}
};



app.classeDiagrammeBarre=function(){
 var events=app.agenda;
 var categories=[];    
 var inClassroomData=[];
 var atHomeData=[];

// if(app.userConfig.ppView){
//     events=app.orderBy(events,"user_matiere","ASC");
//   }
//   else{
//      events=app.orderBy(events,'event_start','DESC');
//   }
events=app.orderBy(events,'event_start','DESC');
for (var i =0, lng= events.length; i <lng; i++) {
 var oEvent=events[i];
 if ((oEvent.event_user !==  app.userConfig.userID && !app.userConfig.ppView) 
  || (oEvent.event_type=="classe" && oEvent.event_type_id!=app.currentClasse.classe_id)
  || (oEvent.event_type=="eleve")
  || (oEvent.event_icon!="glyphicon-home" && oEvent.event_icon!="glyphicon-blackboard")
  ) { 
  continue;
}
var timeSteps=oEvent.event_title.split("*").length-1;
var discipline="";
var atHome=false;
var sessionDuration=(oEvent.event_end-oEvent.event_start)/3600;
if(!sessionDuration || oEvent.event_allDay){
  sessionDuration=1;
}
if(app.userConfig.ppView){  
  var event_user=app.getUserById(oEvent.event_user);
  if(event_user.user_matiere){
    discipline=event_user.user_matiere+" - ";
  }
  else{
    discipline=event_user.user_pseudo+" - ";
  }
}
if(oEvent.event_icon=="glyphicon-home"){
 atHome=true;
}
var titles=oEvent.event_title.split("/").map(function(value){
  if(value){
    return discipline+value.trim();
  }
}
);
var stepsNb=1;
var titlesNb=titles.length;
if(timeSteps<titlesNb){
  stepsNb=titlesNb;
}
else{
  stepsNb=timeSteps;
}
for (var j = stepsNb - 1; j >= 0; j--) {
  var title=titles[j];
  if(!title){continue;}
  var stepDuration=title.split("*").length-1||1; 
  var subTitles=title.split(":").map(function(value){
    if(value){
      return value.trim();
    }
  }
  );
  var mainTitle=subTitles[0];
  for (var k =0,llng=subTitles.length ; k<llng; k++) {
    var subTitle=subTitles[k];
    if(k!=0){
      subTitle=mainTitle+" - "+subTitle;
    }
    var index=categories.indexOf(app.organizerEventTitleRender(subTitle));
    if(index<0){
      categories.push(app.organizerEventTitleRender(subTitle));
      if(atHome){
        atHomeData.push(Math.round(sessionDuration*(stepDuration/stepsNb)*100)/100);
        inClassroomData.push(0);
      }else{
        inClassroomData.push(Math.round(sessionDuration*(stepDuration/stepsNb)*100)/100);
        atHomeData.push(0);
      }
    }
    else{
      if(atHome){
        atHomeData[index]=atHomeData[index]*1+Math.round(sessionDuration*(stepDuration/stepsNb)*100)/100;
      }else{
        inClassroomData[index]=inClassroomData[index]*1+Math.round(sessionDuration*(stepDuration/stepsNb)*100)/100;
      }
    }
  } 
}
}

$('#classe_diagramme_barre').highcharts({
  chart: {
    type: 'bar'
  },
  title: {
   text: 'Séances'
 },
 xAxis: {
  categories: categories,
  title: {
    text: null
  }
},
yAxis: {
  min: 0,
  title: {
    text: 'Nombre de séances',
    align: 'high'
  },
  labels: {
    overflow: 'justify'
  }
},
tooltip: {
  valueSuffix: ''
},
plotOptions: {
  bar: {
    dataLabels: {
      enabled: true
    }
  }
},
legend: {
  layout: 'vertical',
  align: 'right',
  verticalAlign: 'top',
  x: -40,
  y: 10,
  floating: true,
  borderWidth: 1,
  backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
  shadow: true
},
credits: {
  enabled: false
},
series: [
{
  name:'En classe',
  data:inClassroomData
},
{
  name:'À la maison',
  data:atHomeData
}
]
});
}