/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.renderEleveInfos=function(){
	document.getElementById('eleve_infos_save').style.display = "none";	
	var eleve=app.currentEleve;
	eleve.delete_eleve_picture=false;
	//Statut
	if(eleve.eleve_statut=="delegue"){
		document.getElementById('eleveDelegueSelect').value=true;
		document.getElementById('eleve-delegue').innerHTML="délégué"+app.feminize(eleve.eleve_genre,'e');
	}else{
		document.getElementById('eleveDelegueSelect').value=false;
		document.getElementById('eleve-delegue').innerHTML="";
	}
	//Age
	eleve.eleve_birthday=eleve.eleve_birthday||null;
	document.getElementById("eleve_birthday").value=eleve.eleve_birthday;
	var eleve_age=app.renderYearsOld(eleve.eleve_birthday);
	var html="";
	if(eleve.eleve_statut=="delegue"){
		html=",";
	}
	if(eleve_age){
		document.getElementById("eleve-age").innerHTML="<span title='"+eleve.eleve_birthday+"'>"+eleve_age+" ans"+html+"</span>";
	}else{
		document.getElementById("eleve-age").innerHTML="";
	}
	//Photo
	document.getElementById('eleve-picture-input').value="";
	app.studentPictureSet();

	//Jauges
	if(eleve.jaugeNotes===undefined || eleve.jaugeAcquis===undefined){
		app.setElevesJauges([eleve.eleve_id]);
	}
	if(eleve.jaugeNotes){
		document.getElementById('eleve-jauge-notes').style.width=(eleve.jaugeNotes/20)*100+'%';
		document.getElementById('eleve-jauge-notes').style.backgroundColor=app.colorByMoyenne(eleve.jaugeNotes);	
	}	
	if(eleve.jaugeAcquis){
		document.getElementById('eleve-jauge-acquis').style.width=(eleve.jaugeAcquis/20)*100+'%';
		document.getElementById('eleve-jauge-acquis').style.backgroundColor=app.colorByMoyenne(eleve.jaugeAcquis);	
	}	
	//Token
	document.getElementById('eleve_token').innerHTML =eleve.eleve_token;
	document.getElementById('eleve_info_token').value =eleve.eleve_token;
	document.getElementById('eleve_info_token').placeholder =eleve.eleve_token;
	//NOM et Prénom
	document.getElementById('eleve-nom').innerHTML ="<h3>"+ app.renderEleveNom(eleve)+"</h3>";
	document.getElementById('eleve_nom').value=eleve.eleve_nom||"";
	document.getElementById('eleve_prenom').value=eleve.eleve_prenom||"";
	//Genre
	document.getElementById('eleveGenreSelect').value=eleve.eleve_genre||null;
	//Memo
	document.getElementById('eleve_memo').value=app.getMemo('student',eleve.eleve_id)||null;
	document.getElementById('eleve_memo').style.height = 'auto';
    document.getElementById('eleve_memo').style.height = document.getElementById('eleve_memo').scrollHeight+'px';
}
app.renderEleveInfosEdit=function(){
	app.renderEleveInfos();
	document.getElementById('eleve_infos').style.display = "block";
	$('#eleve_infos').goTo();
	$('#eleve_infos_save_btn').button('reset');
}
app.checkBirthdayPattern=function(birthday){
	if(birthday.trim()==""){return true;}
	var re=/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/;
	if(!birthday.match(re)){
		app.alert({title:"La date de naissance doit être au format JJ/MM/AAAA."});
		return false;
	}	
	return true;
}
app.studentSetCycle=function(eleve_id,value){
	if (!app.checkConnection()) {return;}	
	//On met à jour les données
	$.post(app.serveur + "index.php?go=eleves&q=updateStudents"+ app.connexionParam,
	{
		students:JSON.stringify([eleve_id]),
		student_cycle:value,
sessionParams:app.sessionParams
	},
	function(data) {
		app.render(data); 
		if(app.currentView=="eleve"){
			app.setElevesJauges([eleve_id]);  
			app.renderEleveInfos();
		}else{
			app.setElevesJauges(app.currentClasse.eleves);
			if(app.currentClasseView=="liste"){
				app.classeElevesRender();
			}
			else{
				app.planDeClasse();
			}  
		}
	});
}
app.studentSetMemo=function(value){
	var eleve=app.currentEleve;
	app.setMemo('student',eleve.eleve_id,value);
}

app.renderYearsOld=function(birthday){
  if(!birthday){return 0;}
  var today = new Date();
  var tab= birthday.split('/');
  var birthDate = new Date(tab[2], tab[1]-1,tab[0]);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
}
app.eleveInfosUpdate=function(){
	if (!app.checkConnection()) {
		$('#eleve_infos_save_btn').button('reset');
		return;}
	//Statut
	var new_statut="eleve";
	if(document.getElementById('eleveDelegueSelect').value=="true"){
		new_statut="delegue";
	}
	app.currentEleve.eleve_statut=new_statut;
	//NOM et Prenom
	var eleve_nom=document.getElementById('eleve_nom').value;
	var eleve_prenom=document.getElementById('eleve_prenom').value;
	if(eleve_nom=="" && eleve_prenom==""){
		app.alert({title:"Le NOM et le Prénom ne peuvent pas être vides."});
		$('#eleve_infos_save_btn').button('reset');
		return;
	}
	app.currentEleve.eleve_nom=eleve_nom;
	app.currentEleve.eleve_prenom=eleve_prenom;
	//Date de naissance
	var birthday=document.getElementById('eleve_birthday').value;
	if(!app.checkBirthdayPattern(birthday)){
		$('#eleve_infos_save_btn').button('reset');
		return;}
		app.currentEleve.eleve_birthday=birthday;
	//Genre
	var genre=document.getElementById('eleveGenreSelect').value;
	app.currentEleve.eleve_genre=genre;
	//Token
	var token=document.getElementById('eleve_info_token').value;
	app.currentEleve.eleve_token=token;
	//On met à jour les données
	$.post(app.serveur + "index.php?go=eleves&q=update&eleve_id="+app.currentEleve.eleve_id + app.connexionParam,
	{
		eleve_statut:new_statut,
		eleve_nom:eleve_nom,
		eleve_prenom:eleve_prenom,
		eleve_birthday:birthday,
		eleve_genre:genre,
		eleve_token:token,
sessionParams:app.sessionParams
	},
	function(data) {
		app.render(data);   
		app.renderEleveInfosEdit(); 
	});
	app.studentPictureSave();
}
app.makeEleveToken=function()
{
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	for( var i=0; i < 6; i++ )
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	app.show('eleve_infos_save');
	document.getElementById('eleve_info_token').value=text;
}
 /*---------------
-----------------
---Sociogramme----
-----------------
---------------*/
app.renderRelationsForm=function(){
	var relations= app.relations;
	var eleve_relations=[];
	var eleve_relations_ids=[];
	for (var i = relations.length - 1; i >= 0; i--) {
		var relation=relations[i];
		if(relation.relation_from==app.currentEleve.eleve_id && relation.relation_user==app.userConfig.userID){
			eleve_relations.push(relation);
			eleve_relations_ids.push(relation.relation_to);
		}
	}
	var eleves=[];	
	for (var i = 0; i < app.eleves.length; i++) {
		var eleve=app.eleves[i];
		if(app.renderEleveNom(app.currentEleve)==app.renderEleveNom(eleve)){continue;}
		if((app.currentClasse!==null && app.currentClasse.eleves.indexOf(eleve.eleve_id)>=0) || eleve_relations_ids.indexOf(eleve.eleve_id)>=0 || app.currentClasse==null){
			eleves.push(eleve);
		}
	}
	var template = $('#template-student-relations-form').html();
	var rendered = Mustache.render(template, {eleves:app.orderBy(eleves,'eleve_nom','ASC'),
		"renderNom":function(){
			return app.renderEleveNom(this);
		}
	});
	$('.student-relations-select').html(rendered);
	for (var i = 0; i < eleve_relations.length; i++) {
		var relation=eleve_relations[i];
		$('#relation_'+relation.relation_type).val(relation.relation_to);
	}
};
app.saveRelations=function(){
	if (!app.checkConnection()) {
		return;
	}	
	$.post(app.serveur + "index.php?go=sociogramme&q=updateRelations"+ app.connexionParam, {
		eleve_id:app.currentEleve.eleve_id,
		relation_1:document.getElementById('relation_1').value,
		relation_2:document.getElementById('relation_2').value,
		relation_3:document.getElementById('relation_3').value,
		relation_4:document.getElementById('relation_4').value,
		relation_5:document.getElementById('relation_5').value,
		relation_6:document.getElementById('relation_6').value,
		relation_7:document.getElementById('relation_7').value,
sessionParams:app.sessionParams
	},function(data) {
		app.render(data);    
	});
}
/*---------------
-----------------
---Intelligences multiples----
-----------------
---------------*/
app.studentIntelligencesRender=function(){

	var categories=[];
	var data=[];
	var results=[];
	if(app.currentEleve.eleve_intelligences){
		results=jsonParse(app.currentEleve.eleve_intelligences);
	}
	for (var i =0; i<8; i++) {		
		categories.push(app.intelligences[i].name);
		var result=results[i];
		if(!result){
			result=0;
		}
		data.push(result);
	}
	$('#eleve-intelligences').highcharts({
		chart: {
			polar: true,
			type: 'line'
		},
		title: {
			text: '',
			style:"display:none"
		},
		pane: {
			size: '80%'
		},
		xAxis: {
			categories: categories,
			tickmarkPlacement: 'on',
			lineWidth: 0
		},
		yAxis: {
			gridLineInterpolation: 'polygon',
			lineWidth: 0,
			min: 0
		},
		series: [{
			name: 'Profil',
			data: data,
			pointPlacement: 'on'
		}
		]
	});
}
