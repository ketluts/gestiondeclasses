/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.renderEleve=function(eleve_id) {	
	var eleve=app.getEleveById(eleve_id);
	if(!eleve){
		app.go('home');
		return;
	}
	//Initialisation de l'affichage
	app.viewClear();
	document.getElementById('eleve-jauge-notes').style.width='0%';
	app.uiShow([],['template_eleve']);
	app.uiHide([
		'eleve_infos',
		'student-picture-edit',
		'eleve_etoiles',
		'eleve_notes',
		'note_eleve_edit',
		'eleve_event_edition'
		]);	
	app.uiClean([
		'eleve-nav-right',
		'eleve_calendar_events_compteur',
		'eleve_calendar_legende'
		]);
	var classe={};
	app.currentView="eleve";
	app.eleveEventsListCurrent="";
	app.currentEleve = eleve;
	if(app.currentClasse){	
		classe=app.currentClasse;		
		document.getElementById('eleve-nav-classe').innerHTML = '<div class="btn btn-default " onclick="app.go(\'classroom/'+app.currentClasse.classe_id+'\');"> <span class="glyphicon glyphicon-chevron-left"></span> '+(classe.classe_nom||"")+'</div> ';
		app.renderEleveNavRight();
		var n=app.classes.length;
		var classe_num=app.currentClasse.num;
		if(classe_num==n-1 || classe_num==n-2){
			$('#eleve-remove-btn').css('display','none');
		}else{
			$('#eleve-remove-btn').css('display','block');
		}
	}


	html=[];
	html.push('<select class="form-control" onchange="app.studentSetCycle('+eleve_id+',this.value);">');
	html.push('<option value="false">Définir le cycle</option>');
	for (var j =0,llng= app.cycles.length ; j< llng; j++) {
		
		var selected="";
		if(eleve.eleve_cycle==app.cycles[j]['code']){
			selected="selected";
		}
		html.push('<option value="'+app.cycles[j]['code']+'" '+selected+'>'+app.cycles[j]['nom']+'</option>');
	}
	html.push('</select>');
	document.getElementById('eleve-cycle').innerHTML=html.join('');


	document.getElementById('titre').innerHTML = app.renderEleveNom(eleve);
	app.renderEleveInfos();
	//Sociogramme
	app.renderRelationsForm();	
	//Preparation de l'agenda
	app.agendaEventsSource=[];
	app.eventsEventsSource=[];
	app.etoilesEventsSource=[];
	app.show('eleve_calendar'); 
	app.eleveCalendarRender();
	$('#eleve_agenda').fullCalendar( 'incrementDate', { days:-1});
	//Préparation de la liste des évènements
	app.renderEleveEventsInit();
	//Affichage de la vue élève courante
	app.agendaRender();
	app.renderEleveEvents(eleve.eleve_id);
	//Affichage des pièces
	app.renderEleveEtoiles();
	//Affichage des notes
	app.studentTestsRender(eleve.eleve_id);
	app.notesEtoilesRender(app.etoiles,app.currentEleve.eleve_id);
	app.eleveToggleViews(app.userConfig.currentEleveView);
	app.studentIntelligencesRender();
};

/*---------------
-----------------
------Render------
-----------------
---------------*/
app.eleveToggleViews=function(view) {
	$(".eleve-events-view").removeClass('btn-primary').addClass('btn-default');    
	$("#eleve-events-view-"+view).addClass('btn-primary');  
	if(app.userConfig.currentEleveView!=view){
		app.userConfig.currentEleveView = view;
		
		app.pushUserConfig();	
	}	
	if(view=="calendar"){
		app.hide('eleve_events');
		app.show('eleve-organizer-toolbar','inline-block'); 
		app.show('eleve_calendar'); 
	}
	else{
		app.show('eleve_events');
		app.hide('eleve_calendar'); 
		app.hide('eleve-organizer-toolbar'); 
	}
};
app.renderEleveNavRight=function(){
	var html=['<div class="btn-group">'];
	var num=app.currentClasse.eleves.indexOf(app.currentEleve.eleve_id);
	if(num>0){
		var prev=app.currentEleves[num-1].eleve_id;
		html.push('<div class="btn btn-default" onclick="app.go(\'student/'+prev+'\');"><span class="glyphicon glyphicon-chevron-left"></span></div>');
	}else{
		html.push('<div class="btn btn-default"  disabled="disabled"><span class="glyphicon glyphicon-chevron-left"></span></div>');
	}
	if(num<app.currentEleves.length-1){
		var next=app.currentEleves[num*1+1].eleve_id;
		html.push('<div class="btn btn-default" onclick="app.go(\'student/'+next+'\');"><span class="glyphicon glyphicon-chevron-right"></span></div>');
	}else{
		html.push('<div class="btn btn-default"  disabled="disabled"><span class="glyphicon glyphicon-chevron-right"></span></div>');
	}
	html.push('</div>');
	document.getElementById('eleve-nav-right').innerHTML=html.join('');
}
app.renderEleveNom=function(eleve,cut){
	var nom=eleve.eleve_nom;
	var prenom="";
	if(eleve.eleve_prenom){
		prenom=eleve.eleve_prenom;
	}
	if(!nom){
		return ucfirst(prenom);
	}
	if(nom && !prenom){
		return nom.toUpperCase();
	}
	if(cut){
		if(app.userConfig.sorting<4){
			nom=nom[0]+".";
		}
		if(prenom){
			if(app.userConfig.sorting>=4){
				prenom=prenom[0]+".";
			}
		}
	}
	if(app.userConfig.sorting<4){
		return ucfirst(prenom)+" "+nom.toUpperCase();
	}else{
		return nom.toUpperCase()+" "+ucfirst(prenom);
	}	
};

app.studentOrganizerOpen=function(eleve_id){
	app.go('student/'+ eleve_id + '');
	setTimeout(function(){
		app.eleveToggleViews('calendar');
		$('#eleve_agenda_bloc').goTo();
		app.agendaEditionInit();
		app.setAgendaEventIcon('glyphicon-comment');
		document.getElementById('eleve_event_title').focus();
	},500);

}