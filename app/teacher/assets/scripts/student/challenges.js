/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
/*---------------
-----------------
------Etoiles------
-----------------
---------------*/
app.renderEleveEtoiles=function(){
	if(app.currentView!="eleve"){return;}	

	var etoiles=[];
	$('#eleve_agenda').fullCalendar( 'removeEventSource',  app.etoilesEventsSource );
	app.etoilesEventsSource=[];
	for (var i = 0, lng=app.etoiles.length; i <lng; i++) {
		var etoile=app.etoiles[i];
		if(etoile.etoile_eleve==app.currentEleve.eleve_id){
			etoiles.push(etoile);
		}
	};
	var total=0;
	var nb_etoiles_utilisables=0;
	var html=[];
	var controle={};
	var defi={};
	
	for (var i =0,lng=etoiles.length; i <lng; i++) {
		var etoile=etoiles[i];

		if(etoile.etoile_type=="defi"){
			 defi=app.getDefiById(etoile.etoile_defi);
			if(!defi || defi.defi_user!=app.userConfig.userID){
				continue;
			}
		}else{
				controle=app.getTestById(etoile.etoile_defi);
				if(controle.controle_user!=app.userConfig.userID && !app.userConfig.ppView){
					continue;
				}
			}
		total=total*1+etoile.etoile_value*1;
		if((controle.controle_user===app.userConfig.userID || defi.defi_user==app.userConfig.userID) && (!etoile.etoile_state || etoile.etoile_state<etoile.etoile_value)){
			nb_etoiles_utilisables=nb_etoiles_utilisables*1+(etoile.etoile_value*1-etoile.etoile_state*1);	
		}
		if(etoile.etoile_type=='defi'){
			  var user_pseudo=app.getUserById(defi.defi_user).user_pseudo;
			app.legendsUsers.push(defi.defi_user);
			html.push('<div class="col-sm-4 col-xs-12">');
			html.push('<div class="defi flex-rows">');
			html.push('<div class="defi-icon flex-1"><img style="background-color:#'+app.getColor(user_pseudo)+'" src="assets/lib/educational-icons/svg/'+defi.defi_icon+'.svg"/><br/>');
			html.push('<div class="btn btn-default btn-sm defi-etoiles" onclick="app.useEtoiles(\''+etoile.etoile_id+'\');">');
			html.push(app.renderEtoiles(etoile.etoile_value,defi.defi_etoiles,etoile.etoile_state));
			html.push('</div>');
			html.push('</div>');
			html.push('<div class="defi-info flex-3">');
			html.push('<div class="defi_titre h3">'+defi.defi_titre+'</div>');
			html.push('<div class="defi_description h5">'+defi.defi_description+'</div>');
			html.push('</div>');
			html.push('</div>');
			html.push('</div>');
			app.etoilesEventsSource.push({
				title: defi.defi_titre+' <br/>'+app.renderEtoiles(etoile.etoile_value,defi.defi_etoiles,etoile.etoile_state),
				allDay:true,
				start: etoile.etoile_date*1000,
				backgroundColor:"#ffffff",
				textColor:"#000000",               
				borderColor:"#" + app.getColor(user_pseudo),
				className :"etoile_event",
				event_type:"etoile",
				editable:false,
				event_id:etoile.etoile_id
			});
		}
	};
	if(html.length!=0){
		document.getElementById('eleve_etoiles').style.display = "block";
		document.getElementById('eleve_etoiles_LST').innerHTML=html.join('');
		document.getElementById('eleve_etoiles_btn').innerHTML="";
	}
	else{
		document.getElementById('eleve_etoiles').style.display = "none";
		document.getElementById('eleve_etoiles_btn').innerHTML="<div class='btn btn-default btn-sm' onclick=\"app.go('challenges');\"><span class='glyphicon flaticon-puzzle26'></span> Lancer un défi !</div>";
	}	
	document.getElementById('eleve_etoiles_utilisable').innerHTML=nb_etoiles_utilisables;
	$('#eleve_agenda').fullCalendar( 'addEventSource',  app.etoilesEventsSource );
	app.renderLegendsUsers('eleve_calendar_legende');
	document.getElementById('eleve_etoiles_total').innerHTML=total;
};
app.useEtoiles=function(etoile_id){
	if (!app.checkConnection()) {return;}
	$.post(app.serveur + "index.php?go=etoiles&q=useEtoiles"+app.connexionParam,{
		etoile_id:etoile_id,
sessionParams:app.sessionParams
	}, function(data){
		app.render(data); 
		app.renderEleveEtoiles();
		//app.studentTestsRender();
		app.notesEtoilesRender(app.etoiles,app.currentEleve.eleve_id);

	});
};