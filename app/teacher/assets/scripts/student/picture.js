/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.studentPictureSelect=function(input){ 
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {			
			$('#student-edit-picture').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}	
}

app.studentPictureEditCancel=function(){
	app.studentPictureSet();
 	//app.viewToggle('student-picture-edit');
 }

 app.studentPictureSave=function(){ 
 	if (!app.checkConnection()) {return;}

 	if(app.currentEleve.delete_eleve_picture){
 		app.currentEleve.eleve_picture="";
 		$.post(app.serveur + "index.php?go=eleves&q=deletePicture"+app.connexionParam,
 		{
eleve_id:app.currentEleve.eleve_id,
sessionParams:app.sessionParams
 		},
 		function(data) {
 			app.render(data);   
 		});
 		return;
 	}

 	var data=$('#student-edit-picture').attr('src');	
 	if(data.indexOf('base64')<0){
 		app.studentPictureEditCancel();
 		return;
 	}
 	var eleve_id=app.currentEleve.eleve_id;
//On met à jour les données
var input=document.querySelector('#eleve-picture-input');

if(!input.files[0]){
	return;
}

var form = new FormData();
form.append('file', input.files[0]);
form.append('eleve_id',app.currentEleve.eleve_id);
//app.viewToggle('student-picture-edit');	
$.ajax( {
	url: app.serveur + "index.php?go=eleves&q=addPicture"+ app.connexionParam,
	type: 'POST',
	data: form,
	processData: false,
	contentType: false,
	success:function(data){
		app.render(data);   			
	}
} );

}
app.studentPictureSet=function(){
	var photo="assets/favicons/apple-touch-icon-72x72.png";
	if(app.isConnected){
		if(app.currentEleve.eleve_picture){
			photo=app.serveur + "index.php?go=eleves&q=getPicture&file="+app.currentEleve.eleve_picture+ app.connexionParam;
		}
		else{
			photo=''+app.serveur+'/lib/monsterid/monsterid.php?seed='+app.currentEleve.eleve_id+'&size=120';
		}
	}
	$('#student-picture').attr('src', photo);
	$('#student-edit-picture').attr('src', photo);
}

app.studentPictureGet=function(eleve_id){
	var eleve=app.getEleveById(eleve_id);
	var photo="assets/favicons/apple-touch-icon-72x72.png";
	if(app.isConnected){
		if(eleve.eleve_picture){
			photo=app.serveur + "index.php?go=eleves&q=getPicture&file="+eleve.eleve_picture+ app.connexionParam;
		}
		else{
			photo=''+app.serveur+'/lib/monsterid/monsterid.php?seed='+eleve_id+'&size=120';
		}
	}
	return photo;
}

app.studentPictureSelectDelete=function(){
	app.currentEleve.delete_eleve_picture=true;	
	$('#student-edit-picture').attr('src', app.serveur+'/lib/monsterid/monsterid.php?seed='+app.currentEleve.eleve_id+'&size=120');
	app.show('eleve_infos_save');
}