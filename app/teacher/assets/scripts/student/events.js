/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
  /*---------------
-----------------
------Events------
-----------------
---------------*/
app.renderEleveEventsInit=function() {
	var html = [];
	html.push('<table class="table">');
	html.push('<tr>');
	html.push('<td>');
	html.push('</td>');
	for (var i =0,lng= app.periodes.length ; i <lng; i++) {
		var periode=app.periodes[i];
		if(periode.periode_type=="m" || periode.periode_parent!=app.active_periode.periode_parent){continue;}
		html.push('<td>');
		html.push(periode.periode_titre);
		html.push('</td>');
	}
	html.push('<td>');
	html.push('</td>');
	html.push('</tr>');

	for (var k = 0, lng = app.eventsAvailable.length; k < lng; k++) {
		var event_type=app.eventsAvailable[k].type;
		if(app.userConfig.eventsWhiteList.indexOf(event_type)<0){continue;}
		var legende=app.legends[event_type]||"";
		html.push('<tr id="student-events-'+event_type+'" class="student-events-row" onclick="app.renderEleveEventsDetails(\''+event_type+'\');">');
		html.push('<td class="text-left">');
		html.push('<span class="glyphicon ' + event_type + '"></span> ');		
		html.push(legende);
		html.push('</td>');
		for (var i =0,llng= app.periodes.length ; i <llng; i++) {
			var periode=app.periodes[i];
			if(periode.periode_type=="m" || periode.periode_parent!=app.active_periode.periode_parent){continue;}
			html.push('<td class="h5">');
			html.push('<span id="eleveEvents_'+periode.periode_id+'_' + event_type + '_counter_enable">0</span> <small>/<span id="eleveEvents_'+periode.periode_id+'_' + event_type + '_counter_total">0</span></small>');
			html.push('</td>');
		}
		html.push('<td>');
		html.push('<div class="btn-group eleveEventsListBtn">');
		html.push('<div class="btn btn-default btn-xs" title="Tout barrer" onclick="app.studentEventsCheckAll(\''+event_type+'\');"><span class="glyphicon glyphicon-font barre"></span></div>');
		html.push('<div class="btn btn-default btn-xs" title="Tout supprimer" onclick="app.eleveEventsDeleteAll(\''+event_type+'\');"><span class="glyphicon glyphicon-trash"></span></div>');
		html.push('</div>');
		html.push('</td>');
		html.push('</tr>');
	}

	html.push('</table>');
	document.getElementById('eleveEventsLST').innerHTML = html.join('');
};
app.renderEleveEvents=function() {	
	var eleve_id=app.currentEleve.eleve_id;
	var html=[];
	app.eleveEventsList=[];
	var tabEventsCounter=[];
	for (var i = app.agenda.length - 1; i >= 0; i--) {
		var oEvent = app.agenda[i];
		if(app.userConfig.eventsWhiteList.indexOf(oEvent.event_icon)<0){continue;}
		if (oEvent.event_type_id != eleve_id || oEvent.event_type!="eleve") {continue;}
		if(oEvent.event_user != app.userConfig.userID && !app.userConfig.ppView){continue;}
		app.eleveEventsList.push(oEvent);
		var periode=oEvent.event_periode||-1;
		tabEventsCounter[periode]=tabEventsCounter[periode]||[];
		if (oEvent.event_state == "disable") {
		}else{
			if(tabEventsCounter[periode][oEvent.event_icon+'_enable']){
				tabEventsCounter[periode][oEvent.event_icon+'_enable']++;
			}else{
				tabEventsCounter[periode][oEvent.event_icon+'_enable']=1;
			}
		}			
		if(tabEventsCounter[periode][oEvent.event_icon+'_total']){
			tabEventsCounter[periode][oEvent.event_icon+'_total']++;
		}else{
			tabEventsCounter[periode][oEvent.event_icon+'_total']=1;
		}
	}
	for (var k = 0, lng = app.eventsAvailable.length; k < lng; k++) {
		var event_type=app.eventsAvailable[k].type;
		var is_visible=false;
		if(app.userConfig.eventsWhiteList.indexOf(event_type)<0){continue;}
		for (var i = app.periodes.length - 1; i >= -1; i--) {
			var periode=app.periodes[i]||{
				periode_id:-1,
				periode_titre:"Non classés"
			};	
			tabEventsCounter[periode.periode_id]=tabEventsCounter[periode.periode_id]||[];
			var total=tabEventsCounter[periode.periode_id][event_type+'_total'];
			if(total){
				is_visible=true;
				document.getElementById('eleveEvents_'+periode.periode_id+'_' + event_type + '_counter_total').innerHTML=total;
				document.getElementById('eleveEvents_'+periode.periode_id+'_' + event_type + '_counter_enable').innerHTML=tabEventsCounter[periode.periode_id][event_type+'_enable']||0;
			}	
		}
		if(!is_visible){
			$('#student-events-'+event_type+'').css('display','none');
		}else{				
			$('#student-events-'+event_type+'').css('display','');
		}	
	}
	if(app.eleveEventsListCurrent!=""){
		app.renderEleveEventsDetails(app.eleveEventsListCurrent);
	}
};
app.renderEleveEventsDetails=function(icon) {	
	app.eleveEventsListCurrent=icon;
	var eleve_id=app.currentEleve.eleve_id;
	var legende=app.legends[icon]||"";
	app.eleveEventsList=app.orderBy(app.eleveEventsList,"event_start","DESC");
	var html=[];	
	html.push("<div class='h4'>");
	html.push('<span class="glyphicon ' + icon + '"></span> ');	
	html.push(legende);
	html.push("</div>");
	var n=0;
	for (var i = app.eleveEventsList.length - 1; i >= 0; i--) {
		var oEvent = app.eleveEventsList[i];
		if(app.userConfig.eventsWhiteList.indexOf(oEvent.event_icon)<0){continue;}
		if (oEvent.event_type_id != eleve_id || oEvent.event_type!="eleve" || oEvent.event_user != app.userConfig.userID){
			continue;
		}
		if(oEvent.event_icon!=icon){
			continue;
		}
		n++;
		var date = moment(parseInt(oEvent.event_start*1000)).format('DD MMM YYYY à HH[h]mm');
		var eventStateClass = "event_enable";
		var eventStateUrl = "disable";
		if (oEvent.event_state == "disable") {
			eventStateClass = "event_disable";
			eventStateUrl = "enable";
		}
		html.push('<div id="event_' + oEvent.event_id + '" class="eleve-event-liste-item text-center ' + eventStateClass + '" >');
		html.push(date);
		html.push('<div class="btn-group pull-right">');
		html.push('<div class="btn btn-default btn-xs" title="Barrer"  onclick="app.agendaEventUpdate({event_id:'+oEvent.event_id+',event_state:\''+eventStateUrl+'\'});"><span class="glyphicon glyphicon-font barre"></span></div>');
		html.push('<div class="btn btn-default btn-xs" onclick="event.cancelBubble=true;app.agendaEventDelete(\'' + oEvent.event_id + '\',false,app.renderEleveEvents);"><span class="glyphicon glyphicon-trash"></span></div>');
		html.push('</div>');
		html.push('</div>');
	}
	if(n==0){
		html.push("<div>Aucun événement de ce type</div>");
	}
	document.getElementById('eleveEventsLST_details').innerHTML=html.join('');
};
app.studentEventsCheckAll=function(event_type,confirm){
	if(!confirm){
		app.alert({title:'Barrer tous les éléments de ce type ?',type:'confirm'},function(){app.studentEventsCheckAll(event_type,true);});
		return;
	}
	var eleve=app.currentEleve;
	for (var i = 0, lng = app.agenda.length; i < lng; i++) {
		var oEvent = app.agenda[i];		
		if(oEvent.event_icon!=event_type){continue;}
		if (oEvent.event_type_id == eleve.eleve_id && oEvent.event_type=="eleve" && oEvent.event_user == app.userConfig.userID) {
			app.agendaEventUpdate({
				event_id:oEvent.event_id,
				event_state:"disable"
			});
		}
	}
}
app.eleveEventsDeleteAll=function(event_type,confirm){
	if(!confirm){
		app.alert({title:'Supprimer tous les éléments de ce type ?',type:'confirm'},function(){app.eleveEventsDeleteAll(event_type,true);});
		return;
	}
	var eleve=app.currentEleve;
	for (var i = 0, lng = app.agenda.length; i < lng; i++) {
		var oEvent = app.agenda[i];		
		if(oEvent.event_icon!=event_type){continue;}
		if (oEvent.event_type_id == eleve.eleve_id && oEvent.event_type=="eleve" && oEvent.event_user == app.userConfig.userID) {
			app.agendaEventDelete(oEvent.event_id,true);
		}
	}
}