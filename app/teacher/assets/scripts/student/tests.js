/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
/*---------------
-----------------
------Notes------
-----------------
---------------*/
app.studentTestsRender=function(eleve_id,bilan){
	//if(app.currentView!="eleve" && ){return;}
	document.getElementById('eleve_notes').style.display = "none";
	var millesime=app.getPeriodeById(app.active_periode.periode_parent);
	var notes=[];
	$('#eleve_agenda').fullCalendar( 'removeEventSource', app.notesEventsSource );
	app.notesEventsSource=[];
	var notes_events=[];
	for (var i =0,lng= app.notes.length; i <lng; i++) {			
		var note=app.notes[i];
		if(note.note_eleve!=eleve_id){continue;}
		var controle=app.getTestById(note.note_controle);
		var user=app.getUserById(controle.controle_user);
		if(!app.userConfig.ppView && (user.user_pseudo!=ucfirst(app.pseudo_utilisateur))){continue;}
		note.note_num=i;
		notes.push(note);			
	};
	var lng=notes.length;
	if(lng==0){return;}
	var html_final=[];
	document.getElementById('eleve_notes_LST').innerHTML="";
	var periodes=app.notesSortByPeriodes(notes);
	for (var i=0; i<periodes.length; i++) {
		var notesBymatieres=[];
		var matieres=[];
		for (var j =0,lng=periodes[i].notes.length; j<lng; j++) {
			var note=periodes[i].notes[j];
			var controle=app.getTestById(note.note_controle);
			var user=app.getUserById(controle.controle_user);
			var matiere=note.note_matiere;
			var classe=app.getClasseById(controle.controle_classe)
			if(!notesBymatieres[matiere]){
				notesBymatieres[matiere]=[];
				matieres.push(matiere);
			}
			notesBymatieres[matiere].push(note);
		};			
		var html=[];
		html.push('<div class="h4 text-center">Relevé de notes - '+millesime.periode_titre+' - '+periodes[i].titre+'</div>');
		for (var k =0, lllng=matieres.length ; k <lllng; k++) {
			var matiere=matieres[k];
			html.push('<div class="well well-sm col-lg-12">');
			if(k!=0){
			}
			html.push('<div class="h4 col-lg-12 bilans-title">');
			html.push('<span><strong>'+ ucfirst(matiere)+'</strong></span> - <span><strong>'+app.moyenne(notesBymatieres[matiere])+'</strong><small>/20</small></span>');
			html.push('</div>');
			html.push('<span class="col-lg-12">');
			for (var l =0, llllng= notesBymatieres[matiere].length ; l<llllng; l++) {
				var note=notesBymatieres[matiere][l];
				var controle=app.getTestById(note.note_controle);
				var user=app.getUserById(controle.controle_user);
				if(l!=0){
					html.push(' - ');	
				}
				app.legendsUsers.push(user.user_id);
				html.push('<span id="eleve_note_'+note.note_id+'" class="btn-group h4 student-note" style="border-color:#' + app.getColor(user.user_pseudo) + ';" >');
				html.push('<span class="btn btn-default eleve_notes" onclick="app.renderEditNote('+note.note_num+');" data-toggle="tooltip" data-placement="top" title="'+ucfirst(controle.controle_titre)+' - Coeff. '+controle.controle_coefficient+'">');
				var note_render="";
				if(isNaN(note.note_value)){
					html.push(note.note_value);
					note_render=note.note_value;
				}
				else{
					var bonus="";
					if(note.note_bonus!=0){
						bonus="(+"+note.note_bonus+")";
					}
					html.push(note.note_value+bonus+'<small>/'+controle.controle_note+'</small>');
					note_render=note.note_value+bonus+'/'+controle.controle_note;
				}							
				html.push('</span>');
				html.push(' <div class="btn btn-default note_etoiles" style="display:none;" id="eleve_note_etoile_'+controle.controle_id+'" data-toggle="tooltip" data-placement="top" title="Marquer ces pièces."></div>');
				html.push('</span>');
				app.notesEventsSource.push({
					title: ucfirst(controle.controle_titre)+' <br/> '+note_render+'<br/><span id="eleve_note_event_etoile_'+controle.controle_id+'"></span>',
					allDay:true,
					note_id:note.note_id,
					start: note.note_date*1000,
					backgroundColor:"#ffffff",
					textColor:"#000000",               
					borderColor:"#" + app.getColor(user.user_pseudo),
					className :"note_event",
					editable:false,
					event_type:"note"
				});
			};
			html.push('</span>');
			html.push('</div>');
		};		
		html_final.push(html.join(''));
		
	}		

	if(bilan){
		return html_final.join('');
	}
	document.getElementById('eleve_notes_LST').innerHTML+=html.join('');

	document.getElementById('eleve_notes').style.display = "block";
	document.getElementById('eleve_notes_LST').innerHTML+='<div id="notes_graphique" class="col-sm-12" style="width:100%; height:200px;"></div>';
	app.notesGraphiqueRender(notes);
	app.renderLegendsUsers('eleve_calendar_legende');
	$('.eleve_notes').tooltip();
	$('.note_etoiles').tooltip();
	$('#eleve_agenda').fullCalendar( 'addEventSource', app.notesEventsSource );
};
app.renderEditNote=function(note_num){
	var note=app.notes[note_num];
	var controle=app.getTestById(note.note_controle);
	var user=app.getUserById(controle.controle_user);
	var html=[];
	html.push('<div class="col-lg-12 text-center well well-sm" id="eleve_note_form">');
	html.push('<span class="h4">'+controle.controle_titre+'</span>'); 
	html.push('<span class="input-group">'); 
	html.push('<span class="input-group-addon">Note</span>');
	html.push('<input type="text" id="eleve_note_form_value" placeholder="'+note.note_value+'" value="'+note.note_value+'" class="form-control"/>');
	html.push('</span>');
	html.push('<span class="input-group">'); 
	html.push('<span class="input-group-addon">Point bonus</span>');
	html.push('<input type="text"  id="eleve_note_form_bonus" value="'+note.note_bonus+'" placeholder="0" class="form-control"/>');
	html.push('</span>');
	html.push('<div class="btn-group">');
	html.push(' <div class="btn btn-default btn-sm" onclick="document.getElementById(\'note_eleve_edit\').style.display=\'none\';" >Annuler</div> ');
	html.push('<div class="btn btn-primary btn-sm" id="eleve_note_btn" onclick="app.updNote('+note_num+');$(this).button(\'loading\');" data-loading-text="Enregistrement..."><span class="glyphicon glyphicon-save"></span> Enregistrer</div>');
	html.push('</div>');
	html.push('</div>');
	$('#note_eleve_edit').html(html.join('')).css('display','block').goTo();
}  
app.updNote=function(note_num){
	var note=app.notes[note_num];
	var value=document.getElementById('eleve_note_form_value').value;
	var bonus=document.getElementById('eleve_note_form_bonus').value;
	var controle=app.getTestById(note.note_controle);
	controle.previous_controles=app.getPreviousControles(controle);
	var temp_progression=app.getProgressionByEleve(note,controle,value);
	$.post(app.serveur + "index.php?go=notes&q=update"+ app.connexionParam,{
		eleve_id:app.currentEleve.eleve_id,
		note_id:note.note_id,
		note_value:value,
		note_bonus:bonus,
		sessionParams:app.sessionParams
	},function(data) {
		app.notes[note_num].note_value=value.replace(",", ".");
		app.notes[note_num].note_bonus=bonus.replace(",", ".");
		app.render(data);          
		app.studentTestsRender(app.currentEleve.eleve_id);
		$('#eleve_note_btn').button('reset');
		document.getElementById('note_eleve_edit').style.display="none";
		app.notesEtoilesRender(app.etoiles,app.currentEleve.eleve_id);
	});
	var etoiles=app.getEtoilesByProgression(temp_progression[0]);
	if(etoiles>0){
		var tab_etoiles=[];
		tab_etoiles.push([app.currentEleve.eleve_id,etoiles]);
		$.post(app.serveur + "index.php?go=etoiles&q=add"+app.connexionParam, {
			classe_id:app.currentClasse.classe_id,
			etoiles:JSON.stringify(tab_etoiles),
			defi_id:note.note_controle,
			etoile_type:'progression',
			time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
		},function(data) {
			app.render(data);  
		}
		);
	}

};
app.notesEtoilesRender=function(etoiles,eleve_id){
	for (var i = etoiles.length - 1; i >= 0; i--) {
		var etoile=etoiles[i];
		if(etoile.etoile_type==="progression" && etoile.etoile_eleve===eleve_id){
			var controle=app.getTestById(etoile.etoile_defi);
			var html=[];
			html.push(app.renderEtoiles(etoile.etoile_value,3,etoile.etoile_state));
			var div=document.getElementById('eleve_note_etoile_'+controle.controle_id+'');			
			if(div){
				div.innerHTML=html.join('');
				div.style.display="block";				
				(function(id){div.onclick=function(){app.useEtoiles(id)};})(etoile.etoile_id);		
			}
			div=document.getElementById(prefix+'eleve_note_event_etoile_'+controle.controle_id+'');
			if(div){
				div.innerHTML=html.join('');			
				(function(id){div.onclick=function(){app.useEtoiles(id)};})(etoile.etoile_id);	
			}
		}
	};
}
app.notesGraphiqueRender=function(notes){
	var series=[];
	var tabMatieres=[];
	var div='#notes_graphique';	
	//notes.sort(function(a,b) { return parseFloat(a.controle_date) - parseFloat(b.controle_date) } );
	for (var i =0,lng= notes.length; i <lng; i++) {
		var note=notes[i];
		var controle=app.getTestById(note.note_controle);
		var user=app.getUserById(controle.controle_user);
		var classe=app.getClasseById(controle.controle_classe)
		if(isNaN(note.note_value)){continue;}
		var matiere=note.note_matiere+' - '+user.user_pseudo;
		
		if(!app.userConfig.ppView){
			if(controle.controle_categorie){
				matiere+=' - '+controle.controle_categorie;
			}
		}
		var index=tabMatieres.indexOf(matiere);
		if(index==-1){
			index=tabMatieres.length;
			series[index]={};
			series[index].type="line";
			if(app.userConfig.ppView){
				series[index].color="#"+app.getColor(user.user_pseudo);
			}			
			series[index].name=ucfirst(matiere),
			series[index].data=[]; 
			tabMatieres.push(matiere);    
		}
		series[index].data.push([controle.controle_date*1000,note.note_value*1]);
	};
	$(div).highcharts({
		chart: {
			type: 'spline'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: document.ontouchstart === undefined ?
			'Click and drag in the plot area to zoom in' :
			'Pinch the chart to zoom in'
		},
		xAxis: {
			type: 'datetime',
            minRange: 1 * 24 * 3600000*365 // one days
        },
        yAxis: {
        	min:0,
        	title: {
        		text: ''
        	}
        },        
        legend: {
        	layout: 'vertical',
        	align: 'left',
        	verticalAlign: 'middle',
        	borderWidth: 0,
        	useHTML:true
        },      
        series: series
    });
}
app.notesSortByPeriodes=function(notes){
	var notesByPeriodes=[];
	var notesNonClassees=[];
	var periodes=[];
	var periodesTitres=[];
	var k=0;
	for (var i = 0, lng= notes.length ; i <lng; i++) {
		var note=notes[i];
		note.num=i;
		var controle=app.getTestById(note.note_controle);
		var user=app.getUserById(controle.controle_user);
		if(app.userConfig && !app.userConfig.ppView && user.user_id!=app.userConfig.userID){ continue; }
		//On en profite pour lier la note à une matière ou à un professeur
		var discipline_name=user.user_pseudo;
		var discipline=app.getDisciplineById(controle.controle_discipline);
		if(discipline){
			discipline_name=discipline.discipline_name;
		}
		note.note_matiere=discipline_name;			
		var titre=app.getPeriodeById(controle.controle_periode).periode_titre;
		var index=periodesTitres.indexOf(titre);
		if(index<0){
			periodesTitres.push(titre);
			periodes[k]={
				titre:titre,
				notes:[]
			};
			k++;
			index=periodes.length-1;				
		}
		periodes[index].notes.unshift(note);
	};
	return periodes;
}