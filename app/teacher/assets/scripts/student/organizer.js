/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.eleveCalendarRender=function(){
	document.getElementById('eleve_agenda').style.display = "flex";
	$('#eleve_agenda').fullCalendar('destroy');
	$('#eleve_agenda').fullCalendar({
		lang: 'fr',		
		timezone:'local',
		editable: true,
		selectable:true,
		selectHelper:true,
		nowIndicator:true,
		unselectAuto:true,
		contentHeight:550,
		defaultTimedEventDuration:"01:00:00",
		slotEventOverlap:false,
		unselectCancel:"#eleve_event_edition",
		defaultView: app.userConfig.eleveAgendaCurrentView,
		allDaySlot: true,
		slotLabelFormat:"HH:mm",
		firstDay: 1,		
		minTime:app.userConfig.organizerMinTime,
		maxTime:app.userConfig.organizerMaxTime,
		height:"auto",
		timeFormat: 'HH:mm',
		eventBackgroundColor:"#F5F5F5",
		eventBorderColor:"#E3E3E3",
		eventTextColor:"black",
		scrollTime:"07:30:00",
		slotDuration:app.userConfig.organizerSlotDuration,
		columnFormat:'ddd DD/MM/YY',
		weekends:app.userConfig.agendaWeekEnds,
		views: {
			basicThreeDay: {
				type: 'basic',
				duration: { days: 3 },
				buttonText: '3 jours',
				columnFormat:'ddd DD/MM/YY'
			},
			month: {
				columnFormat:'ddd'
			}
		},
		header: {
			left: '',
			center: '',
			right: ''
		}, 
		eventClick: function(calEvent, jsEvent, view) {
			if(calEvent.event_type=="etoile"){

				app.useEtoiles(calEvent.event_id);
				return;
			}
			if (calEvent.event_type=="note") {
				return;
			}
			if(view.name=="agendaWeek" || view.name=="agendaDay" || view.name=="basicThreeDay"){
				if(calEvent.event_type=='agenda'){
					app.agendaOneEventDetailsToggle(calEvent.event_id);
				}
			}
			else{
				app.currentClasseCalendarDate=$('#eleve_agenda').fullCalendar( 'getDate' );
				var  event_state="enable";
				if(calEvent.event_state=="enable"){
					event_state="disable";
				}
				app.agendaEventUpdate({
					event_id:calEvent.event_id,
					event_state:event_state
				});
			}
		},
		select:function(start, end, jsEvent, view,resource){
			app.agendaCurrentEventEdit=null;
			if(document.getElementById('eleve_event_edition').style.display=='none'){
				app.agendaEditionInit();
			}
			$('#eleve_event_edition').goTo();
			app.agendaNewEventStart=start;
			app.agendaNewEventEnd=end;
			var allDay=false;
			if(!start.hasTime()){
				allDay=true;
			}
			if(view.name=="agendaDay"){
				if(allDay){
					document.getElementById('eleve_event_date').innerHTML="le "+start.format("DD MMM YYYY");
				}
				else{
					document.getElementById('eleve_event_date').innerHTML="de "+start.format("HH[h]mm")+" à "+end.format("HH[h]mm");
				}
			}
			else{
				document.getElementById('eleve_event_date').innerHTML="du "+start.format("DD MMM YYYY")+" au "+end.format("DD MMM YYYY");
			}
		},
		viewRender:function( view, element ){
			app.agendaNewEventStart=null;
			app.agendaNewEventEnd=null;
			$('#eleve_agenda').fullCalendar( 'unselect' );
			$(".eleve-agenda-view").removeClass('btn-primary').addClass('btn-default');    
			$("#eleve-agenda-view-"+view.name).addClass('btn-primary');  
			document.getElementById('eleve-agenda-semaine').innerHTML=app.getWeekName(  $('#eleve_agenda').fullCalendar( 'getDate' ).unix()*1000);
			
			if(app.userConfig.eleveAgendaCurrentView!=view.name){    
				app.userConfig.eleveAgendaCurrentView=view.name;
				
				app.pushUserConfig(); 
			}
			var format="";
			if(view.name=="month"){
				format="MMMM YYYY";
				document.getElementById('eleve-agenda-date').innerHTML= $('#eleve_agenda').fullCalendar( 'getDate' ).format(format);
			}
			else if(view.name=="basicThreeDay"){
				format="DD MMM YYYY";
				document.getElementById('eleve-agenda-date').innerHTML= $('#eleve_agenda').fullCalendar( 'getDate' ).add({day:1}).format(format);
			}
			else{
				format="DD MMM YYYY";
				document.getElementById('eleve-agenda-date').innerHTML= $('#eleve_agenda').fullCalendar( 'getDate' ).format(format);
			}
			app.agendaEventDetailsToggle(app.userConfig.agendaEventDetails,false);
		},
		eventMouseover:function( event, jsEvent, view ) {
			this.style.zIndex=5;
		},
		eventMouseout:function( event, jsEvent, view ) {
			this.style.zIndex=1;
		},
		eventDrop:function( event, jsEvent, ui, view ) {
			event.event_type=event.event_type||null;
			if(event.event_type!="agenda"){
				return;
			}
			var oEvent={
				event_id:event.event_id,
				event_start:event.start.unix()
			}
			if(!event.start.hasTime()){
				oEvent.event_allDay=true;
			}
			else{
				oEvent.event_allDay=false;
			}	
			if(!event.end || !event.end.hasTime()){
				oEvent.event_end=event.start.unix();
			}else{
				oEvent.event_end=event.end.unix();     
			}
			app.currentClasseCalendarDate=$('#eleve_agenda').fullCalendar( 'getDate' );
			if(event.event_type=="agenda"){
				app.agendaEventUpdate(oEvent,false);
			}
			else if(event.event_type=="event"){
			}
		},
		eventResize:function( event, jsEvent, ui, view ) {
			event.event_type=event.event_type||null;
			if(event.event_type!="agenda"){
				return false;
			}
			var oEvent={
				event_id:event.event_id,
				event_start:event.start.unix()
			}
			if(!event.start.hasTime()){
				oEvent.event_allDay=true;
			}
			else{
				oEvent.event_allDay=false;
			}
			if(event.end){
				oEvent.event_end=event.end.unix();
			}
			app.agendaEventUpdate(oEvent,false);
		}
	});
}