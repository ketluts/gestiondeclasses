/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

var app=app || {};
app.getEleveById=function(eleve_id) {
	return app.eleves[app.elevesIndex[eleve_id]];
};
app.buildElevesIndex=function(){
	var classe_fictive_num=app.getClasseById(-1).num;
	app.elevesIndex=[];
	for (var i = 0, lng = app.eleves.length; i < lng; i++) {
		app.elevesIndex[app.eleves[i].eleve_id]=i;
		app.eleves[i].eleve_num=i;    		
		app.classes[classe_fictive_num].eleves.push(app.eleves[i].eleve_id);
	}
}