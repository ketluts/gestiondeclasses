/*
Copyright 2018 Pierre GIRARDOT

This file is part of GestionDeClasses.

GestionDeClasses is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version GPL-3.0-or-later of the License.

GestionDeClasses is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GestionDeClasses.  If not, see <https://www.gnu.org/licenses/>.*/

app.studentRemoveFromClassroom=function(confirm) {
	if (!app.checkConnection()) {
		return;
	}
	if(!confirm){
		app.alert({title:'Retirer cet élève de la classe ?',type:'confirm'},function(){app.studentRemoveFromClassroom(true);});
		return;
	}
	$.post(app.serveur + "index.php?go=classe&q=removeStudent" + app.connexionParam,{
		classe_id:app.currentClasse.classe_id,
		eleve_id:app.currentEleve.eleve_id,
		sessionParams:app.sessionParams
	},function(data){
		app.go('classroom/'+app.currentClasse.classe_id);
		app.render(data);
	});
};
app.studentDelete=function(confirm) {
	if (!app.checkConnection()) {
		return;
	}
	if(!confirm){
		app.alert({title:'Supprimer cet élève et TOUTES SES DONNÉES ?',type:'confirm'},function(){app.studentDelete(true);});
		return;
	}
	$.post(app.serveur + "index.php?go=eleves&q=delete" + app.connexionParam,{
		eleves:JSON.stringify([app.currentEleve.eleve_id]),
		time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
	},function(data){
		if(app.currentClasse){
			app.go('classroom/'+app.currentClasse.classe_id);
		}else{
			app.go('home');
		}
		app.render(data);
	});
};
app.studentsDelete=function(confirm){
	if (!app.checkConnection()) {
		return;
	}
	var ids=[];

	for (var i = 0, lng=app.currentClasse.eleves.length ; i<lng; i++){
		var eleve= app.getEleveById(app.currentClasse.eleves[i]);
      //  if(message.message_parent_message!="-1"){continue;}
    //if(document.getElementById("discussion_"+message.message_id+"")){     
    	if(document.getElementById("student_"+eleve.eleve_id+"_checkbox").checked){
    		ids.push(eleve.eleve_id);
    	}
    //}
}

if(ids.length>0){

	if(!confirm){
		app.alert({title:'Supprimer ce'+app.pluralize(ids.length,'s','t')+' élève'+app.pluralize(ids.length,'s','')+' et TOUTES '+app.pluralize(ids.length,'LEURS','SES')+' DONNÉES ?',type:'confirm'},function(){app.studentsDelete(true);});
		return;
	}
	$.post(app.serveur + "index.php?go=eleves&q=delete" + app.connexionParam,{
		eleves:JSON.stringify(ids),
		time:Math.floor(app.myTime()/1000),
sessionParams:app.sessionParams
	},function(data){
		//if(app.currentClasse){
					// app.go('classroom/'+app.currentClasse.classe_id);
					
		// }else{
		// 	app.go('home');
		// }
		app.render(data);
		app.classeExportRender();
	});

}
}